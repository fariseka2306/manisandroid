package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class Address implements Parcelable{
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("postcode")
    @Expose
    public int postcode;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.city);
        dest.writeInt(this.postcode);
    }

    public Address() {
    }

    protected Address(Parcel in) {
        this.city = in.readString();
        this.postcode = in.readInt();
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel source) {
            return new Address(source);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };
}
