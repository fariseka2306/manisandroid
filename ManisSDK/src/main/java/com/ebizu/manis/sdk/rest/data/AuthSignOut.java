package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;

/**
 * Created by rezkya on 17/05/16.
 */
public class AuthSignOut {
    public static class RequestBody {
    }

    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody>{

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response {
    }
}
