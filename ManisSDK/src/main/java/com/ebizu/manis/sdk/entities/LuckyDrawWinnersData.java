package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Deni Rohimat on 24/07/17.
 */
public class LuckyDrawWinnersData implements Parcelable {

    @SerializedName("more") private boolean more;
    @SerializedName("winner_title") private String winnerTitle;
    @SerializedName("winners") private List<LuckyDrawWinner> winners;

    public boolean isMore() {
        return more;
    }

    public String getWinnerTitle() {
        return winnerTitle;
    }

    public List<LuckyDrawWinner> getWinners() {
        return winners;
    }

    public void setMore(boolean more) {
        this.more = more;
    }

    public void setWinnerTitle(String winnerTitle) {
        this.winnerTitle = winnerTitle;
    }

    public void setWinners(List<LuckyDrawWinner> winners) {
        this.winners = winners;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.more ? (byte) 1 : (byte) 0);
        dest.writeString(this.winnerTitle);
        dest.writeTypedList(this.winners);
    }

    public LuckyDrawWinnersData() {
    }

    protected LuckyDrawWinnersData(Parcel in) {
        this.more = in.readByte() != 0;
        this.winnerTitle = in.readString();
        this.winners = in.createTypedArrayList(LuckyDrawWinner.CREATOR);
    }

    public static final Creator<LuckyDrawWinnersData> CREATOR = new Creator<LuckyDrawWinnersData>() {
        @Override
        public LuckyDrawWinnersData createFromParcel(Parcel source) {
            return new LuckyDrawWinnersData(source);
        }

        @Override
        public LuckyDrawWinnersData[] newArray(int size) {
            return new LuckyDrawWinnersData[size];
        }
    };
}
