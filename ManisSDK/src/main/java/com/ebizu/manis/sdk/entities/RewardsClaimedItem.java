package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkyatinnov on 7/1/16.
 */
public class RewardsClaimedItem implements Parcelable {

    public static String REWARD_ZERO_POINT = "0";
    public static String REWARD_FREE_POINT = "FREE";

    @SerializedName("redeemed")
    @Expose
    private int redeemed;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("expired")
    @Expose
    private int expired;
    @SerializedName("store")
    @Expose
    private String store;
    @SerializedName("assets")
    @Expose
    private Assets assets;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("sn")
    @Expose
    private String sn;
    @SerializedName("point")
    @Expose
    private String point;
    @SerializedName("delivery_status")
    @Expose
    private int deliveryStatus;
    @SerializedName("shipment_date")
    @Expose
    private String shipmentDate;
    @SerializedName("shipment_number")
    @Expose
    private String shipmentNumber;
    @SerializedName("shipment_expedition")
    @Expose
    private String shipmentExpedition;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("reward_type")
    @Expose
    private String rewardType;
    @SerializedName("track_link")
    @Expose
    private String trackLink;

    public RewardsClaimedItem() {
    }

    public int getRedeemed() {
        return redeemed;
    }

    public void setRedeemed(int redeemed) {
        this.redeemed = redeemed;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getExpired() {
        return expired;
    }

    public void setExpired(int expired) {
        this.expired = expired;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public Assets getAssets() {
        return assets;
    }

    public void setAssets(Assets assets) {
        this.assets = assets;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public int getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(int deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(String shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public String getShipmentExpedition() {
        return shipmentExpedition;
    }

    public void setShipmentExpedition(String shipmentExpedition) {
        this.shipmentExpedition = shipmentExpedition;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getTrackLink() {
        return trackLink;
    }

    public void setTrackLink(String trackLink) {
        this.trackLink = trackLink;
    }

    public static class Assets implements Parcelable {

        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;
        @SerializedName("logo")
        @Expose
        private String logo;

        public Assets(String thumbnail, String logo) {
            this.thumbnail = thumbnail;
            this.logo = logo;
        }

        public Assets() {
        }

        public Assets(Parcel in) {
            thumbnail = in.readString();
            logo = in.readString();
        }

        public static final Creator<Assets> CREATOR = new Creator<Assets>() {
            @Override
            public Assets createFromParcel(Parcel in) {
                return new Assets(in);
            }

            @Override
            public Assets[] newArray(int size) {
                return new Assets[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(thumbnail);
            dest.writeString(logo);
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.redeemed);
        dest.writeString(this.datetime);
        dest.writeString(this.name);
        dest.writeInt(this.expired);
        dest.writeString(this.store);
        dest.writeParcelable(this.assets, flags);
        dest.writeString(this.type);
        dest.writeString(this.description);
        dest.writeString(this.code);
        dest.writeString(this.sn);
        dest.writeString(this.point);
        dest.writeInt(this.deliveryStatus);
        dest.writeString(this.shipmentDate);
        dest.writeString(this.shipmentNumber);
        dest.writeString(this.shipmentExpedition);
        dest.writeString(this.notes);
        dest.writeString(this.rewardType);
        dest.writeString(this.trackLink);
    }

    protected RewardsClaimedItem(Parcel in) {
        this.redeemed = in.readInt();
        this.datetime = in.readString();
        this.name = in.readString();
        this.expired = in.readInt();
        this.store = in.readString();
        this.assets = in.readParcelable(Assets.class.getClassLoader());
        this.type = in.readString();
        this.description = in.readString();
        this.code = in.readString();
        this.sn = in.readString();
        this.point = in.readString();
        this.deliveryStatus = in.readInt();
        this.shipmentDate = in.readString();
        this.shipmentNumber = in.readString();
        this.shipmentExpedition = in.readString();
        this.notes = in.readString();
        this.rewardType = in.readString();
        this.trackLink = in.readString();
    }

    public static final Creator<RewardsClaimedItem> CREATOR = new Creator<RewardsClaimedItem>() {
        @Override
        public RewardsClaimedItem createFromParcel(Parcel source) {
            return new RewardsClaimedItem(source);
        }

        @Override
        public RewardsClaimedItem[] newArray(int size) {
            return new RewardsClaimedItem[size];
        }
    };
}
