package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class StoreHas implements Parcelable {
    @SerializedName("offers")
    @Expose
    public boolean offers;
    @SerializedName("rewards")
    @Expose
    public boolean rewards;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.offers ? (byte) 1 : (byte) 0);
        dest.writeByte(this.rewards ? (byte) 1 : (byte) 0);
    }

    public StoreHas() {
    }

    protected StoreHas(Parcel in) {
        this.offers = in.readByte() != 0;
        this.rewards = in.readByte() != 0;
    }

    public static final Creator<StoreHas> CREATOR = new Creator<StoreHas>() {
        @Override
        public StoreHas createFromParcel(Parcel source) {
            return new StoreHas(source);
        }

        @Override
        public StoreHas[] newArray(int size) {
            return new StoreHas[size];
        }
    };
}
