package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.Offer;
import com.ebizu.manis.sdk.entities.Reward;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 30/05/16.
 */
public class Saved {

    public static class RequestBody {
    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response {

        @SerializedName("offers")
        @Expose
        public ArrayList<Offer> offers = new ArrayList<>();
        @SerializedName("rewards")
        @Expose
        public ArrayList<Reward> rewards = new ArrayList<>();
    }
}
