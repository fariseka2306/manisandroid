package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 30/05/16.
 */
public class SnapAppeal {

    public static class RequestBody {
        @SerializedName("id")
        @Expose
        public long id;
        @SerializedName("comment")
        @Expose
        public String comment;

        public RequestBody(long id, String comment) {
            this.id = id;
            this.comment = comment;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {
        public Request(double latitude, double longitude, long id, String comment) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(id, comment);
        }
    }

    public static class Response {
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("created_at")
        @Expose
        private long createdAt;
        @SerializedName("body")
        @Expose
        private String body;
        @SerializedName("message_type")
        @Expose
        private String message;

        public Response() {
        }

        public Response(String type, String id, long createdAt, String body, String message) {
            this.type = type;
            this.id = id;
            this.createdAt = createdAt;
            this.body = body;
            this.message = message;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public long getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(long createdAt) {
            this.createdAt = createdAt;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
