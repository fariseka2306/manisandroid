package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;

import com.ebizu.manis.sdk.entities.BeaconPromo;
import com.ebizu.manis.sdk.entities.Coordinate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 30/05/16.
 */
public class BeaconPromoNearby {

    public static class RequestBody {
        @SerializedName("uuid")
        @Expose
        private String uuid;
        @SerializedName("major")
        @Expose
        private String major;
        @SerializedName("minor")
        @Expose
        private String minor;
        @SerializedName("sn")
        @Expose
        private String sn;

        public RequestBody(String uuid, String major, String minor, String sn) {
            this.uuid = uuid;
            this.major = major;
            this.minor = minor;
            this.sn = sn;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getMajor() {
            return major;
        }

        public void setMajor(String major) {
            this.major = major;
        }

        public String getMinor() {
            return minor;
        }

        public void setMinor(String minor) {
            this.minor = minor;
        }

        public String getSn() {
            return sn;
        }

        public void setSn(String sn) {
            this.sn = sn;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude, RequestBody requestBody) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = requestBody;
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends BeaconPromo {
    }
}
