package com.ebizu.manis.sdk.rest.data;

/**
 * Created by halim_ebizu on 9/15/17.
 */

public class RewardLoginSessionPost {

    public static class RequestBody {
        public Data data;
        public String module;
        public String action;
        public String token;

        public RequestBody(Data data, String module, String action, String token) {
            this.data = data;
            this.module = module;
            this.action = action;
            this.token = token;
        }
    }

    public static class Data {

        private String userId;

        public Data(String userId) {
            this.userId = userId;
        }
    }
}
