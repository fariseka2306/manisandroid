package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.MiscLegal;

import java.util.ArrayList;

/**
 * Created by rezkya on 09/06/16.
 */
public class MiscHelpLegal {
    public static class RequestBody{

    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody>{

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response extends ArrayList<MiscLegal> {
    }
}
