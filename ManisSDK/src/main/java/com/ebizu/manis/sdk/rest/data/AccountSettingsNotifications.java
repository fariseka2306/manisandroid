package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.NotificationItem;

import java.util.ArrayList;

/**
 * Created by Rizky Riadhy on 15-Jun-16.
 */
public class AccountSettingsNotifications {

    public static class RequestBody {

    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response extends ArrayList<NotificationItem> {
    }

}
