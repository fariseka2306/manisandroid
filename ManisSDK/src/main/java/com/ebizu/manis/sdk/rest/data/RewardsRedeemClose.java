package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by deni on 06/01/17.
 */
public class RewardsRedeemClose {

    public static class RequestBody {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("ref")
        @Expose
        private String ref;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("sn")
        @Expose
        private String sn;
        @SerializedName("trx")
        @Expose
        private String trx;
        @SerializedName("expired")
        @Expose
        private String expired;
        @SerializedName("voucher_image")
        @Expose
        private String voucher_image;
        @SerializedName("reward_name")
        @Expose
        private String reward_name;
        @SerializedName("com_name")
        @Expose
        private String com_name;
        @SerializedName("reward_type")
        @Expose
        private String reward_type;
        @SerializedName("point")
        @Expose
        private long point;
        @SerializedName("track_link")
        @Expose
        private String trackLink;

        public RequestBody(String id, String ref, String code, String sn, String trx, String expired, String voucher_image, String reward_name, String com_name, String rewardType, long point, String trackLink) {
            this.id = id;
            this.ref = ref;
            this.code = code;
            this.sn = sn;
            this.trx = trx;
            this.expired = expired;
            this.voucher_image = voucher_image;
            this.reward_name = reward_name;
            this.com_name = com_name;
            this.reward_type = rewardType;
            this.point = point;
            this.trackLink = trackLink;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, RequestBody requestBody) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = requestBody;
        }
    }

    public static class Response {
        private int point;

        public Response() {
        }

        public Response(int point) {
            this.point = point;
        }

        public int getPoint() {
            return point;
        }

        public void setPoint(int point) {
            this.point = point;
        }
    }
}
