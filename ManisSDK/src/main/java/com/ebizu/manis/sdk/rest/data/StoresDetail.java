package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.StoreDetail;

/**
 * Created by rezkya on 27/05/16.
 */
public class StoresDetail {

    public static class RequestBody {
        public int id;

        public RequestBody(int id) {
            this.id = id;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, int id) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(id);
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends StoreDetail {

        public Response(Parcel in) {
            super(in);
        }
    }
}
