package com.ebizu.manis.sdk.database;

import android.util.Log;

import com.ebizu.manis.sdk.shared.ManisApp;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

/**
 * Created by rezkya on 24/05/16.
 */
public class ManisSdkDbEncrypted extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "manissdke.db";
    public static String SECURE_KEY = "8jnETG5Gc2xoDP8wa6rrDkqJtRmsRQTQ";
    public static int DATABASE_VERSION = 2;

    private static ManisSdkDbEncrypted instance = null;

    private ManisSdkDbEncrypted() {
        super(ManisApp.context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Lazy Initialization (If required then only)
    public static ManisSdkDbEncrypted getInstance() {
        if (instance == null) {
            // Thread Safe. Might be costly operation in some case
            synchronized (ManisSdkDbEncrypted.class) {
                if (instance == null) {
                    instance = new ManisSdkDbEncrypted();
                }
            }
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(LocalData.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.v(ManisSdkDbEncrypted.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");

        db.execSQL("DROP TABLE IF EXISTS " + LocalData.TABLE_NAME);
        onCreate(db);
    }
}
