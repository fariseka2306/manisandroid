package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by novyandi-ebizu on 8/21/17.
 */
public class LuckyDrawSnapable implements Parcelable {

    @SerializedName("user_allowed_snap")
    private boolean userAllowedSnap;
    @SerializedName("restriction_message")
    private String restrictionMessage;

    public LuckyDrawSnapable() {
    }

    public boolean isUserAllowedSnap() {
        return userAllowedSnap;
    }

    public void setUserAllowedSnap(boolean userAllowedSnap) {
        this.userAllowedSnap = userAllowedSnap;
    }

    public String getRestrictionMessage() {
        return restrictionMessage;
    }

    public void setRestrictionMessage(String restrictionMessage) {
        this.restrictionMessage = restrictionMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.userAllowedSnap ? (byte) 1 : (byte) 0);
        dest.writeString(this.restrictionMessage);
    }

    protected LuckyDrawSnapable(Parcel in) {
        this.userAllowedSnap = in.readByte() != 0;
        this.restrictionMessage = in.readString();
    }

    public static final Parcelable.Creator<LuckyDrawSnapable> CREATOR = new Parcelable.Creator<LuckyDrawSnapable>() {
        @Override
        public LuckyDrawSnapable createFromParcel(Parcel source) {
            return new LuckyDrawSnapable(source);
        }

        @Override
        public LuckyDrawSnapable[] newArray(int size) {
            return new LuckyDrawSnapable[size];
        }
    };
}