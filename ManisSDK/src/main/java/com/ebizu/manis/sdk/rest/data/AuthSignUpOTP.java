package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.OtpData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rizky Riadhy on 06/12/16.
 */
public class AuthSignUpOTP {
    public static String MODULE_LOGIN = "login";

    public static class RequestBody {
        @SerializedName("handphone_no")
        @Expose
        public String handphoneNo;
        @SerializedName("module")
        @Expose
        public String module;

        public RequestBody(String handphoneNo, String module) {
            this.handphoneNo = handphoneNo;
            this.module = module;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, String handphoneNo, String module) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(handphoneNo, module);
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends OtpData {
        public Response(Parcel in) {
            super(in);
        }

        public Response() {
        }

    }
}
