package com.ebizu.manis.sdk.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkyatinnov on 7/1/16.
 */
public class CashVoucher {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("valid")
    @Expose
    private Validity valid;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("stock")
    @Expose
    private int stock;
    @SerializedName("company")
    @Expose
    private Company company;

    public CashVoucher(String id, String name, String description, Validity valid, String image, int stock, Company company) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.valid = valid;
        this.image = image;
        this.stock = stock;
        this.company = company;
    }

    public CashVoucher() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Validity getValid() {
        return valid;
    }

    public void setValid(Validity valid) {
        this.valid = valid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
