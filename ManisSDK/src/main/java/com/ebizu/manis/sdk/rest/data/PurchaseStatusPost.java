package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;

/**
 * Created by halim_ebizu on 9/15/17.
 */

public class PurchaseStatusPost {

    public static class RequestBody {
        public Data data;
        public String session;
        public String module;
        public String action;
        public String token;

        public RequestBody(Data data, String session, String module, String action, String token) {
            this.data = data;
            this.session = session;
            this.module = module;
            this.action = action;
            this.token = token;
        }

        public static class Data {
            private String orderId;
            private String paymentGatewayName;
            private String paymentType;
            private String transactionStatus;
            private String statusCode;
            private String statusMessage;
            private String grossAmount;
            private String transactionTime;
            private Customer customer;

            public Data(String orderId, String paymentGatewayName, String paymentType, String transactionStatus, String statusCode,
                        String statusMessage, String grossAmount, String transactionTime, Customer customer) {
                this.orderId = orderId;
                this.paymentGatewayName = paymentGatewayName;
                this.paymentType = paymentType;
                this.transactionStatus = transactionStatus;
                this.statusCode = statusCode;
                this.statusMessage = statusMessage;
                this.grossAmount = grossAmount;
                this.transactionTime = transactionTime;
                this.customer = customer;
            }

            public static class Customer {

                private String name;
                private String email;
                private String phone;
                private String shipmentInfo;

                public Customer(String name, String email, String phone, String shipmentInfo) {
                    this.name = name;
                    this.email = email;
                    this.phone = phone;
                    this.shipmentInfo = shipmentInfo;
                }
            }
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, RequestBody.Data data, String session,
                       String module,
                       String action,
                       String token) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(data, session, module, action, token);
        }
    }
}
