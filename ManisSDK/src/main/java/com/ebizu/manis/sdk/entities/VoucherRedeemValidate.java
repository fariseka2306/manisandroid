package com.ebizu.manis.sdk.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkyaebizu on 9/1/16.
 */
public class VoucherRedeemValidate {
    @SerializedName("voucherId")
    @Expose
    private String voucherId;
    @SerializedName("attempNum")
    @Expose
    private int attempNum;
    @SerializedName("lastAttemp")
    @Expose
    private long lastAttemp;

    public VoucherRedeemValidate(String voucherId, int attempNum, long lastAttemp) {
        this.voucherId = voucherId;
        this.attempNum = attempNum;
        this.lastAttemp = lastAttemp;
    }

    public VoucherRedeemValidate() {
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public int getAttempNum() {
        return attempNum;
    }

    public void setAttempNum(int attempNum) {
        this.attempNum = attempNum;
    }

    public long getLastAttemp() {
        return lastAttemp;
    }

    public void setLastAttemp(long lastAttemp) {
        this.lastAttemp = lastAttemp;
    }
}
