package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Company;
import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.Offer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by deni rohimat on 17/01/17.
 */
public class DeeplinkRewards {

    public static class RequestBody {
        @SerializedName("id")
        @Expose
        private int id;

        public RequestBody(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude, RequestBody requestBody) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = requestBody;
        }
    }

    public static class Response {
        @SerializedName("store")
        @Expose
        private Company store;
        @SerializedName("offer")
        @Expose
        private Offer offer;

        public Response() {
        }

        public Response(Company store, Offer offer) {
            this.store = store;
            this.offer = offer;
        }

        public Company getStore() {
            return store;
        }

        public void setStore(Company store) {
            this.store = store;
        }

        public Offer getOffer() {
            return offer;
        }

        public void setOffer(Offer offer) {
            this.offer = offer;
        }
    }
}
