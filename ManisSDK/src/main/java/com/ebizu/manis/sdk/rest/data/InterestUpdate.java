package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;

/**
 * Created by rezkya on 24/05/16.
 */
public class InterestUpdate {

    public static class RequestBody {
        public int[] id;

        public RequestBody(int[] id) {
            this.id = id;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody>{

        public Request(double latitude, double longitude, int[] id) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(id);
        }
    }

    public static class Response {
        public String deleted;
        public String added;
    }
}
