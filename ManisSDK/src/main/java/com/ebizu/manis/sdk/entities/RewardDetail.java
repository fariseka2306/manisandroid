package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by deni rohimat on 17/01/17.
 */
public class RewardDetail implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("logo_small")
    @Expose
    private String logoSmall;
    @SerializedName("picture_small")
    @Expose
    private String pictureSmall;
    @SerializedName("stock_left")
    @Expose
    private int stockLeft;
    @SerializedName("expired")
    @Expose
    private String expired;
    @SerializedName("type")
    @Expose
    private int rewardType;
    @SerializedName("value")
    @Expose
    private int value;
    @SerializedName("need_confirm_page")
    @Expose
    private boolean needConfirmPage;
    @SerializedName("company_id")
    @Expose
    private int companyId;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("sequence")
    @Expose
    private int sequence;
    @SerializedName("can_only_redeem_once")
    @Expose
    private boolean canOnlyRedeemOnce;
    @SerializedName("already_redeemed")
    @Expose
    private boolean alreadyRedeemed;
    @SerializedName("params")
    @Expose
    private Params[] params;
    @SerializedName("tos")
    @Expose
    private String[] tos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLogoSmall() {
        return logoSmall;
    }

    public void setLogoSmall(String logoSmall) {
        this.logoSmall = logoSmall;
    }

    public String getPictureSmall() {
        return pictureSmall;
    }

    public void setPictureSmall(String pictureSmall) {
        this.pictureSmall = pictureSmall;
    }

    public int getStockLeft() {
        return stockLeft;
    }

    public void setStockLeft(int stockLeft) {
        this.stockLeft = stockLeft;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public int getRewardType() {
        return rewardType;
    }

    public void setRewardType(int rewardType) {
        this.rewardType = rewardType;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isNeedConfirmPage() {
        return needConfirmPage;
    }

    public void setNeedConfirmPage(boolean needConfirmPage) {
        this.needConfirmPage = needConfirmPage;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public boolean isCanOnlyRedeemOnce() {
        return canOnlyRedeemOnce;
    }

    public void setCanOnlyRedeemOnce(boolean canOnlyRedeemOnce) {
        this.canOnlyRedeemOnce = canOnlyRedeemOnce;
    }

    public boolean isAlreadyRedeemed() {
        return alreadyRedeemed;
    }

    public void setAlreadyRedeemed(boolean alreadyRedeemed) {
        this.alreadyRedeemed = alreadyRedeemed;
    }

    public Params[] getParams() {
        return params;
    }

    public void setParams(Params[] params) {
        this.params = params;
    }

    public String[] getTos() {
        return tos;
    }

    public void setTos(String[] tos) {
        this.tos = tos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.timestamp);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.logo);
        dest.writeString(this.picture);
        dest.writeString(this.logoSmall);
        dest.writeString(this.pictureSmall);
        dest.writeInt(this.stockLeft);
        dest.writeString(this.expired);
        dest.writeInt(this.rewardType);
        dest.writeInt(this.value);
        dest.writeByte(this.needConfirmPage ? (byte) 1 : (byte) 0);
        dest.writeInt(this.companyId);
        dest.writeString(this.companyName);
        dest.writeInt(this.sequence);
        dest.writeByte(this.canOnlyRedeemOnce ? (byte) 1 : (byte) 0);
        dest.writeByte(this.alreadyRedeemed ? (byte) 1 : (byte) 0);
        dest.writeTypedArray(this.params, flags);
        dest.writeStringArray(this.tos);
    }

    public RewardDetail() {
    }

    protected RewardDetail(Parcel in) {
        this.id = in.readInt();
        this.timestamp = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.logo = in.readString();
        this.picture = in.readString();
        this.logoSmall = in.readString();
        this.pictureSmall = in.readString();
        this.stockLeft = in.readInt();
        this.expired = in.readString();
        this.rewardType = in.readInt();
        this.value = in.readInt();
        this.needConfirmPage = in.readByte() != 0;
        this.companyId = in.readInt();
        this.companyName = in.readString();
        this.sequence = in.readInt();
        this.canOnlyRedeemOnce = in.readByte() != 0;
        this.alreadyRedeemed = in.readByte() != 0;
        this.params = in.createTypedArray(Params.CREATOR);
        this.tos = in.createStringArray();
    }

    public static final Creator<RewardDetail> CREATOR = new Creator<RewardDetail>() {
        @Override
        public RewardDetail createFromParcel(Parcel source) {
            return new RewardDetail(source);
        }

        @Override
        public RewardDetail[] newArray(int size) {
            return new RewardDetail[size];
        }
    };
}