package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 27/05/16.
 */
public class SnapStatisticData implements Parcelable{

    @SerializedName("total")
    @Expose
    public double total;
    @SerializedName("data")
    @Expose
    public ArrayList<SnapStatisticDatum> data = new ArrayList<>();

    public SnapStatisticData(Parcel in) {
        total = in.readDouble();
        data = in.createTypedArrayList(SnapStatisticDatum.CREATOR);
    }

    public static final Creator<SnapStatisticData> CREATOR = new Creator<SnapStatisticData>() {
        @Override
        public SnapStatisticData createFromParcel(Parcel in) {
            return new SnapStatisticData(in);
        }

        @Override
        public SnapStatisticData[] newArray(int size) {
            return new SnapStatisticData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(total);
        dest.writeTypedList(data);
    }
}
