package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 30/05/16.
 */
public class Offer implements Parcelable{

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("store")
    @Expose
    public Company store;
    @SerializedName("date")
    @Expose
    public OfferDate date;
    @SerializedName("saved")
    @Expose
    public boolean saved;
    @SerializedName("claim_disabled")
    @Expose
    public boolean claim_disabled;
    @SerializedName("redeemed")
    @Expose
    public long redeemed;
    @SerializedName("tos")
    @Expose
    public String[] tos = new String[] {};

    public Offer() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.photo);
        dest.writeParcelable(this.store, flags);
        dest.writeParcelable(this.date, flags);
        dest.writeByte(this.saved ? (byte) 1 : (byte) 0);
        dest.writeByte(this.claim_disabled ? (byte) 1 : (byte) 0);
        dest.writeLong(this.redeemed);
        dest.writeStringArray(this.tos);
    }

    protected Offer(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.photo = in.readString();
        this.store = in.readParcelable(Company.class.getClassLoader());
        this.date = in.readParcelable(OfferDate.class.getClassLoader());
        this.saved = in.readByte() != 0;
        this.claim_disabled = in.readByte() != 0;
        this.redeemed = in.readLong();
        this.tos = in.createStringArray();
    }

    public static final Creator<Offer> CREATOR = new Creator<Offer>() {
        @Override
        public Offer createFromParcel(Parcel source) {
            return new Offer(source);
        }

        @Override
        public Offer[] newArray(int size) {
            return new Offer[size];
        }
    };
}
