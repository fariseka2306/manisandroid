package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;

/**
 * Created by rezkya on 30/05/16.
 */
public class RewardsUnpin {

    public static class RequestBody {
        public String id;

        public RequestBody(String id) {
            this.id = id;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, String id) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(id);
        }
    }

    public static class Response {
    }
}
