package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.LuckyDrawWinnersData;

/**
 * Created by denirohimat on 01/08/17.
 */
public class LuckyDrawWinnersRest {

    public static class RequestBody {
        public int page;
        public int size;

        public RequestBody(int page, int size) {
            this.page = page;
            this.size = size;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, int page, int size) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(page, size);
        }
    }

    public static class Response extends LuckyDrawWinnersData {
    }
}