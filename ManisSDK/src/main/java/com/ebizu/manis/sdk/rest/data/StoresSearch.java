package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.StoreData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 27/05/16.
 */
public class StoresSearch {

    public static class RequestBody {

        @SerializedName("keyword")
        @Expose
        public String keyword;
        @SerializedName("multiplier")
        @Expose
        public String multiplier;
        @SerializedName("page")
        @Expose
        public int page;
        @SerializedName("size")
        @Expose
        public int size;
        @SerializedName("merchant_tier")
        @Expose
        public String merchantTier;

        public RequestBody(String keyword, String multiplier, int page, int size, String merchantTier) {
            this.keyword = keyword;
            this.multiplier = multiplier;
            this.page = page;
            this.size = size;
            this.merchantTier = merchantTier;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, String keyword, String multiplier, int page, int size, String merchantTier) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(keyword, multiplier, page, size, merchantTier);
        }
    }

    public static class Response {
        @SerializedName("more")
        @Expose
        public boolean more;
        @SerializedName("results")
        @Expose
        public ArrayList<StoreData> results = new ArrayList<>();
    }
}