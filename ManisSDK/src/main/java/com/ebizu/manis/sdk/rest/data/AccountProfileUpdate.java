package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.LoginData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 17/05/16.
 */
public class AccountProfileUpdate {
    public static class RequestBody {
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("birthdate")
        @Expose
        public String birthdate;
        @SerializedName("address")
        @Expose
        public String address;
        @SerializedName("gender")
        @Expose
        public String gender;
        @SerializedName("msisdn")
        @Expose
        public String msisdn;

        public RequestBody(String name, String email, String birthdate, String address, String gender, String msisdn) {
            this.name = name;
            this.email = email;
            this.birthdate = birthdate;
            this.address = address;
            this.gender = gender;
            this.msisdn = msisdn;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody>{

        public Request(double latitude, double longitude, String name, String email, String birthdate, String address, String gender, String msisdn) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody(name, email, birthdate, address, gender, msisdn);
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends LoginData {
        public Response(Parcel in) {
            super(in);
        }
    }
}
