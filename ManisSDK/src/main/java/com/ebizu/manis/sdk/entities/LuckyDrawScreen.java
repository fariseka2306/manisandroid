package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by novyandi-ebizu on 7/20/17.
 */
public class LuckyDrawScreen implements Parcelable {

    @SerializedName("user_tickets_alltime")
    @Expose
    private long userTicketsAll;
    @SerializedName("user_tickets_current_period")
    @Expose
    private long userTicketsPeriod;
    @SerializedName("last_winner_prefix")
    @Expose
    private String lastWinnerPrefix;
    @SerializedName("last_winner_object")
    @Expose
    private String lastWinnerObject;
    @SerializedName("currency")
    @Expose
    private Currency currency;
    @SerializedName("total_price")
    @Expose
    private double totalPrice;
    @SerializedName("prizes")
    @Expose
    private LuckyDrawPrizes luckyDrawPrizes;
    @SerializedName("lucky_draw_expired_date")
    @Expose
    private long luckyDrawExpiredDate;
    @SerializedName("lucky_draw_msg_prefix")
    @Expose
    private String luckyDrawMsgPrefix;
    @SerializedName("lucky_draw_msg_object")
    @Expose
    private String luckyDrawMsgObject;

    public LuckyDrawScreen() {
    }

    public long getUserTicketsAll() {
        return userTicketsAll;
    }

    public void setUserTicketsAll(long userTicketsAll) {
        this.userTicketsAll = userTicketsAll;
    }

    public long getUserTicketsPeriod() {
        return userTicketsPeriod;
    }

    public void setUserTicketsPeriod(long userTicketsPeriod) {
        this.userTicketsPeriod = userTicketsPeriod;
    }

    public String getLastWinnerPrefix() {
        return lastWinnerPrefix;
    }

    public void setLastWinnerPrefix(String lastWinnerPrefix) {
        this.lastWinnerPrefix = lastWinnerPrefix;
    }

    public String getLastWinnerObject() {
        return lastWinnerObject;
    }

    public void setLastWinnerObject(String lastWinnerObject) {
        this.lastWinnerObject = lastWinnerObject;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public LuckyDrawPrizes getLuckyDrawPrizes() {
        return luckyDrawPrizes;
    }

    public void setLuckyDrawPrizes(LuckyDrawPrizes luckyDrawPrizes) {
        this.luckyDrawPrizes = luckyDrawPrizes;
    }

    public long getLuckyDrawExpiredDate() {
        return luckyDrawExpiredDate;
    }

    public void setLuckyDrawExpiredDate(long luckyDrawExpiredDate) {
        this.luckyDrawExpiredDate = luckyDrawExpiredDate;
    }

    public String getLuckyDrawMsgPrefix() {
        return luckyDrawMsgPrefix;
    }

    public void setLuckyDrawMsgPrefix(String luckyDrawMsgPrefix) {
        this.luckyDrawMsgPrefix = luckyDrawMsgPrefix;
    }

    public String getLuckyDrawMsgObject() {
        return luckyDrawMsgObject;
    }

    public void setLuckyDrawMsgObject(String luckyDrawMsgObject) {
        this.luckyDrawMsgObject = luckyDrawMsgObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.userTicketsAll);
        dest.writeLong(this.userTicketsPeriod);
        dest.writeString(this.lastWinnerPrefix);
        dest.writeString(this.lastWinnerObject);
        dest.writeParcelable(this.currency, flags);
        dest.writeDouble(this.totalPrice);
        dest.writeParcelable(this.luckyDrawPrizes, flags);
        dest.writeLong(this.luckyDrawExpiredDate);
        dest.writeString(this.luckyDrawMsgPrefix);
        dest.writeString(this.luckyDrawMsgObject);
    }

    protected LuckyDrawScreen(Parcel in) {
        this.userTicketsAll = in.readLong();
        this.userTicketsPeriod = in.readLong();
        this.lastWinnerPrefix = in.readString();
        this.lastWinnerObject = in.readString();
        this.currency = in.readParcelable(Currency.class.getClassLoader());
        this.totalPrice = in.readDouble();
        this.luckyDrawPrizes = in.readParcelable(LuckyDrawPrizes.class.getClassLoader());
        this.luckyDrawExpiredDate = in.readLong();
        this.luckyDrawMsgPrefix = in.readString();
        this.luckyDrawMsgObject = in.readString();
    }

    public static final Creator<LuckyDrawScreen> CREATOR = new Creator<LuckyDrawScreen>() {
        @Override
        public LuckyDrawScreen createFromParcel(Parcel source) {
            return new LuckyDrawScreen(source);
        }

        @Override
        public LuckyDrawScreen[] newArray(int size) {
            return new LuckyDrawScreen[size];
        }
    };
}