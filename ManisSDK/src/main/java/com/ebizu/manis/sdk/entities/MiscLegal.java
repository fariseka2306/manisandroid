package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 09/06/16.
 */
public class MiscLegal implements Parcelable{

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("content")
    @Expose
    private String content;

    public MiscLegal(Parcel in) {
        type = in.readString();
        title = in.readString();
        content = in.readString();
    }

    public MiscLegal(String type, String title, String content) {
        this.type = type;
        this.title = title;
        this.content = content;
    }

    public static final Creator<MiscLegal> CREATOR = new Creator<MiscLegal>() {
        @Override
        public MiscLegal createFromParcel(Parcel in) {
            return new MiscLegal(in);
        }

        @Override
        public MiscLegal[] newArray(int size) {
            return new MiscLegal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(title);
        dest.writeString(content);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static Creator<MiscLegal> getCREATOR() {
        return CREATOR;
    }
}
