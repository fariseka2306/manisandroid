package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rizky Riadhy on 14/12/16.
 */
public class OTPStatus {

    public static class RequestBody {

        public RequestBody() {
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response {
        @SerializedName("skipped_status")
        @Expose
        public boolean skippedStatus;
    }
}
