package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.NotificationItem;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rizky Riadhy on 15-Jun-16.
 */
public class AccountSettingsNotificationsUpdate {

    public static class RequestBody extends HashMap<String, Integer> {
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response extends ArrayList<NotificationItem> {
    }

}
