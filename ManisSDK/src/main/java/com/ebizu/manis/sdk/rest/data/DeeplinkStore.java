package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.StoreData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 30/05/16.
 */
public class DeeplinkStore {

    public static class RequestBody {
        @SerializedName("id")
        @Expose
        private int id;

        public RequestBody(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude, RequestBody requestBody) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = requestBody;
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends StoreData {
        public Response(Parcel in) {
            super(in);
        }
    }
}
