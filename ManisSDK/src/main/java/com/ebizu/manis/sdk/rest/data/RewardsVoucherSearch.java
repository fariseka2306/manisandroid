package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.Reward;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 30/05/16.
 */
public class RewardsVoucherSearch {

    public static class RequestBody {
        @SerializedName("keyword")
        @Expose
        public String keyword;
        @SerializedName("page")
        @Expose
        public int page;
        @SerializedName("size")
        @Expose
        public int size;

        public RequestBody(String keyword, int page, int size) {
            this.keyword = keyword;
            this.page = page;
            this.size = size;
        }

        public RequestBody() {
        }

        public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, RequestBody requestBody) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = requestBody;
        }
    }

    public static class Response {
        @SerializedName("more")
        @Expose
        private boolean more;
        @SerializedName("results")
        @Expose
        private ArrayList<Reward> results = new ArrayList<>();

        public Response(boolean more, ArrayList<Reward> results) {
            this.more = more;
            this.results = results;
        }

        public Response() {
        }

        public boolean isMore() {
            return more;
        }

        public void setMore(boolean more) {
            this.more = more;
        }

        public ArrayList<Reward> getResults() {
            return results;
        }

        public void setResults(ArrayList<Reward> results) {
            this.results = results;
        }
    }
}
