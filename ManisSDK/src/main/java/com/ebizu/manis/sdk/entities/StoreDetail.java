package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 29/05/16.
 */
public class StoreDetail implements Parcelable{
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("address")
    @Expose
    public Address address;
    @SerializedName("contact")
    @Expose
    public Contact contact;
    @SerializedName("website")
    @Expose
    public String website;
    @SerializedName("point")
    @Expose
    public int point;
    @SerializedName("premium")
    @Expose
    public boolean premium;
    @SerializedName("multiplier")
    @Expose
    public int multiplier;
    @SerializedName("pinned")
    @Expose
    public boolean pinned;
    @SerializedName("offers")
    @Expose
    public ArrayList<Offer> offers = new ArrayList<>();
    @SerializedName("rewards")
    @Expose
    public ArrayList<Reward> rewards = new ArrayList<>();
    @SerializedName("followers")
    @Expose
    public int followers;
    @SerializedName("avaibility")
    @Expose
    public StoreAvailability avaibility;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.description);
        dest.writeParcelable(this.address, flags);
        dest.writeParcelable(this.contact, flags);
        dest.writeString(this.website);
        dest.writeInt(this.point);
        dest.writeByte(this.premium ? (byte) 1 : (byte) 0);
        dest.writeInt(this.multiplier);
        dest.writeByte(this.pinned ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.offers);
        dest.writeTypedList(this.rewards);
        dest.writeInt(this.followers);
        dest.writeParcelable(this.avaibility, flags);
    }

    public StoreDetail() {
    }

    protected StoreDetail(Parcel in) {
        this.description = in.readString();
        this.address = in.readParcelable(Address.class.getClassLoader());
        this.contact = in.readParcelable(Contact.class.getClassLoader());
        this.website = in.readString();
        this.point = in.readInt();
        this.premium = in.readByte() != 0;
        this.multiplier = in.readInt();
        this.pinned = in.readByte() != 0;
        this.offers = in.createTypedArrayList(Offer.CREATOR);
        this.rewards = in.createTypedArrayList(Reward.CREATOR);
        this.followers = in.readInt();
        this.avaibility = in.readParcelable(StoreAvailability.class.getClassLoader());
    }

    public static final Creator<StoreDetail> CREATOR = new Creator<StoreDetail>() {
        @Override
        public StoreDetail createFromParcel(Parcel source) {
            return new StoreDetail(source);
        }

        @Override
        public StoreDetail[] newArray(int size) {
            return new StoreDetail[size];
        }
    };
}
