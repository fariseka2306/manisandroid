package com.ebizu.manis.sdk.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ebizu on 9/14/17.
 */

public class PurchasedVoucherData {

    @SerializedName("pageCount")
    @Expose
    private Integer pageCount;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("more")
    @Expose
    private Boolean more;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("data")
    @Expose
    private List<PurchasedHistory> purchasedHistories;

    public Integer getPageCount() {
        return pageCount;
    }

    public Integer getTotal() {
        return total;
    }

    public Integer getSize() {
        return size;
    }

    public Boolean getMore() {
        return more;
    }

    public Integer getCount() {
        return count;
    }

    public Integer getPage() {
        return page;
    }

    public List<PurchasedHistory> getPurchasedHistories() {
        return purchasedHistories;
    }
}
