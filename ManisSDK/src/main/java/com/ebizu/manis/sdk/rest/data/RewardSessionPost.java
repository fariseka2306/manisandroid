package com.ebizu.manis.sdk.rest.data;

/**
 * Created by halim_ebizu on 9/13/17.
 */

public class RewardSessionPost {

    public static class RequestBody {
        public Data data;
        public String module;
        public String action;
        public String token;

        public RequestBody(Data data, String module, String action, String token) {
            this.data = data;
            this.module = module;
            this.action = action;
            this.token = token;
        }
    }

    public static class Data {

        private String userId;
        private String userName;
        private String userEmail;
        private String userCountry;
        private String userTimezone;
        private String userJoin;
        private String userAgent;
        private String osName;
        private String osVersion;
        private String applicationBuild;
        private String applicationVersion;
        private String manufacture;
        private String model;
        private String msisdn;
        private String imei;
        private String imsi;
        private String session;

        public Data(String userId, String userName, String userEmail, String userCountry,
                    String userTimezone, String userJoin, String userAgent, String osName,
                    String osVersion, String applicationBuild, String applicationVersion,
                    String manufacture, String model, String msisdn, String imei, String imsi, String session) {
            this.userId = userId;
            this.userName = userName;
            this.userEmail = userEmail;
            this.userCountry = userCountry;
            this.userTimezone = userTimezone;
            this.userJoin = userJoin;
            this.userAgent = userAgent;
            this.osName = osName;
            this.osVersion = osVersion;
            this.applicationBuild = applicationBuild;
            this.applicationVersion = applicationVersion;
            this.manufacture = manufacture;
            this.model = model;
            this.msisdn = msisdn;
            this.imei = imei;
            this.imsi = imsi;
            this.session = session;
        }
    }
}
