package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class Datetime implements Parcelable{
    @SerializedName("unixtime")
    @Expose
    public Unixtime unixtime;
    @SerializedName("readable")
    @Expose
    public ReadableTime readable;

    public Datetime(Parcel in) {
        unixtime =  in.readParcelable(Unixtime.class.getClassLoader());
        readable =  in.readParcelable(ReadableTime.class.getClassLoader());
    }

    public static final Creator<Datetime> CREATOR = new Creator<Datetime>() {
        @Override
        public Datetime createFromParcel(Parcel in) {
            return new Datetime(in);
        }

        @Override
        public Datetime[] newArray(int size) {
            return new Datetime[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(unixtime,flags);
        dest.writeParcelable(readable,flags);
    }
}
