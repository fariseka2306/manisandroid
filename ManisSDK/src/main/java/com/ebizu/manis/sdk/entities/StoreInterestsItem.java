package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class StoreInterestsItem implements Parcelable {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("pinned")
    @Expose
    public boolean pinned;
    @SerializedName("count")
    @Expose
    public int count;
    @SerializedName("assets")
    @Expose
    public StoreAsset assets;

    public StoreInterestsItem() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeByte(this.pinned ? (byte) 1 : (byte) 0);
        dest.writeInt(this.count);
        dest.writeParcelable(this.assets, flags);
    }

    protected StoreInterestsItem(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.pinned = in.readByte() != 0;
        this.count = in.readInt();
        this.assets = in.readParcelable(StoreAsset.class.getClassLoader());
    }

    public static final Creator<StoreInterestsItem> CREATOR = new Creator<StoreInterestsItem>() {
        @Override
        public StoreInterestsItem createFromParcel(Parcel source) {
            return new StoreInterestsItem(source);
        }

        @Override
        public StoreInterestsItem[] newArray(int size) {
            return new StoreInterestsItem[size];
        }
    };
}
