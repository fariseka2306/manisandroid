package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.StoreData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 27/05/16.
 */
public class StoresInterest {

    public static class RequestBody {
        public int id;
        public int page;
        public int size;

        public RequestBody(int id, int page, int size) {
            this.id = id;
            this.page = page;
            this.size = size;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, int id, int page, int size) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(id, page, size);
        }
    }

    public static class Response {
        @SerializedName("more")
        @Expose
        public boolean more;
        @SerializedName("results")
        @Expose
        public ArrayList<StoreData> results = new ArrayList<>();
    }
}
