package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Deni Rohimat on 24/07/17.
 */
public class LuckyDrawTickets implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("d_day")
    private long dDay;
    @SerializedName("wording_prefix")
    private String wordingPrefix;
    @SerializedName("wording_object")
    private String wordingObject;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getDDay() {
        return dDay;
    }

    public void setDDay(long dDay) {
        this.dDay = dDay;
    }

    public String getWordingPrefix() {
        return wordingPrefix;
    }

    public void setWordingPrefix(String wordingPrefix) {
        this.wordingPrefix = wordingPrefix;
    }

    public String getWordingObject() {
        return wordingObject;
    }

    public void setWordingObject(String wordingObject) {
        this.wordingObject = wordingObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeLong(this.dDay);
        dest.writeString(this.wordingPrefix);
        dest.writeString(this.wordingObject);
    }

    public LuckyDrawTickets() {
    }

    protected LuckyDrawTickets(Parcel in) {
        this.id = in.readInt();
        this.dDay = in.readLong();
        this.wordingPrefix = in.readString();
        this.wordingObject = in.readString();
    }

    public static final Creator<LuckyDrawTickets> CREATOR = new Creator<LuckyDrawTickets>() {
        @Override
        public LuckyDrawTickets createFromParcel(Parcel source) {
            return new LuckyDrawTickets(source);
        }

        @Override
        public LuckyDrawTickets[] newArray(int size) {
            return new LuckyDrawTickets[size];
        }
    };
}
