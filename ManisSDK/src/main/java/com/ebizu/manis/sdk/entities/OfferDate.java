package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 30/05/16.
 */
public class OfferDate implements Parcelable {

    @SerializedName("start")
    @Expose
    public long start;
    @SerializedName("end")
    @Expose
    public long end;
    @SerializedName("expired")
    @Expose
    public long expired;

    public OfferDate(Parcel in) {
        start = in.readLong();
        end = in.readLong();
        expired = in.readLong();
    }

    public static final Creator<OfferDate> CREATOR = new Creator<OfferDate>() {
        @Override
        public OfferDate createFromParcel(Parcel in) {
            return new OfferDate(in);
        }

        @Override
        public OfferDate[] newArray(int size) {
            return new OfferDate[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(start);
        dest.writeLong(end);
        dest.writeLong(expired);
    }
}
