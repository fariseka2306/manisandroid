package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.LoginData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rizky Riadhy on 02/12/16.
 */
public class AuthSignInGoogle {
    public static class RequestBody {
        @SerializedName("google")
        @Expose
        public Google google;
        @SerializedName("device")
        @Expose
        public Device device;

        public RequestBody(Google google, Device device) {
            this.google = google;
            this.device = device;
        }

        public static class Google {
            @SerializedName("google_id")
            @Expose
            public String id;
            @SerializedName("name")
            @Expose
            public String name;
            @SerializedName("email")
            @Expose
            public String email;
            @SerializedName("gender")
            @Expose
            public String gender;
            @SerializedName("birthday")
            @Expose
            public String birthday;
            @SerializedName("verified")
            @Expose
            public boolean verified;

            public Google(String id, String name, String email, int genderCode, String birthday, boolean verified) {
                this.id = id;
                this.name = name;
                this.email = email;
                if(genderCode == 0){
                    this.gender = "male";
                }else if(genderCode == 1){
                    this.gender = "female";
                }else{
                    this.gender = "";
                }
                this.birthday = birthday;
                this.verified = verified;
            }
        }

        public static class Device {
            @SerializedName("manufacture")
            @Expose
            public String manufacture;
            @SerializedName("model")
            @Expose
            public String model;
            @SerializedName("version")
            @Expose
            public String version;
            @SerializedName("msisdn")
            @Expose
            public String msisdn;
            @SerializedName("operator")
            @Expose
            public String operator;
            @SerializedName("imei")
            @Expose
            public String imei;
            @SerializedName("imsi")
            @Expose
            public String imsi;
            @SerializedName("token")
            @Expose
            public String token;

            public Device(String manufacture, String model, String version, String msisdn, String imei, String imsi, String token, String operator) {
                this.manufacture = manufacture;
                this.model = model;
                this.version = version;
                this.msisdn = msisdn;
                this.imei = imei;
                this.imsi = imsi;
                this.token = token;
                this.operator = operator;
            }
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, RequestBody.Google google, RequestBody.Device device) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(google, device);
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends LoginData {
        public Response(Parcel in) {
            super(in);
        }

        public Response() {
        }

    }
}