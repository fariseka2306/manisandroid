package com.ebizu.manis.sdk.rest;

/**
 * Created by halim_ebizu on 8/29/17.
 */

public class RewardRestResponse<D> {

    public D data;
    public int errorCode;
    public double time;
    public String errorMessage;

    public RewardRestResponse() {
    }

    public RewardRestResponse(D data, int errorCode, double time, String errorMessage) {
        this.data = data;
        this.errorCode = errorCode;
        this.time = time;
        this.errorMessage = errorMessage;
    }

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
