package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.RewardsClaimedItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 30/05/16.
 */
public class RewardsClaimed {

    public static class RequestBody {
        public int page;

        public RequestBody(int page) {
            this.page = page;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, int page) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody(page);
        }
    }

    public static class Response {
        @SerializedName("more")
        @Expose
        private boolean more;
        @SerializedName("results")
        @Expose
        private ArrayList<RewardsClaimedItem> results = new ArrayList<>();

        public Response(boolean more, ArrayList<RewardsClaimedItem> results) {
            this.more = more;
            this.results = results;
        }

        public Response() {
        }

        public boolean isMore() {
            return more;
        }

        public void setMore(boolean more) {
            this.more = more;
        }

        public ArrayList<RewardsClaimedItem> getResults() {
            return results;
        }

        public void setResults(ArrayList<RewardsClaimedItem> results) {
            this.results = results;
        }
    }
}
