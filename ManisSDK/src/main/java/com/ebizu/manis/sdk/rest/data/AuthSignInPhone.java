package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.LoginData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rizky Riadhy on 30/11/16.
 */
public class AuthSignInPhone {
    public static class RequestBody {
        @SerializedName("phone")
        @Expose
        public Phone phone;
        @SerializedName("device")
        @Expose
        public Device device;

        public RequestBody(Phone phone, Device device) {
            this.phone = phone;
            this.device = device;
        }

        public static class Phone {
            @SerializedName("number")
            @Expose
            public String number;

            public Phone(String number) {
                this.number = number;
            }
        }
        public static class Device {
            @SerializedName("manufacture")
            @Expose
            public String manufacture;
            @SerializedName("model")
            @Expose
            public String model;
            @SerializedName("version")
            @Expose
            public String version;
            @SerializedName("msisdn")
            @Expose
            public String msisdn;
            @SerializedName("operator")
            @Expose
            public String operator;
            @SerializedName("imei")
            @Expose
            public String imei;
            @SerializedName("imsi")
            @Expose
            public String imsi;
            @SerializedName("token")
            @Expose
            public String token;

            public Device(String manufacture, String model, String version, String msisdn, String imei, String imsi, String token, String operator) {
                this.manufacture = manufacture;
                this.model = model;
                this.version = version;
                this.msisdn = msisdn;
                this.imei = imei;
                this.imsi = imsi;
                this.token = token;
                this.operator = operator;
            }
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody>{

        public Request(double latitude, double longitude, RequestBody.Phone phone, RequestBody.Device device) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody(phone,device);
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends LoginData {
        public Response(Parcel in) {
            super(in);
        }

    public Response() {
    }

    }
}
