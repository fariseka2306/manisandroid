package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;

/**
 * Created by halim_ebizu on 9/7/17.
 */

public class RedeemPinPost {

    public static class RequestBody {
        private String voucherId;

        public RequestBody(String voucherId) {
            this.voucherId = voucherId;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {
        public Request(double latitude, double longitude, String voucherId) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(voucherId);
        }
    }
}
