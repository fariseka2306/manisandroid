package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class Receipt implements Parcelable {

    @SerializedName("number")
    @Expose
    public String number;
    @SerializedName("amount")
    @Expose
    public double amount;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("image")
    @Expose
    public String image;

    public Receipt(Parcel in) {
        number = in.readString();
        amount = in.readDouble();
        date = in.readString();
        image = in.readString();
    }

    public static final Creator<Receipt> CREATOR = new Creator<Receipt>() {
        @Override
        public Receipt createFromParcel(Parcel in) {
            return new Receipt(in);
        }

        @Override
        public Receipt[] newArray(int size) {
            return new Receipt[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(number);
        dest.writeDouble(amount);
        dest.writeString(date);
        dest.writeString(image);
    }
}
