package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.SnapData;

import java.util.ArrayList;

/**
 * Created by rezkya on 27/05/16.
 */
public class SnapLast {

    public static class RequestBody {
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response extends ArrayList<SnapData> {
    }
}
