package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class StoreAvailabilityDetail implements Parcelable {
    @SerializedName("day")
    @Expose
    public String day;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("open")
    @Expose
    public Object open;
    @SerializedName("close")
    @Expose
    public Object close;

    public StoreAvailabilityDetail(Parcel in) {
        day = in.readString();
        status = in.readString();
    }

    public static final Creator<StoreAvailabilityDetail> CREATOR = new Creator<StoreAvailabilityDetail>() {
        @Override
        public StoreAvailabilityDetail createFromParcel(Parcel in) {
            return new StoreAvailabilityDetail(in);
        }

        @Override
        public StoreAvailabilityDetail[] newArray(int size) {
            return new StoreAvailabilityDetail[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(day);
        dest.writeString(status);
    }
}
