package com.ebizu.manis.sdk.utils;

/**
 * Created by rezkya on 17/05/16.
 */
public class ManisConst {
    public static final String SIGNIN_PREF = "com.ebizu.manis.signin";
    public static final String KEY_PREF = "com.ebizu.manis.key";
    public static final String API_ENV_DEVELOPMENT = "DEVELOPMENT";
    public static final String API_ENV_STAGING = "STAGING";
    public static final String API_ENV_PRODUCTION = "PRODUCTION";
    public static final String TOKEN_EXPIRED_INTENT_ACTION = "com.ebizu.manis.TOKEN_EXPIRED";
    public static final String TOKEN_EXPIRED_INTENT_NAME = "com.ebizu.manis.TOKEN_EXPIRED.message";
}
