package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.sdk.reward.models.Brand;
import com.ebizu.sdk.reward.models.Provider;
import com.ebizu.sdk.reward.models.Term;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu on 9/18/17.
 */

public class MyVoucher implements Parcelable {

    @SerializedName("voucherTransactionType")
    @Expose
    private String voucherTransactionType;
    @SerializedName("voucherCostValue")
    @Expose
    private Integer voucherCostValue;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("inputs")
    @Expose
    private List<Object> inputs = null;
    @SerializedName("purchaseId")
    @Expose
    private String purchaseId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("redeemInstruction")
    @Expose
    private String redeemInstruction;
    @SerializedName("redeemCount")
    @Expose
    private Integer redeemCount;
    @SerializedName("point")
    @Expose
    private Integer point;
    @SerializedName("halalStatus")
    @Expose
    private String halalStatus;
    @SerializedName("expired")
    @Expose
    private String expired;
    @SerializedName("provider")
    @Expose
    private Provider provider;
    @SerializedName("terms")
    @Expose
    private List<Term> terms = null;
    @SerializedName("voucherRedeemType")
    @Expose
    private String voucherRedeemType;
    @SerializedName("trackLink")
    @Expose
    private String trackLink;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("stock")
    @Expose
    private Integer stock;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("brand")
    @Expose
    private Brand brand;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("redeemLimit")
    @Expose
    private String redeemLimit;
    @SerializedName("redeemId")
    @Expose
    private String redeemId;
    @SerializedName("hidePrice")
    @Expose
    private String hidePrice;
    @SerializedName("image64")
    @Expose
    private String image64;
    @SerializedName("redeemLabel")
    @Expose
    private String redeemLabel;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("currency3")
    @Expose
    private String currency3;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("needConfirmation")
    @Expose
    private String needConfirmation;
    @SerializedName("image128")
    @Expose
    private String image128;
    @SerializedName("voucherPurchaseValue")
    @Expose
    private Integer voucherPurchaseValue;

    public String getVoucherTransactionType() {
        return voucherTransactionType;
    }

    public Integer getVoucherCostValue() {
        return voucherCostValue;
    }

    public String getOrderId() {
        return orderId;
    }

    public List<Object> getInputs() {
        return inputs;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public String getRedeemInstruction() {
        return redeemInstruction;
    }

    public Integer getRedeemCount() {
        return redeemCount;
    }

    public Integer getPoint() {
        return point;
    }

    public String getHalalStatus() {
        return halalStatus;
    }

    public String getExpired() {
        return expired;
    }

    public Provider getProvider() {
        return provider;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public String getVoucherRedeemType() {
        return voucherRedeemType;
    }

    public boolean isTypeVoucherEmpty() {
        if (voucherCostValue == null) {
            return true;
        } else {
            return false;
        }
    }

    public String getTrackLink() {
        return trackLink;
    }

    public String getCurrency() {
        return currency;
    }

    public String getId() {
        return id;
    }

    public Integer getStock() {
        return stock;
    }

    public Integer getValue() {
        return value;
    }

    public Brand getBrand() {
        return brand;
    }

    public String getImage() {
        return image;
    }

    public String getRedeemLimit() {
        return redeemLimit;
    }

    public String getRedeemId() {
        return redeemId;
    }

    public String getHidePrice() {
        return hidePrice;
    }

    public String getImage64() {
        return image64;
    }

    public String getRedeemLabel() {
        return redeemLabel;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getTags() {
        return tags;
    }

    public String getCurrency3() {
        return currency3;
    }

    public String getName() {
        return name;
    }

    public String getNeedConfirmation() {
        return needConfirmation;
    }

    public String getImage128() {
        return image128;
    }

    public Integer getVoucherPurchaseValue() {
        return voucherPurchaseValue;
    }

    public boolean isHidePrice() {
        return hidePrice != null && hidePrice.equalsIgnoreCase("Y");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.voucherTransactionType);
        dest.writeValue(this.voucherCostValue);
        dest.writeString(this.orderId);
        dest.writeList(this.inputs);
        dest.writeString(this.purchaseId);
        dest.writeString(this.description);
        dest.writeString(this.type);
        dest.writeString(this.redeemInstruction);
        dest.writeValue(this.redeemCount);
        dest.writeValue(this.point);
        dest.writeString(this.halalStatus);
        dest.writeString(this.expired);
        dest.writeParcelable(this.provider, flags);
        dest.writeTypedList(this.terms);
        dest.writeString(this.voucherRedeemType);
        dest.writeString(this.trackLink);
        dest.writeString(this.currency);
        dest.writeString(this.id);
        dest.writeValue(this.stock);
        dest.writeValue(this.value);
        dest.writeParcelable(this.brand, flags);
        dest.writeString(this.image);
        dest.writeString(this.redeemLimit);
        dest.writeString(this.redeemId);
        dest.writeString(this.hidePrice);
        dest.writeString(this.image64);
        dest.writeString(this.redeemLabel);
        dest.writeString(this.transactionId);
        dest.writeString(this.tags);
        dest.writeString(this.currency3);
        dest.writeString(this.name);
        dest.writeString(this.needConfirmation);
        dest.writeString(this.image128);
        dest.writeValue(this.voucherPurchaseValue);
    }

    public MyVoucher() {
    }

    protected MyVoucher(Parcel in) {
        this.voucherTransactionType = in.readString();
        this.voucherCostValue = (Integer) in.readValue(Integer.class.getClassLoader());
        this.orderId = in.readString();
        this.inputs = new ArrayList<Object>();
        in.readList(this.inputs, Object.class.getClassLoader());
        this.purchaseId = in.readString();
        this.description = in.readString();
        this.type = in.readString();
        this.redeemInstruction = in.readString();
        this.redeemCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.point = (Integer) in.readValue(Integer.class.getClassLoader());
        this.halalStatus = in.readString();
        this.expired = in.readString();
        this.provider = in.readParcelable(Provider.class.getClassLoader());
        this.terms = in.createTypedArrayList(Term.CREATOR);
        this.voucherRedeemType = in.readString();
        this.trackLink = in.readString();
        this.currency = in.readString();
        this.id = in.readString();
        this.stock = (Integer) in.readValue(Integer.class.getClassLoader());
        this.value = (Integer) in.readValue(Integer.class.getClassLoader());
        this.brand = in.readParcelable(Brand.class.getClassLoader());
        this.image = in.readString();
        this.redeemLimit = in.readString();
        this.redeemId = in.readString();
        this.hidePrice = in.readString();
        this.image64 = in.readString();
        this.redeemLabel = in.readString();
        this.transactionId = in.readString();
        this.tags = in.readString();
        this.currency3 = in.readString();
        this.name = in.readString();
        this.needConfirmation = in.readString();
        this.image128 = in.readString();
        this.voucherPurchaseValue = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<MyVoucher> CREATOR = new Creator<MyVoucher>() {
        @Override
        public MyVoucher createFromParcel(Parcel source) {
            return new MyVoucher(source);
        }

        @Override
        public MyVoucher[] newArray(int size) {
            return new MyVoucher[size];
        }
    };
}
