package com.ebizu.manis.sdk.rest.data;

import android.os.Parcel;

import com.ebizu.manis.sdk.entities.BeaconIntervalData;
import com.ebizu.manis.sdk.entities.Coordinate;

/**
 * Created by deni rohimat on 30/11/16.
 */
public class BeaconInterval {

    public static class RequestBody {
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response extends BeaconIntervalData {
        public Response(Parcel in) {
            super(in);
        }

        public Response() {
        }
    }
}
