package com.ebizu.manis.sdk.rest.data;

/**
 * Created by rezkya on 16/05/16.
 */
public class RestResponse<D> {

    public boolean success ;
    public String message;
    public D data;
    public int elapsed;

    public RestResponse() {
    }

    public RestResponse(boolean success, String message, D data, int elapsed) {
        this.success = success;
        this.message = message;
        this.data = data;
        this.elapsed = elapsed;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }

    public int getElapsed() {
        return elapsed;
    }

    public void setElapsed(int elapsed) {
        this.elapsed = elapsed;
    }
}
