package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 17/05/16.
 */
public class StoreData implements Parcelable {
    public static final String LONGTAIL = "longtail";
    public static final String NON_LONGTAIL = "nonlongtail";
    public static final String PARTNER = "partner";
    public static final String IMPORTANT = "important";
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("category")
    @Expose
    public StoreCategory category;
    @SerializedName("assets")
    @Expose
    public StoreAsset assets;
    @SerializedName("premium")
    @Expose
    public boolean premium;
    @SerializedName("coordinate")
    @Expose
    public Coordinate coordinate;
    @SerializedName("has")
    @Expose
    public StoreHas has;
    @SerializedName("multiplier")
    @Expose
    public int multiplier;
    @SerializedName("merchant_tier")
    @Expose
    public String tier;
    public String itemType;

    public StoreData() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.address);
        dest.writeParcelable(this.category, flags);
        dest.writeParcelable(this.assets, flags);
        dest.writeByte(this.premium ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.coordinate, flags);
        dest.writeParcelable(this.has, flags);
        dest.writeInt(this.multiplier);
        dest.writeString(this.tier);
        dest.writeString(this.itemType);
    }

    protected StoreData(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.address = in.readString();
        this.category = in.readParcelable(StoreCategory.class.getClassLoader());
        this.assets = in.readParcelable(StoreAsset.class.getClassLoader());
        this.premium = in.readByte() != 0;
        this.coordinate = in.readParcelable(Coordinate.class.getClassLoader());
        this.has = in.readParcelable(StoreHas.class.getClassLoader());
        this.multiplier = in.readInt();
        this.tier = in.readString();
        this.itemType = in.readString();
    }

    public static final Creator<StoreData> CREATOR = new Creator<StoreData>() {
        @Override
        public StoreData createFromParcel(Parcel source) {
            return new StoreData(source);
        }

        @Override
        public StoreData[] newArray(int size) {
            return new StoreData[size];
        }
    };
}
