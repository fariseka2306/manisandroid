package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 27/05/16.
 */
public class StoreAvailability implements Parcelable{
    @SerializedName("today")
    @Expose
    public StoreAvailabilityDetail today;
    @SerializedName("daily")
    @Expose
    public ArrayList<StoreAvailabilityDetail> daily = new ArrayList<>();

    public StoreAvailability(Parcel in) {
        today = in.readParcelable(StoreAvailabilityDetail.class.getClassLoader());
        daily = in.createTypedArrayList(StoreAvailabilityDetail.CREATOR);
    }

    public static final Creator<StoreAvailability> CREATOR = new Creator<StoreAvailability>() {
        @Override
        public StoreAvailability createFromParcel(Parcel in) {
            return new StoreAvailability(in);
        }

        @Override
        public StoreAvailability[] newArray(int size) {
            return new StoreAvailability[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(today,flags);
        dest.writeTypedList(daily);
    }
}
