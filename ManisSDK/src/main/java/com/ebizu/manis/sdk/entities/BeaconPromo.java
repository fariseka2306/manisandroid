package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.sdk.reward.models.Reward;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkyatinnov on 6/23/16.
 */
public class BeaconPromo implements Parcelable {

    private int idBeacon;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("detail")
    @Expose
    private BeaconPromoDetail detail;
    @SerializedName("sn")
    @Expose
    private String sn;
    //Rewards
    @SerializedName("can_only_redeem_once")
    @Expose
    private boolean canOnlyRedeemOnce;
    @SerializedName("already_redeemed")
    @Expose
    private boolean alreadyRedeemed;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("logo_small")
    @Expose
    private String logoSmall;
    @SerializedName("picture_small")
    @Expose
    private String pictureSmall;
    @SerializedName("stock_left")
    @Expose
    private int stockLeft;
    @SerializedName("expired")
    @Expose
    private String expired;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("reward_type")
    @Expose
    private String rewardType;
    @SerializedName("value")
    @Expose
    private int value;
    @SerializedName("params")
    @Expose
    private Params[] params;
    @SerializedName("tos")
    @Expose
    private String[] tos;
    @SerializedName("need_confirm_page")
    @Expose
    private boolean needConfirmPage;
    @SerializedName("sequence")
    @Expose
    private int sequence;
    //Raw JSON from Rewards
    @SerializedName("raw")
    @Expose
    private com.ebizu.sdk.reward.models.Reward reward;
    // for update data after redeem
    private int redeemed;
    private String datetime;
    private String code;

    public com.ebizu.sdk.reward.models.Reward getReward() {
        return reward;
    }

    public void setReward(Reward reward) {
        this.reward = reward;
    }

    public BeaconPromo() {
    }

    public int getIdBeacon() {
        return idBeacon;
    }

    public void setIdBeacon(int idBeacon) {
        this.idBeacon = idBeacon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BeaconPromoDetail getDetail() {
        return detail;
    }

    public void setDetail(BeaconPromoDetail detail) {
        this.detail = detail;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public boolean isCanOnlyRedeemOnce() {
        return canOnlyRedeemOnce;
    }

    public void setCanOnlyRedeemOnce(boolean canOnlyRedeemOnce) {
        this.canOnlyRedeemOnce = canOnlyRedeemOnce;
    }

    public boolean isAlreadyRedeemed() {
        return alreadyRedeemed;
    }

    public void setAlreadyRedeemed(boolean alreadyRedeemed) {
        this.alreadyRedeemed = alreadyRedeemed;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLogoSmall() {
        return logoSmall;
    }

    public void setLogoSmall(String logoSmall) {
        this.logoSmall = logoSmall;
    }

    public String getPictureSmall() {
        return pictureSmall;
    }

    public void setPictureSmall(String pictureSmall) {
        this.pictureSmall = pictureSmall;
    }

    public int getStockLeft() {
        return stockLeft;
    }

    public void setStockLeft(int stockLeft) {
        this.stockLeft = stockLeft;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Params[] getParams() {
        return params;
    }

    public void setParams(Params[] params) {
        this.params = params;
    }

    public String[] getTos() {
        return tos;
    }

    public void setTos(String[] tos) {
        this.tos = tos;
    }

    public boolean isNeedConfirmPage() {
        return needConfirmPage;
    }

    public void setNeedConfirmPage(boolean needConfirmPage) {
        this.needConfirmPage = needConfirmPage;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getRedeemed() {
        return redeemed;
    }

    public void setRedeemed(int redeemed) {
        this.redeemed = redeemed;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idBeacon);
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.company);
        dest.writeString(this.companyId);
        dest.writeString(this.companyName);
        dest.writeString(this.picture);
        dest.writeString(this.type);
        dest.writeParcelable(this.detail, flags);
        dest.writeString(this.sn);
        dest.writeByte(this.canOnlyRedeemOnce ? (byte) 1 : (byte) 0);
        dest.writeByte(this.alreadyRedeemed ? (byte) 1 : (byte) 0);
        dest.writeString(this.timestamp);
        dest.writeString(this.logo);
        dest.writeString(this.logoSmall);
        dest.writeString(this.pictureSmall);
        dest.writeInt(this.stockLeft);
        dest.writeString(this.expired);
        dest.writeString(this.currency);
        dest.writeString(this.rewardType);
        dest.writeInt(this.value);
        dest.writeTypedArray(this.params, flags);
        dest.writeStringArray(this.tos);
        dest.writeByte(this.needConfirmPage ? (byte) 1 : (byte) 0);
        dest.writeInt(this.sequence);
        dest.writeParcelable(this.reward, flags);
        dest.writeInt(this.redeemed);
        dest.writeString(this.datetime);
        dest.writeString(this.code);
    }

    protected BeaconPromo(Parcel in) {
        this.idBeacon = in.readInt();
        this.id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.company = in.readString();
        this.companyId = in.readString();
        this.companyName = in.readString();
        this.picture = in.readString();
        this.type = in.readString();
        this.detail = in.readParcelable(BeaconPromoDetail.class.getClassLoader());
        this.sn = in.readString();
        this.canOnlyRedeemOnce = in.readByte() != 0;
        this.alreadyRedeemed = in.readByte() != 0;
        this.timestamp = in.readString();
        this.logo = in.readString();
        this.logoSmall = in.readString();
        this.pictureSmall = in.readString();
        this.stockLeft = in.readInt();
        this.expired = in.readString();
        this.currency = in.readString();
        this.rewardType = in.readString();
        this.value = in.readInt();
        this.params = in.createTypedArray(Params.CREATOR);
        this.tos = in.createStringArray();
        this.needConfirmPage = in.readByte() != 0;
        this.sequence = in.readInt();
        this.reward = in.readParcelable(Reward.class.getClassLoader());
        this.redeemed = in.readInt();
        this.datetime = in.readString();
        this.code = in.readString();
    }

    public static final Creator<BeaconPromo> CREATOR = new Creator<BeaconPromo>() {
        @Override
        public BeaconPromo createFromParcel(Parcel source) {
            return new BeaconPromo(source);
        }

        @Override
        public BeaconPromo[] newArray(int size) {
            return new BeaconPromo[size];
        }
    };
}
