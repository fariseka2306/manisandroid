package com.ebizu.manis.sdk.rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.ebizu.manis.sdk.BuildConfig;
import com.ebizu.manis.sdk.utils.ManisConst;
import com.ebizu.manis.sdk.utils.ManisSession;
import com.ebizu.manis.sdk.utils.RSACrypto;
import com.ebizu.manis.sdk.utils.SessionKey;

import java.io.IOException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rezkya on 16/05/16.
 */
public class RestService {

    private static RestService instance;
    private static Retrofit retrofit;
    private static RestConnect restConnect;
    private static RestConnect restConnectWithRetry;
    private static OkHttpClient client;
    private static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

    private static RSACrypto crypto;
    private static Key publicKey;
    private static Key privateKey;
    public static String _publicKey;

    private RestService(Context context) {
        SharedPreferences prefKey = context.getSharedPreferences(
                ManisConst.KEY_PREF, Context.MODE_PRIVATE);
        if (SessionKey.haveKey(prefKey)) {
            try {
                KeyFactory kf = KeyFactory.getInstance("RSA");

                _publicKey = SessionKey.getPublicKey(prefKey);
                byte[] decodedPublicKey = Base64.decode(_publicKey, Base64.NO_WRAP);
                publicKey = kf.generatePublic(new X509EncodedKeySpec(decodedPublicKey));

                byte[] decodePrivateKey = Base64.decode(SessionKey.getPrivateKey(prefKey), Base64.NO_WRAP);
                privateKey = kf.generatePublic(new X509EncodedKeySpec(decodePrivateKey));

            } catch (Exception e) {
                Log.e("Error", "RSA encryption error");
            }
        } else {
            crypto = RSACrypto.getInstance();
            publicKey = crypto.getPublicKey();
            privateKey = crypto.getPrivateKey();
            _publicKey = Base64.encodeToString(publicKey.getEncoded(), Base64.NO_WRAP);
        }
    }

    public static synchronized RestService getInstance(Context context) {
        if (instance == null) {
            instance = new RestService(context);
            loggingInterceptor.setLevel(getEnvironmentLevel(BuildConfig.ENVIRONMENT));
        }

        return instance;
    }

    private static HttpLoggingInterceptor.Level getEnvironmentLevel(int env) {
        return env == 0 ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE;
    }

    public RestConnect getConnections() {
        if (restConnect == null) {
            client = new OkHttpClient.Builder()
                    .addInterceptor(headerInterceptor)
                    .addInterceptor(loggingInterceptor)
//                    .addInterceptor(decryptorInterceptor)
                    .retryOnConnectionFailure(false)
                    .connectTimeout(120000, TimeUnit.MILLISECONDS)
                    .readTimeout(120000, TimeUnit.MILLISECONDS)
                    .writeTimeout(120000, TimeUnit.MILLISECONDS)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(EndPoint.getApiBaseUri())
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            restConnect = retrofit.create(RestConnect.class);
        }
        return restConnect;
    }

    public RestConnect getConnectionsWithRetry() {
        if (restConnectWithRetry == null) {
            client = new OkHttpClient.Builder()
                    .addInterceptor(headerInterceptor)
                    .addInterceptor(loggingInterceptor)
//                    .addInterceptor(decryptorInterceptor)
                    .retryOnConnectionFailure(true)
                    .connectTimeout(120000, TimeUnit.MILLISECONDS)
                    .readTimeout(120000, TimeUnit.MILLISECONDS)
                    .writeTimeout(120000, TimeUnit.MILLISECONDS)
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(EndPoint.getApiBaseUri())
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            restConnectWithRetry = retrofit.create(RestConnect.class);
        }
        return restConnectWithRetry;
    }

    public String encryptedMsg(String data) {
        return crypto.encrypt("" + data);
    }

    private static Interceptor headerInterceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request().newBuilder()
                    .addHeader("manis-version", ManisSession.getSession(ManisSession.SessionName.VERSIONNAME))
                    .addHeader("manis-build", ManisSession.getSession(ManisSession.SessionName.VERSIONCODE))
                    .build();
            return chain.proceed(request);
        }
    };

    private static Interceptor decryptorInterceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            okhttp3.Response response = chain.proceed(chain.request());
            if (response.isSuccessful()) {
                okhttp3.Response.Builder newResponse = response.newBuilder();
                String contentType = response.header("Content-Type");

                String encrypted = response.body().string();
                System.out.println("encrypted : " + encrypted);
                String result = "";
                try {
                    result = crypto.decrypt(encrypted, privateKey);
                    System.out.println("result : " + result);
                } catch (Exception e) {
                    System.out.println("Error : " + e.toString());
                }

                return newResponse.body(ResponseBody.create(MediaType.parse(contentType), result)).build();
            }

            return response;
        }
    };
}
