package com.ebizu.manis.sdk.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ebizu on 9/18/17.
 */

public class MyVoucherData {

    @SerializedName("pageCount")
    @Expose
    private Integer pageCount;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("more")
    @Expose
    private Boolean more;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("data")
    @Expose
    private List<MyVoucher> rewardVouchers;

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Boolean getMore() {
        return more;
    }

    public void setMore(Boolean more) {
        this.more = more;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<MyVoucher> getRewardVouchers() {
        return rewardVouchers;
    }

    public void setRewardVouchers(List<MyVoucher> rewardVouchers) {
        this.rewardVouchers = rewardVouchers;
    }
}
