package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 9/13/17.
 */

public class RewardSession implements Parcelable {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("userEmail")
    @Expose
    private String userEmail;
    @SerializedName("userCountry")
    @Expose
    private String userCountry;
    @SerializedName("userTimezone")
    @Expose
    private String userTimezone;
    @SerializedName("userJoin")
    @Expose
    private String userJoin;
    @SerializedName("userAgent")
    @Expose
    private String userAgent;
    @SerializedName("osName")
    @Expose
    private String osName;
    @SerializedName("osVersion")
    @Expose
    private String osVersion;
    @SerializedName("applicationBuild")
    @Expose
    private String applicationBuild;
    @SerializedName("applicationVersion")
    @Expose
    private String applicationVersion;
    @SerializedName("manufacture")
    @Expose
    private String manufacture;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("msisdn")
    @Expose
    private String msisdn;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("imsi")
    @Expose
    private String imsi;
    @SerializedName("session")
    @Expose
    private String session;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }

    public String getUserTimezone() {
        return userTimezone;
    }

    public void setUserTimezone(String userTimezone) {
        this.userTimezone = userTimezone;
    }

    public String getUserJoin() {
        return userJoin;
    }

    public void setUserJoin(String userJoin) {
        this.userJoin = userJoin;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getApplicationBuild() {
        return applicationBuild;
    }

    public void setApplicationBuild(String applicationBuild) {
        this.applicationBuild = applicationBuild;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userId);
        dest.writeString(this.userName);
        dest.writeString(this.userEmail);
        dest.writeString(this.userCountry);
        dest.writeString(this.userTimezone);
        dest.writeString(this.userJoin);
        dest.writeString(this.userAgent);
        dest.writeString(this.osName);
        dest.writeString(this.osVersion);
        dest.writeString(this.applicationBuild);
        dest.writeString(this.applicationVersion);
        dest.writeString(this.manufacture);
        dest.writeString(this.model);
        dest.writeString(this.msisdn);
        dest.writeString(this.imei);
        dest.writeString(this.imsi);
        dest.writeString(this.session);
    }

    public RewardSession() {
    }

    protected RewardSession(Parcel in) {
        this.userId = in.readString();
        this.userName = in.readString();
        this.userEmail = in.readString();
        this.userCountry = in.readString();
        this.userTimezone = in.readString();
        this.userJoin = in.readString();
        this.userAgent = in.readString();
        this.osName = in.readString();
        this.osVersion = in.readString();
        this.applicationBuild = in.readString();
        this.applicationVersion = in.readString();
        this.manufacture = in.readString();
        this.model = in.readString();
        this.msisdn = in.readString();
        this.imei = in.readString();
        this.imsi = in.readString();
        this.session = in.readString();
    }

    public static final Creator<RewardSession> CREATOR = new Creator<RewardSession>() {
        @Override
        public RewardSession createFromParcel(Parcel source) {
            return new RewardSession(source);
        }

        @Override
        public RewardSession[] newArray(int size) {
            return new RewardSession[size];
        }
    };
}
