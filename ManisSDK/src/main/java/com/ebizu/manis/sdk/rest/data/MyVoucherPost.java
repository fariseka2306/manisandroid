package com.ebizu.manis.sdk.rest.data;

/**
 * Created by ebizu on 9/18/17.
 */

public class MyVoucherPost {
    public static class RequestBody {

        public MyVoucherPost.Data data;
        public String session;
        public String module;
        public String action;
        public String token;

        public RequestBody(MyVoucherPost.Data data, String session, String module, String action, String token) {
            this.data = data;
            this.session = session;
            this.module = module;
            this.action = action;
            this.token = token;
        }
    }

    public static class Data {

        private int size;
        private int page;
        private int order;

        public Data(int size, int page, int order) {
            this.size = size;
            this.page = page;
            this.order = order;
        }
    }
}
