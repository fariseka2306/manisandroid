package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by deni on 06/01/17.
 */
public class RewardsRedeemCloseFailed {

    public static class RequestBody {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("ref")
        @Expose
        private String ref;
        @SerializedName("point")
        @Expose
        private long point;

        public RequestBody(String id, String ref, long point) {
            this.id = id;
            this.ref = ref;
            this.point = point;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, RequestBody requestBody) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = requestBody;
        }
    }

    public static class Response {
        private int point;

        public Response() {
        }

        public Response(int point) {
            this.point = point;
        }

        public int getPoint() {
            return point;
        }

        public void setPoint(int point) {
            this.point = point;
        }
    }
}
