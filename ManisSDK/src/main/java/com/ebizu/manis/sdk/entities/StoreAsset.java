package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 17/05/16.
 */
public class StoreAsset implements Parcelable {

    @SerializedName("icon")
    @Expose
    public String icon;
    @SerializedName("photo")
    @Expose
    public String photo;
    @SerializedName("banner")
    @Expose
    public String banner;

    public StoreAsset(String photo, String banner, String icon) {
        this.photo = photo;
        this.banner = banner;
        this.icon = icon;
    }

    public StoreAsset(Parcel in) {
        photo = in.readString();
        banner = in.readString();
        icon = in.readString();
    }

    public static final Creator<StoreAsset> CREATOR = new Creator<StoreAsset>() {
        @Override
        public StoreAsset createFromParcel(Parcel in) {
            return new StoreAsset(in);
        }

        @Override
        public StoreAsset[] newArray(int size) {
            return new StoreAsset[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(photo);
        dest.writeString(banner);
        dest.writeString(icon);
    }
}
