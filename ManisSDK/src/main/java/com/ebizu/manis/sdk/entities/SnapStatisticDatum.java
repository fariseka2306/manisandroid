package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class SnapStatisticDatum implements Parcelable{
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("amount")
    @Expose
    public double amount;
    @SerializedName("percentage")
    @Expose
    public int percentage;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeDouble(this.amount);
        dest.writeInt(this.percentage);
    }

    public SnapStatisticDatum() {
    }

    protected SnapStatisticDatum(Parcel in) {
        this.name = in.readString();
        this.amount = in.readDouble();
        this.percentage = in.readInt();
    }

    public static final Creator<SnapStatisticDatum> CREATOR = new Creator<SnapStatisticDatum>() {
        @Override
        public SnapStatisticDatum createFromParcel(Parcel source) {
            return new SnapStatisticDatum(source);
        }

        @Override
        public SnapStatisticDatum[] newArray(int size) {
            return new SnapStatisticDatum[size];
        }
    };
}
