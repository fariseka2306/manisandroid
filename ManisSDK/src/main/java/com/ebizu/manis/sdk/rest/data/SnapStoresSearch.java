package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.StoreData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 27/05/16.
 */
public class SnapStoresSearch {

    public static class RequestBody {

        @SerializedName("keyword")
        @Expose
        public String keyword;
        @SerializedName("brand")
        @Expose
        public int brand;
        @SerializedName("page")
        @Expose
        public int page;
        @SerializedName("size")
        @Expose
        public int size;

        public RequestBody(String keyword, int brand, int page, int size) {
            this.keyword = keyword;
            this.brand = brand;
            this.page = page;
            this.size = size;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude, String keyword, int brand, int page, int size) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody(keyword, brand, page, size);
        }
    }

    public static class Response {
        @SerializedName("more")
        @Expose
        public boolean more;
        @SerializedName("results")
        @Expose
        public ArrayList<StoreData> results = new ArrayList<>();
    }
}
