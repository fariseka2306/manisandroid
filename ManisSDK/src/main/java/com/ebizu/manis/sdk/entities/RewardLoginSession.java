package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 9/16/17.
 */

public class RewardLoginSession implements Parcelable {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("session")
    @Expose
    private String session;
    @SerializedName("userName")
    @Expose
    private String userName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userId);
        dest.writeString(this.session);
        dest.writeString(this.userName);
    }

    public RewardLoginSession() {
    }

    protected RewardLoginSession(Parcel in) {
        this.userId = in.readString();
        this.session = in.readString();
        this.userName = in.readString();
    }

    public static final Creator<RewardLoginSession> CREATOR = new Creator<RewardLoginSession>() {
        @Override
        public RewardLoginSession createFromParcel(Parcel source) {
            return new RewardLoginSession(source);
        }

        @Override
        public RewardLoginSession[] newArray(int size) {
            return new RewardLoginSession[size];
        }
    };
}
