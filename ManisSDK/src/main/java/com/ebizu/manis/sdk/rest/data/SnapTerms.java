package com.ebizu.manis.sdk.rest.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class SnapTerms {

    public static class RequestBody {
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response implements Parcelable {
        @SerializedName("validity")
        @Expose
        private String validity;
        @SerializedName("limit")
        @Expose
        private String limit;
        @SerializedName("point")
        @Expose
        private String point;
        @SerializedName("short_description")
        @Expose
        private String shortDescription;

        public Response(String validity, String limit, String point, String shortDescription) {
            this.validity = validity;
            this.limit = limit;
            this.point = point;
            this.shortDescription = shortDescription;
        }

        public Response() {
        }

        protected Response(Parcel in) {
            validity = in.readString();
            limit = in.readString();
            point = in.readString();
            shortDescription = in.readString();
        }

        public static final Creator<Response> CREATOR = new Creator<Response>() {
            @Override
            public Response createFromParcel(Parcel in) {
                return new Response(in);
            }

            @Override
            public Response[] newArray(int size) {
                return new Response[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(validity);
            dest.writeString(limit);
            dest.writeString(point);
            dest.writeString(shortDescription);
        }

        public void setValidity(String validity) {
            this.validity = validity;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public void setPoint(String point) {
            this.point = point;
        }

        public String getValidity() {
            return validity;
        }

        public String getLimit() {
            return limit;
        }

        public String getPoint() {
            return point;
        }

        public String getShortDescription() {
            return shortDescription;
        }

        public void setShortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
        }
    }
}
