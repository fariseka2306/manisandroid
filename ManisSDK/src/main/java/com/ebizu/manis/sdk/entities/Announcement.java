package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 09/06/16.
 */
public class Announcement implements Parcelable{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("assets")
    @Expose
    private StoreAsset assets;

    public Announcement(Parcel in) {
        title = in.readString();
        content = in.readString();
        label = in.readString();
        uri = in.readString();
        assets = in.readParcelable(StoreAsset.class.getClassLoader());
    }

    public Announcement(String title, String content, String label, String uri, StoreAsset assets) {
        this.title = title;
        this.content = content;
        this.label = label;
        this.uri = uri;
        this.assets = assets;
    }

    public static final Creator<Announcement> CREATOR = new Creator<Announcement>() {
        @Override
        public Announcement createFromParcel(Parcel in) {
            return new Announcement(in);
        }

        @Override
        public Announcement[] newArray(int size) {
            return new Announcement[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(label);
        dest.writeString(uri);
        dest.writeParcelable(assets, flags);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public StoreAsset getAssets() {
        return assets;
    }

    public void setAssets(StoreAsset assets) {
        this.assets = assets;
    }

    public static Creator<Announcement> getCREATOR() {
        return CREATOR;
    }
}
