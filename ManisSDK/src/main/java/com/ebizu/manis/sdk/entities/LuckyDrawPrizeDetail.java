package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by novyandi-ebizu on 7/21/17.
 */
public class LuckyDrawPrizeDetail implements Parcelable{

    @SerializedName("prize_id")
    @Expose
    private int prizeId;
    @SerializedName("prize_name")
    @Expose
    private String prizeName;
    @SerializedName("prize_image")
    @Expose
    private String prizeImage;

    public int getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(int prizeId) {
        this.prizeId = prizeId;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public String getPrizeImage() {
        return prizeImage;
    }

    public void setPrizeImage(String prizeImage) {
        this.prizeImage = prizeImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.prizeId);
        dest.writeString(this.prizeName);
        dest.writeString(this.prizeImage);
    }

    public LuckyDrawPrizeDetail() {
    }

    protected LuckyDrawPrizeDetail(Parcel in) {
        this.prizeId = in.readInt();
        this.prizeName = in.readString();
        this.prizeImage = in.readString();
    }

    public static final Creator<LuckyDrawPrizeDetail> CREATOR = new Creator<LuckyDrawPrizeDetail>() {
        @Override
        public LuckyDrawPrizeDetail createFromParcel(Parcel source) {
            return new LuckyDrawPrizeDetail(source);
        }

        @Override
        public LuckyDrawPrizeDetail[] newArray(int size) {
            return new LuckyDrawPrizeDetail[size];
        }
    };
}
