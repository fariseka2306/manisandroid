package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by novyandi-ebizu on 7/21/17.
 */

public class LuckyDrawPrizes implements Parcelable{

    @SerializedName("prize_title")
    @Expose
    private String prizeTitle;
    @SerializedName("prize_details")
    @Expose
    private List<LuckyDrawPrizeDetail> luckyDrawPrizeDetails = null;

    public String getPrizeTitle() {
        return prizeTitle;
    }

    public void setPrizeTitle(String prizeTitle) {
        this.prizeTitle = prizeTitle;
    }

    public List<LuckyDrawPrizeDetail> getLuckyDrawPrizeDetails() {
        return luckyDrawPrizeDetails;
    }

    public void setLuckyDrawPrizeDetails(List<LuckyDrawPrizeDetail> luckyDrawPrizeDetails) {
        this.luckyDrawPrizeDetails = luckyDrawPrizeDetails;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.prizeTitle);
        dest.writeTypedList(this.luckyDrawPrizeDetails);
    }

    public LuckyDrawPrizes() {
    }

    protected LuckyDrawPrizes(Parcel in) {
        this.prizeTitle = in.readString();
        this.luckyDrawPrizeDetails = in.createTypedArrayList(LuckyDrawPrizeDetail.CREATOR);
    }

    public static final Creator<LuckyDrawPrizes> CREATOR = new Creator<LuckyDrawPrizes>() {
        @Override
        public LuckyDrawPrizes createFromParcel(Parcel source) {
            return new LuckyDrawPrizes(source);
        }

        @Override
        public LuckyDrawPrizes[] newArray(int size) {
            return new LuckyDrawPrizes[size];
        }
    };
}
