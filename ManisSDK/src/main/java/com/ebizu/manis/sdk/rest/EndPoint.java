package com.ebizu.manis.sdk.rest;

import com.ebizu.manis.sdk.BuildConfig;
import com.ebizu.manis.sdk.utils.ManisConst;
import com.ebizu.manis.sdk.utils.ManisSession;

/**
 * Created by rezkya on 16/05/16.
 */
public class EndPoint {
    private static final String PRODUCTION_DOMAIN = "https://apimanis.ebizu.com/";
    private static final String DEVELOPMENT_DOMAIN = "https://apixv3.ebizu.com/";
    private static final String STAGING_DOMAIN = "https://apixv3.ebizu.com/";
    private static final String VERSION = "v1/";

    public final static String authSignIn = "auth/signin";
    public final static String authSignInPhone = "auth/signin/phone";
    public final static String authSignInGoogle = "auth/signin/google";
    public final static String authSignUpOTP = "signup/otp";
    public final static String authSignOut = "auth/signout";
    public final static String beaconInterval = "beacon/interval";
    public final static String snapStoresNearby = "snap/stores/nearby";
    public final static String snapStoresPinned = "snap/stores/pinned";
    public final static String snapStoresRecent = "snap/stores/recent";
    public final static String snapStoresSearch = "snap/stores/search";
    public final static String snapReceiptUpload = "snap/receipt/upload";
    public final static String snapHistory = "snap/history";
    public final static String snapStatistic = "snap/statistic";
    public final static String snapLast = "snap/last";
    public final static String snapTerms = "snap/terms";
    public final static String snapAppeal = "snap/appeal";
    public final static String snapSuggestions = "snap/suggestions";
    public final static String snapChecksnapable = "snap/checksnapable";
    public final static String interests = "interests";
    public final static String storesNearby = "stores/nearby";
    public final static String storesInterests = "stores/interests";
    public final static String storesInterest = "stores/interest";
    public final static String storesDetail = "stores/detail";
    public final static String storesPin = "stores/pin";
    public final static String storesUnpin = "stores/unpin";
    public final static String storesOfferPin = "stores/offer/pin";
    public final static String storesOfferUnppin = "stores/offer/unpin";
    public final static String storesOfferRedeem = "stores/offer/redeem";
    public final static String storesSearch = "stores/search";
    public final static String interestUpdate = "interests/update/bulk";
    public final static String interestPinned = "interests/pinned";
    public final static String interestUnpinned = "interests/unpinned";
    public final static String beaconPromoNearby = "beacon/promo/nearby";
    public final static String reviewGetStatus = "review";
    public final static String reviewPostPopup = "review/post";
    public final static String rewardsVouchers = "rewards/vouchers";
    public final static String rewardsVoucherRedeem = "rewards/voucher/redeem";
    public final static String rewardsPinRedeem = "rewards/pin/redeem";
    public final static String rewardsPin = "rewards/pin";
    public final static String rewardsUnpin = "rewards/unpin";
    public final static String rewardsRedeem = "rewards/redeem/new";
    public final static String rewardsRedeemClose = "rewards/redeem/close/new";
    public final static String rewardsRedeemCloseFailed = "rewards/redeem/close/failed/new";
    public final static String rewardsClaimed = "rewards/claimed";
    public final static String rewardsVoucherSearch = "rewards/voucher/search";
    public final static String accountPoint = "account/point";
    public final static String accountGetDetail = "account/get";
    public final static String accountInviteTerms = "account/invite/terms";
    public final static String accountProfileUpdate = "account/profile/update";
    public final static String accountProfilePictureUpdate = "account/profile/picture/update";
    public final static String accountSettingsNotifications = "account/settings/notifications";
    public final static String accountSettingsNotificationsUpdate = "account/settings/notifications/update";
    public final static String saved = "saved";
    public final static String miscHelpLegal = "misc/help/legal";
    public final static String miscHelp = "misc/help";
    public final static String miscAnnouncement = "misc/announcement";
    public final static String deeplinkOffer = "deeplink/offer";
    public final static String deeplinkSnap = "deeplink/snap";
    public final static String deeplinkStore = "deeplink/store";
    public final static String deeplinkCashvoucher = "deeplink/cashvoucher";
    public final static String deeplinkRewards = "deeplink/reward";
    public final static String otpStatus = "otp/status";
    public final static String otpSkip = "otp/skip";
    public final static String brandFeatured = "brands/featured";
    public final static String luckyDrawScreen = "luckydraw/screen";
    public final static String luckyDrawEntries = "luckydraw/entries";
    public final static String luckyDrawWinners = "luckydraw/winners";
    public final static String luckyDrawTerms = "luckydraw/terms";
    public final static String luckyDrawUpload = "luckydraw/upload";
    public final static String rewardApi = "https://2eknxplj51.execute-api.ap-northeast-1.amazonaws.com/production/api";


    public static String getDomain() {
        return (ManisSession.getSession(ManisSession.SessionName.ACTIVE_ENV).compareToIgnoreCase(ManisConst.API_ENV_DEVELOPMENT) == 0) ?
                DEVELOPMENT_DOMAIN :
                ((ManisSession.getSession(ManisSession.SessionName.ACTIVE_ENV).compareToIgnoreCase(ManisConst.API_ENV_STAGING) == 0) ? STAGING_DOMAIN : PRODUCTION_DOMAIN);
    }

    public static String getApiBaseUri() {
        return getDomain() + VERSION;
    }
}
