package com.ebizu.manis.sdk.rest;

import android.content.Intent;

import com.ebizu.manis.sdk.entities.RewardMessage;
import com.ebizu.manis.sdk.shared.ManisApp;
import com.ebizu.manis.sdk.utils.ManisConst;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by halim_ebizu on 8/29/17.
 */


public abstract class RewardCallback<T extends RewardRestResponse> implements retrofit2.Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            if (response.body() != null) {
                if (response.body().getData() != null) {
                    onSuccess(response.code(), response.body());
                } else {
                    if (response.body().errorCode == -1) {
                        RewardMessage rewardMessage = new RewardMessage();
                        rewardMessage.setErrorCode("-1");
                        rewardMessage.setErrorMessage(response.body().getErrorMessage());
                        rewardMessage.setTime(response.body().getTime());
                        response.body().setData(rewardMessage);
                        onSuccess(response.code(), response.body());
                    } else {
                        onFailed(response.code(), response.body().getErrorMessage());
                    }

                }
            } else {
                onFailed(response.code(), ManisApp.context.getString(com.ebizu.manis.sdk.R.string.error_no_data_received));
            }
        } else {
            Gson gson = new Gson();
            try {
                RewardRestResponse body = gson.fromJson(response.errorBody().string(), RewardRestResponse.class);
                if (body != null) {
                    if (response.code() == 446) {
                        Intent intent = new Intent();
                        intent.setAction(ManisConst.TOKEN_EXPIRED_INTENT_ACTION);
                        intent.putExtra(ManisConst.TOKEN_EXPIRED_INTENT_NAME, body.getErrorCode());
                        ManisApp.context.sendBroadcast(intent);
                    } else {
                        onFailed(response.code(), body.getErrorMessage());
                    }
                } else {
                    onFailed(response.code(), ManisApp.context.getString(com.ebizu.manis.sdk.R.string.error_something));
                }
            } catch (Exception e) {
                onFailed(response.code(), ManisApp.context.getString(com.ebizu.manis.sdk.R.string.error_failed_data));
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onFailed(600, ManisApp.context.getString(com.ebizu.manis.sdk.R.string.error_no_connection));
    }

    public abstract void onSuccess(int code, T body);

    public abstract void onFailed(int code, String message);
}
