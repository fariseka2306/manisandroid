package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.LoginData;

/**
 * Created by deni rohimat on 13/01/17.
 */
public class AccountProfileDetail {

    public static class RequestBody {
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody();
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends LoginData {
        public Response(Parcel in) {
            super(in);
        }
    }
}
