package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 26/05/16.
 */
public class StoreCategory implements Parcelable{
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("asset")
    @Expose
    public StoreAsset assets;
    @SerializedName("parent")
    @Expose
    public int parent;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeParcelable(this.assets, flags);
        dest.writeInt(this.parent);
    }

    public StoreCategory() {
    }

    protected StoreCategory(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.assets = in.readParcelable(StoreAsset.class.getClassLoader());
        this.parent = in.readInt();
    }

    public static final Creator<StoreCategory> CREATOR = new Creator<StoreCategory>() {
        @Override
        public StoreCategory createFromParcel(Parcel source) {
            return new StoreCategory(source);
        }

        @Override
        public StoreCategory[] newArray(int size) {
            return new StoreCategory[size];
        }
    };
}
