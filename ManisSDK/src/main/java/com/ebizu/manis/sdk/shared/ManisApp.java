package com.ebizu.manis.sdk.shared;

import android.app.Application;
import android.content.Context;

import com.ebizu.manis.sdk.database.LocalData;

import net.gotev.uploadservice.BuildConfig;
import net.gotev.uploadservice.Logger;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.okhttp.OkHttpStack;
import net.sqlcipher.database.SQLiteDatabase;

import okhttp3.OkHttpClient;

/**
 * Created by rezkya on 16/05/16.
 */
public class ManisApp extends Application {

    private static ManisApp instance;
    public static Context context;

    public static ManisApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        context=getApplicationContext();
        if(LocalData.encrypted) {
            SQLiteDatabase.loadLibs(context);
        }

        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        OkHttpClient client = new OkHttpClient();
        UploadService.HTTP_STACK = new OkHttpStack(client);
        Logger.setLogLevel(Logger.LogLevel.INFO);
    }
}
