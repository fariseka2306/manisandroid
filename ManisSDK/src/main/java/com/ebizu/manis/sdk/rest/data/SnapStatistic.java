package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.Datetime;
import com.ebizu.manis.sdk.entities.SnapStatisticData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class SnapStatistic {

    public static final String LIMIT_THIS_MONTH = "tm";
    public static final String LIMIT_LAST_MONTH = "lm";
    public static final String LIMIT_THIS_WEEK = "tw";
    public static final String LIMIT_LAST_WEEK = "lw";

    public static class RequestBody {

        @SerializedName("limit")
        @Expose
        public String limit;

        public RequestBody(String limit) {
            this.limit = limit;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude, String limit) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody(limit);
        }
    }

    public static class Response {
        @SerializedName("datetime")
        @Expose
        public Datetime datetime;
        @SerializedName("statistic")
        @Expose
        public SnapStatisticData statistic;
    }
}
