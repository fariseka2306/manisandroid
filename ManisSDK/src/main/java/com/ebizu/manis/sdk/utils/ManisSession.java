package com.ebizu.manis.sdk.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.ebizu.manis.sdk.Manis;
import com.ebizu.manis.sdk.shared.ManisApp;

/**
 * Created by rezkya on 17/05/16.
 */
public class ManisSession {
    public static final String TAG = "com.ebizu.manis.sdk.session";

    public static class SessionName {
        public static String ACTIVE_ENV = "com.ebizu.manis.sdk.session-ENV";
        public static String LANG = "com.ebizu.manis.sdk.session-lang";
        public static String TOKEN = "com.ebizu.manis.sdk.session-token";
        public static String DEVICE = "com.ebizu.manis.sdk.session-device";
        public static String VERSION = "com.ebizu.manis.sdk.session-version";
        private static String ENCRYPT_ENABLED = "com.ebizu.manis.sdk.encrypted";
        public static String LAST_KNOWN_LOCATION = "com.ebizu.manis.sdk.session-lastlocation";
        public static String SHOW_EXPLANATION = "com.ebizu.manis.sdk.session-SHOW_EXPLANATION";
        public static String VERSIONNAME = "com.ebizu.manis.sdk.session-versionname";
        public static String VERSIONCODE = "com.ebizu.manis.sdk.session-versioncode";
    }

    public static void setEncryptEnabled(boolean isEncypted) {
        setSession(SessionName.ENCRYPT_ENABLED, isEncypted ? "true" : "false");
    }

    public static boolean isEncryptEnabled() {
        String encryptEnabled = getSession(SessionName.ENCRYPT_ENABLED);
        return encryptEnabled.equalsIgnoreCase("true");
    }

    public static void setSession(String key, String value) {
        SharedPreferences sharedPref = Manis.getContext().getSharedPreferences(TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getSession(String key) {
        SharedPreferences sharedPref = ManisApp.getInstance().getApplicationContext().getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return sharedPref.getString(key, "");
    }

    public static void clearSession() {
        SharedPreferences sharedPref = ManisApp.getInstance().getApplicationContext().getSharedPreferences(TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(SessionName.TOKEN);
        editor.remove(SessionName.LANG);
        editor.remove(SessionName.DEVICE);
        editor.remove(SessionName.VERSION);
        editor.remove(SessionName.ENCRYPT_ENABLED);
        editor.remove(SessionName.LAST_KNOWN_LOCATION);
        editor.apply();
    }

    public static void removeSession(String key) {
        SharedPreferences sharedPref = ManisApp.getInstance().getApplicationContext().getSharedPreferences(TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(key);
        editor.apply();
    }

    public static boolean isLoggedIn() {
        return !getSession(SessionName.TOKEN).isEmpty();
    }
}