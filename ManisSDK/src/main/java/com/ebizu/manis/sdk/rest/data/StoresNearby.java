package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.StoreData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 27/05/16.
 */
public class StoresNearby {

    public static class RequestBody {

        @SerializedName("multiplier")
        @Expose
        public String multiplier;
        @SerializedName("page")
        @Expose
        public int page;
        @SerializedName("size")
        @Expose
        public int size;
        @SerializedName("merchant_tier")
        @Expose
        public String merchantType;

        public RequestBody(String multiplier, int page, int size, String merchantType) {
            this.multiplier = multiplier;
            this.page = page;
            this.size = size;
            this.merchantType = merchantType;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, String multiplier, int page, int size, String merchantType) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(multiplier, page, size, merchantType);
        }
    }

    public static class Response {
        @SerializedName("more")
        @Expose
        public boolean more;
        @SerializedName("results")
        @Expose
        public ArrayList<StoreData> results = new ArrayList<>();
    }
}