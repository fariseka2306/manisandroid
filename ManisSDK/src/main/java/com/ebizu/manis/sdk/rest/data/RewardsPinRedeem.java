package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.RedeemPin;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 30/05/16.
 */
public class RewardsPinRedeem {
    public static final int HTTP_INCORRECT_PIN = 440;
    public static final int HTTP_FAILED_UPDATE_VOUCHER = 441;
    public static final int HTTP_ALREADY_REDEEM_DAILY = 442;
    public static final int HTTP_FAILED_GET_CUSTOMER_ID = 443;
    public static final int HTTP_FAILED_GET_MEMBER_DATA = 444;

    public static class RequestBody {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("pin")
        @Expose
        private String pin;

        public RequestBody(String id, String pin) {
            this.id = id;
            this.pin = pin;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPin() {
            return pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, String id, String pin) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(id,pin);
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends RedeemPin {
        protected Response(Parcel in) {
            super(in);
        }
    }
}
