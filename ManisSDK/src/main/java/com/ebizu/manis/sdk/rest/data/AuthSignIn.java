package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.LoginData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 17/05/16.
 */
public class AuthSignIn {
    public static class RequestBody {
        @SerializedName("facebook")
        @Expose
        public Facebook facebook;
        @SerializedName("device")
        @Expose
        public Device device;

        public RequestBody(Facebook facebook, Device device) {
            this.facebook = facebook;
            this.device = device;
        }

        public static class Facebook {
            @SerializedName("id")
            @Expose
            public String id;
            @SerializedName("name")
            @Expose
            public String name;
            @SerializedName("email")
            @Expose
            public String email;
            @SerializedName("gender")
            @Expose
            public String gender;
            @SerializedName("birthday")
            @Expose
            public String birthday;
            @SerializedName("verified")
            @Expose
            public boolean verified;

            public Facebook(String id, String name, String email, String gender, String birthday, boolean verified) {
                this.id = id;
                this.name = name;
                this.email = email;
                this.gender = gender;
                this.birthday = birthday;
                this.verified = verified;
            }
        }
        public static class Device {
            @SerializedName("manufacture")
            @Expose
            public String manufacture;
            @SerializedName("model")
            @Expose
            public String model;
            @SerializedName("version")
            @Expose
            public String version;
            @SerializedName("msisdn")
            @Expose
            public String msisdn;
            @SerializedName("operator")
            @Expose
            public String operator;
            @SerializedName("imei")
            @Expose
            public String imei;
            @SerializedName("imsi")
            @Expose
            public String imsi;
            @SerializedName("token")
            @Expose
            public String token;

            public Device(String manufacture, String model, String version, String msisdn, String imei, String imsi, String token, String operator) {
                this.manufacture = manufacture;
                this.model = model;
                this.version = version;
                this.msisdn = msisdn;
                this.imei = imei;
                this.imsi = imsi;
                this.token = token;
                this.operator = operator;
            }
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody>{

        public Request(double latitude, double longitude, RequestBody.Facebook facebook, RequestBody.Device device) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody(facebook,device);
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends LoginData {
        public Response(Parcel in) {
            super(in);
        }

    public Response() {
    }

    //        public Response(Parcel in) {
//            super(in);
//        }
    }
}
