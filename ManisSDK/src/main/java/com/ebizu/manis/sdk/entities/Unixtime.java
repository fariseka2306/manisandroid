package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class Unixtime implements Parcelable {

    @SerializedName("start")
    @Expose
    public long start;
    @SerializedName("end")
    @Expose
    public long end;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.start);
        dest.writeLong(this.end);
    }

    public Unixtime() {
    }

    protected Unixtime(Parcel in) {
        this.start = in.readLong();
        this.end = in.readLong();
    }

    public static final Creator<Unixtime> CREATOR = new Creator<Unixtime>() {
        @Override
        public Unixtime createFromParcel(Parcel source) {
            return new Unixtime(source);
        }

        @Override
        public Unixtime[] newArray(int size) {
            return new Unixtime[size];
        }
    };
}
