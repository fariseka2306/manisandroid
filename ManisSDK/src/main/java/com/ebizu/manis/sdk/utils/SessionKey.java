package com.ebizu.manis.sdk.utils;

import android.content.SharedPreferences;

/**
 * Created by rezkya on 17/05/16.
 */
public class SessionKey {

    private final static String PUBLIC_KEY = "PUBLIC_KEY";
    private final static String PRIVATE_KEY = "PRIVATE_KEY";

    public static void setPreferencesKey(SharedPreferences preferences, String publicKey, String privateKey) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PUBLIC_KEY, publicKey);
        editor.putString(PRIVATE_KEY, privateKey);


        editor.apply();
    }

    public static boolean haveKey(SharedPreferences preferences) {
        boolean key = false;
        String privateKey = SessionKey.getPrivateKey(preferences);
        if (!privateKey.equals("")) {
            key = true;
        }
        return key;
    }

    public static void removePreferencesKey(SharedPreferences preferences) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear().apply();
    }

    public static void setPublicKey(SharedPreferences preferences, long ping) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(PUBLIC_KEY, ping);
        editor.apply();
    }

    public static String getPublicKey(SharedPreferences preferences) {
        return preferences.getString(PUBLIC_KEY, "");
    }

    public static void setPrivateKey(SharedPreferences preferences, long pong) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(PRIVATE_KEY, pong);
        editor.apply();
    }

    public static String getPrivateKey(SharedPreferences preferences) {
        return preferences.getString(PRIVATE_KEY, "");
    }
}
