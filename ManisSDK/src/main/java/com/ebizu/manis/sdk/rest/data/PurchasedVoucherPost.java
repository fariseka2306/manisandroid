package com.ebizu.manis.sdk.rest.data;

/**
 * Created by ebizu on 9/14/17.
 */

public class PurchasedVoucherPost {

    public static final int STATUS_PROCESSING = 5;
    public static final int STATUS_PENDING = 3;
    public static final int STATUS_UNPAID = 2;
    public static final int STATUS_PAID = 1;
    public static final int STATUS_ALL = 0;

    public static final int ORDER_DATE = 1;
    public static final int ORDER_AMOUNT = 2;

    public static class RequestBody {

        public PurchasedVoucherPost.Data data;
        public String session;
        public String module;
        public String action;
        public String token;

        public RequestBody(PurchasedVoucherPost.Data data, String session, String module, String action, String token) {
            this.data = data;
            this.session = session;
            this.module = module;
            this.action = action;
            this.token = token;
        }
    }

    public static class Data {

        private int size;
        private int page;
        private int order;
        private int condition;

        public Data(int size, int page, int order, int condition) {
            this.size = size;
            this.page = page;
            this.order = order;
            this.condition = condition;
        }
    }
}
