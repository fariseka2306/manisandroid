package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by rezkya on 26/05/16.
 */
public class LoginData implements Parcelable {

    public static String GENDER_MALE = "male";
    public static String GENDER_FEMALE = "female";

    @SerializedName("acc_id")
    @Expose
    public int accId;
    @SerializedName("acc_facebook_id")
    @Expose
    public long accFacebookId;
    @SerializedName("acc_facebook_email")
    @Expose
    public String accFacebookEmail;
    @SerializedName("acc_google_id")
    @Expose
    public String accGoogleId;
    @SerializedName("acc_google_email")
    @Expose
    public String accGoogleEmail;
    @SerializedName("acc_screen_name")
    @Expose
    public String accScreenName;
    @SerializedName("acc_cty_id")
    @Expose
    public String accCountry;
    @SerializedName("acc_photo")
    @Expose
    public String accPhoto;
    @SerializedName("acc_last_login")
    @Expose
    public long accLastLogin;
    @SerializedName("acc_device_id")
    @Expose
    public String accDeviceId;
    @SerializedName("acc_status")
    @Expose
    public int accStatus;
    @SerializedName("tmz_name")
    @Expose
    public String tmzName;
    @SerializedName("acc_gender")
    @Expose
    public String accGender;
    @SerializedName("acc_msisdn")
    @Expose
    public String accMsisdn;
    @SerializedName("token")
    @Expose
    public String token;
    @SerializedName("currency")
    @Expose
    public Currency currency;
    @SerializedName("acc_birthdate")
    @Expose
    public long accBirthdate;
    @SerializedName("acc_address")
    @Expose
    public String accAddress;
    @SerializedName("acc_otp_status")
    @Expose
    public long accOtpStatus;
    @SerializedName("first_login")
    @Expose
    public boolean firstLogin;
    @SerializedName("point")
    @Expose
    public long point;
    @SerializedName("acc_created_datetime")
    @Expose
    public long accCreatedDateTime;
    @SerializedName("updateable_email")
    @Expose
    public boolean updateAbleEmail;
    @SerializedName("updateable_phone")
    @Expose
    public boolean updateAblePhone;

    public LoginData() {
    }

    public LoginData(int accId, long accFacebookId, String accFacebookEmail, String accScreenName, String accCountry, String accPhoto, long accLastLogin, String accDeviceId, int accStatus, String tmzName, String accGender, String accMsisdn, String token, Currency currency, long accBirthdate, String accAddress, int accOtpStatus, boolean firstLogin, long point, long accCreatedDateTime, boolean updateAbleEmail, boolean updateAblePhone) {
        this.accId = accId;
        this.accFacebookId = accFacebookId;
        this.accFacebookEmail = accFacebookEmail;
        this.accScreenName = accScreenName;
        this.accCountry = accCountry;
        this.accPhoto = accPhoto;
        this.accLastLogin = accLastLogin;
        this.accDeviceId = accDeviceId;
        this.accStatus = accStatus;
        this.tmzName = tmzName;
        this.accGender = accGender;
        this.accMsisdn = accMsisdn;
        this.token = token;
        this.currency = currency;
        this.accBirthdate = accBirthdate;
        this.accAddress = accAddress;
        this.accOtpStatus = accOtpStatus;
        this.firstLogin = firstLogin;
        this.point = point;
        this.accCreatedDateTime = accCreatedDateTime;
        this.updateAbleEmail = updateAbleEmail;
        this.updateAblePhone = updateAblePhone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.accId);
        dest.writeLong(this.accFacebookId);
        dest.writeString(this.accFacebookEmail);
        dest.writeString(this.accGoogleId);
        dest.writeString(this.accGoogleEmail);
        dest.writeString(this.accScreenName);
        dest.writeString(this.accCountry);
        dest.writeString(this.accPhoto);
        dest.writeLong(this.accLastLogin);
        dest.writeString(this.accDeviceId);
        dest.writeInt(this.accStatus);
        dest.writeString(this.tmzName);
        dest.writeString(this.accGender);
        dest.writeString(this.accMsisdn);
        dest.writeString(this.token);
        dest.writeParcelable(this.currency, flags);
        dest.writeLong(this.accBirthdate);
        dest.writeString(this.accAddress);
        dest.writeLong(this.accOtpStatus);
        dest.writeByte(this.firstLogin ? (byte) 1 : (byte) 0);
        dest.writeLong(this.point);
        dest.writeLong(this.accCreatedDateTime);
        dest.writeByte(this.updateAbleEmail ? (byte) 1 : (byte) 0);
        dest.writeByte(this.updateAblePhone ? (byte) 1 : (byte) 0);
    }

    protected LoginData(Parcel in) {
        this.accId = in.readInt();
        this.accFacebookId = in.readLong();
        this.accFacebookEmail = in.readString();
        this.accGoogleId = in.readString();
        this.accGoogleEmail = in.readString();
        this.accScreenName = in.readString();
        this.accCountry = in.readString();
        this.accPhoto = in.readString();
        this.accLastLogin = in.readLong();
        this.accDeviceId = in.readString();
        this.accStatus = in.readInt();
        this.tmzName = in.readString();
        this.accGender = in.readString();
        this.accMsisdn = in.readString();
        this.token = in.readString();
        this.currency = in.readParcelable(Currency.class.getClassLoader());
        this.accBirthdate = in.readLong();
        this.accAddress = in.readString();
        this.accOtpStatus = in.readLong();
        this.firstLogin = in.readByte() != 0;
        this.point = in.readLong();
        this.accCreatedDateTime = in.readLong();
        this.updateAbleEmail = in.readByte() != 0;
        this.updateAblePhone = in.readByte() != 0;
    }

    public static final Creator<LoginData> CREATOR = new Creator<LoginData>() {
        @Override
        public LoginData createFromParcel(Parcel source) {
            return new LoginData(source);
        }

        @Override
        public LoginData[] newArray(int size) {
            return new LoginData[size];
        }
    };
}
