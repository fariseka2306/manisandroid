package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.LuckyDrawPrizes;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 17/05/16.
 */
public class SnapReceiptUpload {

    public static class RequestBody {
        public Receipt receipt;
        public Store store;

        public RequestBody(Receipt receipt, Store store) {
            this.receipt = receipt;
            this.store = store;
        }

        public static class Receipt {

            public String number;
            public double amount;
            public String date;

            public Receipt(String number, double amount, String date) {
                this.number = number;
                this.amount = amount;
                this.date = date;
            }
        }

        public static class Store {

            public String name;
            public int id;
            public int category;
            public String location;

            public Store(String name, int id, int category, String location) {
                this.name = name;
                this.id = id;
                this.category = category;
                this.location = location;
            }
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, String receiptNumber, double receiptAmount, String receiptDate, String storeName, int storeId, int storeCategory, String location) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(new RequestBody.Receipt(receiptNumber, receiptAmount, receiptDate), new RequestBody.Store(storeName, storeId, storeCategory, location));
        }
    }

    public static class Response {

        private long id;
        @SerializedName("snap_luckydraw_id")
        @Expose
        private long snapLuckyDrawId;
        @SerializedName("is_luckydraw")
        @Expose
        private boolean isLuckyDraw;
        @SerializedName("prizes")
        @Expose
        private LuckyDrawPrizes prizes;

        public Response(long id) {
            this.id = id;
        }

        public Response() {
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getSnapLuckyDrawId() {
            return snapLuckyDrawId;
        }

        public void setSnapLuckyDrawId(long snapLuckyDrawId) {
            this.snapLuckyDrawId = snapLuckyDrawId;
        }

        public boolean isLuckyDraw() {
            return isLuckyDraw;
        }

        public void setLuckyDraw(boolean luckyDraw) {
            isLuckyDraw = luckyDraw;
        }

        public LuckyDrawPrizes getPrizes() {
            return prizes;
        }

        public void setPrizes(LuckyDrawPrizes prizes) {
            this.prizes = prizes;
        }
    }
}
