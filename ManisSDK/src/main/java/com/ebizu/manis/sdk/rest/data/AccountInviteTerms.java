package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 31/05/16.
 */
public class AccountInviteTerms {

    public static class RequestBody {
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response {
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("terms")
        @Expose
        public ArrayList<String> terms;
    }
}
