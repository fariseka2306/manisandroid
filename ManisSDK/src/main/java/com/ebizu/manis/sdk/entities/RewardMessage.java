package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 9/15/17.
 */

public class RewardMessage implements Parcelable {

    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("time")
    @Expose
    private double time;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.errorMessage);
        dest.writeString(this.errorCode);
        dest.writeDouble(this.time);
    }

    public RewardMessage() {
    }

    protected RewardMessage(Parcel in) {
        this.errorMessage = in.readString();
        this.errorCode = in.readString();
        this.time = in.readDouble();
    }

    public static final Creator<RewardMessage> CREATOR = new Creator<RewardMessage>() {
        @Override
        public RewardMessage createFromParcel(Parcel source) {
            return new RewardMessage(source);
        }

        @Override
        public RewardMessage[] newArray(int size) {
            return new RewardMessage[size];
        }
    };
}
