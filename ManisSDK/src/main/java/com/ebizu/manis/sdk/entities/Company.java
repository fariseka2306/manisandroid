package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 30/05/16.
 */
public class Company implements Parcelable {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("category")
    @Expose
    public StoreCategory category;
    @SerializedName("assets")
    @Expose
    public StoreAsset assets;

    public Company(Parcel in) {
        id =  in.readInt();
        name = in.readString();
        category = in.readParcelable(StoreCategory.class.getClassLoader());
        assets = in.readParcelable(StoreAsset.class.getClassLoader());
    }

    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel in) {
            return new Company(in);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeParcelable(category, flags);
        dest.writeParcelable(assets, flags);
    }
}
