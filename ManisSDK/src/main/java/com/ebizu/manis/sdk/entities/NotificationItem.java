package com.ebizu.manis.sdk.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rizky Riadhy on 15-Jun-16.
 */
public class NotificationItem {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("value")
    @Expose
    private boolean value;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }
}
