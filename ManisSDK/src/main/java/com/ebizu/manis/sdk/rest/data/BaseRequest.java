package com.ebizu.manis.sdk.rest.data;

/**
 * Created by rezkya on 26/05/16.
 */
public class BaseRequest<H extends BaseHeaderRequest, B> {
    public H header;
    public B body;
}
