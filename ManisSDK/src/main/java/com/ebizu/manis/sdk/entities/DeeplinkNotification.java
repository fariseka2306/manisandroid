package com.ebizu.manis.sdk.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkyatinnov on 6/29/16.
 */
public class DeeplinkNotification {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("app")
    @Expose
    private String app;
    @SerializedName("silent")
    @Expose
    private boolean silent;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("point")
    @Expose
    private Point point;

    public DeeplinkNotification(int id, String app, boolean silent, String message, String type, String uri, String status, Point point) {
        this.id = id;
        this.app = app;
        this.silent = silent;
        this.message = message;
        this.type = type;
        this.uri = uri;
        this.status = status;
        this.point = point;
    }

    public DeeplinkNotification() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public boolean isSilent() {
        return silent;
    }

    public void setSilent(boolean silent) {
        this.silent = silent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Point {
        @SerializedName("snap")
        @Expose
        private String snap;
        @SerializedName("current")
        @Expose
        private String current;

        public Point(String snap, String current) {
            this.snap = snap;
            this.current = current;
        }

        public Point() {
        }

        public String getSnap() {
            return snap;
        }

        public void setSnap(String snap) {
            this.snap = snap;
        }

        public String getCurrent() {
            return current;
        }

        public void setCurrent(String current) {
            this.current = current;
        }
    }

}
