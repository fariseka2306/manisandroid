package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Params implements Parcelable {
    /**
     * field : ref
     * description : Reference number
     */

    private String field;
    @SerializedName("description")
    private String descriptionX;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getDescriptionX() {
        return descriptionX;
    }

    public void setDescriptionX(String descriptionX) {
        this.descriptionX = descriptionX;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.field);
        dest.writeString(this.descriptionX);
    }

    public Params() {
    }

    protected Params(Parcel in) {
        this.field = in.readString();
        this.descriptionX = in.readString();
    }

    public static final Creator<Params> CREATOR = new Creator<Params>() {
        @Override
        public Params createFromParcel(Parcel source) {
            return new Params(source);
        }

        @Override
        public Params[] newArray(int size) {
            return new Params[size];
        }
    };
}