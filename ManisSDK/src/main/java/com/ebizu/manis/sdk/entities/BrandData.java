package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by RizkyRiadhy on 14/02/17.
 */
public class BrandData implements Parcelable {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("multiplier")
    @Expose
    public int multiplier;

    public BrandData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        image = in.readString();
        multiplier = in.readInt();
    }

    public static final Creator<BrandData> CREATOR = new Creator<BrandData>() {
        @Override
        public BrandData createFromParcel(Parcel in) {
            return new BrandData(in);
        }

        @Override
        public BrandData[] newArray(int size) {
            return new BrandData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeInt(multiplier);
    }
}