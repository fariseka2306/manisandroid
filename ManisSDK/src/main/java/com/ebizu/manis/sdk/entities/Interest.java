package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 24/05/16.
 */


public class Interest implements Parcelable{
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("pinned")
    @Expose
    public boolean pinned;

    public Interest() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeByte(this.pinned ? (byte) 1 : (byte) 0);
    }

    protected Interest(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.pinned = in.readByte() != 0;
    }

    public static final Creator<Interest> CREATOR = new Creator<Interest>() {
        @Override
        public Interest createFromParcel(Parcel source) {
            return new Interest(source);
        }

        @Override
        public Interest[] newArray(int size) {
            return new Interest[size];
        }
    };
}
