package com.ebizu.manis.sdk.rest;

import com.ebizu.manis.sdk.entities.LuckyDrawEntryData;
import com.ebizu.manis.sdk.entities.LuckyDrawScreen;
import com.ebizu.manis.sdk.entities.LuckyDrawSnapable;
import com.ebizu.manis.sdk.entities.LuckyDrawWinnersData;
import com.ebizu.manis.sdk.entities.MyVoucherData;
import com.ebizu.manis.sdk.entities.OrderDetail;
import com.ebizu.manis.sdk.entities.PurchaseStatus;
import com.ebizu.manis.sdk.entities.PurchasedVoucherData;
import com.ebizu.manis.sdk.entities.RewardLoginSession;
import com.ebizu.manis.sdk.entities.RewardMessage;
import com.ebizu.manis.sdk.entities.RewardSession;
import com.ebizu.manis.sdk.entities.VoucherTerm;
import com.ebizu.manis.sdk.rest.data.AccountInviteTerms;
import com.ebizu.manis.sdk.rest.data.AccountPoint;
import com.ebizu.manis.sdk.rest.data.AccountProfileDetail;
import com.ebizu.manis.sdk.rest.data.AccountProfilePictureUpdate;
import com.ebizu.manis.sdk.rest.data.AccountProfileUpdate;
import com.ebizu.manis.sdk.rest.data.AccountSettingsNotifications;
import com.ebizu.manis.sdk.rest.data.AccountSettingsNotificationsUpdate;
import com.ebizu.manis.sdk.rest.data.AuthSignIn;
import com.ebizu.manis.sdk.rest.data.AuthSignInGoogle;
import com.ebizu.manis.sdk.rest.data.AuthSignInPhone;
import com.ebizu.manis.sdk.rest.data.AuthSignOut;
import com.ebizu.manis.sdk.rest.data.AuthSignUpOTP;
import com.ebizu.manis.sdk.rest.data.BeaconInterval;
import com.ebizu.manis.sdk.rest.data.BeaconPromoNearby;
import com.ebizu.manis.sdk.rest.data.BrandFeatured;
import com.ebizu.manis.sdk.rest.data.DeeplinkCashvoucher;
import com.ebizu.manis.sdk.rest.data.DeeplinkOffer;
import com.ebizu.manis.sdk.rest.data.DeeplinkSnap;
import com.ebizu.manis.sdk.rest.data.DeeplinkStore;
import com.ebizu.manis.sdk.rest.data.InterestAdd;
import com.ebizu.manis.sdk.rest.data.InterestDelete;
import com.ebizu.manis.sdk.rest.data.InterestUpdate;
import com.ebizu.manis.sdk.rest.data.Interests;
import com.ebizu.manis.sdk.rest.data.MiscAnnouncement;
import com.ebizu.manis.sdk.rest.data.MiscHelp;
import com.ebizu.manis.sdk.rest.data.MiscHelpLegal;
import com.ebizu.manis.sdk.rest.data.MyVoucherPost;
import com.ebizu.manis.sdk.rest.data.OTPSkip;
import com.ebizu.manis.sdk.rest.data.OTPStatus;
import com.ebizu.manis.sdk.rest.data.PinValidationPost;
import com.ebizu.manis.sdk.rest.data.PurchaseStatusPost;
import com.ebizu.manis.sdk.rest.data.PurchasedVoucherPost;
import com.ebizu.manis.sdk.rest.data.RestResponse;
import com.ebizu.manis.sdk.rest.data.Review;
import com.ebizu.manis.sdk.rest.data.RewardCancelPaymentPost;
import com.ebizu.manis.sdk.rest.data.RewardLoginSessionPost;
import com.ebizu.manis.sdk.rest.data.RewardPaymentPost;
import com.ebizu.manis.sdk.rest.data.RewardSessionPost;
import com.ebizu.manis.sdk.rest.data.RewardsClaimed;
import com.ebizu.manis.sdk.rest.data.RewardsDetail;
import com.ebizu.manis.sdk.rest.data.RewardsPin;
import com.ebizu.manis.sdk.rest.data.RewardsPinRedeem;
import com.ebizu.manis.sdk.rest.data.RewardsRedeem;
import com.ebizu.manis.sdk.rest.data.RewardsRedeemClose;
import com.ebizu.manis.sdk.rest.data.RewardsRedeemCloseFailed;
import com.ebizu.manis.sdk.rest.data.RewardsUnpin;
import com.ebizu.manis.sdk.rest.data.RewardsVoucherRedeem;
import com.ebizu.manis.sdk.rest.data.RewardsVoucherSearch;
import com.ebizu.manis.sdk.rest.data.RewardsVouchers;
import com.ebizu.manis.sdk.rest.data.Saved;
import com.ebizu.manis.sdk.rest.data.SnapAppeal;
import com.ebizu.manis.sdk.rest.data.SnapHistory;
import com.ebizu.manis.sdk.rest.data.SnapLast;
import com.ebizu.manis.sdk.rest.data.SnapReceiptUpload;
import com.ebizu.manis.sdk.rest.data.SnapStatistic;
import com.ebizu.manis.sdk.rest.data.SnapStoresNearby;
import com.ebizu.manis.sdk.rest.data.SnapStoresPinned;
import com.ebizu.manis.sdk.rest.data.SnapStoresRecent;
import com.ebizu.manis.sdk.rest.data.SnapStoresSearch;
import com.ebizu.manis.sdk.rest.data.SnapSuggestion;
import com.ebizu.manis.sdk.rest.data.SnapTerms;
import com.ebizu.manis.sdk.rest.data.StoresDetail;
import com.ebizu.manis.sdk.rest.data.StoresInterest;
import com.ebizu.manis.sdk.rest.data.StoresInterests;
import com.ebizu.manis.sdk.rest.data.StoresNearby;
import com.ebizu.manis.sdk.rest.data.StoresOfferPin;
import com.ebizu.manis.sdk.rest.data.StoresOfferRedeem;
import com.ebizu.manis.sdk.rest.data.StoresOfferUnpin;
import com.ebizu.manis.sdk.rest.data.StoresPin;
import com.ebizu.manis.sdk.rest.data.StoresSearch;
import com.ebizu.manis.sdk.rest.data.StoresUnpin;
import com.ebizu.manis.sdk.rest.data.VoucherTermPost;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by rezkya on 16/05/16.
 */
public interface RestConnect {
    @FormUrlEncoded
    @POST(EndPoint.authSignIn)
    Call<RestResponse<AuthSignIn.Response>> signin(
            @Header("coordinate") String coordinate,
            @Header("pubkey") String pubkey,
            @Header("device") String device,
            @Header("version") String version,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.authSignInPhone)
    Call<RestResponse<AuthSignInPhone.Response>> signinPhone(
            @Header("coordinate") String coordinate,
            @Header("pubkey") String pubkey,
            @Header("device") String device,
            @Header("version") String version,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.authSignInGoogle)
    Call<RestResponse<AuthSignInGoogle.Response>> signinGoogle(
            @Header("coordinate") String coordinate,
            @Header("pubkey") String pubkey,
            @Header("device") String device,
            @Header("version") String version,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.authSignUpOTP)
    Call<RestResponse<AuthSignUpOTP.Response>> signupOTP(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.authSignOut)
    Call<RestResponse<AuthSignOut.Response>> signout(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @GET(EndPoint.beaconInterval)
    Call<RestResponse<BeaconInterval.Response>> beaconInterval(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @FormUrlEncoded
    @POST(EndPoint.interestUpdate)
    Call<RestResponse<InterestUpdate.Response>> interestUpdate(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.interestPinned)
    Call<RestResponse<InterestAdd.Response>> interestAdd(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.interestUnpinned)
    Call<RestResponse<InterestDelete.Response>> interestDelete(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @GET(EndPoint.snapStoresNearby)
    Call<RestResponse<SnapStoresNearby.Response>> snapStoresNearby(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.snapSuggestions)
    Call<RestResponse<SnapSuggestion.Response>> snapSuggestions(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    //    @FormUrlEncoded
    @Multipart
    @POST(EndPoint.snapReceiptUpload)
    Call<RestResponse<SnapReceiptUpload.Response>> snapReceiptUpload(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Part MultipartBody.Part file,
            @Part("data") RequestBody data
    );
//    @Multipart
//    @POST(EndPoint.snapReceiptUpload)
//    Call<RestResponse<SnapReceiptUpload.Response>> snapReceiptUpload(
//            @Header("coordinate") String coordinate,
//            @Header("token") String token,
//            @Header("lang") String lang,
//            @Part("photo") RequestBody file,
//            @Part("data") String data
//    );

    @GET(EndPoint.snapStoresPinned)
    Call<RestResponse<SnapStoresPinned.Response>> snapStoresPinned(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.snapStoresRecent)
    Call<RestResponse<SnapStoresRecent.Response>> snapStoresRecent(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.snapStoresSearch)
    Call<RestResponse<SnapStoresSearch.Response>> snapStoresSearch(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @GET(EndPoint.snapHistory)
    Call<RestResponse<SnapHistory.Response>> snapHistory(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.snapLast)
    Call<RestResponse<SnapLast.Response>> snapLast(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.snapTerms)
    Call<RestResponse<SnapTerms.Response>> snapTerms(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.snapStatistic)
    Call<RestResponse<SnapStatistic.Response>> snapStatistic(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.snapAppeal)
    Call<RestResponse<SnapAppeal.Response>> snapAppeal(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @GET(EndPoint.snapChecksnapable)
    Call<RestResponse<LuckyDrawSnapable>> snapChecksnapable(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.interests)
    Call<RestResponse<Interests.Response>> interests(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @FormUrlEncoded
    @POST(EndPoint.storesPin)
    Call<RestResponse<StoresPin.Response>> storesPin(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.storesUnpin)
    Call<RestResponse<StoresUnpin.Response>> storesUnpin(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @GET(EndPoint.storesNearby)
    Call<RestResponse<StoresNearby.Response>> storesNearby(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.storesInterests)
    Call<RestResponse<StoresInterests.Response>> storesInterests(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.storesInterest)
    Call<RestResponse<StoresInterest.Response>> storesInterest(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.storesDetail)
    Call<RestResponse<StoresDetail.Response>> storesDetail(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.storesSearch)
    Call<RestResponse<StoresSearch.Response>> storesSearch(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.storesOfferPin)
    Call<RestResponse<StoresOfferPin.Response>> storesOfferPin(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.storesOfferUnppin)
    Call<RestResponse<StoresOfferUnpin.Response>> storesOfferUnpin(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.storesOfferRedeem)
    Call<RestResponse<StoresOfferRedeem.Response>> storesOfferRedeem(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @GET(EndPoint.reviewGetStatus)
    Call<RestResponse<Review.Response>> getStatusReview(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @FormUrlEncoded
    @POST(EndPoint.reviewPostPopup)
    Call<RestResponse<Review.Response>> postPopupReview(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @GET(EndPoint.rewardsVouchers)
    Call<RestResponse<RewardsVouchers.Response>> rewardsVouchers(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.rewardsVoucherRedeem)
    Call<RestResponse<RewardsVoucherRedeem.Response>> rewardsVoucherRedeem(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.rewardsPinRedeem)
    Call<RestResponse<RewardsPinRedeem.Response>> rewardsPinRedeem(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.rewardsPin)
    Call<RestResponse<RewardsPin.Response>> rewardsPin(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.rewardsUnpin)
    Call<RestResponse<RewardsUnpin.Response>> rewardsUnpin(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.rewardsRedeem)
    Call<RestResponse<RewardsRedeem.Response>> rewardsRedeem(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.rewardsRedeemClose)
    Call<RestResponse<RewardsRedeemClose.Response>> rewardsRedeemClose(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.rewardsRedeemCloseFailed)
    Call<RestResponse<RewardsRedeemCloseFailed.Response>> rewardsRedeemCloseFailed(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @GET(EndPoint.rewardsClaimed)
    Call<RestResponse<RewardsClaimed.Response>> rewardsClaimed(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.deeplinkRewards)
    Call<RestResponse<RewardsDetail.Response>> rewardsDetail(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @FormUrlEncoded
    @POST(EndPoint.rewardsVoucherSearch)
    Call<RestResponse<RewardsVoucherSearch.Response>> rewardsVoucherSearch(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @GET(EndPoint.saved)
    Call<RestResponse<Saved.Response>> saved(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.accountPoint)
    Call<RestResponse<AccountPoint.Response>> accountPoint(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.accountGetDetail)
    Call<RestResponse<AccountProfileDetail.Response>> accountDetail(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.accountInviteTerms)
    Call<RestResponse<AccountInviteTerms.Response>> accountInviteTerms(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @FormUrlEncoded
    @POST(EndPoint.accountProfileUpdate)
    Call<RestResponse<AccountProfileUpdate.Response>> accountProfileUpdate(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @Multipart
    @POST(EndPoint.accountProfilePictureUpdate)
    Call<RestResponse<AccountProfilePictureUpdate.Response>> accountProfilePictureUpdate(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Part MultipartBody.Part file,
            @Part("data") RequestBody data
    );

    @GET(EndPoint.miscHelpLegal)
    Call<RestResponse<MiscHelpLegal.Response>> miscHelpLegal(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.miscHelp)
    Call<RestResponse<MiscHelp.Response>> miscHelp(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.miscAnnouncement)
    Call<RestResponse<MiscAnnouncement.Response>> miscAnnouncement(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.accountSettingsNotifications)
    Call<RestResponse<AccountSettingsNotifications.Response>> accountSettingsNotification(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @FormUrlEncoded
    @POST(EndPoint.accountSettingsNotificationsUpdate)
    Call<RestResponse<AccountSettingsNotificationsUpdate.Response>> accountSettingsNotificationUpdate(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Field("data") String data
    );

    @GET(EndPoint.beaconPromoNearby)
    Call<RestResponse<BeaconPromoNearby.Response>> beaconPromoNearby(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.deeplinkOffer)
    Call<RestResponse<DeeplinkOffer.Response>> deeplinkOffer(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.deeplinkSnap)
    Call<RestResponse<DeeplinkSnap.Response>> deeplinkSnap(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.deeplinkStore)
    Call<RestResponse<DeeplinkStore.Response>> deeplinkStore(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.deeplinkCashvoucher)
    Call<RestResponse<DeeplinkCashvoucher.Response>> deeplinkCashvoucher(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.otpStatus)
    Call<RestResponse<OTPStatus.Response>> otpStatus(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.otpSkip)
    Call<RestResponse<OTPSkip.Response>> otpSkip(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.brandFeatured)
    Call<RestResponse<BrandFeatured.Response>> brandFeatured(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.luckyDrawScreen)
    Call<RestResponse<LuckyDrawScreen>> luckyDrawScreen(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @GET(EndPoint.luckyDrawEntries)
    Call<RestResponse<LuckyDrawEntryData>> luckyDrawEntries(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.luckyDrawWinners)
    Call<RestResponse<LuckyDrawWinnersData>> luckyDrawWinners(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Query("data") String data
    );

    @GET(EndPoint.luckyDrawTerms)
    Call<RestResponse<SnapTerms.Response>> luckyDrawTerms(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang
    );

    @Multipart
    @POST(EndPoint.luckyDrawUpload)
    Call<RestResponse<SnapReceiptUpload.Response>> luckyDrawUpload(
            @Header("coordinate") String coordinate,
            @Header("token") String token,
            @Header("lang") String lang,
            @Part MultipartBody.Part file,
            @Part("data") RequestBody data
    );

    @POST(EndPoint.rewardApi)
    Call<RewardRestResponse<OrderDetail>> rewardPaymentPost(
            @Body RewardPaymentPost.RequestBody requestBody);

    @POST(EndPoint.rewardApi)
    Call<RewardRestResponse<VoucherTerm>> rewardPinPost(
            @Body VoucherTermPost.RequestBody requestBody);

    @POST(EndPoint.rewardApi)
    Call<RewardRestResponse<RewardSession>> rewardSessionPost(
            @Body RewardSessionPost.RequestBody requestBody);

    @POST(EndPoint.rewardApi)
    Call<RewardRestResponse<PurchasedVoucherData>> rewardPurchasedHistory(
            @Body PurchasedVoucherPost.RequestBody requestBody
    );

    @POST(EndPoint.rewardApi)
    Call<RewardRestResponse<MyVoucherData>> rewardMyVoucher(
            @Body MyVoucherPost.RequestBody requestBody
    );

    @POST(EndPoint.rewardApi)
    Call<RewardRestResponse<PurchaseStatus>> purchaseStatusPost(
            @Body PurchaseStatusPost.RequestBody requestBody);

    @POST(EndPoint.rewardApi)
    Call<RewardRestResponse<RewardLoginSession>> rewardLoginSessionPost(
            @Body RewardLoginSessionPost.RequestBody requestBody);

    @POST(EndPoint.rewardApi)
    Call<RewardRestResponse<RewardMessage>> pinValidationPost(
            @Body PinValidationPost.RequestBody requestBody);

    @POST(EndPoint.rewardApi)
    Call<RewardRestResponse<RewardMessage>> rewardCancelPaymentPost(
            @Body RewardCancelPaymentPost.RequestBody requestBody);
}