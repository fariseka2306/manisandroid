package com.ebizu.manis.sdk.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ebizu.manis.sdk.shared.ManisApp;


/**
 * Created by rezkya on 24/05/16.
 */
public class ManisSdkDb extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "manissdk.db";
    public static int DATABASE_VERSION = 2;

    private static ManisSdkDb instance = null;

    private ManisSdkDb() {
        super(ManisApp.context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Lazy Initialization (If required then only)
    public static ManisSdkDb getInstance() {
        if (instance == null) {
            // Thread Safe. Might be costly operation in some case
            synchronized (ManisSdkDb.class) {
                if (instance == null) {
                    instance = new ManisSdkDb();
                }
            }
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(LocalData.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.v(ManisSdkDb.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");

        db.execSQL("DROP TABLE IF EXISTS " + LocalData.TABLE_NAME);
        onCreate(db);
    }
}
