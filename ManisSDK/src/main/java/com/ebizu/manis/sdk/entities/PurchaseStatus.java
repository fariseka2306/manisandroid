package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 9/15/17.
 */

public class PurchaseStatus implements Parcelable {

    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("purchaseId")
    @Expose
    private String purchaseId;
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.statusMessage);
        dest.writeString(this.purchaseId);
        dest.writeString(this.voucherCode);
    }

    public PurchaseStatus() {
    }

    protected PurchaseStatus(Parcel in) {
        this.statusMessage = in.readString();
        this.purchaseId = in.readString();
        this.voucherCode = in.readString();
    }

    public static final Creator<PurchaseStatus> CREATOR = new Creator<PurchaseStatus>() {
        @Override
        public PurchaseStatus createFromParcel(Parcel source) {
            return new PurchaseStatus(source);
        }

        @Override
        public PurchaseStatus[] newArray(int size) {
            return new PurchaseStatus[size];
        }
    };
}
