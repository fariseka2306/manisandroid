package com.ebizu.manis.sdk.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by rezkyatinnov on 6/3/16.
 */
public class ImageUtil {
    private static final int FILESIZELIMIT = 700000;
    private static final int QUALITYPOINTLIMIT = 40;

    public static String compressImage(File file) {
        String filename = "";
        try {
            filename = file.getAbsolutePath();
            Bitmap bmp = BitmapFactory.decodeFile(filename);
            FileOutputStream out = new FileOutputStream(filename);
            long imageSize;
            int qualityPoint = 100;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            do {
                byteArrayOutputStream.reset();
                bmp.compress(Bitmap.CompressFormat.JPEG, qualityPoint, byteArrayOutputStream);
                imageSize = byteArrayOutputStream.size();
                qualityPoint -= 10;
            } while (imageSize >= FILESIZELIMIT && qualityPoint >= QUALITYPOINTLIMIT);
            byteArrayOutputStream.writeTo(out);
        } catch (OutOfMemoryError om) {
            Log.e("ImageUtil", "Out of Memory Error");
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
        return filename;
    }
}