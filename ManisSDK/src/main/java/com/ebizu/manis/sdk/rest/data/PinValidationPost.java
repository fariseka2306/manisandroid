package com.ebizu.manis.sdk.rest.data;

/**
 * Created by halim_ebizu on 9/15/17.
 */

public class PinValidationPost {

    public static class RequestBody {
        public Data data;
        public String session;
        public String module;
        public String action;
        public String token;

        public RequestBody(Data data, String session, String module, String action, String token) {
            this.data = data;
            this.session = session;
            this.module = module;
            this.action = action;
            this.token = token;
        }

        public static class Data {
            private String purchaseId;
            private String transactionId;
            private String pin;
            private String voucherCode;

            public Data(String purchaseId, String transactionId, String pin, String voucherCode) {
                this.purchaseId = purchaseId;
                this.transactionId = transactionId;
                this.pin = pin;
                this.voucherCode = voucherCode;
            }
        }

    }
}
