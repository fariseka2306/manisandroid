package com.ebizu.manis.sdk.rest.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by deni on 06/01/17.
 */
public class RewardsRedeem {

    public static class RequestBody {
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("point")
        @Expose
        public long point;
        @SerializedName("voucher_type")
        @Expose
        public String voucherType;

        public RequestBody(String id, long point, String voucherType) {
            this.id = id;
            this.point = point;
            this.voucherType = voucherType;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, String id, long point, String voucherType) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(id, point, voucherType);
        }
    }

    public static class Response implements Parcelable {
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("point")
        @Expose
        public long point;
        @SerializedName("refcode")
        @Expose
        public String refcode;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeLong(this.point);
            dest.writeString(this.refcode);
        }

        public Response() {
        }

        protected Response(Parcel in) {
            this.id = in.readString();
            this.point = in.readLong();
            this.refcode = in.readString();
        }

        public static final Creator<Response> CREATOR = new Creator<Response>() {
            @Override
            public Response createFromParcel(Parcel source) {
                return new Response(source);
            }

            @Override
            public Response[] newArray(int size) {
                return new Response[size];
            }
        };
    }
}
