package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.SnapData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 27/05/16.
 */
public class SnapHistory {

    public static class RequestBody {

        @SerializedName("filter")
        @Expose
        public int filter;
        @SerializedName("order")
        @Expose
        public int order;
        @SerializedName("sort")
        @Expose
        public int sort;
        @SerializedName("page")
        @Expose
        public int page;
        @SerializedName("size")
        @Expose
        public int size;

        public RequestBody(int filter, int order, int sort, int page, int size) {
            this.filter = filter;
            this.order = order;
            this.sort = sort;
            this.page = page;
            this.size = size;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude, int filter, int order, int sort, int page, int size) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody(filter, order, sort, page, size);
        }
    }

    public static class Response {
        @SerializedName("more")
        @Expose
        public boolean more;
        @SerializedName("results")
        @Expose
        public ArrayList<SnapData> results = new ArrayList<>();
    }
}
