package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.StoreData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rizkyriadhy on 17/05/16.
 */
public class SnapSuggestion {

    public static class RequestBody {
        public int page;
        public int size;

        public RequestBody(int page, int size) {
            this.page = page;
            this.size = size;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude, int page, int size) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody(page,size);
        }
    }

    public static class Response {
        @SerializedName("more")
        @Expose
        public boolean more;
        @SerializedName("suggestions")
        @Expose
        public ArrayList<StoreData> suggestions = new ArrayList<>();
        @SerializedName("recent")
        @Expose
        public ArrayList<StoreData> recent = new ArrayList<>();
    }
}
