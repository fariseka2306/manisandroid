package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Deni Rohimat on 07/06/17.
 */
public class BrandResponse implements Parcelable {

    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("brands")
    @Expose
    public List<BrandData> brands;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<BrandData> getBrands() {
        return brands;
    }

    public void setBrands(List<BrandData> brands) {
        this.brands = brands;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeTypedList(this.brands);
    }

    public BrandResponse() {
    }

    protected BrandResponse(Parcel in) {
        this.title = in.readString();
        this.brands = in.createTypedArrayList(BrandData.CREATOR);
    }

    public static final Creator<BrandResponse> CREATOR = new Creator<BrandResponse>() {
        @Override
        public BrandResponse createFromParcel(Parcel source) {
            return new BrandResponse(source);
        }

        @Override
        public BrandResponse[] newArray(int size) {
            return new BrandResponse[size];
        }
    };
}