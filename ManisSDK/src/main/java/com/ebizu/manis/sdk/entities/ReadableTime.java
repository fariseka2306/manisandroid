package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class ReadableTime implements Parcelable{

    @SerializedName("start")
    @Expose
    public String start;
    @SerializedName("end")
    @Expose
    public String end;

    public ReadableTime() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.start);
        dest.writeString(this.end);
    }

    protected ReadableTime(Parcel in) {
        this.start = in.readString();
        this.end = in.readString();
    }

    public static final Creator<ReadableTime> CREATOR = new Creator<ReadableTime>() {
        @Override
        public ReadableTime createFromParcel(Parcel source) {
            return new ReadableTime(source);
        }

        @Override
        public ReadableTime[] newArray(int size) {
            return new ReadableTime[size];
        }
    };
}
