package com.ebizu.manis.sdk.rest;

import android.content.Intent;

import com.ebizu.manis.sdk.R;
import com.ebizu.manis.sdk.rest.data.RestResponse;
import com.ebizu.manis.sdk.shared.ManisApp;
import com.ebizu.manis.sdk.utils.ManisConst;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by rezkya on 16/05/16.
 */
public abstract class Callback<T extends RestResponse> implements retrofit2.Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            if(response.body() != null) {
                if (response.body().isSuccess()) {
                    onSuccess(response.code(), response.body());
                } else {
                    onFailed(response.code(), response.body().getMessage());
                }
            }else{
                onFailed(response.code(), ManisApp.context.getString(R.string.error_no_data_received));
            }
        } else {
            Gson gson = new Gson();
            try {
                RestResponse body = gson.fromJson(response.errorBody().string(),RestResponse.class);
                if(body != null) {
                    if(response.code()==446){
                        Intent intent=new Intent();
                        intent.setAction(ManisConst.TOKEN_EXPIRED_INTENT_ACTION);
                        intent.putExtra(ManisConst.TOKEN_EXPIRED_INTENT_NAME,body.getMessage());
                        ManisApp.context.sendBroadcast(intent);
                    }else{
                        onFailed(response.code(), body.getMessage());
                    }
                }else{
                    onFailed(response.code(), ManisApp.context.getString(R.string.error_something));
                }
            } catch (Exception e) {
                onFailed(response.code(), ManisApp.context.getString(R.string.error_failed_data));
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onFailed(600, ManisApp.context.getString(R.string.error_no_connection));
    }

    public abstract void onSuccess(int code, T body);

    public abstract void onFailed(int code, String message);
}
