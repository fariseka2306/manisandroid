package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 30/05/16.
 */
public class Validity implements Parcelable {

    @SerializedName("start")
    @Expose
    public long start;
    @SerializedName("end")
    @Expose
    public long end;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.start);
        dest.writeLong(this.end);
    }

    public Validity() {
    }

    protected Validity(Parcel in) {
        this.start = in.readLong();
        this.end = in.readLong();
    }

    public static final Creator<Validity> CREATOR = new Creator<Validity>() {
        @Override
        public Validity createFromParcel(Parcel source) {
            return new Validity(source);
        }

        @Override
        public Validity[] newArray(int size) {
            return new Validity[size];
        }
    };
}
