package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by deni rohimat on 01/12/16.
 */
public class BeaconIntervalData implements Parcelable{

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("interval")
    @Expose
    private int interval;

    public BeaconIntervalData(String type, int interval) {
        this.type = type;
        this.interval = interval;
    }

    public String getType() {
        return type;
    }

    public int getInterval() {
        return interval;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeInt(this.interval);
    }

    public BeaconIntervalData() {
    }

    protected BeaconIntervalData(Parcel in) {
        this.type = in.readString();
        this.interval = in.readInt();
    }

    public static final Creator<BeaconIntervalData> CREATOR = new Creator<BeaconIntervalData>() {
        @Override
        public BeaconIntervalData createFromParcel(Parcel source) {
            return new BeaconIntervalData(source);
        }

        @Override
        public BeaconIntervalData[] newArray(int size) {
            return new BeaconIntervalData[size];
        }
    };
}
