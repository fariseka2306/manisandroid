package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.Interest;

import java.util.ArrayList;

/**
 * Created by rezkya on 24/05/16.
 */
public class InterestList {
    public static class RequestBody{

    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody>{

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response extends ArrayList<Interest> {
    }
}
