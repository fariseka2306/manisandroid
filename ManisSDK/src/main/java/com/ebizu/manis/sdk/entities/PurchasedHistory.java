package com.ebizu.manis.sdk.entities;

import com.ebizu.sdk.reward.models.Brand;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/14/17.
 */

public class PurchasedHistory {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("voucherName")
    @Expose
    private String voucherName;
    @SerializedName("brand")
    @Expose
    private Brand brand;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("paymentTitle")
    @Expose
    private String paymentTitle;
    @SerializedName("paymentSubtitle")
    @Expose
    private String paymentSubtitle;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("paidTitle")
    @Expose
    private String paidTitle;
    @SerializedName("transactionTime")
    @Expose
    private String transactionTime;
    @SerializedName("voucherIcon")
    @Expose
    private String voucherIcon;
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;
    @SerializedName("paymentStatus")
    @Expose
    private String status;
    @SerializedName("customer")
    @Expose
    private Customer customer;

    public String getOrderId() {
        return orderId;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public Brand getBrand() {
        return brand;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public String getPaymentTitle() {
        return paymentTitle;
    }

    public String getPaymentSubtitle() {
        return paymentSubtitle;
    }

    public String getCurrency() {
        return currency;
    }

    public String getAmount() {
        return amount;
    }

    public String getPaidTitle() {
        return paidTitle;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public String getVoucherIcon() {
        return voucherIcon;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public String getStatus() {
        return status;
    }

    public Customer getCustomer() {
        return customer;
    }
}
