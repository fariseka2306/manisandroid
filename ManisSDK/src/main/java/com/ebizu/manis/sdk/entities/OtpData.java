package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by Rizky Riadhy on 06/12/16.
 */
public class OtpData implements Parcelable {
    @SerializedName("acc_id")
    @Expose
    public int accId;
    @SerializedName("phone_no")
    @Expose
    public String phoneNo;
    @SerializedName("country_name")
    @Expose
    public String countryName;
    @SerializedName("country_code")
    @Expose
    public String countryCode;

    public OtpData() {
    }

    public OtpData(int accId, String phoneNo, String countryName, String countryCode) {
        this.accId = accId;
        this.phoneNo = phoneNo;
        this.countryName = countryName;
        this.countryCode = countryCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.accId);
        dest.writeString(this.phoneNo);
        dest.writeString(this.countryName);
        dest.writeString(this.countryCode);
    }

    protected OtpData(Parcel in) {
        this.accId = in.readInt();
        this.phoneNo = in.readString();
        this.countryName = in.readString();
        this.countryCode = in.readString();
    }

    public static final Creator<OtpData> CREATOR = new Creator<OtpData>() {
        @Override
        public OtpData createFromParcel(Parcel source) {
            return new OtpData(source);
        }

        @Override
        public OtpData[] newArray(int size) {
            return new OtpData[size];
        }
    };
}
