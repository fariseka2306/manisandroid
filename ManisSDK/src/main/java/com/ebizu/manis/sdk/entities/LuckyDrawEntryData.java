package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Deni Rohimat on 24/07/17.
 */
public class LuckyDrawEntryData implements Parcelable {

    @SerializedName("more") private boolean more;
    @SerializedName("entries_title") private String entriesTitle;
    @SerializedName("total_tickets") private int totalTickets;
    @SerializedName("tickets") private List<LuckyDrawTickets> tickets;

    public boolean isMore() {
        return more;
    }

    public void setMore(boolean more) {
        this.more = more;
    }

    public String getEntriesTitle() {
        return entriesTitle;
    }

    public void setEntriesTitle(String entriesTitle) {
        this.entriesTitle = entriesTitle;
    }

    public int getTotalTickets() {
        return totalTickets;
    }

    public void setTotalTickets(int totalTickets) {
        this.totalTickets = totalTickets;
    }

    public List<LuckyDrawTickets> getTickets() {
        return tickets;
    }

    public void setTickets(List<LuckyDrawTickets> tickets) {
        this.tickets = tickets;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.more ? (byte) 1 : (byte) 0);
        dest.writeString(this.entriesTitle);
        dest.writeInt(this.totalTickets);
        dest.writeTypedList(this.tickets);
    }

    public LuckyDrawEntryData() {
    }

    protected LuckyDrawEntryData(Parcel in) {
        this.more = in.readByte() != 0;
        this.entriesTitle = in.readString();
        this.totalTickets = in.readInt();
        this.tickets = in.createTypedArrayList(LuckyDrawTickets.CREATOR);
    }

    public static final Creator<LuckyDrawEntryData> CREATOR = new Creator<LuckyDrawEntryData>() {
        @Override
        public LuckyDrawEntryData createFromParcel(Parcel source) {
            return new LuckyDrawEntryData(source);
        }

        @Override
        public LuckyDrawEntryData[] newArray(int size) {
            return new LuckyDrawEntryData[size];
        }
    };
}
