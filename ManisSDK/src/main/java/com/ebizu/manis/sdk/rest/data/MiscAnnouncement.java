package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.Announcement;
import com.ebizu.manis.sdk.entities.StoreAsset;

/**
 * Created by rezkya on 09/06/16.
 */
public class MiscAnnouncement {
    public static class RequestBody{

    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody>{

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody();
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends Announcement {
        public Response(Parcel in) {
            super(in);
        }

        public Response(String title, String content, String label, String uri, StoreAsset assets) {
            super(title, content, label, uri, assets);
        }
    }
}
