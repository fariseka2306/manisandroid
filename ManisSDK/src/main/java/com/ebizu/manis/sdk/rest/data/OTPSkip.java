package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;

/**
 * Created by Rizky Riadhy on 14/12/16.
 */
public class OTPSkip {

    public static class RequestBody {

        public RequestBody() {
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest,RequestBody> {

        public Request(double latitude, double longitude) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
            this.body = new RequestBody();
        }
    }

    public static class Response {
    }
}
