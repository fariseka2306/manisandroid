package com.ebizu.manis.sdk.entities;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

/**
 * Created by rezkyatinnov on 6/29/16.
 */
public class TimestampedData {

    public enum Type {
        BEACON_PROMO, NOTIFICATION_SILENT
    }

    @SerializedName("timestamp")
    @Expose
    private long timestamp;
    @SerializedName("jsonData")
    @Expose
    private String jsonData;
    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("isRead")
    @Expose
    private boolean isRead;
    @SerializedName("isSeen")
    @Expose
    private boolean isSeen;
    @SerializedName("isDeleted")
    @Expose
    private boolean isDeleted;

    public <D> TimestampedData(long timestamp, D data, Type type, boolean isRead) {
        Gson gson = new Gson();
        this.timestamp = timestamp;
        this.jsonData = gson.toJson(data, data.getClass());
        this.type = type;
        this.isRead = isRead;
    }

    public TimestampedData() {
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isSeen() {
        return isSeen;
    }

    public void setSeen(boolean seen) {
        isSeen = seen;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public <D> D getData() {
        D result = null;
        Gson gson = new Gson();
        try {
            if (type == Type.BEACON_PROMO) {
                result = gson.fromJson(jsonData, new TypeToken<BeaconPromo>() {
                }.getType());
            } else if (type == Type.NOTIFICATION_SILENT) {
                result = gson.fromJson(jsonData, new TypeToken<DeeplinkNotification>() {
                }.getType());
            }
        } catch (IncompatibleClassChangeError ie) {
            if (type == Type.BEACON_PROMO) {
                result = (D) new BeaconPromo();
            } else if (type == Type.NOTIFICATION_SILENT) {
                result = (D) new DeeplinkNotification();
            }
            ie.printStackTrace();
        } catch (Exception e) {
            if (type == Type.BEACON_PROMO) {
                result = (D) new BeaconPromo();
            } else if (type == Type.NOTIFICATION_SILENT) {
                result = (D) new DeeplinkNotification();
            }
            e.printStackTrace();
        }
        return result;
    }

    public <D> void setData(D data) {
        Gson gson = new Gson();
        this.jsonData = gson.toJson(data, data.getClass());
    }
}
