package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 30/05/16.
 */
public class Reward implements Parcelable{
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("valid")
    @Expose
    public Validity valid;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("stock")
    @Expose
    public int stock;
    @SerializedName("company")
    @Expose
    public Company company;
    @SerializedName("saved")
    @Expose
    public boolean saved;
    @SerializedName("redeemed")
    @Expose
    public long redeemed;
    @SerializedName("tos")
    @Expose
    public String[] tos = new String[] {};

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeParcelable(this.valid, flags);
        dest.writeString(this.image);
        dest.writeInt(this.stock);
        dest.writeParcelable(this.company, flags);
        dest.writeByte(this.saved ? (byte) 1 : (byte) 0);
        dest.writeLong(this.redeemed);
        dest.writeStringArray(this.tos);
    }

    public Reward() {
    }

    protected Reward(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.valid = in.readParcelable(Validity.class.getClassLoader());
        this.image = in.readString();
        this.stock = in.readInt();
        this.company = in.readParcelable(Company.class.getClassLoader());
        this.saved = in.readByte() != 0;
        this.redeemed = in.readLong();
        this.tos = in.createStringArray();
    }

    public static final Creator<Reward> CREATOR = new Creator<Reward>() {
        @Override
        public Reward createFromParcel(Parcel source) {
            return new Reward(source);
        }

        @Override
        public Reward[] newArray(int size) {
            return new Reward[size];
        }
    };
}
