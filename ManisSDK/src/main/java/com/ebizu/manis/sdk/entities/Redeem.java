package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkyatinnov on 6/5/16.
 */
public class Redeem  implements Parcelable {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("sn")
    @Expose
    private String sn;
    @SerializedName("expired")
    @Expose
    private long expired;

    protected Redeem(Parcel in) {
        type = in.readString();
        code = in.readString();
        sn = in.readString();
        expired = in.readLong();
    }

    public static final Creator<Redeem> CREATOR = new Creator<Redeem>() {
        @Override
        public Redeem createFromParcel(Parcel in) {
            return new Redeem(in);
        }

        @Override
        public Redeem[] newArray(int size) {
            return new Redeem[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public long getExpired() {
        return expired;
    }

    public void setExpired(long expired) {
        this.expired = expired;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(code);
        dest.writeString(sn);
        dest.writeLong(expired);
    }
}
