package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.LuckyDrawScreen;
import com.google.gson.annotations.SerializedName;

/**
 * Created by novyandi-ebizu on 7/21/17.
 */
public class LuckyDrawScreenRest {

    public static class RequestBody {
        @SerializedName("com_id")
        int comId;
        @SerializedName("merchant_tier")
        String merchantTier;

        public RequestBody(int comId, String merchantTier) {
            this.comId = comId;
            this.merchantTier = merchantTier;
        }
    }

    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, int comId, String merchantTier) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(comId, merchantTier);
        }
    }

    public static class Response extends LuckyDrawScreen {
    }
}