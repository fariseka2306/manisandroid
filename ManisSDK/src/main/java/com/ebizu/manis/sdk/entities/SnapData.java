package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rezkya on 27/05/16.
 */
public class SnapData implements Parcelable {

    public static final int STATUS_PENDING = 0;
    public static final int STATUS_APPROVED = 1;
    public static final int STATUS_REJECTED = 2;
    public static final int STATUS_PENDING_APPEAL = 3;
    public static final int STATUS_ALL = 4;
    public static final int ORDER_DATE = 0;
    public static final int ORDER_AMOUNT = 1;
    public static final int SORT_ASC = 0;
    public static final int SORT_DESC = 1;

    @SerializedName("id")
    @Expose
    public long id;
    @SerializedName("appealable")
    @Expose
    public boolean appealable;
    @SerializedName("receipt")
    @Expose
    public Receipt receipt;
    @SerializedName("store")
    @Expose
    public StoreData store;
    @SerializedName("point")
    @Expose
    public long point;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("remark")
    @Expose
    public String remark;
    @SerializedName("uploaded")
    @Expose
    public long uploaded;
    @SerializedName("status_type")
    @Expose
    public int statusType;

    public SnapData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeByte(this.appealable ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.receipt, flags);
        dest.writeParcelable(this.store, flags);
        dest.writeLong(this.point);
        dest.writeString(this.status);
        dest.writeString(this.remark);
        dest.writeLong(this.uploaded);
        dest.writeInt(this.statusType);
    }

    protected SnapData(Parcel in) {
        this.id = in.readLong();
        this.appealable = in.readByte() != 0;
        this.receipt = in.readParcelable(Receipt.class.getClassLoader());
        this.store = in.readParcelable(StoreData.class.getClassLoader());
        this.point = in.readLong();
        this.status = in.readString();
        this.remark = in.readString();
        this.uploaded = in.readLong();
        this.statusType = in.readInt();
    }

    public static final Creator<SnapData> CREATOR = new Creator<SnapData>() {
        @Override
        public SnapData createFromParcel(Parcel source) {
            return new SnapData(source);
        }

        @Override
        public SnapData[] newArray(int size) {
            return new SnapData[size];
        }
    };
}