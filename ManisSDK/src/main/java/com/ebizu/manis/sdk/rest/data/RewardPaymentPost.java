package com.ebizu.manis.sdk.rest.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created by halim_ebizu on 8/29/17.
 */

public class RewardPaymentPost {


    public static class RequestBody {

        public Data data;
        public String session;
        public String module;
        public String action;
        public String token;

        public RequestBody(Data data, String session, String module, String action, String token) {
            this.data = data;
            this.session = session;
            this.module = module;
            this.action = action;
            this.token = token;
        }

        public static class Data {

            public Data(String voucherId, Integer qty) {
                this.voucherId = voucherId;
                this.qty = qty;
            }

            private String voucherId;
            private Integer qty;
        }
    }


    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, String session, String module, String action, String token,
                       String voucherId, Integer qty) {
            RequestBody.Data requestPostData = new RequestBody.Data(voucherId, qty);
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(requestPostData, session, module, action, token);
        }
    }

}
