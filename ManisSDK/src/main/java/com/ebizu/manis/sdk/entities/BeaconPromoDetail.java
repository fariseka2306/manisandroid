package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rezkyatinnov on 6/23/16.
 */
public class BeaconPromoDetail implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("company_id")
    @Expose
    private int companyId;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("valid_start")
    @Expose
    private long validStart;
    @SerializedName("valid_end")
    @Expose
    private long validEnd;
    @SerializedName("expired")
    @Expose
    private long expired;
    @SerializedName("rating")
    @Expose
    private int rating;
    @SerializedName("limit_one_per_customer")
    @Expose
    private String limitOnePerCustomer;
    @SerializedName("allow_redeem")
    @Expose
    private String allowRedeem;
    @SerializedName("share_url")
    @Expose
    private String shareUrl;
    @SerializedName("tos")
    @Expose
    private List<String> tos = new ArrayList<>();

    public BeaconPromoDetail(int id, String name, String description, int companyId, String company, String category, String picture, long validStart, long validEnd, long expired, int rating, String limitOnePerCustomer, String allowRedeem, String shareUrl, List<String> tos) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.companyId = companyId;
        this.company = company;
        this.category = category;
        this.picture = picture;
        this.validStart = validStart;
        this.validEnd = validEnd;
        this.expired = expired;
        this.rating = rating;
        this.limitOnePerCustomer = limitOnePerCustomer;
        this.allowRedeem = allowRedeem;
        this.shareUrl = shareUrl;
        this.tos = tos;
    }

    public BeaconPromoDetail() {
    }

    public BeaconPromoDetail(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        companyId = in.readInt();
        company = in.readString();
        category = in.readString();
        picture = in.readString();
        validStart = in.readLong();
        validEnd = in.readLong();
        expired = in.readLong();
        rating = in.readInt();
        limitOnePerCustomer = in.readString();
        allowRedeem = in.readString();
        shareUrl = in.readString();
        tos = in.createStringArrayList();
    }

    public static final Creator<BeaconPromoDetail> CREATOR = new Creator<BeaconPromoDetail>() {
        @Override
        public BeaconPromoDetail createFromParcel(Parcel in) {
            return new BeaconPromoDetail(in);
        }

        @Override
        public BeaconPromoDetail[] newArray(int size) {
            return new BeaconPromoDetail[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public long getValidStart() {
        return validStart;
    }

    public void setValidStart(long validStart) {
        this.validStart = validStart;
    }

    public long getValidEnd() {
        return validEnd;
    }

    public void setValidEnd(long validEnd) {
        this.validEnd = validEnd;
    }

    public long getExpired() {
        return expired;
    }

    public void setExpired(long expired) {
        this.expired = expired;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getLimitOnePerCustomer() {
        return limitOnePerCustomer;
    }

    public void setLimitOnePerCustomer(String limitOnePerCustomer) {
        this.limitOnePerCustomer = limitOnePerCustomer;
    }

    public String getAllowRedeem() {
        return allowRedeem;
    }

    public void setAllowRedeem(String allowRedeem) {
        this.allowRedeem = allowRedeem;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public List<String> getTos() {
        return tos;
    }

    public void setTos(List<String> tos) {
        this.tos = tos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeInt(companyId);
        dest.writeString(company);
        dest.writeString(category);
        dest.writeString(picture);
        dest.writeLong(validStart);
        dest.writeLong(validEnd);
        dest.writeLong(expired);
        dest.writeInt(rating);
        dest.writeString(limitOnePerCustomer);
        dest.writeString(allowRedeem);
        dest.writeString(shareUrl);
        dest.writeStringList(tos);
    }
}
