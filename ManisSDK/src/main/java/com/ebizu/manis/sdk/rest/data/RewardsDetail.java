package com.ebizu.manis.sdk.rest.data;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.ebizu.manis.sdk.entities.Coordinate;
import com.ebizu.manis.sdk.entities.RewardDetail;

/**
 * Created by rezkya on 27/05/16.
 */
public class RewardsDetail {

    public static class RequestBody {
        public String id;

        public RequestBody(String id) {
            this.id = id;
        }
    }
    public static class Request extends BaseRequest<BaseHeaderRequest, RequestBody> {

        public Request(double latitude, double longitude, String id) {
            this.header = new BaseHeaderRequest(new Coordinate(latitude, longitude));
            this.body = new RequestBody(id);
        }
    }

    @SuppressLint("ParcelCreator")
    public static class Response extends RewardDetail {
        public Response() {
        }

        public Response(Parcel in) {
            super(in);
        }
    }
}
