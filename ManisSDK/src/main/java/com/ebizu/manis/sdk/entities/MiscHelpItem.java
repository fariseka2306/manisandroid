package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rezkya on 09/06/16.
 */
public class MiscHelpItem implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("children")
    @Expose
    private ArrayList<HelpChildItem> children;

    public MiscHelpItem(int id, String title, ArrayList<HelpChildItem> children) {
        this.id = id;
        this.title = title;
        this.children = children;
    }

    public MiscHelpItem() {
    }

    public MiscHelpItem(Parcel in) {
        id = in.readInt();
        title = in.readString();
        children = in.createTypedArrayList(HelpChildItem.CREATOR);
    }

    public static final Creator<MiscHelpItem> CREATOR = new Creator<MiscHelpItem>() {
        @Override
        public MiscHelpItem createFromParcel(Parcel in) {
            return new MiscHelpItem(in);
        }

        @Override
        public MiscHelpItem[] newArray(int size) {
            return new MiscHelpItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeTypedList(children);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<HelpChildItem> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<HelpChildItem> children) {
        this.children = children;
    }

    public static Creator<MiscHelpItem> getCREATOR() {
        return CREATOR;
    }
}
