package com.ebizu.manis.sdk;

import android.content.Context;

import com.ebizu.manis.sdk.entities.LoginData;
import com.ebizu.manis.sdk.entities.LuckyDrawEntryData;
import com.ebizu.manis.sdk.entities.LuckyDrawScreen;
import com.ebizu.manis.sdk.entities.LuckyDrawSnapable;
import com.ebizu.manis.sdk.entities.LuckyDrawWinnersData;
import com.ebizu.manis.sdk.entities.MyVoucherData;
import com.ebizu.manis.sdk.entities.OrderDetail;
import com.ebizu.manis.sdk.entities.PurchasedVoucherData;
import com.ebizu.manis.sdk.entities.PurchaseStatus;
import com.ebizu.manis.sdk.entities.RewardLoginSession;
import com.ebizu.manis.sdk.entities.RewardMessage;
import com.ebizu.manis.sdk.entities.RewardSession;
import com.ebizu.manis.sdk.entities.VoucherTerm;
import com.ebizu.manis.sdk.rest.Callback;
import com.ebizu.manis.sdk.rest.RestConnect;
import com.ebizu.manis.sdk.rest.RestService;
import com.ebizu.manis.sdk.rest.data.AccountInviteTerms;
import com.ebizu.manis.sdk.rest.data.AccountPoint;
import com.ebizu.manis.sdk.rest.data.AccountProfileDetail;
import com.ebizu.manis.sdk.rest.data.AccountProfilePictureUpdate;
import com.ebizu.manis.sdk.rest.data.AccountProfileUpdate;
import com.ebizu.manis.sdk.rest.data.AccountSettingsNotifications;
import com.ebizu.manis.sdk.rest.data.AccountSettingsNotificationsUpdate;
import com.ebizu.manis.sdk.rest.data.AuthSignIn;
import com.ebizu.manis.sdk.rest.data.AuthSignInGoogle;
import com.ebizu.manis.sdk.rest.data.AuthSignInPhone;
import com.ebizu.manis.sdk.rest.data.AuthSignOut;
import com.ebizu.manis.sdk.rest.data.AuthSignUpOTP;
import com.ebizu.manis.sdk.rest.data.BaseRequest;
import com.ebizu.manis.sdk.rest.data.BeaconInterval;
import com.ebizu.manis.sdk.rest.data.BeaconPromoNearby;
import com.ebizu.manis.sdk.rest.data.BrandFeatured;
import com.ebizu.manis.sdk.rest.data.DeeplinkCashvoucher;
import com.ebizu.manis.sdk.rest.data.DeeplinkOffer;
import com.ebizu.manis.sdk.rest.data.DeeplinkSnap;
import com.ebizu.manis.sdk.rest.data.DeeplinkStore;
import com.ebizu.manis.sdk.rest.data.InterestAdd;
import com.ebizu.manis.sdk.rest.data.InterestDelete;
import com.ebizu.manis.sdk.rest.data.InterestUpdate;
import com.ebizu.manis.sdk.rest.data.Interests;
import com.ebizu.manis.sdk.rest.data.LuckyDrawEntriesRest;
import com.ebizu.manis.sdk.rest.data.LuckyDrawScreenRest;
import com.ebizu.manis.sdk.rest.data.LuckyDrawWinnersRest;
import com.ebizu.manis.sdk.rest.data.MiscAnnouncement;
import com.ebizu.manis.sdk.rest.data.MiscHelp;
import com.ebizu.manis.sdk.rest.data.MiscHelpLegal;
import com.ebizu.manis.sdk.rest.data.MyVoucherPost;
import com.ebizu.manis.sdk.rest.data.OTPSkip;
import com.ebizu.manis.sdk.rest.data.OTPStatus;
import com.ebizu.manis.sdk.rest.data.PurchasedVoucherPost;
import com.ebizu.manis.sdk.rest.data.PinValidationPost;
import com.ebizu.manis.sdk.rest.data.PurchaseStatusPost;
import com.ebizu.manis.sdk.rest.data.RestResponse;
import com.ebizu.manis.sdk.rest.data.Review;
import com.ebizu.manis.sdk.rest.data.RewardCancelPaymentPost;
import com.ebizu.manis.sdk.rest.data.RewardLoginSessionPost;
import com.ebizu.manis.sdk.rest.data.RewardPaymentPost;
import com.ebizu.manis.sdk.rest.data.RewardSessionPost;
import com.ebizu.manis.sdk.rest.data.RewardsClaimed;
import com.ebizu.manis.sdk.rest.data.RewardsDetail;
import com.ebizu.manis.sdk.rest.data.RewardsPin;
import com.ebizu.manis.sdk.rest.data.RewardsPinRedeem;
import com.ebizu.manis.sdk.rest.data.RewardsRedeem;
import com.ebizu.manis.sdk.rest.data.RewardsRedeemClose;
import com.ebizu.manis.sdk.rest.data.RewardsRedeemCloseFailed;
import com.ebizu.manis.sdk.rest.data.RewardsUnpin;
import com.ebizu.manis.sdk.rest.data.RewardsVoucherRedeem;
import com.ebizu.manis.sdk.rest.data.RewardsVoucherSearch;
import com.ebizu.manis.sdk.rest.data.RewardsVouchers;
import com.ebizu.manis.sdk.rest.data.Saved;
import com.ebizu.manis.sdk.rest.data.SnapAppeal;
import com.ebizu.manis.sdk.rest.data.SnapHistory;
import com.ebizu.manis.sdk.rest.data.SnapLast;
import com.ebizu.manis.sdk.rest.data.SnapReceiptUpload;
import com.ebizu.manis.sdk.rest.data.SnapStatistic;
import com.ebizu.manis.sdk.rest.data.SnapStoresNearby;
import com.ebizu.manis.sdk.rest.data.SnapStoresPinned;
import com.ebizu.manis.sdk.rest.data.SnapStoresRecent;
import com.ebizu.manis.sdk.rest.data.SnapStoresSearch;
import com.ebizu.manis.sdk.rest.data.SnapSuggestion;
import com.ebizu.manis.sdk.rest.data.SnapTerms;
import com.ebizu.manis.sdk.rest.data.StoresDetail;
import com.ebizu.manis.sdk.rest.data.StoresInterest;
import com.ebizu.manis.sdk.rest.data.StoresInterests;
import com.ebizu.manis.sdk.rest.data.StoresNearby;
import com.ebizu.manis.sdk.rest.data.StoresOfferPin;
import com.ebizu.manis.sdk.rest.data.StoresOfferRedeem;
import com.ebizu.manis.sdk.rest.data.StoresOfferUnpin;
import com.ebizu.manis.sdk.rest.data.StoresPin;
import com.ebizu.manis.sdk.rest.data.StoresSearch;
import com.ebizu.manis.sdk.rest.data.StoresUnpin;
import com.ebizu.manis.sdk.rest.data.VoucherTermPost;
import com.ebizu.manis.sdk.rest.RewardCallback;
import com.ebizu.manis.sdk.rest.RewardRestResponse;
import com.ebizu.manis.sdk.utils.ImageUtil;
import com.ebizu.manis.sdk.utils.ManisSession;
import com.google.gson.GsonBuilder;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by rezkya on 16/05/16.
 */
public class Manis {

    private static Manis instance;
    private static Context mContext;
    private static String TAG = "Manis Upload";
    private Call<RestResponse<StoresNearby.Response>> callStoresNearby;

    private Manis(Context context) {
        this.mContext = context;
    }

    public static Context getContext() {
        return mContext;
    }

    public static synchronized Manis getInstance(Context context) {
        mContext = context;
        if (instance == null) {
            instance = new Manis(context);
            ManisSession.setSession(
                    ManisSession.SessionName.DEVICE, android.provider.Settings.Secure.getString(mContext.getContentResolver(),
                            android.provider.Settings.Secure.ANDROID_ID));
        }
        return instance;
    }

    public void doSignIn(AuthSignIn.Request request, final Callback<RestResponse<AuthSignIn.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AuthSignIn.Response>> call = conn.signin(
                location,
                RestService.getInstance(mContext)._publicKey,
                ManisSession.getSession(ManisSession.SessionName.DEVICE),
                ManisSession.getSession(ManisSession.SessionName.VERSION),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(new Callback<RestResponse<AuthSignIn.Response>>() {
            @Override
            public void onSuccess(int code, RestResponse<AuthSignIn.Response> body) {
                boolean b = ManisLocalData.saveLoginData(body.getData());
                callback.onSuccess(code, body);
            }

            @Override
            public void onFailed(int code, String message) {
                callback.onFailed(code, message);
            }
        });
    }

    public void doSignInPhone(AuthSignInPhone.Request request, final Callback<RestResponse<AuthSignInPhone.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AuthSignInPhone.Response>> call = conn.signinPhone(
                location,
                RestService.getInstance(mContext)._publicKey,
                ManisSession.getSession(ManisSession.SessionName.DEVICE),
                ManisSession.getSession(ManisSession.SessionName.VERSION),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(new Callback<RestResponse<AuthSignInPhone.Response>>() {
            @Override
            public void onSuccess(int code, RestResponse<AuthSignInPhone.Response> body) {
                boolean b = ManisLocalData.saveLoginData(body.getData());
                callback.onSuccess(code, body);
            }

            @Override
            public void onFailed(int code, String message) {
                callback.onFailed(code, message);
            }
        });
    }

    public void doSignInGoogle(AuthSignInGoogle.Request request, final Callback<RestResponse<AuthSignInGoogle.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AuthSignInGoogle.Response>> call = conn.signinGoogle(
                location,
                RestService.getInstance(mContext)._publicKey,
                ManisSession.getSession(ManisSession.SessionName.DEVICE),
                ManisSession.getSession(ManisSession.SessionName.VERSION),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(new Callback<RestResponse<AuthSignInGoogle.Response>>() {
            @Override
            public void onSuccess(int code, RestResponse<AuthSignInGoogle.Response> body) {
                boolean b = ManisLocalData.saveLoginData(body.getData());
                callback.onSuccess(code, body);
            }

            @Override
            public void onFailed(int code, String message) {
                callback.onFailed(code, message);
            }
        });
    }

    public void postSignUpOTP(AuthSignUpOTP.Request request, final Callback<RestResponse<AuthSignUpOTP.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AuthSignUpOTP.Response>> call = conn.signupOTP(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getOTPStatus(OTPStatus.Request request, Callback<RestResponse<OTPStatus.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<OTPStatus.Response>> call = conn.otpStatus(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getOTPSkip(OTPSkip.Request request, Callback<RestResponse<OTPSkip.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<OTPSkip.Response>> call = conn.otpSkip(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void doSignOut(AuthSignOut.Request request, final Callback<RestResponse<AuthSignOut.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AuthSignOut.Response>> call = conn.signout(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG), ""
        );
        call.enqueue(new Callback<RestResponse<AuthSignOut.Response>>() {
            @Override
            public void onSuccess(int code, RestResponse<AuthSignOut.Response> body) {
                ManisLocalData.clearAllCache();
                ManisSession.removeSession(ManisSession.SessionName.TOKEN);
                callback.onSuccess(code, body);
            }

            @Override
            public void onFailed(int code, String message) {
                callback.onFailed(code, message);
            }
        });
    }

    public void getBeaconInterval(BeaconInterval.Request request, Callback<RestResponse<BeaconInterval.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<BeaconInterval.Response>> call = conn.beaconInterval(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getSnapStoresNearby(SnapStoresNearby.Request request, Callback<RestResponse<SnapStoresNearby.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapStoresNearby.Response>> call = conn.snapStoresNearby(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getSnapSuggestion(SnapSuggestion.Request request, Callback<RestResponse<SnapSuggestion.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapSuggestion.Response>> call = conn.snapSuggestions(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postReceiptUpload(final SnapReceiptUpload.Request request, final File fileUpload, final Callback<RestResponse<SnapReceiptUpload.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String compressedFilename = ImageUtil.compressImage(fileUpload);
        File compressedFile = new File(compressedFilename);
        RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpeg"), compressedFile);
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo", compressedFile.getName(), fileBody);
        RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), body);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapReceiptUpload.Response>> call = conn.snapReceiptUpload(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                photoPart,
                description
        );
        call.enqueue(callback);
    }

    public void getSnapHistory(SnapHistory.Request request, Callback<RestResponse<SnapHistory.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapHistory.Response>> call = conn.snapHistory(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getSnapLast(SnapLast.Request request, Callback<RestResponse<SnapLast.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapLast.Response>> call = conn.snapLast(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getSnapTerms(SnapTerms.Request request, Callback<RestResponse<SnapTerms.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapTerms.Response>> call = conn.snapTerms(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getSnapStatistic(SnapStatistic.Request request, Callback<RestResponse<SnapStatistic.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapStatistic.Response>> call = conn.snapStatistic(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getSnapStoresPinned(SnapStoresPinned.Request request, Callback<RestResponse<SnapStoresPinned.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapStoresPinned.Response>> call = conn.snapStoresPinned(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getSnapStoresRecent(SnapStoresRecent.Request request, Callback<RestResponse<SnapStoresRecent.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapStoresRecent.Response>> call = conn.snapStoresRecent(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postSnapStoresSearch(SnapStoresSearch.Request request, Callback<RestResponse<SnapStoresSearch.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapStoresSearch.Response>> call = conn.snapStoresSearch(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postSnapAppeal(SnapAppeal.Request request, Callback<RestResponse<SnapAppeal.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapAppeal.Response>> call = conn.snapAppeal(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getSnapChecksnapable(LuckyDrawScreenRest.Request request, Callback<RestResponse<LuckyDrawSnapable>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<LuckyDrawSnapable>> call = conn.snapChecksnapable(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postInterestUpdate(InterestUpdate.Request request, Callback<RestResponse<InterestUpdate.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<InterestUpdate.Response>> call = conn.interestUpdate(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postInterestAdd(InterestAdd.Request request, Callback<RestResponse<InterestAdd.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<InterestAdd.Response>> call = conn.interestAdd(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postInterestDelete(InterestDelete.Request request, Callback<RestResponse<InterestDelete.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<InterestDelete.Response>> call = conn.interestDelete(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getInterests(Interests.Request request, Callback<RestResponse<Interests.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<Interests.Response>> call = conn.interests(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getStoresInterests(StoresInterests.Request request, Callback<RestResponse<StoresInterests.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<StoresInterests.Response>> call = conn.storesInterests(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getStoresDetail(StoresDetail.Request request, Callback<RestResponse<StoresDetail.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<StoresDetail.Response>> call = conn.storesDetail(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postStoresSearch(StoresSearch.Request request, Callback<RestResponse<StoresSearch.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<StoresSearch.Response>> call = conn.storesSearch(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getStoresInterest(StoresInterest.Request request, Callback<RestResponse<StoresInterest.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<StoresInterest.Response>> call = conn.storesInterest(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getStoresNearby(StoresNearby.Request request, Callback<RestResponse<StoresNearby.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        callStoresNearby = conn.storesNearby(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        callStoresNearby.enqueue(callback);
    }

    public void cancelStoresNearby() {
        if (callStoresNearby != null)
            callStoresNearby.cancel();
    }

    public void postStoresPin(StoresPin.Request request, Callback<RestResponse<StoresPin.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<StoresPin.Response>> call = conn.storesPin(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postStoresUnpin(StoresUnpin.Request request, Callback<RestResponse<StoresUnpin.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<StoresUnpin.Response>> call = conn.storesUnpin(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postStoresOfferPin(StoresOfferPin.Request request, Callback<RestResponse<StoresOfferPin.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<StoresOfferPin.Response>> call = conn.storesOfferPin(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postStoresOfferUnpin(StoresOfferUnpin.Request request, Callback<RestResponse<StoresOfferUnpin.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<StoresOfferUnpin.Response>> call = conn.storesOfferUnpin(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postStoresOfferRedeem(StoresOfferRedeem.Request request, Callback<RestResponse<StoresOfferRedeem.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<StoresOfferRedeem.Response>> call = conn.storesOfferRedeem(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getReviewStatus(Review.Request request, Callback<RestResponse<Review.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<Review.Response>> call = conn.getStatusReview(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void postReviewPopup(Review.Request request, Callback<RestResponse<Review.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<Review.Response>> call = conn.postPopupReview(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getRewardsVouchers(RewardsVouchers.Request request, Callback<RestResponse<RewardsVouchers.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsVouchers.Response>> call = conn.rewardsVouchers(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postRewardsVoucherRedeem(RewardsVoucherRedeem.Request request, Callback<RestResponse<RewardsVoucherRedeem.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsVoucherRedeem.Response>> call = conn.rewardsVoucherRedeem(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postRewardsPinRedeem(RewardsPinRedeem.Request request, Callback<RestResponse<RewardsPinRedeem.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsPinRedeem.Response>> call = conn.rewardsPinRedeem(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postRewardsPin(RewardsPin.Request request, Callback<RestResponse<RewardsPin.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsPin.Response>> call = conn.rewardsPin(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postRewardsUnpin(RewardsUnpin.Request request, Callback<RestResponse<RewardsUnpin.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsUnpin.Response>> call = conn.rewardsUnpin(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postRewardsRedeem(RewardsRedeem.Request request, Callback<RestResponse<RewardsRedeem.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsRedeem.Response>> call = conn.rewardsRedeem(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postRewardsRedeemClose(RewardsRedeemClose.Request request, Callback<RestResponse<RewardsRedeemClose.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsRedeemClose.Response>> call = conn.rewardsRedeemClose(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postRewardsRedeemCloseFailed(RewardsRedeemCloseFailed.Request request, Callback<RestResponse<RewardsRedeemCloseFailed.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsRedeemCloseFailed.Response>> call = conn.rewardsRedeemCloseFailed(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void postRewardsVoucherSearch(RewardsVoucherSearch.Request request, Callback<RestResponse<RewardsVoucherSearch.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsVoucherSearch.Response>> call = conn.rewardsVoucherSearch(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getRewardsClaimed(RewardsClaimed.Request request, Callback<RestResponse<RewardsClaimed.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsClaimed.Response>> call = conn.rewardsClaimed(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getRewardsDetail(RewardsDetail.Request request, Callback<RestResponse<RewardsDetail.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<RewardsDetail.Response>> call = conn.rewardsDetail(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getSaved(Saved.Request request, Callback<RestResponse<Saved.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<Saved.Response>> call = conn.saved(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getAccountPoint(AccountPoint.Request request, final Callback<RestResponse<AccountPoint.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AccountPoint.Response>> call = conn.accountPoint(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(new Callback<RestResponse<AccountPoint.Response>>() {
            @Override
            public void onSuccess(int code, RestResponse<AccountPoint.Response> body) {
                boolean b = ManisLocalData.saveAccountPoint(body.data.point);
                callback.onSuccess(code, body);
            }

            @Override
            public void onFailed(int code, String message) {
                callback.onFailed(code, message);
            }
        });
    }

    public void getAccountDetail(AccountProfileDetail.Request request, final Callback<RestResponse<AccountProfileDetail.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AccountProfileDetail.Response>> call = conn.accountDetail(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(new Callback<RestResponse<AccountProfileDetail.Response>>() {
            @Override
            public void onSuccess(int code, RestResponse<AccountProfileDetail.Response> body) {
                LoginData loginData = body.getData();
                loginData.point = ManisLocalData.getAccountPoint();
                boolean b = ManisLocalData.saveLoginData(loginData);
                callback.onSuccess(code, body);
            }

            @Override
            public void onFailed(int code, String message) {
                callback.onFailed(code, message);
            }
        });
    }

    public void getAccountInviteTerms(AccountInviteTerms.Request request, final Callback<RestResponse<AccountInviteTerms.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AccountInviteTerms.Response>> call = conn.accountInviteTerms(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void postAccountProfileUpdate(AccountProfileUpdate.Request request, final Callback<RestResponse<AccountProfileUpdate.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AccountProfileUpdate.Response>> call = conn.accountProfileUpdate(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(new Callback<RestResponse<AccountProfileUpdate.Response>>() {
            @Override
            public void onSuccess(int code, RestResponse<AccountProfileUpdate.Response> body) {
                boolean b = ManisLocalData.saveLoginData(body.getData());
                callback.onSuccess(code, body);
            }

            @Override
            public void onFailed(int code, String message) {
                callback.onFailed(code, message);
            }
        });
    }

    public void postAccountProfilePictureUpdate(final AccountProfilePictureUpdate.Request request, final File fileUpload, final Callback<RestResponse<AccountProfilePictureUpdate.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String compressedFilename = ImageUtil.compressImage(fileUpload);
        File compressedFile = new File(compressedFilename);
        RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpeg"), compressedFile);
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo", compressedFile.getName(), fileBody);
        RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), body);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AccountProfilePictureUpdate.Response>> call = conn.accountProfilePictureUpdate(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                photoPart,
                description
        );

        call.enqueue(new Callback<RestResponse<AccountProfilePictureUpdate.Response>>() {
            @Override
            public void onSuccess(int code, RestResponse<AccountProfilePictureUpdate.Response> body) {
                boolean b = ManisLocalData.saveLoginData(body.getData());
                callback.onSuccess(code, body);
            }

            @Override
            public void onFailed(int code, String message) {
                callback.onFailed(code, message);
            }
        });
    }

    public void getMiscHelpLegal(MiscHelpLegal.Request request, final Callback<RestResponse<MiscHelpLegal.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<MiscHelpLegal.Response>> call = conn.miscHelpLegal(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getMiscHelp(MiscHelp.Request request, final Callback<RestResponse<MiscHelp.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<MiscHelp.Response>> call = conn.miscHelp(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getMiscAnnouncement(MiscAnnouncement.Request request, final Callback<RestResponse<MiscAnnouncement.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<MiscAnnouncement.Response>> call = conn.miscAnnouncement(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getAccountSettingsNotification(AccountSettingsNotifications.Request request, Callback<RestResponse<AccountSettingsNotifications.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AccountSettingsNotifications.Response>> call = conn.accountSettingsNotification(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void postAccountSettingsNotificationsUpdate(AccountSettingsNotificationsUpdate.Request request, Callback<RestResponse<AccountSettingsNotificationsUpdate.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<AccountSettingsNotificationsUpdate.Response>> call = conn.accountSettingsNotificationUpdate(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getBeaconPromoNearby(BeaconPromoNearby.Request request, Callback<RestResponse<BeaconPromoNearby.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<BeaconPromoNearby.Response>> call = conn.beaconPromoNearby(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getDeeplinkOffer(DeeplinkOffer.Request request, Callback<RestResponse<DeeplinkOffer.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<DeeplinkOffer.Response>> call = conn.deeplinkOffer(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getDeeplinkSnap(DeeplinkSnap.Request request, Callback<RestResponse<DeeplinkSnap.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<DeeplinkSnap.Response>> call = conn.deeplinkSnap(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getDeeplinkStore(DeeplinkStore.Request request, Callback<RestResponse<DeeplinkStore.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<DeeplinkStore.Response>> call = conn.deeplinkStore(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getDeeplinkCashvoucher(DeeplinkCashvoucher.Request request, Callback<RestResponse<DeeplinkCashvoucher.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<DeeplinkCashvoucher.Response>> call = conn.deeplinkCashvoucher(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getBrandFeatured(BrandFeatured.Request request, Callback<RestResponse<BrandFeatured.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<BrandFeatured.Response>> call = conn.brandFeatured(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getLuckyDrawScreen(LuckyDrawScreenRest.Request request, Callback<RestResponse<LuckyDrawScreen>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<LuckyDrawScreen>> call = conn.luckyDrawScreen(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void getLuckyDrawEntries(LuckyDrawEntriesRest.Request request, Callback<RestResponse<LuckyDrawEntryData>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<LuckyDrawEntryData>> call = conn.luckyDrawEntries(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getLuckyDrawWinners(LuckyDrawWinnersRest.Request request, Callback<RestResponse<LuckyDrawWinnersData>> callback) {
        RestConnect conn = getRestConnect(true);
        String body = getBody(request);
        String location = getLastKnownLocation(request);
        Call<RestResponse<LuckyDrawWinnersData>> call = conn.luckyDrawWinners(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                body
        );
        call.enqueue(callback);
    }

    public void getLuckyDrawTerms(SnapTerms.Request request, Callback<RestResponse<SnapTerms.Response>> callback) {
        RestConnect conn = getRestConnect(true);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapTerms.Response>> call = conn.luckyDrawTerms(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG)
        );
        call.enqueue(callback);
    }

    public void postLuckyDrawUpload(final SnapReceiptUpload.Request request, final File fileUpload, final Callback<RestResponse<SnapReceiptUpload.Response>> callback) {
        RestConnect conn = getRestConnect(false);
        String body = getBody(request);
        String compressedFilename = ImageUtil.compressImage(fileUpload);
        File compressedFile = new File(compressedFilename);
        RequestBody fileBody = RequestBody.create(MediaType.parse("image/jpeg"), compressedFile);
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo", compressedFile.getName(), fileBody);
        RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), body);
        String location = getLastKnownLocation(request);
        Call<RestResponse<SnapReceiptUpload.Response>> call = conn.luckyDrawUpload(
                location,
                ManisSession.getSession(ManisSession.SessionName.TOKEN),
                ManisSession.getSession(ManisSession.SessionName.LANG),
                photoPart,
                description
        );
        call.enqueue(callback);
    }

    private RestConnect getRestConnect(boolean withRetry) {
        if (withRetry)
            return RestService.getInstance(mContext).getConnectionsWithRetry();
        else
            return RestService.getInstance(mContext).getConnections();
    }

    private <A extends BaseRequest> String getBody(A request) {
        if (ManisSession.isEncryptEnabled())
            return RestService.getInstance(mContext).encryptedMsg(((new GsonBuilder().disableHtmlEscaping().serializeNulls().create()).toJson(request.body, request.body.getClass())));
        else
            return ((new GsonBuilder().disableHtmlEscaping().serializeNulls().create()).toJson(request.body, request.body.getClass()));
    }

    private String getLastKnownLocation(BaseRequest request) {
        double latitude = request.header.coordinate.latitude;
        double longitude = request.header.coordinate.longitude;
        String location = String.valueOf(latitude) + "," + String.valueOf(longitude);
        if ((latitude == 0) && (longitude == 0)) {
            if (ManisSession.getSession(ManisSession.SessionName.LAST_KNOWN_LOCATION).isEmpty()) {
                ManisSession.setSession(ManisSession.SessionName.LAST_KNOWN_LOCATION, location);
            } else {
                location = ManisSession.getSession(ManisSession.SessionName.LAST_KNOWN_LOCATION);
            }
        } else {
            ManisSession.setSession(ManisSession.SessionName.LAST_KNOWN_LOCATION, location);
        }
        return location;
    }

    public void rewardPaymentPost(final RewardPaymentPost.RequestBody request, final RewardCallback<RewardRestResponse<OrderDetail>> callback) {

        RestConnect conn = getRestConnect(false);
        Call<RewardRestResponse<OrderDetail>> call = conn.rewardPaymentPost(
                request
        );
        call.enqueue(callback);

    }

    public void brandTermsPost(final VoucherTermPost.RequestBody requestBody, final RewardCallback<RewardRestResponse<VoucherTerm>> callback) {
        RestConnect conn = getRestConnect(false);
        Call<RewardRestResponse<VoucherTerm>> call = conn.rewardPinPost(
                requestBody
        );
        call.enqueue(callback);
    }

    public void rewardLoginSessionPost(final RewardLoginSessionPost.RequestBody requestBody, final RewardCallback<RewardRestResponse<RewardLoginSession>> callback) {
        RestConnect conn = getRestConnect(false);
        Call<RewardRestResponse<RewardLoginSession>> call = conn.rewardLoginSessionPost(
                requestBody
        );
        call.enqueue(callback);
    }

    public void rewardSessionPost(final RewardSessionPost.RequestBody request, final RewardCallback<RewardRestResponse<RewardSession>> callback) {
        RestConnect conn = getRestConnect(false);
        Call<RewardRestResponse<RewardSession>> call = conn.rewardSessionPost(
                request
        );
        call.enqueue(callback);
    }

    public void purchasedVoucherHistory(final PurchasedVoucherPost.RequestBody request, final RewardCallback<RewardRestResponse<PurchasedVoucherData>> callback) {
        RestConnect conn = getRestConnect(false);
        Call<RewardRestResponse<PurchasedVoucherData>> call = conn.rewardPurchasedHistory(
                request
        );
        call.enqueue(callback);
    }

    public void rewardMyVoucher(final MyVoucherPost.RequestBody request, final RewardCallback<RewardRestResponse<MyVoucherData>> callback) {
        RestConnect conn = getRestConnect(false);
        Call<RewardRestResponse<MyVoucherData>> call = conn.rewardMyVoucher(
                request
        );
        call.enqueue(callback);
    }

    public void purchaseStatusPost(final PurchaseStatusPost.RequestBody request, final RewardCallback<RewardRestResponse<PurchaseStatus>> callback) {
        RestConnect conn = getRestConnect(false);
        Call<RewardRestResponse<PurchaseStatus>> call = conn.purchaseStatusPost(
                request
        );
        call.enqueue(callback);
    }

    public void pinValidationPost(final PinValidationPost.RequestBody requestBody, final RewardCallback<RewardRestResponse<RewardMessage>> callback) {
        RestConnect conn = getRestConnect(false);
        Call<RewardRestResponse<RewardMessage>> call = conn.pinValidationPost(
                requestBody
        );
        call.enqueue(callback);
    }

    public void rewardCancelPaymentPost(final RewardCancelPaymentPost.RequestBody requestBody, final RewardCallback<RewardRestResponse<RewardMessage>> callback) {
        RestConnect conn = getRestConnect(false);
        Call<RewardRestResponse<RewardMessage>> call = conn.rewardCancelPaymentPost(
                requestBody
        );
        call.enqueue(callback);
    }

}