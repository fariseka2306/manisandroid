package com.ebizu.manis.sdk.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public class VoucherTerm implements Parcelable {

    @SerializedName("terms")
    @Expose
    private String terms;

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.terms);
    }

    public VoucherTerm() {
    }

    protected VoucherTerm(Parcel in) {
        this.terms = in.readString();
    }

    public static final Creator<VoucherTerm> CREATOR = new Creator<VoucherTerm>() {
        @Override
        public VoucherTerm createFromParcel(Parcel source) {
            return new VoucherTerm(source);
        }

        @Override
        public VoucherTerm[] newArray(int size) {
            return new VoucherTerm[size];
        }
    };
}

