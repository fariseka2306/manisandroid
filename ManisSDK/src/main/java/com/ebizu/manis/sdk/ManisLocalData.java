package com.ebizu.manis.sdk;

import android.util.Log;

import com.ebizu.manis.sdk.database.LocalData;
import com.ebizu.manis.sdk.entities.BeaconPromo;
import com.ebizu.manis.sdk.entities.DeeplinkNotification;
import com.ebizu.manis.sdk.entities.LoginData;
import com.ebizu.manis.sdk.entities.SnapData;
import com.ebizu.manis.sdk.entities.StoreData;
import com.ebizu.manis.sdk.entities.StoreDetail;
import com.ebizu.manis.sdk.entities.StoreInterestsItem;
import com.ebizu.manis.sdk.entities.TimestampedData;
import com.ebizu.manis.sdk.entities.VoucherRedeemValidate;
import com.ebizu.manis.sdk.rest.data.AccountInviteTerms;
import com.ebizu.manis.sdk.rest.data.BeaconInterval;
import com.ebizu.manis.sdk.rest.data.SnapTerms;
import com.google.gson.reflect.TypeToken;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by rezkya on 29/05/16.
 */
public class ManisLocalData {

    private static long SECOND = 1000;
    private static long MINUTE = SECOND * 60;
    private static long HOUR = MINUTE * 60;
    private static long DAY = HOUR * 24;

    public static final String BEACON_TYPE_DAY = "day";
    public static final String BEACON_TYPE_HOUR = "hour";

    public static <T extends LoginData> boolean saveLoginData(T loginData) {
        return LocalData.saveLocalData(LocalData.Type.LOGIN_DATA, loginData, new TypeToken<LoginData>() {
        }.getType());
    }

    public static LoginData getLoginData() {
        return LocalData.getLocalData(LocalData.Type.LOGIN_DATA, LoginData.class);
    }

    public static boolean isLoginDataUpdatedToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.LOGIN_DATA));
    }

    public static boolean saveSnapHistory(ArrayList<SnapData> historyData) {
        return LocalData.saveLocalData(LocalData.Type.SNAP_HISTORY, historyData);
    }

    public static boolean saveBeaconInterval(BeaconInterval.Response beaconInterval) {
        return LocalData.saveLocalData(LocalData.Type.BEACON_INTERVAL, beaconInterval);
    }

    public static BeaconInterval.Response getBeaconInterval() {
        return LocalData.getLocalData(LocalData.Type.BEACON_INTERVAL, BeaconInterval.Response.class);
    }

    public static boolean isBeaconIntervalToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.BEACON_INTERVAL));
    }

    public static ArrayList<SnapData> getSnapHistory() {
        return LocalData.getLocalData(LocalData.Type.SNAP_HISTORY, new TypeToken<ArrayList<SnapData>>() {
        }.getType());
    }

    public static boolean isSnapHistoryUpdatedToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.SNAP_HISTORY));
    }

    public static boolean saveSnapStoresNearby(ArrayList<StoreData> snapStoreNearby) {
        return LocalData.saveLocalData(LocalData.Type.SNAP_STORE_NEARBY, snapStoreNearby);
    }

    public static ArrayList<StoreData> getSnapStoresNearby() {
        return LocalData.getLocalData(LocalData.Type.SNAP_STORE_NEARBY, new TypeToken<ArrayList<StoreData>>() {
        }.getType());
    }

    public static boolean isSnapStoresNearbyToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.SNAP_STORE_NEARBY));
    }

    public static boolean saveSnapStoresPinned(ArrayList<StoreData> snapStorePinned) {
        return LocalData.saveLocalData(LocalData.Type.SNAP_STORE_PINNED, snapStorePinned);
    }

    public static ArrayList<StoreData> getSnapStorePinned() {
        return LocalData.getLocalData(LocalData.Type.SNAP_STORE_PINNED, new TypeToken<ArrayList<StoreData>>() {
        }.getType());
    }

    public static boolean isSnapStoreToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.SNAP_STORE_PINNED));
    }

    public static boolean saveSnapStoresRecent(ArrayList<StoreData> snapStoresRecent) {
        return LocalData.saveLocalData(LocalData.Type.SNAP_STORE_RECENT, snapStoresRecent);
    }

    public static ArrayList<StoreData> getSnapStoresRecent() {
        return LocalData.getLocalData(LocalData.Type.SNAP_STORE_RECENT, new TypeToken<ArrayList<StoreData>>() {
        }.getType());
    }

    public static boolean isSnapStoresRecentToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.SNAP_STORE_RECENT));
    }

    public static boolean saveStoresNearby(ArrayList<StoreData> storesNearby) {
        return LocalData.saveLocalData(LocalData.Type.STORE_NEARBY, storesNearby);
    }

    public static ArrayList<StoreData> getStoreNearby() {
        return LocalData.getLocalData(LocalData.Type.STORE_NEARBY, new TypeToken<ArrayList<StoreData>>() {
        }.getType());
    }

    public static boolean isStoreNearbyToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.STORE_NEARBY));
    }

    public static boolean saveSnapTerms(SnapTerms.Response snapTerms) {
        return LocalData.saveLocalData(LocalData.Type.SNAP_TERMS, snapTerms);
    }

    public static SnapTerms.Response getSnapTerms() {
        return LocalData.getLocalData(LocalData.Type.SNAP_TERMS, new TypeToken<SnapTerms.Response>() {
        }.getType());
    }

    public static boolean isSnapTermsToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.SNAP_TERMS));
    }

    public static boolean saveLuckyDrawTerms(SnapTerms.Response luckyDrawTerms) {
        return LocalData.saveLocalData(LocalData.Type.LUCKYDRAW_TERMS, luckyDrawTerms);
    }

    public static SnapTerms.Response getLuckyDrawTerms() {
        return LocalData.getLocalData(LocalData.Type.LUCKYDRAW_TERMS, new TypeToken<SnapTerms.Response>() {
        }.getType());
    }

    public static boolean isLuckyDrawTermsToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.LUCKYDRAW_TERMS));
    }

    public static boolean saveAccountInviteTerms(AccountInviteTerms.Response accountInviteTerms) {
        return LocalData.saveLocalData(LocalData.Type.ACCOUNT_INVITE_TERMS, accountInviteTerms);
    }

    public static AccountInviteTerms.Response getAccountInviteTerms() {
        return LocalData.getLocalData(LocalData.Type.ACCOUNT_INVITE_TERMS, new TypeToken<AccountInviteTerms.Response>() {
        }.getType());
    }

    public static boolean isAccountInviteTermsToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.ACCOUNT_INVITE_TERMS));
    }

    public static boolean saveStoresCategory(ArrayList<StoreData> storesCategory) {
        return LocalData.saveLocalData(LocalData.Type.STORE_CATEGORY, storesCategory);
    }

    public static ArrayList<StoreData> getStoreCategory() {
        return LocalData.getLocalData(LocalData.Type.STORE_CATEGORY, new TypeToken<ArrayList<StoreData>>() {
        }.getType());
    }

    public static boolean isStoreCategoryToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.STORE_CATEGORY));
    }

    public static boolean saveStoresDetail(ArrayList<StoreDetail> storesDetail) {
        return LocalData.saveLocalData(LocalData.Type.STORE_DETAIL, storesDetail);
    }

    public static ArrayList<StoreDetail> getStoreDetail() {
        return LocalData.getLocalData(LocalData.Type.STORE_DETAIL, new TypeToken<ArrayList<StoreDetail>>() {
        }.getType());
    }

    public static boolean isStoreDetailToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.STORE_DETAIL));
    }

    public static boolean saveStoresInterest(ArrayList<StoreData> storeInterest) {
        return LocalData.saveLocalData(LocalData.Type.STORE_INTEREST, storeInterest);
    }

    public static ArrayList<StoreData> getStoreInterest() {
        return LocalData.getLocalData(LocalData.Type.STORE_INTEREST, new TypeToken<ArrayList<StoreData>>() {
        }.getType());
    }

    public static boolean isStoreInterestToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.STORE_INTEREST));
    }

    public static boolean saveStoresInterests(ArrayList<StoreInterestsItem> storeInterestsItems) {
        return LocalData.saveLocalData(LocalData.Type.STORE_INTERESTS, storeInterestsItems);
    }

    public static ArrayList<StoreInterestsItem> getStoreInterests() {
        return LocalData.getLocalData(LocalData.Type.STORE_INTERESTS, new TypeToken<ArrayList<StoreInterestsItem>>() {
        }.getType());
    }

    public static boolean isStoreInterestsToday() {
        return isAlreadyDoItToday(LocalData.getLocalDataTimestamp(LocalData.Type.STORE_INTERESTS));
    }

    public static boolean saveReviewPostedToday(long latestPostTime) {
        return LocalData.saveLocalData(LocalData.Type.REVIEW_POST_TODAY, latestPostTime);
    }

    private static boolean isAlreadyDoItToday(long timestamp) {
        long today = System.currentTimeMillis();
        today = (long) Math.floor(today / DAY);
        timestamp = (long) Math.floor(timestamp / DAY);
        return !(today > timestamp);
    }

    public static boolean saveAccountPoint(long point) {
        LoginData loginData = getLoginData();
        if (loginData != null) {
            loginData.point = point;
            return saveLoginData(loginData);
        } else {
            return false;
        }
    }

    public static boolean saveProfilePicture(String accPhoto) {
        LoginData loginData = getLoginData();
        loginData.accPhoto = accPhoto;
        return saveLoginData(loginData);
    }

    public static long getAccountPoint() {
        LoginData loginData = getLoginData();
        if (loginData != null) {
            return loginData.point;
        } else {
            return 0L;
        }
    }

    public static boolean addDataWithTimestamp(TimestampedData data, final boolean isExpired) {
        if (data.getType() == TimestampedData.Type.NOTIFICATION_SILENT) {
            DeeplinkNotification deeplinkNotification = data.getData();
            if (deeplinkNotification.getApp().equalsIgnoreCase("manis") ||
                    deeplinkNotification.getApp().equalsIgnoreCase("campaign")) {
                removeDataWithTimestamp(data, isExpired);
                clearByInterval();
            }
        } else {
            removeDataWithTimestamp(data, isExpired);
            clearByInterval();
        }
        ArrayList<TimestampedData> timestampedDatas = getAllDataWithTimestamp();
        if (timestampedDatas == null) {
            timestampedDatas = new ArrayList<>();
        }
        if (timestampedDatas.isEmpty()) {
            timestampedDatas.add(data);
        } else {
            if (data.getType() == TimestampedData.Type.BEACON_PROMO) {
                BeaconPromo beaconPromo = data.getData();
                if (getBeaconPromo(beaconPromo.getSn()) == null) {
                    timestampedDatas.add(data);
                }
            } else {
                DeeplinkNotification deeplinkNotification = data.getData();
                if (deeplinkNotification.getUri() != null) {
                    if (getNotifSilentByUri(deeplinkNotification.getUri()) == null) {
                        timestampedDatas.add(data);
                    }
                } else {
                    timestampedDatas.add(data);
                }
            }
        }
        return LocalData.saveLocalData(LocalData.Type.TIMESTAMPED_DATA, timestampedDatas);
    }

    public static boolean updateSeenDataWithTimestamp(TimestampedData data) {
        ArrayList<TimestampedData> timestampedDatas = getAllDataWithTimestamp();
        if (timestampedDatas == null) timestampedDatas = new ArrayList<>();
        for (TimestampedData item : timestampedDatas) {
            try {
                if (item.getType() == TimestampedData.Type.BEACON_PROMO && data.getType() == TimestampedData.Type.BEACON_PROMO) {
                    BeaconPromo beaconPromo = data.getData();
                    if (((BeaconPromo) item.getData()).getSn().equalsIgnoreCase(beaconPromo.getSn())) {
                        item.setSeen(true);
                        break;
                    }
                } else if (item.getType() == TimestampedData.Type.NOTIFICATION_SILENT && data.getType() == TimestampedData.Type.NOTIFICATION_SILENT) {
                    DeeplinkNotification deeplinkNotification = data.getData();
                    if (((DeeplinkNotification) item.getData()).getUri() != null) {
                        if (((DeeplinkNotification) item.getData()).getUri().equalsIgnoreCase(deeplinkNotification.getUri())) {
                            item.setSeen(true);
                            break;
                        }
                    } else {
                        if (item.getTimestamp() == data.getTimestamp()) {
                            item.setSeen(true);
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("ManisLocalData", "Failed to convert data");
            }
        }
        return LocalData.saveLocalData(LocalData.Type.TIMESTAMPED_DATA, timestampedDatas);
    }

    public static boolean updateReadDataWithTimestamp(TimestampedData data) {
        ArrayList<TimestampedData> timestampedDatas = getAllDataWithTimestamp();
        if (timestampedDatas == null) {
            timestampedDatas = new ArrayList<>();
        }

        for (TimestampedData item : timestampedDatas) {
            if (item.getType() == TimestampedData.Type.BEACON_PROMO &&
                    data.getType() == TimestampedData.Type.BEACON_PROMO) {
                BeaconPromo beaconPromo = data.getData();
                if (((BeaconPromo) item.getData()).getSn().equalsIgnoreCase(beaconPromo.getSn())) {
                    item.setRead(true);
                    break;
                }
            } else if (item.getType() == TimestampedData.Type.NOTIFICATION_SILENT &&
                    data.getType() == TimestampedData.Type.NOTIFICATION_SILENT) {
                DeeplinkNotification deeplinkNotification = data.getData();
                if (((DeeplinkNotification) item.getData()).getUri() != null) {
                    if (((DeeplinkNotification) item.getData()).getUri().equalsIgnoreCase(deeplinkNotification.getUri())) {
                        item.setRead(true);
                        break;
                    }
                } else {
                    if (item.getTimestamp() == data.getTimestamp()) {
                        item.setRead(true);
                        break;
                    }
                }
            }
        }
        return LocalData.saveLocalData(LocalData.Type.TIMESTAMPED_DATA, timestampedDatas);
    }

    public static boolean removeDataWithTimestamp(TimestampedData data, final boolean isExpired) {
        ArrayList<TimestampedData> timestampedDatas = getAllDataWithTimestamp();
        if (timestampedDatas == null) {
            timestampedDatas = new ArrayList<>();
        }

        if (!timestampedDatas.isEmpty()) {
            for (TimestampedData item : timestampedDatas) {
                if (item.getType() == TimestampedData.Type.BEACON_PROMO &&
                        data.getType() == TimestampedData.Type.BEACON_PROMO) {
                    BeaconPromo beaconPromo = data.getData();
                    if (((BeaconPromo) item.getData()).getSn().equalsIgnoreCase(beaconPromo.getSn())) {
                        if (isExpired) {
                            timestampedDatas.remove(item);
                            Log.i("BeaconRemoved", "Beacon " + beaconPromo.getSn() + "Removed because expired");
                        } else {
                            item.setDeleted(true);
                            Log.i("BeaconRemoved", "Beacon " + beaconPromo.getSn() + "Removed Temporary");
                        }
                        break;
                    }
                } else if (item.getType() == TimestampedData.Type.NOTIFICATION_SILENT &&
                        data.getType() == TimestampedData.Type.NOTIFICATION_SILENT) {
                    DeeplinkNotification deeplinkNotification = data.getData();
                    if (((DeeplinkNotification) item.getData()).getUri() != null) {
                        if (((DeeplinkNotification) item.getData()).getUri().equalsIgnoreCase(deeplinkNotification.getUri())) {
                            timestampedDatas.remove(item);
                            break;
                        }
                    } else {
                        if (item.getTimestamp() == data.getTimestamp()) {
                            timestampedDatas.remove(item);
                            break;
                        }
                    }
                }
            }
        }
        return LocalData.saveLocalData(LocalData.Type.TIMESTAMPED_DATA, timestampedDatas);
    }

    public static ArrayList<TimestampedData> getAllDataWithTimestamp() {
        ArrayList<TimestampedData> result = new ArrayList<>();
        ArrayList<TimestampedData> temp = LocalData.getLocalData(LocalData.Type.TIMESTAMPED_DATA, new TypeToken<ArrayList<TimestampedData>>() {
        }.getType());
        if (temp != null) {
            for (TimestampedData item : temp) {
                result.add(item);
            }
        }
        return result;
    }

    public static ArrayList<TimestampedData> getAllDataWithTimestampNotDeleted() {
        ArrayList<TimestampedData> result = new ArrayList<>();
        ArrayList<TimestampedData> temp = LocalData.getLocalData(LocalData.Type.TIMESTAMPED_DATA, new TypeToken<ArrayList<TimestampedData>>() {
        }.getType());
        if (temp != null) {
            for (TimestampedData item : temp) {
                if (!item.isDeleted()) {
                    result.add(item);
                }
            }
        }
        return result;
    }

    //bug fix number notif, still dont know the query is
    public static int getCountDataIsNotSeen() {
        int count = 0;
        ArrayList<TimestampedData> temp = getAllDataWithTimestamp();
        if (temp != null)
            for (TimestampedData item : temp) {
                //change from read to seen
                if (!item.isSeen() && !item.isDeleted()) {
                    count++;
                }
            }
        return count;
    }

    public static TimestampedData getBeaconPromo(String serialNumber) {
        ArrayList<TimestampedData> beaconPromos = getAllDataWithTimestamp();
        if (beaconPromos == null) {
            beaconPromos = new ArrayList<>();
        }
        TimestampedData result = null;

        for (TimestampedData data : beaconPromos) {
            if (data.getType() == TimestampedData.Type.BEACON_PROMO) {
                BeaconPromo beaconPromo = data.getData();
                if (beaconPromo.getSn().equalsIgnoreCase(serialNumber)) {
                    result = data;
                    break;
                }
            }
        }
        return result;
    }

    public static TimestampedData getBeaconPromoById(String id) {
        ArrayList<TimestampedData> beaconPromos = getAllDataWithTimestamp();
        if (beaconPromos == null) {
            beaconPromos = new ArrayList<>();
        }
        TimestampedData result = null;

        for (TimestampedData data : beaconPromos) {
            if (data.getType() == TimestampedData.Type.BEACON_PROMO) {
                BeaconPromo beaconPromo = data.getData();
                if (beaconPromo.getId().equalsIgnoreCase(id)) {
                    result = data;
                    break;
                }
            }
        }
        return result;
    }

    public static TimestampedData getNotifSilentByUri(String uri) {
        ArrayList<TimestampedData> deeplinkDatas = getAllDataWithTimestamp();
        if (deeplinkDatas == null) {
            deeplinkDatas = new ArrayList<>();
        }
        TimestampedData result = null;

        for (TimestampedData data : deeplinkDatas) {
            if (data.getType() == TimestampedData.Type.NOTIFICATION_SILENT) {
                DeeplinkNotification deeplinkNotification = data.getData();

                if (deeplinkNotification.getUri() != null) {
                    if (deeplinkNotification.getUri().equals(uri)) {
                        result = data;
                        break;
                    }
                }
            }
        }
        return result;
    }

    public static boolean addVoucherRedeemValidate(VoucherRedeemValidate data) {
        removeVoucherRedeemValidate(data);
        ArrayList<VoucherRedeemValidate> datas = getAllVoucherRedeemValidate();
        if (datas == null) {
            datas = new ArrayList<>();
        }
        datas.add(data);
        return LocalData.saveLocalData(LocalData.Type.VOUCHER_REDEEM_VALIDATE, datas);
    }

    public static boolean removeVoucherRedeemValidate(VoucherRedeemValidate data) {
        ArrayList<VoucherRedeemValidate> datas = getAllVoucherRedeemValidate();
        if (datas == null) {
            datas = new ArrayList<>();
        }
        for (VoucherRedeemValidate voucherData : datas) {
            boolean sameValueExist;
            sameValueExist = (voucherData.getVoucherId()).equalsIgnoreCase(data.getVoucherId());
            if (sameValueExist) {
                datas.remove(voucherData);
                break;
            }
        }
        return LocalData.saveLocalData(LocalData.Type.VOUCHER_REDEEM_VALIDATE, datas);
    }

    public static VoucherRedeemValidate getVoucherRedeemValidate(String voucherId) {
        ArrayList<VoucherRedeemValidate> datas = getAllVoucherRedeemValidate();
        if (datas == null) {
            datas = new ArrayList<>();
        }
        VoucherRedeemValidate result = null;

        for (VoucherRedeemValidate data : datas) {
            if (data.getVoucherId().equalsIgnoreCase(voucherId)) {
                result = data;
                break;
            }
        }
        return result;
    }

    public static ArrayList<VoucherRedeemValidate> getAllVoucherRedeemValidate() {
        return LocalData.getLocalData(LocalData.Type.VOUCHER_REDEEM_VALIDATE, new TypeToken<ArrayList<VoucherRedeemValidate>>() {
        }.getType());
    }

    public static void updateBeaconPromo(String sn, String code) {
        if (getBeaconPromo(sn) != null) {
            TimestampedData timestampedData = getBeaconPromo(sn);
            Calendar redeemed = Calendar.getInstance();
            Date date = new Date();
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateString = sdf.format(date);

            BeaconPromo beaconPromo = timestampedData.getData();
            beaconPromo.setRedeemed((int) redeemed.getTimeInMillis());
            beaconPromo.setCode(code);
            beaconPromo.setDatetime(dateString);
            beaconPromo.setAlreadyRedeemed(true);

            timestampedData.setData(beaconPromo);

            ArrayList<TimestampedData> timestampedDatas = getAllDataWithTimestamp();
            for (int i = 0; i < timestampedDatas.size(); i++) {
                TimestampedData item = timestampedDatas.get(i);
                if (item.getTimestamp() == timestampedData.getTimestamp()) {
                    Log.i("BeaconRewardsUpdated", "Beacon " + beaconPromo.getSn());
                    timestampedDatas.remove(item);
                    timestampedDatas.add(i, timestampedData);
                }
            }
            LocalData.saveLocalData(LocalData.Type.TIMESTAMPED_DATA, timestampedDatas);
        }
    }

    public static boolean clearTimestampedDataList() {
        ArrayList<TimestampedData> timestampedDatas = getAllDataWithTimestamp();
        ArrayList<TimestampedData> timestampedDatasTemp = new ArrayList<>();
        if (!timestampedDatas.isEmpty()) {
            for (int i = 0; i < timestampedDatas.size(); i++) {
                TimestampedData item = timestampedDatas.get(i);
                if (item.getType() == TimestampedData.Type.BEACON_PROMO) {
                    item.setDeleted(true);
                    timestampedDatasTemp.add(item);
                    BeaconPromo beaconPromo = item.getData();
                    Log.i("BeaconCleared", "Beacon " + beaconPromo.getSn());
                }
            }
        }
        return LocalData.saveLocalData(LocalData.Type.TIMESTAMPED_DATA, timestampedDatasTemp);
    }

    public static boolean clearByInterval() {
        ArrayList<TimestampedData> timestampedDatas = getAllDataWithTimestamp();
        ArrayList<TimestampedData> timestampedDatasTemp = new ArrayList<>(timestampedDatas);

        if (!timestampedDatas.isEmpty()) {
            for (int i = 0; i < timestampedDatas.size(); i++) {
                TimestampedData item = timestampedDatas.get(i);
                if (item.getType() == TimestampedData.Type.BEACON_PROMO) {
                    long time = item.getTimestamp();
                    long timeNow = System.currentTimeMillis();
                    long elapsedTime = timeNow - time;
                    long limit;
                    if (getBeaconInterval() != null) {
                        if (getBeaconInterval().getType().equals(BEACON_TYPE_HOUR)) {
                            limit = getBeaconInterval().getInterval() * HOUR;
                        } else {
                            limit = getBeaconInterval().getInterval() * DAY;
                        }
                    } else {
                        limit = DAY;
                    }

                    if (elapsedTime >= limit) {
                        if (item.isDeleted()) {
                            timestampedDatasTemp.remove(item);
                            BeaconPromo beaconPromo = item.getData();
                            Log.i("BeaconCleared", "Beacon " + beaconPromo.getSn());
                        }
                    }
                }
            }
        }
        return LocalData.saveLocalData(LocalData.Type.TIMESTAMPED_DATA, timestampedDatasTemp);
    }

    public static void clearAllCache() {
        LocalData.clearAllCache();
    }

    public static void clearCache(String type) {
        LocalData.clearCache(type);
    }
}
