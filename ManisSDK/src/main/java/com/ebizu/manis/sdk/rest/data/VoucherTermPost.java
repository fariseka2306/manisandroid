package com.ebizu.manis.sdk.rest.data;

/**
 * Created by halim_ebizu on 9/18/17.
 */

public class VoucherTermPost {

    public static class RequestBody {

        public Data data;
        public String session;
        public String module;
        public String action;
        public String token;

        public RequestBody(Data data, String session, String module, String action, String token) {
            this.data = data;
            this.session = session;
            this.module = module;
            this.action = action;
            this.token = token;
        }

        public static class Data {

            public Data(String voucherId) {
                this.voucherId = voucherId;
            }

            private String voucherId;
        }
    }
}
