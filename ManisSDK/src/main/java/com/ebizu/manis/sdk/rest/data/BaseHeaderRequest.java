package com.ebizu.manis.sdk.rest.data;

import com.ebizu.manis.sdk.entities.Coordinate;

/**
 * Created by rezkya on 26/05/16.
 */
public class BaseHeaderRequest {
    public Coordinate coordinate;

    public BaseHeaderRequest(Coordinate coordinate) {
        this.coordinate = coordinate;
    }
}
