package com.ebizu.manis.di.module;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ebizu-User on 05/07/2017.
 */

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity activity) {
        mActivity = activity;
    }

    @Provides
    Activity provideActivity() {
        return mActivity;
    }
}
