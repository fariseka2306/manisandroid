package com.ebizu.manis.di.component;

import com.ebizu.manis.di.module.ActivityModule;
import com.ebizu.manis.mvp.account.accountmenulist.profile.changeprofilephoto.ChangeProfilePhotoActivity;
import com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.PurchaseHistoryActivity;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileActivity;
import com.ebizu.manis.mvp.account.accountmenulist.settings.notification.NotificationActivity;
import com.ebizu.manis.mvp.interest.InterestActivity;
import com.ebizu.manis.mvp.luckydraw.luckydrawguide.LuckyDrawGuideDialog;
import com.ebizu.manis.mvp.luckydraw.luckydrawdialog.LuckyDrawIntroActivity;
import com.ebizu.manis.mvp.luckydraw.luckydrawsubmit.LuckyDrawSubmitDialog;
import com.ebizu.manis.mvp.luckydraw.luckydrawdialog.LuckyDrawWinnerDialog;
import com.ebizu.manis.mvp.luckydraw.luckydrawdialog.LuckyDrawEntryDialog;
import com.ebizu.manis.mvp.luckydraw.luckydrawhelp.LuckyDrawHelpActivity;
import com.ebizu.manis.mvp.luckydraw.luckydrawterm.LuckyDrawTermDialog;
import com.ebizu.manis.mvp.mission.thematic.ThematicDetailActivity;
import com.ebizu.manis.mvp.onboard.activity.OnBoardActivity;
import com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.MyVouchersActivity;
import com.ebizu.manis.mvp.reward.rewardlistcategory.RewardCategoryAbstractActivity;
import com.ebizu.manis.mvp.snap.form.receiptdetail.SnapStoreDetailActivity;
import com.ebizu.manis.mvp.snap.form.storedetail.ReceiptDetailActivity;
import com.ebizu.manis.mvp.mission.shareexperience.ShareExperienceDetailActivity;
import com.ebizu.manis.mvp.snap.receipt.camera.CameraActivity;
import com.ebizu.manis.mvp.snap.receipt.upload.ReceiptUploadActivity;
import com.ebizu.manis.mvp.snap.store.SnapStoreActivity;
import com.ebizu.manis.mvp.splashscreen.SplashScreenActivity;
import com.ebizu.manis.di.scope.PerActivity;
import com.ebizu.manis.mvp.login.LoginActivity;
import com.ebizu.manis.mvp.store.storecategorydetail.StoreCategoryDetailActivity;
import com.ebizu.manis.mvp.store.storedetail.StoreDetailActivity;
import com.ebizu.manis.mvp.store.storedetaillocation.StoreDetailLocationActivity;
import com.ebizu.manis.mvp.store.storedetailmore.StoreDetailMoreActivity;
import com.ebizu.manis.mvp.store.storeoffer.StoreOfferDetailActivity;
import com.ebizu.manis.mvp.store.storereward.StoreRewardDetailActivity;

import dagger.Component;

/**
 * Created by Ebizu-User on 05/07/2017.
 */

@PerActivity
@Component(modules = ActivityModule.class, dependencies = ApplicationComponent.class)
public interface ActivityComponent {

    void inject(LoginActivity loginActivity);

    void inject(SplashScreenActivity splashScreenActivity);

    void inject(InterestActivity interestActivity);

    void inject(StoreCategoryDetailActivity storeCategoryDetailActivity);

    void inject(StoreDetailActivity storeDetailActivity);

    void inject(StoreDetailMoreActivity storeDetailMoreActivity);

    void inject(StoreDetailLocationActivity storeDetailLocationActivity);

    void inject(StoreOfferDetailActivity storeOfferDetailActivity);

    void inject(StoreRewardDetailActivity storeRewardDetailActivity);

    void inject(LuckyDrawHelpActivity luckyDrawHelpActivity);

    void inject(LuckyDrawEntryDialog luckyDrawEntryDialog);

    void inject(LuckyDrawWinnerDialog luckyDrawWinnerDialog);

    void inject(LuckyDrawIntroActivity luckyDrawIntroActivity);

    void inject(LuckyDrawGuideDialog luckyDrawGuideDialog);

    void inject(LuckyDrawTermDialog luckyDrawTermDialog);

    void inject(LuckyDrawSubmitDialog luckyDrawSubmitDialog);

    void inject(MyVouchersActivity myVouchersActivity);

    void inject(NotificationActivity notificationActivity);

    void inject(ReceiptUploadActivity receiptUploadActivity);

    void inject(OnBoardActivity onBoardActivity);

    void inject(ReceiptDetailActivity receiptDetailActivity);

    void inject(SnapStoreDetailActivity snapStoreDetailActivity);

    void inject(SnapStoreActivity snapStoreActivity);

    void inject(PurchaseHistoryActivity purchaseHistoryActivity);

    void inject(ProfileActivity profileActivity);

    void inject(ChangeProfilePhotoActivity changeProfilePhotoActivity);

    void inject(ShareExperienceDetailActivity shareExperienceDetailActivity);

    void inject(ThematicDetailActivity thematicDetailActivity);

    void inject(RewardCategoryAbstractActivity rewardCategoryAbstractActivity);

    void inject(CameraActivity cameraActivity);

}
