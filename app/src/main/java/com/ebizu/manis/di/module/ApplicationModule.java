package com.ebizu.manis.di.module;

import android.content.Context;

import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.preference.RewardSession;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ebizu-User on 05/07/2017.
 */
@Module
public class ApplicationModule {

    private Context context;
    private ManisSession manisSession;
    private RewardSession rewardSession;
    private DeviceSession deviceSession;

    public ApplicationModule(Context application) {
        this.context = application;
        this.manisSession = new ManisSession(context);
        this.rewardSession = new RewardSession(context);
        this.deviceSession = new DeviceSession(context);
    }

    @Provides
    public Context provideContext() {
        return context;
    }

    @Provides
    public ManisApi manisApi() {
        if (manisSession.isLoggedIn()) {
            return ManisApiGenerator.createServiceWithToken(context);
        } else {
            return ManisApiGenerator.createService(context);
        }
    }

    @Provides
    public ManisSession manisSession() {
        return manisSession;
    }

    @Provides
    public RewardSession rewardSession() {
        return rewardSession;
    }

    @Provides
    public DeviceSession deviceSession() {
        return deviceSession;
    }
}