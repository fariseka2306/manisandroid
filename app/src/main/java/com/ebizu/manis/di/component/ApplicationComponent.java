package com.ebizu.manis.di.component;

import android.content.Context;

import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.preference.RewardSession;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Ebizu-User on 05/07/2017.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    Context exposeContext();

    DeviceSession exposeDeviceSession();

    ManisApi exposeManisApi();

    ManisSession exposeManisSession();

    RewardSession exposeRewardSession();
}
