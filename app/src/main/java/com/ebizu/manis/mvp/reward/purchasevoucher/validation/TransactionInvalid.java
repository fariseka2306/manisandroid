package com.ebizu.manis.mvp.reward.purchasevoucher.validation;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;

/**
 * Created by ebizu on 10/4/17.
 */

public class TransactionInvalid extends TransactionStatus
        implements ITransactionStatus {

    public TransactionInvalid(Context context) {
        super(context);
    }

    @Override
    public String statusMidTrans() {
        return TransactionResult.STATUS_INVALID;
    }

    @Override
    public String statusMolPay() {
        return "";
    }

    @Override
    public String statusPayment() {
        return "unpaid";
    }

    @Override
    public String statusTransaction() {
        return "Payment Invalid";
    }

    @Override
    public Drawable iconStatus() {
        return ContextCompat.getDrawable(context, R.drawable.ic_payment_failed);
    }
}
