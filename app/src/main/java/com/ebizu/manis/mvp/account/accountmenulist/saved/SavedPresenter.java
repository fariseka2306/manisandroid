package com.ebizu.manis.mvp.account.accountmenulist.saved;

import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.response.WrapperSaved;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mac on 8/21/17.
 */

public class SavedPresenter extends BaseViewPresenter implements ISavedPresenter {

    private final String TAG = getClass().getSimpleName();
    SavedView savedView;
    private Subscription subsSaved;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        savedView = (SavedView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsSaved != null) subsSaved.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadSaved(ManisApi manisApi) {
        releaseSubscribe(0);
        savedView.showProgressBar();
        subsSaved = manisApi.getSaved()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperSaved>(savedView) {
                    @Override
                    public void onNext(WrapperSaved wrapperSaved) {
                        super.onNext(wrapperSaved);
                        savedView.setSaved(wrapperSaved.getGetSaved());
                    }
                });
    }

}
