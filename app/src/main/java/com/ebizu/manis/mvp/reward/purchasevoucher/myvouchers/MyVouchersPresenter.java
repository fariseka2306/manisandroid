package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.service.reward.requestbody.RewardMyVoucherBody;
import com.ebizu.manis.service.reward.requestbody.RewardShoppingCartBody;
import com.ebizu.manis.service.reward.response.ResponseRewardApi;
import com.ebizu.manis.service.reward.response.WrapperMyVoucher;
import com.ebizu.manis.service.reward.response.WrapperRewardVoucherList;
import com.ebizu.manis.service.reward.response.WrapperShoppingCart;
import com.ebizu.sdk.reward.models.VoucherInput;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.ebizu.manis.sdk.shared.ManisApp.context;

/**
 * Created by ebizu on 10/2/17.
 */

public class MyVouchersPresenter extends BaseViewPresenter implements IMyVouchersPresenter {

    private static final String TAG = MyVouchersPresenter.class.getSimpleName();

    private MyVouchersView myVouchersView;
    private Subscription subsMyVouchers;

    private Subscription subsShoppingCart;
    private Subscription subsMyVoucher;
    private RewardShoppingCartBody.Data rewardShopData;
    public List<VoucherInput> voucherInputs = new ArrayList<>();
    private Context context;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        myVouchersView = (MyVouchersView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsMyVouchers != null) subsMyVouchers.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void getMyVouchers(RewardMyVoucherBody rewardMyVoucherBody, IBaseView.LoadType loadType, String available) {
        releaseSubscribe(0);
        rewardMyVoucherBody.getData().setCondtion(available);
        initializeLoad(loadType);
        unsubsMyVoucher();
        subsMyVoucher = getRewardApi().listMyVoucher(rewardMyVoucherBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperRewardVoucherList>(myVouchersView) {
                    @Override
                    public void onNext(WrapperRewardVoucherList wrapperMyVoucher) {
                        super.onNext(wrapperMyVoucher);
                        dismissTypeProgress(loadType);
                        myVouchersView.dismissProgressBar();
                        myVouchersView.isOffline = false;
                        myVouchersView.showMyVouchersView(wrapperMyVoucher, loadType);
                        Log.d(TAG, "onNext: " + wrapperMyVoucher);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        myVouchersView.dismissProgressBar();
                        dismissTypeProgress(loadType);
                        if(loadType  == IBaseView.LoadType.SCROLL_LOAD){
                            myVouchersView.page--;
                        }
                        myVouchersView.loadMyVouchersViewFail(loadType);
                    }
                });
    }

    @Override
    public void getShoppingCart(RewardVoucher rewardVoucher, int qty) {
        myVouchersView.getBaseActivity()
                .showProgressBarDialog("Loading...", (var1) -> unsubsShoppingCart());
        RewardShoppingCartBody rewardShoppingCartBody =
                new RewardShoppingCartBody(myVouchersView.getContext(), getRewardShopData(rewardVoucher, qty));
        unsubsShoppingCart();
        subsShoppingCart = getRewardApi().getShoppingCart(rewardShoppingCartBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperShoppingCart>(myVouchersView) {
                    @Override
                    public void onNext(WrapperShoppingCart wrapperShoppingCart) {
                        super.onNext(wrapperShoppingCart);
                        myVouchersView.getBaseActivity().dismissProgressBarDialog();
                        myVouchersView.startActivityShoppingCart(wrapperShoppingCart.getData().getShoppingCart(),rewardVoucher);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        myVouchersView.getBaseActivity().dismissProgressBarDialog();
                    }

                    @Override
                    public void onFailure(ResponseRewardApi responseRewardApi) {
                        super.onFailure(responseRewardApi);
                        NegativeScenarioManager.show(responseRewardApi, myVouchersView, NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }
                });
    }

    private RewardShoppingCartBody.Data getRewardShopData(RewardVoucher reward, int qty) {
        if (null == rewardShopData)
            rewardShopData = new RewardShoppingCartBody.Data();
        rewardShopData.setQty(qty);
        rewardShopData.setVoucherId(reward.getId());
        rewardShopData.setBulkId(reward.getBulkId());
        return rewardShopData;
    }

    @Override
    public void unsubsShoppingCart() {
        if (null != subsShoppingCart)
            subsShoppingCart.unsubscribe();
    }

    @Override
    public void setVoucherInputList(List<VoucherInput> voucherInputs) {
            this.voucherInputs.addAll(voucherInputs);
    }

    @Override
    public void unsubsMyVoucher() {
        if (null != subsMyVoucher)
            subsMyVoucher.unsubscribe();
    }

    private void initializeLoad(IBaseView.LoadType loadType) {
        showTypeProgress(loadType);
    }

    private void showTypeProgress(IBaseView.LoadType loadType){
        myVouchersView.setLoading(true);
        switch (loadType){
            case CLICK_LOAD:{
                myVouchersView.invisibleListVoucherView();
                myVouchersView.showProgressBarCircle();
                break;
            }
            case SCROLL_LOAD:{
                myVouchersView.getMyVoucherAdapter().addLoadingFooter();
                break;
            }
            case SEARCH_LOAD:{
                break;
            }
            case SWIPE_LOAD:{
                break;
            }
        }
    }

    private void dismissTypeProgress(IBaseView.LoadType loadType){
        myVouchersView.setLoading(false);
        switch (loadType){
            case CLICK_LOAD:{
                myVouchersView.dismissProgressBarCircle();
                break;
            }
            case SCROLL_LOAD:{
                myVouchersView.getMyVoucherAdapter().removeLoadingFooter();
                break;
            }
            case SEARCH_LOAD:{
                break;
            }
            case SWIPE_LOAD:{
                break;
            }
        }
    }


}