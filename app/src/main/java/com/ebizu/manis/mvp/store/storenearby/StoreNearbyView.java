package com.ebizu.manis.mvp.store.storenearby;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.multiplier.MultiplierAll;
import com.ebizu.manis.manager.multiplier.MultiplierMenuManager;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.StoreResults;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.adapter.StoreAdapter;
import com.ebizu.manis.view.manis.nodatastorefound.NoDataStoreFoundView;
import com.ebizu.manis.view.manis.nointernetconnection.NoInternetConnectionView;
import com.ebizu.manis.view.manis.search.SearchView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 7/30/17.
 */

public class StoreNearbyView extends BaseView
        implements IStoreNearbyView {

    //*** Type Active Store View ***
    private enum StoreActive {
        SEARCH_STORE,
        NEARBY_STORE
    }

    //*** Type Active Store View ***
    private StoreNearbyPresenter storeNearbyPresenter;
    private StoreAdapter storeAdapter;
    private LinearLayoutManager linearLayoutManager;
    private RelativeLayout[] tabStoreMultipliers;

    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.progress_bar_indeterminate)
    ProgressBar searchProgresView;
    @BindView(R.id.progress_bar_circle)
    ProgressBar progressBarCircle;
    @BindView(R.id.rv_stores)
    RecyclerView rvStore;
    @BindView(R.id.swipe_store)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.text_view_min_char)
    TextView textViewMinChar;
    @BindView(R.id.no_data_store_found_view)
    NoDataStoreFoundView noDataStoreFoundView;
    @BindView(R.id.view_inactive_location)
    View viewInactiveLocation;
    @BindView(R.id.no_internet_connection_view)
    NoInternetConnectionView noInternetConnectionView;

    //*** MultiplierView ***
    @BindView(R.id.ll_tab_multiplier)
    LinearLayout linearLayoutMultiplier;
    @BindView(R.id.item_tab_store_multiplier_all)
    RelativeLayout rlTabStoreMultiplierAll;
    @BindView(R.id.item_tab_store_multiplier_ticket)
    RelativeLayout rlTabStoreMultiplierTicket;
    @BindView(R.id.item_tab_store_multiplier_one)
    RelativeLayout rlTabStoreMultiplierOne;
    @BindView(R.id.item_tab_store_multiplier_two)
    RelativeLayout rlTabStoreMultiplierTwo;
    @BindView(R.id.item_tab_store_multiplier_three)
    RelativeLayout rlTabStoreMultiplierThree;
    @BindView(R.id.item_tab_store_multiplier_four)
    RelativeLayout rlTabStoreMultiplierFour;
    @BindView(R.id.item_tab_store_multiplier_five)
    RelativeLayout rlTabStoreMultiplierFive;
    @BindView(R.id.multiplier_line_all)
    View multiplierLineAll;

    //*** Body parameter ***
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int page = 1;
    private String keyword = "";
    private String merchantTier = new MultiplierAll().merchantTier();
    private int multiplierActive = new MultiplierAll().multiplier();
    private StoreActive storeActive;

    public StoreNearbyView(Context context) {
        super(context);
        createView(context);
    }

    public StoreNearbyView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public StoreNearbyView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public StoreNearbyView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        storeNearbyPresenter = (StoreNearbyPresenter) iBaseViewPresenter;
        storeNearbyPresenter.attachView(this);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.store_nearby_view, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        initView();
        initMultiplier();
        initListener();
        addMultiplierListener();
        addSearchViewListener();
        addViewInactiveLocationListener();
    }

    private void initMultiplier() {
        tabStoreMultipliers = new RelativeLayout[]{
                rlTabStoreMultiplierAll,
                rlTabStoreMultiplierTicket,
                rlTabStoreMultiplierOne,
                rlTabStoreMultiplierTwo,
                rlTabStoreMultiplierThree,
                rlTabStoreMultiplierFour,
                rlTabStoreMultiplierFive
        };
    }

    private void addSearchViewListener() {
        searchView.setOnTextChange(new com.ebizu.manis.view.manis.search.SearchView.OnTextChangedListener() {
            @Override
            public void onValid(String keyword) {
                textViewMinChar.setVisibility(INVISIBLE);
                StoreNearbyView.this.keyword = keyword;
                loadPresenter(LoadType.SEARCH_LOAD);
                getAnalyticManager().trackEvent(
                        ConfigManager.Analytic.Category.FRAGMENT_REWARD_FREE,
                        ConfigManager.Analytic.Action.SEARCH,
                        "Keyword '".concat(keyword).concat("'"));
            }

            @Override
            public void onInvalid() {
                storeAdapter.clearStores();
                textViewMinChar.setVisibility(VISIBLE);
                rvStore.setVisibility(INVISIBLE);
                noDataStoreFoundView.setVisibility(INVISIBLE);
                viewInactiveLocation.setVisibility(INVISIBLE);
            }

            @Override
            public void onEmptyText() {
                // Do nothing because we don't use it.
                textViewMinChar.setVisibility(INVISIBLE);
                noDataStoreFoundView.setVisibility(INVISIBLE);
            }
        });
        searchView.setOnDeleteListener(new SearchView.OnDeleteListener() {
            @Override
            public void onDeleteText() {
                // Do nothing because we don't use it.
                textViewMinChar.setVisibility(INVISIBLE);
            }

            @Override
            public void onDeleteAllText() {
                setPage(1);
                textViewMinChar.setVisibility(INVISIBLE);
                noDataStoreFoundView.setVisibility(INVISIBLE);
                loadPresenter(LoadType.CLICK_LOAD);
            }
        });
    }

    private void addMultiplierListener() {
        for (RelativeLayout tabStore : tabStoreMultipliers) {
            tabStore.setOnClickListener(multiplierView -> {
                setPage(1);
                MultiplierMenuManager.activeMultiplierMenu(linearLayoutMultiplier, multiplierView.getId());
                multiplierActive = MultiplierMenuManager.getMultiplier(multiplierView.getId());
                merchantTier = MultiplierMenuManager.getMerchantTier(multiplierView.getId());
                setActiveStore();
                if (storeActive == StoreActive.NEARBY_STORE) {
                    loadPresenter(LoadType.CLICK_LOAD);
                } else if (storeActive == StoreActive.SEARCH_STORE) {
                    loadPresenter(LoadType.SEARCH_LOAD);
                }
            });
        }
    }

    private void addViewInactiveLocationListener() {
        viewInactiveLocation.setOnClickListener(view -> {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            getBaseActivity().startActivityForResult(intent, ConfigManager.Store.LOCATION_RC);
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.FRAGMENT_STORES_NEARBY,
                    ConfigManager.Analytic.Action.CLICK,
                    "Empty State Location");
        });
    }

    @Override
    public void showNoInternetConnection() {
        super.showNoInternetConnection();
        dismissProgressBar();
    }

    private void initView() {
        searchView.setHint(getContext().getString(R.string.fs_txt_search));
        storeAdapter = new StoreAdapter(getBaseActivity(), new ArrayList<>());
        linearLayoutManager = new LinearLayoutManager(getContext());
        rvStore.setLayoutManager(linearLayoutManager);
        rvStore.setAdapter(storeAdapter);
        multiplierLineAll.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.base_pink));
        storeAdapter.setOnClickItemListener(store ->
                getAnalyticManager().trackEvent(
                        ConfigManager.Analytic.Category.FRAGMENT_STORES_NEARBY,
                        ConfigManager.Analytic.Action.ITEM_CLICK,
                        "Item Store ".concat(store.getName()))
        );
    }

    private void initListener() {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (!isLoading) {
                setLoading(true);
                setPage(1);
                setGpsListener();
            } else {
                swipeRefreshLayout.setRefreshing(false);
            }
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.FRAGMENT_STORES_NEARBY,
                    ConfigManager.Analytic.Action.REFRESH,
                    "Swipe Refresh");
        });
        rvStore.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                setPage(page + 1);
                loadPresenter(LoadType.SCROLL_LOAD);
            }

            @Override
            public int getTotalPageCount() {
                return StoreNearbyView.this.storeAdapter.getStores().size();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    public void setGpsListener() {
        if (!storeNearbyPresenter.isGpsEnable()) {
            viewInactiveLocation.setVisibility(VISIBLE);
            rvStore.setVisibility(GONE);
            noInternetConnectionView.setVisibility(GONE);
            noDataStoreFoundView.setVisibility(GONE);
            swipeRefreshLayout.setRefreshing(false);
            setLoading(false);
        } else {
            viewInactiveLocation.setVisibility(GONE);
            loadPresenter(LoadType.SWIPE_LOAD);
        }
    }

    @Override
    public void invisibleListStoreView() {
        rvStore.setVisibility(INVISIBLE);
    }

    @Override
    public void visibleListStoreView() {
        rvStore.setVisibility(VISIBLE);
        noDataStoreFoundView.setVisibility(GONE);
    }

    @Override
    public void addViewStores(StoreResults storeResults, LoadType loadType) {
        List<Store> stores = storeResults.getStores();
        isLastPage = !storeResults.getMore();
        if (!storeResults.getStores().isEmpty() || storeResults.getStores() != null) {
            switch (loadType) {
                case CLICK_LOAD: {
                    replaceStore(stores);
                    break;
                }
                case SCROLL_LOAD: {
                    storeAdapter.addStores(stores);
                    break;
                }
                case SEARCH_LOAD: {
                    replaceStore(stores);
                    break;
                }
                case SWIPE_LOAD: {
                    replaceStore(stores);
                    break;
                }
            }
        }
    }

    @Override
    public void failLoadViewStores(LoadType loadType, Throwable e) {
        switch (loadType) {
            case CLICK_LOAD: {
                setFailLoad(e);
                invisibleListStoreView();
                break;
            }
            case SCROLL_LOAD: {
                NegativeScenarioManager.show(e, this, NegativeScenarioManager.NegativeView.NOTIFICATION);
                break;
            }
            case SEARCH_LOAD: {
                setFailLoad(e);
                invisibleListStoreView();
                break;
            }
            case SWIPE_LOAD: {
                setFailLoad(e);
                invisibleListStoreView();
                break;
            }
        }
    }

    private void setFailLoad(Throwable e) {
        rvStore.setVisibility(GONE);
        if (e instanceof IOException) {
            noInternetConnectionView.setVisibility(VISIBLE);
        } else {
            //Do nothing
        }
    }

    private void replaceStore(List<Store> stores) {
        rvStore.smoothScrollToPosition(0);
        storeAdapter.replaceStores(stores);
        if (storeAdapter.isEmpty()) {
            noDataStoreFoundView.setVisibility(VISIBLE);
            rvStore.setVisibility(INVISIBLE);
            if (storeActive == StoreActive.SEARCH_STORE) {
                noDataStoreFoundView.setSearchView();
            } else {
                noDataStoreFoundView.setNearView();
            }
        } else {
            noDataStoreFoundView.setVisibility(INVISIBLE);
            rvStore.setVisibility(VISIBLE);
        }
    }

    public void loadPresenter(LoadType loadType) {
        setActiveStore();
        switch (storeActive) {
            case NEARBY_STORE:
                storeNearbyPresenter.loadNearStores(
                        StoreHelper.getStoresBody(merchantTier, multiplierActive, page), loadType);
                break;
            case SEARCH_STORE: {
                if (keyword.length() > 2)
                    storeNearbyPresenter.loadSearchStore(
                            StoreHelper.getStoreSearchBody(keyword, multiplierActive, page), loadType);
                break;
            }
        }
    }

    private void setActiveStore() {
        if (searchView.isEmpty()) {
            storeActive = StoreActive.NEARBY_STORE;
        } else {
            storeActive = StoreActive.SEARCH_STORE;
        }
    }

    @Override
    public void showProgressBarCircle() {
        progressBarCircle.setVisibility(VISIBLE);
    }

    @Override
    public void dismissProgressBarCircle() {
        progressBarCircle.setVisibility(INVISIBLE);
    }

    public StoreAdapter getStoreAdapter() {
        return storeAdapter;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public StoreNearbyPresenter getStoreNearbyPresenter() {
        return storeNearbyPresenter;
    }
}