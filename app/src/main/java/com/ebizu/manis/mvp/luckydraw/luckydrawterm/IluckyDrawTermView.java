package com.ebizu.manis.mvp.luckydraw.luckydrawterm;

import com.ebizu.manis.model.LuckyDrawTerm;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public interface IluckyDrawTermView extends IBaseView {

    void setLuckyDrawTerm(LuckyDrawTerm luckyDrawTerm);

    ILuckyDrawTermPresenter getLuckyDrawTermPresenter();
}
