package com.ebizu.manis.mvp.home.reward;

import android.content.Context;

import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.mvp.home.HomeFragment;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.service.reward.RewardApi;
import com.ebizu.manis.service.reward.RewardApiGenerator;
import com.ebizu.manis.service.reward.requestbody.RewardGeneralListBody;
import com.ebizu.manis.service.reward.response.ResponseRewardApi;
import com.ebizu.manis.service.reward.response.WrapperRewardVoucherList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by FARIS_mac on 11/10/17.
 */

public class RewardVoucherHomeHomePresenter implements IRewardVoucherHomePresenter {

    private Context context;
    private RewardApi rewardApi;

    public RewardVoucherHomeHomePresenter(Context context) {
        this.context = context;
    }

    @Override
    public void loadRewardVoucher(RewardGeneralListBody rewardGeneralListBody, HomeFragment homeFragment) {
        if (rewardApi == null)
            rewardApi = RewardApiGenerator.createService(context);
        rewardApi.getRewardVoucherList(rewardGeneralListBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperRewardVoucherList>(homeFragment) {
                    @Override
                    public void onNext(WrapperRewardVoucherList wrapperRewardVoucherList) {
                        super.onNext(wrapperRewardVoucherList);
                        homeFragment.setRewardHome(wrapperRewardVoucherList.getData().getRewardVoucherList());
                    }

                    @Override
                    public void onFailure(ResponseRewardApi responseRewardApi) {
                        super.onFailure(responseRewardApi);
                        noConnection(homeFragment);
                    }
                });
    }

    public void noConnection(HomeFragment homeFragment){
        if(ConnectionDetector.isNetworkConnected(context)) {
            homeFragment.noServer("server busy");
        } else{
            homeFragment.noConnectionReward();
        }
    }

}
