package com.ebizu.manis.mvp.store.storecategorydetail;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.InterestsStore;
import com.ebizu.manis.model.StoreResults;
import com.ebizu.manis.mvp.snap.SnapActivity;
import com.ebizu.manis.mvp.store.storenearby.PaginationScrollListener;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.StoreInterestBody;
import com.ebizu.manis.view.adapter.StoreAdapter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by firef on 7/13/2017.
 */

public class StoreCategoryDetailActivity extends SnapActivity implements IStoreCategoryDetail,
        SwipeRefreshLayout.OnRefreshListener, AppBarLayout.OnOffsetChangedListener {

    @Inject
    Context context;

    @Inject
    ManisApi manisApi;

    @BindView(R.id.sd_toolbar)
    Toolbar toolbar;
    @BindView(R.id.sd_collapsing)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.rv_stores)
    RecyclerView rvStores;
    @BindView(R.id.sd_img_background)
    ImageView sdImgBackground;
    @BindView(R.id.sd_txt_category)
    TextView sdTxtCategory;
    @BindView(R.id.sd_txt_category_shadow)
    TextView sdTxtCategoryShadow;
    @BindView(R.id.swipe_store)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.sd_appbarlayout)
    AppBarLayout appBarLayout;

    private StoreCategoryDetailPresenter storeCategoryDetailPresenter;
    private InterestsStore interestsStore;
    private StoreAdapter storeAdapter;
    private StoreInterestBody storeInterestBody;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int lastChange;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_category_detail);
        ButterKnife.bind(this);
        initView();
        storeCategoryDetailPresenter = new StoreCategoryDetailPresenter(this);
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                performBackAnimation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initView() {
        Bundle extra = getIntent().getExtras();
        StoreInterestBody storeInterestBody = new StoreInterestBody();
        if (extra != null) {
            storeInterestBody.setId(extra.getInt(ConfigManager.Store.INTEREST_STORE_ID));
            storeInterestBody.setPage(1);
            storeInterestBody.setSize(ConfigManager.Store.MAX_PAGE_SIZE);
            InterestsStore interestsStore = extra.getParcelable(ConfigManager.Store.INTEREST_STORE);
            setViewHeader(interestsStore, storeInterestBody);
            initRecyclerView();
            initListener();
            initActionBar();
        }
    }

    private void setViewHeader(InterestsStore interestsStore, StoreInterestBody storeInterestBody) {
        this.storeInterestBody = storeInterestBody;
        this.interestsStore = interestsStore;
        Glide.with(this)
                .load(interestsStore.getAssets().getPhoto())
                .centerCrop()
                .placeholder(R.drawable.default_pic_store_logo)
                .into(sdImgBackground);
        sdTxtCategory.setText(interestsStore.getName());
        sdTxtCategoryShadow.setText(interestsStore.getName());
    }

    private void performBackAnimation() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void initActionBar() {
        collapsingToolbarLayout.setTitle(interestsStore.getName());
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.navigation_back_btn_white);

        sdTxtCategory.setText(interestsStore.getName());
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (lastChange != verticalOffset) {
            sdTxtCategory.setWidth((int) (sdTxtCategoryShadow.getWidth() + (float) (-1 * verticalOffset) / appBarLayout.getTotalScrollRange() * sdTxtCategoryShadow.getX() * 2));
        }
        lastChange = verticalOffset;
    }

    private void initRecyclerView() {
        storeAdapter = new StoreAdapter(this, new ArrayList<>());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvStores.setLayoutManager(linearLayoutManager);
        rvStores.setAdapter(storeAdapter);
        rvStores.setVisibility(View.VISIBLE);
        initCategoryPaging((LinearLayoutManager) rvStores.getLayoutManager());
    }

    private void initCategoryPaging(LinearLayoutManager linearLayoutManager) {
        rvStores.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                storeInterestBody.setPage(storeInterestBody.getPage() + 1);
                new Handler().postDelayed(() -> storeCategoryDetailPresenter.loadStoreCategoryDetailPaging(
                        ManisApiGenerator.createServiceWithToken(StoreCategoryDetailActivity.this), storeInterestBody), 1000);
            }

            @Override
            public int getTotalPageCount() {
                return storeInterestBody.getSize();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        new Handler().postDelayed(() -> storeCategoryDetailPresenter.loadStoreCategoryDetail(
                ManisApiGenerator.createServiceWithToken(this), storeInterestBody), 1000);
    }

    private void initListener() {
        swipeRefreshLayout.setOnRefreshListener(this);
        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    public void loadStoreCategory(StoreResults storeResults, StoreInterestBody storeInterestBody) {
        storeAdapter.clearStores();
        this.storeInterestBody = storeInterestBody;
        storeAdapter.addAll(storeResults.getStores());
        rvStores.setVisibility(View.VISIBLE);
        if (storeResults.getMore()) {
            storeAdapter.addLoadingFooter();
        } else {
            isLastPage = true;
        }
    }

    @Override
    public void loadStoreCategoryPaging(StoreResults storeResults, StoreInterestBody storeInterestBody) {
        storeAdapter.removeLoadingFooter();
        isLoading = false;
        this.storeInterestBody = storeInterestBody;
        storeAdapter.addAll(storeResults.getStores());
        if (storeResults.getMore()) {
            storeAdapter.addLoadingFooter();
        } else {
            isLastPage = true;
        }
    }

    @Override
    public void onRefresh() {
        storeInterestBody.setPage(1);
        storeCategoryDetailPresenter.loadStoreCategoryDetail(ManisApiGenerator.createServiceWithToken(this), storeInterestBody);
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
