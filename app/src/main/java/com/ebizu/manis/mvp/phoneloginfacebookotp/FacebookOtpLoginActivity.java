package com.ebizu.manis.mvp.phoneloginfacebookotp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.LoginChoices;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.launcher.LauncherManager;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.mvp.login.LoginActivity;
import com.ebizu.manis.mvp.phonelogin.PhoneOtpActivity;
import com.ebizu.manis.root.BaseActivity;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.SkinManager;
import com.facebook.accountkit.ui.UIManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by FARIS_mac on 11/1/17.
 */

public class FacebookOtpLoginActivity extends PhoneOtpActivity {

    private static final int FRAMEWORK_REQUEST_CODE = 1;
    private int nextPermissionsRequestCode = 4000;
    private final Map<Integer, OnCompleteListener> permissionsListeners = new HashMap<>();
    private String[] whiteList = {"ID", "MY"};
    private UIManager uiManager;
    private ConfirmCodeFacebookPresenter confirmCodeFacebookOtpPresenter;

    private interface OnCompleteListener {
        void onComplete();
    }

    public static void start(BaseActivity activity, int requestCode) {
        Intent intent = new Intent(activity, FacebookOtpLoginActivity.class);
        intent.putExtra(ConfigManager.Otp.REQUEST_CODE, requestCode);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        confirmCodeFacebookOtpPresenter = new ConfirmCodeFacebookPresenter(this, requestCode);
        confirmCodeFacebookOtpPresenter.attachView(this);
        loginOtp(LoginType.PHONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != FRAMEWORK_REQUEST_CODE) {
            return;
        }
        final AccountKitLoginResult loginResult = AccountKit.loginResultWithIntent(data);
        if (loginResult == null || loginResult.wasCancelled()) {
            finish();
        } else if (loginResult.getError() != null) {
            Log.e(getClass().getSimpleName(), loginResult.getError().getErrorType().getMessage());
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            final AccessToken accessToken = loginResult.getAccessToken();
            if (accessToken != null) {
                showProgressBarDialog(getString(R.string.loading));
                signInWithFacebookOtp();
            } else {
                Log.e(getClass().getSimpleName(), "onActivityResult: " + "Unknown response type");
            }
        }
    }

    private void signInWithFacebookOtp() {
        confirmCodeFacebookOtpPresenter.signInWithFacebookOtp(this);
    }

    public void loginOtp(final LoginType loginType) {
        uiManager = new SkinManager(
                SkinManager.Skin.CLASSIC,
                ContextCompat.getColor(this, R.color.colorAccent));

        final Intent intent = new Intent(this, AccountKitActivity.class);
        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = new AccountKitConfiguration.AccountKitConfigurationBuilder(
                loginType, AccountKitActivity.ResponseType.TOKEN);
        configurationBuilder.setSMSWhitelist(whiteList);
        configurationBuilder.setUIManager(uiManager);
        final AccountKitConfiguration configuration = configurationBuilder.build();
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configuration);
        OnCompleteListener completeListener = () -> startActivityForResult(intent, FRAMEWORK_REQUEST_CODE);
        switch (loginType) {
            case PHONE:
                if (configuration.isReceiveSMSEnabled() && !canReadSmsWithoutPermission()) {
                    final OnCompleteListener receiveSMSCompleteListener = completeListener;
                    completeListener = () -> requestPermissions(
                            Manifest.permission.RECEIVE_SMS,
                            R.string.permissions_receive_sms_title,
                            R.string.permissions_receive_sms_message,
                            receiveSMSCompleteListener);
                }
                if (configuration.isReadPhoneStateEnabled() && !isGooglePlayServicesAvailable()) {
                    final OnCompleteListener readPhoneStateCompleteListener = completeListener;
                    completeListener = () -> requestPermissions(
                            Manifest.permission.READ_PHONE_STATE,
                            R.string.permissions_read_phone_state_title,
                            R.string.permissions_read_phone_state_message,
                            readPhoneStateCompleteListener);
                }
                break;
        }
        completeListener.onComplete();
    }

    private boolean isGooglePlayServicesAvailable() {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int googlePlayServicesAvailable = apiAvailability.isGooglePlayServicesAvailable(this);
        return googlePlayServicesAvailable == ConnectionResult.SUCCESS;
    }

    private boolean canReadSmsWithoutPermission() {
        requestSmsPermission();
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int googlePlayServicesAvailable = apiAvailability.isGooglePlayServicesAvailable(this);
        if (googlePlayServicesAvailable == ConnectionResult.SUCCESS) {
            return true;
        }
        //TODO we should also check for Android O here t18761104

        return false;
    }

    private void requestSmsPermission() {
        String permission = Manifest.permission.READ_SMS;
        int grant = ContextCompat.checkSelfPermission(this, permission);
        if (grant != PackageManager.PERMISSION_GRANTED) {
            String[] permission_list = new String[1];
            permission_list[0] = permission;
            ActivityCompat.requestPermissions(this, permission_list, 1);
        }
    }

    private void requestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final OnCompleteListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        checkRequestPermissions(
                permission,
                rationaleTitleResourceId,
                rationaleMessageResourceId,
                listener);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkRequestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final OnCompleteListener listener) {
        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        final int requestCode = nextPermissionsRequestCode++;
        permissionsListeners.put(requestCode, listener);

        if (shouldShowRequestPermissionRationale(permission)) {
            new AlertDialog.Builder(this)
                    .setTitle(rationaleTitleResourceId)
                    .setMessage(rationaleMessageResourceId)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> requestPermissions(new String[]{permission}, requestCode))
                    .setNegativeButton(android.R.string.no, (dialog, which) -> {
                        // ignore and clean up the listener
                        permissionsListeners.remove(requestCode);
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            requestPermissions(new String[]{permission}, requestCode);
        }
    }

    @TargetApi(23)
    @SuppressWarnings("unused")
    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           final @NonNull String permissions[],
                                           final @NonNull int[] grantResults) {
        final OnCompleteListener permissionsListener = permissionsListeners.remove(requestCode);
        if (permissionsListener != null
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissionsListener.onComplete();
        }
    }

    public void onSuccessSignUpOtp(Account account, String phoneNumber) {
        onNextActivity(account, phoneNumber);
    }

    public void onNextActivity(Account account, String phoneNumber) {
        getManisSession().setSession(account);
        confirmCodeFacebookOtpPresenter.updateCountrySession(phoneNumber);
        confirmCodeFacebookOtpPresenter.updateOtpSession(phoneNumber);
        UtilManis.setIntercomSdk(getApplication(), account, regIdGcm, LoginChoices.PHONE);
        UtilManis.setEbizuCampaign(getApplicationContext(), account, LoginChoices.PHONE);
        UtilManis.setBranchSDK(getApplicationContext(), account);
        Intent intent = LauncherManager.getIntentNextLoginPhone(
                this, this, requestCode);
        startActivity(intent);
        dismissProgressBarDialog();
        ActivityCompat.finishAffinity(this);
    }

    public void onSignInComplete(Account account) {
        confirmCodeFacebookOtpPresenter.loginRewardSdk(account, this);
    }

    @Override
    public void finish() {
        if (requestCode == ConfigManager.Otp.SIGN_UP_OTP_REQUEST_CODE) {
            Intent intent = LauncherManager.getIntentNextLoginEmail(
                    this, this);
            ActivityCompat.finishAffinity(this);
            this.startActivity(intent);
        } else {
            super.finish();
        }
    }

}
