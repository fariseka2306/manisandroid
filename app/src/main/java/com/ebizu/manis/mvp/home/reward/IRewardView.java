package com.ebizu.manis.mvp.home.reward;

import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.sdk.reward.models.Response;

import java.util.List;

/**
 * Created by Raden on 7/13/17.
 */

public interface IRewardView {

    void setRedeemHome(final Response.Data<com.ebizu.sdk.reward.models.Reward> redeemHomeData);

    void setRewardHome(List<RewardVoucher> rewardVoucher);

    void noConnectionReward();

    void noServer(String message);

    void noReward(String message);
}
