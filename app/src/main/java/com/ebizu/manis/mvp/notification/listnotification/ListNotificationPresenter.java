package com.ebizu.manis.mvp.notification.listnotification;

import com.ebizu.manis.database.litepal.NotificationDatabase;
import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;

import java.util.List;

/**
 * Created by Ebizu on 8/22/17.
 */

public class ListNotificationPresenter extends BaseViewPresenter implements IListNotificationPresenter {

    private ListNotificationView listNotificationView;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        listNotificationView = (ListNotificationView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        // not implemented method
    }

    @Override
    public void releaseAllSubscribes() {
        // not implemented method
    }

    @Override
    public void getNotificationList() {
        listNotificationView.showProgressBar();
        List<NotificationTableList> notificationTableListList = NotificationDatabase.getInstance().getNotificationList();
        if (!notificationTableListList.isEmpty()) {
            listNotificationView.setNotificationListView(notificationTableListList);
        } else {
            listNotificationView.emptyNotificationListView();
        }
        listNotificationView.dismissProgressBar();
    }

    @Override
    public void deleteNotificationList() {
        NotificationDatabase.getInstance().deleteAll();
    }
}
