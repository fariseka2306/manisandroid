package com.ebizu.manis.mvp.reward.rewarddetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.model.rewardvoucher.Term;
import com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart.PhysicalShoppingCartActivity;
import com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart.RegularShoppingCartActivity;
import com.ebizu.manis.mvp.reward.rewarddetail.rewardbulkdetail.RewardBulkDetailActivity;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.dialog.redeemdialogsuccess.RedeemDialogSuccess;
import com.ebizu.manis.view.dialog.redeemdialogwithpin.RedeemWithPinTncDialog;
import com.ebizu.sdk.reward.models.Redeem;
import com.ebizu.sdk.reward.models.Reward;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 9/11/17.
 */

public class RewardDetailView extends BaseView implements IRewardDetailView {

    @BindView(R.id.rf_txt_outofstock)
    public TextView txtOutOfStock;
    @BindView(R.id.lin_redeem)
    public LinearLayout linRedeem;
    @BindView(R.id.img_reward)
    ImageView imgReward;

    @BindView(R.id.rf_txt_detail)
    TextView txtDetail;

    @BindView(R.id.txt_price)
    TextView txtPrize;

    @BindView(R.id.txt_point)
    TextView txtPoint;
    @BindView(R.id.lin_buy)
    LinearLayout linBuy;
    @BindView(R.id.rf_ly_tc)
    LinearLayout linTerm;
    private String TAG = getClass().getSimpleName();
    private RewardDetailAbstractActivity rewardDetailActivity;

    private Integer point = null;
    private Integer accountPoint = null;
    private Integer rewardPoint = null;

    private IRewardDetailPresenter iRewardDetailPresenter;
    private RewardVoucher reward;

    public RewardDetailView(Context context) {
        super(context);
        createView(context);
    }

    public RewardDetailView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public RewardDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RewardDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    public void setReward(RewardVoucher reward) {
        this.reward = reward;
        setRewardDetailView(reward);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.reward_detail_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new RewardDetailPresenter(context));
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iRewardDetailPresenter = (IRewardDetailPresenter) iBaseViewPresenter;
        iRewardDetailPresenter.attachView(this);
    }

    @Override
    public void loadReward(Reward reward) {
    }

    @Override
    public IRewardDetailPresenter getRewardDetailPresenter() {
        return iRewardDetailPresenter;
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        rewardDetailActivity = (RewardDetailAbstractActivity) baseActivity;
    }

    public void setRewardDetailView(RewardVoucher reward) {
        rewardPoint = reward.getPoint();
        ImageUtils.loadImage(getContext(), reward.getImage128(), ContextCompat.getDrawable(getContext(), R.drawable.default_pic_rewards_tile_img), imgReward);
        txtDetail.setText(reward.getDescription());
        txtPrize.setText(reward.getCurrency() + " " + (int) reward.getVoucherPurchaseValue());
        String point = UtilManis.longToLocaleNumberFormat(rewardPoint);
        String pts = " " + getContext().getString(R.string.rd_txt_pts);
        String finalString = point + pts;
        Spannable sbPoints = new SpannableString(finalString);
        sbPoints.setSpan(new StyleSpan(Typeface.BOLD), point.length(), finalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtPoint.setText(sbPoints);
        generateTnC(reward.getTerms());
        showVoucherType(reward);
    }

    private void showVoucherType(RewardVoucher reward) {
        // free voucher
        if (reward.getPoint() == 0 &&
                (reward.getVoucherTransactionType().equalsIgnoreCase(Reward.VOUCHER_TRANSACTION_REDEEMABLE) ||
                        reward.getVoucherTransactionType().equalsIgnoreCase(Reward.VOUCHER_TRANSACTION_BOTH) ||
                        reward.getVoucherTransactionType().equals("")) && reward.getVoucherPurchaseValue() == 0) {
            linRedeem.setVisibility(VISIBLE);
            linBuy.setVisibility(INVISIBLE);
        }

        // redeemable only
        else if (reward.getPoint() >= 0 && reward.getVoucherPurchaseValue() == 0 &&
                reward.getVoucherTransactionType().equalsIgnoreCase(Reward.VOUCHER_TRANSACTION_REDEEMABLE)) {
            linBuy.setVisibility(INVISIBLE);
        }

        // purchaseable only
        else if (reward.getPoint() == 0 &&
                reward.getVoucherTransactionType().equalsIgnoreCase(Reward.VOUCHER_TRANSACTION_PURCHASABLE) &&
                reward.getVoucherPurchaseValue() > 0) {
            linRedeem.setVisibility(INVISIBLE);
        }

        //both
        else if (reward.getPoint() > 0 &&
                reward.getVoucherPurchaseValue() > 0) {
            linRedeem.setVisibility(VISIBLE);
            linBuy.setVisibility(VISIBLE);
        } else {
            linRedeem.setVisibility(INVISIBLE);
            linBuy.setVisibility(INVISIBLE);
        }
    }

    private void generateTnC(List<Term> terms) {
        linTerm.removeAllViews();
        for (Term term : terms) {
            View view = getBaseActivity().getLayoutInflater().inflate(R.layout.item_term_reward_detail, linTerm, false);
            TextView txtTerm = (TextView) view.findViewById(R.id.txt_terms);
            txtTerm.setText(term.getText());
            linTerm.addView(view);
        }
    }

    public void setViewRedeem(String refcode) {
        Log.i(TAG, "setViewRedeem: refCode" + refcode);
        getRewardDetailPresenter().setRewardUser(refcode, reward);
    }

    public void showDialogSuccess(Redeem response) {
        getBaseActivity().dismissProgressBarDialog();
        RedeemDialogSuccess redeemDialogSuccess = new RedeemDialogSuccess(getContext());
        redeemDialogSuccess.setActivity(getBaseActivity());
        redeemDialogSuccess.setRedeem(response);
        redeemDialogSuccess.setReward(reward);
        redeemDialogSuccess.show();
    }

    public void showDialogRedeemWithPin(String refCode) {
        getBaseActivity().dismissProgressBarDialog();
        RedeemWithPinTncDialog redeemWithPinTncDialog = new RedeemWithPinTncDialog(getContext());
        redeemWithPinTncDialog.setActivity(getBaseActivity());
        redeemWithPinTncDialog.setReward(reward);
        redeemWithPinTncDialog.setRefCode(refCode);
        redeemWithPinTncDialog.show();
    }

    public void showUpdateDialog() {
        getBaseActivity().showAlertDialog(
                getContext().getString(R.string.error),
                getContext().getString(R.string.update_message),
                false,
                getContext().getString(R.string.text_ok), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    try {
                        getBaseActivity().startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse(getContext().getString(R.string.market_id) + BuildConfig.APPLICATION_ID)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        getBaseActivity().startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse(getContext().getString(R.string.playstore_id) + BuildConfig.APPLICATION_ID)));
                    }
                });
    }

    @Override
    public void startActivityShoppingCart(ShoppingCart shoppingCart) {
        if (null != rewardDetailActivity && null != reward) {
            if (getBaseActivity() instanceof RewardBulkDetailActivity) {
                if (reward.getBulkType().equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_PHYSICAL)) {
                    rewardDetailActivity.nextPurchaseVoucher(PhysicalShoppingCartActivity.class, reward, shoppingCart);
                } else {
                    showUpdateDialog();
                }
            } else {
                if ((reward.getType().equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_ECODE) && reward.getVoucherRedeemType().equalsIgnoreCase(ConfigManager.Reward.REDEEM_TYPE_PIN)) ||
                        (reward.getType().equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_ETU))) {
                    rewardDetailActivity.nextPurchaseVoucher(RegularShoppingCartActivity.class, reward, shoppingCart);
                } else if (reward.getType().equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_PHYSICAL)) {
                    rewardDetailActivity.nextPurchaseVoucher(PhysicalShoppingCartActivity.class, reward, shoppingCart);
                } else {
                    showUpdateDialog();
                }
            }
        }
    }
}
