package com.ebizu.manis.mvp.luckydraw.luckydrawguide;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.mvp.luckydraw.luckydrawdialog.LuckyDrawDialogPresenter;
import com.ebizu.manis.root.BaseView;

import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class LuckyDrawGuideView extends BaseView {

    public LuckyDrawGuideView(Context context) {
        super(context);
        createView(context);
    }

    public LuckyDrawGuideView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public LuckyDrawGuideView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LuckyDrawGuideView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.lucky_draw_guide_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new LuckyDrawDialogPresenter());
    }
}
