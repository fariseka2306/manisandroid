package com.ebizu.manis.mvp.snap.receipt.camera;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.mvp.snap.receipt.upload.ReceiptUploadActivity;
import com.ebizu.manis.view.camera.TouchImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmActivity extends ReceiptCameraActivity {

    @BindView(R.id.touchIV_confirm_receipt)
    TouchImageView imageViewReceipt;
    @BindView(R.id.textView_snap_retake)
    TextView textViewRetake;
    @BindView(R.id.textView_snap_finish)
    TextView textViewFinish;
    @BindView(R.id.layout_snap_add)
    LinearLayout layoutSnapAdd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_snap);
        ButterKnife.bind(this);
        setImageViewReceipt();
    }

    private void setImageViewReceipt() {
        if (bitmapsList == 5) {
            Bitmap bitmapReceipt = ImageUtils.getBitmapFile(ConfigManager.Snap.URI_RECEIPT_PATH +
                    ConfigManager.Snap.URI_RECEIPT_FILE_BITMAP.concat(String.valueOf(bitmapId)));
            imageViewReceipt.setImageBitmap(bitmapReceipt);
            layoutSnapAdd.setVisibility(View.INVISIBLE);
        } else {
            Bitmap bitmapReceipt = ImageUtils.getBitmapFile(ConfigManager.Snap.URI_RECEIPT_PATH +
                    ConfigManager.Snap.URI_RECEIPT_FILE_BITMAP.concat(String.valueOf(bitmapId)));
            imageViewReceipt.setImageBitmap(bitmapReceipt);
        }
    }

    @OnClick(R.id.textView_snap_retake)
    void snapRetake() {
        retakeLongReceipt();
    }

    @OnClick(R.id.textView_snap_finish)
    void snapFinish() {
        nextActivity(ReceiptUploadActivity.class, bitmapId, receiptDataBody, bitmapsList);
    }

    @OnClick(R.id.layout_snap_add)
    void snapAddSection() {
        nextLongReceipt();
    }
}
