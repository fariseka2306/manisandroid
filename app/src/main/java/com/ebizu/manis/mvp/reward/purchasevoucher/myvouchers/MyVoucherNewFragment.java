package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 11/13/17.
 */

public class MyVoucherNewFragment extends BaseFragment {

    @Inject
    Context context;

    @BindView(R.id.my_voucher_views)
    MyVouchersView myVouchersView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_voucher_available,container,false);
        ButterKnife.bind(this, view);
        myVouchersView.setActivity((BaseActivity) getActivity());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initialize();
    }

    private void initialize(){
       myVouchersView.attachPresenter(new MyVouchersPresenter());
    }


}
