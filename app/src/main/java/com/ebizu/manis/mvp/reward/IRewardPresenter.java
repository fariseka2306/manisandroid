package com.ebizu.manis.mvp.reward;

import com.ebizu.manis.root.IBaseViewPresenter;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public interface IRewardPresenter extends IBaseViewPresenter {

    void loadCategoryImage();

    void loadRewardCategory(String categoryName);
}
