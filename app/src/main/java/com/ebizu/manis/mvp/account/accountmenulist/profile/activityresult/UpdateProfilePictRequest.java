package com.ebizu.manis.mvp.account.accountmenulist.profile.activityresult;

import android.content.Intent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileActivity;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 10/13/17.
 */

public class UpdateProfilePictRequest extends ProfileRequestCode
        implements IProfileRequest {

    @BindView(R.id.profile_view)
    ProfileView profileView;

    public UpdateProfilePictRequest(ProfileActivity profileActivity) {
        super(profileActivity);
    }

    @Override
    public int requestCode() {
        return ConfigManager.UpdateProfile.CHANGE_PROFILE_PICTURE_REQ_CODE;
    }

    @Override
    public void doRequest(int resultCode, Intent data) {
        super.doRequest(resultCode, data);
        if (resultCode == ConfigManager.UpdateProfile.SUCCESS_UPDATE_PICTURE_RES_CODE) {
            ButterKnife.bind(this, profileActivity);
            profileView.updatePhotoProfile(profileActivity.getManisSession().getAccountSession());
        }
    }
}
