package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.model.PurchaseHistory;
import com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistoryfactory.PurchaseHistoryFactory;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 9/18/17.
 */

public class PurchasedVoucherDialog extends BaseDialogManis {

    @BindView(R.id.image_view_merchant)
    ImageView imageViewMerchant;

    @BindView(R.id.text_view_title_transaction)
    TextView textViewTitleTransaction;

    @BindView(R.id.text_view_trans_time)
    TextView textViewTransTime;

    @BindView(R.id.text_view_trans_date)
    TextView textViewTransDate;

    @BindView(R.id.text_view_voucher_name)
    TextView textViewVoucherName;

    @BindView(R.id.text_view_store_name)
    TextView textViewStoreName;

    @BindView(R.id.text_view_amount)
    TextView textViewAmount;

    @BindView(R.id.text_view_order_id)
    TextView textViewOrderId;

    @BindView(R.id.text_view_method_payment)
    TextView textViewMethodPayment;

    @BindView(R.id.text_view_voucher_code)
    TextView textViewVoucherCode;

    @BindView(R.id.text_view_email)
    TextView textViewEmail;

    @BindView(R.id.text_view_status)
    TextView textViewStatus;

    @BindView(R.id.image_view_status)
    ImageView imageViewStatus;

    @BindView(R.id.button_close)
    Button buttonClose;

    private PurchaseHistoryFactory purchaseHistoryFactory;

    public PurchasedVoucherDialog(@NonNull Context context) {
        super(context);
        createView(context);
    }

    public PurchasedVoucherDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        createView(context);
    }

    protected PurchasedVoucherDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        createView(context);
    }


    private void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_purchase_history, null, false);
        ButterKnife.bind(this, view);
        getParent().addView(view, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    public void setPurchaseHistoryView(PurchaseHistory purchaseHistory) {
        purchaseHistoryFactory = new PurchaseHistoryFactory(purchaseHistory, getContext());
        Glide.with(getContext())
                .load(purchaseHistory.getVoucherIcon())
                .placeholder(ContextCompat.getColor(getContext(), R.color.warm_grey))
                .into(imageViewMerchant);
        textViewTitleTransaction.setText(purchaseHistory.getPaidTitle());
        textViewTransTime.setText(purchaseHistory.getTransactionTime());
        textViewTransDate.setText(purchaseHistory.getTransactionTime());
        textViewVoucherName.setText(purchaseHistory.getVoucherName());
        textViewStoreName.setText(purchaseHistory.getPurchaseHistoryBrand().getName());
        textViewAmount.setText(purchaseHistory.getCurrency().concat(String.valueOf(purchaseHistory.getAmount())));
        textViewOrderId.setText(purchaseHistory.getOrderId());
        textViewMethodPayment.setText(purchaseHistory.getPaymentType());
        textViewVoucherCode.setText(purchaseHistory.getVoucherCode());
        textViewEmail.setText(purchaseHistory.getPurchaseHistoryCustomer().getEmail());
        textViewStatus.setText(purchaseHistory.getPaymentStatus());
        buttonClose.setOnClickListener(view -> dismiss());
        setPurchaseStatus();
    }

    private void setPurchaseStatus() {
        textViewStatus.setTextColor(purchaseHistoryFactory.getColor());
        imageViewStatus.setImageDrawable(purchaseHistoryFactory.getImageDrawable());
    }
}