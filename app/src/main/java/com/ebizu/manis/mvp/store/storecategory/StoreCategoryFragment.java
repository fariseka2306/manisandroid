package com.ebizu.manis.mvp.store.storecategory;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.service.manis.ManisApiGenerator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by firef on 7/5/2017.
 */

public class StoreCategoryFragment extends BaseFragment {


    @BindView(R.id.store_category_view)
    StoreCategoryView storeCategoryView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_store_category, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible && getContext() != null)
            getAnalyticManager().trackScreenView(ConfigManager.Analytic.ScreenView.FRAGMENT_STORE_CATEGORIES);
    }

    private void initView() {
        storeCategoryView.setActivity((BaseActivity) getActivity());
        storeCategoryView.attachPresenter(new StoreCategoryPresenter(getContext()));
        storeCategoryView.getStoreCategoryPresenter().loadInterestData(ManisApiGenerator.createServiceWithToken(getContext()));
    }

}
