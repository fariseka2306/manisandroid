package com.ebizu.manis.mvp.store.storedetailmore;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.StoreDetail;
import com.ebizu.manis.root.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 7/22/17.
 */

public class StoreDetailMoreView extends BaseView {

    @BindView(R.id.sd_img_background)
    ImageView sdImgBackground;

    @BindView(R.id.sd_txt_category)
    TextView sdTxtCategory;

    @BindView(R.id.sd_img_merchant)
    ImageView sdImgMerchant;

    @BindView(R.id.business_info_description)
    TextView businessInfoDescription;

    public StoreDetailMoreView(Context context) {
        super(context);
        createView(context);
    }

    public StoreDetailMoreView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public StoreDetailMoreView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public StoreDetailMoreView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.store_detail_more_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
    }

    public void setView(Store store, StoreDetail storeDetail) {
        sdTxtCategory.setText(store.getCategory().getName());
        Glide.with(getContext())
                .load(store.getAssets().getBanner())
                .placeholder(R.drawable.default_pic_store_banner)
                .into(sdImgBackground);
        Glide.with(getContext())
                .load(store.getAssets().getPhoto())
                .placeholder(R.drawable.default_pic_store_logo)
                .into(sdImgMerchant);
        businessInfoDescription.setText(storeDetail.getDescription());
    }
}
