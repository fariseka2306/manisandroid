package com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.fragment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.SnapGuideActivity;

import java.util.HashMap;

public class SnapGuideThirdFragment extends Fragment {

    private boolean isCreated = false;
    private AnimatorSet mAnimatorSet;

    // Third Screen
    private TextView textViewTitleThird;
    private TextView textViewDetailThird;
    private TextView textViewDetailThirdChild;
    private View viewOvalWhiteThird;
    private View viewOvalBlueThird;
    private ImageView imageViewBaseThird;
    private ImageView imageViewReceiptThird;
    private ImageView imageViewHandphoneThird;
    private Button buttonNextThird;
    private TextView textViewExplanationThird;

    private HashMap<View, Float> mOriginalXValuesMap = new HashMap<>();
    private HashMap<View, Float> mOriginalYValuesMap = new HashMap<>();
    private SnapGuideActivity snapGuideActivity;

    private View.OnClickListener nextListener = view -> snapGuideActivity.switchToFragment(3);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        snapGuideActivity = (SnapGuideActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_snapguide_third, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isCreated = true;
        Bundle args = getArguments();
        int position = args.getInt(UtilStatic.ARG_POSITION);
        view.setTag(position);
    }


    private void initView(View rootView) {
        textViewTitleThird = (TextView) rootView.findViewById(R.id.textview_title);
        textViewDetailThird = (TextView) rootView.findViewById(R.id.textview_detail);
        viewOvalWhiteThird = rootView.findViewById(R.id.view_oval_white);
        viewOvalBlueThird = rootView.findViewById(R.id.view_oval_blue);
        textViewDetailThirdChild = (TextView) rootView.findViewById(R.id.textview_detail_child);
        imageViewBaseThird = (ImageView) rootView.findViewById(R.id.imageview_base);
        imageViewReceiptThird = (ImageView) rootView.findViewById(R.id.imageview_receipt);
        imageViewHandphoneThird = (ImageView) rootView.findViewById(R.id.imageview_handphone);
        buttonNextThird = (Button) rootView.findViewById(R.id.button_next);
        textViewExplanationThird = (TextView) rootView.findViewById(R.id.textview_explanation);
        textViewExplanationThird.setText(UtilManis.fromHtml(getString(R.string.snapguide_third_explanation)));
        buttonNextThird.setOnClickListener(nextListener);

        initializeFirstPosition();
        initializeStateView();
        rootView.post(() -> getOriginalYValues());

    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && isCreated) {
            getOriginalYValues();
            doAnimation();
        } else if (!menuVisible && isCreated) {
            mAnimatorSet.end();
            setViewsInOriginalPosition();
        }
    }

    private void getOriginalYValues() {
        mOriginalXValuesMap.put(viewOvalBlueThird, viewOvalBlueThird.getScaleX());
        mOriginalYValuesMap.put(viewOvalBlueThird, viewOvalBlueThird.getScaleY());
        mOriginalYValuesMap.put(imageViewBaseThird, imageViewBaseThird.getY());
        mOriginalYValuesMap.put(imageViewReceiptThird, imageViewReceiptThird.getY());
    }

    private void setViewsInOriginalPosition() {
        viewOvalBlueThird.setScaleX(mOriginalXValuesMap.get(viewOvalBlueThird));
        viewOvalBlueThird.setScaleY(mOriginalYValuesMap.get(viewOvalBlueThird));
        imageViewBaseThird.setY(mOriginalYValuesMap.get(imageViewBaseThird));
        imageViewReceiptThird.setY(mOriginalYValuesMap.get(imageViewReceiptThird));

        initializeStateView();
    }

    private void initializeStateView() {
        textViewTitleThird.setAlpha(0f);
        textViewDetailThird.setAlpha(0f);
        textViewDetailThirdChild.setAlpha(0f);
        imageViewHandphoneThird.setAlpha(0f);
        imageViewHandphoneThird.setRotation(20f);
    }

    private void initializeFirstPosition() {
        viewOvalBlueThird.setScaleX(0.5f);
        viewOvalBlueThird.setScaleY(0.5f);
        imageViewBaseThird.setY(imageViewBaseThird.getY() + getResources().getInteger(R.integer.moveYBaseReceipt));
        imageViewReceiptThird.setY(imageViewReceiptThird.getY() + getResources().getInteger(R.integer.moveYBaseReceipt));
    }


    public void doAnimation() {
        ObjectAnimator fadeTitleFirst = ObjectAnimator.ofFloat(textViewTitleThird, "alpha", 0f, 1f);
        fadeTitleFirst.setDuration(700);

        ObjectAnimator fadeDetailFirst = ObjectAnimator.ofFloat(textViewDetailThird, "alpha", 0f, 1f);
        fadeDetailFirst.setDuration(700);

        ObjectAnimator fadeDetail2First = ObjectAnimator.ofFloat(textViewDetailThirdChild, "alpha", 0f, 1f);
        fadeDetailFirst.setDuration(700);

        ObjectAnimator scaleXoval = ObjectAnimator.ofFloat(viewOvalBlueThird, "scaleX", 1f);
        scaleXoval.setDuration(800);

        ObjectAnimator scaleYoval = ObjectAnimator.ofFloat(viewOvalBlueThird, "scaleY", 1f);
        scaleYoval.setDuration(800);

        ObjectAnimator moveBase = ObjectAnimator.ofFloat(imageViewBaseThird, "y", imageViewBaseThird.getY(), imageViewBaseThird.getY() - getResources().getInteger(R.integer.moveYBaseReceipt));
        moveBase.setDuration(800);

        ObjectAnimator moveReceipt = ObjectAnimator.ofFloat(imageViewReceiptThird, "y", imageViewReceiptThird.getY(), imageViewReceiptThird.getY() - getResources().getInteger(R.integer.moveYBaseReceipt));
        moveReceipt.setDuration(800);

        ObjectAnimator fadeHandphone = ObjectAnimator.ofFloat(imageViewHandphoneThird, "alpha", 0f, 1f);
        fadeHandphone.setDuration(700);

        ObjectAnimator rotateHandphone = ObjectAnimator.ofFloat(imageViewHandphoneThird, "rotation", -15f);
        rotateHandphone.setDuration(700);

        ObjectAnimator rotateHandphone2 = ObjectAnimator.ofFloat(imageViewHandphoneThird, "rotation", 0f);
        rotateHandphone2.setDuration(700);

        mAnimatorSet = new AnimatorSet();
        moveBase.setStartDelay(800);
        moveReceipt.setStartDelay(800);
        fadeHandphone.setStartDelay(1600);
        rotateHandphone.setStartDelay(2100);
        rotateHandphone2.setStartDelay(2800);

        mAnimatorSet.play(fadeTitleFirst)
                .with(fadeDetailFirst)
                .with(fadeDetail2First)
                .with(scaleXoval)
                .with(scaleYoval)
                .with(moveBase)
                .with(moveReceipt)
                .with(fadeHandphone)
                .with(rotateHandphone)
                .with(rotateHandphone2);
        mAnimatorSet.start();
    }
}