package com.ebizu.manis.mvp.luckydraw.luckydrawterm;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.model.LuckyDrawTerm;
import com.ebizu.manis.mvp.luckydraw.luckydrawscreen.LuckyDrawPresenter;
import com.ebizu.manis.root.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class LuckyDrawTermView extends BaseView implements IluckyDrawTermView {

    private ILuckyDrawTermPresenter iLuckyDrawTermPresenter;

    @BindView(R.id.ld_help_title)
    TextView ldHelpTitle;

    @BindView(R.id.ld_help_title_below)
    TextView ldHelpTitleBelow;

    @BindView(R.id.ld_help_logo)
    ImageView ldHelpLogo;

    @BindView(R.id.ld_help_subtitle)
    TextView ldHelpSubtitle;

    @BindView(R.id.tv_desc_prize)
    TextView tvDescPrize;

    public LuckyDrawTermView(Context context) {
        super(context);
        createView(context);
    }

    public LuckyDrawTermView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public LuckyDrawTermView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LuckyDrawTermView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.lucky_draw_help_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new LuckyDrawPresenter(getContext()));
        initView();
    }

    @Override
    public void setLuckyDrawTerm(LuckyDrawTerm luckyDrawTerm) {

    }

    @Override
    public ILuckyDrawTermPresenter getLuckyDrawTermPresenter() {
        return iLuckyDrawTermPresenter;
    }

    private void initView() {
        ldHelpTitleBelow.setVisibility(VISIBLE);
        ldHelpTitle.setText(R.string.ld_submitted_title);
        ldHelpTitleBelow.setText(R.string.ld_submitted_sub_title);
        Glide.with(getContext())
                .load(R.drawable.ticket_icon_white)
                .into(ldHelpLogo);
        ldHelpSubtitle.setText(R.string.ld_submitted_desc);
        tvDescPrize.setText(R.string.ld_submitted_desc_prize);
    }
}
