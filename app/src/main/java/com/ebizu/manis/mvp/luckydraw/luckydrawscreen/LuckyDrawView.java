package com.ebizu.manis.mvp.luckydraw.luckydrawscreen;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.LuckyDrawScreen;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.mvp.luckydraw.luckydrawdialog.LuckyDrawEntryDialog;
import com.ebizu.manis.mvp.luckydraw.luckydrawdialog.LuckyDrawWinnerDialog;
import com.ebizu.manis.mvp.luckydraw.luckydrawhelp.LuckyDrawHelpActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.adapter.luckydraw.LuckyDrawPrizeAdapter;
import com.ebizu.manis.view.linearlayout.FixedLinearLayoutManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerComponentButton;
import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerOriginPageLuckyDraw;
import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerPageLuckyDrawHelpPopup;
import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerPageLuckyDrawTotalTicketsEnteredPopup;
import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerPageLuckyDrawViewOurPastWinnersPopup;
import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerSubFeatureGoldenTicket;
import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerSubFeatureHelp;
import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerSubFeatureTotalTicketsEntered;
import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerSubFeatureViewPastWinners;

/**
 * Created by ebizu on 04/08/17.
 */

public class LuckyDrawView extends BaseView implements ILuckyDrawView {

    private ILuckyDrawPresenter iLuckyDrawPresenter;
    private LuckyDrawPrizeAdapter luckyDrawPrizeAdapter;
    private LuckyDrawFragment luckyDrawFragment;
    private CountDownTimer countDownSearch;
    private boolean isCountDownRunning;

    @BindView(R.id.tv_ld_countdown)
    TextView tvLdCountDown;

    @BindView(R.id.tv_ld_entries)
    TextView tvLdEntries;

    @BindView(R.id.tv_ld_tickets)
    TextView tvLdTickets;

    @BindView(R.id.tv_ld_winner)
    TextView tvLdWinner;

    @BindView(R.id.tv_ld_winner_weekly)
    TextView textViewWinnerWeekly;

    @BindView(R.id.tv_ld_prize_info)
    TextView tvLdPrizeInfo;

    @BindView(R.id.ll_ld_prize)
    LinearLayout llldPrize;

    @BindView(R.id.rv_ld_prizes)
    RecyclerView rvPrizes;


    public LuckyDrawView(Context context) {
        super(context);
        createView(context);
    }

    public LuckyDrawView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public LuckyDrawView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LuckyDrawView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.luckydraw_view, null, false);
        addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
        attachPresenter(new LuckyDrawPresenter(getContext()));
        initView();

    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iLuckyDrawPresenter = (ILuckyDrawPresenter) iBaseViewPresenter;
        iLuckyDrawPresenter.attachView(this);
    }

    @Override
    public void setLuckyDrawScreen(LuckyDrawScreen luckyDrawScreen) {
        tvLdEntries.setText(luckyDrawScreen.getUserTicketsAlltime().toString());
        tvLdTickets.setText(luckyDrawScreen.getUserTicketsCurrentPeriod().toString());
        tvLdWinner.setText(String.format("%s %s", luckyDrawScreen.getLastWinnerPrefix(), luckyDrawScreen.getLastWinnerObject()));
        luckyDrawPrizeAdapter.addPrizes(luckyDrawScreen.getPrizes().getPrizeDetails());
        initCurrency(luckyDrawScreen);
        initWeeklyWinner(luckyDrawScreen);

    }

    @Override
    public ILuckyDrawPresenter getLuckyDrawPresenter() {
        return iLuckyDrawPresenter;
    }

    @OnClick(R.id.btn_ld_help)
    public void onBtnLdHelpClick() {
        getContext().startActivity(new Intent(getContext(), LuckyDrawHelpActivity.class));
        getBaseActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        TrackerManager.getInstance().setTrackerData(
                new TrackerStandartRequest(kTrackerOriginPageLuckyDraw, kTrackerPageLuckyDrawHelpPopup,
                        kTrackerComponentButton, kTrackerSubFeatureHelp, "", "")
        );
    }

    @OnClick(R.id.btn_ld_entries)
    public void onBtnLdEntriesClick() {
        getContext().startActivity(new Intent(getContext(), LuckyDrawEntryDialog.class));
        getBaseActivity().overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        TrackerManager.getInstance().setTrackerData(
                new TrackerStandartRequest(kTrackerOriginPageLuckyDraw, kTrackerPageLuckyDrawTotalTicketsEnteredPopup,
                        kTrackerComponentButton, kTrackerSubFeatureTotalTicketsEntered, "", "")
        );
    }

    @OnClick(R.id.btn_ld_winners)
    public void onBtnLdWinnersClick() {
        getContext().startActivity(new Intent(getContext(), LuckyDrawWinnerDialog.class));
        getBaseActivity().overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        TrackerManager.getInstance().setTrackerData(
                new TrackerStandartRequest(kTrackerOriginPageLuckyDraw, kTrackerPageLuckyDrawViewOurPastWinnersPopup, kTrackerComponentButton, kTrackerSubFeatureViewPastWinners, "", "")
        );
    }

    @OnClick(R.id.iv_ld_ticket)
    public void onClickGoldenTicket() {
        TrackerManager.getInstance().setTrackerData(
                new TrackerStandartRequest(kTrackerOriginPageLuckyDraw, kTrackerOriginPageLuckyDraw,
                        kTrackerComponentButton, kTrackerSubFeatureGoldenTicket, "", "")
        );
    }

    public void attachFragment(LuckyDrawFragment luckyDrawFragment) {
        this.luckyDrawFragment = luckyDrawFragment;
    }

    private void initView() {
        luckyDrawPrizeAdapter = new LuckyDrawPrizeAdapter(getContext(), new ArrayList<>());
        FixedLinearLayoutManager fixedLinearLayoutManager = new FixedLinearLayoutManager(getContext());
        fixedLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvPrizes.setLayoutManager(fixedLinearLayoutManager);
        rvPrizes.setAdapter(luckyDrawPrizeAdapter);
        isCountDownRunning = false;
    }

    private void initCurrency(LuckyDrawScreen luckyDrawScreen) {
        if (luckyDrawScreen != null) {
            if (luckyDrawScreen.getCurrency().getIso3().equalsIgnoreCase("RM")) {
                tvLdPrizeInfo.setText(getContext().getString(R.string.ld_main_txt_prize_info,
                        luckyDrawScreen.getCurrency().getIso3(),
                        String.valueOf(luckyDrawScreen.getTotalPrice())));
            } else {
                tvLdPrizeInfo.setText(getContext().getString(R.string.ld_main_txt_prize_info,
                        luckyDrawScreen.getCurrency().getIso2(),
                        UtilManis.formatMoney(Math.round(luckyDrawScreen.getTotalPrice()))));
            }
        } else {
            llldPrize.setVisibility(INVISIBLE);
        }

    }

    private void initWeeklyWinner(LuckyDrawScreen luckyDrawScreen) {
        if (luckyDrawScreen.getLuckyDrawExpiredDate() != 0) {
            prepareCountDownTimer(luckyDrawScreen);
            if (countDownSearch != null) {
                countDownSearch.cancel();
                if (!isCountDownRunning) countDownSearch.start();
            }
        } else {
            tvLdCountDown.setText(luckyDrawScreen.getLuckyDrawMsgPrefix().concat(" "));
            textViewWinnerWeekly.setText(luckyDrawScreen.getLuckyDrawMsgObject());
            textViewWinnerWeekly.setTypeface(Typeface.DEFAULT_BOLD);
            llldPrize.setVisibility(INVISIBLE);
        }
    }

    public void noConnection() {
        getBaseActivity().showAlertDialog(
                getContext().getString(R.string.error),
                getContext().getString(R.string.error_cannot_load),
                true,
                getContext().getString(R.string.text_ok), ((dialog, which) -> dialog.dismiss()));
    }

    private void prepareCountDownTimer(LuckyDrawScreen luckyDrawScreen) {
        Long finishTime = Long.valueOf(luckyDrawScreen.getLuckyDrawExpiredDate());
        if (finishTime < 1000000000000L) finishTime *= 1000;
        finishTime = finishTime - System.currentTimeMillis();
        countDownSearch = new CountDownTimer(finishTime, 1000) {
            @Override
            public void onTick(final long millisUntilFinished) {
                isCountDownRunning = true;
                new Handler().post(() -> tvLdCountDown.setText(UtilManis.fromHtml(
                        getContext().getString(R.string.ld_main_txt_count_down,
                                luckyDrawScreen.getLuckyDrawMsgPrefix(),
                                UtilManis.getTimeCountDownLeft(millisUntilFinished,
                                        getContext().getString(R.string.ld_main_txt_count_down_day))))));
            }

            @Override
            public void onFinish() {
                isCountDownRunning = false;
                new Handler().post(() -> {
                    setCountDownFinish(luckyDrawScreen);
                });
            }
        };
    }

    private void setCountDownFinish(LuckyDrawScreen luckyDrawScreen) {
        try {
            if (luckyDrawFragment != null)
                if (luckyDrawFragment.isAdded()) {
                    tvLdCountDown.setText(UtilManis.fromHtml(
                            getContext().getString(R.string.ld_main_txt_count_down,
                                    luckyDrawScreen.getLuckyDrawMsgPrefix(),
                                    luckyDrawScreen.getLuckyDrawMsgObject()
                            )));
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
