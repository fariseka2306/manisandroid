package com.ebizu.manis.mvp.account.accountmenulist.profile;

import com.ebizu.manis.model.Account;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by abizu-alvio on 7/26/2017.
 */

public interface IProfileView extends IBaseView {

    void showProfileData(Account account);

    void updatePhotoProfile(Account account);

    void setInputEmail(Account account);

    void setInputName(Account account);

    void setInputBirthdate(Account account);

    void setInputCountry(Account account);

    void setInputMobile(Account account);

    void setInputGender(Account account);

    void setInputAddresss(Account account);

    void setPropertiesAddress();

    void setPropertiesBirthdate();

    void setPropertiesName();

    void setPropertiesEmail();

    void updateFlagViewIndo();

    void updateFlagViewMalay();

    IProfilePresenter getProfilePresenter();

}
