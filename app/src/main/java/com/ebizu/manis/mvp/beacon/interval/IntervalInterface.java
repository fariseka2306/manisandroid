package com.ebizu.manis.mvp.beacon.interval;

/**
 * Created by ebizu on 9/7/17.
 */

public interface IntervalInterface {

    String type();

    long interval(int interval);

}
