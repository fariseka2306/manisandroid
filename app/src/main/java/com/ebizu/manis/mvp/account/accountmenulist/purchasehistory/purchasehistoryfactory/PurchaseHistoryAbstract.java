package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistoryfactory;

import android.content.Context;

import com.ebizu.manis.model.PurchaseHistory;

/**
 * Created by halim_ebizu on 11/24/17.
 */

public abstract class PurchaseHistoryAbstract implements PurchaseStatus {

    protected PurchaseHistory purchaseHistory;
    protected Context context;

    public PurchaseHistoryAbstract(PurchaseHistory purchaseHistory, Context context) {
        this.purchaseHistory = purchaseHistory;
        this.context = context;
    }
}
