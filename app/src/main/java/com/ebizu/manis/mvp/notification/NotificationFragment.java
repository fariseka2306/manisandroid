package com.ebizu.manis.mvp.notification;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.mvp.notification.listnotification.ListNotificationView;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.view.manis.toolbar.ToolbarNotification;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Raden on 7/4/17.
 */

public class NotificationFragment extends BaseFragment {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.toolbar_notification)
    ToolbarNotification toolbarNotification;
    @BindView(R.id.rel_left)
    RelativeLayout relBack;
    @BindView(R.id.notification_list)
    ListNotificationView listNotificationView;
    @BindView(R.id.view_group_bluetooth_area)
    ConstraintLayout viewBluetooth;
    @BindView(R.id.fn_switch_bluetooth)
    Switch switchBluetooth;
    @BindView(R.id.text_view_free_stuff)
    TextView txtFreeStruff;
    @BindView(R.id.text_view_instruct_bluetooth)
    TextView txtViewInstruct;
    @BindView(R.id.recycler_view_notification)
    RecyclerView recyclerView;

    BluetoothAdapter mBluetoothAdapter;

    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(mBluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, mBluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        viewBluetooth.setVisibility(View.VISIBLE);
                        txtFreeStruff.setVisibility(View.VISIBLE);
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        break;
                    case BluetoothAdapter.STATE_ON:
                        viewBluetooth.setVisibility(View.GONE);
                        txtFreeStruff.setVisibility(View.GONE);
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_notification2, container, false);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        ButterKnife.bind(this, view);
        initializeView();
        return view;
    }

    private void initializeView() {
        toolbarNotification.setTitle(getActivity().getString(R.string.notifications));
        checkBluetooth();
    }

    public void syncNotificationView() {
        listNotificationView.executeSyncNotifications();
    }

    @OnClick(R.id.btn_clear_all)
    void onClickClearAll() {
        listNotificationView.deleteAllNotificationList();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_NOTIFICATION,
                ConfigManager.Analytic.Action.CLICK,
                "Button Clear All"
        );
    }

    @OnClick(R.id.image_view_bluetooth)
    void onClickBluetooth() {
        enableDisableBT();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_NOTIFICATION,
                ConfigManager.Analytic.Action.CLICK,
                "Button Bluetooth"
        );
    }

    public void enableDisableBT() {
        if (mBluetoothAdapter == null) {
            Log.d(TAG, "enableDisableBT: Does not have BT compatibles.");
        }
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBTIntent);

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            getActivity().registerReceiver(mBroadcastReceiver1, BTIntent);

        }
        if (mBluetoothAdapter.isEnabled()) {
            Log.d(TAG, "enableDisableBT: disabling BT.");
            mBluetoothAdapter.disable();

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            getActivity().registerReceiver(mBroadcastReceiver1, BTIntent);
        }
    }

    private void checkBluetooth() {
        BluetoothAdapter mBluetoothAdapetr = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapetr != null) {
            if (!mBluetoothAdapetr.isEnabled()) {
                viewBluetooth.setVisibility(View.VISIBLE);
                txtFreeStruff.setVisibility(View.VISIBLE);
                switchBluetooth.setChecked(false);
            } else {
                viewBluetooth.setVisibility(View.GONE);
                txtFreeStruff.setVisibility(View.GONE);
                switchBluetooth.setChecked(true);
            }
        }
    }

    @OnClick(R.id.rel_left)
    void relBackPressed() {
        ((MainActivity) getActivity()).closeDrawer();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_NOTIFICATION,
                ConfigManager.Analytic.Action.CLICK,
                "Button Back"
        );
    }

    @OnClick(R.id.text_view_free_stuff)
    void clickFreezeStuff() {
        txtFreeStruff.getFreezesText();
    }

    @OnClick(R.id.text_view_instruct_bluetooth)
    void clickFreezeInstruct() {
        txtViewInstruct.getFreezesText();
    }

    @OnClick(R.id.recycler_view_notification)
    void clickFreeze() {
        recyclerView.getContext();
    }
}
