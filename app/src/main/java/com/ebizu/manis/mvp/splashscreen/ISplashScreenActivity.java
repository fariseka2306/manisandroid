package com.ebizu.manis.mvp.splashscreen;

import com.ebizu.manis.root.IBaseActivity;

/**
 * Created by Ebizu-User on 06/07/2017.
 */

public interface ISplashScreenActivity extends IBaseActivity {

    void setTextDescSplash();

    void loadLogoAnimation();

    void showDialogDeviceBlock();

    void showDialogManisPlaystore();

    void nextActivity();

}
