package com.ebizu.manis.mvp.account.accountmenulist.profile.activityresult;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileActivity;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileView;
import com.ebizu.manis.preference.ManisSession;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 10/16/17.
 */

public class SignUpOtpRequest extends ProfileRequestCode
        implements IProfileRequest {

    @BindView(R.id.profile_view)
    ProfileView profileView;
    @BindView(R.id.edittext_mobile)
    EditText editTextMobile;
    @BindView(R.id.imageview_flagmobile)
    ImageView imageViewFlag;
    @BindView(R.id.textview_flagmobile)
    TextView textViewFlag;
    @BindView(R.id.spin_country)
    Spinner spinner;

    public SignUpOtpRequest(ProfileActivity profileActivity) {
        super(profileActivity);
    }

    @Override
    public int requestCode() {
        return ConfigManager.Otp.FORCE_SIGN_UP_OTP;
    }

    @Override
    public void doRequest(int resultCode, Intent data) {
        super.doRequest(resultCode, data);
        if (resultCode == ConfigManager.Otp.SUCCESS_SIGN_UP_OTP) {
            ButterKnife.bind(this, profileActivity);
            ManisSession manisSession = profileActivity.getManisSession();
            Account account = manisSession.getAccountSession();
            editTextMobile.setText(UtilManis.convertPhoneNumber(account));
            imageViewFlag.setImageDrawable(UtilManis.getPhoneCountryImage(profileActivity, account));
            int position;
            if (account.getAccMsisdn().startsWith("62")) {
                position = 0;
                profileView.updateFlagViewIndo();
                textViewFlag.setText("+62");
            } else {
                position = 1;
                profileView.updateFlagViewMalay();
                textViewFlag.setText("+60");
            }
            spinner.setSelection(position);
        }
    }
}
