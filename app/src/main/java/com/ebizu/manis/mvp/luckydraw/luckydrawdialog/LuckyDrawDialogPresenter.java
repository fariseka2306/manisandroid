package com.ebizu.manis.mvp.luckydraw.luckydrawdialog;

import android.util.Log;
import android.util.SparseArray;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseSubsriberLambda;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.requestbodyv2.data.PaginationData;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawEntry;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawWinner;
import com.google.gson.Gson;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ebizu on 08/08/17.
 */

public class LuckyDrawDialogPresenter extends BaseViewPresenter implements ILuckyDrawDialogPresenter {

    private final String TAG = getClass().getSimpleName();

    private LuckyDrawDialogView luckyDrawDialogView;
    private SparseArray<Subscription> subsLuckyDraw;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        luckyDrawDialogView = (LuckyDrawDialogView) baseView;
        subsLuckyDraw = new SparseArray<>();
    }

    @Override
    public void releaseSubscribe(int key) {
        Subscription subscription = subsLuckyDraw.get(key);
        if (subscription != null) {
            subscription.unsubscribe();
            subsLuckyDraw.remove(key);
        }
    }

    @Override
    public void releaseAllSubscribes() {
        luckyDrawDialogView.unSubscribeAll(subsLuckyDraw);
    }

    @Override
    public RequestBody createLuckyDrawWinnersRB(int page) {
        RequestBody requestBody = new RequestBodyBuilder(luckyDrawDialogView.getContext())
                .setFunction(ConfigManager.LuckyDraw.FUNCTION_GET_WINNERS)
                .create();
        PaginationData paginationData = new PaginationData();
        paginationData.setPage(String.valueOf(page));
        requestBody.addData(paginationData);
        return requestBody;
    }

    @Override
    public void loadLuckyDrawWinner(IBaseView.LoadType loadType, RequestBody requestBody) {
        luckyDrawDialogView.showProgressView(loadType, ILuckyDrawDialogView.ViewType.LUCKY_DRAW_WINNER);
        releaseSubscribe(1);
        Subscription subsWinner = getManisApiV2().getWinners(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperLuckyDrawWinner>(luckyDrawDialogView) {
                    @Override
                    protected void onSuccess(WrapperLuckyDrawWinner wrapperLuckyDrawWinner) {
                        super.onSuccess(wrapperLuckyDrawWinner);
                        luckyDrawDialogView.dismissProgressView(loadType, ILuckyDrawDialogView.ViewType.LUCKY_DRAW_WINNER);
                        luckyDrawDialogView.setLuckyDrawWinnerData(loadType, wrapperLuckyDrawWinner.getLuckyDrawWinnerData());
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                        luckyDrawDialogView.dismissProgressView(loadType, ILuckyDrawDialogView.ViewType.LUCKY_DRAW_WINNER);
                        luckyDrawDialogView.showNegativeScenario(message);
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        luckyDrawDialogView.dismissProgressView(loadType, ILuckyDrawDialogView.ViewType.LUCKY_DRAW_WINNER);
                        luckyDrawDialogView.showNegativeScenario(message);
                    }
                });
        Log.d(TAG, "loadLuckyDrawWinner: ".concat(new Gson().toJson(requestBody)));
        subsLuckyDraw.put(1, subsWinner);
    }

    @Override
    public RequestBody createLuckyDrawEntriesRB(int page) {
        RequestBody requestBody = new RequestBodyBuilder(luckyDrawDialogView.getContext())
                .setFunction(ConfigManager.LuckyDraw.FUNCTION_GET_ENTRIES)
                .create();
        PaginationData paginationData = new PaginationData();
        paginationData.setPage(String.valueOf(page));
        requestBody.addData(paginationData);
        return requestBody;
    }

    @Override
    public void loadLuckyDrawEntryData(IBaseView.LoadType loadType, RequestBody requestBody) {
        luckyDrawDialogView.showProgressView(loadType, ILuckyDrawDialogView.ViewType.LUCKY_DRAW_ENTRY);
        releaseSubscribe(0);
        Subscription subsEntry = getManisApiV2().getEntries(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperLuckyDrawEntry>(luckyDrawDialogView) {
                    @Override
                    protected void onSuccess(WrapperLuckyDrawEntry wrapperLuckyDrawEntry) {
                        super.onSuccess(wrapperLuckyDrawEntry);
                        luckyDrawDialogView.dismissProgressView(loadType, ILuckyDrawDialogView.ViewType.LUCKY_DRAW_ENTRY);
                        luckyDrawDialogView.setLuckyDrawEntryData(loadType, wrapperLuckyDrawEntry.getLuckyDrawEntryData());
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                        luckyDrawDialogView.dismissProgressView(loadType, ILuckyDrawDialogView.ViewType.LUCKY_DRAW_ENTRY);
                        luckyDrawDialogView.showNegativeScenario(message);
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        luckyDrawDialogView.dismissProgressView(loadType, ILuckyDrawDialogView.ViewType.LUCKY_DRAW_ENTRY);
                        luckyDrawDialogView.showNegativeScenario(message);
                    }
                });
        Log.d(TAG, "loadLuckyDrawEntryData: ".concat(new Gson().toJson(requestBody)));
        subsLuckyDraw.put(0, subsEntry);
    }

}
