package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.purchasevoucher.CustomerDetails;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.view.manis.textinput.ShopCartNumberTextInput;
import com.ebizu.manis.view.manis.textinput.ShopCartTextInput;

public class PhysicalShoppingCartView extends ShoppingCartView {
    ShopCartNumberTextInput textInputPostcode;
    ShopCartTextInput textInputAddress;
    ShopCartTextInput textInputCity;
    ShopCartTextInput textInputState;
    ShopCartTextInput textInputCountry;

    public PhysicalShoppingCartView(Context context) {
        super(context);
    }

    public PhysicalShoppingCartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public PhysicalShoppingCartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PhysicalShoppingCartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public int bottomLayoutId() {
        return R.layout.view_physical_shopping_cart;
    }

    @Override
    public void initBottomViewChildren(View view) {
        textInputAddress = view.findViewById(R.id.text_input_address);
        textInputPostcode = view.findViewById(R.id.text_input_post_code);
        textInputCity = view.findViewById(R.id.text_input_city);
        textInputState = view.findViewById(R.id.text_input_state);
        textInputCountry = view.findViewById(R.id.text_input_country);
    }

    @Override
    public void setShoppingCartView(RewardVoucher reward) {
        super.setShoppingCartView(reward);
        Account account = getManisSession().getAccountSession();
        textInputAddress.setInput(account.getAccAddress());
        textInputPostcode.setInput(account.getAciPostcode());
        textInputCity.setInput(account.getAciCity());
        textInputState.setInput(account.getAciState());
        textInputCountry.setInput(account.getAciCountry());
        textInputCountry.setDoneImeAction();
        textInputPostcode.setInputType(InputType.TYPE_CLASS_NUMBER);
        textInputAddress.setInputFilterWithLength("", 128);
        textInputCity.setInputFilterWithLength("", 50);
        textInputState.setInputFilterWithLength("", 50);
        textInputCountry.setInputFilterWithLength("", 50);
        textInputPostcode.setInputFilterWithLength("[0-9]", 10);
    }

    @Override
    protected CustomerDetails getCustomerDetails() {
        CustomerDetails customerDetails = super.getCustomerDetails();
        customerDetails.setAddress(textInputAddress.getText());
        customerDetails.setPostCode(textInputPostcode.getText());
        customerDetails.setTown(textInputCity.getText());
        customerDetails.setState(textInputState.getText());
        customerDetails.setCountry(textInputCountry.getText());
        return customerDetails;
    }

    @Override
    protected void initView() {
        super.initView();
        shopCartTextInputs.add(textInputAddress);
        shopCartTextInputs.add(textInputPostcode);
        shopCartTextInputs.add(textInputCity);
        shopCartTextInputs.add(textInputState);
        shopCartTextInputs.add(textInputCountry);
    }
}