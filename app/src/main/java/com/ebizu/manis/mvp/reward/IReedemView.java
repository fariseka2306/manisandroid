package com.ebizu.manis.mvp.reward;

import com.ebizu.manis.helper.LoadType;
import com.ebizu.sdk.reward.models.Response;

/**
 * Created by Raden on 7/26/17.
 */

public interface IReedemView {
    void setRedeem(final Response.Data<com.ebizu.sdk.reward.models.Reward> rewardData, LoadType loadType);

    android.widget.SearchView getSearchView();

    int getPage();

    void failedConnected();

    void noData(String message);

    void showProgress();

    void dissmissProgress();

    void swipeProgress();

    void dissmissSwipeProgress();

    void paginationProgress();

    void dissmissPaginaionProgress();

}
