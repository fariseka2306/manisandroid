package com.ebizu.manis.mvp.luckydraw.luckydrawscreen;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by ebizu on 04/08/17.
 */

public interface ILuckyDrawPresenter extends IBaseViewPresenter {

    void loadLuckyDrawScreenData(ManisApi manisApi);

    void loadLuckyDrawScreenData();

}
