package com.ebizu.manis.mvp.account.accountmenulist.faq;

import com.ebizu.manis.model.Faq;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.response.WrapperFaq;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by abizu-alvio on 7/31/2017.
 */

public class FaqPresenter extends BaseViewPresenter implements IFaqPresenter {

    private final String TAG = getClass().getSimpleName();

    private FaqView faqView;
    private Subscription subsFAQ;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        faqView = (FaqView) baseView;
    }

    @Override
    public void loadFaq(ManisApi manisApi, Faq faq) {
        releaseSubscribe(0);
        subsFAQ = manisApi.getFaq()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperFaq>(faqView) {
                    @Override
                    public void onNext(WrapperFaq wrapperFaq) {
                        super.onNext(wrapperFaq);
                        List<Faq> faqses = wrapperFaq.getFaq();
                        faqView.showFaqs(faqses);
                    }
                });
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsFAQ != null) subsFAQ.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }
}
