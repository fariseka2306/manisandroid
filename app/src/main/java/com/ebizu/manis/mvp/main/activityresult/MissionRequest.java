package com.ebizu.manis.mvp.main.activityresult;

import android.content.Intent;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.main.MainActivity;

/**
 * Created by andrifashbir on 24/10/17.
 */

public class MissionRequest extends MainRequestCode
        implements IMainRequestCode {

    public MissionRequest(MainActivity mainActivity) {
        super(mainActivity);
    }

    @Override
    public int requestCode() {
        return ConfigManager.Missions.MISSION_DETAIL_CODE;
    }

    @Override
    public void jobRequest(int resultCode, Intent data) {
        super.jobRequest(resultCode, data);
        if (resultCode == ConfigManager.Missions.MISSION_DETAIL_CODE) {
            mPager.setCurrentItem(POS_MISSION_VIEW);
            mainActivity.missionFragment.showInviteFriendsDialog();
        }
    }
}
