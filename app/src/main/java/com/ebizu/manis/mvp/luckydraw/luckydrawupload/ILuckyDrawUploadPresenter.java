package com.ebizu.manis.mvp.luckydraw.luckydrawupload;

import com.ebizu.manis.model.LuckyDrawUpload;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.LuckyDrawUploadBody;
import com.ebizu.manis.service.manis.requestbody.LuckyDrawUploadPhotoBody;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public interface ILuckyDrawUploadPresenter extends IBaseViewPresenter {

    void loadLuckyDrawUpload(ManisApi manisApi,
                             LuckyDrawUploadBody luckyDrawUploadBody,
                             LuckyDrawUploadPhotoBody luckyDrawUploadPhotoBody);
}
