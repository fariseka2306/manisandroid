package com.ebizu.manis.mvp.account.accountmenulist.redemptionhistory;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by abizu-alvio on 8/7/2017.
 */

public interface IRedemptionHistoryPresenter extends IBaseViewPresenter {

    void loadRedemptionHistory(ManisApi manisApi, int page);

    void loadRedemptionHistoryNext(ManisApi manisApi, int page);

}
