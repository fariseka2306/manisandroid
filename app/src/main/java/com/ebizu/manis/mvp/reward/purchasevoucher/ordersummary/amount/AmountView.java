package com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary.amount;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.preference.ManisSession;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 11/17/17.
 */

public class AmountView extends RelativeLayout {

    @BindView(R.id.text_view_amount)
    TextView textViewAmount;
    @BindView(R.id.text_view_currency)
    TextView textViewCurrency;

    public AmountView(Context context) {
        super(context);
        createView(context);
    }

    public AmountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public AmountView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AmountView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    private void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_amount, null, false);
        addView(view, new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    public void setView(Order order) {
        Account account = ManisSession.getInstance(getContext()).getAccountSession();
        String amount = UtilManis.addSeparator(account.getCurrency().getIso2(), order.getProduct().getAmount());

        textViewCurrency.setText(account.getCurrency().getIso2());
        textViewAmount.setText(amount);
    }
}
