package com.ebizu.manis.mvp.versioning;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.mvp.splashscreen.SplashScreenActivity;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseActivityPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.VersioningBody;
import com.ebizu.manis.service.manis.response.WrapperVersioning;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mac on 8/22/17.
 */

public class VersioningPresenter extends BaseActivityPresenter implements IVersioningPresenter {

    private final String TAG = getClass().getSimpleName();

    private SplashScreenActivity splashScreenActivity;
    private Subscription subsVersioning;
    ManisApi manisApi;

    @Inject
    public VersioningPresenter() {

    }

    @Override
    public void attachView(BaseActivity view) {
        super.attachView(view);
        this.splashScreenActivity = (SplashScreenActivity) view;

    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsVersioning != null) subsVersioning.unsubscribe();
    }

    @Override
    public void releaseAllSubscribe() {
        releaseSubscribe(0);
    }

    @Override
    public void requestVersioning() {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceVersioning(getContext());
        VersioningBody versioningBody = new VersioningBody();
        versioningBody.setCode(BuildConfig.VERSION_CODE);
        versioningBody.setPlatform("android");
        versioningBody.setLang(Locale.getDefault().toString());
        releaseSubscribe(0);
        subsVersioning = manisApi.getVersioning(versioningBody)
                .timeout(6, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperVersioning>(splashScreenActivity) {
                    @Override
                    public void onNext(WrapperVersioning wrapperVersioning) {
                        super.onNext(wrapperVersioning);
                        splashScreenActivity.showVersioning(wrapperVersioning.getVersioning(), getContext());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        splashScreenActivity.nextActivity();
                    }
                });
    }
}
