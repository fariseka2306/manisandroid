package com.ebizu.manis.mvp.account.accountmenulist.profile.changeprofilephoto;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.rx.ResponseSubsriberLambda;
import com.ebizu.manis.sdk.utils.ImageUtil;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.WrapperAccount;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by abizu-alvio on 7/26/2017.
 */

public class ChangePresenter extends BaseViewPresenter implements IChangePresenter {

    private final String TAG = getClass().getSimpleName();

    private Context context;
    private ChangeProfilePhotoView changeProfilePhotoView;
    private Subscription subsChange;


    public ChangePresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        changeProfilePhotoView = (ChangeProfilePhotoView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsChange != null) subsChange.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void saveProfilePhoto(ManisApi manisApi, File file) {
        releaseSubscribe(0);
        changeProfilePhotoView.getBaseActivity().showProgressBarDialog(context.getString(R.string.please_wait));
        subsChange = manisApi.saveProfilePhoto(getMultipartBody(file.getName(), getPhotoRequestBody(file)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperAccount>(changeProfilePhotoView) {
                    @Override
                    public void onNext(WrapperAccount wrapperAccount) {
                        super.onNext(wrapperAccount);
                        changeProfilePhotoView.getBaseActivity().dismissProgressBarDialog();
                        changeProfilePhotoView.getManisSession().updateSession(wrapperAccount.getAccount());
                        changeProfilePhotoView.successUploadPhoto();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        changeProfilePhotoView.getBaseActivity().dismissProgressBarDialog();
                        if (!ConnectionDetector.isNetworkConnected(changeProfilePhotoView.getContext()))
                            NegativeScenarioManager.show(throwable, changeProfilePhotoView, NegativeScenarioManager.NegativeView.DIALOG);
                    }

                    @Override
                    public void onErrorFailure(ErrorResponse errorResponse) {
                        super.onErrorFailure(errorResponse);
                        NegativeScenarioManager.show(errorResponse, changeProfilePhotoView, NegativeScenarioManager.NegativeView.DIALOG);
                    }
                });
    }

    private okhttp3.RequestBody getPhotoRequestBody(File file) {
        String compressedFilename = ImageUtil.compressImage(file);
        File compressedFile = new File(compressedFilename);
        return okhttp3.RequestBody.create(MediaType.parse("image/jpeg"), compressedFile);
    }

    private MultipartBody.Part getMultipartBody(String fileName, okhttp3.RequestBody requestBody) {
        return MultipartBody.Part.createFormData("photo", fileName, requestBody);
    }

    @Override
    public void savePhotoProfile(RequestBody requestBody) {
        releaseSubscribe(0);
        changeProfilePhotoView.getBaseActivity().showProgressBarDialog(context.getString(R.string.please_wait));
        getManisApiV2().updateAccountPhotoProfile(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperAccount>(changeProfilePhotoView) {

                    @Override
                    protected void onSuccess(WrapperAccount wrapperAccount) {
                        super.onSuccess(wrapperAccount);
                        changeProfilePhotoView.getBaseActivity().dismissProgressBarDialog();
                        changeProfilePhotoView.getManisSession().updateSession(wrapperAccount.getAccount());
                        changeProfilePhotoView.successUploadPhoto();
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                        changeProfilePhotoView.getBaseActivity().showManisAlertDialog(message);
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        changeProfilePhotoView.getBaseActivity().showManisAlertDialog(message);
                    }
                });
    }
}
