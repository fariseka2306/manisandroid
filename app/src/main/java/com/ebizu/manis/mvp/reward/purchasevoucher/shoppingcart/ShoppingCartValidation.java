package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.view.manis.textinput.ShopCartTextInput;

import java.util.List;

/**
 * Created by ebizu on 9/29/17.
 */

public class ShoppingCartValidation {

    private static Context mContext;
    private TextInputValidation[] textInputValidations = {
            new TextInputName(mContext),
            new TextInputEmail(mContext),
            new TextInputPhoneNumber(mContext),
            new TextInputAddress(mContext),
            new TextInputPostcode(mContext),
            new TextInputCity(mContext),
            new TextInputState(mContext),
            new TextInputCountry(mContext)
    };

    public void check(Context context, List<ShopCartTextInput> shopCartTextInputs, ShoppingCartView shoppingCartView) {
        mContext = context;
        if (isComplete(shopCartTextInputs, shoppingCartView)) {
            shoppingCartView.validFormOrderDetail();
        }
    }

    private boolean isComplete(List<ShopCartTextInput> shopCartTextInputs, ShoppingCartView shoppingCartView) {
        int index = 0;
        boolean complete = false;

        for (ShopCartTextInput shopCartTextInput : shopCartTextInputs) {
            TextInputValidation textInputValidation = textInputValidations[index];
            if (!shopCartTextInput.getText().replace(" ", "").isEmpty()) {
                complete = true;
            } else {
                shoppingCartView.showMessage(textInputValidation.errorMessage());
                complete = false;
                return complete;
            }

            if (textInputValidation instanceof TextInputPhoneNumber) {
                TextInputPhoneNumber textInputPhoneNumber = new TextInputPhoneNumber(mContext);
                if (shopCartTextInput.getText().length() < textInputPhoneNumber.minLength()) {
                    shoppingCartView.showMessage("Please enter minimum 8 digits");
                    complete = false;
                    return complete;
                }
            }

            if (textInputValidation instanceof TextInputPostcode) {
                TextInputPostcode TextInputPostcode = new TextInputPostcode(mContext);
                if (shopCartTextInput.getText().length() > TextInputPostcode.minLength()) {
                    shoppingCartView.showMessage(mContext.getString(R.string.error_valid_postcode));
                    complete = false;
                    return complete;
                }
            }

            if (textInputValidation instanceof TextInputEmail) {
                if (!UtilManis.isValidEmail(shopCartTextInput.getText())) {
                    shoppingCartView.showMessage(mContext.getString(R.string.error_valid_email));
                    complete = false;
                    return complete;
                }
            }

            index++;
        }
        return complete;
    }

    public interface ITextInputValidation {
        String errorMessage();

        String type();
    }

    public class TextInputValidation
            implements ITextInputValidation {

        protected Context context;

        public TextInputValidation(Context context) {
            this.context = context;
        }

        @Override
        public String errorMessage() {
            return mContext.getString(R.string.error_empty_shopping_detail);
        }

        @Override
        public String type() {
            return "";
        }
    }

    private class TextInputName extends TextInputValidation
            implements ITextInputValidation {

        public TextInputName(Context context) {
            super(context);
        }


        @Override
        public String type() {
            return "Name";
        }
    }

    private class TextInputEmail extends TextInputValidation
            implements ITextInputValidation {

        public TextInputEmail(Context context) {
            super(context);
        }


        @Override
        public String type() {
            return "Email";
        }
    }

    private class TextInputPhoneNumber extends TextInputValidation
            implements ITextInputValidation {

        public TextInputPhoneNumber(Context context) {
            super(context);
        }


        @Override
        public String type() {
            return "Phone no";
        }

        public int minLength() {
            return 8;
        }
    }

    private class TextInputAddress extends TextInputValidation
            implements ITextInputValidation {

        public TextInputAddress(Context context) {
            super(context);
        }

        @Override
        public String type() {
            return "Address";
        }
    }

    private class TextInputPostcode extends TextInputValidation
            implements ITextInputValidation {

        public TextInputPostcode(Context context) {
            super(context);
        }


        @Override
        public String type() {
            return "Postcode";
        }

        public int minLength() {
            return 10;
        }
    }

    private class TextInputCity extends TextInputValidation
            implements ITextInputValidation {

        public TextInputCity(Context context) {
            super(context);
        }

        @Override
        public String type() {
            return "City";
        }
    }

    private class TextInputState extends TextInputValidation
            implements ITextInputValidation {

        public TextInputState(Context context) {
            super(context);
        }


        @Override
        public String type() {
            return "State";
        }
    }

    private class TextInputCountry extends TextInputValidation
            implements ITextInputValidation {

        public TextInputCountry(Context context) {
            super(context);
        }


        @Override
        public String type() {
            return "Country";
        }
    }
}
