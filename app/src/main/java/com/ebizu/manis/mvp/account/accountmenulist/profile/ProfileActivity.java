package com.ebizu.manis.mvp.account.accountmenulist.profile;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Toast;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.mvp.account.accountmenulist.profile.activityresult.ProfileRequest;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ebizu.manis.helper.ConfigManager.UpdateProfile.UPDATE_PROFILE_TYPE_INTENT;

/**
 * Created by abizu-alvio on 7/25/2017.
 */

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;
    @BindView(R.id.profile_view)
    ProfileView profileView;

    private ProfileRequest profileRequest;
    private Context context;
    private int updateProfileType;
    private ReceiptStore receiptStore;

    @Inject
    ManisSession manisSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_menulist_profile);
        context = this;
        ButterKnife.bind(this);
        getIntentData();
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void getIntentData() {
        try {
            updateProfileType = getIntent().getIntExtra(UPDATE_PROFILE_TYPE_INTENT, 0);
            receiptStore = getIntent().getParcelableExtra(ConfigManager.Snap.MERCHANT_DATA);
            profileView.setUpdateProfileType(updateProfileType);
            profileView.setReceiptStore(receiptStore);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void initView() {
        toolbarView.setTitle(R.string.pf_title);
        profileView.setActivity(this);
        profileView.attachPresenter(new ProfilePresenter(context));
        profileView.showProfileData(manisSession.getAccountSession());
        profileRequest = new ProfileRequest(this);
        if (manisSession.isFirstLogin()) {
            toolbarView.setVisibility(View.GONE);
        } else {
            toolbarView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finish() {
        super.finish();
        slideOutHorizontalAnim();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.PROFILE_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Back");
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ConfigManager.Permission.CAMERA_REQUEST_CODE && getPermissionManager().hasPermissions(permissions)) {
            profileView.changeActivity();
        } else {
            Toast.makeText(this, "Some Permission is Denied", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        profileRequest.doRequest(requestCode, resultCode, data);
    }

    @Override
    public void
    onBackPressed() {
        if (manisSession.isFirstLogin()) {
            profileView.showNotificationMessage(getResources().getString(R.string.first_login_interest_page_back_pressed_info));
        } else {
            super.onBackPressed();
        }
    }

    public void updateProfilePhoto() {
        try {
            final String dir = Environment.getExternalStorageDirectory() + "/Manis";
            File newdir = new File(dir);
            if (!newdir.exists()) newdir.mkdirs();
            String imageFileName = newdir + "/ProfilePicture.jpg";
            File image = new File(imageFileName);
            ImageUtils.loadImage(context, image.getPath(), profileView.imageViewPhoto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}