package com.ebizu.manis.mvp.luckydraw.luckydrawupload;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.model.LuckyDrawUpload;
import com.ebizu.manis.mvp.luckydraw.luckydrawscreen.LuckyDrawPresenter;
import com.ebizu.manis.root.BaseView;

import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class LuckyDrawUploadView extends BaseView implements ILuckyDrawUploadView {

    private ILuckyDrawUploadPresenter iLuckyDrawUploadPresenter;

    public LuckyDrawUploadView(Context context) {
        super(context);
        createView(context);
    }

    public LuckyDrawUploadView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public LuckyDrawUploadView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LuckyDrawUploadView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.luckydraw_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new LuckyDrawPresenter(getContext()));
        initView();
    }

    @Override
    public void setLuckyDrawUpload(LuckyDrawUpload luckyDrawUpload) {

    }

    @Override
    public ILuckyDrawUploadPresenter getLuckyDrawUploadPresenter() {
        return iLuckyDrawUploadPresenter;
    }


    private void initView() {
    }
}
