package com.ebizu.manis.mvp.luckydraw.luckydrawupload;

import com.ebizu.manis.model.LuckyDrawUpload;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public interface ILuckyDrawUploadView extends IBaseView {

    void setLuckyDrawUpload(LuckyDrawUpload luckyDrawUpload);

    ILuckyDrawUploadPresenter getLuckyDrawUploadPresenter();
}
