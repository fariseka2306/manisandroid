package com.ebizu.manis.mvp.store.storedetail;

import com.ebizu.manis.model.Store;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by firef on 7/14/2017.
 */

public interface IStoreDetailPresenter extends IBaseViewPresenter {

    void loadStoreDetailData(ManisApi manisApi, Store store);

    void postStoreDetailFollower(ManisApi manisApi, Store store);

    void postStoreDetailUnfollow(ManisApi manisApi, Store store);

    void isSnapable(Store store);
}
