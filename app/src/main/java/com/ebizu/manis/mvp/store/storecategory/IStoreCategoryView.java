package com.ebizu.manis.mvp.store.storecategory;

import com.ebizu.manis.model.InterestsStore;
import com.ebizu.manis.root.IBaseView;

import java.util.ArrayList;

/**
 * Created by firef on 7/10/2017.
 */

public interface IStoreCategoryView extends IBaseView {

    void loadInterestsStore(ArrayList<InterestsStore> interestsStores);

    void noInternetConnection();

    IStoreCategoryPresenter getStoreCategoryPresenter();

}
