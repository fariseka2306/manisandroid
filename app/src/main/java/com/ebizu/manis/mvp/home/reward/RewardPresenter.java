package com.ebizu.manis.mvp.home.reward;

import android.app.Activity;
import android.content.Context;

import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.sdk.reward.EbizuReward;
import com.ebizu.sdk.reward.models.Response;
import com.ebizu.sdk.reward.models.Reward;

/**
 * Created by Raden on 7/13/17.
 */

public class RewardPresenter implements IRewardPresenter {

    private Context context;
    private Activity activity;
    private IRewardView iRewardView;

    public RewardPresenter(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
    }

    public void attachView(IRewardView iRewardView) {
        this.iRewardView = iRewardView;
    }

    @Override
    public void loadReedeemData() {
        EbizuReward.getProvider(context).featuredRewards(1, 999, false, new com.ebizu.sdk.reward.core.interfaces.Callback<Response.Data<Reward>>() {

            @Override
            public void onSuccess(Response.Data<Reward> redeemHomeData) {
                iRewardView.setRedeemHome(redeemHomeData);
            }

            @Override
            public void onFailure(String s) {
                noConnection(iRewardView);
            }
        });
    }

    public void noConnection(IRewardView iRewardView) {
        if (!ConnectionDetector.isNetworkConnected(context)) {
            iRewardView.noConnectionReward();
        } else {
            iRewardView.noReward("no data");
        }
    }

    public void noRewardData(IRewardView iRewardView) {
        iRewardView.noReward("no data");
    }
}

