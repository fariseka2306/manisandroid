package com.ebizu.manis.mvp.store.storeoffer;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Offer;
import com.ebizu.manis.model.Redeem;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.StoreDetail;
import com.ebizu.manis.mvp.store.storedetail.IStoreDetailPresenter;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.view.dialog.offerclaim.OfferClaimDialog;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 8/22/17.
 */

public class StoreOfferDetailView extends BaseView implements IStoreOfferDetailView,
        View.OnClickListener {

    private Store store;
    private StoreDetail storeDetail;
    private Offer offer;
    private IStoreOfferDetailPresenter iStoreOfferDetailPresenter;

    @BindView(R.id.od_img_rewards)
    ImageView odImgRewards;

    @BindView(R.id.od_txt_detail)
    TextView odTxtDetail;

    @BindView(R.id.od_txt_start)
    TextView odTxtStart;

    @BindView(R.id.od_txt_end)
    TextView odTxtEnd;

    @BindView(R.id.od_lin_save)
    LinearLayout odLinSave;

    @BindView(R.id.od_txt_save)
    TextView odTxtSave;

    @BindView(R.id.od_txt_expired)
    TextView odTxtExpired;

    @BindView(R.id.od_img_save)
    ImageView odImgSave;

    @BindView(R.id.od_wv_tc)
    WebView webTos;

    public StoreOfferDetailView(Context context) {
        super(context);
        createView(context);
    }

    public StoreOfferDetailView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public StoreOfferDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public StoreOfferDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.offers_detail_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        initListener();
        attachPresenter(new StoreOfferDetailPresenter(getContext()));
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iStoreOfferDetailPresenter = (IStoreOfferDetailPresenter) iBaseViewPresenter;
        iStoreOfferDetailPresenter.attachView(this);
    }

    @Override
    public void onClick(View view) {
        ManisApi manisApi = ManisApiGenerator.createServiceWithToken(getContext());
        if (!offer.getSaved()) {
            iStoreOfferDetailPresenter.loadPinOffer(manisApi, offer);
        } else {
            iStoreOfferDetailPresenter.loadUnpinOffer(manisApi, offer);
        }
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.OFFER,
                ConfigManager.Analytic.Action.CLICK,
                "Button Save"
        );
    }

    @Override
    public void setPinOffer(ResponseData responseData) {
        if (responseData.getSuccess()) {
            odTxtSave.setText(getContext().getString(R.string.rd_txt_saved));
            odImgSave.setImageResource(R.drawable.rewards_saved);
        }
    }

    @Override
    public void setUnpiOffer(ResponseData responseData) {
        if (responseData.getSuccess()) {
            odTxtSave.setText(getContext().getString(R.string.rd_txt_save));
            odImgSave.setImageResource(R.drawable.rewards_save);
        }
    }

    @Override
    public void claimOffer(Redeem redeem) {
        OfferClaimDialog offerClaimDialog = new OfferClaimDialog(getContext());
        offerClaimDialog.setOfferClaimDialogView(redeem, offer, store);
        offerClaimDialog.show();
    }

    @Override
    public IStoreOfferDetailPresenter getStoreOfferDetailPresenter() {
        return iStoreOfferDetailPresenter;
    }

    public void setStoreOfferDetailView(Offer offer, Store store, StoreDetail storeDetail) {
        this.offer = offer;
        this.store = store;
        this.storeDetail = storeDetail;
        Glide.with(getContext())
                .load(offer.getPhoto())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(R.drawable.default_pic_rewards_tile_img)
                .animate(R.anim.fade_in_image)
                .into(odImgRewards);
        odTxtDetail.setText(offer.getDescription());
        odTxtStart.setText(convertDate(offer.getOfferDate().getStart()));
        odTxtEnd.setText(convertDate(offer.getOfferDate().getEnd()));
        odTxtExpired.setText(convertDate(offer.getOfferDate().getExpired()));
        if (offer.getSaved()) {
            odTxtSave.setText(getContext().getString(R.string.rd_txt_saved));
            odImgSave.setImageResource(R.drawable.rewards_saved);
        } else {
            odTxtSave.setText(getContext().getString(R.string.rd_txt_save));
            odImgSave.setImageResource(R.drawable.rewards_save);
        }

        String tos = "<html><body style=\"margin: 0; font-size: 12px; color: #747474;\">Details :<ul style=\"padding: 0; padding-left: 20px; margin: 0;\">";
        for (int i = 0; i < offer.getTos().length; i++) {
            tos += "<li>" + offer.getTos()[i] + "</li>";
        }
        tos += "</ul></body></html>";
        if (offer.getTos().length > 0) {
            webTos.loadData(tos, "text/html", "utf-8");
        }
    }

    private void initListener() {
        odLinSave.setOnClickListener(this);
    }

    private String convertDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000L);
        return DateFormat.format("dd MMM yyyy", cal).toString();
    }

}
