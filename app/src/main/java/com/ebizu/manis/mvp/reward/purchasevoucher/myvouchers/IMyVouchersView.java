package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers;

import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.service.reward.response.WrapperMyVoucher;
import com.ebizu.manis.service.reward.response.WrapperRewardVoucherList;

/**
 * Created by ebizu on 10/2/17.
 */

public interface IMyVouchersView extends IBaseView {

    void showMyVouchersView(WrapperRewardVoucherList wrapperMyVoucher, LoadType loadType);

    void loadMyVouchersViewFail(LoadType loadType);

    void invisibleListVoucherView();

    void visibleListVoucherView();

    void showProgressBarCircle();

    void dismissProgressBarCircle();

    void startActivityShoppingCart(ShoppingCart shoppingCart, RewardVoucher reward);



    IMyVouchersPresenter getMyVouchersPresenter();
}
