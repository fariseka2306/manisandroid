package com.ebizu.manis.mvp.snap.receipt.camera;

import android.graphics.Bitmap;

import com.ebizu.manis.root.IBaseActivityPresenter;

/**
 * Created by andrifashbir on 13/12/17.
 */

public interface ICameraActivityPresenter<View> extends IBaseActivityPresenter {
    void processData(byte[] data);

    void stitchImage();
}
