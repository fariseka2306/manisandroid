package com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.fragment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.SnapGuideActivity;

import java.util.HashMap;


public class SnapGuideFourthStichFragment extends Fragment {

    private boolean isCreated = false;
    private AnimatorSet mAnimatorSet;

    // Second Screen
    private TextView textViewTitleStitch;
    private TextView textViewDetailStitch;
    private ImageView imageViewRightHand;
    private ImageView imageViewLeftHand;
    private ImageView imageViewStitchReview;
    private ImageView imageViewLongReceipt;
    private ImageView imageViewPlus;
    private View viewOvalWhite;
    private Button buttonNext;
    private TextView textViewExplanationStitch;

    private HashMap<View, Float> mOriginalYValuesMap = new HashMap<>();
    private HashMap<View, Float> mOriginalXValuesMap = new HashMap<>();
    private SnapGuideActivity snapGuideActivity;

    private View.OnClickListener nextListener = view -> {
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        snapGuideActivity = (SnapGuideActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_snapguide_fourth_stich, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isCreated = true;
        Bundle args = getArguments();
        int position = args.getInt(UtilStatic.ARG_POSITION);
        view.setTag(position);
    }

    private void initView(View rootView) {
        textViewTitleStitch = (TextView) rootView.findViewById(R.id.textview_title);
        textViewDetailStitch = (TextView) rootView.findViewById(R.id.textview_detail);
        imageViewRightHand = (ImageView) rootView.findViewById(R.id.imageview_left_hand);
        imageViewLeftHand = (ImageView) rootView.findViewById(R.id.imageview_right_hand);
        imageViewLongReceipt = (ImageView) rootView.findViewById(R.id.imageview_receipt);
        imageViewStitchReview = (ImageView) rootView.findViewById(R.id.imageview_stitch_review);
        imageViewPlus = (ImageView) rootView.findViewById(R.id.imageview_plus);
        viewOvalWhite = rootView.findViewById(R.id.view_oval_white);
        buttonNext = (Button) rootView.findViewById(R.id.button_next);
        textViewExplanationStitch = (TextView) rootView.findViewById(R.id.textview_explanation);
        buttonNext.setOnClickListener(nextListener);

        initializeFirstPosition();
        initializeStateView();
        rootView.post(this::getOriginalYValues);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && isCreated) {
            getOriginalYValues();
            doAnimation();
        } else if (!menuVisible && isCreated) {
            mAnimatorSet.end();
            setViewsInOriginalPosition();
        }
    }

    private void getOriginalYValues() {
        mOriginalYValuesMap.put(imageViewLongReceipt, imageViewLongReceipt.getY());
        mOriginalXValuesMap.put(imageViewRightHand, imageViewRightHand.getX());
        mOriginalXValuesMap.put(imageViewLeftHand, imageViewLeftHand.getX());
    }

    private void setViewsInOriginalPosition() {
        imageViewLongReceipt.setY(mOriginalYValuesMap.get(imageViewLongReceipt));
        imageViewRightHand.setX(mOriginalXValuesMap.get(imageViewRightHand));
        imageViewLeftHand.setX(mOriginalXValuesMap.get(imageViewLeftHand));
        initializeStateView();
    }

    private void initializeStateView() {
        textViewTitleStitch.setAlpha(0f);
        textViewDetailStitch.setAlpha(0f);
        imageViewLongReceipt.setAlpha(1f);
        imageViewLeftHand.setAlpha(1f);
        imageViewStitchReview.setAlpha(0f);
        imageViewPlus.setAlpha(0f);
    }

    private void initializeFirstPosition() {
        imageViewLongReceipt.setY(imageViewLongReceipt.getY() + getResources().getInteger(R.integer.moveYreceipt));
        imageViewRightHand.setX(imageViewRightHand.getX() + getResources().getInteger(R.integer.moveHand));
        imageViewLeftHand.setX(imageViewLeftHand.getX() - getResources().getInteger(R.integer.moveHand) * 2);
    }

    public void doAnimation() {
        ObjectAnimator fadeTitleFirst = ObjectAnimator.ofFloat(textViewTitleStitch, "alpha", 0f, 1f);
        fadeTitleFirst.setDuration(700);

        ObjectAnimator fadeDetailFirst = ObjectAnimator.ofFloat(textViewDetailStitch, "alpha", 0f, 1f);
        fadeDetailFirst.setDuration(700);

        ObjectAnimator fadeInImageReview = ObjectAnimator.ofFloat(imageViewStitchReview, "alpha", 0f, 1f);
        fadeInImageReview.setDuration(700);

        ObjectAnimator fadeInImagePlus = ObjectAnimator.ofFloat(imageViewPlus, "alpha", 0f, 1f);
        fadeInImagePlus.setDuration(700);

        ObjectAnimator fadeOutRightHand = ObjectAnimator.ofFloat(imageViewLeftHand, "alpha", 1f, 0f);
        fadeOutRightHand.setDuration(700);

        ObjectAnimator fadeOutImageReceipt = ObjectAnimator.ofFloat(imageViewLongReceipt, "alpha", 1f, 0f);
        fadeOutImageReceipt.setDuration(700);

        ObjectAnimator moveReceipt = ObjectAnimator.ofFloat(imageViewLongReceipt, "y", imageViewLongReceipt.getY(), imageViewLongReceipt.getY() - getResources().getInteger(R.integer.moveYreceipt));
        moveReceipt.setDuration(800);

        ObjectAnimator moveRightHand = ObjectAnimator.ofFloat(imageViewRightHand, "x", imageViewRightHand.getX(), imageViewRightHand.getX() - getResources().getInteger(R.integer.moveHand));
        moveRightHand.setDuration(1600);

        ObjectAnimator moveRightHandSecond = ObjectAnimator.ofFloat(imageViewRightHand, "x", (imageViewRightHand.getX() - getResources().getInteger(R.integer.moveHand)), (imageViewRightHand.getX() - getResources().getInteger(R.integer.moveHand)) + getResources().getInteger(R.integer.moveHandSecond));
        moveRightHandSecond.setDuration(800);

        ObjectAnimator moveLeftHand = ObjectAnimator.ofFloat(imageViewLeftHand, "x", imageViewLeftHand.getX(), imageViewLeftHand.getX() + getResources().getInteger(R.integer.moveHand) * 2);
        moveLeftHand.setDuration(1600);

        mAnimatorSet = new AnimatorSet();
        moveLeftHand.setStartDelay(500);
        moveRightHand.setStartDelay(1300);
        moveRightHandSecond.setStartDelay(4000);
        fadeOutImageReceipt.setStartDelay(5200);
        fadeInImageReview.setStartDelay(5000);
        fadeOutRightHand.setStartDelay(5200);
        fadeInImagePlus.setStartDelay(6000);

        mAnimatorSet
                .play(fadeTitleFirst)
                .with(fadeDetailFirst)
                .with(moveReceipt)
                .with(moveLeftHand)
                .with(moveRightHand)
                .with(moveRightHandSecond)
                .with(fadeOutImageReceipt)
                .with(fadeInImageReview)
                .with(fadeOutRightHand)
                .with(fadeInImagePlus);
        mAnimatorSet.start();
    }
}