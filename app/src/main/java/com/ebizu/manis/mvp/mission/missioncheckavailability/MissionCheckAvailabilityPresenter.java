package com.ebizu.manis.mvp.mission.missioncheckavailability;

import android.content.Context;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.mvp.mission.MissionDetailActivity;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseV2Subscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperienceCheckAvalaibilityRB;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperShareExperienceCheckAvalaibility;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MissionCheckAvailabilityPresenter extends BaseViewPresenter implements ICheckAvailability {

    private ManisApi manisApi;
    private Context context;
    private Subscription subsAvailability;

    public MissionCheckAvailabilityPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void checkAvailability(MissionDetailActivity missionDetailActivity, ShareExperienceCheckAvalaibilityRB shareExperienceCheckAvalaibilityRB) {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceMission(context);
        releaseSubscribe(0);
        subsAvailability = manisApi.checkAvailabilityMission(BuildConfig.MISSION_URL_PARAM, shareExperienceCheckAvalaibilityRB)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseV2Subscriber<WrapperShareExperienceCheckAvalaibility>(missionDetailActivity) {
                    @Override
                    public void onNext(WrapperShareExperienceCheckAvalaibility wrapperShareExperienceCheckAvalaibility) {
                        super.onNext(wrapperShareExperienceCheckAvalaibility);
                        missionDetailActivity.setupButton(wrapperShareExperienceCheckAvalaibility);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        missionDetailActivity.setupErrorButton(e);
                    }
                });
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsAvailability != null) subsAvailability.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }
}
