package com.ebizu.manis.mvp.snap.receipt.upload;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.FileUtils;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.model.LuckyDrawUpload;
import com.ebizu.manis.model.snap.Photo;
import com.ebizu.manis.mvp.luckydraw.luckydrawsubmit.LuckyDrawSubmitDialog;
import com.ebizu.manis.mvp.snap.receipt.camera.ReceiptCameraActivity;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.snap.PhotoMultipartBody;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptDataRequestBody;
import com.ebizu.manis.view.camera.TouchImageView;
import com.ebizu.manis.view.dialog.receipt.ReceiptUploadDialog;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;

import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;

import static com.ebizu.manis.helper.ConfigManager.Snap.OPEN_LUCKY_DRAW_FRAGMENT;

/**
 * Created by Ebizu-User on 14/08/2017.
 */

public class ReceiptUploadActivity extends ReceiptCameraActivity implements IReceiptUploadActivity {

    @BindView(R.id.image_view_receipt)
    TouchImageView imageViewReceipt;

    @Inject
    Context context;

    private ReceiptUploadPresenter receiptUploadPresenter;
    private ManisApi manisApi;
    private Bitmap bitmapReceipt;
    private ReceiptUploadDialog receiptUploadDialog;
    private LuckyDrawSubmitDialog luckyDrawSubmitDialog;
    private Uri imageUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_preview);
        ButterKnife.bind(this);
        initializeComponent();
        setImageViewReceipt();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initializeComponent() {
        manisApi = ManisApiGenerator.createServiceWithToken(context);
        receiptUploadPresenter = new ReceiptUploadPresenter();
        receiptUploadPresenter.attachView(this);
    }

    private void setImageViewReceipt() {
        String imagePath = ConfigManager.Snap.URI_RECEIPT_PATH +
                ConfigManager.Snap.URI_RECEIPT_FILE_BITMAP.concat(String.valueOf(bitmapId));
        imageUri = ImageUtils.getUri(bitmapId);
        bitmapReceipt = ImageUtils.getBitmapFile(imagePath);
        imageViewReceipt.setImageBitmap(bitmapReceipt);
    }

    @OnClick(R.id.su_rel_retake)
    void snapRetake() {
        backToCameraActivity();
    }

    @OnClick(R.id.su_rel_upload)
    void snapUpload() {
        onClickUploadButton();
    }

    void onClickUploadButton() {
        File imageFile = FileUtils.getFile(this, imageUri);
        okhttp3.RequestBody receiptDataRequestBody = new ReceiptDataRequestBody(receiptDataBody).create();
        MultipartBody.Part receiptMultiBody = new PhotoMultipartBody(imageFile).createFormData();
        Photo photo = receiptUploadPresenter.createPhoto(bitmapReceipt);
        if (!isSnapLuckyDraw()) {
            receiptUploadPresenter.uploadReceiptImage(receiptDataRequestBody, receiptMultiBody);
        } else {
            RequestBody requestBody = receiptUploadPresenter.createLuckyDrawUploadReceiptRB(receiptDataBody, photo);
            receiptUploadPresenter.uploadReceiptImageLuckyDraw(requestBody);
        }
    }

    @Override
    public void onSuccessUpload(boolean isLuckyDraw) {
        ImageUtils.deleteRecentFile(imageUri);
        onSuccessUpload(isLuckyDraw, null);
    }

    @Override
    public void onSuccessUpload(boolean isLuckyDraw, LuckyDrawUpload luckyDrawUpload) {
        ImageUtils.deleteRecentFile(imageUri);
        if (isLuckyDraw && null != luckyDrawUpload) {
            showUploadSnapEarnTicket(luckyDrawUpload);
        } else {
            showUploadSnapEarnPoint();
        }
    }

    @Override
    public void showUploadSnapEarnPoint() {
        if (receiptUploadDialog == null)
            receiptUploadDialog = new ReceiptUploadDialog(this);
        receiptUploadDialog.setActivity(this);
        receiptUploadDialog.setBitmapReceipt(bitmapReceipt);
        receiptUploadDialog.setOnBackpressedListener(new ReceiptUploadDialog.OnBackPressedListener() {
            @Override
            public void onBackPressed() {
                finishActivityUploadSuccess();
            }

            @Override
            public void onMorePoints() {
                finishActivityGetMorePoints();
            }
        });
        receiptUploadDialog.setOnKeyListener((arg0, keyCode, event) -> {
            // TODO Auto-generated method stub
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                finishActivityUploadSuccess();
            }
            return true;
        });
        receiptUploadDialog.setOnCancelListener(dialogInterface -> finishActivityUploadSuccess());
        receiptUploadDialog.show();
    }

    @Override
    public void showUploadSnapEarnTicket(LuckyDrawUpload luckyDrawUpload) {
        if (luckyDrawSubmitDialog == null)
            luckyDrawSubmitDialog = new LuckyDrawSubmitDialog(this);
        luckyDrawSubmitDialog.setActivity(this);
        luckyDrawSubmitDialog.setLuckyDrawSubmitDialogView(luckyDrawUpload);
        luckyDrawSubmitDialog.setOnKeyListener((arg0, keyCode, event) -> {
            // TODO Auto-generated method stub
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                finishActivityUploadReceiptEarnTicket(false);
            }
            return true;
        });
        luckyDrawSubmitDialog.setOnDismissListener(dialogInterface -> finishActivityUploadReceiptEarnTicket(false));
        luckyDrawSubmitDialog.setOnCancelListener(dialogInterface -> finishActivityUploadReceiptEarnTicket(false));
        luckyDrawSubmitDialog.setOnButtonClickListener(new LuckyDrawSubmitDialog.OnButtonClickListener() {
            @Override
            public void onLuckyDraw() {
                finishActivityUploadReceiptEarnTicket(true);
            }

            @Override
            public void onOk() {
                finishActivityUploadReceiptEarnTicket(false);
            }
        });
        luckyDrawSubmitDialog.show();
    }

    private void finishActivityUploadSuccess() {
        if (receiptUploadDialog != null) {
            receiptUploadDialog.dismiss();
        }
        setResult(ConfigManager.Snap.EARN_POINT_RECEIPT_SUCCESS_UPLOAD_RS);
        finish();
    }

    private void finishActivityGetMorePoints() {
        if (receiptUploadDialog != null) {
            receiptUploadDialog.dismiss();
        }
        setResult(ConfigManager.Snap.GET_MORE_POINTS_RS);
        finish();
    }

    private void finishActivityUploadReceiptEarnTicket(boolean openLuckyDraw) {
        if (receiptUploadDialog != null) {
            receiptUploadDialog.dismiss();
        }
        getIntent().putExtra(OPEN_LUCKY_DRAW_FRAGMENT, openLuckyDraw);
        setResult(ConfigManager.Snap.EARN_TICKET_RECEIPT_SUCCESS_UPLOAD_RS, getIntent());
        finish();
    }
}