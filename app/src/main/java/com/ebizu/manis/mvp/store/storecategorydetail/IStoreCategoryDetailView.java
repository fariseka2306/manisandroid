package com.ebizu.manis.mvp.store.storecategorydetail;

import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.StoreResults;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.service.manis.requestbody.StoreInterestBody;

import java.util.ArrayList;

/**
 * Created by firef on 7/12/2017.
 */

public interface IStoreCategoryDetailView extends IBaseView {

    void loadStoreCategory(StoreResults storeResults, StoreInterestBody storeInterestBody);

    void loadStoreCategoryPaging(StoreResults storeResults, StoreInterestBody storeInterestBody);

    IStoreCategoryDetailPresenter getStoreCategoryDetailPresenter();
}
