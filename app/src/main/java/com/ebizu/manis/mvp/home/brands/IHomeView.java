package com.ebizu.manis.mvp.home.brands;

import com.ebizu.manis.model.Brand;

import java.util.ArrayList;

/**
 * Created by Raden on 7/4/17.
 */

public interface IHomeView {
    void loadViewBrand(ArrayList<Brand> brands);

    void setBrandData(String message);

//    void loadViewReward(ArrayList<com.ebizu.sdk.reward.models.Reward> rewards);

    void failedConnected();

    void serverBusy(String message);

    void noBrandData();
}
