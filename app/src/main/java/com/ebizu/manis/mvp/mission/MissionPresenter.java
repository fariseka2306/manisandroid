package com.ebizu.manis.mvp.mission;

import android.view.View;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseV2Subscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperienceMissionsRB;
import com.ebizu.manis.service.manis.responsev2.status.Status;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperMissions;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class MissionPresenter extends BaseViewPresenter implements IMissionPresenter {

    private MissionView missionView;
    private ManisApi manisApi;
    private Subscription subsMission;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        missionView = (MissionView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsMission != null) subsMission.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadShareExperienceMission(ShareExperienceMissionsRB shareExperienceMissionsRB) {
        releaseSubscribe(0);
        if (missionView.getAdapter().getItemCount() == 0)
            missionView.showProgressBar();
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceMission(missionView.getContext());
        subsMission = manisApi.getMissions(BuildConfig.MISSION_URL_PARAM, shareExperienceMissionsRB)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseV2Subscriber<WrapperMissions>(missionView) {
                    @Override
                    public void onNext(WrapperMissions wrapperMissions) {
                        super.onNext(wrapperMissions);
                        missionView.dismissNoInternetConnection();
                        missionView.dismissProgressBar();
                        if (wrapperMissions.getExperienceList().isEmpty()) {
                            missionView.layoutEmpty.setVisibility(View.VISIBLE);
                            missionView.recyclerViewShareExperience.setVisibility(View.GONE);
                        } else {
                            missionView.layoutEmpty.setVisibility(View.GONE);
                            missionView.showShareExperience(wrapperMissions);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        missionView.dismissProgressBar();
                        if (!ConnectionDetector.isNetworkConnected(missionView.getContext())) {
                            missionView.showNoInternetConnection();
                        } else {
                            missionView.showNotificationMessage(missionView.getContext().getResources().getString(R.string.server_busy));
                        }
                        missionView.recyclerViewShareExperience.setVisibility(View.GONE);
                    }

                    @Override
                    protected void onErrorFailure(Status status) {
                        super.onErrorFailure(status);
                        missionView.showNotificationMessage(status.getMessageClient());
                    }
                });
    }

    @Override
    public void loadShareExperienceNextPage(ShareExperienceMissionsRB shareExperienceMissionsRB) {
        releaseSubscribe(0);
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceMission(missionView.getContext());
        subsMission = manisApi.getMissions(BuildConfig.MISSION_URL_PARAM, shareExperienceMissionsRB)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseV2Subscriber<WrapperMissions>(missionView) {
                    @Override
                    public void onNext(WrapperMissions wrapperMissions) {
                        super.onNext(wrapperMissions);
                        missionView.showShareExperienceNextPage(wrapperMissions);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        NegativeScenarioManager.show(e, missionView, NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }

                    @Override
                    protected void onErrorFailure(Status status) {
                        super.onErrorFailure(status);
                        missionView.showNotificationMessage(status.getMessageClient());
                        missionView.getAdapter().removeLoadingFooter();
                    }
                });
    }
}
