package com.ebizu.manis.mvp.snap.store.list.near;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.snap.SnapStoreResult;
import com.ebizu.manis.mvp.snap.store.list.IListSnapStorePresenter;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.manis.nodatastorefound.NoDataStoreFoundView;
import com.ebizu.manis.view.manis.recyclerview.SnapStoreRecyclerView;
import com.ebizu.manis.view.manis.swiperefresh.SwipeRefreshManis;
import com.ebizu.manis.view.nestedscroll.NestedScrollChangeListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public class NearStoreView extends BaseView
        implements INearStoreView {

    private NearStorePresenter nearStorePresenter;
    private SnapStoreRecyclerView snapStoreRecyclerView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshManis swipeRefresh;
    @BindView(R.id.text_view_title)
    TextView textViewTitle;

    private int page = 1;
    private boolean loading;
    private boolean lastPage;

    public NearStoreView(Context context) {
        super(context);
        createView(context);
    }

    public NearStoreView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public NearStoreView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NearStoreView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_snap_store_near, null, false);
        addView(view, new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        addRecyclerView();
        addSwipeRefresh();
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        nearStorePresenter = (NearStorePresenter) iBaseViewPresenter;
        nearStorePresenter.attachView(this);
    }

    @Override
    public void addNearStores(SnapStoreResult snapStoreResult, LoadType loadType) {
        setLastPage(!snapStoreResult.getMore());
        switch (loadType) {
            case CLICK_LOAD: {
                snapStoreRecyclerView.replaceData(snapStoreResult.getStores());
                break;
            }
            case SWIPE_LOAD: {
                snapStoreRecyclerView.replaceData(snapStoreResult.getStores());
                break;
            }
            case SCROLL_LOAD: {
                snapStoreRecyclerView.addData(snapStoreResult.getStores());
                break;
            }
        }
        setLoading(false);
        setViewStore();
    }

    private void setViewStore() {
        if (snapStoreRecyclerView.isEmptyStores()) {
            snapStoreRecyclerView.setVisibility(GONE);
            invisibleTitle();
            showNoDataStoreView(NoDataStoreFoundView.Type.NEAR_VIEW);
        } else if (!snapStoreRecyclerView.isShown()) {
            dismissGpsDeactivateView();
            visibleTitle();
            snapStoreRecyclerView.setVisibility(VISIBLE);
        }
    }

    private void visibleTitle() {
        textViewTitle.setVisibility(VISIBLE);
    }

    private void invisibleTitle() {
        textViewTitle.setVisibility(GONE);
    }

    @Override
    public NearStorePresenter getNearStorePresenter() {
        return nearStorePresenter;
    }

    @Override
    public void setOnSnapAbleListener(IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener) {
        snapStoreRecyclerView.setOnSnapAbleListener(onCheckSnapAbleListener);
    }

    private void addRecyclerView() {
        snapStoreRecyclerView = new SnapStoreRecyclerView(getContext());
        swipeRefresh.addViewToSwipe(snapStoreRecyclerView,
                new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT));
    }

    private void addSwipeRefresh() {
        swipeRefresh.setOnRefreshListener(() -> {
            if (!loading) {
                setLoading(true);
                setPage(1);
                if (nearStorePresenter.isGpsEnable()) {
                    getNearStorePresenter().getSnapStoresNearby(page, LoadType.SWIPE_LOAD);
                } else {
                    setLoading(false);
                    swipeRefresh.dismissSwipe();
                }
            }
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.FRAGMENT_STORES_NEARBY,
                    ConfigManager.Analytic.Action.REFRESH,
                    "Swipe Refresh"
            );
        });
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        snapStoreRecyclerView.initialize(baseActivity);
        addScrollListener();
    }

    private void addScrollListener() {
        swipeRefresh.getNestedScrollView().setOnScrollChangeListener(
                new NestedScrollChangeListener(snapStoreRecyclerView.getLinearLayoutManager(), snapStoreRecyclerView, swipeRefresh) {
                    @Override
                    protected void onLoadMore() {
                        setPage(page + 1);
                        getNearStorePresenter().getSnapStoresNearby(page, IBaseView.LoadType.SCROLL_LOAD);
                    }

                    @Override
                    protected boolean isLoading() {
                        return loading;
                    }

                    @Override
                    protected boolean isLastPage() {
                        return lastPage;
                    }
                });
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }

    public SwipeRefreshManis getSwipeRefresh() {
        return swipeRefresh;
    }

    public SnapStoreRecyclerView getSnapStoreRecyclerView() {
        return snapStoreRecyclerView;
    }
}
