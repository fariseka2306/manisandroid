package com.ebizu.manis.mvp.store.storedetaillocation;

import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by ebizu on 9/11/17.
 */

public class StoreLocationPresenter extends BaseViewPresenter
        implements IStoreLocationPresenter {

    private StoreLocationView storeLocationView;

    @Override
    public void attachView(BaseView baseView) {
        storeLocationView = (StoreLocationView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        // not implemented method
    }

    @Override
    public void releaseAllSubscribes() {
        // not implemented method
    }

    @Override
    public void loadMap() {
        storeLocationView.mapFragment.getMapAsync(storeLocationView);
    }
}