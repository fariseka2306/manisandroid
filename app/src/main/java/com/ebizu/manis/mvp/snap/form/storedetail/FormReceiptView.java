package com.ebizu.manis.mvp.snap.form.storedetail;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.CommasThousandSeparator;
import com.ebizu.manis.helper.PeriodsThousandSeparator;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.root.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 09/08/2017.
 */

public class FormReceiptView extends BaseView implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.text_input_total_amount)
    TextInputEditText textInputTotalAmount;

    @BindView(R.id.edge_switch)
    Switch edgeSwitch;

    private SharedPreferences sharedPreferences;
    private String countryId;

    public FormReceiptView(Context context) {
        super(context);
        createView(context);
    }

    public FormReceiptView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public FormReceiptView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public FormReceiptView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_form_receipt_details, null, false);
        addView(view, new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        Account account = getManisSession().getAccountSession();
        countryId = account.getAccCountry();
        setupAmountInput();
        initSwitchEdge(context);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        UtilSessionFirstIn.setValueAutoEdgeSwitch(sharedPreferences, isChecked);
    }

    public boolean isValidInput() {
        if (textInputTotalAmount.getText().toString().isEmpty())
            YoYo.with(Techniques.Shake).duration(500).playOn(textInputTotalAmount);
        return !textInputTotalAmount.getText().toString().isEmpty();
    }

    private void initSwitchEdge(Context context) {
        sharedPreferences = context.getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
        edgeSwitch.setOnCheckedChangeListener(this);
        edgeSwitch.setChecked(UtilSessionFirstIn.isAutoEdgeSwitch(sharedPreferences));
    }

    private double totalAmount() {
        return new Double(textInputTotalAmount.getText().toString());
    }

    public double getTotalAmount() {
        String totalAmount = textInputTotalAmount.getText().toString();
        if (countryId.equalsIgnoreCase("ID")) {
            return new Double(PeriodsThousandSeparator.trimPeriodsOfString(totalAmount));
        } else if (countryId.equalsIgnoreCase("MY")) {
            return new Double(CommasThousandSeparator.trimCommasOfString(totalAmount));
        }
        return new Double(0);
    }

    private void setupAmountInput() {
        if (countryId.equalsIgnoreCase("ID")) {
            textInputTotalAmount.addTextChangedListener(new PeriodsThousandSeparator(textInputTotalAmount));
        } else if (countryId.equalsIgnoreCase("MY")) {
            textInputTotalAmount.addTextChangedListener(new CommasThousandSeparator(textInputTotalAmount));
        }
    }
}