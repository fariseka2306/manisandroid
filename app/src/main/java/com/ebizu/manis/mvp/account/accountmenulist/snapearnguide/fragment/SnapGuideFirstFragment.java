package com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.fragment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.SnapGuideActivity;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.requestbodyv2.data.ManisTncData;
import com.ebizu.manis.view.dialog.term.TermDialog;

import java.util.HashMap;

public class SnapGuideFirstFragment extends BaseFragment {

    private boolean isCreated = false;
    private AnimatorSet mAnimatorSet;

    // First Screen
    private TextView textViewTitleFirst;
    private TextView textViewDetailFirst;
    private View viewOvalRedFirst;
    private ImageView imageViewStoreFirst;
    private ImageView imageViewSnapFirst;
    private ImageView imageViewReceiptFirst;
    private TextView textViewStoreFirst;
    private TextView textViewSnapFirst;
    private TextView textViewReceiptFirst;
    private TextView textViewTerms;
    private View viewOvalWhiteFirst;
    private Button buttonNextFirst;
    private TextView textViewExplanationFirst;

    private HashMap<View, Float> mOriginalXValuesMap = new HashMap<>();
    private HashMap<View, Float> mOriginalYValuesMap = new HashMap<>();
    private SnapGuideActivity snapGuideActivity;

    private View.OnClickListener termListener = view -> {
        TermDialog termDialog = new TermDialog(getActivity());
        termDialog.setIsManisLegal(false);
        termDialog.show();
        termDialog.setActivity((BaseActivity) getActivity());
        termDialog.getTermPresenter().loadTerm(setRequestBody(getContext()), ConfigManager.ManisLegal.TNC_TOU);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.SNAP_GUIDE_FIRST_FRAGMENT,
                ConfigManager.Analytic.Action.CLICK,
                "Text Terms"
        );
    };

    private View.OnClickListener nextListener = view -> snapGuideActivity.switchToFragment(1);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        snapGuideActivity = (SnapGuideActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_snapguide_first, container, false);
        initView(view, savedInstanceState);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isCreated = true;
        Bundle args = getArguments();
        int position = args.getInt(UtilStatic.ARG_POSITION);
        view.setTag(position);
    }

    private void initView(View rootView, final Bundle savedInstanceState) {
        textViewTitleFirst = (TextView) rootView.findViewById(R.id.textview_title);
        textViewDetailFirst = (TextView) rootView.findViewById(R.id.textview_detail);
        viewOvalRedFirst = rootView.findViewById(R.id.view_oval_red);
        imageViewStoreFirst = (ImageView) rootView.findViewById(R.id.imageview_store);
        imageViewSnapFirst = (ImageView) rootView.findViewById(R.id.imageview_snap);
        imageViewReceiptFirst = (ImageView) rootView.findViewById(R.id.imageview_receipt);
        textViewStoreFirst = (TextView) rootView.findViewById(R.id.textview_store);
        textViewSnapFirst = (TextView) rootView.findViewById(R.id.textview_snap);
        textViewReceiptFirst = (TextView) rootView.findViewById(R.id.textview_receipt);
        textViewTerms = (TextView) rootView.findViewById(R.id.textview_terms);
        viewOvalWhiteFirst = rootView.findViewById(R.id.view_oval_white);
        buttonNextFirst = (Button) rootView.findViewById(R.id.button_next);
        textViewExplanationFirst = (TextView) rootView.findViewById(R.id.textview_explanation);
        textViewExplanationFirst.setText(UtilManis.fromHtml(getString(R.string.snapguide_first_explanation)));
        textViewReceiptFirst.setText(UtilManis.fromHtml(getString(R.string.snapguide_first_receipt)));
        textViewTerms.setText(UtilManis.fromHtml(getString(R.string.snapguide_first_terms)));
        buttonNextFirst.setOnClickListener(nextListener);
        textViewTerms.setOnClickListener(termListener);

        initializeFirstPosition();
        initializeStateView();
        rootView.post(() -> getOriginalXYValues());

        if (savedInstanceState == null) {
            new Handler().postDelayed(() -> {
                mOriginalYValuesMap.put(viewOvalWhiteFirst, viewOvalWhiteFirst.getY());
                doAnimation();
            }, 700);

        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && isCreated) {
            doAnimation();
        } else if (!menuVisible && isCreated) {
            if (mAnimatorSet != null) mAnimatorSet.end();
            setViewsInOriginalPosition();
        }
    }

    private void getOriginalXYValues() {
        mOriginalYValuesMap.put(viewOvalWhiteFirst, viewOvalWhiteFirst.getY());
        mOriginalXValuesMap.put(viewOvalRedFirst, viewOvalRedFirst.getX());
        mOriginalXValuesMap.put(imageViewStoreFirst, imageViewStoreFirst.getX());
        mOriginalXValuesMap.put(imageViewSnapFirst, imageViewSnapFirst.getX());
        mOriginalXValuesMap.put(imageViewReceiptFirst, imageViewReceiptFirst.getX());
    }

    private void setViewsInOriginalPosition() {
        if (mOriginalXValuesMap != null && mOriginalYValuesMap != null) {
            viewOvalWhiteFirst.setY(mOriginalYValuesMap.get(viewOvalWhiteFirst));
            viewOvalRedFirst.setX(mOriginalXValuesMap.get(viewOvalRedFirst));
            imageViewStoreFirst.setX(mOriginalXValuesMap.get(imageViewStoreFirst));
            imageViewSnapFirst.setX(mOriginalXValuesMap.get(imageViewSnapFirst));
            imageViewReceiptFirst.setX(mOriginalXValuesMap.get(imageViewReceiptFirst));
            initializeStateView();
        }
    }


    private void initializeStateView() {
        textViewTitleFirst.setAlpha(0f);
        textViewDetailFirst.setAlpha(0f);
        textViewStoreFirst.setAlpha(0f);
        textViewSnapFirst.setAlpha(0f);
        textViewReceiptFirst.setAlpha(0f);
        textViewTerms.setAlpha(0f);
        buttonNextFirst.setAlpha(0f);
        textViewExplanationFirst.setAlpha(0f);
    }

    private void initializeFirstPosition() {
        viewOvalWhiteFirst.setY(viewOvalWhiteFirst.getY() + getResources().getInteger(R.integer.moveYOvalWhite));
        viewOvalRedFirst.setX(viewOvalRedFirst.getX() - getResources().getInteger(R.integer.moveXovalRed));
        imageViewStoreFirst.setX(imageViewStoreFirst.getX() - getResources().getInteger(R.integer.moveXSSR));
        imageViewSnapFirst.setX(imageViewSnapFirst.getX() - getResources().getInteger(R.integer.moveXSSR));
        imageViewReceiptFirst.setX(imageViewReceiptFirst.getX() - getResources().getInteger(R.integer.moveXSSR));
    }

    public void doAnimation() {
        ObjectAnimator moveOvalWhite = ObjectAnimator.ofFloat(viewOvalWhiteFirst, "y", viewOvalWhiteFirst.getY(), viewOvalWhiteFirst.getY() - getResources().getInteger(R.integer.moveYOvalWhite));
        moveOvalWhite.setDuration(800);

        ObjectAnimator moveOvalRed = ObjectAnimator.ofFloat(viewOvalRedFirst, "x", viewOvalRedFirst.getX(), viewOvalRedFirst.getX() + getResources().getInteger(R.integer.moveXovalRed));
        moveOvalRed.setDuration(800);

        ObjectAnimator fadeTitleFirst = ObjectAnimator.ofFloat(textViewTitleFirst, "alpha", 0f, 1f);
        fadeTitleFirst.setDuration(1200);

        ObjectAnimator fadeDetailFirst = ObjectAnimator.ofFloat(textViewDetailFirst, "alpha", 0f, 1f);
        fadeDetailFirst.setDuration(700);

        ObjectAnimator moveStoreFirst = ObjectAnimator.ofFloat(imageViewStoreFirst, "X", imageViewStoreFirst.getX(), imageViewStoreFirst.getX() + getResources().getInteger(R.integer.moveXSSR));
        moveStoreFirst.setDuration(900);

        ObjectAnimator moveSnapFirst = ObjectAnimator.ofFloat(imageViewSnapFirst, "X", imageViewSnapFirst.getX(), imageViewSnapFirst.getX() + getResources().getInteger(R.integer.moveXSSR));
        moveSnapFirst.setDuration(900);

        ObjectAnimator moveReceiptFirst = ObjectAnimator.ofFloat(imageViewReceiptFirst, "X", imageViewReceiptFirst.getX(), imageViewReceiptFirst.getX() + getResources().getInteger(R.integer.moveXSSR));
        moveReceiptFirst.setDuration(900);

        ObjectAnimator fadeTextStoreFirst = ObjectAnimator.ofFloat(textViewStoreFirst, "alpha", 0f, 1f);
        fadeTextStoreFirst.setDuration(700);

        ObjectAnimator fadeTextSnapFirst = ObjectAnimator.ofFloat(textViewSnapFirst, "alpha", 0f, 1f);
        fadeTextSnapFirst.setDuration(700);

        ObjectAnimator fadeTextReceiptFirst = ObjectAnimator.ofFloat(textViewReceiptFirst, "alpha", 0f, 1f);
        fadeTextReceiptFirst.setDuration(700);

        ObjectAnimator fadeTerms = ObjectAnimator.ofFloat(textViewTerms, "alpha", 0f, 1f);
        fadeTerms.setDuration(700);

        ObjectAnimator fadeNext = ObjectAnimator.ofFloat(buttonNextFirst, "alpha", 0f, 1f);
        fadeNext.setDuration(400);

        ObjectAnimator fadeTextExplanation = ObjectAnimator.ofFloat(textViewExplanationFirst, "alpha", 0f, 1f);
        fadeTextExplanation.setDuration(400);

        mAnimatorSet = new AnimatorSet();
        moveOvalRed.setStartDelay(500);
        fadeTitleFirst.setStartDelay(500);
        fadeDetailFirst.setStartDelay(1100);
        moveStoreFirst.setStartDelay(800);
        moveSnapFirst.setStartDelay(1200);
        moveReceiptFirst.setStartDelay(1600);
        fadeTextStoreFirst.setStartDelay(1400);
        fadeTextSnapFirst.setStartDelay(1800);
        fadeTextReceiptFirst.setStartDelay(2200);
        fadeTerms.setStartDelay(2600);
        fadeNext.setStartDelay(2600);
        fadeTextExplanation.setStartDelay(2800);

        mAnimatorSet.play(moveOvalWhite)
                .with(moveOvalRed)
                .with(fadeTitleFirst)
                .with(fadeDetailFirst)
                .with(moveStoreFirst)
                .with(moveSnapFirst)
                .with(moveReceiptFirst)
                .with(fadeTextStoreFirst)
                .with(fadeTextSnapFirst)
                .with(fadeTextReceiptFirst)
                .with(fadeTerms)
                .with(fadeNext)
                .with(fadeTextExplanation);
        mAnimatorSet.start();
    }

    private RequestBody setRequestBody(Context context) {
        ManisTncData manisTncData = new ManisTncData();
        manisTncData.setType(ConfigManager.ManisLegal.TNC_TYPE_SNE);
        RequestBody requestBody = new RequestBodyBuilder(context)
                .setCommand(ConfigManager.ManisLegal.COMMAND_MANIS_LEGAL)
                .setFunction(ConfigManager.ManisLegal.FUNCTION_MANIS_LEGAL)
                .setData(manisTncData)
                .create();
        return requestBody;
    }
}