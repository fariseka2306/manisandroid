package com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary;

import com.ebizu.manis.model.purchasevoucher.MolPaymentResult;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.mvp.reward.purchasevoucher.validation.TransactionValidation;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.service.manis.requestbody.RewardRedeemBody;
import com.ebizu.manis.service.reward.requestbody.RewardPurchaseStatusBody;
import com.ebizu.manis.service.reward.response.WrapperTransactionStatus;
import com.ebizu.sdk.reward.models.Reward;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ebizu on 10/3/17.
 */

public class OrderSummaryPresenter extends BaseViewPresenter implements IOrderSummaryPresenter {

    private OrderSummaryView orderSummaryView;
    private TransactionValidation transactionValidation;
    private RewardRedeemBody rewardRedeemBody;
    private Subscription subsOrderSummary;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        orderSummaryView = (OrderSummaryView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsOrderSummary != null) subsOrderSummary.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void sendPurchaseStatus(MolPaymentResult molPaymentResult, Order order) {
        transactionValidation = new TransactionValidation(orderSummaryView.getContext(), molPaymentResult.getStatus());
        RewardPurchaseStatusBody rewardPurchaseStatusBody = new RewardPurchaseStatusBody(orderSummaryView.getContext());
        RewardPurchaseStatusBody.Data data = new RewardPurchaseStatusBody.Data();

        data.setOrderId(order.getProduct().getOrderId());
        data.setCustomer(order.getCustomerDetails());
        data.setGrossAmount(order.getProduct().getAmount());
        data.setPaymentGatewayName("molpay");
        data.setPaymentType(molPaymentResult.getPaymentType());
        data.setStatusCode(molPaymentResult.getStatusCode());
        data.setStatusMessage(molPaymentResult.getStatusMessage());
        data.setTransactionStatus(transactionValidation.getTransactionPayment());
        data.setTransactionTime(molPaymentResult.getTransactionTime());
        rewardPurchaseStatusBody.setData(data);

        this.sendPurchaseStatus(rewardPurchaseStatusBody);
    }

    @Override
    public void sendPurchaseStatus(TransactionResult transactionResult, Order order) {
        transactionValidation = new TransactionValidation(orderSummaryView.getContext(), transactionResult.getStatus());
        RewardPurchaseStatusBody rewardPurchaseStatusBody = new RewardPurchaseStatusBody(orderSummaryView.getContext());
        RewardPurchaseStatusBody.Data data = new RewardPurchaseStatusBody.Data();

        data.setOrderId(order.getProduct().getOrderId());
        data.setCustomer(order.getCustomerDetails());
        data.setGrossAmount(order.getProduct().getAmount());
        data.setPaymentGatewayName("midtrans");
        data.setPaymentType(transactionResult.getResponse().getPaymentType());
        data.setStatusCode(transactionResult.getResponse().getStatusCode());
        data.setStatusMessage(transactionResult.getResponse().getStatusMessage());
        data.setTransactionStatus(transactionValidation.getTransactionPayment());
        data.setTransactionTime(transactionResult.getResponse().getTransactionTime());
        rewardPurchaseStatusBody.setData(data);

        this.sendPurchaseStatus(rewardPurchaseStatusBody);
    }

    @Override
    public void sendPurchaseStatus(RewardPurchaseStatusBody rewardPurchaseStatusBody) {
        releaseSubscribe(0);
        orderSummaryView.getBaseActivity().showProgressBarDialog("Purchase...");
        subsOrderSummary = getRewardApi().sendResultPurchaseStatus(rewardPurchaseStatusBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperTransactionStatus>(orderSummaryView) {
                    @Override
                    public void onNext(WrapperTransactionStatus wrapperTransactionStatus) {
                        super.onNext(wrapperTransactionStatus);
                        orderSummaryView.getBaseActivity().dismissProgressBarDialog();
                        orderSummaryView.showDialogTransactionStatus(wrapperTransactionStatus.getPurchase());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        orderSummaryView.getBaseActivity().dismissProgressBarDialog();
                        orderSummaryView.showDialogTransactionStatus(null);
                    }
                });
    }

    private RewardRedeemBody rewardRedeemBody(Reward reward) {
        if (rewardRedeemBody == null)
            rewardRedeemBody = new RewardRedeemBody();
        rewardRedeemBody.setId(reward.getId());
        rewardRedeemBody.setPoint(reward.getPoint());
        rewardRedeemBody.setVoucher_type(reward.getType());
        return rewardRedeemBody;
    }
}
