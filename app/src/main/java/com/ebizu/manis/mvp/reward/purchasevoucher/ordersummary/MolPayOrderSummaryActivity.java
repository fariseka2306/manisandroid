package com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.model.purchasevoucher.MolPaymentResult;
import com.ebizu.manis.payment.molpay.MolPayTransactionRequest;
import com.molpay.molpayxdk.MOLPayActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.molpay.molpayxdk.MOLPayActivity.MOLPayPaymentDetails;

/**
 * Created by ebizu on 10/4/17.
 */

public class MolPayOrderSummaryActivity extends OrderSummaryActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MOLPayActivity.MOLPayXDK) {
            String response = data.getStringExtra(MOLPayActivity.MOLPayTransactionResult);
            if (isError(response)) {
                showErrorMessage(response);
            } else {
                MolPaymentResult molPaymentResult = getMolPaymentResult(response);
                orderSummaryView.setTransactionResult(molPaymentResult);
                orderSummaryPresenter.sendPurchaseStatus(molPaymentResult, order);
            }
        }
    }

    @Override
    protected void starTransaction() {
        super.starTransaction();
        MolPayTransactionRequest molPayTransactionRequest = new MolPayTransactionRequest(this, order);
        Intent intent = new Intent(this, MOLPayActivity.class);
        intent.putExtra(MOLPayPaymentDetails, molPayTransactionRequest.getPaymentDetails());
        startActivityForResult(intent, MOLPayActivity.MOLPayXDK);
    }

    private boolean isError(String response) {
        String errorMessage = "";
        try {
            JSONObject jsonObject = new JSONObject(response);
            errorMessage = jsonObject.getString("Error");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return !errorMessage.isEmpty();
    }

    private MolPaymentResult getMolPaymentResult(String response) {
        MolPaymentResult molPaymentResult = new MolPaymentResult();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        try {
            JSONObject jsonObject = new JSONObject(response);
            molPaymentResult.setStatus(
                    jsonObject.getString("status_code"));
            molPaymentResult.setTransactionTime(
                    dateFormat.format(date));
            molPaymentResult.setPaymentType(
                    jsonObject.getString("channel"));
            molPaymentResult.setStatusCode(
                    jsonObject.getString("status_code"));
            molPaymentResult.setStatusMessage("");
            molPaymentResult.setAmount(
                    jsonObject.getString("amount").replace(".00", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return molPaymentResult;
    }

    private void showErrorMessage(String response) {
        String errorMessage = "";
        try {
            JSONObject jsonObject = new JSONObject(response);
            errorMessage = jsonObject.getString("Error");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!errorMessage.isEmpty())
            orderSummaryView.showNotificationMessage(errorMessage);
    }
}