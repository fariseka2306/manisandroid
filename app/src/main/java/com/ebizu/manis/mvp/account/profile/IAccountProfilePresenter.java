package com.ebizu.manis.mvp.account.profile;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by abizu-alvio on 7/14/2017.
 */

public interface IAccountProfilePresenter extends IBaseViewPresenter {

    void loadAccountProfile(ManisApi manisApi);

    void loadPointProfile(ManisApi manisApi);

    void loadAccountProfile();

    void loadPointProfile();

}
