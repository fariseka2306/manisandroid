package com.ebizu.manis.mvp.snap.store.list.near;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.helper.GPSTracker;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.mvp.snap.store.SnapStoreHelper;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.WrapperSnapStore;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public class NearStorePresenter extends BaseViewPresenter implements INearStorePresenter {

    private NearStoreView nearStoreView;
    private GPSTracker gpsTracker;
    private Subscription subsNearStore;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        nearStoreView = (NearStoreView) baseView;
        gpsTracker = new GPSTracker(nearStoreView.getContext());
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsNearStore != null) subsNearStore.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void getSnapStoresNearby(int page, IBaseView.LoadType loadType) {
        checkingGps(() -> nearStoreView.getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_SNAP_STORE,
                ConfigManager.Analytic.Action.CLICK,
                "Empty State Location"
        ));
        if (!isGpsEnable()) return;
        showProgress();
        nearStoreView.setLoading(true);
        releaseSubscribe(0);
        subsNearStore = nearStoreView.getManisApi().getSnapStoresNearby(SnapStoreHelper.getSnapStoreBody(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperSnapStore>(nearStoreView) {
                    @Override
                    public void onNext(WrapperSnapStore wrapperSnapStore) {
                        super.onNext(wrapperSnapStore);
                        dissProgress();
                        nearStoreView.addNearStores(wrapperSnapStore.getSnapStoreResult(), loadType);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        nearStoreView.setLoading(false);
                        dissProgress();
                        if (!ConnectionDetector.isNetworkConnected(nearStoreView.getContext()))
                            nearStoreView.showNotificationMessage(
                                    nearStoreView.getResources().getString(R.string.error_no_connection)
                            );
                    }

                    @Override
                    public void onErrorFailure(ErrorResponse errorResponse) {
                        super.onErrorFailure(errorResponse);
                        NegativeScenarioManager.show(errorResponse, nearStoreView, NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }
                });
    }

    private void showProgress() {
        if (nearStoreView.getSnapStoreRecyclerView().isEmptyStores()) {
            nearStoreView.showProgressBar();
        } else {
            nearStoreView.getSnapStoreRecyclerView().showProgressitem();
        }
    }

    private void dissProgress() {
        if (nearStoreView.getSnapStoreRecyclerView().isEmptyStores()) {
            nearStoreView.dismissProgressBar();
        } else {
            nearStoreView.getSnapStoreRecyclerView().dismissProgressItem();
        }
        nearStoreView.getSwipeRefresh().dismissSwipe();
    }
}