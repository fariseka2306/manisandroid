package com.ebizu.manis.mvp.account.accountmenulist.settings.notification;

import android.content.Context;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.NotificationsSetting;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.rx.ResponseSubsriberLambda;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.response.WrapperNotifications;
import com.google.gson.JsonObject;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by abizu-alvio on 7/24/2017.
 */

public class NotificationsPresenter extends BaseViewPresenter implements INotificationsPresenter {

    private final String TAG = getClass().getSimpleName();
    private NotificationsView notificationsView;
    private Context context;
    private Subscription subsNotification;

    public NotificationsPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        notificationsView = (NotificationsView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsNotification != null) subsNotification.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadViewNotifications(ManisApi manisApi) {
        releaseSubscribe(0);
        subsNotification = manisApi.getNotifications()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperNotifications>(notificationsView) {
                    @Override
                    public void onNext(WrapperNotifications wrapperNotifications) {
                        super.onNext(wrapperNotifications);
                        notificationsView.showNotification(wrapperNotifications.getNotificationsSettings());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        notificationsView.showNoInternetConnection();
                        notificationsView.saveButton.setVisibility(View.INVISIBLE);
                    }
                });
    }

    @Override
    public void saveViewNotifications(ManisApi manisAPi, List<NotificationsSetting> notificationsSettingList) {
        releaseSubscribe(0);
        JsonObject jsonObject = new JsonObject();
        for (NotificationsSetting notificationsSetting : notificationsSettingList) {
            if (notificationsSetting.getValue()) {
                jsonObject.addProperty(notificationsSetting.getTitle(), 1);
            } else {
                jsonObject.addProperty(notificationsSetting.getTitle(), 0);
            }
        }
        notificationsView.getBaseActivity().showProgressBarDialog(context.getString(R.string.please_wait));
        subsNotification = manisAPi.saveAccountNotifications(jsonObject.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperNotifications>(notificationsView) {
                    @Override
                    public void onNext(WrapperNotifications wrapperNotifications) {
                        super.onNext(wrapperNotifications);
                        notificationsView.getBaseActivity().dismissProgressBarDialog();
                        notificationsView.finishActivity();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        notificationsView.dismissProgressBar();
                    }
                });
    }

    @Override
    public void loadViewNotifications() {
        releaseSubscribe(0);
        notificationsView.getBaseActivity().showProgressBarDialog(context.getString(R.string.please_wait), dialogInterface -> {
            releaseSubscribe(0);
            notificationsView.getBaseActivity().dismissProgressBarDialog();
            notificationsView.getBaseActivity().finish();
        });
        RequestBody requestBody = new RequestBodyBuilder(notificationsView.getContext())
                .setFunction(ConfigManager.Account.FUNCTION_GET_NOTIF)
                .create();
        subsNotification = getManisApiV2().getNotifications(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperNotifications>(notificationsView) {
                    @Override
                    protected void onSuccess(WrapperNotifications wrapperNotifications) {
                        super.onSuccess(wrapperNotifications);
                        notificationsView.getBaseActivity().dismissProgressBarDialog();
                        notificationsView.showNotification(wrapperNotifications.getNotificationsSettings());
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                        notificationsView.getBaseActivity().dismissProgressBarDialog();
                        notificationsView.getBaseActivity().showManisAlertDialog(message, dialogInterface -> notificationsView.getBaseActivity().finish());
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        notificationsView.getBaseActivity().dismissProgressBarDialog();
                        notificationsView.getBaseActivity().showManisAlertDialog(message, dialogInterface -> notificationsView.getBaseActivity().finish());
                    }
                });
    }

    @Override
    public void saveViewNotifications(RequestBody requestBody) {
        releaseSubscribe(0);
        notificationsView.getBaseActivity().showProgressBarDialog(context.getString(R.string.please_wait));
        subsNotification = getManisApiV2().saveAccountNotifications(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperNotifications>(notificationsView) {
                    @Override
                    protected void onSuccess(WrapperNotifications wrapperNotifications) {
                        super.onSuccess(wrapperNotifications);
                        notificationsView.getBaseActivity().dismissProgressBarDialog();
                        notificationsView.finishActivity();
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                        notificationsView.getBaseActivity().dismissProgressBarDialog();
                        notificationsView.getBaseActivity().showManisAlertDialog(message);
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        notificationsView.getBaseActivity().dismissProgressBarDialog();
                        notificationsView.getBaseActivity().showManisAlertDialog(message);
                    }
                });
    }

    @Override
    public RequestBody createAccountUpdateNotifConfigRB(List<NotificationsSetting> notificationsSettingList) {
        JsonObject jsonObject = new JsonObject();
        for (NotificationsSetting notificationsSetting : notificationsSettingList) {
            if (notificationsSetting.getValue()) {
                jsonObject.addProperty(notificationsSetting.getTitle(), 1);
            } else {
                jsonObject.addProperty(notificationsSetting.getTitle(), 0);
            }
        }
        return new RequestBodyBuilder(notificationsView.getContext())
                .setFunction(ConfigManager.Account.FUNCTION_UPDATE_NOTIF)
                .setData(jsonObject)
                .create();
    }
}