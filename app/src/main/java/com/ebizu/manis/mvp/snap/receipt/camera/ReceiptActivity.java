package com.ebizu.manis.mvp.snap.receipt.camera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.snap.SnapActivity;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptDataBody;

import java.util.ArrayList;

import static com.ebizu.manis.helper.ConfigManager.Snap.LUCKY_DRAW_INTENT_DATA;

/**
 * Created by ebizu on 8/25/17.
 */

public class ReceiptActivity extends SnapActivity {

    protected int bitmapId;
    protected ReceiptDataBody receiptDataBody;
    protected int bitmapsList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideInHorizontalAnim();
        getIntentData();
    }

    private void getIntentData() {
        bitmapId = getIntent().getExtras().getInt(ConfigManager.Snap.RECEIPT_BITMAP_ID);
        receiptDataBody = getIntent().getExtras().getParcelable(ConfigManager.Snap.RECEIPT_DATA_PARCEL);
        bitmapsList = getIntent().getExtras().getInt(ConfigManager.Snap.RECEIPT_BITMAP_COUNT);
    }

    protected void nextActivity(Class nextClassAct, int bitmapId, ReceiptDataBody receiptDataBody, int bitmapList) {
        Intent intent = new Intent(this, nextClassAct);
        intent.putExtra(ConfigManager.Snap.RECEIPT_BITMAP_ID, bitmapId);
        intent.putExtra(ConfigManager.Snap.RECEIPT_DATA_PARCEL, receiptDataBody);
        intent.putExtra(LUCKY_DRAW_INTENT_DATA, snapLuckyDraw);
        intent.putExtra(ConfigManager.Snap.RECEIPT_BITMAP_COUNT, bitmapList);
        startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
    }

    @Override
    public void finish() {
        super.finish();
        slideOutHorizontalAnim();
    }
}
