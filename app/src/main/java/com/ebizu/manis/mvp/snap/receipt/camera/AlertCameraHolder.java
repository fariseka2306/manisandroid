package com.ebizu.manis.mvp.snap.receipt.camera;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.RelativeLayout;

import com.daimajia.androidanimations.library.YoYo;
import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FARIS_mac on 30/11/17.
 */

public class AlertCameraHolder extends BaseView {

    @BindView(R.id.alertPhoneHolder)
    RelativeLayout alertPhoneHolder;

    public AlertCameraHolder(Context context) {
        super(context);
        createView(context);
    }

    public AlertCameraHolder(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public AlertCameraHolder(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AlertCameraHolder(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.rotationcamera_holder, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        ViewTreeObserver viewTreeObserver = alertPhoneHolder.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                alertPhoneHolder.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                setAnimationAlertCamera();
            }
        });
    }

    public void setAnimationAlertCamera() {
        YoYo.with(new StandDownAnimator()).pivot(YoYo.CENTER_PIVOT, YoYo.CENTER_PIVOT).duration(4000).interpolate(new AnticipateOvershootInterpolator()).repeat(0)
                .onStart(animator -> {
                    try {
                        YoYo.with(new StandDownAnimator()).duration(4000).pivot(YoYo.CENTER_PIVOT, YoYo.CENTER_PIVOT).playOn(alertPhoneHolder);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).onEnd(animator -> setAnimationAlertCamera()).playOn(alertPhoneHolder);
    }

}
