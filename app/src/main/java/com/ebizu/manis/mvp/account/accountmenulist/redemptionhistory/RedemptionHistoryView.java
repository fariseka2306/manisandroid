package com.ebizu.manis.mvp.account.accountmenulist.redemptionhistory;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.EndlessRecyclerOnScrollListener;
import com.ebizu.manis.model.RedemptionHistoryParam;
import com.ebizu.manis.model.RedemptionHistoryResult;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.view.adapter.RedemptionHistoryAdapter;
import com.ebizu.manis.view.linearlayout.ItemOffsetsDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 8/7/2017.
 */

public class RedemptionHistoryView extends BaseView implements IRedemptionHistoryView {

    @BindView(R.id.recycler_view_redemption_history)
    RecyclerView recyclerView;
    @BindView(R.id.textview_empty)
    TextView textViewEmpty;

    public int currentPage = 1;

    private boolean isLoading = false;
    public boolean isLastPage = false;

    private RedemptionHistoryParam redemptionHistoryParam;
    private RedemptionHistoryAdapter redemptionHistoryAdapter;
    private IRedemptionHistoryPresenter iRedemptionHistoryPresenter;
    private Context context;

    public RedemptionHistoryView(Context context) {
        super(context);
        createView(context);
    }

    public RedemptionHistoryView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public RedemptionHistoryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RedemptionHistoryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_redemption_history, null, false);
        redemptionHistoryAdapter = new RedemptionHistoryAdapter(context, getBaseActivity(), new ArrayList<>());
        redemptionHistoryParam = new RedemptionHistoryParam();
        this.context = context;
        addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this);
        attachPresenter(new RedemptionHistoryPresenter());
        initialize();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        this.iRedemptionHistoryPresenter = (IRedemptionHistoryPresenter) iBaseViewPresenter;
        this.iRedemptionHistoryPresenter.attachView(this);
    }

    private void initialize() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new ItemOffsetsDecoration(getContext(), R.dimen.five_dp));
        recyclerView.setAdapter(redemptionHistoryAdapter);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                redemptionHistoryParam.setPage(currentPage);
                iRedemptionHistoryPresenter.loadRedemptionHistoryNext(ManisApiGenerator.createServiceWithToken(getContext()), currentPage);
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (redemptionHistoryAdapter.getItemViewType(position)) {
                    case 0:
                        return 1;
                    case 1:
                        return 2;
                    default:
                        return -1;
                }
            }
        });
    }

    @Override
    public void onRetry() {
        super.onRetry();
        int page = 1;
        getRedemptionHistoryPresenter().loadRedemptionHistory(ManisApiGenerator.createServiceWithToken(context), page);
    }

    @Override
    public void setRedemptionHistory(List<RedemptionHistoryResult> redemptionHistory) {
        redemptionHistoryAdapter.replaceRedemptionHistory(redemptionHistory);
        if (redemptionHistory.size() < 1) {
            textViewEmpty.setVisibility(VISIBLE);
        }
        if (!isLastPage)
            redemptionHistoryAdapter.addLoadingFooter();
    }

    @Override
    public void setRedemptionHistoryNext(List<RedemptionHistoryResult> redemptionHistory) {
        redemptionHistoryAdapter.removeLoadingFooter();
        isLoading = false;
        redemptionHistoryAdapter.addAll(redemptionHistory);
        if (!isLastPage)
            redemptionHistoryAdapter.addLoadingFooter();
    }

    @Override
    public IRedemptionHistoryPresenter getRedemptionHistoryPresenter() {
        return iRedemptionHistoryPresenter;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finishActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void finishActivity() {
        getBaseActivity().finish();
        getBaseActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
