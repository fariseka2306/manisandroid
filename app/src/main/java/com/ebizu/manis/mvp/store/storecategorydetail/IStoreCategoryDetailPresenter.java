package com.ebizu.manis.mvp.store.storecategorydetail;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.StoreInterestBody;

/**
 * Created by firef on 7/12/2017.
 */

public interface IStoreCategoryDetailPresenter extends IBaseViewPresenter {

    void loadStoreCategoryDetail(ManisApi manisApi, StoreInterestBody storeInterestBody);

    void loadStoreCategoryDetailPaging(ManisApi manisApi, StoreInterestBody storeInterestBody);
}
