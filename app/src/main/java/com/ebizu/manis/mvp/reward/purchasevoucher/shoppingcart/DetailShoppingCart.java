package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.purchasevoucher.Product;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by andrifashbir on 15/11/17.
 */

public class DetailShoppingCart extends RelativeLayout {

    @BindView(R.id.text_view_product)
    TextView textViewProduct;
    @BindView(R.id.text_view_quantity)
    TextView textViewQuantity;
    @BindView(R.id.text_view_amount)
    TextView textViewAmount;
    @BindView(R.id.text_view_total_amount)
    TextView textViewTotal;
    @BindView(R.id.root_Voucher)
    LinearLayout rootLayout;

    Context mContext;

    public DetailShoppingCart(Context context) {
        super(context);
        createView(context);
    }

    public DetailShoppingCart(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public DetailShoppingCart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DetailShoppingCart(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    protected void createView(Context context) {
        mContext = context;
        View view = LayoutInflater.from(context).inflate(R.layout.view_detail_shopping_cart, null, false);
        addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    protected void setViewValue(Product product, String currency) {
        textViewProduct.setText(product.getName());
        textViewQuantity.setText(String.valueOf(product.getQty()).concat("x"));
        textViewAmount.setText(currency + " " + UtilManis.addSeparator(currency, product.getAmount()));
        textViewTotal.setText(currency + " " + UtilManis.addSeparator(currency, product.getTotalAmount()));
    }

    protected void addVoucher(List<RewardVoucher> rewardVouchers) {
        if (!rewardVouchers.isEmpty()) {
            for (RewardVoucher rewardVoucher : rewardVouchers) {
                VoucherShoppingCart voucherView = new VoucherShoppingCart(mContext);
                voucherView.setValue(rewardVoucher);
                rootLayout.addView(voucherView);
            }
        }
    }
}
