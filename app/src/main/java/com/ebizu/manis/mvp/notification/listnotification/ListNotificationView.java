package com.ebizu.manis.mvp.notification.listnotification;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.view.adapter.NotificationAdapter;
import com.ebizu.manis.view.adapter.NotificationSwipeableAdapter;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.h6ah4i.android.widget.advrecyclerview.touchguard.RecyclerViewTouchActionGuardManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu on 8/22/17.
 */

public class ListNotificationView extends BaseView implements IListNotificationView {

    private final String TAG = getClass().getSimpleName();

    /**
     * RecyclerViewSwipeAdapter
     **/
    private RecyclerView.Adapter recyclerViewAdapter;
    private NotificationSwipeableAdapter notificationSwipeableAdapter;
    private RecyclerViewSwipeManager recyclerViewSwipeManager;
    private RecyclerViewTouchActionGuardManager recyclerViewTouchActionGuardManager;

    /**
     * RecyclerViewAdapter
     **/
    private NotificationAdapter notificationAdapter;

    /**
     * Presenter
     **/
    private ListNotificationPresenter listNotificationPresenter;

    @BindView(R.id.text_view_no_notifications)
    TextView textViewNoNotifications;
    @BindView(R.id.recycler_view_notification)
    RecyclerView recyclerViewNotification;

    public ListNotificationView(Context context) {
        super(context);
        createView(context);
    }

    public ListNotificationView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ListNotificationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ListNotificationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_notification, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        initRececylerView();
        listNotificationPresenter = new ListNotificationPresenter();
        listNotificationPresenter.attachView(this);
    }

    private void initRececylerView() {
        if (notificationAdapter == null)
            createRecyclerViewAdapter();
    }

    private void createRecyclerViewSwipeAdapter() {
        recyclerViewNotification.setLayoutManager(new LinearLayoutManager(getContext()));
        notificationSwipeableAdapter = new NotificationSwipeableAdapter();
        notificationSwipeableAdapter.setEventListener(position -> {
            //Do nothing
        });
        notificationSwipeableAdapter.setHasStableIds(true);
        recyclerViewSwipeManager = new RecyclerViewSwipeManager();
        recyclerViewAdapter = recyclerViewSwipeManager.createWrappedAdapter(notificationSwipeableAdapter);
        recyclerViewSwipeManager.attachRecyclerView(recyclerViewNotification);

        recyclerViewTouchActionGuardManager = new RecyclerViewTouchActionGuardManager();
        recyclerViewTouchActionGuardManager.setInterceptVerticalScrollingWhileAnimationRunning(true);
        recyclerViewTouchActionGuardManager.setEnabled(true);
        recyclerViewTouchActionGuardManager.attachRecyclerView(recyclerViewNotification);
        recyclerViewNotification.setAdapter(recyclerViewAdapter);
    }

    private void createRecyclerViewAdapter() {
        recyclerViewNotification.setLayoutManager(new LinearLayoutManager(getContext()));
        notificationAdapter = new NotificationAdapter();
        notificationAdapter.setEventListener(this::displayEmptyNotifications);
        recyclerViewNotification.setAdapter(notificationAdapter);
    }

    public void executeSyncNotifications() {
        listNotificationPresenter.getNotificationList();
    }

    @Override
    public void setNotificationListView(List<NotificationTableList> notificationList) {
        textViewNoNotifications.setVisibility(INVISIBLE);
        recyclerViewNotification.setVisibility(VISIBLE);
        notificationAdapter.replaceNotificationList(notificationList);
    }

    @Override
    public void emptyNotificationListView() {
        textViewNoNotifications.setVisibility(VISIBLE);
        recyclerViewNotification.setVisibility(VISIBLE);
    }

    @Override
    public void deleteAllNotificationList() {
        listNotificationPresenter.deleteNotificationList();
        notificationAdapter.deleteAll();
        displayEmptyNotifications();
    }

    private void displayEmptyNotifications() {
        new Handler().postDelayed(() -> {
            textViewNoNotifications.setVisibility(VISIBLE);
            recyclerViewNotification.setVisibility(INVISIBLE);
        }, 300);
    }
}
