package com.ebizu.manis.mvp.snap.form.storedetail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.LinearLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.snap.form.FormReceiptActivity;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;
import com.ebizu.manis.view.dialog.snaptnc.SnapEarnPointTncDialog;
import com.ebizu.manis.view.dialog.snaptnc.SnapEarnPointTncPresenter;
import com.ebizu.manis.view.dialog.snaptnc.SnapEarnTicketTncDialog;
import com.ebizu.manis.view.dialog.snaptnc.SnapEarnTicketTncPresenter;
import com.ebizu.manis.view.dialog.tutor.SnapEarnTicketGuideDialog;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ebizu-User on 09/08/2017.
 */

public class ReceiptDetailActivity extends FormReceiptActivity {

    //*** View ***
    @BindView(R.id.toolbar_view)
    ToolbarView toolbarView;
    @BindView(R.id.smd_lin_start_snap)
    LinearLayout buttonStartSnap;
    @BindView(R.id.form_receipt_view)
    FormReceiptView formReceiptView;
    @Inject
    DeviceSession deviceSession;
    @Inject
    ManisSession manisSession;
    @Inject
    SnapEarnPointTncPresenter snapEarnPointTncPresenter;
    @Inject
    SnapEarnTicketTncPresenter snapEarnTicketTncPresenter;
    //*** Data ***
    private ReceiptStore receiptStore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideInHorizontalAnim();
        setContentView(R.layout.activity_snap_receipt_detail);
        getIntentData();
        ButterKnife.bind(this);
        toolbarView.setTitle(getResources().getString(R.string.smd_txt_title));
        loadTerms();
        showEarnTicketGuide();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void showEarnTicketGuide() {
        if (isSnapLuckyDraw()) {
            if (!deviceSession.isAlwaysShowGuideEarnTicket()) {
                SnapEarnTicketGuideDialog snapEarnTicketGuideDialog = new SnapEarnTicketGuideDialog(this);
                snapEarnTicketGuideDialog.setActivity(this);
                snapEarnTicketGuideDialog.show();
            }
        }
    }

    private void loadTerms() {
        snapEarnPointTncPresenter.attachDialog(new SnapEarnPointTncDialog(this));
        snapEarnTicketTncPresenter.attachDialog(new SnapEarnTicketTncDialog(this));
        snapEarnPointTncPresenter.getTermsAndSave();
        snapEarnTicketTncPresenter.getTermsAndSave();
    }

    private void getIntentData() {
        try {
            receiptStore = getIntent().getParcelableExtra(ConfigManager.Snap.MERCHANT_DATA);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void finish() {
        super.finish();
        slideOutHorizontalAnim();
    }

    @OnClick(R.id.smd_lin_start_snap)
    void onClickStartSnap() {
        if (formReceiptView.isValidInput()) {
            showHelpDialog();
        } else {
            formReceiptView.showNotificationMessage(getResources().getString(R.string.txt_input_validation_msg));
        }
    }

    private void showHelpDialog() {
        if (isSnapLuckyDraw()) {
            showLuckyDrawHelpDialog();
        } else {
            showSnapHelpDialog();
        }
    }

    private void showSnapHelpDialog() {
        if (!manisSession.getTermsSnapEarnPoints().isEmpty()) {
            if (!manisSession.isAcceptAlwaysTncSnap()) {
                SnapEarnPointTncDialog snapEarnPointTncDialog = new SnapEarnPointTncDialog(this);
                snapEarnPointTncDialog.setActivity(this);
                snapEarnPointTncDialog.setOnAcceptListener(this::startCameraActivity);
                snapEarnPointTncDialog.show();
            } else {
                startCameraActivity();
            }
        } else {
            startCameraActivity();
        }
    }

    private void showLuckyDrawHelpDialog() {
        if (!manisSession.getTermsSnapEarnTickets().isEmpty()) {
            if (!manisSession.isAcceptAlwaysTncLuckyDraw()) {
                SnapEarnTicketTncDialog snapEarnTicketTncDialog = new SnapEarnTicketTncDialog(this);
                snapEarnTicketTncDialog.setActivity(this);
                snapEarnTicketTncDialog.setOnAcceptListener(this::startCameraActivity);
                snapEarnTicketTncDialog.show();
            } else {
                startCameraActivity();
            }
        } else {
            startCameraActivity();
        }
    }

    private void startCameraActivity() {
        if (getPermissionManager().checkPermissionCamera())
            startActivityCamera(receiptStore, formReceiptView.getTotalAmount());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ConfigManager.Permission.CAMERA_REQUEST_CODE)
            if (getPermissionManager().hasPermissions(permissions)) {
                startCameraActivity();
            }
    }
}
