package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistorypyshicalfactory;

import android.graphics.drawable.Drawable;

/**
 * Created by halim_ebizu on 11/16/17.
 */

public interface PurchasePyshicalStatus {

    String getDeliveryStatus();

    String getStatus();

    Drawable imageIcon();

    String getInstruction();

    int getInstructionVisibility();
}
