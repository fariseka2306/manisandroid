package com.ebizu.manis.mvp.branddetail;

import com.ebizu.manis.model.snap.SnapStoreResult;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by Raden on 7/21/17.
 */

public interface IBrandDetailView {

    void addViewBrandDetail(SnapStoreResult snapStoreResult, IBaseView.LoadType loadType);

    void failLoadViewBrand(IBaseView.LoadType loadType, Throwable e);

    void noDataBrandDetail();


}
