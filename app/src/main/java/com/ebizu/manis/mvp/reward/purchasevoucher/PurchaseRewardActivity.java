package com.ebizu.manis.mvp.reward.purchasevoucher;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.sdk.reward.models.Reward;

/**
 * Created by ebizu on 9/27/17.
 */

public class PurchaseRewardActivity extends BaseActivity {

    protected RewardVoucher reward;
    protected ShoppingCart shoppingCart;
    protected Order order;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getIntentData();
    }

    private void getIntentData() {
        try {
            reward = getIntent().getParcelableExtra(ConfigManager.PurchaseVoucher.REWARD_PARCELABLE);
            shoppingCart = getIntent().getParcelableExtra(ConfigManager.PurchaseVoucher.SHOPPING_CART_PARCELABLE);
            order = getIntent().getParcelableExtra(ConfigManager.PurchaseVoucher.ORDER_PARCELABLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConfigManager.PurchaseVoucher.REQUEST_CODE) {
            if (resultCode == ConfigManager.PurchaseVoucher.SUCCESS_RESULT_CODE) {
                setResult(ConfigManager.PurchaseVoucher.SUCCESS_RESULT_CODE);
                finish();
            } else if (resultCode == ConfigManager.PurchaseVoucher.TRANS_FINISH_RESULT_CODE) {
                setResult(ConfigManager.PurchaseVoucher.TRANS_FINISH_RESULT_CODE);
                finish();
            }
        }
    }

    public void nextPurchaseVoucher(PurchaseRewardActivity activityClass, Reward reward) {
        Intent intent = new Intent(this, activityClass.getClass());
        intent.putExtra(ConfigManager.PurchaseVoucher.REWARD_PARCELABLE, reward);
        startActivityForResult(intent, ConfigManager.PurchaseVoucher.REQUEST_CODE);
    }

    public void nextPurchaseVoucher(Class activityClass, RewardVoucher reward, ShoppingCart shoppingCart) {
        Intent intent = new Intent(this, activityClass);
        intent.putExtra(ConfigManager.PurchaseVoucher.REWARD_PARCELABLE, reward);
        intent.putExtra(ConfigManager.PurchaseVoucher.SHOPPING_CART_PARCELABLE, shoppingCart);
        startActivityForResult(intent, ConfigManager.PurchaseVoucher.REQUEST_CODE);
    }

    public void nextPurchaseVoucher(Class activityClass, Order order) {
        Intent intent = new Intent(this, activityClass);
        if (null != reward)
            intent.putExtra(ConfigManager.PurchaseVoucher.REWARD_PARCELABLE, reward);
        if (null != shoppingCart)
            intent.putExtra(ConfigManager.PurchaseVoucher.SHOPPING_CART_PARCELABLE, shoppingCart);
        intent.putExtra(ConfigManager.PurchaseVoucher.ORDER_PARCELABLE, order);
        startActivityForResult(intent, ConfigManager.PurchaseVoucher.REQUEST_CODE);
    }

}