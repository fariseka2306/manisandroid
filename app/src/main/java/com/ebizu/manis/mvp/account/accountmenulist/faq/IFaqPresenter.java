package com.ebizu.manis.mvp.account.accountmenulist.faq;

import com.ebizu.manis.model.Faq;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by abizu-alvio on 7/31/2017.
 */

public interface IFaqPresenter extends IBaseViewPresenter {

    void loadFaq(ManisApi manisApi, Faq faq);

}
