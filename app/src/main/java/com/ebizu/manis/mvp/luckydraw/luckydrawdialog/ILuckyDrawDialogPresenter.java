package com.ebizu.manis.mvp.luckydraw.luckydrawdialog;

import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;

/**
 * Created by ebizu on 08/08/17.
 */

public interface ILuckyDrawDialogPresenter extends IBaseViewPresenter {

    void loadLuckyDrawEntryData(IBaseView.LoadType loadType, RequestBody requestBody);

    void loadLuckyDrawWinner(IBaseView.LoadType loadType, RequestBody requestBody);

    RequestBody createLuckyDrawEntriesRB(int page);

    RequestBody createLuckyDrawWinnersRB(int page);

}
