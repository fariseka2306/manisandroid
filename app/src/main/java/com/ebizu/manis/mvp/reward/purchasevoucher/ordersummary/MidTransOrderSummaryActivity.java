package com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.mvp.reward.purchasevoucher.validation.TransactionValidation;
import com.ebizu.manis.payment.midtrans.MidTransTransactionRequest;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;

/**
 * Created by ebizu on 10/4/17.
 */

public class MidTransOrderSummaryActivity extends OrderSummaryActivity
        implements TransactionFinishedCallback {

    private TransactionValidation transactionValidation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SdkUIFlowBuilder.init(this, BuildConfig.MIDTRANS_CLIENT_KEY, BuildConfig.MIDTRANS_BASE_URL, this)
                .enableLog(true)
                .useBuiltInTokenStorage(false)
                .buildSDK();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void starTransaction() {
        super.starTransaction();
        MidTransTransactionRequest midTransTransactionRequest =
                new MidTransTransactionRequest(this, order);
        MidtransSDK.getInstance().setTransactionRequest(midTransTransactionRequest);
        MidtransSDK.getInstance().startPaymentUiFlow(this);
    }

    @Override
    public void onTransactionFinished(TransactionResult transactionResult) {
        if (!transactionResult.isTransactionCanceled()) {
            orderSummaryView.setTransactionResult(transactionResult);
            orderSummaryPresenter.sendPurchaseStatus(transactionResult, order);
        }
    }

}
