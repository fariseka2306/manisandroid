package com.ebizu.manis.mvp.account.accountmenulist.profile.changeprofilephoto;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseActivity;
import com.facebook.CallbackManager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/27/2017.
 */

public class ChangeProfilePhotoActivity extends BaseActivity {

    @BindView(R.id.change_profile)
    ChangeProfilePhotoView changeProfilePhotoView;
    @Inject
    ManisSession manisSession;

    private Context context;
    private CallbackManager callbackManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        slideInVerticalAnim();
        setContentView(R.layout.change_profile_photo);
        ButterKnife.bind(this);
        context = this;
        callbackManager = CallbackManager.Factory.create();
        initView();
    }

    @Override
    protected void prepareInjection() {
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {
        changeProfilePhotoView.setActivity(this);
        changeProfilePhotoView.attachPresenter(new ChangePresenter(context));
        changeProfilePhotoView.setProfilePhoto(manisSession.getAccountSession());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ConfigManager.UpdateProfile.CAMERA) {
            if (changeProfilePhotoView.mImageUri != null)
                changeProfilePhotoView.setProfilePhoto(Uri.parse(changeProfilePhotoView.mImagePath));
        } else if (resultCode == RESULT_OK && requestCode == ConfigManager.UpdateProfile.GALLERY) {
            if (data.getData() != null) changeProfilePhotoView.setProfilePhoto(data.getData());
        } else {
            if (changeProfilePhotoView.callbackManager != null)
                changeProfilePhotoView.callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void finish() {
        super.finish();
        slideOutVerticalAnim();
    }
}