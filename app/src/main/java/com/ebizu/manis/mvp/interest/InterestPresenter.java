package com.ebizu.manis.mvp.interest;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.service.manis.response.WrapperInterest;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public class InterestPresenter extends BaseViewPresenter implements IInterestPresenter {

    private InterestView interestView;
    private Subscription subsInterest;

    @Override
    public void attachView(BaseView baseView) {
        interestView = (InterestView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsInterest != null) subsInterest.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void getUserInterest(ManisApi manisApi) {
        releaseSubscribe(0);
        interestView.showProgressBar();
        subsInterest = manisApi.getInterests()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperInterest>(interestView) {
                    @Override
                    public void onNext(WrapperInterest wrapperInterest) {
                        super.onNext(wrapperInterest);
                        interestView.dismissProgressBar();
                        interestView.setInterestView(wrapperInterest.getInterests());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        interestView.dismissProgressBar();
                        if (!ConnectionDetector.isNetworkConnected(interestView.getContext())) {
                            interestView.showNoInternetConnection();
                        } else {
                            interestView.showNotificationMessage(interestView.getContext().getResources().getString(R.string.server_busy));
                        }
                    }
                });
    }

    @Override
    public void saveInterest(ManisApi manisApi, String data) {
        interestView.showProgressSave();
        interestView.invisibleSaveButton();
        subsInterest = manisApi.saveInterest(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<ResponseData>(interestView) {

                    @Override
                    public void onNext(ResponseData responseData) {
                        super.onNext(responseData);
                        interestView.dismissProgressSave();
                        interestView.visibleSaveButton();
                        interestView.getOnInterestListener().onSuccessSave();
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        interestView.dismissProgressSave();
                        interestView.visibleSaveButton();
                        if (!ConnectionDetector.isNetworkConnected(interestView.getContext())) {
                            interestView.showNotificationMessage(interestView.getContext().getResources().getString(R.string.error_no_connection));
                        } else {
                            interestView.showNotificationMessage(interestView.getContext().getResources().getString(R.string.server_busy));
                        }
                    }
                });
    }
}