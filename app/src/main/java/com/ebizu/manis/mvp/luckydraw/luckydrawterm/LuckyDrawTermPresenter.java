package com.ebizu.manis.mvp.luckydraw.luckydrawterm;

import android.util.Log;

import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawTerm;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class LuckyDrawTermPresenter extends BaseViewPresenter implements ILuckyDrawTermPresenter {

    private final String TAG = getClass().getSimpleName();

    private LuckyDrawTermView luckyDrawTermView;
    private Subscription subsLuckyDraw;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        luckyDrawTermView = (LuckyDrawTermView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsLuckyDraw != null) subsLuckyDraw.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadLuckyDrawTerm(ManisApi manisApi) {
        releaseSubscribe(0);
        subsLuckyDraw = manisApi.getLuckyDrawTerm().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperLuckyDrawTerm>(luckyDrawTermView) {
                    @Override
                    public void onNext(WrapperLuckyDrawTerm wrapperLuckyDrawTerm) {
                        super.onNext(wrapperLuckyDrawTerm);
                        luckyDrawTermView.setLuckyDrawTerm(wrapperLuckyDrawTerm.getLuckyDrawTerm());
                    }
                });
    }
}
