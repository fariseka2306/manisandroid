package com.ebizu.manis.mvp.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.Constants;
import com.ebizu.manis.helper.Dummy;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.account.AccountFragment;
import com.ebizu.manis.mvp.beacon.BeaconPresenter;
import com.ebizu.manis.mvp.home.HomeFragment;
import com.ebizu.manis.mvp.luckydraw.luckydrawdialog.LuckyDrawIntroActivity;
import com.ebizu.manis.mvp.luckydraw.luckydrawscreen.LuckyDrawFragment;
import com.ebizu.manis.mvp.main.activityresult.MainRequest;
import com.ebizu.manis.mvp.main.toolbar.ToolbarMainMainView;
import com.ebizu.manis.mvp.mission.MissionFragment;
import com.ebizu.manis.mvp.notification.NotificationFragment;
import com.ebizu.manis.mvp.reward.newreward.NewRewardFragment;
import com.ebizu.manis.mvp.reward.rewardlistcategory.RewardConverterAdapter;
import com.ebizu.manis.mvp.store.storenearby.StoreFragment;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.BeaconPromoBody;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.view.adapter.AdapterHolder;
import com.ebizu.manis.view.dialog.review.GetReviewPresenter;
import com.ebizu.manis.view.dialog.review.ReviewPopUpDialog;
import com.ebizu.manis.view.dialog.review.reviewdialog.IReviewDialog;
import com.ebizu.manis.view.walkthrough.WalkthroughActivityFirst;
import com.ebizu.manis.view.walkthrough.WalkthroughtActivitySecond;
import com.ebizu.sdk.reward.models.Reward;
import com.viewpagerindicator.TitlePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UnreadConversationCountListener;
import io.intercom.android.sdk.identity.Registration;

public class MainActivity extends BaseActivity implements IReviewDialog {

    @BindView(R.id.mn_rel_home)
    RelativeLayout relHome;
    @BindView(R.id.mn_rel_discover)
    RelativeLayout relDiscover;
    @BindView(R.id.mn_rel_redeem)
    RelativeLayout relRedeem;
    @BindView(R.id.mn_rel_profile)
    RelativeLayout relProfile;
    @BindView(R.id.mn_img_home)
    ImageView imgHome;
    @BindView(R.id.mn_img_discover)
    ImageView imgDiscover;
    @BindView(R.id.mn_img_redeem)
    ImageView imgRedeem;
    @BindView(R.id.mn_img_profile)
    ImageView imgProfile;
    @BindView(R.id.parent)
    DrawerLayout parentDrawable;
    @BindView(R.id.toolbar_main)
    ToolbarMainMainView toolbarMainMainView;
    @BindView(R.id.page_indicator)
    TitlePageIndicator pageIndicator;

    private Context context;
    private ImageView imgLuckyDraw;
    private RelativeLayout relLuckyDraw;
    private ViewPager mPager;
    private int page;
    private NotificationFragment mNotificationFragment;
    private ActionBarDrawerToggle mDrawerToggle;
    private boolean isAlreadyChangedView;
    private AppCompatActivity mainActivity;
    private GetReviewPresenter getReviewPresenter;

    public HomeFragment homeFragment;
    private StoreFragment storeFragment;
    private NewRewardFragment newRewardFragment;
    private boolean exit = false;
    public MissionFragment missionFragment;

    //************* Main Activity request code *************
    private MainRequest mainRequest;

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int page) {
            setCurrentItemHeader(page);
            UtilManis.closeKeyboard(MainActivity.this, mPager);
            isAlreadyChangedView = true;
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // not implemented method
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // not implemented method
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
        mainRequest = new MainRequest(this);

        this.toolbarMainMainView.setActivity(this);
        this.toolbarMainMainView.getToolbarPresenter().loadGetPoint(ManisApiGenerator.createServiceWithToken(context));
        mNotificationFragment = (NotificationFragment) getSupportFragmentManager().findFragmentById(R.id.right_slider);

        this.getReviewPresenter = new GetReviewPresenter(context);
        getReviewPresenter.getReviewStatus(this);

        parentDrawable.requestDisallowInterceptTouchEvent(true);

        Intercom.client().setLauncherVisibility(Intercom.VISIBLE);
        Intercom.client().registerIdentifiedUser(Registration.create().withUserId(BuildConfig.INTERCOM_APIID));

        declareView();
        showWalkThroughView();
        setOnListenerNotif();
//        testBeacon();
        setUnreadCountIntercom();

    }

    private void setUnreadCountIntercom() {
        int unreadCount = Intercom.client().getUnreadConversationCount();
        setBadgeVisibility(unreadCount);
    }

    private void setBadgeVisibility(int unreadCount) {
        if (unreadCount == 0) {
            Intercom.client().setLauncherVisibility(Intercom.GONE);
        } else {
            Intercom.client().setLauncherVisibility(Intercom.VISIBLE);
        }
    }

    private final UnreadConversationCountListener unreadConversationCountListener = new UnreadConversationCountListener() {
        @Override
        public void onCountUpdate(int unreadCount) {
            setBadgeVisibility(unreadCount);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        Intercom.client().addUnreadConversationCountListener(unreadConversationCountListener);
        Intercom.client().handlePushMessage();
        toolbarMainMainView.getToolbarPresenter().loadTotalNotifUnread();
        SharedPreferences prefFirst = getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
        if (UtilSessionFirstIn.isShowWalktroughNotif(prefFirst)) {
            toolbarMainMainView.setCountNotification(2);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intercom.client().removeUnreadConversationCountListener(unreadConversationCountListener);
    }

    private void testBeacon() {
        BeaconPresenter beaconPresenter = new BeaconPresenter(this);
        BeaconPromoBody beaconPromoBody1 = new BeaconPromoBody();
        beaconPromoBody1.setMajor(Dummy.BeaconPromo.MAJOR);
        beaconPromoBody1.setMinor(Dummy.BeaconPromo.MINOR);
        beaconPromoBody1.setUuid(Dummy.BeaconPromo.BEACON_UUID);
        beaconPromoBody1.setSn(Dummy.BeaconPromo.SERIAL);
        BeaconPromoBody beaconPromoBody2 = new BeaconPromoBody();
        beaconPromoBody2.setMajor(Dummy.BeaconPromo2.MAJOR);
        beaconPromoBody2.setMinor(Dummy.BeaconPromo2.MINOR);
        beaconPromoBody2.setSn(Dummy.BeaconPromo2.SERIAL);
        beaconPromoBody2.setUuid(Dummy.BeaconPromo2.BEACON_UUID);
        BeaconPromoBody beaconPromoBody3 = new BeaconPromoBody();
        beaconPromoBody3.setMajor(Dummy.BeaconPromo3.MAJOR);
        beaconPromoBody3.setMinor(Dummy.BeaconPromo3.MINOR);
        beaconPromoBody3.setSn(Dummy.BeaconPromo3.SERIAL);
        beaconPromoBody3.setUuid(Dummy.BeaconPromo3.BEACON_UUID);

        declareView();
        setCurrentItemHeader(page);
        beaconPresenter.getBeaconPromos(ManisApiGenerator.createServiceWithToken(this), beaconPromoBody1);
        beaconPresenter.getBeaconPromos(ManisApiGenerator.createServiceWithToken(this), beaconPromoBody2);
        beaconPresenter.getBeaconPromos(ManisApiGenerator.createServiceWithToken(this), beaconPromoBody3);
        new Handler().postDelayed(() -> toolbarMainMainView.getToolbarPresenter().loadTotalNotifUnread(), 6000);
    }

    private void showWalkThroughView() {
        SharedPreferences prefFirst = getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
        if (UtilSessionFirstIn.isShowWalktrough(prefFirst)) {
            Intent intent = new Intent(context, WalkthroughActivityFirst.class);
            startActivityForResult(intent, ConfigManager.Walkthrough.WALKTHROUGH);
            overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        }
    }

    public void initWalkThroughNotification() {
        SharedPreferences prefFirst = getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
        if (UtilSessionFirstIn.isShowWalktroughNotif(prefFirst)) {
            Intent intent = new Intent(context, WalkthroughtActivitySecond.class);
            startActivityForResult(intent, ConfigManager.Walkthrough.WALKTHROUGH);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    private void checkLuckyDrawIntro() {
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(getBaseContext(), LuckyDrawIntroActivity.class);
            startActivity(intent);
        }, 500);
    }

    public void showLuckyDraw() {
        new Handler().postDelayed(() -> {
            SharedPreferences prefFirst = getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
            UtilSessionFirstIn.setShowWalktroughNotif(prefFirst, false);
            UtilSessionFirstIn.setShowLuckyDrawIntro(prefFirst, true);
            checkLuckyDrawIntro();
        }, 500);
    }

    private void declareView() {
        mDrawerToggle = new ActionBarDrawerToggle(this, parentDrawable, R.string.app_name, R.string.app_name) {};
        homeFragment = new HomeFragment();
        newRewardFragment = new NewRewardFragment();
        missionFragment = new MissionFragment();
        storeFragment = new StoreFragment();
        homeFragment.setOnRefreshHomeListener(() ->
                toolbarMainMainView.getToolbarPresenter().loadGetPoint(
                        ManisApiGenerator.createServiceWithToken(this)));
        AdapterHolder mAdapter = new AdapterHolder(getSupportFragmentManager(), getFragments());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        mPager.addOnPageChangeListener(pageChangeListener);
        mPager.setOffscreenPageLimit(Constants.MAX_FRAGMENT_TO_DESTROY);

        imgLuckyDraw = (ImageView) findViewById(R.id.mn_img_draw);
        relLuckyDraw = (RelativeLayout) findViewById(R.id.mn_rel_draw);
        relHome.setOnClickListener(new HeaderListener(Constants.POS_HOME));
        relDiscover.setOnClickListener(new HeaderListener(Constants.POS_DISCOVER));
        relLuckyDraw.setOnClickListener(new HeaderListener(Constants.POS_LUCKY_DRAW));
        relRedeem.setOnClickListener(new HeaderListener(Constants.POS_REDEEM));
        relProfile.setOnClickListener(new HeaderListener(Constants.POS_PROFILE));

        relHome.setSelected(true);
        homeFragment.setOnRewardClickListener((Reward reward) -> {
            RewardVoucher rewardVoucher = RewardConverterAdapter.convertToRewardVoucer(reward);
            newRewardFragment.showRewardSearch(rewardVoucher);
        });
//        homeFragment.setOnRewardVoucherHomeListener(rewardVoucher -> {
//            newRewardFragment.showRewardSearch(rewardVoucher);
//        });
    }

    public void switchToRedeem() {
        mPager.setCurrentItem(3);
    }

    private void setCurrentItemHeader(int position) {
        relHome.setSelected(position == 0);
        relDiscover.setSelected(position == 1);
        relLuckyDraw.setSelected(position == 2);
        relRedeem.setSelected(position == 3);
        relProfile.setSelected(position == 4);
        imgHome.setImageResource(position == 0 ? R.drawable.tab_bar_home_active : R.drawable.tab_bar_home);
        imgDiscover.setImageResource(position == 1 ? R.drawable.tab_bar_stores_active : R.drawable.tab_bar_stores);
        imgLuckyDraw.setImageResource(position == 2 ? R.drawable.tab_bar_missions_active : R.drawable.tab_bar_missionsgrey);
        imgRedeem.setImageResource(position == 3 ? R.drawable.tab_bar_rewards_active : R.drawable.tab_bar_rewards);
        imgProfile.setImageResource(position == 4 ? R.drawable.luckydraw_icon_select : R.drawable.luckydraw_iconwhite);
        switch (position) {
            case 0:
                toolbarMainMainView.invisbleTitle();
                break;
            case 1:
                toolbarMainMainView.setTitle(getString(R.string.hm_stores));
                break;
            case 2:
                toolbarMainMainView.setTitleMission(getString(R.string.hm_missions));
                break;
            case 3:
                toolbarMainMainView.setTitle(getString(R.string.hm_rewards));
                break;
            case 4:
                toolbarMainMainView.setTitle(getString(R.string.hm_draws));
                break;
            case 5:
                toolbarMainMainView.setTitle(getString(R.string.hm_account));
                break;
        }
    }

    private List<Fragment> getFragments() {
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(homeFragment);
        fragmentList.add(storeFragment);
        fragmentList.add(missionFragment);
        fragmentList.add(newRewardFragment);
        fragmentList.add(new LuckyDrawFragment());
        fragmentList.add(new AccountFragment());
        return fragmentList;
    }

    @Override
    public void setReview(ResponseData responseData) {
        if (responseData.getSuccess()) {
            ReviewPopUpDialog reviewPopUpDialog = new ReviewPopUpDialog(context);
            reviewPopUpDialog.setActivity(this);
            reviewPopUpDialog.show();
        }
    }

    private class HeaderListener implements View.OnClickListener {
        private int position;

        HeaderListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            if (mPager != null && mPager.getCurrentItem() != position) {
                mPager.setCurrentItem(position);
            }
            UtilManis.closeKeyboard(baseActivity(), v);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mainRequest.doJobRequest(requestCode, resultCode, data);
    }

    private void setOnListenerNotif() {
        toolbarMainMainView.setOnLisetenerNotification(() -> {
            if (parentDrawable != null)
                if (parentDrawable.isDrawerOpen(Gravity.END)) {
                    parentDrawable.closeDrawer(Gravity.END);
                } else {
                    parentDrawable.openDrawer(Gravity.END);
                    mNotificationFragment.syncNotificationView();
                    toolbarMainMainView.setCountNotification(0);
                }
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.MAIN_ACTIVITY,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Notification"
            );
        });
    }

    @Override
    public void onBackPressed() {
        if (parentDrawable.isDrawerOpen(Gravity.END)) {
            closeDrawer();
        } else {
            if (!exit) {
                exit = true;
                Toast.makeText(this, R.string.text_exit_info, Toast.LENGTH_SHORT).show();
            } else {
                finish();
            }
        }
    }

    public void closeDrawer() {
        if (parentDrawable != null) {
            if (parentDrawable.isDrawerOpen(Gravity.END)) {
                parentDrawable.closeDrawer(Gravity.END);
            } else {
                parentDrawable.openDrawer(Gravity.END);
            }
        }

    }

    public void switchToAccount() {
        mPager.setCurrentItem(5);
    }

    public StoreFragment getStoreFragment() {
        return storeFragment;
    }
}
