package com.ebizu.manis.mvp.store.storenearby;

import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbody.StoreBody;
import com.ebizu.manis.service.manis.requestbody.StoreSearchBody;

/**
 * Created by ebizu on 9/11/17.
 */

public interface IStoreNearbyPresenter extends IBaseViewPresenter {

    void loadNearStores(StoreBody storeBody, IBaseView.LoadType loadType);

    void loadSearchStore(StoreSearchBody storeSearchBody, IBaseView.LoadType loadType);

}
