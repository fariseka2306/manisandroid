package com.ebizu.manis.mvp.reward.purchasevoucher.validation;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Created by ebizu on 10/4/17.
 */

public abstract class TransactionStatus implements ITransactionStatus{

    protected Context context;

    public TransactionStatus(Context context) {
        this.context = context;
    }

    @Override
    public String statusMolPay() {
        return null;
    }

    @Override
    public String statusMidTrans() {
        return "";
    }

    @Override
    public String statusPayment() {
        return "";
    }

    @Override
    public String statusTransaction() {
        return "";
    }

    @Override
    public Drawable iconStatus() {
        return null;
    }
}
