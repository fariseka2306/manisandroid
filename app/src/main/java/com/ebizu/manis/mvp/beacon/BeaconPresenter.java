package com.ebizu.manis.mvp.beacon;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.database.litepal.NotificationDatabase;
import com.ebizu.manis.helper.TimeHelper;
import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.model.notification.beacon.BeaconPromo;
import com.ebizu.manis.mvp.beacon.interval.NotificationBeaconInterval;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.mvp.main.toolbar.ToolbarMainMainView;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.BeaconPromoBody;
import com.google.gson.Gson;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ebizu on 8/23/17.
 */

public class BeaconPresenter implements IBeaconPresenter {

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private BeaconNotification beaconNotification;
    private DeviceSession deviceSession;

    public BeaconPresenter(Context context) {
        this.context = context;
        this.beaconNotification = new BeaconNotification(context);
        this.deviceSession = new DeviceSession(context);
    }

    @Override
    public void getBeaconPromos(ManisApi manisApi, BeaconPromoBody beaconPromoBody) {
        manisApi.getBeaconPromos(new Gson().toJson(beaconPromoBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(wrapperBeaconPromo -> {
                            if (wrapperBeaconPromo.getSuccess()) {
                                saveToDatabase(wrapperBeaconPromo.getBeaconPromo());
                            }
                        },
                        Throwable::printStackTrace);
    }

    @Override
    public void getIntevalBeacon(ManisApi manisApi) {
        manisApi.getIntervalBeacon()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(wrapperInterval -> deviceSession.setSessionIntervalBeacon(wrapperInterval.getIntervalBeaconPromo()),
                        Throwable::printStackTrace);
    }

    private void saveToDatabase(BeaconPromo beaconPromo) {
        NotificationDatabase notificationDb = NotificationDatabase.getInstance();
        if (notificationDb.dataIsExist(beaconPromo)) {
            NotificationTableList notification = notificationDb.getNotification(beaconPromo.getId());
            if (!isIntervalNotif(notification)) {
                notificationDb.delete(notification);
                insert(beaconPromo);
            }
        } else {
            insert(beaconPromo);
        }
    }

    private boolean isIntervalNotif(NotificationTableList notification) {
        long intervalMillis = NotificationBeaconInterval.getInterval(deviceSession.getSessionIntervalBeacon());
        long timeNow = TimeHelper.getTimeStamp();
        long savedTime = notification.getSavedTime();
        return timeNow < (savedTime + intervalMillis);
    }

    private void insert(BeaconPromo beaconPromo) {
        NotificationDatabase.getInstance().insert(beaconPromo);
        beaconNotification.showBeaconNotification(beaconPromo);
        if (context instanceof MainActivity)
            loadNotifUnread();
    }

    private void loadNotifUnread() {
        try {
            MainActivity mainActivity = ((MainActivity) context);
            ToolbarMainMainView toolbarMainMainView = (ToolbarMainMainView) mainActivity.findViewById(R.id.toolbar_main);
            toolbarMainMainView.getToolbarPresenter().loadTotalNotifUnread();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}