package com.ebizu.manis.mvp.snap.store.list.recent;

import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public interface IRecentStorePresenter extends IBaseViewPresenter {

    void getSnapStoresRecent(int page, IBaseView.LoadType loadType);

}
