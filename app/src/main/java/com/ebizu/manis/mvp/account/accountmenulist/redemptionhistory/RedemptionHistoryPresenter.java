package com.ebizu.manis.mvp.account.accountmenulist.redemptionhistory;

import android.util.Log;
import android.view.View;

import com.ebizu.manis.model.RedemptionHistoryParam;
import com.ebizu.manis.model.RedemptionHistoryResult;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.response.WrapperRedemptionHistory;
import com.google.gson.Gson;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by abizu-alvio on 8/7/2017.
 */

public class RedemptionHistoryPresenter extends BaseViewPresenter implements IRedemptionHistoryPresenter {

    private final String TAG = getClass().getSimpleName();

    private RedemptionHistoryView redemptionHistoryView;
    private Subscription subsRedemption;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        redemptionHistoryView = (RedemptionHistoryView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsRedemption != null) subsRedemption.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadRedemptionHistory(ManisApi manisApi, int page) {
        releaseSubscribe(0);
        RedemptionHistoryParam redemptionHistoryParam = new RedemptionHistoryParam();
        redemptionHistoryParam.setPage(page);
        redemptionHistoryParam.setSize(20);
        redemptionHistoryView.showProgressBar();
        subsRedemption = manisApi.getRedemptionHistory(
                new Gson().toJson(redemptionHistoryParam))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperRedemptionHistory>(redemptionHistoryView) {
                    @Override
                    public void onNext(WrapperRedemptionHistory wrapperRedemptionHistory) {
                        super.onNext(wrapperRedemptionHistory);
                        List<RedemptionHistoryResult> redemptionHistoryResults = wrapperRedemptionHistory.getClaimedData().getGetRedemptionHistoryResultList();
                        redemptionHistoryView.isLastPage = !wrapperRedemptionHistory.getClaimedData().getMore();
                        redemptionHistoryView.setRedemptionHistory(redemptionHistoryResults);
                        redemptionHistoryView.dismissProgressBar();
                        Log.d(TAG, "RedemptionHistory : " + wrapperRedemptionHistory.toString());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        redemptionHistoryView.textViewEmpty.setVisibility(View.VISIBLE);
                        redemptionHistoryView.showNoInternetConnection();
                    }
                });
    }

    @Override
    public void loadRedemptionHistoryNext(ManisApi manisApi, int page) {
        releaseSubscribe(0);
        RedemptionHistoryParam redemptionHistoryParam = new RedemptionHistoryParam();
        redemptionHistoryParam.setPage(page);
        redemptionHistoryParam.setSize(20);

        subsRedemption = manisApi.getRedemptionHistory(
                new Gson().toJson(redemptionHistoryParam))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperRedemptionHistory>(redemptionHistoryView) {
                    @Override
                    public void onNext(WrapperRedemptionHistory wrapperRedemptionHistory) {
                        super.onNext(wrapperRedemptionHistory);
                        redemptionHistoryView.isLastPage = !wrapperRedemptionHistory.getClaimedData().getMore();
                        List<RedemptionHistoryResult> redemptionHistoryResults = wrapperRedemptionHistory.getClaimedData().getGetRedemptionHistoryResultList();
                        redemptionHistoryView.setRedemptionHistoryNext(redemptionHistoryResults);
                        Log.d(TAG, "RedemptionHistory : " + wrapperRedemptionHistory.toString());
                    }
                });
    }
}
