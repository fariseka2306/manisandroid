package com.ebizu.manis.mvp.store.storedetaillocation;

import com.ebizu.manis.root.IBaseViewPresenter;

/**
 * Created by ebizu on 9/11/17.
 */

public interface IStoreLocationPresenter extends IBaseViewPresenter {

    void loadMap();
}
