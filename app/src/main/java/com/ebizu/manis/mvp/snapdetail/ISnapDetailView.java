package com.ebizu.manis.mvp.snapdetail;

import com.ebizu.manis.model.snap.SnapData;

import java.util.ArrayList;

/**
 * Created by Raden on 7/17/17.
 */

public interface ISnapDetailView {
//    void loadViewSnapDetails(ArrayList<SnapDetailsData> snapDetailsDatas);

    void loadViewSnapDetails(ArrayList<SnapData> snapDetailsDatas);

}
