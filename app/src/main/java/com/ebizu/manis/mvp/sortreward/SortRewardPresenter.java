package com.ebizu.manis.mvp.sortreward;

import android.content.Context;

import com.ebizu.manis.helper.ConfigManager;

/**
 * Created by Raden on 8/3/17.
 */

public class SortRewardPresenter implements ISortRewardPresenter{

    public Context context;
    public ISortRewardView iSortRewardView;


    public SortRewardPresenter(Context context){
        this.context = context;
    }

    public void attachSort(ISortRewardView iSortRewardView){
        this.iSortRewardView = iSortRewardView;
    }

    @Override
    public void loadSortReward() {

    }
}
