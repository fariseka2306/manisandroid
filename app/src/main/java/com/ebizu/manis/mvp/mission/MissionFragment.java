package com.ebizu.manis.mvp.mission;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperienceMissionsRB;
import com.ebizu.manis.service.manis.requestbodyv2.data.ShareExperienceMissionData;
import com.ebizu.manis.view.linearlayout.FixedRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerOriginPageMissions;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class MissionFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.share_experience_view)
    MissionView missionView;
    @BindView(R.id.swipe_refresh)
    FixedRefreshLayout swipeRefresh;

    private ShareExperienceMissionsRB requestBody;

    public MissionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_share_experience, container, false);
        ButterKnife.bind(this, view);
        swipeRefresh.setOnRefreshListener(this);
        swipeRefresh.setColorSchemeResources(R.color.base_pink);
        missionView.setActivity((BaseActivity) getActivity());
        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            loadShareExperienceMission();
            TrackerManager.getInstance().setTrackerData(
                    new TrackerStandartRequest(kTrackerOriginPageMissions, kTrackerOriginPageMissions, "", "", "", "")
            );
        }
    }

    @Override
    public void onRefresh() {
        loadShareExperienceMission();
        if (swipeRefresh.isRefreshing()) {
            swipeRefresh.setRefreshing(false);
        }
    }

    private void loadShareExperienceMission() {
        missionView.setActivity((BaseActivity) getActivity());
        missionView.attachPresenter(new MissionPresenter());
        missionView.resetMissionParam();
        missionView.isRefresh = true;
        missionView.layoutEmpty.setVisibility(View.GONE);
        ShareExperienceMissionData data = new ShareExperienceMissionData(getContext());
        requestBody = new ShareExperienceMissionsRB();
        data.setCurrentPage(1);
        requestBody.setData(data);
        missionView.getSharedExperiencePresenter().loadShareExperienceMission(requestBody);
    }

    public void showInviteFriendsDialog() {
        missionView.inviteFriends();
    }
}
