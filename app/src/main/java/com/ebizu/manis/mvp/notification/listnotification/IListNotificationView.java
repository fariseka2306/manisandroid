package com.ebizu.manis.mvp.notification.listnotification;

import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.root.IBaseView;

import java.util.List;

/**
 * Created by Ebizu on 8/22/17.
 */

public interface IListNotificationView extends IBaseView {

    void setNotificationListView(List<NotificationTableList> notificationList);

    void emptyNotificationListView();

    void deleteAllNotificationList();
}