package com.ebizu.manis.mvp.reward;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.LoadType;
import com.ebizu.manis.mvp.filtercategories.FilterCategoriesActivity;
import com.ebizu.manis.mvp.reward.rewarddetail.rewarddetailregular.RewardRegularDetailActivity;
import com.ebizu.manis.mvp.sortreward.SortRewardFragment;
import com.ebizu.manis.view.adapter.RedeemAdapter;
import com.ebizu.manis.view.linearlayout.ItemOffsetsDecoration;
import com.ebizu.sdk.reward.models.Filter;
import com.ebizu.sdk.reward.models.Response;
import com.ebizu.sdk.reward.models.Reward;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Raden on 7/4/17.
 */

public class ReedemFragment extends Fragment implements IReedemView, RedeemAdapter.OnclickListener{

    public static final String RECEIVER_REWARD = "receiver_reward";
    public static final String FILTER_REWARD = "filter_reward";
    public static final String SORT_REWARD_TYPE = "sort_reward_type";
    public static final String ADD_REMOVE_FILTER_CATEGORY = "add_remove_filter_category";

    private Context context;
    private Boolean isLoaded = false;
    private ReedemPresenter reedemPresenter;
    private RedeemAdapter redeemAdapter;
    private List<Reward> rewards;
    private ArrayList<String> filterCategories,filterBrands;
    private int sorting, page;
    private Filter rewardFilter;

    @BindView(R.id.progress_redeem)
    ProgressBar progressRedeem;
    @BindView(R.id.recycler_redeem)
    RecyclerView recyclerRedeem;
    @BindView(R.id.sv_search)
    SearchView svSearch;
    @BindView(R.id.layout_empty_txt)
    TextView txtEmpty;
    @BindView(R.id.layout_empty)
    LinearLayout linearEmpty;
    @BindView(R.id.layout_empty_img)
    ImageView imgEmpty;
    @BindView(R.id.frf_animator)
    ViewAnimator viewAnim;
    @BindView(R.id.swipe_reward)
    SwipeRefreshLayout swipeReward;
    @BindView(R.id.rel_progress)
    RelativeLayout relProgress;

    @BindView(R.id.btn_filter)
    LinearLayout btnFilter;
    @BindView(R.id.filter_image)
    ImageView filterImage;
    @BindView(R.id.filter_txt)
    TextView filterTxt;

    private final BroadcastReceiver rewardBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(ConfigManager.Reedeem.SORT_REWARD_TYPE)) {
                String sorting = intent.getStringExtra(ConfigManager.Reedeem.SORT_REWARD_TYPE);
                setSorting(Integer.parseInt(sorting));
                page = 1;
                rewardFilter.setSorting(Integer.valueOf(sorting));
                reedemPresenter.loadRedem(rewardFilter,LoadType.SEARCH_LOAD);
            }else if(intent.hasExtra(ConfigManager.Reedeem.FILTER_REWARD) && intent.getBooleanExtra(ConfigManager.Reedeem.FILTER_REWARD, false)){
                if (intent.hasExtra(ConfigManager.Reedeem.ADD_REMOVE_FILTER_CATEGORY)) {
                    filterCategories.clear();
                    filterCategories.addAll(intent.getStringArrayListExtra(ConfigManager.Reedeem.ADD_REMOVE_FILTER_CATEGORY));
                    rewardFilter.setCategories(filterCategories);
                }
                if (intent.hasExtra(ConfigManager.Reedeem.ADD_REMOVE_FILTER_BRAND)) {
                    filterBrands.clear();
                    filterBrands.addAll(intent.getStringArrayListExtra(ConfigManager.Reedeem.ADD_REMOVE_FILTER_BRAND));
                }
                page = 1;
                reedemPresenter.loadRedem(rewardFilter,LoadType.SEARCH_LOAD);
            }else if(intent.hasExtra(ConfigManager.Reedeem.CLEAR_FILTER) && intent.getBooleanExtra(ConfigManager.Reedeem.CLEAR_FILTER, false)){
                onFilterClear();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(rewardBroadcastReceiver, new IntentFilter(ConfigManager.Reedeem.RECEIVER_REWARD));
        context = getActivity();
        rewards = new ArrayList<>();
        filterCategories = new ArrayList<>();
        filterBrands = new ArrayList<>();
        rewardFilter = new Filter();
        sorting = 0;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (!menuVisible && isLoaded) {
            svSearch.setQuery("", false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_redeem, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        reedemPresenter = new ReedemPresenter(context,getActivity());
        reedemPresenter.attachView(this);

        initialize();
        setUICallbacks();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        reedemPresenter.loadRedem(rewardFilter,LoadType.FIRST_LOAD);
    }

    private void initialize(){
        swipeReward.setColorSchemeResources(R.color.base_pink);

        redeemAdapter = new RedeemAdapter(context);
        redeemAdapter.setOnclickListener(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerRedeem.setLayoutManager(gridLayoutManager);
        recyclerRedeem.addItemDecoration(new ItemOffsetsDecoration(getActivity(), R.dimen.five_dp));
        recyclerRedeem.setAdapter(redeemAdapter);
    }

    private void setUICallbacks() {
        svSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                svSearch.clearFocus();
                reedemPresenter.loadRedem(rewardFilter,LoadType.SEARCH_LOAD);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        svSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        swipeReward.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                reedemPresenter.loadRedem(rewardFilter,LoadType.SWIPE_LOAD);
            }
        });
    }

    @Override
    public void setRedeem(Response.Data<com.ebizu.sdk.reward.models.Reward> rewardData,LoadType loadType)  {
           this.redeemAdapter.addRedeem(rewardData.getList());
           String textEmpty = null;
           if (this.redeemAdapter.isEmptyReward()) {
               linearEmpty.setVisibility(View.VISIBLE);
               txtEmpty.setVisibility(View.VISIBLE);
               textEmpty = getString(R.string.error_reward_notfound_filter);
           }else {
               linearEmpty.setVisibility(View.GONE);
               recyclerRedeem.setVisibility(View.VISIBLE);
               progressRedeem.setVisibility(View.GONE);
           }
           txtEmpty.setText(textEmpty);
    }

    @Override
    public SearchView getSearchView() {
        return svSearch;
    }

    @Override
    public int getPage() {
        return 1;
    }

    @Override
    public void failedConnected() {

    }

    @Override
    public void noData(String message) {

    }

    @Override
    public void showProgress() {
        progressRedeem.setVisibility(View.VISIBLE);
    }

    @Override
    public void dissmissProgress() {
        progressRedeem.setVisibility(View.GONE);
    }

    @Override
    public void swipeProgress() {

    }

    @Override
    public void dissmissSwipeProgress() {
        swipeReward.setRefreshing(false);
    }

    @Override
    public void paginationProgress() {

    }

    @Override
    public void dissmissPaginaionProgress() {

    }

    @Override
    public void onRewardDetailsClickListener(Reward reward) {
        Intent intent = new Intent(getActivity(), RewardRegularDetailActivity.class);
        intent.putExtra(ConfigManager.Reward.REWARD,reward);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
    }

    @OnClick(R.id.btn_filter)
    void onClickFilter(){
        Intent intent = new Intent(getActivity(), FilterCategoriesActivity.class);
        intent.putStringArrayListExtra(FilterCategoriesActivity.CATEGORIES,filterCategories);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
    }

    @OnClick(R.id.btn_sort)
    void onClickSort(){
        SortRewardFragment sortRewardFragment = new SortRewardFragment();
        sortRewardFragment.setSorting(sorting);
        sortRewardFragment.show(getFragmentManager(),null);
    }

    public void setSorting(int sorting) {
        this.sorting = sorting;
    }

    private void onFilterClear() {
        page = 1;
        onClearFilter();
        reedemPresenter.loadRedem(rewardFilter,LoadType.SEARCH_LOAD);
    }

    public void onClearFilter() {
        filterCategories.clear();
        filterBrands.clear();
    }
}