package com.ebizu.manis.mvp.mission;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperienceMissionsRB;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public interface IMissionPresenter extends IBaseViewPresenter {

    void loadShareExperienceMission(ShareExperienceMissionsRB shareExperienceMissionsRB);

    void loadShareExperienceNextPage(ShareExperienceMissionsRB shareExperienceMissionsRB);

}
