package com.ebizu.manis.mvp.snap.receipt.camera;

import android.content.Intent;

import com.ebizu.manis.helper.ConfigManager;

import static com.ebizu.manis.helper.ConfigManager.Snap.RECEIPT_RETAKE_RESULT_CODE;

/**
 * Created by ebizu on 10/9/17.
 */

public class ReceiptCameraActivity extends ReceiptActivity {

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConfigManager.Snap.RECEIPT_REQUEST_CODE)
            if (resultCode == RECEIPT_RETAKE_RESULT_CODE) {
                backToCameraActivity();
            }
    }

    protected void backToCameraActivity(){
        setResult(ConfigManager.Snap.RECEIPT_RETAKE_RESULT_CODE);
        finish();
    }

    protected void retakeLongReceipt() {
        setResult(ConfigManager.Snap.LONG_RECEIPT_RETAKE_RESULT_CODE);
        finish();
    }

    protected void nextLongReceipt() {
        setResult(ConfigManager.Snap.LONG_RECEIPT_ADD_RESULT_CODE);
        finish();
    }

}
