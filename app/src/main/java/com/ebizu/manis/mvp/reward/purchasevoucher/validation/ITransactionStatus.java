package com.ebizu.manis.mvp.reward.purchasevoucher.validation;

import android.graphics.drawable.Drawable;

/**
 * Created by ebizu on 10/4/17.
 */

public interface ITransactionStatus {

    String statusMidTrans();

    String statusMolPay();

    String statusPayment();

    String statusTransaction();

    Drawable iconStatus();
}
