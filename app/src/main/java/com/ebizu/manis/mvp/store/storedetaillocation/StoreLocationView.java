package com.ebizu.manis.mvp.store.storedetaillocation;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.GPSTracker;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Coordinate;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by ebizu on 9/11/17.
 */

public class StoreLocationView extends BaseView
        implements IStoreLocationView, OnMapReadyCallback {

    public SupportMapFragment mapFragment;


    private StoreLocationPresenter storeLocationPresenter;
    private Store store;
    private URL urlMap;
    private double gpsTrackerLatitude, gpsTrackerLongitude, storeLatitude, storeLongitude;

    public StoreLocationView(Context context) {
        super(context);
        createView(context);
    }

    public StoreLocationView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public StoreLocationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public StoreLocationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_store_location, null, false);
        addView(view, new RecyclerView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(view);
    }

    @Override
    public void setStore(Store store) {
        this.store = store;
    }

    @Override
    public void setCoordinateLocation(GPSTracker gpsTracker, Coordinate coordinate) {
        this.gpsTrackerLatitude = gpsTracker.getLatitude();
        this.gpsTrackerLongitude = gpsTracker.getLongitude();
        this.storeLatitude = coordinate.getLatitude();
        this.storeLongitude = coordinate.getLongitude();
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        initializeMap();
    }

    private void initializeMap() {
        try {
            mapFragment = (SupportMapFragment) getBaseActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_store_map);
            urlMap = new URL("http://maps.googleapis.com/maps/api/directions/json?origin="
                    + gpsTrackerLatitude + ","
                    + gpsTrackerLongitude + "&destination="
                    + storeLatitude + ","
                    + storeLongitude + "&sensor=true&mode=drive");
            storeLocationPresenter = new StoreLocationPresenter();
            storeLocationPresenter.attachView(this);
            storeLocationPresenter.loadMap();
            showProgressBar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        loadMap(googleMap);
    }

    private void loadMap(GoogleMap googleMap) {
        new Thread() {

            @Override
            public void run() {
                try {
                    HttpURLConnection conn = (HttpURLConnection) urlMap.openConnection();
                    StringBuilder builder = new StringBuilder();
                    InputStream is = conn.getInputStream();
                    byte[] b = new byte[256];
                    int length;
                    while ((length = is.read(b)) > 0) {
                        builder.append(new String(b, 0, length));
                    }
                    is.close();
                    conn.disconnect();
                    JSONObject routes = new JSONObject(builder.toString());
                    JSONObject route = routes.getJSONArray("routes").getJSONObject(0);
                    JSONObject leg = route.getJSONArray("legs").getJSONObject(0);
                    final String startAddress = leg.getString("start_address");
                    final String endAddress = leg.getString("end_address");
                    final List<LatLng> line = UtilManis.decodePoly(route.getJSONObject("overview_polyline").getString("points"));
                    getBaseActivity().runOnUiThread(() -> {
                        try {
                            LatLng userLoc = new LatLng(gpsTrackerLatitude, gpsTrackerLongitude);
                            LatLng storeLoc = new LatLng(storeLatitude, storeLongitude);
                            LatLngBounds.Builder builder1 = new LatLngBounds.Builder().include(userLoc).include(storeLoc);
                            MarkerOptions sourceMarker =
                                    new MarkerOptions().position(userLoc).title(getBaseActivity().getString(R.string.route_your_location))
                                            .snippet(startAddress)
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                            MarkerOptions destinationMarker =
                                    new MarkerOptions().position(storeLoc).title(store.getName())
                                            .snippet(store.getAddress())
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                            googleMap.addMarker(sourceMarker);
                            googleMap.addMarker(destinationMarker).showInfoWindow();
                            PolylineOptions polyline = new PolylineOptions();
                            for (LatLng latLng : line) {
                                polyline.add(latLng);
                                builder1.include(latLng);
                            }
                            polyline.geodesic(true);
                            polyline.color(Color.CYAN);
                            googleMap.addPolyline(polyline);
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder1.build(), 50));
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(storeLoc, 12.0f));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    getBaseActivity().runOnUiThread(() -> {
                        LatLng source = new LatLng(gpsTrackerLatitude, gpsTrackerLongitude);
                        LatLng destination = new LatLng(storeLatitude, storeLongitude);
                        LatLngBounds bounds = new LatLngBounds.Builder().include(source).include(destination).build();
                        MarkerOptions sourceMarker =
                                new MarkerOptions().position(source).title(getBaseActivity().getString(R.string.route_your_location));
                        MarkerOptions destinationMarker =
                                new MarkerOptions().position(destination).title(store.getName()).snippet(store.getAddress());
                        googleMap.addMarker(sourceMarker);
                        googleMap.addMarker(destinationMarker).showInfoWindow();
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(destination, 12.0f));
                    });
                }
            }

        }.start();
        dismissProgressBar();
    }
}