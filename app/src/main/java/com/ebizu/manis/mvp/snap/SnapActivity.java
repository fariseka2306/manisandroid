package com.ebizu.manis.mvp.snap;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptDataBody;

import static com.ebizu.manis.helper.ConfigManager.Snap.LUCKY_DRAW_INTENT_DATA;
import static com.ebizu.manis.helper.ConfigManager.Snap.RECEIPT_DATA_PARCEL;

/**
 * Created by ebizu on 8/25/17.
 */

public class SnapActivity extends BaseActivity {

    protected boolean snapLuckyDraw;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getIntentData();
    }

    private void getIntentData() {
        try {
            snapLuckyDraw = getIntent().getBooleanExtra(LUCKY_DRAW_INTENT_DATA, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConfigManager.Snap.RECEIPT_REQUEST_CODE)
            if (resultCode == ConfigManager.Snap.EARN_POINT_RECEIPT_SUCCESS_UPLOAD_RS) {
                setResult(ConfigManager.Snap.EARN_POINT_RECEIPT_SUCCESS_UPLOAD_RS);
                finish();
            } else if (resultCode == ConfigManager.Snap.EARN_TICKET_RECEIPT_SUCCESS_UPLOAD_RS) {
                boolean openLuckyDraw = data.getBooleanExtra(ConfigManager.Snap.OPEN_LUCKY_DRAW_FRAGMENT, false);
                getIntent().putExtra(ConfigManager.Snap.OPEN_LUCKY_DRAW_FRAGMENT, openLuckyDraw);
                setResult(ConfigManager.Snap.EARN_TICKET_RECEIPT_SUCCESS_UPLOAD_RS, getIntent());
                finish();
            } else if (resultCode == ConfigManager.Snap.GET_MORE_POINTS_RS) {
                setResult(ConfigManager.Snap.GET_MORE_POINTS_RS);
                finish();
            } else if (resultCode == ConfigManager.Snap.CANT_ACCESS_CAMERA) {
                showManisAlertDialog(getResources().getString(R.string.permission_must_photo));
            }
    }

    protected void nextActivity(Class nextClassAct, ReceiptDataBody receiptDataBody) {
        Intent intent = new Intent(this, nextClassAct);
        intent.putExtra(RECEIPT_DATA_PARCEL, receiptDataBody);
        intent.putExtra(LUCKY_DRAW_INTENT_DATA, snapLuckyDraw);
        startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
    }

    protected boolean isSnapLuckyDraw() {
        return snapLuckyDraw;
    }
}