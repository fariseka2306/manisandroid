package com.ebizu.manis.mvp.reward.rewardlistcategory.rewardregular;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.reward.rewardlistcategory.RewardListCategoryAbstractView;
import com.ebizu.manis.service.manis.requestbody.RewardCategoryBody;
import com.ebizu.sdk.reward.models.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public class RewardListCategoryView extends RewardListCategoryAbstractView {

    public RewardListCategoryView(Context context) {
        super(context);
    }

    public RewardListCategoryView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RewardListCategoryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RewardListCategoryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void loadRewardPresenter() {
        Bundle bundle = getBaseActivity().getIntent().getExtras();
        String rewardCategoryId = bundle.getString(ConfigManager.Reward.REWARD_CATEGORY_ID);
        Filter filter = new Filter();
        List<String> filterCategories = new ArrayList<>();
        filterCategories.add(rewardCategoryId);
        filter.setCategories(filterCategories);
        filter.setSorting(0);
        getRewardCategoryPresenter().loadRewardVoucherListNext(
                new RewardCategoryBody(page,
                        ConfigManager.Reward.REWARD_CATEGORY_MAX_PAGE), rewardCategoryId == null ? new Filter() : filter, rewardSearchKeyword);
    }

    @Override
    public void reloadRewardPresenter() {
        Bundle bundle = getBaseActivity().getIntent().getExtras();
        String rewardCategoryId = bundle.getString(ConfigManager.Reward.REWARD_CATEGORY_ID);
        Filter filter = new Filter();
        List<String> filterCategories = new ArrayList<>();
        filterCategories.add(rewardCategoryId);
        filter.setCategories(filterCategories);
        filter.setSorting(0);
        getRewardCategoryPresenter().loadRewardVoucherList(
                new RewardCategoryBody(ConfigManager.Reward.REWARD_CATEGORY_PAGE,
                        ConfigManager.Reward.REWARD_CATEGORY_MAX_PAGE), rewardCategoryId == null ? new Filter() : filter, rewardSearchKeyword);
    }

}
