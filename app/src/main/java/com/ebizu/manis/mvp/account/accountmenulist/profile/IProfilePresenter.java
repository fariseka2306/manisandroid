package com.ebizu.manis.mvp.account.accountmenulist.profile;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbody.AccountBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;

/**
 * Created by abizu-alvio on 7/26/2017.
 */

public interface IProfilePresenter extends IBaseViewPresenter {

    void saveProfile(AccountBody accountBody);

    void saveProfileData(RequestBody requestBody);

}
