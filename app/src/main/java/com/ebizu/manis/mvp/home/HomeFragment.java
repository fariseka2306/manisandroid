package com.ebizu.manis.mvp.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilsStatic;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.Brand;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.model.snap.SnapData;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.SnapGuideActivity;
import com.ebizu.manis.mvp.branddetail.BrandDetailActivity;
import com.ebizu.manis.mvp.home.brands.HomePresenter;
import com.ebizu.manis.mvp.home.brands.IHomeView;
import com.ebizu.manis.mvp.home.reward.IRewardView;
import com.ebizu.manis.mvp.home.reward.RewardPresenter;
import com.ebizu.manis.mvp.home.reward.RewardVoucherHomeHomePresenter;
import com.ebizu.manis.mvp.home.snaphistory.ISnapView;
import com.ebizu.manis.mvp.home.snaphistory.SnapPresenter;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.mvp.snap.store.SnapStoreActivity;
import com.ebizu.manis.mvp.snapdetail.SnapDetailsActivity;
import com.ebizu.manis.mvp.viewhistory.SnapViewHistoryActivity;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.service.reward.requestbody.RewardGeneralListBody;
import com.ebizu.manis.view.adapter.BrandAdapter;
import com.ebizu.manis.view.adapter.RewardAdapter;
import com.ebizu.manis.view.adapter.RewardVoucherHomeAdapter;
import com.ebizu.manis.view.adapter.SnapAdapter;
import com.ebizu.manis.view.linearlayout.FixedLinearLayoutManager;
import com.ebizu.sdk.reward.models.Response;
import com.ebizu.sdk.reward.models.Reward;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerOriginPageHome;

/**
 * Created by Raden on 7/4/17.
 */

public class HomeFragment extends BaseFragment implements IHomeView, IRewardView, ISnapView, UtilsStatic, SnapAdapter.onClickListener, RewardAdapter.onclickListener,
        RewardVoucherHomeAdapter.RewardVoucherListener {

    private static final String TAG = HomeFragment.class.getSimpleName();

    @BindView(R.id.recycler_get_point)
    RecyclerView recyclerGetPoint;
    @BindView(R.id.recycler_rewrad_view)
    RecyclerView recyclerRewardView;
    @BindView(R.id.recycler_last_snap)
    RecyclerView recyclerLastSnap;

    @BindView(R.id.txt_view_history)
    TextView txtViewHistory;
    @BindView(R.id.txt_negative_snap)
    TextView txtNegativeSnap;
    @BindView(R.id.txt_detail_redeem)
    TextView txtDetailRedeem;
    @BindView(R.id.txt_get_brand)
    TextView txtGetBrand;

    @BindView(R.id.get_point_ProgressBar)
    ProgressBar progressGetPoint;
    @BindView(R.id.redeem_ProgressBar_redeem)
    ProgressBar progressRedeem;
    @BindView(R.id.last_snap_ProgressBar_redeem)
    ProgressBar progessLastSnap;

    @BindView(R.id.no_Connection_Brand)
    LinearLayout noConnectionBrand;
    @BindView(R.id.no_Connection_reward)
    LinearLayout noConnectionReward;
    @BindView(R.id.no_Connection_redeem)
    LinearLayout noConnectionRedeem;
    @BindView(R.id.lin_empty_rewards)
    LinearLayout linEmptyRewards;

    @BindView(R.id.layout_empty_img_brand)
    ImageView imgNoConnectBrand;
    @BindView(R.id.img_no_connection_snap)
    ImageView imgNoConnectSnap;
    @BindView(R.id.img_no_connection_reward)
    ImageView imgNoConnectReward;
    @BindView(R.id.swipe_Refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private DeviceSession deviceSession;
    private Context mContext;
    private HomePresenter homePresenter;
    private SnapPresenter snapPresenter;
    private RewardPresenter rewardPresenter;
    private BrandAdapter brandAdapter;
    private SnapAdapter snapAdapter;
    private RewardAdapter rewardAdapter;
    private RewardVoucherHomeAdapter rewardVoucherHomeAdapter;
    private MainActivity mainActivity;
    private RewardVoucherHomeHomePresenter rewardVoucherHomePresenter;
    private ArrayList<Brand> brand;
    private OnRewardClickListener onRewardClickListener;
    private RewardVoucherHomeListener rewardVoucherHomeListener;

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            homePresenter.loadBrandData(brand);
            rewardPresenter.loadReedeemData();
            snapPresenter.loadSnapData();
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }
            if (onRefreshHomeListener != null)
                onRefreshHomeListener.onRefresh();
        }
    };
    private OnRefreshHomeListener onRefreshHomeListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mainActivity = (MainActivity) getActivity();
        brand = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        swipeRefreshLayout.setColorSchemeResources(R.color.base_pink);
        swipeRefreshLayout.setOnRefreshListener(refreshListener);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        homePresenter = new HomePresenter(mContext, getActivity());
        homePresenter.attachView(this);

        rewardPresenter = new RewardPresenter(mContext, getActivity());
        rewardPresenter.attachView(this);

        rewardVoucherHomePresenter = new RewardVoucherHomeHomePresenter(mContext);

        snapPresenter = new SnapPresenter(mContext);
        snapPresenter.attachView(this);

        initialize();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        homePresenter.loadBrandData(brand);
        rewardPresenter.loadReedeemData();
//        loadRewardHome();
        snapPresenter.loadSnapData();
        showBluetoothRequest();
    }

    private void loadRewardHome() {
        RewardGeneralListBody.Data data = new RewardGeneralListBody.Data();
        data.setNoCache("N");
        data.setShowEmptyStock("N");
        data.setOrder(1);
        data.setKeyword("");
        data.setPage(1);
        data.setSize(999);
        RewardGeneralListBody requestBody = new RewardGeneralListBody(getContext(), data);
        rewardVoucherHomePresenter.loadRewardVoucher(requestBody, this);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getContext() != null) {
            getAnalyticManager().trackScreenView(ConfigManager.Analytic.ScreenView.FRAGMENT_HOME);
            TrackerManager.getInstance().setTrackerData(
                    new TrackerStandartRequest(kTrackerOriginPageHome, kTrackerOriginPageHome, "", "", "", "")
            );
        }
    }

    private void initialize() {
        Log.d(TAG, "initialize: ");
//        get point view
        brandAdapter = new BrandAdapter(mContext);
        FixedLinearLayoutManager fixedLinearLayoutManager = new FixedLinearLayoutManager(getContext());
        fixedLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerGetPoint.setLayoutManager(fixedLinearLayoutManager);
        recyclerGetPoint.setHasFixedSize(true);
        recyclerGetPoint.setAdapter(brandAdapter);

//        last snap  view
        snapAdapter = new SnapAdapter(mContext);
        snapAdapter.setSnapOnClick(this);
        FixedLinearLayoutManager layoutManagerVertical = new FixedLinearLayoutManager(getContext());
        layoutManagerVertical.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerLastSnap.setLayoutManager(layoutManagerVertical);
        recyclerLastSnap.setHasFixedSize(true);
        recyclerLastSnap.setAdapter(snapAdapter);

//        reward view
        rewardAdapter = new RewardAdapter(getContext(), new ArrayList<>());
        FixedLinearLayoutManager fixedLinearLayoutManagerRedeem = new FixedLinearLayoutManager(getContext());
        fixedLinearLayoutManagerRedeem.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerRewardView.setLayoutManager(fixedLinearLayoutManagerRedeem);
        recyclerRewardView.setHasFixedSize(true);
        recyclerRewardView.setAdapter(rewardAdapter);

        setOnListener();
        rewardAdapter.setRewardOnClick(this);
//        rewardVoucherHomeAdapter.setRewardOnClick(this);
        deviceSession = new DeviceSession(getContext());
    }

    @Override
    public void loadViewBrand(ArrayList<Brand> brands) {
        this.brandAdapter.addBrands(brands);

        recyclerGetPoint.setVisibility(View.VISIBLE);
        progressGetPoint.setVisibility(View.GONE);
        noConnectionBrand.setVisibility(View.GONE);
    }

    @Override
    public void setBrandData(String message) {
        txtGetBrand.setText(message);
    }

    @Override
    public void noBrandData() {
        imgNoConnectBrand.setImageResource(R.drawable.home_cashback_no_connection);
        noConnectionBrand.setVisibility(View.VISIBLE);
        txtGetBrand.setText(R.string.hm_cashback);
    }

    @Override
    public void failedConnected() {
        imgNoConnectBrand.setImageResource(R.drawable.home_cashback_no_connection);
        noConnectionBrand.setVisibility(View.VISIBLE);
        txtGetBrand.setText(R.string.hm_cashback);
    }

    @Override
    public void serverBusy(String message) {
        recyclerGetPoint.setVisibility(View.VISIBLE);
        progressGetPoint.setVisibility(View.GONE);
        noConnectionBrand.setVisibility(View.GONE);
    }

    public void loadViewSnap(ArrayList<SnapData> snapDatas) {
        this.snapAdapter.addSnaps(snapDatas);
        Log.d(TAG, "loadViewSnap: " + snapDatas);
        recyclerLastSnap.setVisibility(View.VISIBLE);
        progessLastSnap.setVisibility(View.GONE);
        noConnectionReward.setVisibility(View.GONE);
        if (snapDatas == null) {
            noSnap("mesage");
        } else {
            txtNegativeSnap.setVisibility(View.GONE);
        }

        if (snapDatas == null) {
            txtViewHistory.setVisibility(View.GONE);
        } else {
            txtViewHistory.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void connectionErrorSnap() {
        noConnectionReward.setVisibility(View.VISIBLE);
        recyclerLastSnap.setVisibility(View.GONE);
    }

    @Override
    public void serverBusySnap(String message) {
        recyclerLastSnap.setVisibility(View.VISIBLE);
        progessLastSnap.setVisibility(View.GONE);
        noConnectionReward.setVisibility(View.GONE);
    }

    @Override
    public void snapDone() {
        recyclerLastSnap.setVisibility(View.VISIBLE);
        txtViewHistory.setVisibility(View.VISIBLE);
    }

    @Override
    public void noSnap(String message) {
        progessLastSnap.setVisibility(View.GONE);
        txtViewHistory.setVisibility(View.GONE);
        txtNegativeSnap.setVisibility(View.VISIBLE);
        progessLastSnap.setVisibility(View.GONE);
        message = getString(R.string.error_no_lastsnap);
        txtNegativeSnap.setText(message);
    }

    @OnClick(R.id.layout_empty_img_brand)
    void clickConnect() {
        homePresenter.loadBrandData(brand);
        rewardPresenter.loadReedeemData();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_HOME,
                ConfigManager.Analytic.Action.CLICK,
                "Image View Refresh No Brand"
        );
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_HOME,
                ConfigManager.Analytic.Action.CLICK,
                "Image View Refresh No Redeem"
        );
    }

    @Override
    public void OnSnapClickListener(SnapData snapData) {
        Log.d(TAG, "OnSnapClickListenerSnap: " + snapData);
        Intent intent = new Intent(getActivity(), SnapDetailsActivity.class);
        intent.putExtra(ConfigManager.Snap.DATA, snapData);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
    }

    private void setOnListener() {
        brandAdapter.setOnClickListener(brand -> {
            if (!deviceSession.hasShowSnapEarnGuide()) {
                Intent intent = new Intent(getContext(), SnapGuideActivity.class);
                intent.putExtra(ConfigManager.Snap.SNAP_EARN_GUIDE, ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_BRAND);
                intent.putExtra(ConfigManager.BrandDetails.BRAND_DETAILS, brand);
                getActivity().startActivityForResult(intent, ConfigManager.Snap.SNAP_EARN_GUIDE_REQUEST_CODE);
            } else {
                Intent intent = new Intent(getActivity(), BrandDetailActivity.class);
                intent.putExtra(ConfigManager.BrandDetails.BRAND_DETAILS, brand);
                getActivity().startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });
    }

    @OnClick(R.id.txt_view_history)
    void clickViewHistory() {
        Intent intent = new Intent(getActivity(), SnapViewHistoryActivity.class);
        startActivity(intent);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_HOME,
                ConfigManager.Analytic.Action.CLICK,
                "Text View History"
        );
    }

    @OnClick(R.id.txt_detail_redeem)
    void clickReedem() {
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        mainActivity.switchToRedeem();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_HOME,
                ConfigManager.Analytic.Action.CLICK,
                "Text View All Redeem"
        );
    }

    @Override
    public void onRewardClickListener(Reward reward) {
        onRewardClickListener.onRewardClickListener(reward);
    }

    @Override
    public void setRedeemHome(Response.Data<Reward> redeemHomeData) {
        this.rewardAdapter.addReward((ArrayList<Reward>) redeemHomeData.getList());
        if (redeemHomeData.getList().size() >= 1) {
            recyclerRewardView.setVisibility(View.VISIBLE);
            progressRedeem.setVisibility(View.GONE);
            noConnectionRedeem.setVisibility(View.GONE);
        } else {
            progressRedeem.setVisibility(View.GONE);
            linEmptyRewards.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setRewardHome(List<RewardVoucher> rewardVoucher) {
        rewardVoucherHomeAdapter.addRewardVoucher(rewardVoucher);
        if (rewardVoucher.size() >= 1) {
            recyclerRewardView.setVisibility(View.VISIBLE);
            progressRedeem.setVisibility(View.GONE);
            noConnectionRedeem.setVisibility(View.GONE);
        } else {
            progressRedeem.setVisibility(View.GONE);
            linEmptyRewards.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void noConnectionReward() {
        imgNoConnectReward.setImageResource(R.drawable.home_cashback_no_connection);
        noConnectionRedeem.setVisibility(View.VISIBLE);
        recyclerRewardView.setVisibility(View.GONE);
        linEmptyRewards.setVisibility(View.GONE);
    }

    @Override
    public void noServer(String message) {
        progressRedeem.setVisibility(View.VISIBLE);
        recyclerRewardView.setVisibility(View.VISIBLE);
        noConnectionRedeem.setVisibility(View.GONE);
    }

    @Override
    public void noReward(String message) {
        progressRedeem.setVisibility(View.GONE);
        linEmptyRewards.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.floatbar_snap)
    void onClickSnap() {
        if (!deviceSession.hasShowSnapEarnGuide()) {
            Intent intent = new Intent(getContext(), SnapGuideActivity.class);
            intent.putExtra(ConfigManager.Snap.SNAP_EARN_GUIDE, ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_SNAP);
            getActivity().startActivityForResult(intent, ConfigManager.Snap.SNAP_EARN_GUIDE_REQUEST_CODE);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        } else {
            Intent intent = new Intent(getContext(), SnapStoreActivity.class);
            getActivity().startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_HOME,
                ConfigManager.Analytic.Action.CLICK,
                "Button Snap"
        );
    }

    public void setOnRefreshHomeListener(OnRefreshHomeListener onRefreshHomeListener) {
        this.onRefreshHomeListener = onRefreshHomeListener;
    }

    public interface OnRefreshHomeListener {

        void onRefresh();

    }

    public void setOnRewardClickListener(OnRewardClickListener onRewardClickListener) {
        this.onRewardClickListener = onRewardClickListener;
    }

    public void setOnRewardVoucherHomeListener(RewardVoucherHomeListener onRewardVoucherHomeListener) {
        rewardVoucherHomeListener = onRewardVoucherHomeListener;
    }

    @Override
    public void onRewardClickListener(RewardVoucher rewardVoucher) {
        rewardVoucherHomeListener.onRewardVoucherHomeListener(rewardVoucher);
    }

    public interface OnRewardClickListener {
        void onRewardClickListener(Reward reward);
    }

    public interface RewardVoucherHomeListener {
        void onRewardVoucherHomeListener(RewardVoucher rewardVoucher);
    }

    private void showBluetoothRequest() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
//                    int notifBTCount =

                } catch (IllegalStateException ie) {
                    Log.e("Bluetooth", ie.getMessage());
                }
            }
        }, 3000);
    }

    public SnapPresenter getSnapPresenter() {
        if (null == snapPresenter)
            snapPresenter = new SnapPresenter(getContext());
        return snapPresenter;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != homePresenter && null != snapPresenter) {
            homePresenter.releaseAllSubscribe();
            snapPresenter.releaseAllSubscribes();
        }
    }
}
