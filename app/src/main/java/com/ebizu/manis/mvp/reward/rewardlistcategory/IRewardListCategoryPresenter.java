package com.ebizu.manis.mvp.reward.rewardlistcategory;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbody.RewardCategoryBody;
import com.ebizu.manis.service.reward.requestbody.RewardBulkListBody;
import com.ebizu.manis.service.reward.requestbody.RewardGeneralListBody;
import com.ebizu.sdk.reward.models.Filter;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public interface IRewardListCategoryPresenter extends IBaseViewPresenter {

    void loadRewardVoucherList(RewardCategoryBody rewardCategoryBody, Filter filter, String keyword);

    void loadRewardVoucherListNext(RewardCategoryBody rewardCategoryBody, Filter filter, String keyword);

    void loadRewardsBulkList(RewardBulkListBody rewardBulkListBody);

    void loadRewardBulkListNext(RewardBulkListBody rewardBulkListBody);

}
