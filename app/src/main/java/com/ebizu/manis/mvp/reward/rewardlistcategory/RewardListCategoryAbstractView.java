package com.ebizu.manis.mvp.reward.rewardlistcategory;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.EndlessRecyclerOnScrollListener;
import com.ebizu.manis.helper.ItemOffsetDecoration;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.rewarddetail.rewardbulkdetail.RewardBulkDetailActivity;
import com.ebizu.manis.mvp.reward.rewarddetail.rewarddetailregular.RewardRegularDetailActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.adapter.rewardvoucher.BulkRewardViewHolder;
import com.ebizu.manis.view.adapter.rewardvoucher.GenericRewardAdapter;
import com.ebizu.manis.view.adapter.rewardvoucher.SingleRewardViewHolder;
import com.ebizu.manis.view.linearlayout.FixedGridLayoutManager;
import com.ebizu.sdk.reward.models.Filter;
import com.ebizu.sdk.reward.models.Reward;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FARIS_mac on 16/11/17.
 */

public abstract class RewardListCategoryAbstractView extends BaseView implements IRewardListCategoryView, SwipeRefreshLayout.OnRefreshListener,
        BulkRewardViewHolder.BulkRewardListener, SingleRewardViewHolder.RewardListener {

    protected IRewardListCategoryPresenter iRewardListCategoryPresenter;
    protected GenericRewardAdapter genericRewardAdapter;
    protected Filter filter;
    protected int sorting;
    protected boolean isLoading = false;
    protected boolean isLastPage = false;
    protected int page = 1;
    protected String rewardSearchKeyword;

    @BindView(R.id.swipe_refresh)
    protected SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.recyclerview_reward_category)
    protected RecyclerView rvReward;
    @BindView(R.id.layout_empty)
    public LinearLayout layoutEmpty;
    @BindView(R.id.layout_empty_txt)
    public TextView textViewEmpty;

    public RewardListCategoryAbstractView(Context context) {
        super(context);
        createView(context);
    }

    public RewardListCategoryAbstractView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public RewardListCategoryAbstractView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RewardListCategoryAbstractView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.reward_category_view, null, false);
        addView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        attachPresenter(new RewardListCategoryPresenter(context));
        initView();
        initListener();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iRewardListCategoryPresenter = (IRewardListCategoryPresenter) iBaseViewPresenter;
        iRewardListCategoryPresenter.attachView(this);
    }

    protected void initView() {
        FixedGridLayoutManager gridLayoutManager = new FixedGridLayoutManager(getContext(), 2);
        genericRewardAdapter = new GenericRewardAdapter(getContext(), new ArrayList<>(), this, this);
        rvReward.setLayoutManager(gridLayoutManager);
        rvReward.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.five_dp));
        rvReward.setAdapter(genericRewardAdapter);
        rvReward.addOnScrollListener(new EndlessRecyclerOnScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                page++;
                loadRewardPresenter();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (genericRewardAdapter.getItemViewType(position)) {
                    case 0:
                        return 1;
                    case 1:
                        return 2;
                    default:
                        return -1;
                }
            }
        });
    }

    protected void initListener() {
        swipeRefresh.setOnRefreshListener(this);
    }

    @Override
    public void loadRewarVoucherList(List<Reward> rewardVoucherList, String keyword) {
        dismissProgressBar();
        rewardSearchKeyword = keyword;
        genericRewardAdapter.replaceReward(rewardVoucherList);
        rvReward.setVisibility(VISIBLE);
        if (!isLastPage)
            genericRewardAdapter.addLoadingFooter();
    }

    @Override
    public void loadRewardVoucherListNext(List<Reward> rewardVoucherList) {
        genericRewardAdapter.removeLoadingFooter();
        isLoading = false;
        genericRewardAdapter.addAll(rewardVoucherList);
        if (!isLastPage)
            genericRewardAdapter.addLoadingFooter();
    }

    @Override
    public void loadRewardBulkList(List<RewardVoucher> rewardVouchers) {
        dismissProgressBar();
        genericRewardAdapter.replaceReward(rewardVouchers);
        rvReward.setVisibility(VISIBLE);
        if (!isLastPage)
            genericRewardAdapter.addLoadingFooter();
    }

    @Override
    public void loadRewardBulkListNext(List<RewardVoucher> rewardVouchers) {
        genericRewardAdapter.removeLoadingFooter();
        isLoading = false;
        genericRewardAdapter.addAll(rewardVouchers);
        if (!isLastPage)
            genericRewardAdapter.addLoadingFooter();
    }

    @Override
    public IRewardListCategoryPresenter getRewardCategoryPresenter() {
        return iRewardListCategoryPresenter;
    }

    @Override
    public void handleConnectionFailure() {
        if (genericRewardAdapter.isEmpty()) {
            showNoInternetConnection();
        } else {
            genericRewardAdapter.removeLoadingFooter();
            getBaseActivity().showAlertDialog(
                    getResources().getString(R.string.error),
                    getResources().getString(R.string.error_no_connection),
                    false, getResources().getString(R.string.ok), (dialogInterface, i) -> dialogInterface.dismiss());
        }
    }

    @Override
    public void onRetry() {
        super.onRetry();
        reloadRewardPresenter();
    }

    public void loadRewardPresenter() {
    }

    public void reloadRewardPresenter() {
    }

    public void showEmptyState(String rewardSearchKeyword) {
        if (rewardSearchKeyword.equals("")) {
            layoutEmpty.setVisibility(View.VISIBLE);
            textViewEmpty.setText(R.string.error_no_redeem);
        } else {
            layoutEmpty.setVisibility(View.VISIBLE);
            textViewEmpty.setText(R.string.error_reward_notfound);
        }
    }

    @Override
    public void onRefresh() {
        reloadRewardPresenter();
        page = 1;
        if (swipeRefresh.isRefreshing()) {
            swipeRefresh.setRefreshing(false);
        }
    }

    @Override
    public void onBulkRewardListener(RewardVoucher reward) {
        Intent intent = new Intent(getContext(), RewardBulkDetailActivity.class);
        intent.putExtra(ConfigManager.Reward.REWARD, reward);
        getContext().startActivity(intent);
    }

    @Override
    public void onRewardListener(Reward reward) {
        RewardVoucher rewardVoucher = RewardConverterAdapter.convertToRewardVoucer(reward);
        Intent intent = new Intent(getContext(), RewardRegularDetailActivity.class);
        intent.putExtra(ConfigManager.Reward.REWARD, rewardVoucher);
        getContext().startActivity(intent);
    }
}
