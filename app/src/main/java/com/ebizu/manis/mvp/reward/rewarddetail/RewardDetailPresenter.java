package com.ebizu.manis.mvp.reward.rewarddetail;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.RedeemCloseBody;
import com.ebizu.manis.service.manis.requestbody.RedeemCloseFailedBody;
import com.ebizu.manis.service.manis.requestbody.RewardRedeemBody;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.service.reward.requestbody.RewardShoppingCartBody;
import com.ebizu.manis.service.reward.response.ResponseRewardApi;
import com.ebizu.manis.service.reward.response.WrapperShoppingCart;
import com.ebizu.sdk.reward.EbizuReward;
import com.ebizu.sdk.reward.models.Redeem;
import com.ebizu.sdk.reward.models.Reward;
import com.ebizu.sdk.reward.models.SessionData;
import com.ebizu.sdk.reward.models.VoucherInput;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raden on 8/1/17.
 */

public class RewardDetailPresenter extends BaseViewPresenter implements IRewardDetailPresenter {

    private final String TAG = getClass().getSimpleName();
    public List<VoucherInput> voucherInputs = new ArrayList<>();
    private Context context;
    private RewardDetailView rewardDetailView;
    private ManisApi manisApi;
    private RewardShoppingCartBody.Data rewardShopData;
    private SparseArray<Subscription> subsRewardDetail;

    public RewardDetailPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        rewardDetailView = (RewardDetailView) baseView;
        subsRewardDetail = new SparseArray<>();
    }

    @Override
    public void releaseSubscribe(int key) {
        Subscription subscription = subsRewardDetail.get(key);
        if (subscription != null) {
            subscription.unsubscribe();
            subsRewardDetail.remove(key);
        }
    }

    @Override
    public void releaseAllSubscribes() {
        rewardDetailView.unSubscribeAll(subsRewardDetail);
    }

    @Override
    public void loadReward(RewardRedeemBody rewardRedeemBody) {
        releaseSubscribe(0);
        rewardDetailView.getBaseActivity().showProgressBarDialog(context.getString(R.string.please_wait));
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceWithToken(context);
        Subscription subsReward = manisApi.getRewardRedeem(new Gson().toJson(rewardRedeemBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(wrapperRewardRedeem -> {
                            if (wrapperRewardRedeem.getRewardRedeem() == null ||
                                    wrapperRewardRedeem.getRewardRedeem().getRefcode() == null) {
                                UtilManis.info(context, context.getString(R.string.text_error_occured),
                                        wrapperRewardRedeem.getMessage().toString());
                                rewardDetailView.getBaseActivity().dismissProgressBarDialog();
                                return;
                            } else {
                                rewardDetailView.setViewRedeem(wrapperRewardRedeem.getRewardRedeem().getRefcode());
                            }
                        },
                        throwable -> {
                            rewardDetailView.getBaseActivity().dismissProgressBarDialog();
                            if (!ConnectionDetector.isNetworkConnected(context)) {
                                UtilManis.info(context, context.getString(R.string.error),
                                        rewardDetailView.getResources().getString(R.string.error_no_connection));
                            } else {
                                UtilManis.info(context, context.getString(R.string.text_error_occured),
                                        rewardDetailView.getResources().getString(R.string.text_error_occured));
                            }
                        });
        subsRewardDetail.put(0, subsReward);

    }

    @Override
    public void setRewardUser(final String refCode, RewardVoucher reward) {
        ManisSession manisSession = new ManisSession(context);
        EbizuReward.getSession(context).login(manisSession.getAccountSession().getAccId(), manisSession.getAccountSession().getAccScreenName(),
                manisSession.getAccountSession().getAccFacebookEmail(), manisSession.getAccountSession().getAccCountry(), manisSession.getAccountSession().getAccCreatedDateTime(),
                new com.ebizu.sdk.reward.core.interfaces.Callback<SessionData>() {

                    @Override
                    public void onSuccess(SessionData sessionData) {
                        Log.i("SetRewardUser", "Reward set up user success");
                        callRewardRedeem(refCode, reward);
                    }

                    @Override
                    public void onFailure(String s) {
                        rewardDetailView.getBaseActivity().dismissProgressBarDialog();
                        if (!ConnectionDetector.isNetworkConnected(context)) {
                            UtilManis.info(context, context.getString(R.string.error),
                                    rewardDetailView.getResources().getString(R.string.error_no_connection));
                        } else {
                            UtilManis.info(context, context.getString(R.string.text_error_occured),
                                    rewardDetailView.getResources().getString(R.string.text_error_occured));
                        }
                        Log.e("SetRewardUser", "Reward set up user failed because " + s);
                    }
                });
    }

    private void callRewardRedeem(String refCode, RewardVoucher reward) {
        EbizuReward.getRedemption(context).redeem(reward.getId(), refCode, voucherInputs, new com.ebizu.sdk.reward.core.interfaces.Callback<Redeem>() {
            @Override
            public void onSuccess(Redeem response) {
                if (response != null) {
                    ManisSession manisSession = new ManisSession(context);
                    int point = manisSession.getPointSession().getPoint() - reward.getPoint();
                    manisSession.setPoint(point);
                    if (!reward.getVoucherRedeemType().equals(Reward.VOUCHER_REDEEM_PIN)) {
                        rewardDetailView.showDialogSuccess(response);
                    }
                    redeemClose(refCode, response, reward);

                    HashMap<String, String> eventData = new HashMap<>();
                    eventData.put(UtilStatic.MANIS_KEY_TYPE, UtilStatic.MANIS_TYPE_REWARD);
                    eventData.put(UtilStatic.MANIS_KEY_ITEM_ID, reward.getId());
                    eventData.put(UtilStatic.MANIS_KEY_POINT, (Long.valueOf(point)).toString());
                    eventData.put(UtilStatic.MANIS_KEY_STORE_ID, UtilStatic.MANIS_EBIZU_STORE_ID);
                    eventData.put(UtilStatic.MANIS_KEY_STORE_NAME, UtilStatic.MANIS_EBIZU_STORE_NAME);
                    eventData.put(UtilStatic.MANIS_KEY_STORE_CATEGORY, UtilStatic.MANIS_EBIZU_STORE_CATEGORY);
                    eventData.put(UtilStatic.MANIS_KEY_STORE_CATEGORY_ID, UtilStatic.MANIS_EBIZU_STORE_CATEGORY_ID);
                    UtilManis.ebizuTrackCustomEvent(context, UtilStatic.MANIS_EVENT_REDEEMED, eventData);
                }
            }

            @Override
            public void onFailure(String s) {
                Log.e(TAG, "Redeemption : " + s);
                redeemCloseFailed(reward.getId(), refCode, reward.getPoint());
                UtilManis.info(context, context.getString(R.string.text_error_occured), s);
                rewardDetailView.getBaseActivity().dismissProgressBarDialog();
            }
        });
    }

    private void redeemClose(String refCode, Redeem respose, RewardVoucher reward) {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceWithToken(context);

        RedeemCloseBody redeemCloseBody = new RedeemCloseBody();
        redeemCloseBody.setId(reward.getId());
        redeemCloseBody.setRef(refCode);
        redeemCloseBody.setCode(respose.getCode());
        redeemCloseBody.setSn(respose.getSn());
        redeemCloseBody.setTrx(respose.getTransactionId());
        redeemCloseBody.setExpired(reward.getExpired());
        redeemCloseBody.setVoucher_image(reward.getImage128());
        redeemCloseBody.setReward_name(reward.getName());
        redeemCloseBody.setCom_name(reward.getProvider().getName());
        redeemCloseBody.setReward_type(reward.getType());
        redeemCloseBody.setPoint(reward.getPoint());
        redeemCloseBody.setTrackLink(respose.getTrackLink());

        releaseSubscribe(1);
        Subscription subsRedeemClose = manisApi.getRedeemClose(new Gson().toJson(redeemCloseBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<ResponseData>(rewardDetailView) {
                    @Override
                    public void onNext(ResponseData responseData) {
                        super.onNext(responseData);
                        if (reward.getVoucherRedeemType().equals(Reward.VOUCHER_REDEEM_PIN)) {
                            rewardDetailView.showDialogRedeemWithPin(refCode);
                        }
                    }

                    @Override
                    public void onErrorFailure(ErrorResponse errorResponse) {
                        super.onErrorFailure(errorResponse);
                        rewardDetailView.getBaseActivity().dismissProgressBarDialog();
                        UtilManis.info(context, context.getString(R.string.text_error_occured), errorResponse.getMessage());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        rewardDetailView.getBaseActivity().dismissProgressBarDialog();
                        if (!ConnectionDetector.isNetworkConnected(context)) {
                            UtilManis.info(context, context.getString(R.string.error),
                                    rewardDetailView.getResources().getString(R.string.error_no_connection));
                        }
                    }
                });
        subsRewardDetail.put(1, subsRedeemClose);
    }

    private void redeemCloseFailed(final String id, final String ref, final long point) {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceWithToken(context);
        RedeemCloseFailedBody redeemCloseFailedBody = new RedeemCloseFailedBody();
        redeemCloseFailedBody.setId(id);
        redeemCloseFailedBody.setRef(ref);
        redeemCloseFailedBody.setPoint(point);
        Subscription subsCloseFailed = manisApi.getRedeemCloseFailed(new Gson().toJson(redeemCloseFailedBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(wrapperredeemclosefailed -> {
                        },
                        throwable -> {
                        });
        subsRewardDetail.put(2, subsCloseFailed);
    }

    @Override
    public void getShoppingCart(RewardVoucher reward, int qty) {
        releaseSubscribe(3);
        rewardDetailView.getBaseActivity()
                .showProgressBarDialog("Loading...", (var1) -> releaseSubscribe(3));
        RewardShoppingCartBody rewardShoppingCartBody =
                new RewardShoppingCartBody(rewardDetailView.getContext(), getRewardShopData(reward, qty));
        Subscription subsShoppingCart = getRewardApi().getShoppingCart(rewardShoppingCartBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperShoppingCart>(rewardDetailView) {
                    @Override
                    protected void onSuccess(WrapperShoppingCart wrapperShoppingCart) {
                        super.onSuccess(wrapperShoppingCart);
                        rewardDetailView.getBaseActivity().dismissProgressBarDialog();
                        rewardDetailView.startActivityShoppingCart(wrapperShoppingCart.getData().getShoppingCart());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        rewardDetailView.getBaseActivity().dismissProgressBarDialog();
                        if (!ConnectionDetector.isNetworkConnected(rewardDetailView.getContext()))
                            NegativeScenarioManager.show(rewardDetailView.getResources().getString(R.string.error_no_connection),
                                    rewardDetailView, NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }

                    @Override
                    public void onFailure(ResponseRewardApi responseRewardApi) {
                        super.onFailure(responseRewardApi);
                        rewardDetailView.getBaseActivity().dismissProgressBarDialog();
                        NegativeScenarioManager.show(responseRewardApi, rewardDetailView, NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }
                });
        subsRewardDetail.put(3, subsShoppingCart);
    }

    private RewardShoppingCartBody.Data getRewardShopData(RewardVoucher reward, int qty) {
        if (null == rewardShopData)
            rewardShopData = new RewardShoppingCartBody.Data();
        rewardShopData.setQty(qty);
        rewardShopData.setVoucherId(reward.getId());
        rewardShopData.setBulkId(reward.getBulkId());
        return rewardShopData;
    }

    @Override
    public void setVoucherInputList(List<VoucherInput> voucherInputs) {
        this.voucherInputs.clear();
        this.voucherInputs.addAll(voucherInputs);
    }
}
