package com.ebizu.manis.mvp.snap.store.list.searchresult;

import com.ebizu.manis.model.snap.SnapStoreResult;
import com.ebizu.manis.mvp.snap.store.list.IListSnapStorePresenter;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by Ebizu-User on 08/08/2017.
 */

public interface ISearchResultView extends IBaseView {

    void addSnapStores(SnapStoreResult snapStoreResult, LoadType loadType);

    SearchResultPresenter getSearchResultPresenter();

    void setOnSnapAbleListener(IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener);

}
