package com.ebizu.manis.mvp.onboard.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;

/**
 * Created by ebizu on 8/22/17.
 */

public class OnBoardEarn extends OnBoard
        implements IOnBoard {

    public OnBoardEarn(Context context) {
        super(context);
    }

    @Override
    public int index() {
        return 1;
    }

    @Override
    public String title() {
        return context.getResources().getString(R.string.exp_earnpoint);
    }

    @Override
    public String messageBody() {
        return context.getResources().getString(R.string.exp_earnpoint_detail);
    }

    @Override
    public int colorBackground() {
        return ContextCompat.getColor(context, R.color.color_graph_green);
    }

    @Override
    public int rawVideo() {
        return R.raw.onboard2;
    }
}
