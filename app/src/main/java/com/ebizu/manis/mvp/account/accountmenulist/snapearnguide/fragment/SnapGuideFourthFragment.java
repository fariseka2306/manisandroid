package com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.fragment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.helper.UtilStatic;

import java.util.HashMap;

public class SnapGuideFourthFragment extends Fragment {

    private boolean isCreated = false;
    private AnimatorSet mAnimatorSet;

    // Fourth Screen
    private TextView textViewTitleFifth;
    private TextView textviewDetailFifth;
    private View viewOvalWhiteFifth;
    private View viewOvalWhite2Fifth;
    private View viewOvalSlateFifth;
    private ImageView imageViewReceiptFifth;
    private ImageView imageViewCorrectFifth;
    private ImageView imageViewIncorrectFifth;
    private View imageViewReceiptBorderFifth;
    private View imageViewReceiptDetectionFifth;
    private Button buttonNextFourth;
    private TextView textViewExplanationFifth;

    private HashMap<View, Float> mOriginalXValuesMap = new HashMap<>();
    private HashMap<View, Float> mOriginalYValuesMap = new HashMap<>();

    private View.OnClickListener nextListener = v -> {
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_snapguide_fourth, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isCreated = true;
        Bundle args = getArguments();
        int position = args.getInt(UtilStatic.ARG_POSITION);
        view.setTag(position);
    }

    private void initView(View rootView) {
        textViewTitleFifth = (TextView) rootView.findViewById(R.id.textview_title);
        textviewDetailFifth = (TextView) rootView.findViewById(R.id.textview_detail);
        viewOvalWhiteFifth = rootView.findViewById(R.id.view_oval_white);
        viewOvalWhite2Fifth = rootView.findViewById(R.id.view_oval_white2);
        viewOvalSlateFifth = rootView.findViewById(R.id.view_oval_slate);
        imageViewReceiptFifth = (ImageView) rootView.findViewById(R.id.imageview_receipt);
        imageViewCorrectFifth = (ImageView) rootView.findViewById(R.id.imageview_correct);
        imageViewIncorrectFifth = (ImageView) rootView.findViewById(R.id.imageview_incorrect);
        imageViewReceiptBorderFifth = rootView.findViewById(R.id.view_border);
        imageViewReceiptDetectionFifth = rootView.findViewById(R.id.view_detection);
        buttonNextFourth = (Button) rootView.findViewById(R.id.button_next);
        textViewExplanationFifth = (TextView) rootView.findViewById(R.id.textview_explanation);
        textViewExplanationFifth.setText(UtilManis.fromHtml(getString(R.string.snapguide_fifth_explanation)));
        buttonNextFourth.setOnClickListener(nextListener);

        initializeFirstPosition();
        initializeStateView();
        rootView.post(() -> getOriginalYValues());

    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && isCreated) {
            doAnimation();
        } else if (!menuVisible && isCreated) {
            if (mAnimatorSet != null) mAnimatorSet.end();
            setViewsInOriginalPosition();
        }
    }

    private void getOriginalYValues() {
        mOriginalXValuesMap.put(viewOvalWhite2Fifth, viewOvalWhite2Fifth.getScaleX());
        mOriginalYValuesMap.put(viewOvalWhite2Fifth, viewOvalWhite2Fifth.getScaleY());
        mOriginalXValuesMap.put(imageViewReceiptBorderFifth, imageViewReceiptBorderFifth.getScaleX());
        mOriginalYValuesMap.put(imageViewReceiptBorderFifth, imageViewReceiptBorderFifth.getScaleY());
        mOriginalXValuesMap.put(imageViewReceiptFifth, imageViewReceiptFifth.getScaleX());
        mOriginalYValuesMap.put(imageViewReceiptFifth, imageViewReceiptFifth.getScaleY());
        mOriginalXValuesMap.put(imageViewIncorrectFifth, imageViewIncorrectFifth.getScaleX());
        mOriginalYValuesMap.put(imageViewIncorrectFifth, imageViewIncorrectFifth.getScaleY());
    }

    private void setViewsInOriginalPosition() {
        if (mOriginalXValuesMap != null & mOriginalYValuesMap != null) {
            viewOvalWhite2Fifth.setScaleX(mOriginalXValuesMap.get(viewOvalWhite2Fifth));
            viewOvalWhite2Fifth.setScaleY(mOriginalYValuesMap.get(viewOvalWhite2Fifth));
            imageViewReceiptBorderFifth.setScaleX(mOriginalXValuesMap.get(imageViewReceiptBorderFifth));
            imageViewReceiptBorderFifth.setScaleY(mOriginalYValuesMap.get(imageViewReceiptBorderFifth));
            imageViewReceiptFifth.setScaleX(mOriginalXValuesMap.get(imageViewReceiptFifth));
            imageViewReceiptFifth.setScaleY(mOriginalYValuesMap.get(imageViewReceiptFifth));
            imageViewIncorrectFifth.setScaleX(mOriginalXValuesMap.get(imageViewIncorrectFifth));
            imageViewIncorrectFifth.setScaleY(mOriginalYValuesMap.get(imageViewIncorrectFifth));
            initializeStateView();
        }
    }

    private void initializeStateView() {
        imageViewReceiptDetectionFifth.setAlpha(0f);
        imageViewCorrectFifth.setAlpha(0f);
        viewOvalSlateFifth.setAlpha(0f);
    }

    private void initializeFirstPosition() {
        viewOvalWhite2Fifth.setScaleX(0.5f);
        viewOvalWhite2Fifth.setScaleY(0.5f);
        imageViewReceiptBorderFifth.setScaleX(0f);
        imageViewReceiptBorderFifth.setScaleY(0f);
        imageViewReceiptFifth.setScaleX(0f);
        imageViewReceiptFifth.setScaleY(0f);
        imageViewIncorrectFifth.setScaleX(0f);
        imageViewIncorrectFifth.setScaleY(0f);
    }


    public void doAnimation() {
        ObjectAnimator fadeTitleFourth = ObjectAnimator.ofFloat(textViewTitleFifth, "alpha", 0f, 1f);
        fadeTitleFourth.setDuration(700);

        ObjectAnimator fadeDetailFourth = ObjectAnimator.ofFloat(textviewDetailFifth, "alpha", 0f, 1f);
        fadeDetailFourth.setDuration(700);

        ObjectAnimator scaleXoval = ObjectAnimator.ofFloat(viewOvalWhite2Fifth, "scaleX", 1f);
        scaleXoval.setDuration(800);

        ObjectAnimator scaleYoval = ObjectAnimator.ofFloat(viewOvalWhite2Fifth, "scaleY", 1f);
        scaleYoval.setDuration(800);

        ObjectAnimator scaleXreceipt = ObjectAnimator.ofFloat(imageViewReceiptFifth, "scaleX", 1f);
        scaleXoval.setDuration(800);

        ObjectAnimator scaleYreceipt = ObjectAnimator.ofFloat(imageViewReceiptFifth, "scaleY", 1f);
        scaleYoval.setDuration(800);

        ObjectAnimator scaleXreceiptBorder = ObjectAnimator.ofFloat(imageViewReceiptBorderFifth, "scaleX", 1f);
        scaleXoval.setDuration(800);

        ObjectAnimator scaleYreceiptBorder = ObjectAnimator.ofFloat(imageViewReceiptBorderFifth, "scaleY", 1f);
        scaleYoval.setDuration(800);

        ObjectAnimator scaleXincorrect = ObjectAnimator.ofFloat(imageViewIncorrectFifth, "scaleX", 1f);
        scaleXoval.setDuration(800);

        ObjectAnimator scaleYincorrect = ObjectAnimator.ofFloat(imageViewIncorrectFifth, "scaleY", 1f);
        scaleYoval.setDuration(800);

        ObjectAnimator fadeSlate = ObjectAnimator.ofFloat(viewOvalSlateFifth, "alpha", 0f, 1f);
        fadeSlate.setDuration(700);

        ObjectAnimator fadeDetection = ObjectAnimator.ofFloat(imageViewReceiptDetectionFifth, "alpha", 0f, 1f);
        fadeDetection.setDuration(700);

        ObjectAnimator fadeCorrect = ObjectAnimator.ofFloat(imageViewCorrectFifth, "alpha", 0f, 1f);
        fadeCorrect.setDuration(700);


        mAnimatorSet = new AnimatorSet();
        scaleXreceipt.setStartDelay(400);
        scaleXreceipt.setStartDelay(400);
        scaleXreceiptBorder.setStartDelay(400);
        scaleYreceiptBorder.setStartDelay(400);
        scaleXincorrect.setStartDelay(1200);
        scaleYincorrect.setStartDelay(1200);
        fadeSlate.setStartDelay(2000);
        fadeCorrect.setStartDelay(2800);
        fadeDetection.setStartDelay(3000);

        mAnimatorSet.play(fadeTitleFourth)
                .with(fadeDetailFourth)
                .with(scaleXoval)
                .with(scaleYoval)
                .with(scaleXreceipt)
                .with(scaleYreceipt)
                .with(scaleXreceiptBorder)
                .with(scaleYreceiptBorder)
                .with(scaleXincorrect)
                .with(scaleYincorrect)
                .with(fadeSlate)
                .with(fadeDetection)
                .with(fadeCorrect);
        mAnimatorSet.start();
    }
}
