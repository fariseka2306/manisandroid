package com.ebizu.manis.mvp.account.accountmenulist.profile.activityresult;

import android.content.Intent;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileActivity;

/**
 * Created by ebizu on 10/13/17.
 */

public class LuckyDrawSnapRequest extends ProfileRequestCode
        implements IProfileRequest {

    public LuckyDrawSnapRequest(ProfileActivity profileActivity) {
        super(profileActivity);
    }

    @Override
    public int requestCode() {
        return ConfigManager.Snap.RECEIPT_REQUEST_CODE;
    }

    @Override
    public void doRequest(int resultCode, Intent data) {
        super.doRequest(resultCode, data);
        if (resultCode == ConfigManager.Snap.EARN_TICKET_RECEIPT_SUCCESS_UPLOAD_RS) {
            boolean openLuckyDraw = data.getBooleanExtra(ConfigManager.Snap.OPEN_LUCKY_DRAW_FRAGMENT, false);
            profileActivity.getIntent().putExtra(ConfigManager.Snap.OPEN_LUCKY_DRAW_FRAGMENT, openLuckyDraw);
            profileActivity.setResult(ConfigManager.Snap.EARN_TICKET_RECEIPT_SUCCESS_UPLOAD_RS);
            profileActivity.finish();
        }
    }
}
