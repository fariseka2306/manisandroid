package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public class PurchaseHistoryActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;

    @BindView(R.id.purchase_history_view)
    PurchaseHistoryView purchaseHistoryView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_purchase_history);
        ButterKnife.bind(this);
        purchaseHistoryView.setActivity(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {
        toolbarView.setTitle(R.string.ph_title);
    }
}
