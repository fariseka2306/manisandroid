package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.service.reward.requestbody.RewardCancelPaymentBody;
import com.ebizu.manis.service.reward.response.WrapperShoppingCart;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ebizu on 9/27/17.
 */

public class ShoppingCartPresenter extends BaseViewPresenter implements IShoppingCartPresenter {

    private ShoppingCartView shoppingCartView;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        shoppingCartView = (ShoppingCartView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        //not implemented method
    }

    @Override
    public void releaseAllSubscribes() {
        //not implemented method
    }

    @Override
    public void cancelPayment(String orderId) {
        RewardCancelPaymentBody.Data data = new RewardCancelPaymentBody.Data();
        data.setOrderId(orderId);
        RewardCancelPaymentBody rewardCancelPaymentBody = new RewardCancelPaymentBody(shoppingCartView.getContext(), data);
        getRewardApi()
                .cancelPayment(rewardCancelPaymentBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperShoppingCart>(shoppingCartView) {});
    }
}
