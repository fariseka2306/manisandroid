package com.ebizu.manis.mvp.onboard.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.mvp.onboard.fragment.OnBoardFragment;
import com.ebizu.manis.mvp.onboard.view.IOnBoard;
import com.ebizu.manis.mvp.onboard.view.OnBoardInterface;

import java.util.List;

/**
 * Created by ebizu on 8/22/17.
 */

public class OnBoardFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private List<OnBoardFragment> onBoardFragments;
    private OnBoardInterface onBoardInterface;
    private AnalyticManager analyticManager;

    public OnBoardFragmentPagerAdapter(FragmentManager fm, OnBoardInterface onBoardInterface, List<OnBoardFragment> onBoardFragments) {
        super(fm);
        this.onBoardInterface = onBoardInterface;
        this.onBoardFragments = onBoardFragments;
        if (null == analyticManager)
            analyticManager = new AnalyticManager(onBoardInterface.getContext());
    }

    @Override
    public Fragment getItem(int position) {
        OnBoardFragment onBoardFragment = onBoardFragments.get(position);
        onBoardFragment.setOnCreateViewListener(() -> {
                    setOnBoardFragmentView(onBoardFragment, onBoardInterface.getViews()[position]);
                    startVideoFirst(position);
                }
        );
        traceEvent(position);
        return onBoardFragment;
    }

    @Override
    public int getCount() {
        return onBoardFragments.size();
    }


    private void setOnBoardFragmentView(OnBoardFragment onBoardFragment, IOnBoard onBoard) {
        onBoardFragment.setTitle(onBoard.title());
        onBoardFragment.setMessageBody(onBoard.messageBody());
        onBoardFragment.setBackgroundColor(onBoard.colorBackground());
        onBoardFragment.setVideoRaw(onBoard.rawVideo());
    }

    private void startVideoFirst(int position) {
        if (position == 0)
            onBoardFragments.get(position).startVideo();
    }

    private void traceEvent(int position) {
        if (position == 0) {
            analyticManager.trackScreenView(ConfigManager.Analytic.ScreenView.FRAGMENT_ONBOARDING_EARN);
        } else if (position == 1) {
            analyticManager.trackScreenView(ConfigManager.Analytic.ScreenView.FRAGMENT_ONBOARDING_SNAP);
        } else if (position == 2) {
            analyticManager.trackScreenView(ConfigManager.Analytic.ScreenView.FRAGMENT_ONBOARDING_STUFF);
        }
    }
}