package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.mvp.reward.purchasevoucher.PurchaseRewardActivity;
import com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary.MidTransOrderSummaryActivity;
import com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary.MolPayOrderSummaryActivity;
import com.ebizu.manis.view.manis.toolbar.ToolbarShopCartView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 9/27/17.
 */

public abstract class ShoppingCartActivity extends PurchaseRewardActivity {

    @BindView(R.id.toolbar_cart_view)
    ToolbarShopCartView toolbarShopCartView;
    @BindView(R.id.physical_shopping_cart_view)
    PhysicalShoppingCartView physicalCartView;
    @BindView(R.id.general_shopping_cart_view)
    RegularShoppingCartView generalCartView;

    private ShoppingCartView baseView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_shopping_chart);
        ButterKnife.bind(this);
        baseView = getBaseView();
        initiateView();
        setOnValidShoppingCartLister();
    }

    protected void setOnValidShoppingCartLister() {
        Account account = getManisSession().getAccountSession();
        String accCountry = account.getAccCountry();
        baseView.setOnValidationListener(() -> {
            if (accCountry.equals(getString(R.string.localization_IDN_tag))) {
                nextPurchaseVoucher(MidTransOrderSummaryActivity.class, baseView.getOrder());
            } else if (accCountry.equals(getString(R.string.localization_MYS_tag))) {
                nextPurchaseVoucher(MolPayOrderSummaryActivity.class, baseView.getOrder());
            }
        });
    }

    protected void initiateView() {
        baseView.setVisibility(View.VISIBLE);
        baseView.setActivity(this);
        baseView.attachPresenter(new ShoppingCartPresenter());
        baseView.setShoppingCartView(reward);
        baseView.setOrderDetailView(shoppingCart);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {
        baseView.getShoppingCartPresenter().cancelPayment(shoppingCart.getOrderId());
        super.onBackPressed();
    }

    public abstract ShoppingCartView getBaseView();
}
