package com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary;

import com.ebizu.manis.model.purchasevoucher.MolPaymentResult;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.reward.requestbody.RewardPurchaseStatusBody;
import com.ebizu.sdk.reward.models.Reward;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;

/**
 * Created by ebizu on 10/3/17.
 */

public interface IOrderSummaryPresenter
        extends IBaseViewPresenter {

    void sendPurchaseStatus(MolPaymentResult molPaymentResult, Order order);

    void sendPurchaseStatus(TransactionResult transactionResult, Order order);

    void sendPurchaseStatus(RewardPurchaseStatusBody rewardPurchaseStatusBody);

}
