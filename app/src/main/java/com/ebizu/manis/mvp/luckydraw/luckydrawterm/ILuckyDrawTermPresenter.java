package com.ebizu.manis.mvp.luckydraw.luckydrawterm;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public interface ILuckyDrawTermPresenter extends IBaseViewPresenter {

    void loadLuckyDrawTerm(ManisApi manisApi);
}
