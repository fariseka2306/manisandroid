package com.ebizu.manis.mvp.snap.store.list.recent;

import com.ebizu.manis.mvp.snap.store.SnapStoreHelper;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.response.WrapperSnapStore;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public class RecentStorePresenter extends BaseViewPresenter implements IRecentStorePresenter {

    private RecentStoreView recentStoreView;
    private Subscription subsRecentStore;

    @Override
    public void attachView(BaseView baseView) {
        recentStoreView = (RecentStoreView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsRecentStore != null) subsRecentStore.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void getSnapStoresRecent(int page, IBaseView.LoadType loadType) {
        recentStoreView.setLoading(true);
        releaseSubscribe(0);
        subsRecentStore = recentStoreView.getManisApi().getSnapStoresRecent(SnapStoreHelper.getSnapStoreBody(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperSnapStore>(recentStoreView) {
                    @Override
                    public void onNext(WrapperSnapStore wrapperSnapStore) {
                        super.onNext(wrapperSnapStore);
                        recentStoreView.addRecentStores(wrapperSnapStore.getSnapStoreResult(), loadType);
                        recentStoreView.getOnRefresListener().onRefreshComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        recentStoreView.setLoading(false);
                        recentStoreView.getOnRefresListener().onRefreshComplete();
                    }
                });
    }
}
