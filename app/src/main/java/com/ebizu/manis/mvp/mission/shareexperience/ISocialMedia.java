package com.ebizu.manis.mvp.mission.shareexperience;

import com.ebizu.manis.manager.facebook.FacebookManager;
import com.ebizu.manis.manager.twitter.TwitterManager;
import com.ebizu.manis.root.IBaseActivityPresenter;

public interface ISocialMedia extends IBaseActivityPresenter {
    void shareViaFB();

    void takePhoto();

    void shareViaTwitter();

    void loginTwitter();

    void getFriendsCount();

    void checkFollowerCount(int followersCount);

    FacebookManager getFacebookManager();

    TwitterManager getTwitterManager();
}
