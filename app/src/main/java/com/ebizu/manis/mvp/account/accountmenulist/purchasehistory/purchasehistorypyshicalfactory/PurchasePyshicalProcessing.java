package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistorypyshicalfactory;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.model.PurchaseHistory;

/**
 * Created by halim_ebizu on 11/16/17.
 */

public class PurchasePyshicalProcessing extends PurchasePyshicalAbstract implements PurchasePyshicalStatus {

    public PurchasePyshicalProcessing(PurchaseHistory purchaseHistory, Context context) {
        super(purchaseHistory, context);
    }

    @Override
    public String getDeliveryStatus() {
        return "4";
    }

    @Override
    public String getStatus() {
        return context.getString(R.string.processing);
    }

    @Override
    public Drawable imageIcon() {
        return ContextCompat.getDrawable(context, R.drawable.processing_icon);
    }

    @Override
    public String getInstruction() {
        return context.getString(R.string.ph_physical_processing, purchaseHistory.getNotes());
    }

    @Override
    public int getInstructionVisibility() {
        return View.GONE;
    }
}
