package com.ebizu.manis.mvp.account.accountmenulist.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.accountlistmenusettings.AccountListMenuSettingsManager;
import com.ebizu.manis.manager.accountlistmenusettings.AccountListSettings;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.adapter.account.AccountListMenuSettingsAdapter;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/20/2017.
 */

public class SettingsListActivity extends BaseActivity implements AccountListMenuSettingsAdapter.AccountMenuListener {

    @BindView(R.id.recylerview_account_menulist_settings)
    RecyclerView recyclerViewAccountMenuListSettings;
    @BindView(R.id.toolbar)
    ToolbarView toolbarView;

    AccountListMenuSettingsAdapter accountListMenuSettingsAdapter;
    private LinearLayoutManager linearLayoutManagerAccountMenu;
    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_menulist_settings);
        context = this;
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        toolbarView.setTitle(R.string.st_title);

        this.accountListMenuSettingsAdapter = new AccountListMenuSettingsAdapter(context, new ArrayList<>());
        accountListMenuSettingsAdapter.setAccountListener(this);
        linearLayoutManagerAccountMenu = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewAccountMenuListSettings.setLayoutManager(linearLayoutManagerAccountMenu);
        recyclerViewAccountMenuListSettings.setAdapter(accountListMenuSettingsAdapter);
        showViewAccountSettings();
    }

    private void showViewAccountSettings() {
        AccountListMenuSettingsManager.addAccountListMenu(accountListMenuSettingsAdapter);
    }

    @Override
    public void onClick(AccountListSettings accountList) {
        AccountListMenuSettingsManager.onClickMenu(context, accountList);
        AccountListMenuSettingsManager.onClickMenu(this, accountList);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finishActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.SETTING_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button AboutActivity");
    }
}