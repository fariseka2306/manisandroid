package com.ebizu.manis.mvp.interest;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public interface IInterestPresenter extends IBaseViewPresenter {

    void getUserInterest(ManisApi manisApi);

    void saveInterest(ManisApi manisApi, String data);
}