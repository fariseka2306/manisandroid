package com.ebizu.manis.mvp.splashscreen;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ManisTime;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.launcher.LauncherManager;
import com.ebizu.manis.manager.versioning.VersioningManager;
import com.ebizu.manis.model.Versioning;
import com.ebizu.manis.mvp.beacon.BeaconPresenter;
import com.ebizu.manis.mvp.versioning.IVersioningView;
import com.ebizu.manis.mvp.versioning.VersioningPresenter;
import com.ebizu.manis.mvp.beacon.BeaconPresenter;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.VersioningBody;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.google.android.gms.common.ConnectionResult;

import org.json.JSONObject;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by Ebizu-User on 06/07/2017.
 */

public class SplashScreenActivity extends BaseActivity
        implements ISplashScreenActivity, IVersioningView {

    @Inject
    Context context;
    @Inject
    ManisApi manisApi;
    @Inject
    ManisSession manisSession;
    @Inject
    SplashScreenActivityPresenter presenter;
    @Inject
    VersioningPresenter versioningPresenter;

    @BindView(R.id.sp_img_manis)
    ImageView manisLogoImageView;
    @BindView(R.id.sp_txt_desc)
    TextView splashDecscTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        loadLogoAnimation();
        presenter.attachView(this);
        versioningPresenter.attachView(this);
        attachPresenter(presenter);
        attachPresenter(versioningPresenter);
        if (presenter.checkAllRequire()) {
            versioningPresenter.requestVersioning();
            getBeaconInteraval();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Branch branch = Branch.getInstance(getApplicationContext());
        branch.initSession((referringParams, error) -> {
            if (error == null) {
                // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
                // params will be empty if no data found
                // ... insert custom logic here ...
            } else {
                Log.i("MyApp", error.getMessage());
            }
        }, this.getIntent().getData(), this);
        Branch.getInstance().isUserIdentified();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent = DaggerActivityComponent
                .builder()
                .applicationComponent(getApplicationComponent())
                .build();
        activityComponent.inject(this);
    }

    private void getBeaconInteraval() {
        BeaconPresenter beaconPresenter = new BeaconPresenter(this);
        beaconPresenter.getIntevalBeacon(manisApi);
    }

    @Override
    public void setTextDescSplash() {
        splashDecscTextView.setText(UtilManis.fromHtml(getString(R.string.sp_desc)));
    }

    @Override
    public void loadLogoAnimation() {
        setTextDescSplash();
        ObjectAnimator anim = (ObjectAnimator) AnimatorInflater.loadAnimator(context, R.animator.flipping);
        anim.setTarget(manisLogoImageView);
        anim.setDuration(ManisTime.ANIMATION_SPLASHSCREEN_MILLI_SECOND);
        anim.start();
    }

    @Override
    public void showDialogDeviceBlock() {
        showAlertDialog(getString(R.string.error), getString(R.string.error_emulator), false,
                getString(R.string.dr_btn_retry), (dialog, which) -> {
                    presenter.checkDeviceRooted();
                    presenter.checkDeviceEmulator();
                },
                getString(R.string.dr_btn_close), (dialog, which) -> finish());
    }

    @Override
    public void showDialogManisPlaystore() {
        showAlertDialog(getString(R.string.error_invalid), getString(R.string.error_external_apk), false, R.drawable.ic_launcher,
                getString(R.string.dr_btn_continue), (dialog, which) -> {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.market_id) + BuildConfig.APPLICATION_ID)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.playstore_id) + BuildConfig.APPLICATION_ID)));
                    }
                    finish();
                },
                getString(R.string.dr_btn_close), (dialog, which) -> finish());
    }

    @Override
    public void nextActivity() {
        new Handler().postDelayed(() -> {
            startActivity(LauncherManager.getIntentNextSplash(this, this));
            finish();
        }, ManisTime.ANIMATION_SPLASHSCREEN_MILLI_SECOND);
    }

    @Override
    public void showGoogleConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void showVersioning(Versioning versionings, Context context) {
        BaseActivity baseActivity = this;
        VersioningManager.getVersioningStatus(versionings, context, baseActivity);
    }
}