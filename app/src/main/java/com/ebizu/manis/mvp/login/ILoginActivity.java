package com.ebizu.manis.mvp.login;

import com.ebizu.manis.model.Account;
import com.ebizu.manis.root.IBaseActivity;

/**
 * Created by Ebizu-User on 10/07/2017.
 */

public interface ILoginActivity extends IBaseActivity {

    void nextActivity(Account account);

    void validateOtp(Account account);

}
