package com.ebizu.manis.mvp.luckydraw.luckydrawdialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerOriginPageLuckyDraw;

/**
 * Created by Halim on 8/6/17.
 */

public class LuckyDrawEntryDialog extends BaseActivity {

    @Inject
    Context context;
    @Inject
    ManisApi manisApi;
    @BindView(R.id.lucky_draw_dialog_view)
    LuckyDrawDialogView luckyDrawDialogView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideInVerticalAnim();
        setContentView(R.layout.dialog_lucky_draw_entry);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    @OnClick(R.id.ld_btn_close)
    public void onLdBtnClose() {
        finish();
    }

    private void initView() {
        luckyDrawDialogView.setActivity(this);
        luckyDrawDialogView.setViewType(ILuckyDrawDialogView.ViewType.LUCKY_DRAW_ENTRY);
        luckyDrawDialogView.rvLuckyDrawEntry.setVisibility(View.VISIBLE);
        luckyDrawDialogView.attachPresenter(new LuckyDrawDialogPresenter());
        RequestBody requestBody = luckyDrawDialogView.getLuckyDrawDialogPresenter().createLuckyDrawEntriesRB(1);
        luckyDrawDialogView.getLuckyDrawDialogPresenter().loadLuckyDrawEntryData(IBaseView.LoadType.CLICK_LOAD, requestBody);
    }

    @Override
    public void finish() {
        super.finish();
        slideOutVerticalAnim();
        TrackerManager.getInstance().setTrackerData(
                new TrackerStandartRequest(kTrackerOriginPageLuckyDraw, kTrackerOriginPageLuckyDraw, "", "", "", ""));
    }
}
