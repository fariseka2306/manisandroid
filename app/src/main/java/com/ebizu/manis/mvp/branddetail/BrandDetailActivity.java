package com.ebizu.manis.mvp.branddetail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.Brand;
import com.ebizu.manis.model.snap.SnapStoreResult;
import com.ebizu.manis.mvp.snap.SnapActivity;
import com.ebizu.manis.mvp.store.storenearby.PaginationScrollListener;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.view.adapter.SnapStoreAdapter;
import com.ebizu.manis.view.linearlayout.FixedLinearLayoutManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 7/21/17.
 */

public class BrandDetailActivity extends SnapActivity implements IBrandDetailView {

    private static final String TAG = BrandDetailActivity.class.getSimpleName();

    public static String BRAND_DATA = "brand-data";

    private BrandDetailPresenter brandDetailPresenter;
    private Context context;
    private Brand brand;
    private int page = 1;
    private SnapStoreAdapter snapStoreAdapter;
    private LinearLayoutManager layoutManager;

    private boolean isLoading = false;
    private boolean isLastPage = false;

    @BindView(R.id.rv_stores)
    RecyclerView recyclerBrandDetails;
    @BindView(R.id.pb_loader)
    ProgressBar progressLoad;
    @BindView(R.id.sh_toolbar)
    Toolbar shToolbar;
    @BindView(R.id.pb_loader_pagination)
    ProgressBar progressLoadPagination;
    @BindView(R.id.lin_progress_bar_pagination)
    LinearLayout linProgressPagination;
    @BindView(R.id.swipe_home)
    SwipeRefreshLayout swipeHome;
    @BindView(R.id.text_no_store)
    TextView txtNoStoreBrand;

    private OnRefreshBrandDetailListener onRefreshBrandDetailListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_detail);
        context = this;
        ButterKnife.bind(this);

        brand = getIntent().getParcelableExtra(ConfigManager.BrandDetails.BRAND_DETAILS);
        brandDetailPresenter = new BrandDetailPresenter(context);
        brandDetailPresenter.attachBrandDetail(this);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        initialize();
        initListener();
        brandDetailPresenter.loadBrandDetailList("", page, brand.getId(), IBaseView.LoadType.CLICK_LOAD);
        attachPresenter(brandDetailPresenter);
    }

    public void initListener() {
        swipeHome.setOnRefreshListener(() -> {
            page = 1;
            brandDetailPresenter.loadBrandDetailList("", page, brand.getId(), IBaseView.LoadType.SWIPE_LOAD);
            if (swipeHome.isRefreshing()) {
                swipeHome.setRefreshing(false);
            }
            if (onRefreshBrandDetailListener != null) {
                onRefreshBrandDetailListener.onRefresh();
            }
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.SNAP_BY_BRAND,
                    ConfigManager.Analytic.Action.REFRESH,
                    "Swipe Refresh"
            );
        });
        recyclerBrandDetails.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                setPage(page + 1);
                brandDetailPresenter.loadBrandDetailList("", page, brand.getId(), IBaseView.LoadType.SCROLL_LOAD);
            }

            @Override
            public int getTotalPageCount() {
                return BrandDetailActivity.this.snapStoreAdapter.getSnapStores().size();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void initialize() {
        shToolbar.setContentInsetsAbsolute(0, 0);
        setSupportActionBar(shToolbar);
        ActionBar actionBar = getSupportActionBar();

        View view = getLayoutInflater().inflate(R.layout.actionbar_back, null);
        TextView txtTitle = (TextView) view.findViewById(R.id.txt_title_action);
        txtTitle.setText(getString(R.string.se_title));
        RelativeLayout relBack = (RelativeLayout) view.findViewById(R.id.rel_left);
        relBack.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });

        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.CENTER
                )
        );
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);

        snapStoreAdapter = new SnapStoreAdapter(this, new ArrayList<>());

        FixedLinearLayoutManager fixedLinearLayoutManager = new FixedLinearLayoutManager(context);
        fixedLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerBrandDetails.setLayoutManager(layoutManager);
        recyclerBrandDetails.setAdapter(snapStoreAdapter);
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public SnapStoreAdapter getSnapStoreAdapter() {
        return snapStoreAdapter;
    }

    @Override
    public void addViewBrandDetail(SnapStoreResult snapStoreResult, IBaseView.LoadType loadType) {
        isLastPage = !snapStoreResult.getMore();
        switch (loadType) {
            case CLICK_LOAD:
                snapStoreAdapter.replaceNearStores(snapStoreResult.getStores());
                break;
            case SCROLL_LOAD:
                snapStoreAdapter.addNearStores(snapStoreResult.getStores());
                break;
            case SWIPE_LOAD:
                snapStoreAdapter.replaceNearStores(snapStoreResult.getStores());
                break;
        }
        recyclerBrandDetails.setVisibility(View.VISIBLE);
        progressLoad.setVisibility(View.GONE);
    }

    @Override
    public void failLoadViewBrand(IBaseView.LoadType loadType, Throwable e) {
        switch (loadType) {
            case CLICK_LOAD:
                break;
            case SCROLL_LOAD:
//                NegativeScenarioManager.show(e, this, NegativeScenarioManager.NegativeView.NOTIFICATION);
                break;
            case SWIPE_LOAD:
                break;
        }
    }

    @Override
    public void noDataBrandDetail() {
        progressLoad.setVisibility(View.GONE);
        txtNoStoreBrand.setVisibility(View.VISIBLE);
        txtNoStoreBrand.setText(getString(R.string.error_no_data));
    }

    @Override
    public void finish() {
        super.finish();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.SNAP,
                ConfigManager.Analytic.Action.REFRESH,
                "Button Back");
    }

    public void setPage(int page){
        this.page = page;
    }

    public interface OnRefreshBrandDetailListener {
        void onRefresh();
    }
}
