package com.ebizu.manis.mvp.reward.rewardlistcategory.rewardregular;

import android.os.Bundle;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.rewardrequesttype.RewardRequestTypeManager;
import com.ebizu.manis.mvp.reward.rewardlistcategory.RewardCategoryAbstractActivity;
import com.ebizu.manis.mvp.reward.rewardlistcategory.RewardListCategoryAbstractView;

/**
 * Created by FARIS_mac on 16/11/17.
 */

public class RewardRegularActivity extends RewardCategoryAbstractActivity {

    @Override
    public void initView() {
        super.initView();
        toolbarView.setTitle(rewardCategoryName != null ? rewardCategoryName : rewardSearchKeyword);
    }

    @Override
    public void initParameter() {
        RewardRequestTypeManager.getRequestType(this, rewardCategoryView, requestRewardType, rewardSearchKeyword, rewardCategoryId);
    }

    @Override
    public RewardListCategoryAbstractView getRewardCategoryAbstractView() {
        return rewardCategoryView;
    }


}
