package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.EndlessRecyclerOnScrollListener;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.ebizu.manis.model.rewardvoucher.Data;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart.PhysicalShoppingCartActivity;
import com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart.RegularShoppingCartActivity;
import com.ebizu.manis.mvp.reward.rewarddetail.rewardbulkdetail.RewardBulkDetailActivity;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.sdk.entities.MyVoucher;
import com.ebizu.manis.service.reward.requestbody.RewardMyVoucherBody;
import com.ebizu.manis.service.reward.response.WrapperRewardVoucherList;
import com.ebizu.manis.view.adapter.MyVoucherAdapter;
import com.ebizu.manis.view.dialog.redeemdialoginputvoucher.RedeemInputVoucherDialog;
import com.ebizu.manis.view.dialog.redeemdialogwithpin.RedeemWithPinTncDialog;
import com.ebizu.manis.view.holder.MyVoucherViewHolder;
import com.ebizu.sdk.reward.models.VoucherInput;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ebizu.manis.sdk.shared.ManisApp.context;

/**
 * Created by ebizu on 10/2/17.
 */

public class MyVouchersView extends BaseView
        implements IMyVouchersView, SwipeRefreshLayout.OnRefreshListener , MyVoucherViewHolder.MyVoucherListener
                    , RedeemInputVoucherDialog.RedeemInputListener{

    private IMyVouchersPresenter iMyVouchersPresenter;
    private MyVoucherAdapter myVoucherAdapter;
    private RewardMyVoucherBody.Data rewardMyVoucherData;
    private RewardMyVoucherBody rewardMyVoucherBody;
    private GridLayoutManager gridLayoutManager;
    RewardVoucher rewardVoucher = new RewardVoucher();

    private boolean isLoadMore = false;


    private boolean isLoading = false;
    public boolean isLastPage = false;
    public boolean isOffline = false;
    public int page = 1;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progress_bar_voucher)
    ProgressBar progressBarVoucher;

    @BindView(R.id.view_empty_my_voucher)
    View viewMyVouchersEmpty;

    @BindView(R.id.view_no_internet_connection)
    View viewNoInternetConnection;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private MyVouchersActivity myVouchersActivity;

    public MyVouchersView(Context context) {
        super(context);
        createView(context);
    }

    public MyVouchersView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public MyVouchersView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MyVouchersView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_my_vouchers, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        attachPresenter(new MyVouchersPresenter());
        myVouchersActivity = (MyVouchersActivity) baseActivity;
        initView();
        initListener();
        initMyVouchers();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iMyVouchersPresenter = (IMyVouchersPresenter) iBaseViewPresenter;
        iMyVouchersPresenter.attachView(this);
    }

    @Override
    public void showMyVouchersView(WrapperRewardVoucherList wrapperMyVoucher, LoadType loadType) {
        dismissEmptyPurchasedVouchers();
        isLoadMore = !wrapperMyVoucher.getData().getMore();
        loadMyVouchersView(wrapperMyVoucher.getData(), loadType);
    }

    @Override
    public IMyVouchersPresenter getMyVouchersPresenter() {
        return iMyVouchersPresenter;
    }

    @Override
    public void onRefresh() {
        page = ConfigManager.Reward.MY_VOUCHER_FIRST_PAGE;
        rewardMyVoucherData.setPage(page);
        rewardMyVoucherBody = new RewardMyVoucherBody(getContext(), rewardMyVoucherData);
        iMyVouchersPresenter.getMyVouchers(rewardMyVoucherBody, LoadType.SWIPE_LOAD, ConfigManager.Reward.MY_VOUCHER_CONDITION_AVAILABLE);
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void initView() {
        myVoucherAdapter = new MyVoucherAdapter(new ArrayList<MyVoucher>(),this,this);
        gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setAdapter(myVoucherAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.base_pink));

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                page++;
                rewardMyVoucherBody.getData().setPage(page);
                iMyVouchersPresenter.getMyVouchers(rewardMyVoucherBody, LoadType.SCROLL_LOAD,ConfigManager.Reward.MY_VOUCHER_CONDITION_AVAILABLE);
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (myVoucherAdapter.getItemViewType(position)) {
                    case 0:
                        return 1;
                    case 1:
                        return 2;
                    default:
                        return -1;
                }
            }
        });
    }

    private void initListener() {
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void initMyVouchers() {
        rewardMyVoucherData = new RewardMyVoucherBody.Data();
        rewardMyVoucherData.setOrder(ConfigManager.Reward.MY_VOUCHER_ORDER);
        rewardMyVoucherData.setSize(ConfigManager.Reward.MY_VOUCHER_MAX_PAGE_SIZE);
        rewardMyVoucherData.setPage(ConfigManager.Reward.MY_VOUCHER_FIRST_PAGE);
        rewardMyVoucherData.setCondtion(ConfigManager.Reward.MY_VOUCHER_CONDITION_AVAILABLE);

        rewardMyVoucherBody = new RewardMyVoucherBody(getContext(), rewardMyVoucherData);
        iMyVouchersPresenter.getMyVouchers(rewardMyVoucherBody, LoadType.CLICK_LOAD, ConfigManager.Reward.MY_VOUCHER_CONDITION_AVAILABLE);
    }

    private void loadMyVouchersView(Data dataResult, LoadType loadType) {
        isLastPage = !dataResult.getMore();
        switch (loadType) {
            case CLICK_LOAD: {
                myVoucherAdapter.replacePurchasedVouchers(dataResult.getRewardVoucherList());
                break;
            }
            case SCROLL_LOAD: {
                myVoucherAdapter.addAll(dataResult.getRewardVoucherList());
                break;
            }
            case SWIPE_LOAD: {
                myVoucherAdapter.replacePurchasedVouchers(dataResult.getRewardVoucherList());
                break;
            }
        }
        progressBarVoucher.setVisibility(GONE);
        viewNoInternetConnection.setVisibility(GONE);
        recyclerView.setVisibility(View.VISIBLE);
        if (myVoucherAdapter.isEmpty())
            showEmptyPurchasedVouchers();
    }

    @Override
    public void loadMyVouchersViewFail(LoadType loadType) {
        switch (loadType) {
            case CLICK_LOAD: {
                checkConnection(loadType);
                break;
            }
            case SCROLL_LOAD: {
                checkConnection(loadType);
                break;
            }
            case SWIPE_LOAD: {
                checkConnection(loadType);
                break;
            }
        }
    }


    @Override
    public void invisibleListVoucherView() {
        recyclerView.setVisibility(INVISIBLE);
    }

    @Override
    public void visibleListVoucherView() {
            recyclerView.setVisibility(VISIBLE);
    }

    @Override
    public void showProgressBarCircle() {
        progressBarVoucher.setVisibility(VISIBLE);
    }

    @Override
    public void dismissProgressBarCircle() {
        progressBarVoucher.setVisibility(INVISIBLE);
    }

    @Override
    public void startActivityShoppingCart(ShoppingCart shoppingCart, RewardVoucher reward) {
        // TODO: 17/11/17 implement manager and make sure again about conditions
        if (null != myVouchersActivity && null != reward) {
            if (getBaseActivity() instanceof RewardBulkDetailActivity) {
                if (reward.getBulkType().equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_ECODE) && reward.getVoucherRedeemType().equalsIgnoreCase(ConfigManager.Reward.REDEEM_TYPE_PIN)) {
                    myVouchersActivity.nextPurchaseVoucher(RegularShoppingCartActivity.class, reward, shoppingCart);
                } else if (reward.getBulkType().equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_PHYSICAL)) {
                    myVouchersActivity.nextPurchaseVoucher(PhysicalShoppingCartActivity.class, reward, shoppingCart);
                } else {
                    getBaseActivity().showAlertDialog(
                            getContext().getString(R.string.error),
                            "Please Update Your App",
                            true,
                            getContext().getString(R.string.text_ok), ((dialog, which) -> dialog.dismiss()));
                }
            } else {
                if ((reward.getType().equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_ECODE) && reward.getVoucherRedeemType().equalsIgnoreCase(ConfigManager.Reward.REDEEM_TYPE_PIN)) ||
                        (reward.getType().equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_ETU))) {
                    myVouchersActivity.nextPurchaseVoucher(RegularShoppingCartActivity.class, reward, shoppingCart);
                } else if (reward.getType().equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_PHYSICAL)) {
                    myVouchersActivity.nextPurchaseVoucher(PhysicalShoppingCartActivity.class, reward, shoppingCart);
                } else {
                    getBaseActivity().showAlertDialog(
                            getContext().getString(R.string.error),
                            "Please Update Your App",
                            true,
                            getContext().getString(R.string.text_ok), ((dialog, which) -> dialog.dismiss()));
                }
            }
        }
    }

    private void checkConnection(LoadType loadType) {
        if (!UtilManis.isNetworkConnected(getContext()) && !isOffline) {
            switch (loadType) {
                case CLICK_LOAD: {
                    viewNoInternetConnection.setVisibility(View.VISIBLE);
                    break;
                }
                case SCROLL_LOAD: {
                    Toast.makeText(getContext(), R.string.error_connection, Toast.LENGTH_SHORT).show();
                    break;
                }
                case SWIPE_LOAD: {
                    if (myVoucherAdapter.isEmpty()) {
                        viewNoInternetConnection.setVisibility(View.VISIBLE);
                        viewMyVouchersEmpty.setVisibility(GONE);
                    } else {
                        Toast.makeText(getContext(), R.string.error_connection, Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }
            isOffline = true;
        } else {
            switch (loadType) {
                case CLICK_LOAD: {
                    viewMyVouchersEmpty.setVisibility(View.VISIBLE);
                    break;
                }
                case SCROLL_LOAD: {
                    break;
                }
                case SWIPE_LOAD: {
                    if (myVoucherAdapter.isEmpty()) {
                        viewMyVouchersEmpty.setVisibility(View.VISIBLE);
                    } else {
                    }
                    break;
                }
            }
        }
    }

    private void showEmptyPurchasedVouchers() {
        if (!viewMyVouchersEmpty.isShown()) {
            recyclerView.setVisibility(View.GONE);
            viewNoInternetConnection.setVisibility(GONE);
            viewMyVouchersEmpty.setVisibility(View.VISIBLE);
        }
    }

    private void dismissEmptyPurchasedVouchers() {
        if (viewMyVouchersEmpty.isShown()) {
            recyclerView.setVisibility(View.GONE);
            viewMyVouchersEmpty.setVisibility(GONE);
            viewMyVouchersEmpty.setVisibility(View.GONE);
        }
    }

    public void setPage(int page){
        this.page = page;
    }

    public MyVoucherAdapter getMyVoucherAdapter() {
        return myVoucherAdapter;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    @Override
    public void onMyVoucherListener(RewardVoucher rewardVoucher) {
        if(rewardVoucher.getType() != null){
            if(rewardVoucher.getType().equals(ConfigManager.MyVoucherAvailable.REWARD_TYPE_ECODE) && rewardVoucher.getVoucherRedeemType().equals(ConfigManager.MyVoucherAvailable.REWARD_TYPE_PIN)) {
                this.getBaseActivity().dismissProgressBarDialog();

                RedeemWithPinTncDialog redeemWithPinTncDialog = new RedeemWithPinTncDialog(getContext());

                redeemWithPinTncDialog.setActivity(this.getBaseActivity());
                redeemWithPinTncDialog.setReward(rewardVoucher);
                redeemWithPinTncDialog.setRefCode(rewardVoucher.getTransactionId());
                redeemWithPinTncDialog.show();
            } else if (rewardVoucher.getType().equals(ConfigManager.MyVoucherAvailable.REWARD_TYPE_ETU)) {
                RedeemInputVoucherDialog redeemInputVoucherDialog = new RedeemInputVoucherDialog(getContext());

                redeemInputVoucherDialog.setReward(rewardVoucher, false);
                redeemInputVoucherDialog.setRedeemInputListener(this);
                redeemInputVoucherDialog.setActivity(this.getBaseActivity());
                redeemInputVoucherDialog.show();
            } else{
                this.getBaseActivity().showAlertDialog(getContext().getString(R.string.error_invalid), getContext().getString(R.string.error_external_apk), false, R.drawable.ic_launcher,
                        getContext().getString(R.string.dr_btn_continue), (dialog, which) -> {
                            try {
                                this.getBaseActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getContext().getString(R.string.market_id) + BuildConfig.APPLICATION_ID)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                this.getBaseActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getContext().getString(R.string.playstore_id) + BuildConfig.APPLICATION_ID)));
                            }
                            this.getBaseActivity().finish();
                        },
                        getContext().getString(R.string.dr_btn_close), (dialog, which) -> this.getBaseActivity().finish());
            }
        }
    }

    @Override
    public void finish(List<VoucherInput> result, boolean isPurchaseable) {
        this.getMyVouchersPresenter().setVoucherInputList(result);
//        this.getMyVouchersPresenter().getShoppingCart(rewardVoucher,1);
    }
}
