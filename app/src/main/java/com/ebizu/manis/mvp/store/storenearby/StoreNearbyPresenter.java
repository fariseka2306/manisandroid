package com.ebizu.manis.mvp.store.storenearby;

import android.util.SparseArray;
import android.view.View;

import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.StoreBody;
import com.ebizu.manis.service.manis.requestbody.StoreSearchBody;
import com.ebizu.manis.service.manis.response.WrapperSearchStore;
import com.ebizu.manis.service.manis.response.WrapperStore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ebizu on 9/11/17.
 */

public class StoreNearbyPresenter extends BaseViewPresenter implements IStoreNearbyPresenter {

    private StoreNearbyView storeNearbyView;
    private SparseArray<Subscription> subsStoreNearby;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        storeNearbyView = (StoreNearbyView) baseView;
        subsStoreNearby = new SparseArray<>();
    }

    @Override
    public void releaseSubscribe(int key) {
        Subscription subscription = subsStoreNearby.get(key);
        if (subscription != null) {
            subscription.unsubscribe();
            subsStoreNearby.remove(key);
        }
    }

    @Override
    public void releaseAllSubscribes() {
        storeNearbyView.unSubscribeAll(subsStoreNearby);
    }

    @Override
    public void loadNearStores(StoreBody storeBody, IBaseView.LoadType loadType) {
        setGpsView();
        if (!isGpsEnable()) return;
        initializeLoad(loadType);
        releaseSubscribe(0);
        Subscription subsLoadNearStore = getObserveStoreNearby(storeBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperStore>(storeNearbyView) {
                               @Override
                               public void onNext(WrapperStore wrapperStore) {
                                   super.onNext(wrapperStore);
                                   dismissTypeProgress(loadType);
                                   storeNearbyView.visibleListStoreView();
                                   storeNearbyView.addViewStores(wrapperStore.getStoreResults(), loadType);
                               }

                               @Override
                               public void onError(Throwable e) {
                                   super.onError(e);
                                   dismissTypeProgress(loadType);
                                   storeNearbyView.failLoadViewStores(loadType, e);
                               }
                           }
                );
        subsStoreNearby.put(0, subsLoadNearStore);
    }

    @Override
    public void loadSearchStore(StoreSearchBody storeSearchBody, IBaseView.LoadType loadType) {
        initializeLoad(loadType);
        releaseSubscribe(1);
        Subscription subsLoadSearch = getObservableSearchStore(storeSearchBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperSearchStore>(storeNearbyView) {
                    @Override
                    public void onNext(WrapperSearchStore wrapperSearchStore) {
                        super.onNext(wrapperSearchStore);
                        dismissTypeProgress(loadType);
                        storeNearbyView.visibleListStoreView();
                        storeNearbyView.addViewStores(wrapperSearchStore.getStoreResults(), loadType);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        dismissTypeProgress(loadType);
                        storeNearbyView.failLoadViewStores(loadType, e);
                    }
                });
        subsStoreNearby.put(1, subsLoadSearch);
    }

    private Observable<WrapperStore> getObserveStoreNearby(StoreBody storeBody) {
        return ManisApiGenerator.createServiceWithToken(storeNearbyView.getContext()).getStoreNearby(new Gson().toJson(storeBody));
    }

    private Observable<WrapperSearchStore> getObservableSearchStore(StoreSearchBody storeSearchBody) {
        String storeBody = new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(storeSearchBody);
        return ManisApiGenerator.createServiceWithToken(storeNearbyView.getContext()).getSearchStore(storeBody);
    }

    private void initializeLoad(IBaseView.LoadType loadType) {
        dismissAllProgress();
        showTypeProgress(loadType);
    }

    private void showTypeProgress(IBaseView.LoadType loadType) {
        storeNearbyView.setLoading(true);
        switch (loadType) {
            case CLICK_LOAD: {
                storeNearbyView.invisibleListStoreView();
                storeNearbyView.showProgressBarCircle();
                break;
            }
            case SCROLL_LOAD: {
                storeNearbyView.getStoreAdapter().addLoadingFooter();
                break;
            }
            case SEARCH_LOAD: {
                storeNearbyView.invisibleListStoreView();
                storeNearbyView.searchProgresView.setVisibility(View.VISIBLE);
                break;
            }
            case SWIPE_LOAD: {
                break;
            }
        }
    }

    private void dismissTypeProgress(IBaseView.LoadType loadType) {
        storeNearbyView.setLoading(false);
        switch (loadType) {
            case CLICK_LOAD: {
                storeNearbyView.dismissProgressBarCircle();
                break;
            }
            case SCROLL_LOAD: {
                storeNearbyView.getStoreAdapter().removeLoadingFooter();
                break;
            }
            case SEARCH_LOAD: {
                storeNearbyView.searchProgresView.setVisibility(View.INVISIBLE);
                break;
            }
            case SWIPE_LOAD: {
                storeNearbyView.swipeRefreshLayout.setRefreshing(false);
                break;
            }
        }
    }

    private void dismissAllProgress() {
        storeNearbyView.noDataStoreFoundView.setVisibility(View.GONE);
        storeNearbyView.progressBarCircle.setVisibility(View.GONE);
        storeNearbyView.searchProgresView.setVisibility(View.GONE);
        storeNearbyView.viewInactiveLocation.setVisibility(View.GONE);
        storeNearbyView.noInternetConnectionView.setVisibility(View.GONE);
        storeNearbyView.textViewMinChar.setVisibility(View.GONE);
        if (storeNearbyView.swipeRefreshLayout.isRefreshing())
            storeNearbyView.swipeRefreshLayout.setRefreshing(false);
    }

    private void setGpsView() {
        if (!isGpsEnable()) {
            storeNearbyView.viewInactiveLocation.setVisibility(View.VISIBLE);
            storeNearbyView.rvStore.setVisibility(View.GONE);
            storeNearbyView.noInternetConnectionView.setVisibility(View.GONE);
            return;
        } else {
            storeNearbyView.viewInactiveLocation.setVisibility(View.GONE);
        }
    }
}