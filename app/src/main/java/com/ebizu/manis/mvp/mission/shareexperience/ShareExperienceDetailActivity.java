package com.ebizu.manis.mvp.mission.shareexperience;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.tracker.TrackerConstant;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.mvp.mission.MissionDetailActivity;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperiencePostRB;
import com.ebizu.manis.service.manis.requestbodyv2.data.ShareExperiencePostData;
import com.ebizu.manis.service.manis.twitter.TwitterBroadcastReceiver;

import javax.inject.Inject;

/**
 * Created by FARIS_mac on 10/11/17.
 */

public class ShareExperienceDetailActivity extends MissionDetailActivity {

    private String TAG = getClass().getSimpleName();

    @Inject
    Context context;
    @Inject
    ShareExperienceSocialMediaPresenter shareExperienceSocialMediaPresenter;

    private TwitterBroadcastReceiver twitterBroadcastReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shareExperienceSocialMediaPresenter.attachView(this);
        attachPresenter(shareExperienceSocialMediaPresenter);
        setButtonText(getString(R.string.button_share));
        if (mission.getShareExperienceTypeId() == 2)
            initTwitterReceiver();
    }

    @Override
    protected void onDestroy() {
        if (twitterBroadcastReceiver != null)
            unregisterReceiver(twitterBroadcastReceiver);
        super.onDestroy();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent())
                .build();
        activityComponent.inject(this);
    }

    private void initTwitterReceiver() {
        twitterBroadcastReceiver = new TwitterBroadcastReceiver();
        twitterBroadcastReceiver.setActivityHandler(this);
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("com.twitter.sdk.android.tweetcomposer.UPLOAD_SUCCESS");
        mIntentFilter.addAction("com.twitter.sdk.android.tweetcomposer.UPLOAD_FAILURE");
        mIntentFilter.addAction("com.twitter.sdk.android.tweetcomposer.TWEET_COMPOSE_CANCEL");
        registerReceiver(twitterBroadcastReceiver, mIntentFilter);
    }

    @Override
    public void executeMission() {
        super.executeMission();
        TrackerManager.getInstance().setTrackerData(new TrackerStandartRequest(TrackerConstant.kTrackerOriginPageMissions, TrackerConstant.kTrackerPageImagePicker, TrackerConstant.kTrackerComponentButton, TrackerConstant.kTrackerSubFeatureExecuteMission, mission.getMissionId().toString(), mission.getMissionName()));
        switch (mission.getShareExperienceTypeId()) {
            case 1: { //for facebook
                shareFacebookMission();
                break;
            }
            case 2: { //for twitter
                shareExperienceSocialMediaPresenter.loginTwitter();
                break;
            }
        }
    }

    private void shareFacebookMission() {
        String packageName = "com.facebook.katana";
        Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent != null) {
            shareExperienceSocialMediaPresenter.getFriendsCount();
        } else {
            showAlertDialog(getString(R.string.error),
                    getString(R.string.mission_facebook_must_install_facebook_app_first),
                    true,
                    R.drawable.manis_logo,
                    getString(R.string.ok),
                    (dialog, which) -> {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.facebook_market_id))));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.facebook_playstore_id))));
                        }
                    });
        }
    }

    public void showDialogCantExecute() {
        String dialogMessage = String.format(getString(mission.getShareExperienceTypeId() == 1 ?
                R.string.mission_facebook_insufficient_friends_message :
                R.string.mission_twitter_insufficient_follower_message), mission.getShareExperinceMinimumFriends());
        showAlertDialog(getString(R.string.error),
                dialogMessage,
                false,
                R.drawable.manis_logo,
                getString(R.string.ok),
                (dialog, which) -> {
                });
    }

    public void showFailureDialog() {
        showAlertDialog(getString(R.string.error),
                getString(R.string.mission_social_general_error_message),
                false,
                R.drawable.manis_logo,
                getString(R.string.ok),
                (dialog, which) -> {
                });
    }

    public void postShareExperience() {
        ShareExperienceExecutePresenter missionExecutePresenter = new ShareExperienceExecutePresenter(this);
        ShareExperiencePostData data = new ShareExperiencePostData(this, mission.getMissionId());
        ShareExperiencePostRB requestBody = new ShareExperiencePostRB();
        requestBody.setData(data);
        missionExecutePresenter.executeShareExp(this, requestBody);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ConfigManager.UpdateProfile.CAMERA) {
            if (shareExperienceSocialMediaPresenter.mImageUri != null) {
                switch (mission.getShareExperienceTypeId()) {
                    case 1: {
                        shareExperienceSocialMediaPresenter.shareViaFB();
                        break;
                    }
                    case 2: {
                        shareExperienceSocialMediaPresenter.shareViaTwitter();
                        break;
                    }
                }
            }
        } else if (shareExperienceSocialMediaPresenter.getFacebookManager() != null && ConfigManager.FacebookParam.REQUEST_CODE == requestCode) {
            shareExperienceSocialMediaPresenter.getFacebookManager().getCallbackManagerFacebook().onActivityResult(requestCode, resultCode, data);
        } else if (shareExperienceSocialMediaPresenter.getTwitterManager() != null && ConfigManager.TwitterParam.REQUEST_CODE == requestCode) {
            shareExperienceSocialMediaPresenter.getTwitterManager().getTwitterAuthClient().onActivityResult(requestCode, resultCode, data);
        } else if (shareExperienceSocialMediaPresenter.callbackManager != null) {
            shareExperienceSocialMediaPresenter.callbackManager.onActivityResult(requestCode, resultCode, data);
        } if (requestCode == ConfigManager.UpdateProfile.CAMERA) {
            TrackerManager.getInstance().setTrackerData(new TrackerStandartRequest(TrackerConstant.kTrackerOriginPageMissions, TrackerConstant.kTrackerPageImagePicker, TrackerConstant.kTrackerComponentButton, TrackerConstant.kTrackerSubFeatureDismissView, "", ""));
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ConfigManager.Permission.CAMERA_REQUEST_CODE && getPermissionManager().hasPermissions(permissions)) {
            shareExperienceSocialMediaPresenter.openCamera();
        } else {
            Toast.makeText(this, "Some Permission is Denied", Toast.LENGTH_SHORT).show();
        }
    }
}
