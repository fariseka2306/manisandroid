package com.ebizu.manis.mvp.reward;

import com.ebizu.manis.root.IBaseView;

import java.util.List;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public interface IRewardView extends IBaseView {

    void loadImageCategory(List<Integer> imageCategories, List<String> categoriesName);

    void loadRewardCategory(String categoryId);

    IRewardPresenter getRewardPresenter();
}
