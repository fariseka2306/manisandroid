package com.ebizu.manis.mvp.snap.store.list;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.snap.store.list.recent.RecentStorePresenter;
import com.ebizu.manis.mvp.snap.store.list.recent.RecentStoreView;
import com.ebizu.manis.mvp.snap.store.list.suggested.SuggestedPresenter;
import com.ebizu.manis.mvp.snap.store.list.suggested.SuggestedView;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.view.manis.swiperefresh.SwipeRefreshManis;
import com.ebizu.manis.view.nestedscroll.NestedScrollChangeListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public class ListHistorySnapStoreView extends BaseView {

    @BindView(R.id.swipe_refresh_manis)
    SwipeRefreshManis swipeRefresh;

    private SuggestedView suggestedView;
    private RecentStoreView recentStoreView;

    public ListHistorySnapStoreView(Context context) {
        super(context);
        createView(context);
    }

    public ListHistorySnapStoreView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ListHistorySnapStoreView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ListHistorySnapStoreView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_history_snap_store, null, false);
        addView(view, new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT
        ));
        ButterKnife.bind(this, view);
        addSwipeRefresh();
        addSwipeRefreshListener();
    }

    private void addSwipeRefresh() {
        suggestedView = new SuggestedView(getContext());
        suggestedView.attachPresenter(new SuggestedPresenter());
        recentStoreView = new RecentStoreView(getContext());
        recentStoreView.attachPresenter(new RecentStorePresenter());
        swipeRefresh.addViewToSwipe(suggestedView,
                new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
        swipeRefresh.addViewToSwipe(recentStoreView,
                new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
    }

    private void addSwipeRefreshListener() {
        recentStoreView.setOnRefreshListener(() -> swipeRefresh.dismissSwipe());
        swipeRefresh.setOnRefreshListener(() -> {
            if (!suggestedView.isLoading() && !recentStoreView.isLoading()) {
                suggestedView.setLoading(true);
                recentStoreView.setLoading(true);
                suggestedView.setPage(1);
                recentStoreView.setPage(1);
                suggestedView.getSuggestedPresenter().getSnapStoresSuggest(
                        suggestedView.getPage(), IBaseView.LoadType.SWIPE_LOAD);
                recentStoreView.getRecentStorePresenter().getSnapStoresRecent(
                        recentStoreView.getPage(), IBaseView.LoadType.SWIPE_LOAD);
                getAnalyticManager().trackEvent(
                        ConfigManager.Analytic.Category.FRAGMENT_SNAP_RECENT,
                        ConfigManager.Analytic.Action.REFRESH,
                        "Swipe Refresh"
                );
            }
        });
    }

    private void addNestedScrollChange() {
        LinearLayoutManager linearLayoutManager = recentStoreView.getSnapStoreRecyclerView().getLinearLayoutManager();
        RecyclerView recyclerView = recentStoreView.getSnapStoreRecyclerView();
        swipeRefresh.getNestedScrollView().setOnScrollChangeListener(
                new NestedScrollChangeListener(linearLayoutManager, recyclerView, swipeRefresh) {

                    @Override
                    protected void onLoadMore() {
                        recentStoreView.setPage(recentStoreView.getPage() + 1);
                        recentStoreView.getRecentStorePresenter().getSnapStoresRecent(
                                recentStoreView.getPage(), LoadType.SCROLL_LOAD);
                    }

                    @Override
                    protected boolean isLoading() {
                        return recentStoreView.isLoading();
                    }

                    @Override
                    protected boolean isLastPage() {
                        return recentStoreView.isLastPage();
                    }
                });
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        suggestedView.setActivity(baseActivity);
        recentStoreView.setActivity(baseActivity);
        addNestedScrollChange();
    }

    public void setOnCheckSnapAbleListener(IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener) {
        suggestedView.setOnSnapAbleListener(onCheckSnapAbleListener);
        recentStoreView.setOnSnapAbleListener(onCheckSnapAbleListener);
    }

    public void loadAllPresenter() {
        suggestedView.getSuggestedPresenter().getSnapStoresSuggest(
                1, IBaseView.LoadType.CLICK_LOAD);
        recentStoreView.getRecentStorePresenter().getSnapStoresRecent(
                1, IBaseView.LoadType.CLICK_LOAD);
    }

    public SuggestedView getSuggestedView() {
        return suggestedView;
    }

    public RecentStoreView getRecentStoreView() {
        return recentStoreView;
    }
}
