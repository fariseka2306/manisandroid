package com.ebizu.manis.mvp.snap.receipt.camera;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseActivityPresenter;

import java.util.ArrayList;
import java.util.Random;

import javax.inject.Inject;

/**
 * Created by andrifashbir on 13/12/17.
 */

public class CameraActivityPresenter extends BaseActivityPresenter implements ICameraActivityPresenter {
    private CameraActivity cameraActivity;

    @Inject
    public CameraActivityPresenter() {
    }

    @Override
    public void attachView(BaseActivity view) {
        super.attachView(view);
        cameraActivity = (CameraActivity) view;
    }

    @Override
    public void releaseSubscribe(int key) {
        // not implemented method
    }

    @Override
    public void releaseAllSubscribe() {
        // not implemented method
    }

    @Override
    public void processData(byte[] data) {
        Bitmap bitmap = ImageUtils.convertBitmap(data);
        Random rn = new Random();
        cameraActivity.bitmapId = rn.nextInt(1000);

        if (bitmap != null) {
            Bitmap croppedBitmap = bitmap;
            if (cameraActivity.layoutRecent.getVisibility() == View.VISIBLE) {
                String manufacturer = Build.MANUFACTURER;
                int hiddenHeight = manufacturer.equalsIgnoreCase("Xiaomi") ?
                        (int) (cameraActivity.croppedHeight - cameraActivity.getResources().getDimension(R.dimen.four_dp)) :
                        cameraActivity.croppedHeight;
                croppedBitmap = Bitmap.createBitmap(bitmap, 0, hiddenHeight, bitmap.getWidth(),
                        bitmap.getHeight() - hiddenHeight);
            }
            croppedBitmap = Bitmap.createBitmap(croppedBitmap, 0, 0,
                    croppedBitmap.getWidth(), croppedBitmap.getHeight() - getPixel(cameraActivity.snapController.getMeasuredHeight()));

            cameraActivity.bitmapsList.add(croppedBitmap);
            stitchImage();
            cameraActivity.nextActivity(ConfirmActivity.class, cameraActivity.bitmapId, cameraActivity.receiptDataBody, cameraActivity.bitmapsList.size());
        }
    }

    private int getPixel(int value) {
        return (int) (value / Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    public void stitchImage() {
        ArrayList<Bitmap> bitmapsList = cameraActivity.bitmapsList;
        int width = bitmapsList.get(0).getWidth();
        int height = 0;

        for (Bitmap bitmap : bitmapsList) {
            height += bitmap.getHeight();
        }

        Bitmap finalBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(finalBitmap);
        float tempHeight = 0;

        for (int i = 0; i < bitmapsList.size(); i++) {
            Bitmap bitmap = bitmapsList.get(i);
            canvas.drawBitmap(bitmap, 0f, tempHeight, null);
            tempHeight += bitmap.getHeight();
        }

        saveImageFile(finalBitmap);
        ImageUtils.compressImage(cameraActivity.uri.getPath(), 1536, 2048);
    }

    private void saveImageFile(Bitmap savedBitmap) {
        ImageUtils.createDirectoryAndSaveFile(savedBitmap,
                ConfigManager.Snap.URI_RECEIPT_FILE_BITMAP.concat(String.valueOf(cameraActivity.bitmapId)));
        cameraActivity.uri = ImageUtils.getUri(cameraActivity.bitmapId);
    }
}
