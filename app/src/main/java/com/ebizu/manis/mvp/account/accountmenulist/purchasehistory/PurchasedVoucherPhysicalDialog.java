package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.model.PurchaseHistory;
import com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistorypyshicalfactory.PurchasePyshicalFactory;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by halim_ebizu on 11/16/17.
 */

public class PurchasedVoucherPhysicalDialog extends BaseDialogManis {

    @BindView(R.id.image_view_merchant)
    ImageView imageViewMerchant;

    @BindView(R.id.text_view_merchant_name)
    TextView textViewMerchantName;

    @BindView(R.id.text_view_merchant_info)
    TextView textViewMerchantInfo;

    @BindView(R.id.text_view_date)
    TextView textViewDate;

    @BindView(R.id.text_view_status)
    TextView textViewStatus;

    @BindView(R.id.image_view_status)
    ImageView imageViewStatus;

    @BindView(R.id.text_view_instruction)
    TextView textViewInstruction;

    @BindView(R.id.textview_instruction_bottom)
    TextView textviewInstructionBottom;

    public PurchasedVoucherPhysicalDialog(@NonNull Context context) {
        super(context);
        createDialog(context);
    }

    public PurchasedVoucherPhysicalDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        createDialog(context);
    }

    protected PurchasedVoucherPhysicalDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        createDialog(context);
    }

    private void createDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_purchase_history_pyshical, null, false);
        ButterKnife.bind(this, view);
        getParent().addView(view, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @OnClick(R.id.button_close)
    public void onButtonCloseClicked() {
        dismiss();
    }

    public void setPurchasedVoucherPhysicalDialogView(PurchaseHistory purchaseHistory) {
        PurchasePyshicalFactory purchasePyshicalFactory = new PurchasePyshicalFactory(purchaseHistory, getContext());
        Glide.with(getContext())
                .load(purchaseHistory.getVoucherIcon())
                .thumbnail(0.1f)
                .fitCenter()
                .animate(R.anim.fade_in_image)
                .placeholder(R.drawable.default_pic_promo_details_pic_large)
                .into(imageViewMerchant);
        textViewMerchantInfo.setText(purchaseHistory.getVoucherName());
        textViewMerchantName.setText(purchaseHistory.getPurchaseHistoryBrand().getName());
        textViewDate.setText(purchaseHistory.getTransactionTime());
        textViewStatus.setText(purchasePyshicalFactory.getStatus());
        imageViewStatus.setImageDrawable(purchasePyshicalFactory.imageIcon());
        textViewInstruction.setText(purchasePyshicalFactory.getInstruction());
        if (purchasePyshicalFactory.getInstructionVisibility() == View.GONE) {
            textviewInstructionBottom.setVisibility(View.GONE);
        } else {
            textviewInstructionBottom.setVisibility(View.VISIBLE);
        }

    }
}
