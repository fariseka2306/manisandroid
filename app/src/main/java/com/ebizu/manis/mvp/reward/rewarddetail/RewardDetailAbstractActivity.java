package com.ebizu.manis.mvp.reward.rewarddetail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.vouchertransactiontype.VoucherTransactionTypeManager;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.phoneloginfacebookotp.FacebookOtpLoginActivity;
import com.ebizu.manis.mvp.reward.purchasevoucher.PurchaseRewardActivity;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.service.manis.requestbody.RewardRedeemBody;
import com.ebizu.manis.view.dialog.redeemdialoginputvoucher.RedeemInputVoucherDialog;
import com.ebizu.manis.view.dialog.redeemdialogvoucher.RedeemVoucherDialog;
import com.ebizu.manis.view.manis.toolbar.ToolbarRewardView;
import com.ebizu.sdk.reward.models.Reward;
import com.ebizu.sdk.reward.models.VoucherInput;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by FARIS_mac on 15/11/17.
 */

public abstract class RewardDetailAbstractActivity extends PurchaseRewardActivity implements View.OnClickListener, RedeemInputVoucherDialog.RedeemInputListener {

    @BindView(R.id.toolbar)
    protected ToolbarRewardView toolbarRewardView;

    @BindView(R.id.rf_txt_redeem)
    protected TextView textViewRedeem;

    @BindView(R.id.rf_rl_buy)
    public RelativeLayout rfRlBuy;

    @BindView(R.id.rf_rl_redeem)
    public RelativeLayout rfRlRedeem;

    @BindView(R.id.reward_detail_view)
    protected RewardDetailView rewardDetailView;

    private RewardVoucher rewardVoucher;
    private RewardRedeemBody rewardRedeemBody;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_reward_detail);
        ButterKnife.bind(this);
        rewardDetailView.setActivity(this);
        getRewardData();
        initView();
    }

    protected void getRewardData() {
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            rewardVoucher = extra.getParcelable(ConfigManager.Reward.REWARD);
        }
    }

    protected void initView() {
        ManisSession manisSession = new ManisSession(this);
        int accountPoint = manisSession.getPointSession().getPoint();
        toolbarRewardView.setToolbarView(rewardVoucher);
        rewardDetailView.setReward(rewardVoucher);
        toolbarRewardView.setOnClickListener(this);
        showVoucherType();
        checkRedeemable(accountPoint);
        checkStock();
    }

    private void showVoucherType() {
        if (rewardVoucher.getStock() <= 0) {
            rfRlBuy.setVisibility(View.GONE);
            rfRlRedeem.setVisibility(View.VISIBLE);
        } else {
            setButtonVisibility();
        }
    }

    private void setButtonVisibility() {
        VoucherTransactionTypeManager.checkDetailVoucherTransactionType(rewardVoucher, this);
    }

    private void checkRedeemable(int accountPoint) {
        if (accountPoint >= rewardVoucher.getPoint()) {
            setRedeemable();
        } else {
            setNotRedeemable();
        }
    }

    public void setRedeemable() {
        textViewRedeem.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        textViewRedeem.setText(getString(R.string.rd_txt_redeem));
        textViewRedeem.setEnabled(true);
    }

    public void setNotRedeemable() {
        rfRlRedeem.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_btn_rounded_softgrey));
        textViewRedeem.setTextColor(ContextCompat.getColor(this, R.color.colorGray));
        textViewRedeem.setText(getString(R.string.rd_txt_not_redeemable));
        textViewRedeem.setEnabled(false);
        rfRlRedeem.setClickable(false);
    }

    private void checkStock() {
        if (rewardVoucher.getStock() < 1) {
            rfRlRedeem.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_btn_rounded_softgrey));
            textViewRedeem.setTextColor(ContextCompat.getColor(this, R.color.colorGray));
            rewardDetailView.txtOutOfStock.setVisibility(View.VISIBLE);
            textViewRedeem.setEnabled(false);
            textViewRedeem.setText(getString(R.string.txt_btn_temp_outofstock));
            rfRlRedeem.setClickable(false);
        }
    }

    @OnClick(R.id.rf_rl_redeem)
    public void onClickRedeem() {
        if (getManisSession().isOtpRegistered()) {
            onSuccessOtp();
        } else {
            showAlertDialog(getString(R.string.app_name),
                    getString(R.string.verify_otp_reward),
                    false,
                    R.drawable.manis_logo,
                    getString(R.string.text_ok),
                    (dialog, which) -> {
                        boolean permissionIsAllow = getPermissionManager().checkPermissionSMS();
                        if (permissionIsAllow) {
                            setValidationOTP();
                        }
                    });
        }
        initAnalyticTrack();
    }

    public void initAnalyticTrack() {
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.REWARD_POINT_DETAIL,
                ConfigManager.Analytic.Action.CLICK,
                "Button Redeem"
        );
    }

    @OnClick(R.id.rf_rl_buy)
    public void onCLickBuy() {
        rewardDetailView.getRewardDetailPresenter().getShoppingCart(rewardVoucher, 1);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.REWARD_POINT_DETAIL,
                ConfigManager.Analytic.Action.CLICK,
                "Button Buy Now"
        );
    }

    private void onSuccessOtp() {
        if (rewardVoucher.getInputs().isEmpty()) {
            redeemReward();
        } else {
            redeemInputVoucher(false);
        }
    }

    private void redeemReward() {
        if (rewardVoucher.isNeedConfirmationPage()) {
            RedeemVoucherDialog redeemVoucherDialog = new RedeemVoucherDialog(this);
            redeemVoucherDialog.setActivity(this);
            redeemVoucherDialog.setReward(rewardVoucher);
            redeemVoucherDialog.show();
        } else {
            rewardRedeemBody = new RewardRedeemBody();
            rewardRedeemBody.setId(rewardVoucher.getId());
            rewardRedeemBody.setPoint(rewardVoucher.getPoint());
            rewardRedeemBody.setVoucher_type(rewardVoucher.getType());
            rewardDetailView.getRewardDetailPresenter().loadReward(rewardRedeemBody);
        }
    }

    private void redeemInputVoucher(boolean isPurchaseable) {
        RedeemInputVoucherDialog redeemInputVoucherDialog = new RedeemInputVoucherDialog(this);
        redeemInputVoucherDialog.setReward(rewardVoucher, isPurchaseable);
        redeemInputVoucherDialog.setRedeemInputListener(this);
        redeemInputVoucherDialog.show();
        redeemInputVoucherDialog.setActivity(this);
    }

    public void setValidationOTP() {
        FacebookOtpLoginActivity.start(this, ConfigManager.Otp.FORCE_SIGN_UP_OTP);
    }

    @Override
    public void onClick(View v) {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.REWARD_POINT_DETAIL,
                ConfigManager.Analytic.Action.CLICK,
                "Button Back");
    }

    @Override
    public void finish(List<VoucherInput> result, boolean isPurchaseable) {
        rewardDetailView.getRewardDetailPresenter().setVoucherInputList(result);
        if (!isPurchaseable) {
            redeemReward();
        } else {
            rewardDetailView.getRewardDetailPresenter().getShoppingCart(rewardVoucher, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ConfigManager.Otp.FORCE_SIGN_UP_OTP && getPermissionManager().hasPermissions(permissions)) {
            showAlertDialog(getString(R.string.app_name),
                    getString(R.string.permission_must_digits),
                    false,
                    R.drawable.manis_logo,
                    getString(R.string.text_ok),
                    (dialog, which) -> {
                        setValidationOTP();
                    });
        }
    }

}
