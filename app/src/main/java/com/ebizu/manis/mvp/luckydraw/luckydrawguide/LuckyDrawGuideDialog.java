package com.ebizu.manis.mvp.luckydraw.luckydrawguide;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.Window;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApi;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Halim on 10-Aug-17.
 */
public class LuckyDrawGuideDialog extends BaseActivity {

    @Inject
    Context context;

    @Inject
    ManisApi manisApi;

    @BindView(R.id.lucky_draw_guide_view)
    LuckyDrawGuideView luckyDrawGuideView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_lucky_draw_guide);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            performBackAnimation();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @OnClick(R.id.ld_btn_dismiss)
    public void onLdBtnDismissClick() {
        SharedPreferences prefFirst = getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
        UtilSessionFirstIn.setShowLuckyDrawGuide(prefFirst, false);
        performBackAnimation();
    }

    @OnClick(R.id.ld_btn_ok)
    public void onLdBtnOkClick() {
        performBackAnimation();
    }

    private void initView() {

    }

    private void performBackAnimation() {
        finish();
        overridePendingTransition(R.anim.slide_out_top, R.anim.slide_in_bottom);
    }
}
