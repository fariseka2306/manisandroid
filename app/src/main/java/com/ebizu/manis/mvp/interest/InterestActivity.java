package com.ebizu.manis.mvp.interest;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public class InterestActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;
    @BindView(R.id.interest_view)
    InterestView interestView;

    @Inject
    ManisApi manisApi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideInHorizontalAnim();
        setContentView(R.layout.activity_interest);
        ButterKnife.bind(this);
        interestView.setActivity(this);
        interestView.attachPresenter(new InterestPresenter());
        setViewInterest();
        interestView.getInterestPresenter().getUserInterest(manisApi);
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent = DaggerActivityComponent
                .builder()
                .applicationComponent(getApplicationComponent())
                .build();
        activityComponent.inject(this);
    }

    private void setViewInterest() {
        toolbarView.setTitle(R.string.in_title);
        if (getManisSession().isFirstLogin()) {
            toolbarView.setVisibility(View.INVISIBLE);
            interestView.setTitleFisrtLogin();
            interestView.setOnSuccessSaveListener(() -> {
                getManisSession().setSession(ConfigManager.AccountSession.FIRST_LOGIN, false);
                new Handler().postDelayed(() -> {
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                }, 200);
            });
        } else {
            toolbarView.setVisibility(View.VISIBLE);
            interestView.enableSaveButton();
            interestView.setOnSuccessSaveListener(() -> {
                getManisSession().setSession(ConfigManager.AccountSession.FIRST_LOGIN, false);
                new Handler().postDelayed(this::finish, 200);
            });
        }
    }

    /**
     * No back pressed till completed save interest
     */
    @Override
    public void onBackPressed() {
        if (getManisSession().isFirstLogin()) {
            interestView.showNotificationMessage(
                    getResources().getString(R.string.first_login_interest_page_back_pressed_info));
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void finish() {
        super.finish();
        slideOutHorizontalAnim();
    }
}