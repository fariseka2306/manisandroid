package com.ebizu.manis.mvp.versioning;

import android.content.Context;

import com.ebizu.manis.model.Versioning;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.IBaseActivity;
import com.ebizu.manis.root.IBaseView;

import java.util.List;

/**
 * Created by mac on 8/22/17.
 */

public interface IVersioningView extends IBaseActivity{

    void showVersioning(Versioning versionings,Context context);

    void nextActivity();

}
