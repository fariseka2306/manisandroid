package com.ebizu.manis.mvp.home.snaphistory;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.mvp.home.brands.HomePresenter;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raden on 7/7/17.
 */

public class SnapPresenter extends BaseViewPresenter implements ISnapPresenter {

    private static final String TAG = HomePresenter.class.getSimpleName();

    private Context context;
    private ISnapView iSnapView;
    private Subscription subsSnap;

    public SnapPresenter(Context context) {
        this.context = context;
    }

    public void attachView(ISnapView iSnapView) {
        this.iSnapView = iSnapView;
    }

    @Override
    public void loadSnapData() {
        releaseSubscribe(0);
        ManisApi manisApi = ManisApiGenerator.createServiceWithToken(context);
        subsSnap = manisApi.getSnap().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        wrapperSnap -> {
                            iSnapView.loadViewSnap(wrapperSnap.getSnapDatas());
                            Log.d(TAG, "loadSnapDatahistory: " + wrapperSnap.toString());
                        },
                        throwable -> {
                            cekConnection(iSnapView);
                            Log.e(TAG, "loadSnapDatahistory: " + throwable.getMessage());
                        }
                );
    }

    public void cekConnection(ISnapView iSnapView) {
        if (!ConnectionDetector.isNetworkConnected(context)) {
            iSnapView.connectionErrorSnap();
        } else {
            iSnapView.noSnap("no snap");
        }
    }

    public void cekSnap(ISnapView iSnapView) {
        iSnapView.noSnap("no snap");
    }

    public void txtViewHistory(ISnapView iSnapView) {
        iSnapView.snapDone();
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsSnap != null) subsSnap.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }
}
