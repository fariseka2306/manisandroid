package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.statusmyvoucher.expiredstatus;

import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.service.reward.response.WrapperRewardVoucherList;

/**
 * Created by Raden on 11/14/17.
 */

public interface IExpiredStatusView extends IBaseView{

    void showVouchersExpirerdView(WrapperRewardVoucherList wrapperRewardVoucherList, LoadType loadType);

    void loadMyVouchersViewFail(LoadType loadType);

    void invisibleListVoucherView();

    void showProgressBarCircle();

    void dismissProgressBarCircle();
}
