package com.ebizu.manis.mvp.account.accountmenulist.faq;

import com.ebizu.manis.model.Faq;
import com.ebizu.manis.root.IBaseView;

import java.util.List;

/**
 * Created by abizu-alvio on 7/31/2017.
 */

public interface IFaqView extends IBaseView{

    void showFaqs(List<Faq> faqs);

    IFaqPresenter getFaqPresenter();

}
