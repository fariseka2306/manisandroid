package com.ebizu.manis.mvp.filtercategories;

/**
 * Created by Raden on 8/2/17.
 */

public interface IFilterCategoriesPresenter {

    void loadFilterCategories();
}
