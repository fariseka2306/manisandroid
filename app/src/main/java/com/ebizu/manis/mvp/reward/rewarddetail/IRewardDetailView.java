package com.ebizu.manis.mvp.reward.rewarddetail;

import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.sdk.reward.models.Reward;

/**
 * Created by Raden on 8/1/17.
 */

public interface IRewardDetailView extends IBaseView {
    void loadReward(Reward reward);

    IRewardDetailPresenter getRewardDetailPresenter();

    void startActivityShoppingCart(ShoppingCart shoppingCart);
}
