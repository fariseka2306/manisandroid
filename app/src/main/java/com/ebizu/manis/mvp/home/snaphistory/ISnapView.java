package com.ebizu.manis.mvp.home.snaphistory;

import com.ebizu.manis.model.snap.SnapData;

import java.util.ArrayList;

/**
 * Created by Raden on 7/7/17.
 */

public interface ISnapView {
    void loadViewSnap(ArrayList<SnapData> snapDatas);

    void connectionErrorSnap();

    void serverBusySnap(String message);

    void snapDone();

    void noSnap(String message);
}
