package com.ebizu.manis.mvp.mission.shareexperience;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.manager.facebook.FacebookManager;
import com.ebizu.manis.manager.tracker.TrackerConstant;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.manager.twitter.TwitterManager;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseActivityPresenter;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class ShareExperienceSocialMediaPresenter extends BaseActivityPresenter implements ISocialMedia {

    private static final float maxHeight = 1280.0f;
    private static final float maxWidth = 1280.0f;
    CallbackManager callbackManager;
    Uri mImageUri;
    private ShareExperienceDetailActivity iShareXDetailView;
    private FacebookManager facebookManager;
    private TwitterManager twitterManager;
    private String mImagePath;
    private String mHashTag;

    @Inject
    public ShareExperienceSocialMediaPresenter() {

    }

    @Override
    public void attachView(BaseActivity view) {
        super.attachView(view);
        iShareXDetailView = (ShareExperienceDetailActivity) view;
        callbackManager = CallbackManager.Factory.create();
        String hashTag = iShareXDetailView.mission.getHashTag();
        mHashTag = hashTag != null ? hashTag : "";
    }

    @Override
    public void releaseSubscribe(int key) {
        // not implemented method
    }

    @Override
    public void releaseAllSubscribe() {
        // not implemented method
    }


    @Override
    public void shareViaFB() {
        CustomImage customImage = new CustomImage(1);
        customImage.execute();
    }

    @Override
    public void takePhoto() {
        openCamera();
    }

    @Override
    public void shareViaTwitter() {
        CustomImage customImage = new CustomImage(2);
        customImage.execute();
    }

    @Override
    public void loginTwitter() {
        getTwitterManager().connectTwitter();
    }

    @Override
    public void getFriendsCount() {
        if (AccessToken.getCurrentAccessToken() != null) {
            takePhoto();
        } else {
            getFacebookManager().startLoginFacebook(true);
        }
    }

    @Override
    public void checkFollowerCount(int followersCount) {
        if (followersCount >= iShareXDetailView.mission.getShareExperinceMinimumFriends()) {
            openCamera();
        } else {
            iShareXDetailView.showDialogCantExecute();
        }
    }

    @Override
    public FacebookManager getFacebookManager() {
        if (facebookManager == null) {
            facebookManager = new FacebookManager(iShareXDetailView, this);
        }
        return facebookManager;
    }

    @Override
    public TwitterManager getTwitterManager() {
        if (twitterManager == null) {
            twitterManager = new TwitterManager(iShareXDetailView, this);
        }
        return twitterManager;
    }

    private void buildShareDialog() {
        SharePhoto photo = new SharePhoto.Builder()
                .setImageUrl(Uri.parse(mImagePath))
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .setShareHashtag(new ShareHashtag
                        .Builder()
                        .setHashtag(mHashTag)
                        .build())
                .build();

        ShareDialog shareDialog = new ShareDialog(iShareXDetailView);
        shareDialog.show(content);

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                iShareXDetailView.postShareExperience();
            }

            @Override
            public void onCancel() {
                iShareXDetailView.dismissProgressBarDialog();
                Log.d(this.getClass().getSimpleName(), "sharing cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                iShareXDetailView.dismissProgressBarDialog();
                iShareXDetailView.showFailureDialog();
                Toast.makeText(iShareXDetailView, "sharing error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void buildTweet() {
        final TwitterSession activeSession = TwitterCore.getInstance()
                .getSessionManager().getActiveSession();
        final Intent intent = new ComposerActivity.Builder(iShareXDetailView)
                .session(activeSession)
                .image(Uri.parse(mImagePath))
                .hashtags(mHashTag)
                .createIntent();
        iShareXDetailView.startActivity(intent);
    }

    void openCamera() {
        boolean permissionIsAllow = getBaseActivity().getPermissionManager().checkPermissionCamera();
        if (permissionIsAllow) {
            TrackerManager.getInstance().setTrackerData(new TrackerStandartRequest(TrackerConstant.kTrackerOriginPageMissions, TrackerConstant.kTrackerPageImagePicker, TrackerConstant.kTrackerComponentButton, TrackerConstant.kTrackerSubFeatureCapturePicture, "", ""));
            try {
                mImageUri = FileProvider.getUriForFile(iShareXDetailView, "com.ebizu.manis.provider", createImageFile());
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                List<ResolveInfo> resInfoList = iShareXDetailView.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    iShareXDetailView.grantUriPermission(packageName, mImageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                iShareXDetailView.startActivityForResult(intent, ConfigManager.UpdateProfile.CAMERA);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private File createImageFile() throws IOException {
        final String dir = Environment.getExternalStorageDirectory() + "/Manis/Missions";
        File newdir = new File(dir);
        if (!newdir.exists()) newdir.mkdirs();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = newdir + "/IMG_" + timeStamp + ".jpg";
        File image = new File(imageFileName);
        if (!image.exists()) image.createNewFile();
        mImagePath = "file:" + image.getAbsolutePath();
        return image;
    }

    private class CustomImage extends AsyncTask<Void, Void, Boolean> {

        private int type;

        CustomImage(int type) {
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            iShareXDetailView.showProgressBarDialog(iShareXDetailView.baseActivity().getString(R.string.please_wait), false);
        }

        protected void onPostExecute(Boolean result) {
            switch (type) {
                case 1:
                    buildShareDialog();
                    break;
                case 2:
                    buildTweet();
                    break;
            }
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                ImageUtils.addWaterMark(iShareXDetailView, mImagePath, mHashTag, Color.WHITE, 90, 120);
                File file = new File(Uri.parse(mImagePath).getPath());
                String filePath = file.getPath();
                ImageUtils.compressImage(filePath, maxWidth, maxHeight);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
    }
}
