package com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.purchasevoucher.MolPaymentResult;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.model.purchasevoucher.Purchase;
import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.purchasevoucher.PurchaseRewardActivity;
import com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary.amount.AmountView;
import com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary.orderdetails.OrderDetailsView;
import com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary.shippingdetails.ShippingDetailsView;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.view.dialog.redeemdialogwithpin.RedeemWithPinDialog;
import com.ebizu.manis.view.dialog.redeemdialogwithpin.RedeemWithPinTncDialog;
import com.ebizu.manis.view.dialog.transactionstatus.TransactionStatusDialog;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ebizu.manis.helper.ConfigManager.PurchaseVoucher.TRANS_FINISH_RESULT_CODE;

/**
 * Created by ebizu on 10/3/17.
 */

public class OrderSummaryView extends BaseView
        implements IOrderSummaryView {

    public TransactionStatusDialog transactionStatusDialog;
    @BindView(R.id.view_amount)
    AmountView amountView;
    @BindView(R.id.view_order_details)
    OrderDetailsView orderDetailsView;
    @BindView(R.id.view_shipping_details)
    ShippingDetailsView shippingDetailsView;
    private OrderSummaryPresenter orderSummaryPresenter;

    private RewardVoucher reward;
    private PurchaseRewardActivity purchaseRewardActivity;

    public OrderSummaryView(Context context) {
        super(context);
        createView(context);
    }

    public OrderSummaryView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public OrderSummaryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public OrderSummaryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_order_summary, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        purchaseRewardActivity = (PurchaseRewardActivity) baseActivity;
        transactionStatusDialog = new TransactionStatusDialog(getBaseActivity());
    }

    @Override
    public void setOrderSummaryView(ShoppingCart shoppingCart, Order order) {
        amountView.setView(order);
        orderDetailsView.setView(shoppingCart, order);
        shippingDetailsView.setView(order);
    }

    @Override
    public void setTransactionResult(TransactionResult transactionResult) {
        transactionStatusDialog.setTransactionValidation(transactionResult.getStatus());
    }

    @Override
    public void setTransactionResult(MolPaymentResult molPaymentResult) {
        transactionStatusDialog.setTransactionValidation(molPaymentResult.getStatus());
    }

    @Override
    public void setReward(RewardVoucher reward) {
        this.reward = reward;
    }

    public OrderSummaryPresenter getOrderSummaryPresenter() {
        if (orderSummaryPresenter == null) {
            orderSummaryPresenter = new OrderSummaryPresenter();
            orderSummaryPresenter.attachView(this);
        }
        return orderSummaryPresenter;
    }

    @Override
    @Nullable
    public void showDialogTransactionStatus(Purchase purchase) {
        transactionStatusDialog.setTransactionStatusView(reward, purchase);
        transactionStatusDialog.setOnActionListener(new TransactionStatusDialog.OnActionListener() {
            @Override
            public void onDismissDialog() {
                finishTransactionActivity();
            }

            @Override
            public void onSuccessTrans(Purchase purchase) {
                String type = reward.getType().isEmpty() ? reward.getBulkType() : reward.getType();
                if ((type.equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_PHYSICAL)) ||
                        (type.equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_ETU))) {
                    getBaseActivity().showAlertDialog(purchase.getStatusMessage(), false, "OK",
                            (dialogInterface, i) -> finishTransactionActivity(), null, null);
                } else if (type.equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_ECODE)
                        && reward.getVoucherRedeemType().equalsIgnoreCase(ConfigManager.Reward.REDEEM_TYPE_PIN)) {
                    showTnCDialog(purchase);
                }
            }

            @Override
            public void onPendingTrans(Purchase purchase) {
                String type = reward.getType().isEmpty() ? reward.getBulkType() : reward.getType();
                if ((type.equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_PHYSICAL)) ||
                        (type.equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_ETU))) {
                    getBaseActivity().showAlertDialog(purchase.getStatusMessage(), false, "OK",
                            (dialogInterface, i) -> finishTransactionActivity(), null, null);
                } else if (type.equalsIgnoreCase(ConfigManager.Reward.VOUCHER_TYPE_ECODE)
                        && reward.getVoucherRedeemType().equalsIgnoreCase(ConfigManager.Reward.REDEEM_TYPE_PIN)) {
                    finishTransactionActivity();
                }
            }
        });
        transactionStatusDialog.show();
    }

    @Override
    public void showDialogRedeemPin(String refCode) {
        RedeemWithPinDialog redeemWithPinDialog = new RedeemWithPinDialog(getBaseActivity());
        redeemWithPinDialog.setReward(reward);
        redeemWithPinDialog.setRefCode(refCode);
        redeemWithPinDialog.setOnCancelListener(dialogInterface -> finishTransactionActivity());
        redeemWithPinDialog.setOnDismissListener(dialogInterface -> finishTransactionActivity());
        redeemWithPinDialog.show();
    }

    private void finishTransactionActivity() {
        purchaseRewardActivity.setResult(TRANS_FINISH_RESULT_CODE);
        purchaseRewardActivity.finish();
    }

    private void showTnCDialog(Purchase purchase) {
        RedeemWithPinTncDialog redeemWithPinTncDialog = new RedeemWithPinTncDialog(getBaseActivity());
        redeemWithPinTncDialog.setActivity(getBaseActivity());
        redeemWithPinTncDialog.setReward(reward);
        redeemWithPinTncDialog.setPurchase(purchase);
        redeemWithPinTncDialog.setOnDismissListener(dialogInterface -> finishTransactionActivity());
        redeemWithPinTncDialog.setOnCancelListener(dialogInterface -> finishTransactionActivity());

        redeemWithPinTncDialog.show();
    }
}
