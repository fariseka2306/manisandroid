package com.ebizu.manis.mvp.versioning;

import com.ebizu.manis.root.IBaseActivityPresenter;

/**
 * Created by mac on 8/22/17.
 */

public interface IVersioningPresenter<View> extends IBaseActivityPresenter {

    void requestVersioning();

}
