package com.ebizu.manis.mvp.store.storedetaillocation;

import com.ebizu.manis.helper.GPSTracker;
import com.ebizu.manis.model.Coordinate;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by ebizu on 9/11/17.
 */

public interface IStoreLocationView extends IBaseView {

    void setStore(Store store);

    void setCoordinateLocation(GPSTracker gpsTracker, Coordinate coordinate);

}
