package com.ebizu.manis.mvp.store.storedetail;

import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.StoreDetail;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.service.manis.response.WrapperPin;
import com.ebizu.manis.service.manis.response.WrapperUnpin;

/**
 * Created by firef on 7/20/2017.
 */

public interface IStoreDetailView extends IBaseView {

    void loadStoreDetailData(Store store, StoreDetail storeDetail);

    void setFollowerData(WrapperPin wrapperPin);

    void setUnFollowerData(WrapperUnpin wrapperUnpin);

    IStoreDetailPresenter getStoreDetailPresenter();

    void startActivityReceiptDetail();

    void startActivityProfile();
}
