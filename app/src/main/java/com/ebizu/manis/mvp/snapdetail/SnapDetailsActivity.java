package com.ebizu.manis.mvp.snapdetail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Currency;
import com.ebizu.manis.model.snap.SnapData;
import com.ebizu.manis.model.snap.SnapDetailsData;
import com.ebizu.manis.mvp.snapdetail.viewimage.ViewImageSnap;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseActivity;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Raden on 7/17/17.
 */

public class SnapDetailsActivity extends BaseActivity implements ISnapDetailView {

    @BindView(R.id.relative_date_snap)
    RelativeLayout relativeDateSnap;
    @BindView(R.id.linear_snap_store)
    LinearLayout linearSnapStore;

    @BindView(R.id.img_merchant)
    ImageView imgMerchant;
    @BindView(R.id.img_upload)
    ImageView imgUpload;
    @BindView(R.id.txt_time_text)
    TextView txtTimeText;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_merchant)
    TextView txtMerchant;
    @BindView(R.id.txt_location)
    TextView txtLocation;
    @BindView(R.id.txt_currency)
    TextView txtCurrency;
    @BindView(R.id.txt_currency_number)
    TextView txtCurrencyNumber;
    @BindView(R.id.img_snap)
    ImageView imgSnap;
    @BindView(R.id.btn_viewimage)
    Button btnViewImage;
    @BindView(R.id.txt_status)
    TextView txtStatus;
    @BindView(R.id.img_status)
    ImageView imgStatus;

    @BindView(R.id.main_review)
    LinearLayout mainReview;
    @BindView(R.id.txt_review)
    TextView txtReview;
    @BindView(R.id.btn_appeal)
    Button btnAppeal;

    @BindView(R.id.lin_button)
    LinearLayout linButton;
    @BindView(R.id.btn_close)
    Button btnClose;

    private Context context;
    private SnapDetailPresenter snapDetailPresenter;
    private ISnapDetailView iSnapDetailView;
    private ArrayList<SnapDetailsData> detailsDatas = new ArrayList<>();
    private SnapData snapData;
    private Currency currency;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        snapData = getIntent().getParcelableExtra(ConfigManager.Snap.DATA);

        setContentView(R.layout.snap_details);
        context = this;
        ButterKnife.bind(this);

        snapDetailPresenter = new SnapDetailPresenter(context);
        snapDetailPresenter.attachSnapDetail(this);

        snapDetailPresenter.loadSnapDetails();

        currency = new ManisSession(this).getAccountSession().getCurrency();
        ManisSession manisSession = new ManisSession(this);

    }

    @Override
    public void loadViewSnapDetails(ArrayList<SnapData> snapDetailsDatas) {
        txtMerchant.setVisibility(View.VISIBLE);
        txtMerchant.setText(UtilManis.getStoreName(snapData.getStore().getName()));
        txtLocation.setVisibility(View.VISIBLE);
        txtLocation.setText(UtilManis.getStoreLocation(snapData.getStore().getName(), snapData.getStore().getAddress()));
        Glide.with(context)
                .load(snapData.getReceipt().getImage())
                .asBitmap()
                .thumbnail(0.1f)
                .fitCenter()
                .placeholder(R.drawable.store_noimage)
                .into(imgSnap);

        Glide.with(context)
                .load(snapData.getStore().getAssets().getPhoto())
                .asBitmap()
                .thumbnail(0.1f)
                .fitCenter()
                .placeholder(R.drawable.default_pic_store_logo)
                .into(imgMerchant);

        txtTimeText.setVisibility(View.VISIBLE);
        txtTimeText.setText(UtilManis.getTimeAgoMiddle(snapData.uploaded, context));

        String pattern = DateFormat.is24HourFormat(context) ? "HH:mm" : "hh:mm a";
        SimpleDateFormat timeFomat = new SimpleDateFormat(pattern, Locale.getDefault());
        Date dateTime = new Date(snapData.uploaded * 1000L);
        txtTime.setText(timeFomat.format(dateTime));
        txtTime.setVisibility(View.VISIBLE);

        SimpleDateFormat dateFomat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        Date date = new Date(snapData.uploaded * 1000L);
        txtDate.setText(dateFomat.format(date));
        txtDate.setVisibility(View.VISIBLE);

        if (currency != null) {
            txtCurrency.setText(currency.getIso2());
            if (currency.getIso2().trim().equalsIgnoreCase("RM"))
                txtCurrencyNumber.setText(String.valueOf(snapData.getReceipt().getAmount()));
            else
                txtCurrencyNumber.setText(String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(snapData.getReceipt().getAmount())));
            txtCurrencyNumber.setText(String.format(Locale.getDefault(), currency.getIso2().equalsIgnoreCase("RM") ? "%,.2f" : "%,.0f", snapData.getReceipt().getAmount()));
        }

        imgStatus.setVisibility(View.VISIBLE);
        txtStatus.setVisibility(View.VISIBLE);
        mainReview.setVisibility(View.VISIBLE);
        txtStatus.setText(snapData.status);
        if (snapData.statusType == SnapData.STATUS_APPROVED) {
            imgStatus.setImageResource(R.drawable.snap_history_accepted);
            mainReview.setVisibility(View.GONE);
        } else if (snapData.statusType == SnapData.STATUS_REJECTED) {
            imgStatus.setImageResource(R.drawable.snap_history_rejected);
            txtReview.setText(snapData.remark);
        } else {
            imgStatus.setImageResource(R.drawable.snap_history_pending);
            if (snapData.statusType == SnapData.STATUS_PENDING)
                txtReview.setText(getString(R.string.ds_txt_review));
            else
                txtReview.setText(getString(R.string.ds_txt_appeal));
        }
    }

    @OnClick(R.id.btn_close)
    void closeOnClick() {
        finish();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.SNAP_POP_UP_DETAIL,
                ConfigManager.Analytic.Action.CLICK,
                "Button Close"
        );
    }

    @OnClick(R.id.btn_viewimage)
    void viewImageOnClick() {
        Intent intent = new Intent(context, ViewImageSnap.class);
        intent.putExtra(ConfigManager.Snap.DATA, snapData);
        startActivity(intent);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.SNAP_POP_UP_DETAIL,
                ConfigManager.Analytic.Action.CLICK,
                "Image Receipt"
        );
    }

}
