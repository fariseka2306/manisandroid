package com.ebizu.manis.mvp.reward;

import com.ebizu.manis.helper.LoadType;
import com.ebizu.sdk.reward.models.Filter;
import com.ebizu.sdk.reward.models.Reward;

/**
 * Created by Raden on 7/26/17.
 */

public interface IReedemPresenter {
    void loadReedem();

    void loadRedem(Filter rewardFilter, LoadType loadType);
}
