package com.ebizu.manis.mvp.account.accountmenulist.profile;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.helper.GSONHelper;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.rx.ResponseSubsriberLambda;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.AccountBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.WrapperAccount;
import com.google.gson.Gson;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by abizu-alvio on 7/26/2017.
 */

public class ProfilePresenter extends BaseViewPresenter implements IProfilePresenter {

    private final String TAG = getClass().getSimpleName();

    private ProfileView profileView;
    private ManisApi manisApi;
    private Context context;
    private Subscription subsProfile;

    public ProfilePresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        profileView = (ProfileView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsProfile != null) subsProfile.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void saveProfile(AccountBody accountBody) {
        releaseSubscribe(0);
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceWithToken(context);
        profileView.getBaseActivity().showProgressBarDialog(context.getString(R.string.please_wait));
        subsProfile = manisApi.saveAccountProfile(GSONHelper.newInstance().getGson().toJson(accountBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperAccount>(profileView) {
                    @Override
                    public void onNext(WrapperAccount wrapperAccount) {
                        super.onNext(wrapperAccount);
                        profileView.finishActivity();
                        profileView.getBaseActivity().dismissProgressBarDialog();
                        profileView.getManisSession().updateSession(wrapperAccount.getAccount());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        profileView.getBaseActivity().dismissProgressBarDialog();
                        if (!ConnectionDetector.isNetworkConnected(context))
                            NegativeScenarioManager.show(throwable, profileView, NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }

                    @Override
                    public void onErrorFailure(ErrorResponse errorResponse) {
                        super.onErrorFailure(errorResponse);
                        NegativeScenarioManager.show(errorResponse, profileView, NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }
                });
        Log.d(ConfigManager.API_TAG, "saveProfile: ".concat(new Gson().toJson(accountBody)));
    }

    @Override
    public void saveProfileData(RequestBody requestBody) {
        releaseSubscribe(0);
        profileView.getBaseActivity().showProgressBarDialog(context.getString(R.string.please_wait));
        subsProfile = getManisApiV2().saveAccountProfile(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperAccount>(profileView) {
                    @Override
                    protected void onSuccess(WrapperAccount wrapperAccount) {
                        super.onSuccess(wrapperAccount);
                        profileView.getManisSession().updateSession(wrapperAccount.getAccount());
                        profileView.getBaseActivity().dismissProgressBarDialog();
                        profileView.finishActivity();
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                        profileView.getBaseActivity().dismissProgressBarDialog();
                        profileView.getBaseActivity().showManisAlertDialog(message);
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        profileView.getBaseActivity().dismissProgressBarDialog();
                        profileView.getBaseActivity().showManisAlertDialog(message);
                    }
                });
    }
}
