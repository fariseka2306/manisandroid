package com.ebizu.manis.mvp.reward.rewardlistcategory.rewardbulk;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.reward.rewardlistcategory.RewardListCategoryAbstractView;
import com.ebizu.manis.service.reward.requestbody.RewardBulkListBody;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public class RewardListBulkView extends RewardListCategoryAbstractView {

    public RewardListBulkView(Context context) {
        super(context);
    }

    public RewardListBulkView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RewardListBulkView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RewardListBulkView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    @Override
    public void loadRewardPresenter() {
        RewardBulkListBody.Data data = new RewardBulkListBody.Data();
        data.setSize(ConfigManager.Reward.REWARD_BULK_SIZE);
        data.setPage(page);
        data.setKeyword("");
        data.setOrder(1);
        data.setNoCache("Y");
        RewardBulkListBody requestBody = new RewardBulkListBody(getContext(), data);
        getRewardCategoryPresenter().loadRewardBulkListNext(requestBody);
    }

    @Override
    public void reloadRewardPresenter() {
        RewardBulkListBody.Data data = new RewardBulkListBody.Data();
        data.setSize(ConfigManager.Reward.REWARD_BULK_SIZE);
        data.setPage(ConfigManager.Reward.REWARD_BULK_FIRST_PAGE);
        data.setKeyword("");
        data.setOrder(1);
        data.setNoCache("Y");
        RewardBulkListBody requestBody = new RewardBulkListBody(getContext(), data);
        getRewardCategoryPresenter().loadRewardsBulkList(requestBody);
    }
}
