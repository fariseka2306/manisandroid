package com.ebizu.manis.mvp.snap.store.list;

import com.ebizu.manis.model.Store;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by ebizu on 11/27/17.
 */

public interface IListSnapStoreView
        extends IBaseView {

    void startActivityReceiptDetail(Store store);

    void startActivityProfile(Store store);

}
