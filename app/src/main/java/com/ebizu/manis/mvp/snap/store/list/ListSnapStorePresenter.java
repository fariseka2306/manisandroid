package com.ebizu.manis.mvp.snap.store.list;

import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.snap.SnapableLuckyDraw;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.rx.ResponseSubsriberLambda;
import com.ebizu.manis.service.manis.requestbody.snap.SnapableRequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.WrapperSnapable;
import com.google.gson.Gson;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ebizu on 11/27/17.
 */

public class ListSnapStorePresenter extends BaseViewPresenter implements IListSnapStorePresenter {

    private ListSnapStoreView listSnapStoreView;
    private Subscription subsSnapStore;


    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        listSnapStoreView = (ListSnapStoreView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsSnapStore != null) subsSnapStore.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void isSnapAble(Store store) {
        listSnapStoreView.getBaseActivity().showProgressBarDialog("Please wait...",
                dialogInterface -> releaseSubscribe(0));
        SnapableRequestBody snapableRequestBody = new SnapableRequestBody();
        snapableRequestBody.setComId(store.getId());
        snapableRequestBody.setMerchantTier(store.getMerchantTier());
        subsSnapStore = getManisApi().isSnapable(new Gson().toJson(snapableRequestBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperSnapable>(listSnapStoreView) {
                    @Override
                    public void onNext(WrapperSnapable wrapperSnapable) {
                        super.onNext(wrapperSnapable);
                        listSnapStoreView.getBaseActivity().dismissProgressBarDialog();
                        SnapableLuckyDraw snapableLuckyDraw = wrapperSnapable.getSnapableLuckyDraw();
                        if (snapableLuckyDraw.isUserAllowedSnap()) {
                            if (listSnapStoreView.getManisSession().isNotAvailableNameUser()) {
                                listSnapStoreView.startActivityProfile(store);
                            } else {
                                listSnapStoreView.startActivityReceiptDetail(store);
                            }
                        } else {
                            listSnapStoreView.getBaseActivity().showManisAlertDialog(snapableLuckyDraw.getRestrictionMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        listSnapStoreView.getBaseActivity().dismissProgressBarDialog();
                        if (!ConnectionDetector.isNetworkConnected(listSnapStoreView.getContext()))
                            NegativeScenarioManager.show(e, listSnapStoreView, NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }

                    @Override
                    public void onErrorFailure(ErrorResponse errorResponse) {
                        super.onErrorFailure(errorResponse);
                        listSnapStoreView.getBaseActivity().dismissProgressBarDialog();
                        NegativeScenarioManager.show(errorResponse, listSnapStoreView,
                                NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }
                });
    }

    @Override
    public void isSnapAble(Store store, RequestBody requestBody) {
        subsSnapStore = getManisApiV2().checkSnapAbleLuckyDraw(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperSnapable>(listSnapStoreView) {

                    @Override
                    protected void onSuccess(WrapperSnapable wrapperSnapable) {
                        super.onSuccess(wrapperSnapable);
                        listSnapStoreView.getBaseActivity().dismissProgressBarDialog();
                        SnapableLuckyDraw snapableLuckyDraw = wrapperSnapable.getSnapableLuckyDraw();
                        if (snapableLuckyDraw.isUserAllowedSnap()) {
                            if (listSnapStoreView.getManisSession().isNotAvailableNameUser()) {
                                listSnapStoreView.startActivityProfile(store);
                            } else {
                                listSnapStoreView.startActivityReceiptDetail(store);
                            }
                        } else {
                            listSnapStoreView.getBaseActivity().showManisAlertDialog(snapableLuckyDraw.getRestrictionMessage());
                        }
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                        listSnapStoreView.getBaseActivity().dismissProgressBarDialog();
                        listSnapStoreView.getBaseActivity().showManisAlertDialog(message);
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        listSnapStoreView.getBaseActivity().dismissProgressBarDialog();
                        listSnapStoreView.getBaseActivity().showManisAlertDialog(message);
                    }
                });
    }
}
