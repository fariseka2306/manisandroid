package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistorypyshicalfactory;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.PurchaseHistory;

/**
 * Created by halim_ebizu on 11/16/17.
 */

public class PurchasePyshicalShipped extends PurchasePyshicalAbstract implements PurchasePyshicalStatus {

    public PurchasePyshicalShipped(PurchaseHistory purchaseHistory, Context context) {
        super(purchaseHistory, context);
    }

    @Override
    public String getDeliveryStatus() {
        return "5";
    }

    @Override
    public String getStatus() {
        return ConfigManager.PurchaseHistory.PYSHICAL_STATUS_SHIPPED;
    }

    @Override
    public Drawable imageIcon() {
        return ContextCompat.getDrawable(context, R.drawable.shipped_icon);
    }

    @Override
    public String getInstruction() {
        return context.getString(R.string.ph_physical_shipped).concat(" ").concat(purchaseHistory.getShipmentNumber());
    }

    @Override
    public int getInstructionVisibility() {
        return View.GONE;
    }
}
