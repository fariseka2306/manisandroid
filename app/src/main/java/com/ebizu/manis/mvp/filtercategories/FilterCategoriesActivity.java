package com.ebizu.manis.mvp.filtercategories;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.FilterHolder;
import com.ebizu.manis.helper.FixedLinearLayoutManager;
import com.ebizu.manis.helper.GridSpacingItemDecoration;
import com.ebizu.manis.mvp.reward.ReedemFragment;
import com.ebizu.manis.view.adapter.AdapterFilterListener;
import com.ebizu.manis.view.adapter.FilterCategoryAdapter;
import com.ebizu.manis.view.holder.FilterContentViewHolder;
import com.ebizu.manis.view.linearlayout.ItemOffsetsDecoration;
import com.ebizu.sdk.reward.models.Category;
import com.ebizu.sdk.reward.models.Response;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Raden on 8/2/17.
 */

public class FilterCategoriesActivity extends AppCompatActivity implements AdapterFilterListener<Category>,IFilterCategoriesView{

    public static final String CATEGORIES = "categories";
    private Context context;
    private FilterCategoryAdapter<Category> categoryFilterCategoryAdapter;
    private ArrayList<Category> data;
    private ArrayList<String> filterCategories;
    private FilterCategoriesPresenter filterCategoriesPresenter;
    int spanCount = 8000;
    int spacing = 14;
    boolean includeEdge = true;

    @BindView(R.id.rv_filter_content)
    RecyclerView rvFilterContent;
    @BindView(R.id.btn_filter)
    Button btnFilter;
    @BindView(R.id.list_filter_subtitle)
    TextView txtFilterSubtitle;
    @BindView(R.id.progress_filter)
    ProgressBar progressFilter;
    @BindView(R.id.animator)
    ViewAnimator viewAnimator;

    @BindView(R.id.tv_page_title)
    TextView txtPageTitle;
    @BindView(R.id.iv_right_action)
    ImageView imgRightAction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        filterCategories = getIntent().getStringArrayListExtra(CATEGORIES);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_categories);
        data = new ArrayList<>();
        context = this;
        ButterKnife.bind(this);

        filterCategoriesPresenter = new FilterCategoriesPresenter(context);
        filterCategoriesPresenter.attachView(this);

        filterCategoriesPresenter.loadFilterCategories();

        initiliaze();
        updateFilterButton();
    }

    private void initiliaze(){
        txtFilterSubtitle.setText(getString(R.string.list_filter_category));
        txtPageTitle.setText(getString(R.string.filter_reward_by) + " " + getString(R.string.categories));

        categoryFilterCategoryAdapter = new FilterCategoryAdapter<>(this,data);
        categoryFilterCategoryAdapter.setAdapterFilterListener(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        rvFilterContent.setLayoutManager(gridLayoutManager);
        rvFilterContent.addItemDecoration(new GridSpacingItemDecoration(spanCount,spacing,includeEdge));
        rvFilterContent.setAdapter(categoryFilterCategoryAdapter);
    }

    @Override
    public void onCreateListItem(Category data, FilterContentViewHolder holder, int position) {
        holder.txtCheck.setText(data.getName());
        if (filterCategories.contains(data.getId())) {
            holder.viewCheck.setVisibility(View.VISIBLE);
        }else{
            holder.viewCheck.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSelectItem(Category data, boolean isSelected) {
        if (!isSelected)
            filterCategories.add(data.getId());
        else
            filterCategories.remove(data.getId());
        updateFilterButton();
    }

    @Override
    public void setFilterCategories(List<Category> categoryData) {
      try {
        data.addAll(categoryData);
        categoryFilterCategoryAdapter.notifyDataSetChanged();
        viewAnimator.setDisplayedChild(1);
      }catch (Exception e){
        e.printStackTrace();
      }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom);
    }

    @OnClick(R.id.btn_filter)
    void onClickBtnFilter(){
        Intent intent = new Intent(ReedemFragment.RECEIVER_REWARD);
        intent.putExtra(ReedemFragment.FILTER_REWARD, true);
        intent.putStringArrayListExtra(ReedemFragment.ADD_REMOVE_FILTER_CATEGORY, filterCategories);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        onBackPressed();
    }

    private void updateFilterButton(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                int sizeFilter = filterCategories.size();
                if (sizeFilter == 0)
                    btnFilter.setText(getString(R.string.txt_btn_filter, "", getString(R.string.category)));
                else if (sizeFilter >= 2)
                    btnFilter.setText(getString(R.string.txt_btn_filter, String.valueOf(sizeFilter), getString(R.string.categories)));
                else
                    btnFilter.setText(getString(R.string.txt_btn_filter, String.valueOf(sizeFilter), getString(R.string.category)));
            }
        });
    }
}
