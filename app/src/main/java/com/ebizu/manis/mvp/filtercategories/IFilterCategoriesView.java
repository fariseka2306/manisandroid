package com.ebizu.manis.mvp.filtercategories;

import com.ebizu.sdk.reward.models.Category;
import com.ebizu.sdk.reward.models.Response;

import java.util.List;

/**
 * Created by Raden on 8/2/17.
 */

public interface IFilterCategoriesView {

    void setFilterCategories(final List<Category> categoryData);
}
