package com.ebizu.manis.mvp.luckydraw.luckydrawhelp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.root.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Halim on 8/6/17.
 */

public class LuckyDrawHelpSecondFragment extends BaseFragment {

    private LuckyDrawHelpActivity luckyDrawHelpActivity;

    @BindView(R.id.lucky_draw_help_view)
    LuckyDrawHelpView luckyDrawHelpView;

    @BindView(R.id.luckydraw_btn_ok)
    Button luckyDrawBtnOk;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        luckyDrawHelpActivity = (LuckyDrawHelpActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_lucky_draw_help_second, container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        int position = args.getInt(UtilStatic.ARG_POSITION);
        view.setTag(position);
    }

    @OnClick(R.id.luckydraw_btn_ok)
    public void onBtnAwesomeClick() {
        luckyDrawHelpActivity.switchToFragment(ConfigManager.LuckyDraw.LUCKY_DRAW_THIRD_FRAGMENT);
    }

    private void initView() {
        luckyDrawBtnOk.setText(R.string.ld_help_btn_next);
        luckyDrawHelpView.setLuckyDrawHelpView(R.string.ld_help2_title,
                R.string.ld_help2_subtitle, R.drawable.receiptvalidity);
    }
}
