package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.statusmyvoucher.expiredstatus;

import android.util.Log;

import com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.MyVouchersPresenter;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.service.reward.requestbody.RewardMyVoucherBody;
import com.ebizu.manis.service.reward.response.WrapperRewardVoucherList;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raden on 11/14/17.
 */

public class ExpiredStatusPresenter extends BaseViewPresenter implements IExpiredStatusPresenter{

    private static final String TAG = MyVouchersPresenter.class.getSimpleName();

    private VoucherExpiredView voucherExpiredView;
    private Subscription subVoucherExpired;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        voucherExpiredView = (VoucherExpiredView) baseView;

    }

    @Override
    public void releaseSubscribe(int key) {
        if (subVoucherExpired != null) {
            subVoucherExpired.unsubscribe();
        }
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void getExpiredVouchers(RewardMyVoucherBody rewardMyVoucherBody, IBaseView.LoadType loadType, String expired) {
        releaseSubscribe(0);
        rewardMyVoucherBody.getData().setCondtion(expired);
        initializeLoad(loadType);
        subVoucherExpired = getRewardApi().listMyVoucher(rewardMyVoucherBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperRewardVoucherList>(voucherExpiredView){
                    @Override
                    public void onNext(WrapperRewardVoucherList wrapperMyVoucher) {
                        super.onNext(wrapperMyVoucher);
                        dismissTypeProgress(loadType);
                        voucherExpiredView.isOffline = false;
                        voucherExpiredView.dismissProgressBar();
                        voucherExpiredView.showVouchersExpirerdView(wrapperMyVoucher, loadType);
                        Log.d(TAG, "onNextExpired: " + wrapperMyVoucher);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        voucherExpiredView.dismissProgressBar();
                        dismissTypeProgress(loadType);
                        if(loadType == IBaseView.LoadType.SCROLL_LOAD){
                            voucherExpiredView.page--;
                        }
                        voucherExpiredView.loadMyVouchersViewFail(loadType);
                    }
                });
    }

    private void initializeLoad(IBaseView.LoadType loadType){
        showTypeProgress(loadType);
    }

    private void showTypeProgress(IBaseView.LoadType loadType){
        voucherExpiredView.setLoading(true);
        switch (loadType){
            case CLICK_LOAD:{
                voucherExpiredView.invisibleListVoucherView();
                voucherExpiredView.showProgressBarCircle();
                break;
            }
            case SCROLL_LOAD:{
                voucherExpiredView.getMyVoucherAdapter().addLoadingFooter();
                break;
            }
            case SEARCH_LOAD:{
                break;
            }
            case SWIPE_LOAD:{
                break;
            }
        }
    }

    private void dismissTypeProgress(IBaseView.LoadType loadType){
        voucherExpiredView.setLoading(false);
        switch (loadType){
            case CLICK_LOAD:{
                voucherExpiredView.dismissProgressBarCircle();
                break;
            }
            case SCROLL_LOAD:{
                voucherExpiredView.getMyVoucherAdapter().removeLoadingFooter();
                break;
            }
            case SEARCH_LOAD:{
                break;
            }
            case SWIPE_LOAD:{
                break;
            }
        }
    }
}
