package com.ebizu.manis.mvp.main.activityresult;

import android.content.Intent;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.GPSTracker;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by ebizu on 10/25/17.
 */

public class StoreRequest extends MainRequestCode
        implements IMainRequestCode   {

    public StoreRequest(MainActivity mainActivity) {
        super(mainActivity);
    }

    @Override
    public int requestCode() {
        return ConfigManager.Store.LOCATION_RC;
    }

    @Override
    public void jobRequest(int resultCode, Intent data) {
        super.jobRequest(resultCode, data);
        if(new GPSTracker(mainActivity).isGPSEnabled()){
            mainActivity.getStoreFragment()
                    .getStoreNearbyFragment()
                    .getStoreNearbyView()
                    .loadPresenter(IBaseView.LoadType.CLICK_LOAD);
        }
    }
}
