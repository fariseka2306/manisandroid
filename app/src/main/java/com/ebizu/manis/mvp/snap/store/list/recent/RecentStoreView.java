package com.ebizu.manis.mvp.snap.store.list.recent;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.snap.SnapStoreResult;
import com.ebizu.manis.mvp.snap.store.list.IListSnapStorePresenter;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.manis.recyclerview.SnapStoreRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public class RecentStoreView extends BaseView implements IRecentStoreView {

    @BindView(R.id.text_view_title)
    TextView textViewTitle;
    @BindView(R.id.snap_store_recycler_view)
    SnapStoreRecyclerView snapStoreRecyclerView;

    private RecentStorePresenter recentStorePresenter;
    private int page = 1;
    private boolean loading = false;
    private boolean lastPage = false;

    private OnRefresListener onRefresListener;

    public RecentStoreView(Context context) {
        super(context);
        createView(context);
    }

    public RecentStoreView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public RecentStoreView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RecentStoreView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_recent, null, false);
        addView(view, new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        recentStorePresenter = (RecentStorePresenter) iBaseViewPresenter;
        recentStorePresenter.attachView(this);
    }

    @Override
    public void addRecentStores(SnapStoreResult snapStoreResult, LoadType loadType) {
        setLastPage(!snapStoreResult.getMore());
        switch (loadType) {
            case CLICK_LOAD: {
                snapStoreRecyclerView.replaceData(snapStoreResult.getStores());
                break;
            }
            case SWIPE_LOAD: {
                snapStoreRecyclerView.replaceData(snapStoreResult.getStores());
                break;
            }
            case SCROLL_LOAD: {
                snapStoreRecyclerView.addData(snapStoreResult.getStores());
                break;
            }
        }
        if (snapStoreRecyclerView.getSnapStoreAdapter().getSnapStores().size() > 0){
            textViewTitle.setVisibility(VISIBLE);
        }else {
            textViewTitle.setVisibility(GONE);
        }
        setLoading(false);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        snapStoreRecyclerView.initialize(baseActivity);
    }

    @Override
    public void setOnSnapAbleListener(IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener) {
        snapStoreRecyclerView.setOnSnapAbleListener(onCheckSnapAbleListener);
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }

    public SnapStoreRecyclerView getSnapStoreRecyclerView() {
        return snapStoreRecyclerView;
    }

    public int getPage() {
        return page;
    }

    public boolean isLoading() {
        return loading;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public RecentStorePresenter getRecentStorePresenter() {
        return recentStorePresenter;
    }

    public void setOnRefreshListener(OnRefresListener onRefreshListener) {
        if (this.onRefresListener == null)
            this.onRefresListener = onRefreshListener;
    }

    public OnRefresListener getOnRefresListener() {
        return onRefresListener;
    }

    public interface OnRefresListener {
        void onRefreshComplete();
    }
}
