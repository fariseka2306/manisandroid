package com.ebizu.manis.mvp.luckydraw.luckydrawscreen;

import com.ebizu.manis.model.LuckyDrawScreen;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by ebizu on 04/08/17.
 */

public interface ILuckyDrawView extends IBaseView {

    void setLuckyDrawScreen(LuckyDrawScreen luckyDrawScreen);

    ILuckyDrawPresenter getLuckyDrawPresenter();
}
