package com.ebizu.manis.mvp.account.accountmenulist.settings.notification;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.NotificationsSetting;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.view.adapter.account.AccountNotificationsAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by abizu-alvio on 7/24/2017.
 */

public class NotificationsView extends BaseView implements INotificationsView, AccountNotificationsAdapter.SwitchNotificationListener {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.recyclerview_notification)
    RecyclerView recyclerViewNotification;
    @BindView(R.id.lin_save)
    LinearLayout saveButton;

    private Context context;
    private ConstraintLayout noInternetConnectionView;
    private RelativeLayout parent;
    private AccountNotificationsAdapter accountNotificationsAdapter;
    LinearLayoutManager linearLayoutManager;

    private INotificationsPresenter iNotificationsPresenter;

    public NotificationsView(Context context) {
        super(context);
        createView(context);
    }

    public NotificationsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public NotificationsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NotificationsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.account_settings_notification_recycler, null, false);
        accountNotificationsAdapter = new AccountNotificationsAdapter(context, new ArrayList<>());
        accountNotificationsAdapter.setSwitchListener(this);
        this.context = context;
        addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        attachPresenter(new NotificationsPresenter(context));
        initialize();
    }

    private void initialize() {
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewNotification.setLayoutManager(linearLayoutManager);
        recyclerViewNotification.setAdapter(accountNotificationsAdapter);
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        this.iNotificationsPresenter = (INotificationsPresenter) iBaseViewPresenter;
        this.iNotificationsPresenter.attachView(this);
    }

    @Override
    public void showNotification(List<NotificationsSetting> notificationsSettings) {
        this.accountNotificationsAdapter.addNotificationsList(notificationsSettings);
    }

    @Override
    public void sendPost(List<NotificationsSetting> notificationsSettings) {
        iNotificationsPresenter.saveViewNotifications(getNotificationPresenter().createAccountUpdateNotifConfigRB(notificationsSettings));
    }

    @Override
    public INotificationsPresenter getNotificationPresenter() {
        return iNotificationsPresenter;
    }

    @Override
    public void onSwitch(NotificationsSetting notificationsSetting) {
    }

    @Override
    public void onRetry() {
        super.onRetry();
        getNotificationPresenter().loadViewNotifications();
    }

    @OnClick(R.id.lin_save)
    void onClickSaveClick() {
        RequestBody requestBody = getNotificationPresenter().createAccountUpdateNotifConfigRB(accountNotificationsAdapter.getModelNotificationsSettings());
        getNotificationPresenter().saveViewNotifications(requestBody);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.NOTIFICATION_SETTINGS_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Save"
        );
    }

    public void finishActivity() {
        getBaseActivity().finish();
        getBaseActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.NOTIFICATION_SETTINGS_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Back"
        );
    }
}