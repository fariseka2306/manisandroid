package com.ebizu.manis.mvp.home.brands;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.model.Brand;
import com.ebizu.manis.root.BaseActivityPresenter;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;

import java.util.ArrayList;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raden on 7/5/17.
 */

public class HomePresenter extends BaseActivityPresenter implements IHomePresenter {

    private static final String TAG = HomePresenter.class.getSimpleName();

    private Context context;
    private IHomeView iHomeView;
    private Activity activity;
    private Subscription subsBrand;

    public HomePresenter(Context context, Activity activity) {
        this.activity = activity;
        this.context = context;
    }

    public void attachView(IHomeView iHomeView) {
        this.iHomeView = iHomeView;
    }

    @Override
    public void loadBrandData(ArrayList<Brand> brand) {
        releaseSubscribe(0);
        ManisApi manisApi = ManisApiGenerator.createServiceWithToken(context);
        subsBrand = manisApi.getBrands().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        wrapperBrand -> {
                            iHomeView.loadViewBrand(wrapperBrand.getGetData().getBrands());
                            iHomeView.setBrandData(wrapperBrand.getGetData().getTitle());
                            Log.d(TAG, "loadSnapData: " + wrapperBrand.toString());
                            Log.d(TAG, "setBrandData: " + wrapperBrand.getGetData().getTitle());
                        },
                        throwable -> {
                            cekConnection(iHomeView);
                            Log.e(TAG, "loadSnapData: " + throwable.getMessage());
                        }
                );
    }


    public void cekConnection(IHomeView iHomeView) {
        if (!ConnectionDetector.isNetworkConnected(context)) {
            iHomeView.failedConnected();
        } else {
            iHomeView.noBrandData();
        }
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsBrand != null) subsBrand.unsubscribe();
    }

    @Override
    public void releaseAllSubscribe() {
        releaseSubscribe(0);
    }
}
