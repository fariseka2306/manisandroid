package com.ebizu.manis.mvp.store.storecategory;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.InterestsStore;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.SnapGuideActivity;
import com.ebizu.manis.mvp.store.storecategorydetail.StoreCategoryDetailActivity;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.view.adapter.ListAdapter;
import com.ebizu.manis.view.adapter.StoreCategoryAdapter;
import com.ebizu.manis.view.manis.nointernetconnection.NoInternetConnectionView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Halim on 7/28/17.
 */

public class StoreCategoryView extends BaseView implements IStoreCategoryView,
        ListAdapter.OnClickListener<InterestsStore> {

    private StoreCategoryAdapter storeCategoryAdapter;
    private IStoreCategoryPresenter iStoreCategoryPresenter;
    private DeviceSession deviceSession;

    @BindView(R.id.rv_store_category)
    RecyclerView rvStoreCategory;

    @BindView(R.id.no_internet_connection_view)
    NoInternetConnectionView noInternetConnectionView;

    @BindView(R.id.no_network_view)
    ConstraintLayout noNetworkView;

    public StoreCategoryView(Context context) {
        super(context);
        createView(context);
    }

    public StoreCategoryView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public StoreCategoryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public StoreCategoryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.store_category_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new StoreCategoryPresenter(context));
        initView();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iStoreCategoryPresenter = (IStoreCategoryPresenter) iBaseViewPresenter;
        iStoreCategoryPresenter.attachView(this);
    }

    @Override
    public void loadInterestsStore(ArrayList<InterestsStore> interestsStores) {
        storeCategoryAdapter.addAdapterList(interestsStores);
        rvStoreCategory.setVisibility(VISIBLE);
        noInternetConnectionView.setVisibility(GONE);
    }

    @Override
    public void noInternetConnection() {
        noInternetConnectionView.setVisibility(VISIBLE);
        rvStoreCategory.setVisibility(GONE);
    }

    @Override
    public void showNoInternetConnection() {
        super.showNoInternetConnection();
        rvStoreCategory.setVisibility(GONE);
    }

    @OnClick(R.id.no_network_view)
    public void onNoNetworkViewClick() {
        iStoreCategoryPresenter.loadInterestData(ManisApiGenerator.createServiceWithToken(getContext()));
        noInternetConnectionView.setVisibility(GONE);
    }

    @Override
    public IStoreCategoryPresenter getStoreCategoryPresenter() {
        return iStoreCategoryPresenter;
    }

    private void initView() {
        storeCategoryAdapter = new StoreCategoryAdapter(getContext(), new ArrayList<InterestsStore>());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvStoreCategory.getContext(),
                linearLayoutManager.getOrientation());

        rvStoreCategory.setLayoutManager(linearLayoutManager);
        rvStoreCategory.setAdapter(storeCategoryAdapter);
        rvStoreCategory.addItemDecoration(dividerItemDecoration);
        storeCategoryAdapter.setOnClickListener(this);
        storeCategoryAdapter.setPaginationAble(false);
        deviceSession = new DeviceSession(getContext());
    }

    @Override
    public void onClick(InterestsStore interestsStore) {
        if (!deviceSession.hasShowSnapEarnGuide()) {
            Intent intent = new Intent(getContext(), SnapGuideActivity.class);
            intent.putExtra(ConfigManager.Snap.SNAP_EARN_GUIDE, ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_STORE);
            intent.putExtra(ConfigManager.Store.INTEREST_STORE, interestsStore);
            getBaseActivity().startActivityForResult(intent, ConfigManager.Snap.SNAP_EARN_GUIDE_REQUEST_CODE);
        } else {
            Intent intent = new Intent(getContext(), StoreCategoryDetailActivity.class);
            intent.putExtra(ConfigManager.Store.INTEREST_STORE, interestsStore);
            intent.putExtra(ConfigManager.Store.INTEREST_STORE_ID, interestsStore.getId());
            getBaseActivity().startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
        }
    }
}
