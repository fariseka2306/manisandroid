package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistoryfactory;

import android.graphics.drawable.Drawable;

/**
 * Created by halim_ebizu on 10/17/17.
 */

public interface PurchaseStatus {

    String getStatus();

    Drawable imageIcon();

    int color();

    boolean isShowInfo();
}
