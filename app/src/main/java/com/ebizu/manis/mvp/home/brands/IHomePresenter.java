package com.ebizu.manis.mvp.home.brands;

import com.ebizu.manis.model.Brand;
import com.ebizu.manis.root.IBaseActivityPresenter;

import java.util.ArrayList;

/**
 * Created by Raden on 7/4/17.
 */

public interface IHomePresenter extends IBaseActivityPresenter {

    void loadBrandData(ArrayList<Brand> brand);

}
