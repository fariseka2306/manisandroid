package com.ebizu.manis.mvp.snap.store.list.near;

import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public interface INearStorePresenter extends IBaseViewPresenter {

    void getSnapStoresNearby(int page, IBaseView.LoadType loadType);
}
