package com.ebizu.manis.mvp.beacon.interval;

/**
 * Created by ebizu on 9/7/17.
 */

public class HourInterval extends NotificationInterval implements IntervalInterface {

    @Override
    public String type() {
        return "hour";
    }

    @Override
    public long interval(int interval) {
        return NotificationBeaconInterval.HOUR * interval;
    }

}
