package com.ebizu.manis.mvp.snap.receipt.camera;

import android.animation.ObjectAnimator;
import android.view.View;

import com.daimajia.androidanimations.library.BaseViewAnimator;

/**
 * Created by FARIS_mac on 30/11/17.
 */

public class StandDownAnimator extends BaseViewAnimator {
    @Override
    protected void prepare(View target) {
        float x = (target.getWidth() - target.getPaddingLeft() - target.getPaddingRight()) / 2 + target.getPaddingLeft();
        float y = target.getHeight() - target.getPaddingBottom();
        getAnimatorAgent().playTogether(
                ObjectAnimator.ofFloat(target, "rotationX", 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 60, 60, 55, 50, 40, 35, 30, 25, 20, 15, 10, 5, 0),
                ObjectAnimator.ofFloat(target, "pivotX", x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x, x),
                ObjectAnimator.ofFloat(target, "pivotY", y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y, y)
        );
    }
}
