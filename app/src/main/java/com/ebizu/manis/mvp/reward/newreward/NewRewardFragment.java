package com.ebizu.manis.mvp.reward.newreward;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.mvp.reward.rewardlistcategory.rewardregular.RewardRegularActivity;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.view.manis.search.SearchView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerOriginPageRewards;

/**
 * Created by FARIS_mac on 9/22/17.
 */

public class NewRewardFragment extends BaseFragment {

    public static final String RECEIVER_REWARD = "receiver_reward";
    public static final String ARG_PARAM_REWARD_FRAGMENT = "EXTRA_REWARD_FRAGMENT";
    public static String REWARD_CATEGORY_NAME = "category-name";
    public static String REWARD_CATEGORY_ID = "category-id";
    public static final String SORT_REWARD_TYPE = "sort_reward_type";
    public static final String CLEAR_FILTER = "clear_filter";
    public static final String ADD_REMOVE_FILTER_CATEGORY = "add_remove_filter_category";

    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.reward_view)
    NewRewardView rewardView;

    private MainActivity mainActivity;
    private ArrayList<String> filterCategories, filterBrands;

    private final BroadcastReceiver rewardBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(rewardBroadcastReceiver, new IntentFilter(RECEIVER_REWARD));
        View view = inflater.inflate(R.layout.fragment_reward, container, false);
        mainActivity = (MainActivity) getActivity();
        filterCategories = new ArrayList<>();
        filterBrands = new ArrayList<>();
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getContext() != null) {
            rewardView.setVisibility(View.VISIBLE);
            getAnalyticManager().trackScreenView(ConfigManager.Analytic.ScreenView.FRAGMENT_REWARD);
            TrackerManager.getInstance().setTrackerData(
                    new TrackerStandartRequest(kTrackerOriginPageRewards, kTrackerOriginPageRewards, "", "", "", "")
            );
        }
    }

    public void initView() {
        rewardView.setActivity((BaseActivity) getActivity());
        rewardView.getNewRewardPresenter().loadCategoryImage();
        searchView.setOnImeiSearch();
        searchView.setOnSearchListener((keyword) -> {
            //When click imei search listener
            searchRewardVoucherFromHome(keyword, ConfigManager.Reward.RequestRewardType.REQUEST_REWARD_TYPE_SEARCH);
            UtilManis.closeKeyboard(getActivity(), getView());
        });
        searchView.setOnTextChange(new SearchView.OnTextChangedListener() {
            @Override
            public void onValid(String keyword) {
            }

            @Override
            public void onInvalid() {
            }

            @Override
            public void onEmptyText() {
            }
        });
    }

    public void showRewardSearch(RewardVoucher rewardVoucher) {
        searchRewardVoucherFromHome(rewardVoucher.getBrand().getName(), ConfigManager.Reward.RequestRewardType.REQUEST_REWARD_TYPE_HOME);
    }

    private void searchRewardVoucherFromHome(String keyword, String requestRewardType) {
        Intent intent = new Intent(getContext(), RewardRegularActivity.class);
        intent.putExtra(ConfigManager.Reward.REWARD_SEARCH_KEYWORD, keyword);
        intent.putExtra(ConfigManager.Reward.REQUEST_REWARD_TYPE, requestRewardType);
        getContext().startActivity(intent);
    }

}
