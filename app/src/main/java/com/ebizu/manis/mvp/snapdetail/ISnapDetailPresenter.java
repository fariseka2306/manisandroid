package com.ebizu.manis.mvp.snapdetail;

/**
 * Created by Raden on 7/17/17.
 */

public interface ISnapDetailPresenter {
    void loadSnapDetails();
}
