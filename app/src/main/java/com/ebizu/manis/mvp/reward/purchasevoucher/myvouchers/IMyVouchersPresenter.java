package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbody.RewardRedeemBody;
import com.ebizu.manis.service.reward.requestbody.RewardMyVoucherBody;
import com.ebizu.sdk.reward.models.VoucherInput;

import java.util.List;

/**
 * Created by ebizu on 10/2/17.
 */

public interface IMyVouchersPresenter extends IBaseViewPresenter {

    void getMyVouchers(RewardMyVoucherBody rewardMyVoucherBody, IBaseView.LoadType loadType, String available);

    void getShoppingCart(RewardVoucher rewardVoucher, int qty);

    void unsubsShoppingCart();

    void setVoucherInputList(List<VoucherInput> voucherInputs);

    void unsubsMyVoucher();

}
