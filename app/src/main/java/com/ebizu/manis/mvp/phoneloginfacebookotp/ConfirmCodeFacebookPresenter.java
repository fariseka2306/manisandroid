package com.ebizu.manis.mvp.phoneloginfacebookotp;

import android.util.SparseArray;
import android.widget.Toast;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.helper.DeviceUtils;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Phone;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseActivityPresenter;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.DataOtp;
import com.ebizu.manis.service.manis.requestbody.DataPhoneAuth;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.service.manis.response.WrapperAccount;
import com.ebizu.manis.service.reward.RewardApiGenerator;
import com.ebizu.manis.service.reward.requestbody.RewardRegisterBody;
import com.ebizu.manis.service.reward.response.WrapperRegisterSession;
import com.ebizu.sdk.reward.EbizuReward;
import com.ebizu.sdk.reward.models.SessionData;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.LoginType;
import com.google.gson.Gson;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by FARIS_mac on 11/1/17.
 */

public class ConfirmCodeFacebookPresenter extends BaseActivityPresenter implements IConfirmCodeFacebookOtpPresenter {

    private BaseActivity baseActivity;
    private String phoneNumber;
    private ManisApi manisApi;
    private int requestCode;
    private SparseArray<Subscription> subsConfirmCode;

    public ConfirmCodeFacebookPresenter(BaseActivity baseActivity, int requestCode) {
        this.baseActivity = baseActivity;
        this.requestCode = requestCode;
        subsConfirmCode = new SparseArray<>();
    }

    @Override
    public void signInWithFacebookOtp(FacebookOtpLoginActivity facebookOtpLoginActivity) {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                PhoneNumber phone = account.getPhoneNumber();
                phoneNumber = fixPhoneNumber(phone.toString());
                signIn(phoneNumber, facebookOtpLoginActivity);
            }

            @Override
            public void onError(final AccountKitError error) {
            }
        });
    }

    private void signIn(String user_phone, FacebookOtpLoginActivity facebookOtpLoginActivity) {
        ConfirmCodeFacebookOtpHelper.authManis(baseActivity, this, getPhone(user_phone), facebookOtpLoginActivity);
    }

    @Override
    public void loginAccountManis(Phone phone, String regIdGcm, FacebookOtpLoginActivity facebookOtpLoginActivity) {
        releaseSubscribe(0);
        if (manisApi == null)
            manisApi = ManisApiGenerator.createService(baseActivity);
        Subscription subsLogin = manisApi.signInPhone(new Gson().toJson(getDataPhoneAuth(phone)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperAccount>(facebookOtpLoginActivity) {
                    @Override
                    public void onNext(WrapperAccount wrapperAccount) {
                        super.onNext(wrapperAccount);
                        if (wrapperAccount.getSuccess()) {
                            facebookOtpLoginActivity.onSignInComplete(wrapperAccount.getAccount());
                        } else {
                            Toast.makeText(baseActivity, "error", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        baseActivity.dismissProgressBarDialog();
                        facebookOtpLoginActivity.showAlertDialog(facebookOtpLoginActivity.getString(R.string.error),
                                getGeneralErrorMessage(),
                                false,
                                R.drawable.manis_logo,
                                facebookOtpLoginActivity.getString(R.string.ok),
                                (dialog, which) -> {
                                    facebookOtpLoginActivity.finish();
                                });
                    }
                });
        subsConfirmCode.put(0, subsLogin);
    }

    @Override
    public void signUpOtp(com.ebizu.manis.model.Account account, FacebookOtpLoginActivity facebookOtpLoginActivity) {
        releaseSubscribe(1);
        ManisApi manisApi = ManisApiGenerator.createServiceWithToken(baseActivity);
        DataOtp dataOtp = new DataOtp();
        dataOtp.setHandphoneNo(phoneNumber);
        dataOtp.setModule(ConfigManager.Auth.LOGIN);
        Subscription subsSignUpOtp = manisApi.signUpOtp(new Gson().toJson(dataOtp))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<ResponseData>(facebookOtpLoginActivity) {

                    @Override
                    public void onNext(ResponseData responseData) {
                        super.onNext(responseData);
                        if (responseData.getSuccess()) {
                            facebookOtpLoginActivity.getManisSession().setSession(account);
                            if (requestCode == ConfigManager.Otp.SIGN_UP_OTP_REQUEST_CODE) {
                                facebookOtpLoginActivity.onSuccessSignUpOtp(account, phoneNumber);
                            } else {
                                updateCountrySession(phoneNumber);
                                updateOtpSession(phoneNumber);
                                facebookOtpLoginActivity.setResult(ConfigManager.Otp.SUCCESS_SIGN_UP_OTP);
                                facebookOtpLoginActivity.finish();
                            }
                        } else {
                            Toast.makeText(baseActivity, "error", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        baseActivity.dismissProgressBarDialog();
                        if (!ConnectionDetector.isNetworkConnected(facebookOtpLoginActivity))
                            facebookOtpLoginActivity.showAlertDialog(facebookOtpLoginActivity.getString(R.string.error),
                                    facebookOtpLoginActivity.getString(R.string.error_connection),
                                    false,
                                    R.drawable.manis_logo,
                                    facebookOtpLoginActivity.getString(R.string.ok),
                                    (dialog, which) -> {
                                        facebookOtpLoginActivity.finish();
                                    });
                    }

                    @Override
                    public void onErrorFailure(ErrorResponse errorResponse) {
                        super.onErrorFailure(errorResponse);
                        baseActivity.dismissProgressBarDialog();
                        facebookOtpLoginActivity.showAlertDialog(facebookOtpLoginActivity.getString(R.string.error),
                                errorResponse.getMessage(),
                                false,
                                R.drawable.manis_logo,
                                facebookOtpLoginActivity.getString(R.string.ok),
                                (dialog, which) -> {
                                    facebookOtpLoginActivity.loginOtp(LoginType.PHONE);
                                });
                    }
                });
        subsConfirmCode.put(1, subsSignUpOtp);
    }

    @Override
    public void registerRewardSession(com.ebizu.manis.model.Account account, FacebookOtpLoginActivity facebookOtpLoginActivity) {
        releaseSubscribe(2);
        RewardRegisterBody.Data data = new RewardRegisterBody.Data();
        data.setUserId(account.getAccId());
        RewardRegisterBody rewardRegisterBody = new RewardRegisterBody(data);
        Observable<WrapperRegisterSession> observable = RewardApiGenerator.createService(baseActivity).registerSession(rewardRegisterBody);
        Subscription subsRegisterReward = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new ResponseRewardSubscriber<WrapperRegisterSession>(facebookOtpLoginActivity) {
                            @Override
                            public void onNext(WrapperRegisterSession wrapperRegisterSession) {
                                super.onNext(wrapperRegisterSession);
                                String session = wrapperRegisterSession.getRewardSession().getSession();
                                facebookOtpLoginActivity.getRewardSession().setSession(session);
                                loginRewardSdk(account, facebookOtpLoginActivity);
                            }

                            @Override
                            public void onError(Throwable e) {
                                super.onError(e);
                                baseActivity.dismissProgressBarDialog();
                                facebookOtpLoginActivity.showAlertDialog(facebookOtpLoginActivity.getString(R.string.error),
                                        getGeneralErrorMessage(),
                                        false,
                                        R.drawable.manis_logo,
                                        facebookOtpLoginActivity.getString(R.string.ok),
                                        (dialog, which) -> {
                                            facebookOtpLoginActivity.finish();
                                        });
                            }
                        }
                );
        subsConfirmCode.put(2, subsRegisterReward);
    }

    @Override
    public void loginRewardSdk(com.ebizu.manis.model.Account account, FacebookOtpLoginActivity facebookOtpLoginActivity) {
        EbizuReward.getSession(facebookOtpLoginActivity.getBaseContext()).login(account.getAccId(), account.getAccScreenName(),
                account.getEmail(), account.getAccCountry(), account.getAccCreatedDateTime(), new com.ebizu.sdk.reward.core.interfaces.Callback<SessionData>() {
                    @Override
                    public void onSuccess(SessionData sessionData) {
                        String rewardSession = sessionData.getValue();
                        facebookOtpLoginActivity.getRewardSession().setSession(rewardSession);
                        if (!baseActivity.getManisSession().isOtpRegistered()) {
                            signUpOtp(account, facebookOtpLoginActivity);
                        } else {
                            facebookOtpLoginActivity.onNextActivity(account, phoneNumber);
                        }
                    }

                    @Override
                    public void onFailure(String s) {
                        baseActivity.dismissProgressBarDialog();
                        facebookOtpLoginActivity.showAlertDialog(facebookOtpLoginActivity.getString(R.string.error),
                                getGeneralErrorMessage(),
                                false,
                                R.drawable.manis_logo,
                                facebookOtpLoginActivity.getString(R.string.ok),
                                (dialog, which) -> {
                                    facebookOtpLoginActivity.finish();
                                });
                    }
                });
    }

    private String fixPhoneNumber(String phoneNumber) {
        return UtilManis.replaceAllSymbols(phoneNumber);
    }

    private DataPhoneAuth getDataPhoneAuth(Phone phone) {
        DataPhoneAuth dataPhoneAuth = new DataPhoneAuth();
        dataPhoneAuth.setDataDevice(DeviceUtils.getDevice(baseActivity));
        dataPhoneAuth.setPhone(phone);
        return dataPhoneAuth;
    }

    private Phone getPhone(String phoneNumber) {
        Phone phone = new Phone();
        phone.setNumber(phoneNumber);
        return phone;
    }

    private String getGeneralErrorMessage() {
        return baseActivity.getResources().getString(ConnectionDetector.isNetworkConnected(baseActivity) ?
                R.string.server_busy : R.string.no_network_msg);
    }

    @Override
    public void releaseSubscribe(int key) {
        Subscription subscription = subsConfirmCode.get(key);
        if (subscription != null) {
            subscription.unsubscribe();
            subsConfirmCode.remove(key);
        }
    }

    @Override
    public void releaseAllSubscribe() {
        baseActivity.unSubscribeAll(subsConfirmCode);
    }
}
