package com.ebizu.manis.mvp.account.profile;

import android.util.SparseArray;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.rx.ResponseSubsriberLambda;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.response.WrapperAccount;
import com.ebizu.manis.service.manis.response.WrapperPoint;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by abizu-alvio on 7/14/2017.
 */

public class AccountProfilePresenter extends BaseViewPresenter implements IAccountProfilePresenter {

    private final String TAG = getClass().getSimpleName();

    private AccountProfileView accountProfileView;
    private SparseArray<Subscription> subsProfile;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        accountProfileView = (AccountProfileView) baseView;
        subsProfile = new SparseArray<>();
    }

    @Override
    public void releaseSubscribe(int key) {
        Subscription subscription = subsProfile.get(key);
        if (subscription != null) {
            subscription.unsubscribe();
            subsProfile.remove(key);
        }
    }

    @Override
    public void releaseAllSubscribes() {
        accountProfileView.unSubscribeAll(subsProfile);
    }

    @Override
    public void loadAccountProfile(ManisApi manisApi) {
        releaseSubscribe(0);
        Subscription subsAccountProfile = manisApi.getAccount()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperAccount>(accountProfileView) {
                    @Override
                    public void onNext(WrapperAccount wrapperAccount) {
                        super.onNext(wrapperAccount);
                        accountProfileView.setAccountProfile(wrapperAccount.getAccount());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                    }
                });
        subsProfile.put(0, subsAccountProfile);
    }

    @Override
    public void loadPointProfile(ManisApi manisApi) {
        releaseSubscribe(1);
        Subscription subsPoint = manisApi.getPoint()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperPoint>(accountProfileView) {
                    @Override
                    public void onNext(WrapperPoint wrapperPoint) {
                        super.onNext(wrapperPoint);
                        accountProfileView.setPointProfile(wrapperPoint.getPoint());
                        accountProfileView.getManisSession().setPoint(wrapperPoint.getPoint().getPoint());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                    }
                });
        subsProfile.put(1, subsPoint);
    }

    @Override
    public void loadAccountProfile() {
        releaseSubscribe(0);
        RequestBody requestBody = new RequestBodyBuilder(accountProfileView.getContext())
                .setFunction(ConfigManager.Account.FUNCTION_GET_ACCOUNT)
                .create();
        Subscription subsAccount = getManisApiV2().getAccount(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperAccount>(accountProfileView) {
                    @Override
                    protected void onSuccess(WrapperAccount wrapperAccount) {
                        super.onSuccess(wrapperAccount);
                        accountProfileView.setAccountProfile(wrapperAccount.getAccount());
                    }
                });
        subsProfile.put(0, subsAccount);
    }

    @Override
    public void loadPointProfile() {
        releaseSubscribe(1);
        RequestBody requestBody = new RequestBodyBuilder(accountProfileView.getContext())
                .setFunction(ConfigManager.Account.FUNCTION_GET_POINT)
                .create();
        Subscription subsPoint = getManisApiV2().getPoint(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperPoint>(accountProfileView) {
                    @Override
                    protected void onSuccess(WrapperPoint wrapperPoint) {
                        super.onSuccess(wrapperPoint);
                        accountProfileView.setPointProfile(wrapperPoint.getPoint());
                        accountProfileView.getManisSession().setPoint(wrapperPoint.getPoint().getPoint());
                    }
                });
        subsProfile.put(1, subsPoint);
    }
}