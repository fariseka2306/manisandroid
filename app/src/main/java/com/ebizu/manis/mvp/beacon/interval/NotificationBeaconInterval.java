package com.ebizu.manis.mvp.beacon.interval;

import com.ebizu.manis.model.notification.IntervalBeaconPromo;
import com.ebizu.manis.model.notification.NotificationTableList;

/**
 * Created by ebizu on 9/7/17.
 */

public class NotificationBeaconInterval {

    public static long SECOND = 1000;
    public static long MINUTE = SECOND * 60;
    public static long HOUR = MINUTE * 60;
    public static long DAY = HOUR * 24;

    public static long getInterval(IntervalBeaconPromo intervalBeaconPromo) {
        String typeInterval = intervalBeaconPromo.getType();
        int interval = intervalBeaconPromo.getInterval();
        for (IntervalInterface intervalInterface : notificationIntervals()) {
            if (intervalInterface.type().equals(typeInterval)) {
                return intervalInterface.interval(interval);
            }
        }
        return new DayInterval().interval(interval);
    }

    private static NotificationInterval[] notificationIntervals() {
        NotificationInterval[] notificationInterval = {
                new HourInterval(),
                new DayInterval()
        };
        return notificationInterval;
    }
}
