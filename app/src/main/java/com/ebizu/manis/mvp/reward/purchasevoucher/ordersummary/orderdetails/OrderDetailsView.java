package com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary.orderdetails;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.model.purchasevoucher.Product;
import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart.VoucherShoppingCart;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 11/16/17.
 */

public class OrderDetailsView extends BaseView {

    @BindView(R.id.text_view_title_order)
    TextView textViewTitleOrder;
    @BindView(R.id.text_view_amount)
    TextView textViewAmount;

    @BindView(R.id.view_list_items)
    LinearLayout llListReward;

    public OrderDetailsView(Context context) {
        super(context);
        createView(context);
    }

    public OrderDetailsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public OrderDetailsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public OrderDetailsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_order_details, null, false);
        addView(view, new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    public void setView(ShoppingCart shoppingCart, Order order) {
        Product product = order.getProduct();
        Account account = ManisSession.getInstance(getContext()).getAccountSession();
        String currencyIso2 = account.getCurrency().getIso2();
        String amount = currencyIso2.concat(" ").concat(
                UtilManis.addSeparator(account.getCurrency().getIso2(), product.getAmount())
        );

        textViewTitleOrder.setText(product.getName());
        textViewAmount.setText(amount);
        addVoucher(shoppingCart.getVouchers());
    }

    private void addVoucher(List<RewardVoucher> rewardVouchers) {
        if (!rewardVouchers.isEmpty()) {
            for (RewardVoucher rewardVoucher : rewardVouchers) {
                VoucherShoppingCart voucherView = new VoucherShoppingCart(getContext());
                voucherView.setValue(rewardVoucher);
                voucherView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
                voucherView.setInputTextColor(ContextCompat.getColor(getContext(), R.color.charCoalGrey));
                llListReward.addView(voucherView);
            }
        }
    }

}
