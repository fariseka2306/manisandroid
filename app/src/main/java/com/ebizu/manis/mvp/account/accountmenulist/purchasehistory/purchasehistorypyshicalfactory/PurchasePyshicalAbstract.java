package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistorypyshicalfactory;

import android.content.Context;

import com.ebizu.manis.model.PurchaseHistory;

/**
 * Created by halim_ebizu on 11/24/17.
 */

public abstract class PurchasePyshicalAbstract implements PurchasePyshicalStatus{

    protected PurchaseHistory purchaseHistory;
    protected Context context;

    public PurchasePyshicalAbstract(PurchaseHistory purchaseHistory, Context context) {
        this.purchaseHistory = purchaseHistory;
        this.context = context;
    }
}
