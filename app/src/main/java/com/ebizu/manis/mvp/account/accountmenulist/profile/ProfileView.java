package com.ebizu.manis.mvp.account.accountmenulist.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.mvp.account.accountmenulist.profile.changeprofilephoto.ChangeProfilePhotoActivity;
import com.ebizu.manis.mvp.interest.InterestActivity;
import com.ebizu.manis.mvp.phoneloginfacebookotp.FacebookOtpLoginActivity;
import com.ebizu.manis.mvp.snap.form.storedetail.ReceiptDetailActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbody.AccountBody;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.view.manis.textinput.AccountProfileAddressTextInput;
import com.ebizu.manis.view.manis.textinput.AccountProfileTextInput;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by abizu-alvio on 7/26/2017.
 */
public class ProfileView extends BaseView implements IProfileView {

    @BindView(R.id.imageview_photo)
    ImageView imageViewPhoto;
    @BindView(R.id.edittext_mobile)
    EditText editTextMobile;
    @BindView(R.id.textview_male)
    TextView textViewMale;
    @BindView(R.id.textview_female)
    TextView textViewFemale;

    @BindView(R.id.imageview_flagmobile)
    ImageView imageviewFlagMobile;
    @BindView(R.id.textview_flagmobile)
    TextView textviewFlagMobile;

    @BindView(R.id.text_account_input_name)
    AccountProfileTextInput textInputName;

    @BindView(R.id.text_account_input_email)
    AccountProfileTextInput textInputEmail;

    @BindView(R.id.text_account_input_birthdate)
    AccountProfileTextInput textInputBirthdate;

    @BindView(R.id.text_account_input_address)
    AccountProfileAddressTextInput textInputAddress;

    @BindView(R.id.text_account_input_address_state)
    AccountProfileAddressTextInput textInputAddressState;

    @BindView(R.id.text_account_input_address_town)
    AccountProfileAddressTextInput textInputAddressTown;

    @BindView(R.id.text_account_input_address_postcode)
    AccountProfileAddressTextInput textInputAddressPostcode;

    @BindView(R.id.spin_country)
    Spinner spinner;
    long timeBirthdate;
    private AccountProfileTextInput[] accountProfileTextInputs;
    private AccountProfileAddressTextInput[] accountProfileAddressTextInputs;
    private AccountBody accountBody;
    private Calendar calendar = Calendar.getInstance();
    private IProfilePresenter iProfilePresenter;
    private int updateProfileType = 0;
    private ReceiptStore receiptStore;
    private int selectedId;

    public ProfileView(Context context) {
        super(context);
        createView(context);
    }

    public ProfileView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ProfileView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ProfileView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    private void findViews() {
        spinner = (Spinner) this.findViewById(R.id.spin_country);
    }

    private void setCenterSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    TextView textView = (TextView) v.findViewById(android.R.id.text1);
                    textView.setText("");
                    textView.setHint(getItem(getCount())); //"Hint to be displayed"
                    textView.setTextAlignment(TEXT_ALIGNMENT_CENTER);
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1; // you dont display last item. It is used as hint.
            }
        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add("Indonesia");
        adapter.add("Malaysia");
        adapter.add("Select a country");

        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getCount());
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.account_menulist_profile_view, null, false);
        addView(view);
        accountBody = new AccountBody();
        ButterKnife.bind(this, view);
        setPropertiesName();
        setPropertiesEmail();
        setPropertiesBirthdate();
        setPropertiesAddress();
        findViews();
        setCenterSpinner();
        attachPresenter(new ProfilePresenter(context));
    }

    @Override
    public void setPropertiesName() {
        textInputName.setTitleAccount(getContext().getString(R.string.pf_edt_name));
    }

    @Override
    public void setPropertiesEmail() {
        textInputEmail.setTitleAccount(getContext().getString(R.string.pf_edt_email));
        textInputEmail.setImeOptionAccount(EditorInfo.IME_ACTION_DONE);
    }

    @Override
    public void setPropertiesBirthdate() {
        textInputBirthdate.setTitleAccount(getContext().getString(R.string.pf_edt_birthdate));
        textInputBirthdate.setOnAcccountProfileListener(getAnalyticManager(), this);
        textInputBirthdate.setImeOptionAccount(EditorInfo.IME_ACTION_DONE);
        textInputBirthdate.setKeyboardAccount(InputType.TYPE_NULL);
    }

    @Override
    public void setPropertiesAddress() {
        textInputAddress.setHintAccountAdrress(getContext().getString(R.string.pf_edt_address));
        textInputAddressTown.setHintAccountAdrress(getContext().getString(R.string.pf_edt_address_town));
        textInputAddressState.setHintAccountAdrress(getContext().getString(R.string.pf_edt_address_state));
        textInputAddressPostcode.setHintAccountAdrress(getContext().getString(R.string.pf_edt_address_postcode));
        textInputAddressPostcode.setKeyboardAccountAddress(InputType.TYPE_CLASS_NUMBER);
        textInputAddressPostcode.setImeOptionAccountAddress(EditorInfo.IME_ACTION_DONE);
        textInputAddress.setLengthAccountAddress(128, false);
        textInputAddressTown.setLengthAccountAddress(50, true);
        textInputAddressState.setLengthAccountAddress(50, true);
        textInputAddressPostcode.setLengthAccountAddress(10, true);

    }

    public void setBirthDate(String birthDate) {
        accountBody.setBirthdate(birthDate);
    }


    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        this.iProfilePresenter = (IProfilePresenter) iBaseViewPresenter;
        this.iProfilePresenter.attachView(this);
    }

    @Override
    public void showProfileData(Account account) {
        ImageUtils.loadImage(getContext(), account.getAccPhoto(), imageViewPhoto);
        setInputName(account);
        setInputEmail(account);
        setInputBirthdate(account);
        setInputCountry(account);
        setInputMobile(account);
        setInputAddresss(account);
        setInputGender(account);
    }

    @Override
    public void setInputAddresss(Account account) {
        textInputAddress.setInputAccountAdress(account.getAccAddress());
        textInputAddressPostcode.setInputAccountAdress(account.getAciPostcode());
        textInputAddressTown.setInputAccountAdress(account.getAciCity());
        textInputAddressState.setInputAccountAdress(account.getAciState());
    }

    @Override
    public void setInputGender(Account account) {
        if (account.getAccGender().equalsIgnoreCase("male")) {
            textViewMale.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));
            accountBody.setGender("male");
        } else if (account.getAccGender().equalsIgnoreCase("female")) {
            textViewFemale.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));
            accountBody.setGender("female");
        } else {
            accountBody.setGender("");
        }
    }

    @Override
    public void setInputMobile(Account account) {
        editTextMobile.setEnabled(account.isUpdateablePhone());
        editTextMobile.setText(UtilManis.convertPhoneNumber(account));
    }

    @Override
    public void setInputCountry(Account account) {
        if (!account.getAccCountry().isEmpty()) {
            if (account.getAccMsisdn().isEmpty()) {
                spinner.setEnabled(true);
            } else {
                spinner.setEnabled(false);
            }
            if (account.getAccCountry().contains("ID")) {
                selectedId = 0;
                updateFlagViewIndo();
            } else if (account.getAccCountry().contains("MY")) {
                selectedId = 1;
                updateFlagViewMalay();
            }
            spinner.setSelection(selectedId);
        } else {
            spinner.setEnabled(true);
            imageviewFlagMobile.setVisibility(INVISIBLE);
            textviewFlagMobile.setVisibility(INVISIBLE);
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setTextAlignment(TEXT_ALIGNMENT_CENTER);
                selectedId = position;
                textviewFlagMobile.setText(position == 0 ? "+62" : "+60");
                if (position == 0) {
                    updateFlagViewIndo();
                    imageviewFlagMobile.setVisibility(VISIBLE);
                    textviewFlagMobile.setVisibility(VISIBLE);
                } else if (position == 1) {
                    updateFlagViewMalay();
                    imageviewFlagMobile.setVisibility(VISIBLE);
                    textviewFlagMobile.setVisibility(VISIBLE);
                }
                parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setInputBirthdate(Account account) {
        if (account.getAccBirthdate() == 0) {
            textInputBirthdate.setInputAccount("");
        } else {
            timeBirthdate = account.getAccBirthdate();
            textInputBirthdate.setInputAccount(getDate(timeBirthdate));
            accountBody.setBirthdate(String.valueOf(timeBirthdate));
        }
    }

    @Override
    public void setInputName(Account account) {
        textInputName.setInputAccount(account.getAccScreenName());
    }

    @Override
    public void setInputEmail(Account account) {
        if (account.getEmail().equals("No email")) {
            textInputEmail.setHintAccount(account.getEmail());
        } else {
            textInputEmail.setInputAccount(account.getEmail());
        }
        textInputEmail.setEnableAccount(account.isUpdateableEmail());
    }

    @Override
    public void updatePhotoProfile(Account account) {
        ImageUtils.loadImage(getContext(), account.getAccPhoto(), imageViewPhoto);
    }

    @Override
    public void updateFlagViewIndo() {
        imageviewFlagMobile.getLayoutParams().height = UtilManis.dpToPx(19);
        imageviewFlagMobile.getLayoutParams().width = UtilManis.dpToPx(28);
        imageviewFlagMobile.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.flag_indonesia));
    }

    @Override
    public void updateFlagViewMalay() {
        imageviewFlagMobile.getLayoutParams().height = UtilManis.dpToPx(19);
        imageviewFlagMobile.getLayoutParams().width = UtilManis.dpToPx(38);
        imageviewFlagMobile.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.flag_malaysia));
    }

    @Override
    public IProfilePresenter getProfilePresenter() {
        return iProfilePresenter;
    }

    @OnClick(R.id.edittext_mobile)
    void onClickInputMobile() {
        getBaseActivity().showAlertDialog(getContext().getString(R.string.app_name),
                getContext().getString(R.string.verify_phone), true,
                R.drawable.manis_logo, getContext().getString(R.string.text_ok),
                (dialogInterface, i) -> FacebookOtpLoginActivity.start(getBaseActivity(),
                        ConfigManager.Otp.FORCE_SIGN_UP_OTP), getContext().getString(R.string.btn_cancel), null);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.PROFILE_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Edittext Phone");
    }

    @OnClick(R.id.textview_male)
    void setMale() {
        accountBody.setGender("male");
        textViewMale.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));
        textViewFemale.setTextColor(ContextCompat.getColor(getContext(), R.color.colorInputTextGray));
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.PROFILE_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Male");
    }

    @OnClick(R.id.text_account_input_address)
    void setEdittextAddress() {
        textInputBirthdate.getText();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.PROFILE_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "EditText Address");
    }


    @OnClick(R.id.textview_female)
    void setFemale() {
        accountBody.setGender("female");
        textViewFemale.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));
        textViewMale.setTextColor(ContextCompat.getColor(getContext(), R.color.colorInputTextGray));
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.PROFILE_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Female");
    }

    private String getDate(long time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        Date date = new Date(time * 1000L);
        return dateFormat.format(date);
    }

    @OnClick(R.id.lin_save)
    void onSave() {
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.PROFILE_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Save");
        if (textInputName.getText().equals("Customer Name N/A")) {
            showNotificationMessage(getContext().getString(R.string.error_name));
            YoYo.with(Techniques.Shake).duration(500).playOn(textInputName);
            return;
        }
        if (textInputEmail.getText().isEmpty()) {
            Toast.makeText(getContext(), getContext().getString(R.string.error_valid_email), Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(500).playOn(textInputEmail);
            return;
        }
        if (!UtilManis.isValidEmail(textInputEmail.getText())) {
            Toast.makeText(getContext(), getContext().getString(R.string.error_valid_email), Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(500).playOn(textInputEmail);
            return;
        }
        if (textInputName.getText().trim().length() == 0) {
            Toast.makeText(getContext(), getContext().getString(R.string.error_empty_name), Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(500).playOn(textInputName);
            return;
        }
        if (textInputBirthdate.getText().trim().length() == 0) {
            Toast.makeText(getContext(), getContext().getString(R.string.error_empty_birthdate), Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).duration(500).playOn(textInputBirthdate);
            return;
        }

        accountBody.setName(String.valueOf(textInputName.getText()));
        accountBody.setMsisdn(getBaseActivity().getManisSession().getAccountSession().getAccMsisdn());
        accountBody.setEmail(String.valueOf(textInputEmail.getText()));
        accountBody.setAddress(String.valueOf(textInputAddress.getText()));
        accountBody.setCity(String.valueOf(textInputAddressTown.getText()));
        accountBody.setState(String.valueOf(textInputAddressState.getText()));
        accountBody.setPostcode(String.valueOf(textInputAddressPostcode.getText()));
        accountBody.setCountry(String.valueOf(spinner.getSelectedItem().toString()));
        RequestBody requestBody = new RequestBodyBuilder(getContext())
                .setFunction(ConfigManager.Account.FUNCTION_UPDATE_PROFILE)
                .setData(accountBody)
                .create();
        getProfilePresenter().saveProfileData(requestBody);
    }

    public void finishActivity() {
        if (updateProfileType == ConfigManager.UpdateProfile.UPDATE_PROFILE_LUCKY_DRAW_TYPE) {
            Intent intent = new Intent(getContext(), ReceiptDetailActivity.class);
            intent.putExtra(ConfigManager.Snap.MERCHANT_DATA, receiptStore);
            intent.putExtra(ConfigManager.Snap.LUCKY_DRAW_INTENT_DATA, true);
            getBaseActivity().startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
        } else if (updateProfileType == ConfigManager.UpdateProfile.UPDATE_PROFILE_LOGIN_TYPE) {
            Intent intent = new Intent(getContext(), InterestActivity.class);
            getBaseActivity().startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
            getBaseActivity().finish();
        } else {
            getBaseActivity().finish();
        }
    }

    @OnClick(R.id.imageview_photo)
    void clickPhotoProfile() {
        boolean permissionIsAllow = getBaseActivity().getPermissionManager().checkPermissionCamera();
        if (permissionIsAllow)
            changeActivity();
    }

    public void changeActivity() {
        Intent intent = new Intent(getContext(), ChangeProfilePhotoActivity.class);
        getBaseActivity().startActivityForResult(intent, ConfigManager.UpdateProfile.CHANGE_PROFILE_PICTURE_REQ_CODE);
    }

    public void setUpdateProfileType(int updateProfileType) {
        this.updateProfileType = updateProfileType;
    }

    public void setReceiptStore(ReceiptStore receiptStore) {
        this.receiptStore = receiptStore;
    }
}
