package com.ebizu.manis.mvp.snap.receipt.upload;

import com.ebizu.manis.model.LuckyDrawUpload;
import com.ebizu.manis.root.IBaseActivity;

/**
 * Created by ebizu on 8/21/17.
 */

public interface IReceiptUploadActivity extends IBaseActivity {

    void onSuccessUpload(boolean isLuckyDraw);

    void onSuccessUpload(boolean isLuckyDraw, LuckyDrawUpload luckyDrawUpload);

    void showUploadSnapEarnPoint();

    void showUploadSnapEarnTicket(LuckyDrawUpload luckyDrawUpload);

}