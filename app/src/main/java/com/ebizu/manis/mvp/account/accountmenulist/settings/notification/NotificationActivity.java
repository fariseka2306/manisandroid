package com.ebizu.manis.mvp.account.accountmenulist.settings.notification;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.snap.store.list.near.INearStorePresenter;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/21/2017.
 */

public class NotificationActivity extends BaseActivity {

    @Inject
    Context context;

    @Inject
    ManisApi manisApi;

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;

    @BindView(R.id.recycler_notification)
    NotificationsView notificationsView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings_notifications);
        ButterKnife.bind(this);
        initView();
    }


    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {
        toolbarView.setTitle(R.string.notifications);
        notificationsView.setActivity(this);
        notificationsView.attachPresenter(new NotificationsPresenter(context));
        INotificationsPresenter notificationsPresenter = notificationsView.getNotificationPresenter();
        notificationsPresenter.loadViewNotifications();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finishActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
