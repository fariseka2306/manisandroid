package com.ebizu.manis.mvp.snap.form.receiptdetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.LinearLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.mvp.snap.form.FormReceiptActivity;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;
import com.ebizu.manis.view.dialog.snaptnc.SnapEarnPointTncDialog;
import com.ebizu.manis.view.dialog.snaptnc.SnapEarnPointTncPresenter;
import com.ebizu.manis.view.dialog.snaptnc.SnapEarnTicketTncDialog;
import com.ebizu.manis.view.dialog.snaptnc.SnapEarnTicketTncPresenter;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ebizu-User on 09/08/2017.
 */

public class SnapStoreDetailActivity extends FormReceiptActivity {

    @BindView(R.id.toolbar_view)
    ToolbarView toolbarView;
    @BindView(R.id.form_store_detail_view)
    FormStoreDetailView formStoreDetailView;
    @BindView(R.id.sma_lin_next)
    LinearLayout buttonNext;

    @Inject
    ManisSession manisSession;
    @Inject
    SnapEarnPointTncPresenter snapEarnPointTncPresenter;
    @Inject
    SnapEarnTicketTncPresenter snapEarnTicketTncPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideInHorizontalAnim();
        setContentView(R.layout.activity_snap_store_detail);
        ButterKnife.bind(this);
        toolbarView.setTitle(getResources().getString(R.string.sma_txt_title));
        loadTerms();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void loadTerms() {
        snapEarnPointTncPresenter.attachDialog(new SnapEarnPointTncDialog(this));
        snapEarnTicketTncPresenter.attachDialog(new SnapEarnTicketTncDialog(this));
        snapEarnPointTncPresenter.getTermsAndSave();
        snapEarnTicketTncPresenter.getTermsAndSave();
    }

    @Override
    public void finish() {
        super.finish();
        slideOutHorizontalAnim();
    }

    @OnClick(R.id.sma_lin_next)
    void onClickNext() {
        if (formStoreDetailView.isValidInput()) {
            showHelpDialog();
        } else {
            formStoreDetailView.showNotificationMessage(getString(R.string.txt_input_validation_msg));
        }
    }

    private void showHelpDialog() {
        if (isSnapLuckyDraw()) {
            showLuckyDrawHelpDialog();
        } else {
            showSnapHelpDialog();
        }
    }

    private void showSnapHelpDialog() {
        if (!manisSession.getTermsSnapEarnPoints().isEmpty()) {
            if (!manisSession.isAcceptAlwaysTncSnap()) {
                SnapEarnPointTncDialog snapEarnPointTncDialog = new SnapEarnPointTncDialog(this);
                snapEarnPointTncDialog.setActivity(this);
                snapEarnPointTncDialog.setOnAcceptListener(this::startCameraActivity);
                snapEarnPointTncDialog.show();
            } else {
                startCameraActivity();
            }
        } else {
            startCameraActivity();
        }
    }

    private void showLuckyDrawHelpDialog() {
        if (!manisSession.getTermsSnapEarnTickets().isEmpty()) {
            if (!manisSession.isAcceptAlwaysTncLuckyDraw()) {
                SnapEarnTicketTncDialog snapEarnTicketTncDialog = new SnapEarnTicketTncDialog(this);
                snapEarnTicketTncDialog.setActivity(this);
                snapEarnTicketTncDialog.setOnAcceptListener(this::startCameraActivity);
                snapEarnTicketTncDialog.show();
            } else {
                startCameraActivity();
            }
        } else {
            startCameraActivity();
        }
    }

    private void startCameraActivity() {
        if (getPermissionManager().checkPermissionCamera()) {
            ReceiptStore receiptStore = createReceiptStore(formStoreDetailView.storeName(), formStoreDetailView.storeLocation());
            startActivityCamera(receiptStore, Integer.valueOf(formStoreDetailView.totalAmount()));
        }
    }
}
