package com.ebizu.manis.mvp.splashscreen;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseActivityPresenter;

import javax.inject.Inject;

import io.fabric.sdk.android.services.common.CommonUtils;

/**
 * Created by Ebizu-User on 06/07/2017.
 */

public class SplashScreenActivityPresenter extends BaseActivityPresenter implements ISplashScreenActivityPresenter {

    private SplashScreenActivity splashScreenView;
    private boolean rootedDevice = false;
    private boolean emulatorDevice = false;
    private boolean exManisApp = false;

    @Inject
    public SplashScreenActivityPresenter() {
    }

    @Override
    public void attachView(BaseActivity view) {
        super.attachView(view);
        splashScreenView = (SplashScreenActivity) view;
    }

    @Override
    public void releaseSubscribe(int key) {
        // not implemented method
    }

    @Override
    public void releaseAllSubscribe() {
        // not implemented method
    }

    @Override
    public boolean checkAllRequire() {
        boolean emulatorDebugable = splashScreenView.getResources().getBoolean(R.bool.emulatorDebugable);
        if (emulatorDebugable) {
            return true;
        } else {
            checkDeviceRooted();
            checkDeviceEmulator();
            checkExManisApp();
            if (!rootedDevice) if (!emulatorDevice) if (!exManisApp) return true;
        }
        return false;
    }

    @Override
    public void checkDeviceEmulator() {
        emulatorDevice = CommonUtils.isEmulator(getContext());
        if (emulatorDevice)
            splashScreenView.showDialogDeviceBlock();
    }

    @Override
    public void checkDeviceRooted() {
        rootedDevice = CommonUtils.isRooted(getContext());
        if (rootedDevice)
            splashScreenView.showDialogDeviceBlock();
    }

    @Override
    public void checkExManisApp() {
        exManisApp = BuildConfig.APPLICATION_ID.equals(UtilManis.exManis);
        if (exManisApp)
            splashScreenView.showDialogManisPlaystore();
    }

}