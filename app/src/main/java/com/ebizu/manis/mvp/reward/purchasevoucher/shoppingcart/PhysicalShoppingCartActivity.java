package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

/**
 * Created by andrifashbir on 15/11/17.
 */

public class PhysicalShoppingCartActivity extends ShoppingCartActivity {

    @Override
    public ShoppingCartView getBaseView() {
        return physicalCartView;
    }
}
