package com.ebizu.manis.mvp.notification.item;

import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;
import com.ebizu.manis.database.litepal.NotificationDatabase;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.view.holder.NotificationSwipeViewHolder;

/**
 * Created by ebizu on 8/28/17.
 */

public class NotificationSnap extends com.ebizu.manis.mvp.notification.item.NotificationItem implements NotificationInterface {

    private final String STATUS_APPROVED = "approved";
    private final String STATUS_REJECTED = "rejected";


    public NotificationSnap(NotificationTableList notificationTableList, NotificationSwipeViewHolder notificationSwipeViewHolder) {
        super(notificationTableList, notificationSwipeViewHolder);

    }

    @Override
    public String notificationType() {
        return NotificationDatabase.TYPE_SNAP;
    }

    @Override
    public void setNotificationView() {
        notificationSwipeViewHolder.setTitle(notificationTableList.getSnapMessage());
        long savedTime = notificationTableList.getSavedTime();
        notificationSwipeViewHolder.setTime(UtilManis.getTimeAgo(savedTime, notificationSwipeViewHolder.getContext()));
        setIconSnapStatus();
        notificationTableList.setRead(true);
        notificationTableList.save();
    }

    private void setIconSnapStatus() {
        if (notificationTableList.getSnapStatus().equalsIgnoreCase(STATUS_APPROVED)) {
            notificationSwipeViewHolder.setItemIconDrawable(ContextCompat.getDrawable(notificationSwipeViewHolder.getContext(), R.drawable.snap_history_accepted));
        } else if (notificationTableList.getSnapStatus().equalsIgnoreCase(STATUS_REJECTED)) {
            notificationSwipeViewHolder.setItemIconDrawable(ContextCompat.getDrawable(notificationSwipeViewHolder.getContext(), R.drawable.snap_history_rejected));
        }
    }
}
