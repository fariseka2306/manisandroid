package com.ebizu.manis.mvp.snap.store.list.searchresult;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.snap.SnapStoreResult;
import com.ebizu.manis.mvp.snap.store.list.IListSnapStorePresenter;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.manis.nodatastorefound.NoDataStoreFoundView;
import com.ebizu.manis.view.manis.recyclerview.SnapStoreRecyclerView;
import com.ebizu.manis.view.manis.swiperefresh.SwipeRefreshManis;
import com.ebizu.manis.view.nestedscroll.NestedScrollChangeListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 08/08/2017.
 */

public class SearchResultView extends BaseView
        implements ISearchResultView {

    private SearchResultPresenter searchResultPresenter;
    private SnapStoreRecyclerView snapStoreRecyclerView;

    @BindView(R.id.swipe_refresh_manis)
    SwipeRefreshManis swipeRefresh;
    @BindView(R.id.progress_bar_indeterminate)
    ProgressBar progressBar;
    @BindView(R.id.text_view_title)
    TextView textViewTitle;

    private String keyword;

    private int page = 1;
    private boolean loading;
    private boolean lastPage;

    public SearchResultView(Context context) {
        super(context);
        createView(context);
    }

    public SearchResultView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public SearchResultView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SearchResultView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_search_result_merchant, null, false);
        addView(view, new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT)
        );
        ButterKnife.bind(this, view);
        addRecyclerView();
        addSwipeRefresh();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        searchResultPresenter = (SearchResultPresenter) iBaseViewPresenter;
        searchResultPresenter.attachView(this);
    }

    private void addRecyclerView() {
        snapStoreRecyclerView = new SnapStoreRecyclerView(getContext());
        swipeRefresh.addViewToSwipe(snapStoreRecyclerView,
                new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
    }

    private void addSwipeRefresh() {
        swipeRefresh.setOnRefreshListener(() -> {
            setPage(1);
            getSearchResultPresenter().findMerchant(keyword, page, LoadType.SWIPE_LOAD);
        });
    }

    @Override
    public void addSnapStores(SnapStoreResult snapStoreResult, LoadType loadType) {
        setLastPage(!snapStoreResult.getMore());
        switch (loadType) {
            case SEARCH_LOAD: {
                snapStoreRecyclerView.replaceData(snapStoreResult.getStores());
                break;
            }
            case SCROLL_LOAD: {
                snapStoreRecyclerView.addData(snapStoreResult.getStores());
                break;
            }
            case SWIPE_LOAD: {
                snapStoreRecyclerView.replaceData(snapStoreResult.getStores());
                break;
            }
        }
        setLoading(false);
        setViewStore();
    }

    private void setViewStore() {
        if (snapStoreRecyclerView.getSnapStoreAdapter().getSnapStores().size() <= 0) {
            snapStoreRecyclerView.setVisibility(GONE);
            invisibleTitle();
            showNoDataStoreView(NoDataStoreFoundView.Type.SEARCH_VIEW);
        } else if (!snapStoreRecyclerView.isShown()) {
            dismissGpsDeactivateView();
            visibleTitle();
            snapStoreRecyclerView.setVisibility(VISIBLE);
        }
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        snapStoreRecyclerView.initialize(baseActivity);
        addScrollListener();
    }

    @Override
    public void setOnSnapAbleListener(IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener) {
        snapStoreRecyclerView.setOnSnapAbleListener(onCheckSnapAbleListener);
    }

    private void addScrollListener() {
        swipeRefresh.getNestedScrollView().setOnScrollChangeListener(new NestedScrollChangeListener(
                snapStoreRecyclerView.getLinearLayoutManager(), snapStoreRecyclerView, swipeRefresh) {
            @Override
            protected void onLoadMore() {
                setPage(page + 1);
                getSearchResultPresenter().findMerchant(keyword, page, LoadType.SCROLL_LOAD);
            }

            @Override
            protected boolean isLoading() {
                return loading;
            }

            @Override
            protected boolean isLastPage() {
                return lastPage;
            }
        });
    }

    @Override
    public SearchResultPresenter getSearchResultPresenter() {
        return searchResultPresenter;
    }

    public void visibleTitle() {
        textViewTitle.setVisibility(VISIBLE);
    }

    public void invisibleTitle() {
        textViewTitle.setVisibility(INVISIBLE);
    }

    public SnapStoreRecyclerView getSnapStoreRecyclerView() {
        return snapStoreRecyclerView;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public boolean isLoading() {
        return loading;
    }
}
