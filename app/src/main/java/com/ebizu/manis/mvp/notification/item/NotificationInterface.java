package com.ebizu.manis.mvp.notification.item;

/**
 * Created by ebizu on 8/28/17.
 */

public interface NotificationInterface {

    String notificationType();

    void setNotificationView();
}
