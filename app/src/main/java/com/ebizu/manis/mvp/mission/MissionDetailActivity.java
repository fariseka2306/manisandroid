package com.ebizu.manis.mvp.mission;

import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.manager.tracker.TrackerConstant;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.missions.Missions;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.mvp.mission.missioncheckavailability.MissionCheckAvailabilityPresenter;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperienceCheckAvalaibilityRB;
import com.ebizu.manis.service.manis.requestbodyv2.data.ShareExperienceCheckAvalaibilityData;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperMissionExecute;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperShareExperienceCheckAvalaibility;
import com.ebizu.manis.view.dialog.missionsdialog.MissionSuccessDialog;
import com.ebizu.manis.view.dialog.missionsdialog.MissionTncDialog;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public abstract class MissionDetailActivity extends BaseActivity {

    public static String EXTRA_SHARE_EXPERIENCE_DETAIL = "detail";
    public Missions mission;
    @BindView(R.id.button_execute)
    public Button buttonExecute;
    @BindView(R.id.pb_execute)
    public ProgressBar pbExecute;
    @BindView(R.id.toolbar)
    ToolbarView toolbarView;
    @BindView(R.id.imageview_banner)
    ImageView imageViewBanner;
    @BindView(R.id.imageview_icon)
    ImageView imageViewIcon;
    @BindView(R.id.iv_mission_type)
    ImageView imageViewMissionType;
    @BindView(R.id.textview_point)
    TextView textViewPoint;
    @BindView(R.id.textview_available_date)
    TextView textViewAvailableDate;
    @BindView(R.id.textview_mission_name)
    TextView textViewMissionName;
    @BindView(R.id.textview_mission_desc)
    TextView tvMissionDesc;
    @BindView(R.id.textview_mission_type)
    TextView tvMissionType;
    @BindView(R.id.textview_mission_tc)
    TextView tvMissionTC;
    private String textButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_share_experience_detail);
        ButterKnife.bind(this);
        if (getIntent().hasExtra(EXTRA_SHARE_EXPERIENCE_DETAIL)) {
            mission = getIntent().getParcelableExtra(EXTRA_SHARE_EXPERIENCE_DETAIL);
        } else {
            throw new IllegalArgumentException("Detail activity must receive a movie parcelable");
        }
        initView();
        checkAvailability();
    }

    private void initView() {
        toolbarView.setTitle(getResources().getString(R.string.mission_details));
        setProgressBarColor();

        ImageUtils.loadFitCenterImage(this, mission.getAsset().getBanner(), ContextCompat.getDrawable(this, R.drawable.default_pic_store_logo), imageViewBanner);
        ImageUtils.loadImage(this, mission.getAsset().getIcon(), ContextCompat.getDrawable(this, R.drawable.default_pic_store_logo), imageViewIcon);
        ImageUtils.loadImage(this, mission.getAsset().getIconMission(), ContextCompat.getDrawable(this, R.drawable.default_pic_store_logo), imageViewMissionType);

        String form = mission.getIncentive().getForm();
        String formValidation = mission.getIncentive().getForm().toLowerCase();
        if (formValidation.contains(getResources().getString(R.string.fa_txt_points)) || formValidation.contains("point")) {
            textViewPoint.setText("+ " + String.valueOf(mission.getIncentive().getAmount()) + " " + form);
        } else {
            textViewPoint.setText(String.valueOf(mission.getIncentive().getAmount()) + " " + form);
        }

        if (mission.getMissionDescription() != null) {
            if (!mission.getMissionDescription().isEmpty()) {
                tvMissionDesc.setVisibility(View.VISIBLE);
                tvMissionDesc.setText(mission.getMissionDescription());
            }
        }
        textViewMissionName.setText(mission.getMissionName());
        textViewAvailableDate.setText(String.valueOf(mission.getStartDateDescription()) + " " + getDate(mission.getStartDate())
                + " " + mission.getEndDateDescription() + " " + getDate(mission.getEndDate()));
        tvMissionTC.setPaintFlags(tvMissionTC.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        setMissionTypeText();
    }

    private void setMissionTypeText() {
        tvMissionType.setText(mission.getMissionTypeName());
        tvMissionType.getViewTreeObserver().addOnPreDrawListener(() -> {
            if (tvMissionType.getWidth() > (UtilManis.getScreenWidth(baseContext()) / 2) - imageViewMissionType.getWidth()) {
                int tvMissionTypeWitdh = (UtilManis.getScreenWidth(baseContext()) / 2) - imageViewMissionType.getWidth();
                tvMissionType.setWidth(tvMissionTypeWitdh);
                tvMissionType.setTextAlignment(TextView.TEXT_ALIGNMENT_TEXT_START);
            }
            return true;
        });
    }

    private void setProgressBarColor() {
        pbExecute.getIndeterminateDrawable().setColorFilter(
                ContextCompat.getColor(this, R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }

    private String getDate(long time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        Date date = new Date(time * 1000L);
        return dateFormat.format(date);
    }

    public void setButtonText(String string) {
        textButton = string;
    }

    @OnClick(R.id.button_execute)
    public void executeMission() {
    }

    @OnClick(R.id.textview_mission_tc)
    public void showDialogTnc() {
        MissionTncDialog missionTncDialog = new MissionTncDialog(this);
        missionTncDialog.show();
        missionTncDialog.setActivity(this);
        missionTncDialog.setMissions(mission);
        TrackerManager.getInstance().setTrackerData(
                new TrackerStandartRequest(TrackerConstant.kTrackerOriginPageMissions, TrackerConstant.kTrackerPageMissionTandC, TrackerConstant.kTrackerComponentButton, TrackerConstant.kTrackerSubFeatureTandC, "", "")
        );
    }

    public void showDialogSuccessMission(WrapperMissionExecute wrapperMissionExecute) {
        String form = wrapperMissionExecute.getMissionsPost().getIncentive().getForm();
        if (form.equalsIgnoreCase(getResources().getString(R.string.fa_txt_points))) {
            ManisSession manisSession = new ManisSession(this);
            int point = wrapperMissionExecute.getMissionsPost().getIncentive().getAmount();
            manisSession.setPoint(manisSession.getPointSession().getPoint() + point);
        }
        MissionSuccessDialog missionSuccessDialog = new MissionSuccessDialog(this);
        missionSuccessDialog.setWrapperMissionPost(wrapperMissionExecute);
        missionSuccessDialog.setMissions(mission);
        missionSuccessDialog.setActivity(this);
        missionSuccessDialog.show();
    }

    private void checkAvailability() {
        MissionCheckAvailabilityPresenter missionCheckAvailabilityPresenter = new MissionCheckAvailabilityPresenter(this);
        ShareExperienceCheckAvalaibilityData data = new ShareExperienceCheckAvalaibilityData(this, mission.getMissionId());
        data.setVoucherId(mission.getVoucherId());
        ShareExperienceCheckAvalaibilityRB requestBody = new ShareExperienceCheckAvalaibilityRB();
        requestBody.setData(data);
        missionCheckAvailabilityPresenter.checkAvailability(this, requestBody);
    }

    public void setupButton(WrapperShareExperienceCheckAvalaibility availability) {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            pbExecute.setVisibility(View.GONE);
            if (availability.getShareExperienceCheckAvalaibilityList().getAvailability() == 1) {
                buttonExecute.setEnabled(true);
            }
            buttonExecute.setText(textButton);
        }, 1500);
    }

    public void setupErrorButton(Throwable e) {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            pbExecute.setVisibility(View.GONE);
            buttonExecute.setText(textButton);
            NegativeScenarioManager.show(e, this, NegativeScenarioManager.NegativeView.DIALOG);
        }, 1500);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finishActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
