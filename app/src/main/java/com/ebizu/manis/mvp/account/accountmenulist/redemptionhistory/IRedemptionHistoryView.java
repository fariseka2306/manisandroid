package com.ebizu.manis.mvp.account.accountmenulist.redemptionhistory;

import com.ebizu.manis.model.RedemptionHistoryResult;

import java.util.List;

/**
 * Created by abizu-alvio on 8/7/2017.
 */

public interface IRedemptionHistoryView {

    void setRedemptionHistory(List<RedemptionHistoryResult> redemptionHistory);

    void setRedemptionHistoryNext(List<RedemptionHistoryResult> redemptionHistoryNext);

    IRedemptionHistoryPresenter getRedemptionHistoryPresenter();

}
