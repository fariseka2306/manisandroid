package com.ebizu.manis.mvp.store.storedetail;

import android.util.SparseArray;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.snap.SnapableLuckyDraw;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.StoreDetailBody;
import com.ebizu.manis.service.manis.requestbody.StorePinBody;
import com.ebizu.manis.service.manis.requestbody.snap.SnapableRequestBody;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.WrapperPin;
import com.ebizu.manis.service.manis.response.WrapperSnapable;
import com.ebizu.manis.service.manis.response.WrapperStoreDetail;
import com.ebizu.manis.service.manis.response.WrapperUnpin;
import com.google.gson.Gson;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by firef on 7/14/2017.
 */

public class StoreDetailPresenter extends BaseViewPresenter implements IStoreDetailPresenter {

    private final String TAG = getClass().getSimpleName();

    private StoreDetailView storeDetailView;
    private SparseArray<Subscription> subsStoreDetail;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        storeDetailView = (StoreDetailView) baseView;
        subsStoreDetail = new SparseArray<>();
    }

    @Override
    public void releaseSubscribe(int key) {
        Subscription subscription = subsStoreDetail.get(key);
        if (subscription != null) {
            subscription.unsubscribe();
            subsStoreDetail.remove(key);
        }
    }

    @Override
    public void releaseAllSubscribes() {
        storeDetailView.unSubscribeAll(subsStoreDetail);
    }

    @Override
    public void loadStoreDetailData(ManisApi manisApi, Store store) {
        releaseSubscribe(0);
        storeDetailView.getBaseActivity().showProgressBarDialog(storeDetailView.getResources().
                getString(R.string.please_wait));
        StoreDetailBody storeDetailBody = new StoreDetailBody();
        storeDetailBody.setId(store.getId());
        Subscription subsLoadStoreDetail = manisApi.getStoreDetail(new Gson().toJson(storeDetailBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperStoreDetail>(storeDetailView) {
                    @Override
                    public void onNext(WrapperStoreDetail wrapperStoreDetail) {
                        super.onNext(wrapperStoreDetail);
                        storeDetailView.getBaseActivity().dismissProgressBarDialog();
                        storeDetailView.loadStoreDetailData(store, wrapperStoreDetail.getStoreDetail());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        storeDetailView.getBaseActivity().dismissProgressBarDialog();
                    }
                });
        subsStoreDetail.put(0, subsLoadStoreDetail);
    }

    @Override
    public void postStoreDetailFollower(ManisApi manisApi, Store store) {
        releaseSubscribe(1);
        StorePinBody storePinBody = new StorePinBody();
        storePinBody.setId(store.getId());
        Subscription subsPostStoreDetailFollowers = manisApi.getPinnedFollower(new Gson().toJson(storePinBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperPin>(storeDetailView) {
                    @Override
                    public void onNext(WrapperPin wrapperPin) {
                        super.onNext(wrapperPin);
                        storeDetailView.setFollowerData(wrapperPin);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                    }
                });
        subsStoreDetail.put(1, subsPostStoreDetailFollowers);
    }

    @Override
    public void postStoreDetailUnfollow(ManisApi manisApi, Store store) {
        releaseSubscribe(2);
        StorePinBody storePinBody = new StorePinBody();
        storePinBody.setId(store.getId());
        Subscription subsPostStoreDetailUnfollow = manisApi.getUnpinnedFollower(new Gson().toJson(storePinBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperUnpin>(storeDetailView) {
                    @Override
                    public void onNext(WrapperUnpin wrapperUnpin) {
                        super.onNext(wrapperUnpin);
                        storeDetailView.setUnFollowerData(wrapperUnpin);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                    }
                });
        subsStoreDetail.put(2, subsPostStoreDetailUnfollow);
    }

    @Override
    public void isSnapable(Store store) {
        releaseSubscribe(3);
        storeDetailView.getBaseActivity().showProgressBarDialog("Please wait...", dialogInterface -> {
            releaseSubscribe(3);
        });
        SnapableRequestBody snapableRequestBody = new SnapableRequestBody();
        snapableRequestBody.setComId(store.getId());
        snapableRequestBody.setMerchantTier(store.getMerchantTier());
        Subscription subsIsSnapable = getManisApi().isSnapable(new Gson().toJson(snapableRequestBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperSnapable>(storeDetailView) {
                    @Override
                    public void onNext(WrapperSnapable wrapperSnapable) {
                        super.onNext(wrapperSnapable);
                        storeDetailView.getBaseActivity().dismissProgressBarDialog();
                        SnapableLuckyDraw snapableLuckyDraw = wrapperSnapable.getSnapableLuckyDraw();
                        if (snapableLuckyDraw.isUserAllowedSnap()) {
                            if (storeDetailView.getManisSession().isNotAvailableNameUser()) {
                                storeDetailView.startActivityProfile();
                            } else {
                                storeDetailView.startActivityReceiptDetail();
                            }
                        } else {
                            storeDetailView.getBaseActivity().showManisAlertDialog(snapableLuckyDraw.getRestrictionMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        storeDetailView.getBaseActivity().dismissProgressBarDialog();
                        if (!ConnectionDetector.isNetworkConnected(storeDetailView.getContext()))
                            NegativeScenarioManager.show(e, storeDetailView, NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }

                    @Override
                    public void onErrorFailure(ErrorResponse errorResponse) {
                        super.onErrorFailure(errorResponse);
                        storeDetailView.getBaseActivity().dismissProgressBarDialog();
                        NegativeScenarioManager.show(errorResponse, storeDetailView, NegativeScenarioManager.NegativeView.NOTIFICATION);
                    }
                });
        subsStoreDetail.put(3, subsIsSnapable);
    }
}
