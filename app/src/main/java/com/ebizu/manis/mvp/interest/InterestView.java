package com.ebizu.manis.mvp.interest;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.Interest;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.view.adapter.InterestAdapter;
import com.ebizu.manis.view.manis.interest.model.InterestValidation;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public class InterestView extends BaseView implements IInterestView {

    @BindView(R.id.view_group_title)
    View viewTitle;
    @BindView(R.id.recycler_view_interest)
    RecyclerView recyclerViewInterest;
    @BindView(R.id.linear_layout_snap_interest)
    LinearLayout buttonSave;
    @BindView(R.id.text_view_count)
    TextView textViewCount;
    @BindView(R.id.progress_bar_save)
    ProgressBar progressBarSave;

    private InterestPresenter interestPresenter;
    private InterestAdapter interestAdapter;
    private ManisApi manisApi;
    private OnInterestListener onInterestListener;

    public InterestView(Context context) {
        super(context);
        createView(context);
    }

    public InterestView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public InterestView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public InterestView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_interest, null, false);
        addView(view);
        view.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        initializeComponent();
        setlistenerView();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        interestPresenter = (InterestPresenter) iBaseViewPresenter;
        interestPresenter.attachView(this);
    }

    private void initializeComponent() {
        manisApi = ManisApiGenerator.createServiceWithToken(getContext());
        interestAdapter = new InterestAdapter(getContext(), this);
        recyclerViewInterest.setLayoutManager(new GridLayoutManager(getContext(), 4));
        recyclerViewInterest.setAdapter(interestAdapter);
        textViewCount.setText("3 ".concat(getContext().getString(R.string.in_txt_more)));
        if (getManisSession().isFirstLogin()) {
            buttonSave.setClickable(false);
        } else {
            buttonSave.setClickable(true);
        }
    }

    private void setlistenerView() {
        buttonSave.setOnClickListener((v) -> {
            String data = InterestValidation.getJsonIds(interestAdapter.getInterests());
            interestPresenter.saveInterest(manisApi, data);
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.INTEREST,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Pick"
            );
        });
    }

    @Override
    public void setTitleFisrtLogin() {
        viewTitle.setVisibility(VISIBLE);
    }

    @Override
    public void enableSaveButton() {
        buttonSave.setBackgroundResource(R.drawable.bg_btn_rounded_pink_full);
        buttonSave.setClickable(true);
        textViewCount.setText(getManisSession().isFirstLogin() ? getContext().getString(R.string.in_txt_thats_it)
                : getContext().getString(R.string.save));
        textViewCount.setTextColor(Color.parseColor("#FFFFFF"));
    }

    @Override
    public void unableSaveButton(int pickSize) {
        buttonSave.setBackgroundResource(R.drawable.bg_btn_rounded_darkgrey);
        buttonSave.setClickable(false);
        textViewCount.setText(3 - pickSize + " " + getContext().getString(R.string.in_txt_more));
        textViewCount.setTextColor(Color.parseColor("#FFFFFF"));
    }

    @Override
    public void setInterestView(ArrayList<Interest> interests) {
        interestAdapter.setInterests(interests);
    }

    @Override
    public void setOnSuccessSaveListener(OnInterestListener onInterestListener) {
        this.onInterestListener = onInterestListener;
    }

    @Override
    public void onRetry() {
        super.onRetry();
        interestPresenter.getUserInterest(manisApi);
    }

    @Override
    public void showProgressSave() {
        if (!progressBarSave.isShown())
            progressBarSave.setVisibility(VISIBLE);
    }

    @Override
    public void dismissProgressSave() {
        if (progressBarSave.isShown())
            progressBarSave.setVisibility(GONE);
    }

    @Override
    public void visibleSaveButton() {
        if (!buttonSave.isShown())
            buttonSave.setVisibility(VISIBLE);
    }

    @Override
    public void invisibleSaveButton() {
        if (buttonSave.isShown())
            buttonSave.setVisibility(INVISIBLE);
    }

    public InterestPresenter getInterestPresenter() {
        return interestPresenter;
    }

    public OnInterestListener getOnInterestListener() {
        return onInterestListener;
    }
}