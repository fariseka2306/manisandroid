package com.ebizu.manis.mvp.main.toolbar;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Point;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.facebook.shimmer.ShimmerFrameLayout;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;

/**
 * Created by Raden on 8/21/17.
 */

public class ToolbarMainMainView extends BaseView implements IToolbarMainView {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.rel_left)
    RelativeLayout relativeConversation;
    @BindView(R.id.img_left)
    ImageView imgConversation;
    @BindView(R.id.img_manis)
    ImageView imgManisLogo;
    @BindView(R.id.shimmer_title)
    ShimmerFrameLayout shimemerTitle;
    @BindView(R.id.txt_point)
    TextView txtPoint;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.beta_tag)
    ImageView imageViewBetaIcon;
    @BindView(R.id.txt_pts)
    TextView txtPointPts;
    @BindView(R.id.rel_right)
    RelativeLayout relativeNotification;
    @BindView(R.id.img_right)
    ImageView imgNotification;
    @BindView(R.id.relative_layout_notif)
    RelativeLayout viewGroupCountNotification;
    @BindView(R.id.text_view_count_notif)
    TextView textViewCountNotification;
    @BindView(R.id.view_group_point)
    ViewGroup viewGroupPoint;


 

    private IToolbarMainPresenter iToolbarMainPresenter;
    private int colorGrey,colorPink,colorTrans;

    public ToolbarMainMainView(Context context) {
        super(context);
        createView(context);
    }

    public ToolbarMainMainView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ToolbarMainMainView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.actionbar_title, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);


        colorGrey = ContextCompat.getColor(context, R.color.color_graph_grey);
        colorPink = ContextCompat.getColor(context, R.color.colorAccent);
        colorTrans = ContextCompat.getColor(context, R.color.color_graph_trans);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        attachPresenter(new ToolbarMainMainPresenter());
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        this.iToolbarMainPresenter = (IToolbarMainPresenter) iBaseViewPresenter;
        this.iToolbarMainPresenter.attachView(this);
    }

    @Override
    public void loadViewGetPoint(Point point) {
        if (point.getPoint() == 0) {
            txtPoint.setText(UtilManis.longToLocaleNumberFormat(point.getPoint()));
            txtPoint.setTextColor(colorGrey);
        } else {
            txtPoint.setText(UtilManis.longToLocaleNumberFormat(point.getPoint()));
            txtPoint.setTextColor(colorPink);
            Log.i(TAG, "loadViewPointProfile");
        }
    }

    @Override
    public void setOnLisetenerNotification(OnClickListenerNotification onClickListenerNotification) {
        relativeNotification.setOnClickListener(v -> {
            onClickListenerNotification.onShowNotification();
        });
    }

    @Override
    public void setOnListenerConversation(OnClickListenerConversation onClickListenerConversation) {
        relativeConversation.setOnClickListener(v -> {
            onClickListenerConversation.onShowConversation();
        });
    }

    @Override
    public IToolbarMainPresenter getToolbarPresenter() {
        return iToolbarMainPresenter;
    }

    @Override
    public void stopAnimationShimmer() {
        shimemerTitle.stopShimmerAnimation();
        txtPoint.setBackgroundColor(colorTrans);
    }

    @Override
    public void startAnimationShimmer() {
        shimemerTitle.startShimmerAnimation();
        shimemerTitle.setVisibility(VISIBLE);
    }

    public void prepareLoadPoint() {
        txtPoint.setText("         ");
        txtPoint.setBackgroundColor(colorGrey);
    }

    public void setTitle(String title){
        txtTitle.setText(title);
        txtTitle.setVisibility(VISIBLE);
        txtTitle.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        viewGroupPoint.setVisibility(INVISIBLE);
        imageViewBetaIcon.setVisibility(INVISIBLE);
    }

    public void setTitleMission(String title){
        txtTitle.setText(title);
        txtTitle.setVisibility(VISIBLE);
        imageViewBetaIcon.setVisibility(VISIBLE);
        viewGroupPoint.setVisibility(INVISIBLE);
    }

    public void invisbleTitle(){
        txtTitle.setVisibility(INVISIBLE);
        txtTitle.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        viewGroupPoint.setVisibility(VISIBLE);
        imageViewBetaIcon.setVisibility(INVISIBLE);
    }

    @Override
    public void setCountNotification(int countNotif) {
        if (countNotif > 0) {
            viewGroupCountNotification.setVisibility(VISIBLE);
            textViewCountNotification.setText(String.valueOf(countNotif));
            YoYo.with(Techniques.Shake).duration(2000).playOn(relativeNotification);
        } else {
            viewGroupCountNotification.setVisibility(INVISIBLE);
            textViewCountNotification.setText("");
        }
    }

    @OnClick(R.id.rel_left)
        void relativeConversation(){
            MainActivity mainActivity = (MainActivity) getContext();
            mainActivity.switchToAccount();
//            Intercom.client().displayConversationsList();
        }
}
