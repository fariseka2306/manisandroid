package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by andrifashbir on 16/11/17.
 */

public class VoucherShoppingCart extends RelativeLayout {

    @BindView(R.id.view_group)
    ViewGroup viewGroup;
    @BindView(R.id.textView_voucher)
    TextView textViewVoucher;
    @BindView(R.id.textView_bullet)
    TextView textViewBullet;

    public VoucherShoppingCart(Context context) {
        super(context);
        createView(context);
    }

    public VoucherShoppingCart(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public VoucherShoppingCart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public VoucherShoppingCart(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    protected void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_voucher_shopping_cart, null, false);
        addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    public void setValue(RewardVoucher rewardVoucher) {
        textViewVoucher.setText(rewardVoucher.getQuantity() + " " + rewardVoucher.getName());
    }

    @Override
    public void setBackgroundColor(int color) {
        super.setBackgroundColor(color);
        viewGroup.setBackgroundColor(color);
    }

    public void setInputTextColor(int color) {
        textViewVoucher.setTextColor(color);
        textViewBullet.setTextColor(color);
    }
}
