package com.ebizu.manis.mvp.snap.store.list;

import com.ebizu.manis.model.Store;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;

/**
 * Created by ebizu on 11/27/17.
 */

public interface IListSnapStorePresenter
        extends IBaseViewPresenter {

    void isSnapAble(Store store);

    void isSnapAble(Store store, RequestBody requestBody);

    interface OnCheckSnapAbleListener {

        void onCheckSnapAble(Store store);

    }

}
