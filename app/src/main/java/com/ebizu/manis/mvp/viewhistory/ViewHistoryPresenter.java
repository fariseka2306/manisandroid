package com.ebizu.manis.mvp.viewhistory;

import android.content.Context;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.model.ViewHistoryParam;
import com.ebizu.manis.root.BaseActivityPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.WrapperViewHistory;
import com.google.gson.Gson;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raden on 7/24/17.
 */

public class ViewHistoryPresenter extends BaseActivityPresenter implements IHistoryPresenter {

    private Context context;
    private SnapViewHistoryActivity snapViewHistoryActivity;
    private IHistoryView iHistoryView;
    private Subscription subViewHistory;

    public ViewHistoryPresenter(Context context) {
        this.context = context;
    }

    public void attachViewHistory(IHistoryView iHistoryView, SnapViewHistoryActivity snapViewHistoryActivity) {
        this.snapViewHistoryActivity = snapViewHistoryActivity;
        this.iHistoryView = iHistoryView;
    }

    @Override
    public void loadViewHistory(ViewHistoryParam viewHistoryParam, IBaseView.LoadType loadType) {
        releaseSubscribe(0);
        showProgressDialog(loadType);
        ManisApi manisApi = ManisApiGenerator.createServiceWithToken(context);
        subViewHistory = manisApi.getViewHistory(new Gson().toJson(viewHistoryParam)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new ResponseSubscriber<WrapperViewHistory>(snapViewHistoryActivity) {
                            @Override
                            public void onNext(WrapperViewHistory wrapperViewHistory) {
                                super.onNext(wrapperViewHistory);
                                dismissProgressDialog(loadType);
                                if (null != wrapperViewHistory.getViewHistoryResult()) {
                                    snapViewHistoryActivity.setViewHistory(wrapperViewHistory.getViewHistoryResult(), loadType);
                                }
                            }

                            @Override
                            public void onErrorFailure(ErrorResponse errorResponse) {
                                super.onErrorFailure(errorResponse);
                                dismissProgressDialog(loadType);
                                snapViewHistoryActivity.showManisAlertDialog(errorResponse.getMessage());
                            }

                            @Override
                            public void onError(Throwable throwable) {
                                super.onError(throwable);
                                dismissProgressDialog(loadType);
                                checkConnection(loadType);
                            }
                        });
    }

    private void showProgressDialog(IBaseView.LoadType loadType) {
        snapViewHistoryActivity.setLoading(true);
        snapViewHistoryActivity.layoutEmpty.setVisibility(View.GONE);
        switch (loadType) {
            case CLICK_LOAD: {
                snapViewHistoryActivity.recyclerViewHistory.setVisibility(View.GONE);
                snapViewHistoryActivity.showBaseProgressBar();
                break;
            }
            case SWIPE_LOAD: {
                snapViewHistoryActivity.recyclerViewHistory.setVisibility(View.GONE);
                snapViewHistoryActivity.showBaseProgressBar();
                break;
            }
            case SCROLL_LOAD: {
                snapViewHistoryActivity.recyclerViewHistory.setVisibility(View.VISIBLE);
                snapViewHistoryActivity.showProgressBarRecycler();
                break;
            }
            default: {
                snapViewHistoryActivity.dismissBaseProgressBar();
                break;
            }
        }
    }

    private void dismissProgressDialog(IBaseView.LoadType loadType) {
        snapViewHistoryActivity.setLoading(false);
        switch (loadType) {
            case CLICK_LOAD: {
                snapViewHistoryActivity.dismissBaseProgressBar();
                break;
            }
            case SWIPE_LOAD: {
                snapViewHistoryActivity.dismissBaseProgressBar();
                break;
            }
            case SCROLL_LOAD: {
                snapViewHistoryActivity.dismissProgressBarRecycler();
                break;
            }
            default: {
                snapViewHistoryActivity.dismissBaseProgressBar();
                snapViewHistoryActivity.dismissProgressBarRecycler();
                break;
            }
        }
    }

    private void checkConnection(IBaseView.LoadType loadType) {
        if (!ConnectionDetector.isNetworkConnected(context)) {
            if (snapViewHistoryActivity.viewHistoryAdapter.isEmpty() && loadType != IBaseView.LoadType.SCROLL_LOAD) {
                iHistoryView.noConnectionError();
            } else {
                snapViewHistoryActivity.showNotification(
                        snapViewHistoryActivity.getResources().getString(R.string.error_no_connection));
            }
        }
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subViewHistory != null)
            subViewHistory.unsubscribe();
    }

    @Override
    public void releaseAllSubscribe() {
        releaseSubscribe(0);
    }
}
