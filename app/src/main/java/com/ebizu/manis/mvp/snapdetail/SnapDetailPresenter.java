package com.ebizu.manis.mvp.snapdetail;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raden on 7/17/17.
 */

public class SnapDetailPresenter implements ISnapDetailPresenter {

    private final String TAG = getClass().getSimpleName();

    private Context context;
    private ISnapDetailView iSnapDetailView;

    public SnapDetailPresenter(Context context) {
        this.context = context;
    }

    public void attachSnapDetail(ISnapDetailView iSnapDetailView) {
        this.iSnapDetailView = iSnapDetailView;
    }

    @Override
    public void loadSnapDetails() {
        ManisApi manisApi = ManisApiGenerator.createServiceWithToken(context);
        manisApi.getSnapDetails().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        wrapperSnapDetails -> {
                            iSnapDetailView.loadViewSnapDetails(wrapperSnapDetails.getSnapDatas());
                            Log.d(TAG, "loadSnapDatahistory: " + wrapperSnapDetails.toString());
                        },
                        throwable -> {
                            cekConnection(iSnapDetailView);
                            Log.e(TAG, "loadSnapDatahistory: " + throwable.getMessage());
                        }

                );

    }

    public void cekConnection(ISnapDetailView iSnapDetailView) {
        if (ConnectionDetector.isNetworkConnected(context)) {
//            iSnapView.serverBusySnap("server sibuk");
        } else {
//            iSnapView.connectionErrorSnap();
        }
    }
}
