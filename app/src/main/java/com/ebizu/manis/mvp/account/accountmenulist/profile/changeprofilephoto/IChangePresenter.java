package com.ebizu.manis.mvp.account.accountmenulist.profile.changeprofilephoto;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;

import java.io.File;

/**
 * Created by abizu-alvio on 7/26/2017.
 */

public interface IChangePresenter extends IBaseViewPresenter {

    void saveProfilePhoto(ManisApi manisApi, File file);

    void savePhotoProfile(RequestBody requestBody);

}
