package com.ebizu.manis.mvp.account.accountmenulist.settings.notification;

import com.ebizu.manis.model.NotificationsSetting;
import com.ebizu.manis.root.IBaseView;

import java.util.List;

/**
 * Created by abizu-alvio on 7/24/2017.
 */

public interface INotificationsView extends IBaseView {

    void showNotification(List<NotificationsSetting> notificationsSettingList);

    void sendPost(List<NotificationsSetting> notificationsSettingList);



    INotificationsPresenter getNotificationPresenter();

}
