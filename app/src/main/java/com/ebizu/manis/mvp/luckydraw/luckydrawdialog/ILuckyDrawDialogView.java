package com.ebizu.manis.mvp.luckydraw.luckydrawdialog;

import com.ebizu.manis.model.LuckyDrawEntryData;
import com.ebizu.manis.model.LuckyDrawWinnerData;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by ebizu on 08/08/17.
 */

public interface ILuckyDrawDialogView extends IBaseView {

    void setLuckyDrawEntryData(LoadType loadType, LuckyDrawEntryData luckyDrawEntryData);

    void setLuckyDrawWinnerData(LoadType loadType, LuckyDrawWinnerData luckyDrawWinnerData);

    void showNegativeScenario(String message);

    ILuckyDrawDialogPresenter getLuckyDrawDialogPresenter();

    void showProgressView(IBaseView.LoadType loadType, ViewType viewType);

    void dismissProgressView(IBaseView.LoadType loadType, ViewType viewType);

    boolean isEmptyList();

    void showListProgressBar();

    void dismissListProgressBar();

    void setLoadingPresenter();

    void setUnloadingPresenter();

    void setPaginationPresenter(boolean isMore);

    void setPagePresenter(int page);

    int getPagePresenter();

    enum ViewType {
        LUCKY_DRAW_ENTRY,
        LUCKY_DRAW_WINNER
    }

}
