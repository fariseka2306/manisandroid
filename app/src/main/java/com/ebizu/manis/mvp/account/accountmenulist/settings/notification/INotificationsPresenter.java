package com.ebizu.manis.mvp.account.accountmenulist.settings.notification;

import com.ebizu.manis.model.NotificationsSetting;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;

import java.util.List;

/**
 * Created by abizu-alvio on 7/24/2017.
 */

public interface INotificationsPresenter extends IBaseViewPresenter {

    void loadViewNotifications(ManisApi manisApi);

    void saveViewNotifications(ManisApi manisAPi, List<NotificationsSetting> notificationsSettingList);

    void loadViewNotifications();

    void saveViewNotifications(RequestBody requestBody);

    RequestBody createAccountUpdateNotifConfigRB(List<NotificationsSetting> notificationsSettingList);
}
