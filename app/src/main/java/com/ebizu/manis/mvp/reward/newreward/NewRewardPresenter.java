package com.ebizu.manis.mvp.reward.newreward;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.service.reward.RewardApi;
import com.ebizu.sdk.reward.EbizuReward;
import com.ebizu.sdk.reward.core.interfaces.Callback;
import com.ebizu.sdk.reward.models.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARIS_mac on 9/22/17.
 */

public class NewRewardPresenter extends BaseViewPresenter implements INewRewardPresenter {

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private RewardApi rewardApi;

    private NewRewardView newRewardView;
    private List<String> categoriesId = new ArrayList<>();
    private List<Integer> categoriesPicture = new ArrayList<>();
    private List<String> categoriesName = new ArrayList<>();

    public NewRewardPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        newRewardView = (NewRewardView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        //not implemented method
    }

    @Override
    public void releaseAllSubscribes() {
        //not implemented method
    }

    @Override
    public void loadCategoryImage() {
        int pictures[] = {
                R.drawable.reward_category_entertainment,
                R.drawable.reward_category_fb,
                R.drawable.reward_category_healthbeauty,
                R.drawable.reward_category_mobilereload,
                R.drawable.reward_category_ecommerce,
                R.drawable.reward_category_retail,
                R.drawable.reward_category_travel,
                R.drawable.reward_category_service,
                R.drawable.reward_category_lifestyle,
                R.drawable.reward_category_newdeals};

        String category[] = {
                "entertainment",
                "f&b",
                "health&beauty",
                "mobilereload",
                "e-commerce",
                "retail",
                "travel",
                "service",
                "lifestyle",
                "newdeals"};

        List<Integer> assetPictures = new ArrayList<>();
        final List<String> assetCategories = new ArrayList<>();

        for (int i = 0; i < category.length; i++) {
            assetPictures.add(pictures[i]);
            assetCategories.add(category[i]);
        }
        showCategoryReward(assetCategories, assetPictures);
    }

    private void showCategoryReward(final List<String> names, final List<Integer> pictures) {
        EbizuReward.getProvider(context).rewardCategory(new Callback<List<Category>>() {
            @Override
            public void onSuccess(List<Category> categories) {
                newRewardView.dismissNoInternetConnection();
                if (categories != null) {
                    for (Category category : categories) {
                        categoriesId.add(category.getId());
                        categoriesName.add(category.getName());
                        String picName = category.getName().trim()
                                .replace(" ", "")
                                .replace("-", "")
                                .replace("&", "");
                        int resID = context.getResources().getIdentifier(
                                "reward_category_" + picName.toLowerCase(),
                                "drawable", context.getPackageName());
                        if (resID != 0) {
                            categoriesPicture.add(resID);
                        } else {
                            categoriesPicture.add(R.drawable.reward_category_default);
                        }
                    }
                    categoriesId.add("");
                    categoriesPicture.add(R.drawable.my_voucher);
                    categoriesName.add(context.getString(R.string.my_voucher_text));

                    categoriesId.add(0, "");
                    categoriesName.add(0, "Combo Deals");
                    categoriesPicture.add(0, R.drawable.combo_deals);
                    newRewardView.loadImageCategory(categoriesPicture, categoriesName, categoriesId);
                } else {
                    NegativeScenarioManager.showNotificationMessage(newRewardView, newRewardView.getContext().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(String s) {
                Log.d(getClass().getName(), "Failure with message : " + s);
                if (!ConnectionDetector.isNetworkConnected(context)) {
                    newRewardView.showNoInternetConnection();
                    newRewardView.rvRewardCategory.setVisibility(View.GONE);
                }
            }
        });
    }

}
