package com.ebizu.manis.mvp.account.spending;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.datespendingbar.DateSpendingManager;
import com.ebizu.manis.model.Statistic;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.response.WrapperStatistic;
import com.ebizu.manis.view.adapter.account.AccountCategoryAdapter;
import com.ebizu.manis.view.spendingbar.SpendingBarTextView;
import com.ebizu.manis.view.spendingbar.SpendingBarView;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/14/2017.
 */

public class AccountSpendView extends BaseView implements IAccountSpendView {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.text_view_currency)
    TextView textViewCurrency;
    @BindView(R.id.text_view_number_spend)
    TextView textViewNumberSpend;
    @BindView(R.id.recyclerview_account_category)
    RecyclerView recyclerViewAccountSpend;
    @BindView(R.id.spinner_type)
    Spinner spinSpendingCategory;
    @BindView(R.id.lin_emptystatistic)
    LinearLayout emptyStatic;

    @BindView(R.id.shimmer_category)
    ShimmerFrameLayout shimmerCategory;
    @BindView(R.id.shimmer_number)
    ShimmerFrameLayout shimmerNumber;
    @BindView(R.id.shimmer_bar)
    ShimmerFrameLayout shimmerBar;

    @BindView(R.id.spending_bar_text_view)
    SpendingBarTextView spendingBarTextView;
    @BindView(R.id.spending_bar_view)
    SpendingBarView spendingBarView;

    public AccountCategoryAdapter accountCategoryAdapter;
    private LinearLayoutManager linearLayoutManagerAccountCategory;
    private ManisSession manisSession;

    IAccountSpendPresenter iAccountSpendPresenter;

    public AccountSpendView(Context context) {
        super(context);
        createView(context);
    }

    public AccountSpendView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public AccountSpendView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AccountSpendView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.account_spendingbar, null, false);
        accountCategoryAdapter = new AccountCategoryAdapter(context, new ArrayList<>());
        addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        attachPresenter(new AccountSpendPresenter());
        initialize();

    }

    private void initialize() {
        linearLayoutManagerAccountCategory = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewAccountSpend.setLayoutManager(linearLayoutManagerAccountCategory);
        recyclerViewAccountSpend.setAdapter(accountCategoryAdapter);
        manisSession = new ManisSession(getContext());
    }

    @Override
    public void setSpendCategory(Context context) {
        spinSpendingCategory.setAdapter(DateSpendingManager.getSpendingSpinnerAdapter(context));
        spinSpendingCategory.setOnItemSelectedListener(itemSpinListener);
    }

    @Override
    public void showViewAccountSpend(List<Statistic> statistic) {
        this.accountCategoryAdapter.addAccountList(statistic);
        Log.i(TAG, "Data Statistic size " + String.valueOf(statistic.size()));
        if (statistic.size() >= 1) {
            shimmerBar.setVisibility(GONE);
            textViewCurrency.setVisibility(VISIBLE);
            textViewNumberSpend.setVisibility(VISIBLE);
        } else {
            shimmerCategory.setVisibility(GONE);
            emptyStatic.setVisibility(View.VISIBLE);
            shimmerNumber.setVisibility(VISIBLE);
            textViewCurrency.setVisibility(INVISIBLE);
            textViewNumberSpend.setVisibility(INVISIBLE);
        }
    }

    @Override
    public void showViewAccountTotalSpend(WrapperStatistic.Data wrapperStatistic) {
        textViewNumberSpend.setText(UtilManis.formatMoney(manisSession.getAccountSession().getCurrency().getIso2(), wrapperStatistic.getTotal()));
    }

    @Override
    public void showCurrenctyAccount() {
        textViewCurrency.setText(manisSession.getAccountSession().getCurrency().getIso2());
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        this.iAccountSpendPresenter = (IAccountSpendPresenter) iBaseViewPresenter;
        this.iAccountSpendPresenter.attachView(this);
    }

    @Override
    public void setSpendingBarViewTextView(List<Statistic> statistics) {
        spendingBarTextView.setSpendingBarTextView(statistics);
    }

    @Override
    public void setSpendingBarView(List<Statistic> statistics) {
        spendingBarView.setSpendingBarView(statistics);
    }

    @Override
    public IAccountSpendPresenter getSpendPresenter() {
        return iAccountSpendPresenter;
    }

    @Override
    public void startAnimationShimmer() {
        shimmerNumber.startShimmerAnimation();
        shimmerCategory.startShimmerAnimation();
        shimmerBar.startShimmerAnimation();
        shimmerNumber.setVisibility(VISIBLE);
        shimmerCategory.setVisibility(VISIBLE);
        shimmerBar.setVisibility(VISIBLE);
        spendingBarView.setVisibility(GONE);
        emptyStatic.setVisibility(GONE);
        spendingBarTextView.removeAllViews();
    }

    @Override
    public void stopAnimationShimmer() {
        emptyStatic.setVisibility(GONE);
        shimmerNumber.stopShimmerAnimation();
        shimmerCategory.stopShimmerAnimation();
        shimmerBar.stopShimmerAnimation();
        spendingBarView.setVisibility(VISIBLE);
    }

    private AdapterView.OnItemSelectedListener itemSpinListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            iAccountSpendPresenter.loadAccountSpend(ManisApiGenerator.createServiceWithToken(getContext()),
                    DateSpendingManager.getStatisticBody(position, getContext()));
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
}