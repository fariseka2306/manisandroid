package com.ebizu.manis.mvp.luckydraw.luckydrawhelp;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 11/08/17.
 */

public class LuckyDrawHelpView extends BaseView {

    @BindView(R.id.ld_help_title)
    TextView ldHelpTitle;

    @BindView(R.id.ld_help_title_below)
    TextView ldHelpTitleBelow;

    @BindView(R.id.ld_help_logo)
    ImageView ldHelpLogo;

    @BindView(R.id.ld_help_subtitle)
    TextView ldHelpSubtitle;

    @BindView(R.id.tv_desc_prize)
    TextView tvDescPrize;

    public LuckyDrawHelpView(Context context) {
        super(context);
        createView(context);
    }

    public LuckyDrawHelpView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public LuckyDrawHelpView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LuckyDrawHelpView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.lucky_draw_help_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
    }

    public void setLuckyDrawHelpView(int title, int subtitle, int logo) {
        ldHelpTitle.setText(title);
        ldHelpSubtitle.setText(subtitle);
        Glide.with(getContext())
                .load(logo)
                .into(ldHelpLogo);
    }

    public void setLuckyDrawSubmitView() {
        ldHelpTitleBelow.setVisibility(VISIBLE);
        ldHelpTitle.setText(R.string.ld_submitted_title);
        ldHelpTitleBelow.setText(R.string.ld_submitted_sub_title);
        Glide.with(getContext())
                .load(R.drawable.ticket_icon_white)
                .into(ldHelpLogo);
        ldHelpSubtitle.setText(R.string.ld_submitted_desc);
        tvDescPrize.setText(R.string.ld_submitted_desc_prize);
    }
}
