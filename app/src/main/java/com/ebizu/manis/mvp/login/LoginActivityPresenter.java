package com.ebizu.manis.mvp.login;

import android.content.Intent;
import android.util.SparseArray;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.DeviceUtils;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.facebook.FacebookManager;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.Device;
import com.ebizu.manis.model.Google;
import com.ebizu.manis.mvp.phoneloginfacebookotp.FacebookOtpLoginActivity;
import com.ebizu.manis.preference.RewardSession;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseActivityPresenter;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.sdk.rest.data.AuthSignIn;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.DataFacebookAuth;
import com.ebizu.manis.service.manis.requestbody.DataGmailAuth;
import com.ebizu.manis.service.manis.response.WrapperAccount;
import com.ebizu.manis.service.reward.RewardApiGenerator;
import com.ebizu.manis.service.reward.requestbody.RewardRegisterBody;
import com.ebizu.manis.service.reward.response.WrapperRegisterSession;
import com.ebizu.sdk.reward.EbizuReward;
import com.ebizu.sdk.reward.models.SessionData;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ebizu-User on 10/07/2017.
 */

public class LoginActivityPresenter extends BaseActivityPresenter implements ILoginActivityPresenter {

    GoogleSignInAccount googleSignInAccount;
    private LoginActivity loginView;
    private FacebookManager facebookManager;
    private ManisApi manisApi;
    private String regIdGCM;
    private SparseArray<Subscription> subsLogin;

    @Inject
    public LoginActivityPresenter() {

    }

    @Override
    public void attachView(BaseActivity view) {
        super.attachView(view);
        loginView = (LoginActivity) view;
        subsLogin = new SparseArray<>();
    }

    @Override
    public void releaseSubscribe(int key) {
        Subscription subscription = subsLogin.get(key);
        if (subscription != null) {
            subscription.unsubscribe();
            subsLogin.remove(key);
        }
    }

    @Override
    public void releaseAllSubscribe() {
        loginView.unSubscribeAll(subsLogin);
    }

    @Override
    public void loginByGoogle(String regIdGCM) {
        this.regIdGCM = regIdGCM;
        getGoogleApiManager().disconnect();
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(getGoogleApiManager().googleApiClient());
        loginView.baseActivity().startActivityForResult(signInIntent, ConfigManager.Auth.GMAIL_REQUEST_CODE);
    }

    @Override
    public void getGooglePerson(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        loginView.showProgressBarDialog(getContext().getString(R.string.txt_loading), false);
        if (result.isSuccess()) {
            googleSignInAccount = result.getSignInAccount();
            assert googleSignInAccount != null;
            Plus.PeopleApi.load(getGoogleApiManager().googleApiClient(), googleSignInAccount.getId())
                    .setResultCallback(loadPeopleResult -> {
                        try {
                            Person person = loadPeopleResult.getPersonBuffer().get(0);
                            loginAccountManis(getGoogleAccount(person, googleSignInAccount));
                        } catch (Exception e) {
                            loginView.dismissProgressBarDialog();
                            UtilManis.info(loginView.baseActivity(),
                                    loginView.baseContext().getString(R.string.error),
                                    loginView.baseContext().getString(R.string.error_google));
                        }
                        getGoogleApiManager().disconnect();
                    }, 5, TimeUnit.SECONDS);
        } else {
            getGoogleApiManager().disconnect();
            loginView.dismissProgressBarDialog();
            UtilManis.info(loginView,
                    loginView.baseContext().getString(R.string.error),
                    loginView.baseContext().getString(R.string.error_google));
        }
    }

    @Override
    public void loginByFacebook(String regIdGCM) {
        this.regIdGCM = regIdGCM;
        getFacebookManager().startLoginFacebook(false);
    }

    @Override
    public void loginByPhone(String regIdGCM) {
        Intent intent = new Intent(loginView.baseContext(), FacebookOtpLoginActivity.class);
        intent.putExtra(ConfigManager.Auth.REGID_GCM_INTENT, regIdGCM);
        loginView.baseActivity().startActivity(intent);
    }

    @Override
    public void loginAccountManis(Google google) {
        loginView.showProgressBarDialog(loginView.baseActivity().getString(R.string.txt_loading));
        releaseSubscribe(0);
        Subscription subsGoogle = getManisApi().signInGoogle(new Gson().toJson(getDataGmailAuth(google)))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperAccount>(loginView) {
                    @Override
                    public void onNext(WrapperAccount wrapperAccount) {
                        super.onNext(wrapperAccount);
                        loginRewardSdk(wrapperAccount.getAccount());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        loginView.dismissProgressBarDialog();
                        UtilManis.info(loginView.baseActivity(),
                                loginView.baseContext().getString(R.string.error),
                                loginView.baseContext().getString(R.string.error_server));
                    }
                });
        subsLogin.put(0, subsGoogle);
    }

    @Override
    public void loginAccountManis(AuthSignIn.RequestBody.Facebook facebook) {
        loginView.showProgressBarDialog(loginView.baseActivity().getString(R.string.txt_loading));
        releaseSubscribe(1);
        Subscription subsFacebook = getManisApi().signInFacebook(new Gson().toJson(getDataFacebookAuth(facebook)))
                .timeout(10, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperAccount>(loginView) {
                    @Override
                    public void onNext(WrapperAccount wrapperAccount) {
                        super.onNext(wrapperAccount);
                        loginRewardSdk(wrapperAccount.getAccount());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        loginView.dismissProgressBarDialog();
                        UtilManis.info(loginView.baseActivity(),
                                loginView.baseContext().getString(R.string.error),
                                loginView.baseContext().getString(R.string.error_server));
                    }
                });
        subsLogin.put(1, subsFacebook);
    }

    @Override
    public FacebookManager getFacebookManager() {
        if (facebookManager == null) {
            facebookManager = new FacebookManager(loginView, this);
        }
        return facebookManager;
    }

    @Override
    public void loginRewardSdk(Account account) {
        EbizuReward.getSession(loginView).login(account.getAccId(), account.getAccScreenName(),
                account.getEmail(), account.getAccCountry(), account.getAccCreatedDateTime(), new com.ebizu.sdk.reward.core.interfaces.Callback<SessionData>() {
                    @Override
                    public void onSuccess(SessionData sessionData) {
                        String rewardSession = sessionData.getValue();
                        RewardSession.getInstance(loginView).setSession(rewardSession);
                        loginView.nextActivity(account);
                    }

                    @Override
                    public void onFailure(String s) {
                        loginView.dismissProgressBarDialog();
                        UtilManis.info(loginView.baseActivity(),
                                loginView.baseContext().getString(R.string.error),
                                loginView.baseContext().getString(R.string.error_server));
                    }
                });
    }

    private DataFacebookAuth getDataFacebookAuth(AuthSignIn.RequestBody.Facebook facebook) {
        DataFacebookAuth dataFacebookAuth = new DataFacebookAuth();
        Device device = DeviceUtils.getDevice(loginView.baseActivity());
        device.setToken(regIdGCM);
        dataFacebookAuth.setDataDevice(device);
        dataFacebookAuth.setFacebook(facebook);
        return dataFacebookAuth;
    }

    private DataGmailAuth getDataGmailAuth(Google google) {
        DataGmailAuth dataGmailAuth = new DataGmailAuth();
        Device device = DeviceUtils.getDevice(loginView.baseActivity());
        device.setToken(regIdGCM);
        dataGmailAuth.setDataDevice(device);
        dataGmailAuth.setGoogle(google);
        return dataGmailAuth;
    }

    private Google getGoogleAccount(Person person, GoogleSignInAccount account) {
        Google google = new Google();
        google.setId(account.getId());
        google.setEmail(account.getEmail());
        google.setName(account.getDisplayName());
        google.setBirthday(person.getBirthday());
        google.setGender(person.hasGender() ? person.getGender() : -1);
        google.setVerified(person.isVerified());
        return google;
    }

    public ManisApi getManisApi() {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createService(loginView.baseContext());
        return manisApi;
    }

    @Override
    public void registerRewardSession(Account account) {
        releaseSubscribe(2);
        RewardRegisterBody.Data data = new RewardRegisterBody.Data();
        data.setUserId(account.getAccId());
        RewardRegisterBody rewardRegisterBody = new RewardRegisterBody(data);
        Observable<WrapperRegisterSession> observable = RewardApiGenerator.createService(loginView).registerSession(rewardRegisterBody);
        Subscription subsRegisterReward = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new ResponseRewardSubscriber<WrapperRegisterSession>(loginView) {
                            @Override
                            public void onNext(WrapperRegisterSession wrapperRegisterSession) {
                                super.onNext(wrapperRegisterSession);
                                String session = wrapperRegisterSession.getRewardSession().getSession();
                                loginView.rewardSession.setSession(session);
                                loginRewardSdk(account);
                            }

                            @Override
                            public void onError(Throwable e) {
                                super.onError(e);
                                loginView.dismissProgressBarDialog();
                                UtilManis.info(loginView.baseActivity(),
                                        loginView.baseContext().getString(R.string.error),
                                        loginView.baseContext().getString(R.string.error_server));
                            }
                        }
                );
        subsLogin.put(2, subsRegisterReward);
    }

}