package com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary.shippingdetails;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.model.purchasevoucher.CustomerDetails;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.view.manis.textview.ShippingDetailsFormText;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 11/16/17.
 */

public class ShippingDetailsView extends BaseView {

    @BindView(R.id.text_name)
    ShippingDetailsFormText textName;
    @BindView(R.id.text_email)
    ShippingDetailsFormText textEmail;
    @BindView(R.id.text_phone)
    ShippingDetailsFormText textPhone;
    @BindView(R.id.text_address)
    ShippingDetailsFormText textAddress;
    @BindView(R.id.text_post_code)
    ShippingDetailsFormText textPostCode;
    @BindView(R.id.text_city)
    ShippingDetailsFormText textCity;
    @BindView(R.id.text_state)
    ShippingDetailsFormText textState;
    @BindView(R.id.text_country)
    ShippingDetailsFormText textCountry;
    @BindView(R.id.text_shipping_info)
    ShippingDetailsFormText textShipmentInfo;
    @BindView(R.id.physical_shipping_details)
    RelativeLayout layoutPhysical;

    public ShippingDetailsView(Context context) {
        super(context);
        createView(context);
    }

    public ShippingDetailsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ShippingDetailsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    public ShippingDetailsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_shipping_details, null, false);
        addView(view, new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    public void setView(Order order) {
        CustomerDetails customerDetails = order.getCustomerDetails();
        textName.setInputText(customerDetails.getName());
        textEmail.setInputText(customerDetails.getEmail());
        textPhone.setInputText(customerDetails.getPhoneNo());
        if (customerDetails.getAddress().isEmpty()) {
            textShipmentInfo.setVisibility(View.VISIBLE);
            textShipmentInfo.setInputText(customerDetails.getShipmentInfo());
        } else {
            layoutPhysical.setVisibility(View.VISIBLE);
            textAddress.setInputText(customerDetails.getAddress());
            textPostCode.setInputText(customerDetails.getPostCode());
            textCity.setInputText(customerDetails.getTown());
            textState.setInputText(customerDetails.getState());
            textCountry.setInputText(customerDetails.getCountry());
        }
    }
}
