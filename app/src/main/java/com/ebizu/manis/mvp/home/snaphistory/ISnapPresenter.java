package com.ebizu.manis.mvp.home.snaphistory;

/**
 * Created by Raden on 7/7/17.
 */

public interface ISnapPresenter {
    void loadSnapData();
}
