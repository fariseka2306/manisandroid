package com.ebizu.manis.mvp.main.toolbar;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by Raden on 8/21/17.
 */

public interface IToolbarMainPresenter extends IBaseViewPresenter {

    void loadGetPoint(ManisApi manisApi);

    void loadTotalNotifUnread();

}
