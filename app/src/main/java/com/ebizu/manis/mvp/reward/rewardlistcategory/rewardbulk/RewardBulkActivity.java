package com.ebizu.manis.mvp.reward.rewardlistcategory.rewardbulk;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.reward.rewardlistcategory.RewardCategoryAbstractActivity;
import com.ebizu.manis.mvp.reward.rewardlistcategory.RewardListCategoryAbstractView;
import com.ebizu.manis.service.reward.requestbody.RewardBulkListBody;

/**
 * Created by FARIS_mac on 16/11/17.
 */

public class RewardBulkActivity extends RewardCategoryAbstractActivity {

    @Override
    public void initView() {
        toolbarView.setTitle(ConfigManager.Reward.REWARD_BULK_TITLE, R.drawable.navigation_back_btn);
    }

    @Override
    public void initParameter() {
        RewardBulkListBody.Data data = new RewardBulkListBody.Data();
        data.setSize(ConfigManager.Reward.REWARD_BULK_SIZE);
        data.setPage(ConfigManager.Reward.REWARD_BULK_FIRST_PAGE);
        data.setKeyword("");
        data.setOrder(1);
        data.setNoCache("N");
        RewardBulkListBody requestBody = new RewardBulkListBody(this, data);
        rewardBulkView.getRewardCategoryPresenter().loadRewardsBulkList(requestBody);
    }

    @Override
    public RewardListCategoryAbstractView getRewardCategoryAbstractView() {
        return rewardBulkView;
    }
}
