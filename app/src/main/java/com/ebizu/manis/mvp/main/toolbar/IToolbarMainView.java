package com.ebizu.manis.mvp.main.toolbar;

import com.ebizu.manis.model.Point;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by Raden on 8/21/17.
 */

public interface IToolbarMainView extends IBaseView {

    void loadViewGetPoint(Point point);

    void setOnLisetenerNotification(OnClickListenerNotification onClickListenerNotification);

    void setOnListenerConversation(OnClickListenerConversation onClickListenerConversation);

    void setCountNotification(int countNotification);

    void stopAnimationShimmer();

    void startAnimationShimmer();

    IToolbarMainPresenter getToolbarPresenter();

    interface OnClickListenerNotification{
        void onShowNotification();
    }

    interface OnClickListenerConversation{
        void onShowConversation();
    }
}
