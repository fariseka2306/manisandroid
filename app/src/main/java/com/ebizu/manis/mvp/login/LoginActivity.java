package com.ebizu.manis.mvp.login;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.LoginChoices;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.login.FacebookLogin;
import com.ebizu.manis.manager.login.GoogleSignInLogin;
import com.ebizu.manis.manager.login.LoginManager;
import com.ebizu.manis.manager.login.PhoneLogin;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.mvp.interest.InterestActivity;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.mvp.phoneloginfacebookotp.FacebookOtpLoginActivity;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.preference.RewardSession;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.IBaseActivity;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.requestbodyv2.data.ManisTncData;
import com.ebizu.manis.view.dialog.term.TermDialog;
import com.ebizu.manis.view.videoview.VideoManis;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerOriginPageLogin;

/**
 * Created by Ebizu-User on 10/07/2017.
 */

public class LoginActivity extends BaseActivity implements ILoginActivity {

    @Inject
    Context context;
    @Inject
    LoginActivityPresenter loginPresenter;
    @Inject
    ManisSession manisSession;
    @Inject
    RewardSession rewardSession;

    @BindView(R.id.lg_txt_term)
    TextView textViewTerm;

    private VideoManis videoManis;
    private String regIdGCM;
    LoginChoices loginChoices;

    @Override

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loginPresenter.attachView(this);
        attachPresenter(loginPresenter);
        setLoginActivityView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent())
                .build();
        activityComponent.inject(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConfigManager.Auth.GMAIL_REQUEST_CODE) {
            loginPresenter.getGooglePerson(data);
        } else if (loginPresenter.getFacebookManager() != null && ConfigManager.FacebookParam.REQUEST_CODE == requestCode) {
            loginPresenter.getFacebookManager().getCallbackManagerFacebook().onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!videoManis.isPlaying())
            videoManis.startPlayVideo();
        TrackerManager.getInstance().setTrackerData(
                new TrackerStandartRequest(kTrackerOriginPageLogin, kTrackerOriginPageLogin, "", "", "", "")
        );
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoManis.isPlaying())
            videoManis.pauseVideo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.lg_btn_google)
    void onClickGoogleLogin() {
        loginChoices = LoginChoices.GOOGLE;
        GoogleSignInLogin googleSignInLogin = new GoogleSignInLogin();
        showPermissionPopUp(googleSignInLogin.requestCode());
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.LOGIN_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Google"
        );
    }

    @OnClick(R.id.lg_btn_facebook)
    void onClickFacebookLogin() {
        loginChoices = LoginChoices.FACEBOOK;
        FacebookLogin facebookLogin = new FacebookLogin();
        showPermissionPopUp(facebookLogin.requestCode());
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.LOGIN_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Facebook"
        );
    }

    @OnClick(R.id.lg_btn_phone)
    void onClickPhoneLogin() {
        loginChoices = LoginChoices.PHONE;
        PhoneLogin phoneLogin = new PhoneLogin();
        showPermissionPopUp(phoneLogin.requestCode());
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.LOGIN_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Phone"
        );
    }

    private void showPermissionPopUp(int requestCode) {
        showDialogPermission(requestCode, new IBaseActivity.OnDialogPermissionsListener() {
            @Override
            public void onAllow(int requestCode) {
                showAllowPermissionDialog(requestCode);
            }

            @Override
            public void onDeny() {
                //Do fucking nothing
            }
        });
    }

    private void showAllowPermissionDialog(int requestCode) {
        DeviceSession deviceSession = new DeviceSession(this);
        if (deviceSession.isAllowDialogPermission()) {
            showAlertDialog(LoginManager.getPermissionRequires(loginPresenter, requestCode), true,
                    "Ok", (var1, var2) -> {
                        LoginActivity.this.getPermissionManager().requestPermission(requestCode, LoginManager.getPermissions(requestCode));
                        deviceSession.allowDialogPermission();
                    },
                    "Cancel", (var1, var2) -> {
                        String messageDeny = LoginManager.getMessageDeny(loginPresenter, requestCode);
                        showAlertDialog(messageDeny, true,
                                "Ok", null, null, null);
                    });
        } else {
            LoginActivity.this.getPermissionManager().requestPermission(requestCode, LoginManager.getPermissions(requestCode));
        }
    }

    @OnClick(R.id.lg_txt_term)
    void onClickTerm() {
        TermDialog termDialog = new TermDialog(this);
        termDialog.setIsManisLegal(true);
        termDialog.show();
        termDialog.setActivity(this);
        termDialog.getTermPresenter().loadTerm(setRequestBody(context), ConfigManager.ManisLegal.TNC_TOU);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.LOGIN_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Text Terms");
    }

    @Override
    public void showGoogleConnectionFailed(ConnectionResult connectionResult) {

    }

    private void setLoginActivityView() {
        videoManis = new VideoManis((ViewGroup) getWindow().getDecorView(), this);
        textViewTerm.setText(UtilManis.fromHtml(getString(R.string.lg_tos)));
    }

    @Override
    public void nextActivity(Account account) {
        manisSession.setSession(account);
        UtilManis.setIntercomSdk(getApplication(), account, regIdGCM, loginChoices);
        UtilManis.setEbizuCampaign(getApplicationContext(), account, loginChoices);
        UtilManis.setBranchSDK(getApplicationContext(), account);
        validateOtp(account);
    }

    private void nextActivityMain(Account account) {
        if (account.isFirstLogin()) {
            startActivity(new Intent(this, InterestActivity.class));
        } else {
            startActivity(new Intent(this, MainActivity.class));
        }
        dismissProgressBarDialog();
        ActivityCompat.finishAffinity(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (getPermissionManager().hasPermissions(permissions)) {
            setupGCM(requestCode);
        } else {
            String messageDeny = LoginManager.getMessageDeny(loginPresenter, requestCode);
            showAlertDialog(messageDeny, true,
                    "Ok", null, null, null);
        }
    }

    @Override
    public void validateOtp(Account account) {
        if (!manisSession.isOtpRegistered()) {
            manisSession.setSession(ConfigManager.AccountSession.OTP_STATUS_DIALOG, true);
            dismissProgressBarDialog();
            showAlertDialog(getResources().getString(R.string.app_name),
                    getResources().getString(R.string.verify_otp_login),
                    false, R.drawable.manis_logo,
                    getResources().getString(R.string.text_ok), ((dialog, which) -> FacebookOtpLoginActivity.start(this, ConfigManager.Otp.SIGN_UP_OTP_REQUEST_CODE)),
                    getResources().getString(R.string.text_skip), ((dialog, which) -> nextActivityMain(account)));
        } else {
            nextActivityMain(account);
        }
    }

    private void setupGCM(int requestCode) {
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
        new RegisterGCMAsyncTask(gcm, requestCode).execute();
    }

    private class RegisterGCMAsyncTask extends AsyncTask<Void, Void, String> {

        GoogleCloudMessaging gcm;
        private int requestCode;
        private volatile boolean running = true;

        RegisterGCMAsyncTask(GoogleCloudMessaging gcm, int requestCode) {
            this.gcm = gcm;
            this.requestCode = requestCode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBarDialog(getString(R.string.txt_loading));
        }

        @Override
        protected String doInBackground(Void... params) {
            String regid = null;
            while (running) {
                try {
                    regid = gcm.register(getString(R.string.GoogleDevKey));
                    running = false;
                } catch (IOException e) {
                    e.printStackTrace();
                    running = false;
                }
            }
            return regid;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (!result.equals("")) {
                    dismissProgressBarDialog();
                    regIdGCM = result;
                    LoginManager.login(loginPresenter, requestCode, regIdGCM);
                } else {
                    dismissProgressBarDialog();
                }
            } else {
                Toast.makeText(context, getString(R.string.error_wrong), Toast.LENGTH_LONG).show();
                dismissProgressBarDialog();
            }
        }
    }

    private RequestBody setRequestBody(Context context) {
        ManisTncData manisTncData = new ManisTncData();
        manisTncData.setType(ConfigManager.ManisLegal.TNC_TYPE_NON_SNE);
        RequestBody requestBody = new RequestBodyBuilder(context)
                .setCommand(ConfigManager.ManisLegal.COMMAND_MANIS_LEGAL)
                .setFunction(ConfigManager.ManisLegal.FUNCTION_MANIS_LEGAL)
                .setData(manisTncData)
                .create();
        return requestBody;
    }
}