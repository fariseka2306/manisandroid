package com.ebizu.manis.mvp.reward.rewarddetail.rewardbulkdetail;

import com.ebizu.manis.mvp.reward.rewarddetail.RewardDetailAbstractActivity;

/**
 * Created by FARIS_mac on 15/11/17.
 */

public class RewardBulkDetailActivity extends RewardDetailAbstractActivity {

    @Override
    public void onClickRedeem() {
        rewardDetailView.showUpdateDialog();
        initAnalyticTrack();
    }

    @Override
    public void onCLickBuy() {
        super.onCLickBuy();
    }
}
