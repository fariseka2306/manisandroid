package com.ebizu.manis.mvp.reward.purchasevoucher.validation;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Created by ebizu on 10/4/17.
 */

public class TransactionValidation {

    private TransactionStatus[] transactionStatuses;
    private String paymentStatus;
    private Context context;

    public TransactionValidation(Context context, String paymentStatus) {
        this.context = context;
        this.paymentStatus = paymentStatus.toLowerCase();
        transactionStatuses = new TransactionStatus[]{
                new TransactionSuccess(context),
                new TransactionPending(context),
                new TransactionFailed(context),
                new TransactionInvalid(context),
        };
    }

    public String getTransactionPayment() {
        for (TransactionStatus transactionStatus : transactionStatuses) {
            if (paymentStatus.equals(transactionStatus.statusMidTrans()) || paymentStatus.equals(transactionStatus.statusMolPay()))
                return transactionStatus.statusPayment();
        }
        return new TransactionPending(context).statusPayment();
    }


    public String getTransactionStatus() {
        for (TransactionStatus transactionStatus : transactionStatuses) {
            if (paymentStatus.equals(transactionStatus.statusMidTrans()) || paymentStatus.equals(transactionStatus.statusMolPay()))
                return transactionStatus.statusTransaction();
        }
        return new TransactionPending(context).statusTransaction();
    }

    public Drawable getTransactionIcon() {
        for (TransactionStatus transactionStatus : transactionStatuses) {
            if (paymentStatus.equals(transactionStatus.statusMidTrans()) || paymentStatus.equals(transactionStatus.statusMolPay()))
                return transactionStatus.iconStatus();
        }
        return new TransactionPending(context).iconStatus();
    }

    public boolean successTrans() {
        TransactionSuccess transactionSuccess = new TransactionSuccess(context);
        return (paymentStatus.equals(transactionSuccess.statusMidTrans()) || paymentStatus.equals(transactionSuccess.statusMolPay()));
    }

    public boolean pendingTrans() {
        TransactionPending transactionPending = new TransactionPending(context);
        return (paymentStatus.equals(transactionPending.statusMidTrans()) || paymentStatus.equals(transactionPending.statusMolPay()));
    }
}