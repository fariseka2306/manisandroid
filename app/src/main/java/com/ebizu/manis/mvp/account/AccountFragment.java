package com.ebizu.manis.mvp.account;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.StatisticBody;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.mvp.account.menulist.AccountMenuListView;
import com.ebizu.manis.mvp.account.profile.AccountProfileView;
import com.ebizu.manis.mvp.account.spending.AccountSpendView;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.VersioningBody;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerOriginPageAccount;

/**
 * Created by abizu-alvio on 7/5/2017.
 */

public class AccountFragment extends BaseFragment {

    private final String TAG = getClass().getSimpleName();

    StatisticBody statisticBody;
    VersioningBody versioningBody;

    @BindView(R.id.account_profile_view)
    AccountProfileView accountProfileView;
    @BindView(R.id.account_spend_view)
    AccountSpendView accountSpendView;
    @BindView(R.id.account_menu_list)
    AccountMenuListView accountMenuListView;

    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        versioningBody = new VersioningBody();
        statisticBody = new StatisticBody();
        statisticBody.setLimit("tm");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);
        setOnListener();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        accountProfileView.getProfilePresenter().loadAccountProfile();
        accountProfileView.getProfilePresenter().loadPointProfile();
        accountSpendView.setSpendCategory(context);
        accountSpendView.getSpendPresenter().loadAccountSpend(ManisApiGenerator.createServiceWithToken(context), statisticBody);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            accountProfileView.getProfilePresenter().loadAccountProfile();
            accountProfileView.getProfilePresenter().loadPointProfile();
            accountSpendView.setSpendCategory(context);
            accountSpendView.getSpendPresenter().loadAccountSpend(ManisApiGenerator.createServiceWithToken(context), statisticBody);
            TrackerManager.getInstance().setTrackerData(
                    new TrackerStandartRequest(kTrackerOriginPageAccount, kTrackerOriginPageAccount, "", "", "", "")
            );
        }
    }

    private void setOnListener() {
        accountProfileView.setActivity((BaseActivity) getActivity());
        accountSpendView.setActivity((BaseActivity) getActivity());
        accountMenuListView.setActivity((BaseActivity) getActivity());
        accountProfileView.setOnClickSignOutSccess(() -> accountProfileView.getBaseActivity().signOut());
    }

    @Override
    public ManisSession getManisSession() {
        return null;
    }
}