package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory;

import com.ebizu.manis.model.PurchaseHistoryPaging;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public interface IPurchaseHistoryView extends IBaseView {

    void setPurchaseHistory(PurchaseHistoryPaging purchaseHistoryPaging, LoadType loadType);

    void loadPurchaseHistoryViewFail(LoadType loadType);

    IPurchaseHistoryPresenter getPurchaseHistoryPresenter();
}
