package com.ebizu.manis.mvp.store.storedetaillocation;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.GPSTracker;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 25/07/17.
 */

public class StoreDetailLocationActivity extends BaseActivity {

    @Inject
    Context context;

    @Inject
    ManisApi manisApi;

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;

    @BindView(R.id.sd_txt_address)
    TextView sdTxtAddress;

    @BindView(R.id.sd_btn_maps)
    Button sdBtnMaps;

    @BindView(R.id.store_location_view)
    StoreLocationView storeLocationView;

    private Store store;
    private GPSTracker gpsTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stores_detail_location);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        Store store = bundle.getParcelable(ConfigManager.Store.STORE_MAP_LOCATION);
        this.store = store;
        if (!store.getName().isEmpty()) {
            String[] storeName = store.getName().split("@");
            toolbarView.setTitle(storeName[0], R.drawable.navigation_back_btn_black);
        }
        sdTxtAddress.setText(store.getAddress());
        sdBtnMaps.setOnClickListener(btnMapListener);

        gpsTracker = new GPSTracker(this);
        gpsTracker.updateLocation();

        storeLocationView.setStore(store);
        storeLocationView.setCoordinateLocation(gpsTracker, store.getCoordinate());
        storeLocationView.setActivity(this);
    }

    private View.OnClickListener btnMapListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Uri gmmIntentUri = Uri.parse("geo:" + Double.toString(store.getCoordinate().getLatitude()) + ","
                    + Double.toString(store.getCoordinate().getLongitude()));
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
    };
}
