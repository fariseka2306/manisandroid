package com.ebizu.manis.mvp.store.storecategorydetail;

import com.ebizu.manis.model.StoreResults;
import com.ebizu.manis.service.manis.requestbody.StoreInterestBody;

public interface IStoreCategoryDetail {
    void loadStoreCategory(StoreResults storeResults, StoreInterestBody storeInterestBody);

    void loadStoreCategoryPaging(StoreResults storeResults, StoreInterestBody storeInterestBody);
}
