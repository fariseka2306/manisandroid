package com.ebizu.manis.mvp.main.activityresult;

import android.content.Intent;

import com.ebizu.manis.mvp.main.MainActivity;

/**
 * Created by ebizu on 10/11/17.
 */

public class MainRequest {

    private MainRequestCode[] requests;

    public MainRequest(MainActivity mainActivity) {
        requests = new MainRequestCode[]{
                new SavedRequest(mainActivity),
                new SnapRequest(mainActivity),
                new MissionRequest(mainActivity),
                new SnapEarnGuideRequest(mainActivity),
                new WalkThroughRequest(mainActivity)
        };
    }

    public void doJobRequest(int requestCode, int resultCode, Intent data) {
        for (MainRequestCode request : requests) {
            if (requestCode == request.requestCode())
                request.jobRequest(resultCode, data);
        }
    }

}
