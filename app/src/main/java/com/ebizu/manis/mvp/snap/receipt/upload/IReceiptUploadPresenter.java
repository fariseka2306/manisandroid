package com.ebizu.manis.mvp.snap.receipt.upload;

import android.graphics.Bitmap;

import com.ebizu.manis.model.snap.Photo;
import com.ebizu.manis.root.IBaseActivityPresenter;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptDataBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;

import okhttp3.MultipartBody;

/**
 * Created by ebizu on 8/21/17.
 */

public interface IReceiptUploadPresenter extends IBaseActivityPresenter {

    void uploadReceiptImage(okhttp3.RequestBody requestBody, MultipartBody.Part multipartBody);

    void uploadReceiptImageLuckyDraw(okhttp3.RequestBody requestBody, MultipartBody.Part multipartBody);

    void uploadReceipt(RequestBody requestBody);

    void uploadReceiptImageLuckyDraw(RequestBody requestBody);

    Photo createPhoto(Bitmap bitmap);

    RequestBody createSnapReceiptUploadRB(ReceiptDataBody receiptDataBody, Photo photo);

    RequestBody createLuckyDrawUploadReceiptRB(ReceiptDataBody receiptDataBody, Photo photo);

}