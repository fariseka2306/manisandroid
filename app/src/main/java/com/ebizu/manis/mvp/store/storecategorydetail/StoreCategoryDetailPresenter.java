package com.ebizu.manis.mvp.store.storecategorydetail;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.StoreInterestBody;
import com.ebizu.manis.service.manis.response.WrapperInterestStore;
import com.google.gson.Gson;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by firef on 7/12/2017.
 */

public class StoreCategoryDetailPresenter extends BaseViewPresenter implements IStoreCategoryDetailPresenter {

    private static final String TAG = StoreCategoryDetailPresenter.class.getSimpleName();
    private Context context;
    private StoreCategoryDetailActivity storeCategoryDetailActivity;
    private Subscription subsInterestStore;

    public StoreCategoryDetailPresenter(Context context) {
        this.context = context;
        storeCategoryDetailActivity = (StoreCategoryDetailActivity) context;
    }

    @Override
    public void loadStoreCategoryDetail(ManisApi manisApi, StoreInterestBody storeInterestBody) {
        releaseSubscribe(0);
        storeCategoryDetailActivity.showBaseProgressBar();
        subsInterestStore = manisApi.getInterestStore(new Gson().toJson(storeInterestBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperInterestStore>(storeCategoryDetailActivity) {
                    @Override
                    public void onNext(WrapperInterestStore wrapperInterestStore) {
                        super.onNext(wrapperInterestStore);
                        storeCategoryDetailActivity.loadStoreCategory(wrapperInterestStore.getStoreResults(), storeInterestBody);
                        storeCategoryDetailActivity.dismissBaseProgressBar();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        checkConnection();
                        storeCategoryDetailActivity.dismissBaseProgressBar();

                    }
                });
    }

    @Override
    public void loadStoreCategoryDetailPaging(ManisApi manisApi, StoreInterestBody storeInterestBody) {
        releaseSubscribe(0);
        subsInterestStore = manisApi.getInterestStore(new Gson().toJson(storeInterestBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperInterestStore>(storeCategoryDetailActivity) {
                    @Override
                    public void onNext(WrapperInterestStore wrapperInterestStore) {
                        super.onNext(wrapperInterestStore);
                        storeCategoryDetailActivity.loadStoreCategoryPaging(wrapperInterestStore.getStoreResults(), storeInterestBody);
                        storeCategoryDetailActivity.dismissBaseProgressBar();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        checkConnection();
                        storeCategoryDetailActivity.dismissBaseProgressBar();
                    }
                });
    }

    private void checkConnection() {
        if (!ConnectionDetector.isNetworkConnected(context)) {
            storeCategoryDetailActivity.showNotificationMessage(context.getString(R.string.error_no_connection));
        }
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsInterestStore != null) subsInterestStore.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }
}
