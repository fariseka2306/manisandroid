package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistorypyshicalfactory;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.model.PurchaseHistory;

/**
 * Created by halim_ebizu on 11/16/17.
 */

public class PurchasePyshicalPending extends PurchasePyshicalAbstract implements PurchasePyshicalStatus {

    public PurchasePyshicalPending(PurchaseHistory purchaseHistory, Context context) {
        super(purchaseHistory, context);
    }

    @Override
    public String getDeliveryStatus() {
        return "2";
    }

    @Override
    public String getStatus() {
        return context.getString(R.string.pending);
    }

    @Override
    public Drawable imageIcon() {
        return ContextCompat.getDrawable(context, R.drawable.on_hold_icon);
    }

    @Override
    public String getInstruction() {
        return context.getString(R.string.redeemption_status_pending);
    }

    @Override
    public int getInstructionVisibility() {
        return View.VISIBLE;
    }
}
