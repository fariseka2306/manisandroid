package com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.R;
import com.ebizu.manis.mvp.reward.purchasevoucher.PurchaseRewardActivity;
import com.ebizu.manis.view.manis.toolbar.ToolbarOrderSummary;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ebizu on 10/3/17.
 */

public class OrderSummaryActivity extends PurchaseRewardActivity {

    @BindView(R.id.toolbar_order_summary)
    ToolbarOrderSummary toolbarOrderSummary;
    @BindView(R.id.order_summary_view)
    OrderSummaryView orderSummaryView;

    protected OrderSummaryPresenter orderSummaryPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_order_summary);
        ButterKnife.bind(this);
        toolbarOrderSummary.setActivity(this);
        orderSummaryView.setActivity(this);
        orderSummaryView.setOrderSummaryView(shoppingCart, order);
        orderSummaryView.setReward(reward);
        orderSummaryPresenter = orderSummaryView.getOrderSummaryPresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @OnClick(R.id.button_continue)
    void onClickContinue() {
        starTransaction();
    }

    protected void starTransaction() {

    }
}
