package com.ebizu.manis.mvp.store.storenearby;

import com.ebizu.manis.manager.multiplier.MultiplierAll;
import com.ebizu.manis.sdk.rest.data.StoresNearby;
import com.ebizu.manis.service.manis.requestbody.StoreBody;
import com.ebizu.manis.service.manis.requestbody.StoreSearchBody;

/**
 * Created by ebizu on 9/12/17.
 */

public class StoreHelper {

    public static StoreBody getStoresBody(String merchantTier, int multiplier, int page) {
        StoreBody storeBody = new StoreBody();
        storeBody.setMerchantTier(merchantTier);
        storeBody.setMultiplier(multiplier);
        storeBody.setPage(page);
        storeBody.setSize(20);
        return storeBody;
    }

    public static StoreSearchBody getStoreSearchBody(String keyword, int multiplier, int page) {
        StoreSearchBody storeSearchBody = new StoreSearchBody();
        storeSearchBody.setKeyword(keyword);
        storeSearchBody.setMultiplier(multiplier);
        storeSearchBody.setSize(20);
        storeSearchBody.setPage(page);
        return storeSearchBody;
    }

    public static StoreBody firstStoresBody() {
        StoreBody storeBody = new StoreBody();
        storeBody.setSize(20);
        storeBody.setMultiplier(new MultiplierAll().multiplier());
        storeBody.setPage(1);
        storeBody.setMerchantTier(new MultiplierAll().merchantTier());
        return storeBody;
    }

}
