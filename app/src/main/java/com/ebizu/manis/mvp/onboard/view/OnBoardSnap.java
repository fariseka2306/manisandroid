package com.ebizu.manis.mvp.onboard.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;

/**
 * Created by ebizu on 8/22/17.
 */

public class OnBoardSnap extends OnBoard
        implements IOnBoard {

    public OnBoardSnap(Context context){
        super(context);
    }

    @Override
    public int index() {
        return 0;
    }

    @Override
    public String title() {
        return context.getResources().getString(R.string.exp_snaptrack);
    }

    @Override
    public String messageBody() {
        return context.getResources().getString(R.string.exp_snaptrack_detali);
    }

    @Override
    public int colorBackground() {
        return ContextCompat.getColor(context, R.color.color_graph_red);
    }

    @Override
    public int rawVideo() {
        return R.raw.onboard1;
    }
}
