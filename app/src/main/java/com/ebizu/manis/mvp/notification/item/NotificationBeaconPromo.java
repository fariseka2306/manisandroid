package com.ebizu.manis.mvp.notification.item;

import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;
import com.ebizu.manis.database.litepal.NotificationDatabase;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.view.holder.NotificationSwipeViewHolder;

/**
 * Created by ebizu on 8/28/17.
 */

public class NotificationBeaconPromo extends com.ebizu.manis.mvp.notification.item.NotificationItem implements NotificationInterface {

    public NotificationBeaconPromo(NotificationTableList notificationTableList, NotificationSwipeViewHolder notificationSwipeViewHolder) {
        super(notificationTableList, notificationSwipeViewHolder);
    }

    @Override
    public String notificationType() {
        return NotificationDatabase.TYPE_BEACON;
    }

    @Override
    public void setNotificationView() {
        String promoDesc = notificationTableList.getBeaconPromoDesc();
        notificationSwipeViewHolder.setTitle(promoDesc);
        long savedTime = notificationTableList.getSavedTime();
        notificationSwipeViewHolder.setTime(UtilManis.getTimeAgo(savedTime, notificationSwipeViewHolder.getContext()));
        setIconReadStatus();
        notificationTableList.setRead(true);
        notificationTableList.save();
    }

    private void setIconReadStatus(){
        if (notificationTableList.isRead()) {
            notificationSwipeViewHolder.setItemIconDrawable(ContextCompat.getDrawable(notificationSwipeViewHolder.getContext(), R.drawable.notifications_beacon_icon_read));
        } else {
            notificationSwipeViewHolder.setItemIconDrawable(ContextCompat.getDrawable(notificationSwipeViewHolder.getContext(), R.drawable.notifications_beacon_icon));
        }
    }
}
