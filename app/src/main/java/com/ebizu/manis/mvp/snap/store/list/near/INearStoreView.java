package com.ebizu.manis.mvp.snap.store.list.near;

import com.ebizu.manis.model.snap.SnapStoreResult;
import com.ebizu.manis.mvp.snap.store.list.IListSnapStorePresenter;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public interface INearStoreView extends IBaseView {

    void addNearStores(SnapStoreResult snapStoreResult, LoadType loadType);

    NearStorePresenter getNearStorePresenter();

    void setOnSnapAbleListener(IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener);
}