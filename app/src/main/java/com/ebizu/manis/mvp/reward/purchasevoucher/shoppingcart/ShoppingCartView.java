package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.purchasevoucher.CustomerDetails;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.model.purchasevoucher.Product;
import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.manis.textinput.ShopCartNumberTextInput;
import com.ebizu.manis.view.manis.textinput.ShopCartTextInput;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/*** Created by ebizu on 9/27/17.*/
public abstract class ShoppingCartView extends BaseView implements IShoppingCartView {
    @BindView(R.id.detail_shop_cart)
    DetailShoppingCart detailShoppingCart;
    @BindView(R.id.text_input_name)
    ShopCartTextInput textInputName;
    @BindView(R.id.text_input_email)
    ShopCartTextInput textInputEmail;
    @BindView(R.id.text_input_phone_number)
    ShopCartNumberTextInput textInputPhoneNumber;
    @BindView(R.id.root_shopping_cart)
    ConstraintLayout rootLayout;
    View bottomView;
    private ShoppingCartPresenter shoppingCartPresenter;
    private ShoppingCartValidation shoppingCartValidation;
    private OnValidationListener onValidationListener;
    private Order order;
    private RewardVoucher reward;
    private ShoppingCart shoppingCart;
    private Account account;
    protected ArrayList<ShopCartTextInput> shopCartTextInputs;

    public ShoppingCartView(Context context) {
        super(context);
        createView(context);
    }

    public ShoppingCartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ShoppingCartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ShoppingCartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_shopping_cart, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        shoppingCartValidation = new ShoppingCartValidation();
        addBottomView();
        initView();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
    }

    protected void initView() {
        shopCartTextInputs = new ArrayList<>();
        shopCartTextInputs.add(textInputName);
        shopCartTextInputs.add(textInputEmail);
        shopCartTextInputs.add(textInputPhoneNumber);
    }

    private void addBottomView() {
        bottomView = LayoutInflater.from(getContext()).inflate(bottomLayoutId(), null, false);
        rootLayout.addView(bottomView);
        ConstraintSet set = new ConstraintSet();
        set.clone(rootLayout);
        set.connect(bottomView.getId(), ConstraintSet.TOP, textInputPhoneNumber.getId(), ConstraintSet.BOTTOM);
        set.applyTo(rootLayout);
        initBottomViewChildren(bottomView);
    }

    @Override
    public void setShoppingCartView(RewardVoucher reward) {
//        Localization please
        account = getManisSession().getAccountSession();
        this.reward = reward;
        textInputName.setInput(account.getAccScreenName());
        textInputEmail.setInput(account.getEmail());
        textInputPhoneNumber.setInput(account.getAccMsisdn());
        textInputPhoneNumber.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        textInputPhoneNumber.setInputFilterWithLength("[0-9 +]", 16);
    }

    @Override
    public void setOrderDetailView(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
        String currency = account.getCurrency().getIso2();
        detailShoppingCart.setViewValue(getProduct(), currency);
        detailShoppingCart.addVoucher(shoppingCart.getVouchers());
    }

    @Override
    public void showMessage(String message) {
        String errorTitle = getBaseActivity().getString(R.string.error);
        getBaseActivity().showAlertDialog(errorTitle, message, true, "Yes", (dialogInterface, i) -> {
            //do nothing
        }, null, null);
    }

    @Override
    public void validFormOrderDetail() {
        if (onValidationListener != null) onValidationListener.onValid();
    }

    public ShoppingCartPresenter getShoppingCartPresenter() {
        if (null == shoppingCartPresenter) {
            shoppingCartPresenter = new ShoppingCartPresenter();
            shoppingCartPresenter.attachView(this);
        }
        return shoppingCartPresenter;
    }

    public void setOnValidationListener(OnValidationListener onValidationListener) {
        this.onValidationListener = onValidationListener;
    }

    public Order getOrder() {
        if (order == null) order = new Order();
        order.setCustomerDetails(getCustomerDetails());
        order.setProduct(getProduct());
        return order;
    }

    protected CustomerDetails getCustomerDetails() {
        CustomerDetails customerDetails = new CustomerDetails();
        customerDetails.setEmail(textInputEmail.getText());
        customerDetails.setName(textInputName.getText());
        customerDetails.setPhoneNo(textInputPhoneNumber.getText());
        return customerDetails;
    }

    private Product getProduct() {
        Product product = new Product();
        product.setName(reward.getName());
        product.setAmount(Double.parseDouble(shoppingCart.getAmount()));
        product.setOrderId(shoppingCart.getOrderId());
        product.setQty(1);
        product.setTotalAmount(Double.parseDouble(shoppingCart.getTotal()));
        return product;
    }

    @OnClick(R.id.button_continue)
    void onClickContinue() {
        shoppingCartValidation.check(getContext(), shopCartTextInputs, this);
    }

    @OnClick(R.id.text_view_cancel)
    void onClickCancel() {
        getShoppingCartPresenter().cancelPayment(shoppingCart.getOrderId());
        getBaseActivity().finish();
    }

    public abstract int bottomLayoutId();

    public abstract void initBottomViewChildren(View view);

    public interface OnValidationListener {
        void onValid();
    }
}