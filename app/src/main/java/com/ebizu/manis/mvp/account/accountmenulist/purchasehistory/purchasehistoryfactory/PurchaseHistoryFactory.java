package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistoryfactory;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.ebizu.manis.model.PurchaseHistory;

/**
 * Created by halim_ebizu on 10/17/17.
 */

public class PurchaseHistoryFactory {

    private PurchaseHistoryAbstract[] purchaseHistories;
    private PurchaseHistory purchaseHistory;

    public PurchaseHistoryFactory(PurchaseHistory purchaseHistory, Context context) {
        this.purchaseHistory = purchaseHistory;
        instancePurchaseHistories(context, purchaseHistory);
    }

    private void instancePurchaseHistories(Context context, PurchaseHistory purchaseHistory) {
        purchaseHistories = new PurchaseHistoryAbstract[]{
                new PurchaseStatusPaid(purchaseHistory, context),
                new PurchaseStatusUnpaid(purchaseHistory, context),
                new PurchaseStatusPending(purchaseHistory, context),
                new PurchaseStatusProcessing(purchaseHistory, context)
        };
    }

    public Drawable getImageDrawable() {
        Drawable imageDrawable = null;
        for (PurchaseStatus purchaseStatus : purchaseHistories) {
            if (purchaseHistory.getPaymentStatus().equalsIgnoreCase(purchaseStatus.getStatus())) {
                imageDrawable = purchaseStatus.imageIcon();
                break;
            }
        }
        return imageDrawable;
    }

    public int getColor() {
        int color = 0;
        for (PurchaseStatus purchaseStatus : purchaseHistories) {
            if (purchaseHistory.getPaymentStatus().equalsIgnoreCase(purchaseStatus.getStatus())) {
                color = purchaseStatus.color();
                break;
            }
        }
        return color;
    }

    public boolean isShowInfo() {
        boolean showInfo = false;
        for (PurchaseStatus purchaseStatus : purchaseHistories) {
            if (purchaseHistory.getPaymentStatus().equalsIgnoreCase(purchaseStatus.getStatus())) {
                showInfo = purchaseStatus.isShowInfo();
                break;
            }
        }
        return showInfo;
    }
}
