package com.ebizu.manis.mvp.reward.purchasevoucher.ordersummary;

import com.ebizu.manis.model.purchasevoucher.MolPaymentResult;
import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.sdk.reward.models.Reward;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.model.purchasevoucher.Purchase;
import com.ebizu.manis.root.IBaseView;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;

/**
 * Created by ebizu on 10/3/17.
 */

public interface IOrderSummaryView
        extends IBaseView {

    void setOrderSummaryView(ShoppingCart shoppingCart, Order order);

    void setReward(RewardVoucher reward);

    void setTransactionResult(TransactionResult transactionResult);

    void setTransactionResult(MolPaymentResult molPaymentResult);

    void showDialogTransactionStatus(Purchase purchase);

    void showDialogRedeemPin(String refCode);

}
