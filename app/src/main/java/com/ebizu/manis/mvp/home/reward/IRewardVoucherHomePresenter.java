package com.ebizu.manis.mvp.home.reward;

import com.ebizu.manis.mvp.home.HomeFragment;
import com.ebizu.manis.service.reward.requestbody.RewardGeneralListBody;

/**
 * Created by FARIS_mac on 11/10/17.
 */

public interface IRewardVoucherHomePresenter {

    void loadRewardVoucher(RewardGeneralListBody rewardGeneralListBody, HomeFragment homeFragment);

}
