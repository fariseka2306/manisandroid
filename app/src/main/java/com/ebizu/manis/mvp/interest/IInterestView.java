package com.ebizu.manis.mvp.interest;

import com.ebizu.manis.model.Interest;
import com.ebizu.manis.root.IBaseView;

import java.util.ArrayList;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public interface IInterestView extends IBaseView {

    void setTitleFisrtLogin();

    void enableSaveButton();

    void unableSaveButton(int pickSize);

    void visibleSaveButton();

    void invisibleSaveButton();

    void setInterestView(ArrayList<Interest> interests);

    void setOnSuccessSaveListener(OnInterestListener onInterestListener);

    void showProgressSave();

    void dismissProgressSave();

    interface OnInterestListener {
        void onSuccessSave();
    }
}