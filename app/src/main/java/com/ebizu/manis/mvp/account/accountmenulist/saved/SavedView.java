package com.ebizu.manis.mvp.account.accountmenulist.saved;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.NestedListView;
import com.ebizu.manis.model.saved.Saved;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.adapter.SavedOfferAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mac on 8/21/17.
 */

public class SavedView extends BaseView implements ISavedView, View.OnClickListener {

    @BindView(R.id.button_browse_stores)
    Button buttonStores;
    @BindView(R.id.button_browse_rewards)
    Button buttonRewards;
    @BindView(R.id.listview_offer)
    RecyclerView listViewOffer;
    @BindView(R.id.layout_empty)
    LinearLayout linearLayoutEmpty;
    @BindView(R.id.lin_list)
    LinearLayout linearLayoutList;
    @BindView(R.id.rel_all)
    RelativeLayout relAll;
    @BindView(R.id.textview_all)
    TextView textViewAll;
    @BindView(R.id.rel_rewards)
    RelativeLayout relRewards;
    @BindView(R.id.textview_rewards)
    TextView textViewRewards;
    @BindView(R.id.rel_offers)
    RelativeLayout relOffer;
    @BindView(R.id.textview_offers)
    TextView textViewOffers;

    private ISavedPresenter iSavedPresenter;
    private Saved saved;

    public SavedView(Context context) {
        super(context);
        createView(context);
    }

    public SavedView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public SavedView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SavedView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.account_saved, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        attachPresenter(new SavedPresenter());
        ButterKnife.bind(this, view);
        initView();
    }

    private void initView() {
        buttonStores.setOnClickListener(this);
        buttonRewards.setOnClickListener(this);
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iSavedPresenter = (ISavedPresenter) iBaseViewPresenter;
        iSavedPresenter.attachView(this);
    }

    @Override
    public ISavedPresenter getSavedPresenter() {
        return iSavedPresenter;
    }

    @Override
    public void setSaved(Saved saved) {
        this.saved = saved;
        if (!saved.getOffers().isEmpty()) {
            linearLayoutEmpty.setVisibility(INVISIBLE);
        }
        dismissProgressBar();
        SavedOfferAdapter savedOfferAdapter = new SavedOfferAdapter(getContext(), saved.getOffers());
        listViewOffer.setAdapter(savedOfferAdapter);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finishActivity(getBaseActivity());
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void finishActivity(Activity activity) {
        activity.finish();
        activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @OnClick(R.id.rel_all)
    void showAllSaved() {
        relAll.setBackgroundResource(R.drawable.bg_btn_rounded_pink_full);
        relRewards.setBackgroundResource(R.drawable.bg_btn_rounded_pink);
        relOffer.setBackgroundResource(R.drawable.bg_btn_rounded_pink);
        textViewAll.setTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
        textViewRewards.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));
        textViewOffers.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));

        if (!saved.getOffers().isEmpty()) {
            linearLayoutList.setVisibility(VISIBLE);
            linearLayoutEmpty.setVisibility(INVISIBLE);
        }
        dismissProgressBar();
        SavedOfferAdapter savedOfferAdapter = new SavedOfferAdapter(getContext(), saved.getOffers());
        listViewOffer.setAdapter(savedOfferAdapter);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.SAVED_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button All"
        );
    }

    @OnClick(R.id.rel_rewards)
    void showRewardsSaved() {
        relAll.setBackgroundResource(R.drawable.bg_btn_rounded_pink);
        relRewards.setBackgroundResource(R.drawable.bg_btn_rounded_pink_full);
        relOffer.setBackgroundResource(R.drawable.bg_btn_rounded_pink);
        textViewAll.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));
        textViewRewards.setTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
        textViewOffers.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));

        linearLayoutList.setVisibility(INVISIBLE);
        linearLayoutEmpty.setVisibility(VISIBLE);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.SAVED_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Reward"
        );
    }

    @OnClick(R.id.rel_offers)
    void showOffersSaved() {
        relAll.setBackgroundResource(R.drawable.bg_btn_rounded_pink);
        relRewards.setBackgroundResource(R.drawable.bg_btn_rounded_pink);
        relOffer.setBackgroundResource(R.drawable.bg_btn_rounded_pink_full);
        textViewAll.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));
        textViewRewards.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPink));
        textViewOffers.setTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));

        if (!saved.getOffers().isEmpty()) {
            linearLayoutList.setVisibility(VISIBLE);
            linearLayoutEmpty.setVisibility(INVISIBLE);
        }
        dismissProgressBar();
        SavedOfferAdapter savedOfferAdapter = new SavedOfferAdapter(getContext(), saved.getOffers());
        listViewOffer.setAdapter(savedOfferAdapter);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.SAVED_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Offer"
        );
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_browse_stores) {
            getBaseActivity().setResult(ConfigManager.Saved.STORES_RESULT_CODE);
            finishActivity(getBaseActivity());
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.SAVED_ACTIVITY,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Browse Store"
            );
        } else {
            getBaseActivity().setResult(ConfigManager.Saved.REWARDS_RESULT_CODE);
            finishActivity(getBaseActivity());
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.SAVED_ACTIVITY,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Browse Rewards"
            );
        }
    }
}
