package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory;

import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.service.manis.requestbody.PurchaseHistoryBody;
import com.ebizu.manis.service.reward.response.WrapperPurchaseHistory;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public class PurchaseHistoryPresenter extends BaseViewPresenter implements IPurchaseHistoryPresenter {

    private PurchaseHistoryView purchaseHistoryView;
    private Subscription subsPurchase;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        purchaseHistoryView = (PurchaseHistoryView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsPurchase != null) subsPurchase.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadPurchaseHistory(PurchaseHistoryBody purchaseHistoryBody, IBaseView.LoadType loadType) {
        releaseSubscribe(0);
        purchaseHistoryView.showProgressBar(loadType);
        subsPurchase = getRewardApi().listPurchaseHistory(purchaseHistoryBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperPurchaseHistory>(purchaseHistoryView) {
                    @Override
                    public void onNext(WrapperPurchaseHistory wrapperPurchaseHistory) {
                        super.onNext(wrapperPurchaseHistory);
                        purchaseHistoryView.dismissProgressBar(loadType);
                        purchaseHistoryView.setPurchaseHistory(wrapperPurchaseHistory.getPurchaseHistoryPaging(), loadType);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        purchaseHistoryView.dismissProgressBar(loadType);
                        purchaseHistoryView.loadPurchaseHistoryViewFail(loadType);
                    }
                });
    }
}
