package com.ebizu.manis.mvp.snap.store.list.searchresult;

import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;

/**
 * Created by Ebizu-User on 08/08/2017.
 */

public interface ISearchResultPresenter extends IBaseViewPresenter {

    void findMerchant(String keyword, int page, IBaseView.LoadType loadType);
}
