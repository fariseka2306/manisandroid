package com.ebizu.manis.mvp.account.profile;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.Point;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/14/2017.
 */

public class AccountProfileView extends BaseView implements IAccountProfileView {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.text_view_name_profile)
    TextView textViewNameProfile;
    @BindView(R.id.image_view_photo_profile)
    ImageView imageViewPhotoProfile;
    @BindView(R.id.text_view_point)
    TextView textViewPoint;
    @BindView(R.id.rel_logout)
    RelativeLayout relLogoutBtn;

    private IAccountProfilePresenter iAccountProfilePresenter;

    public AccountProfileView(Context context) {
        super(context);
        createView(context);
    }

    public AccountProfileView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public AccountProfileView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AccountProfileView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.account_profile, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new AccountProfilePresenter());
    }

    @Override
    public void setAccountProfile(Account account) {
        textViewNameProfile.setText(account.getAccScreenName());
        ImageUtils.loadImage(getContext(), account.getAccPhoto(), imageViewPhotoProfile);
        getManisSession().updateSession(account);
    }

    @Override
    public void setPointProfile(Point point) {
        if (point.getPoint() != null) {
            textViewPoint.setText(String.format(Locale.getDefault(), "%,d", point.getPoint()));
        } else {
            Log.i(TAG, "loadViewPointProfile");
        }
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        this.iAccountProfilePresenter = (IAccountProfilePresenter) iBaseViewPresenter;
        this.iAccountProfilePresenter.attachView(this);
    }

    @Override
    public IAccountProfilePresenter getProfilePresenter() {
        return iAccountProfilePresenter;
    }

    @Override
    public void setOnClickSignOutSccess(IAccountProfileView.OnClickListener onClickSignOutSccess) {
        relLogoutBtn.setOnClickListener((v) -> {
            onClickSignOutSccess.onClickSignOut();
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.FRAGMENT_ACCOUNT,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Logout");
        });
    }
}
