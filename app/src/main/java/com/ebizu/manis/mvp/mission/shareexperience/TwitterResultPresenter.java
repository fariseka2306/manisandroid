package com.ebizu.manis.mvp.mission.shareexperience;

import android.content.Context;

public class TwitterResultPresenter implements ITwitterResult {

    private final String TAG = getClass().getSimpleName();
    private Context context;

    public TwitterResultPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void onSuccessTweet() {
        if (context != null)
            ((ShareExperienceDetailActivity) context).postShareExperience();
    }
}
