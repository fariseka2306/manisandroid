package com.ebizu.manis.mvp.account.accountmenulist.profile.activityresult;

import android.content.Intent;

import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileActivity;

/**
 * Created by ebizu on 10/13/17.
 */

public class ProfileRequest {

    private ProfileRequestCode[] profileRequestCodes;

    public ProfileRequest(ProfileActivity profileActivity) {
        profileRequestCodes = new ProfileRequestCode[]{
                new UpdateProfilePictRequest(profileActivity),
                new LuckyDrawSnapRequest(profileActivity),
                new SignUpOtpRequest(profileActivity)
        };
    }

    public void doRequest(int requestCode, int resultCode, Intent intent) {
        for (ProfileRequestCode profileRequestCode : profileRequestCodes) {
            if (profileRequestCode.requestCode() == requestCode) {
                profileRequestCode.doRequest(resultCode, intent);
            }
        }
    }
}
