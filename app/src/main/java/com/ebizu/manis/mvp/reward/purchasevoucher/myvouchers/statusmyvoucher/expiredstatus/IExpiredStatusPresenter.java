package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.statusmyvoucher.expiredstatus;

import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.reward.requestbody.RewardMyVoucherBody;

/**
 * Created by Raden on 11/14/17.
 */

public interface IExpiredStatusPresenter  extends IBaseViewPresenter {

    void getExpiredVouchers(RewardMyVoucherBody rewardMyVoucherBody, IBaseView.LoadType loadType ,String expired);

}
