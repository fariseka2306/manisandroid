package com.ebizu.manis.mvp.store.storenearby;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.multiplier.MultiplierAll;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.service.manis.requestbody.StoreBody;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by firef on 7/5/2017.
 */

public class StoreNearbyFragment extends BaseFragment {

    @BindView(R.id.store_nearby_view)
    StoreNearbyView storeNearbyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_store_nearby, container, false);
        ButterKnife.bind(this, view);
        initializePresenter();
        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible && getContext() != null)
            getAnalyticManager().trackScreenView(ConfigManager.Analytic.ScreenView.FRAGMENT_STORES_NEARBY);
    }

    private void initializePresenter() {
        storeNearbyView.attachPresenter(new StoreNearbyPresenter());
        storeNearbyView.setActivity((BaseActivity) getActivity());
        storeNearbyView.getStoreNearbyPresenter()
                .loadNearStores(StoreHelper.firstStoresBody(), IBaseView.LoadType.CLICK_LOAD);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public StoreNearbyView getStoreNearbyView() {
        return storeNearbyView;
    }
}
