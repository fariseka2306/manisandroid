package com.ebizu.manis.mvp.account.accountmenulist.settings.aboutthisapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.accountlistsettingsabout.AccountListSettingsAbout;
import com.ebizu.manis.manager.accountlistsettingsabout.AccountListSettingsAboutManager;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.adapter.account.AccountListSettingAboutAdapter;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/21/2017.
 */

public class AboutThisAppsActivity extends BaseActivity implements AccountListSettingAboutAdapter.AccountMenuListener {

    @BindView(R.id.recyclerview_settings_about)
    RecyclerView recyclerViewSettingAbout;
    @BindView(R.id.about_item_name)
    TextView textViewAbout;
    @BindView(R.id.toolbar)
    ToolbarView toolbarView;

    AccountListSettingAboutAdapter accountListSettingAboutThisAppAdapter;
    private LinearLayoutManager linearLayoutManager;
    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        context = this;
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        Activity activity = (Activity) context;
        toolbarView.setTitle(R.string.ab_txt_title, R.drawable.navigation_back_btn);
        this.accountListSettingAboutThisAppAdapter = new AccountListSettingAboutAdapter(context, new ArrayList<>(), activity);
        accountListSettingAboutThisAppAdapter.setAccountListener(this);
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewSettingAbout.setLayoutManager(linearLayoutManager);
        recyclerViewSettingAbout.setAdapter(accountListSettingAboutThisAppAdapter);
        textViewAbout.setText(UtilManis.fromHtml(getString(R.string.sp_desc)));
        showViewSettingsAbout();
    }

    private void showViewSettingsAbout() {
        AccountListSettingsAboutManager.addAccountListMenu(accountListSettingAboutThisAppAdapter);
    }

    @Override
    public void onClick(AccountListSettingsAbout accountListSettingsAbout) {
        AccountListSettingsAboutManager.onClickMenu(context, accountListSettingsAbout);
        AccountListSettingsAboutManager.onClickMenu(this, accountListSettingsAbout);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finishActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
