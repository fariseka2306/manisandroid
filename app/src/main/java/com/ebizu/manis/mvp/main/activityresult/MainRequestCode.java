package com.ebizu.manis.mvp.main.activityresult;

import android.content.Intent;
import android.support.v4.view.ViewPager;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.main.MainActivity;

/**
 * Created by ebizu on 10/11/17.
 */

public class MainRequestCode implements IMainRequestCode {

    protected MainActivity mainActivity;

    protected ViewPager mPager;
    protected final int POS_HOME_VIEW = 0;
    protected final int POS_STORE_VIEW = 1;
    protected final int POS_MISSION_VIEW = 2;
    protected final int POS_REWARD_VIEW = 3;
    protected final int POS_LUCKY_DRAW_VIEW = 4;
    protected final int POS_ACCOUNT_VIEW = 5;

    public MainRequestCode(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.mPager = (ViewPager) mainActivity.findViewById(R.id.pager);
    }

    @Override
    public int requestCode() {
        return ConfigManager.Saved.SAVED_REQUEST_CODE;
    }

    @Override
    public void jobRequest(int resultCode, Intent data) {

    }
}