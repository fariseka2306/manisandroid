package com.ebizu.manis.mvp.account.spending;

import android.content.Context;

import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.Currency;
import com.ebizu.manis.model.Statistic;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.service.manis.response.WrapperStatistic;

import java.util.List;

/**
 * Created by abizu-alvio on 7/14/2017.
 */

public interface IAccountSpendView extends IBaseView {

    void setSpendCategory(Context context);

    void showViewAccountSpend(List<Statistic> statistic);

    void showViewAccountTotalSpend(WrapperStatistic.Data wrapperStatistic);

    void showCurrenctyAccount();

    void setSpendingBarViewTextView(List<Statistic> statistics);

    void setSpendingBarView(List<Statistic> statistics);

    void stopAnimationShimmer();

    void startAnimationShimmer();

    IAccountSpendPresenter getSpendPresenter();

}
