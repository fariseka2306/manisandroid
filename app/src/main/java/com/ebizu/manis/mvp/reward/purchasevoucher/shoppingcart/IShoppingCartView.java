package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by ebizu on 9/27/17.
 */

public interface IShoppingCartView extends IBaseView {

    void setShoppingCartView(RewardVoucher reward);

    void setOrderDetailView(ShoppingCart shoppingCart);

    void showMessage(String message);

    void validFormOrderDetail();
}
