package com.ebizu.manis.mvp.store.storeoffer;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.SparseArray;

import com.ebizu.manis.R;
import com.ebizu.manis.model.Offer;
import com.ebizu.manis.model.Redeem;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.OfferClaimBody;
import com.ebizu.manis.service.manis.requestbody.StorePinBody;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.service.manis.response.WrapperOfferClaim;
import com.google.gson.Gson;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by halim_ebizu on 8/23/17.
 */

public class StoreOfferDetailPresenter extends BaseViewPresenter implements IStoreOfferDetailPresenter {

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private StoreOfferDetailView storeOfferDetailView;
    private SparseArray<Subscription> subsStoreOffer;

    public StoreOfferDetailPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        storeOfferDetailView = (StoreOfferDetailView) baseView;
        subsStoreOffer = new SparseArray<>();
    }

    @Override
    public void releaseSubscribe(int key) {
        Subscription subscription = subsStoreOffer.get(key);
        if (subscription != null) {
            subscription.unsubscribe();
            subsStoreOffer.remove(key);
        }
    }

    @Override
    public void releaseAllSubscribes() {
        storeOfferDetailView.unSubscribeAll(subsStoreOffer);
    }

    @Override
    public void loadPinOffer(ManisApi manisApi, Offer offer) {
        releaseSubscribe(0);
        StorePinBody storePinBody = new StorePinBody();
        storePinBody.setId(offer.getId());
        Subscription subsLoadPinOffer = manisApi.getOfferPinned(new Gson().toJson(storePinBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<ResponseData>(storeOfferDetailView) {
                    @Override
                    public void onNext(ResponseData responseData) {
                        super.onNext(responseData);
                        storeOfferDetailView.setPinOffer(responseData);
                    }
                });
        subsStoreOffer.put(0, subsLoadPinOffer);
    }

    @Override
    public void loadUnpinOffer(ManisApi manisApi, Offer offer) {
        releaseSubscribe(1);
        StorePinBody storePinBody = new StorePinBody();
        storePinBody.setId(offer.getId());
        Subscription subsLoadUnpinOffer = manisApi.getOfferUnpinned(new Gson().toJson(storePinBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<ResponseData>(storeOfferDetailView) {
                    @Override
                    public void onNext(ResponseData responseData) {
                        super.onNext(responseData);
                        storeOfferDetailView.setUnpiOffer(responseData);
                    }
                });
        subsStoreOffer.put(1, subsLoadUnpinOffer);
    }

    @Override
    public void loadClaimOffer(ManisApi manisApi, OfferClaimBody offerClaimBody) {
        releaseSubscribe(2);
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        Subscription subsLoadClaimOffer = manisApi.postClaimOffer(new Gson().toJson(offerClaimBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperOfferClaim>(storeOfferDetailView) {
                    @Override
                    public void onNext(WrapperOfferClaim wrapperOfferClaim) {
                        super.onNext(wrapperOfferClaim);
                        progressDialog.dismiss();
                        storeOfferDetailView.claimOffer(wrapperOfferClaim.getRedeem());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                    }
                });
        subsStoreOffer.put(2, subsLoadClaimOffer);
    }
}
