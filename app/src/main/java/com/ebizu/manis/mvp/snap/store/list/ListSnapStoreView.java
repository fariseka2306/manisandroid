package com.ebizu.manis.mvp.snap.store.list;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.multiplier.MultiplierMenuManager;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileActivity;
import com.ebizu.manis.mvp.snap.form.storedetail.ReceiptDetailActivity;
import com.ebizu.manis.mvp.snap.store.SnapStoreHelper;
import com.ebizu.manis.mvp.snap.store.list.near.NearStorePresenter;
import com.ebizu.manis.mvp.snap.store.list.near.NearStoreView;
import com.ebizu.manis.mvp.snap.store.list.searchresult.SearchResultPresenter;
import com.ebizu.manis.mvp.snap.store.list.searchresult.SearchResultView;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.view.manis.search.SearchView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 03/08/2017.
 */

public class ListSnapStoreView extends BaseView
        implements IListSnapStoreView {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.near_store_view)
    NearStoreView nearStoreView;
    @BindView(R.id.history_snap_store_view)
    ListHistorySnapStoreView historySnapStoreView;
    @BindView(R.id.search_result_view)
    SearchResultView searchResultView;
    @BindView(R.id.text_view_min_char)
    TextView textViewMinChar;

    private ListSnapStorePresenter listSnapStorePresenter;

    public ListSnapStoreView(Context context) {
        super(context);
        createView(context);
    }

    public ListSnapStoreView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ListSnapStoreView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ListSnapStoreView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_snap, null, false);
        addView(view, new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        listSnapStorePresenter = new ListSnapStorePresenter();
        listSnapStorePresenter.attachView(this);
        nearStoreView.attachPresenter(new NearStorePresenter());
        searchResultView.attachPresenter(new SearchResultPresenter());
        searchView.setHint(getContext().getString(R.string.fs_txt_search));
        searchView.setOnTextChange(new SearchView.OnTextChangedListener() {
            @Override
            public void onValid(String keyword) {
                visibleView(searchResultView);
                searchResultView.getSearchResultPresenter().findMerchant(keyword, 1, IBaseView.LoadType.SEARCH_LOAD);
                searchResultView.setKeyword(keyword);
                getAnalyticManager().trackEvent(
                        ConfigManager.Analytic.Category.SNAP,
                        ConfigManager.Analytic.Action.SEARCH,
                        "Keyword '".concat(keyword).concat("'")
                );
            }

            @Override
            public void onInvalid() {
                visibleView(textViewMinChar);
            }

            @Override
            public void onEmptyText() {
                visibleView(historySnapStoreView);
            }
        });
    }

    @Override
    public void startActivityReceiptDetail(Store store) {
        Intent intent = new Intent(getContext(), ReceiptDetailActivity.class);
        putIntentExtraReceiptDetail(intent, store);
        getBaseActivity().startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
    }

    @Override
    public void startActivityProfile(Store store) {
        getBaseActivity().showAlertDialog("Lucky Draw", getContext().getString(R.string.ld_dialog_complete_profile),
                true, R.drawable.manis_logo,
                "Ok", (dialogInterface, i) -> {
                    Intent intent = new Intent(getContext(), ProfileActivity.class);
                    intent.putExtra(ConfigManager.UpdateProfile.UPDATE_PROFILE_TYPE_INTENT, ConfigManager.UpdateProfile.UPDATE_PROFILE_LUCKY_DRAW_TYPE);
                    putIntentExtraReceiptDetail(intent, store);
                    getBaseActivity().startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
                }, null, null);
    }

    private void putIntentExtraReceiptDetail(Intent intent, Store store) {
        boolean isLuckyDraw = MultiplierMenuManager.isLuckyDraw(store.getMerchantTier());
        intent.putExtra(ConfigManager.Snap.LUCKY_DRAW_INTENT_DATA, isLuckyDraw);
        intent.putExtra(ConfigManager.Snap.MERCHANT_DATA, SnapStoreHelper.createReceiptStore(store));
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        nearStoreView.setActivity(baseActivity);
        historySnapStoreView.setActivity(baseActivity);
        searchResultView.setActivity(baseActivity);
        addCheckSnapAbleListener();
    }

    public void visibleView(View view) {
        nearStoreView.setVisibility(INVISIBLE);
        historySnapStoreView.setVisibility(INVISIBLE);
        searchResultView.setVisibility(INVISIBLE);
        textViewMinChar.setVisibility(INVISIBLE);
        view.setVisibility(VISIBLE);
    }

    private void addCheckSnapAbleListener() {
        nearStoreView.setOnSnapAbleListener((store) -> listSnapStorePresenter.isSnapAble(store));
        historySnapStoreView.setOnCheckSnapAbleListener(store -> listSnapStorePresenter.isSnapAble(store));
        searchResultView.setOnSnapAbleListener(store -> listSnapStorePresenter.isSnapAble(store));
    }

    public NearStoreView getNearStoreView() {
        return nearStoreView;
    }

    public ListHistorySnapStoreView getHistorySnapStoreView() {
        return historySnapStoreView;
    }

}