package com.ebizu.manis.mvp.account.accountmenulist.profile.activityresult;

import android.content.Intent;

import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileActivity;

/**
 * Created by ebizu on 10/13/17.
 */

public class ProfileRequestCode implements IProfileRequest {

    protected ProfileActivity profileActivity;


    public ProfileRequestCode(ProfileActivity profileActivity) {
        this.profileActivity = profileActivity;
    }

    @Override
    public int requestCode() {
        return 0;
    }

    @Override
    public void doRequest(int resultCode, Intent data) {

    }
}
