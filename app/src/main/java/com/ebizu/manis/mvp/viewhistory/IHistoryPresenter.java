package com.ebizu.manis.mvp.viewhistory;

import com.ebizu.manis.model.ViewHistoryParam;
import com.ebizu.manis.root.IBaseActivityPresenter;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by Raden on 7/24/17.
 */

public interface IHistoryPresenter extends IBaseActivityPresenter {

    void loadViewHistory(ViewHistoryParam viewHistoryParam, IBaseView.LoadType loadType);
}
