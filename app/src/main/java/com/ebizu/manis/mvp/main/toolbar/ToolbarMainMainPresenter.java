package com.ebizu.manis.mvp.main.toolbar;

import android.util.Log;

import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

import org.litepal.crud.DataSupport;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raden on 8/21/17.
 */

public class ToolbarMainMainPresenter extends BaseViewPresenter implements IToolbarMainPresenter {

    private final String TAG = getClass().getSimpleName();


    private BaseActivity baseActivity;
    private ToolbarMainMainView toolbarMainMainView;
    private Subscription subsToolbarMain;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        toolbarMainMainView = (ToolbarMainMainView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsToolbarMain != null) subsToolbarMain.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadGetPoint(ManisApi manisApi) {
        releaseSubscribe(0);
        toolbarMainMainView.prepareLoadPoint();
        toolbarMainMainView.startAnimationShimmer();
        subsToolbarMain = manisApi.getPoint()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        wrapperPoint -> {
                            toolbarMainMainView.stopAnimationShimmer();
                            toolbarMainMainView.loadViewGetPoint(wrapperPoint.getPoint());
                        },
                        throwable -> {
                            toolbarMainMainView.stopAnimationShimmer();
                            Log.e(TAG, throwable.getMessage());
                        }
                );
    }

    @Override
    public void loadTotalNotifUnread() {
        List<NotificationTableList> notificationList =
                DataSupport.where("read = ?", "0").find(NotificationTableList.class);
        toolbarMainMainView.setCountNotification(notificationList.size());
    }
}
