package com.ebizu.manis.mvp.account.accountmenulist.snapearnguide;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ColorTransformer;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ViewPagerCustomDuration;
import com.ebizu.manis.manager.multiplier.MultiplierMenuManager;
import com.ebizu.manis.model.Brand;
import com.ebizu.manis.model.InterestsStore;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.fragment.SnapGuideFirstFragment;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.fragment.SnapGuideFourthFragment;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.fragment.SnapGuideFourthStichFragment;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.fragment.SnapGuideSecondFragment;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.fragment.SnapGuideThirdFragment;
import com.ebizu.manis.mvp.branddetail.BrandDetailActivity;
import com.ebizu.manis.mvp.snap.SnapActivity;
import com.ebizu.manis.mvp.snap.form.storedetail.ReceiptDetailActivity;
import com.ebizu.manis.mvp.snap.store.SnapStoreActivity;
import com.ebizu.manis.mvp.store.storecategorydetail.StoreCategoryDetailActivity;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;
import com.ebizu.manis.view.adapter.SnapGuideAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import static com.ebizu.manis.helper.UtilStatic.REQUEST_SNAP;

public class SnapGuideActivity extends BaseActivity {

    private Context context;
    private static final int NUM_PAGES = 4;
    private int SCROLL_DURATION_FACTOR = 3;
    public static int FROM_SNAP = 2;
    public static int FROM_BRAND = 1;
    private ArrayList<Integer> arrayColor = new ArrayList<>();
    private CirclePageIndicator mIndicator;
    private ViewPagerCustomDuration mPager;
    private int colorTransparent = 0x00000000;
    private int colorGrey = 0xFFE4E4E4;
    private int colorRed = 0xFFFF003F;
    private int strokeWidth = 1;
    private int snapEarnGuideType;
    private DeviceSession deviceSession;
    private Brand brand;
    private Store store;
    private InterestsStore interestsStore;
    private OnPageChangeListener pageChangeListener = new OnPageChangeListener() {

        @Override
        public void onPageSelected(int page) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                if (page == 0) {
                    window.setStatusBarColor(ContextCompat.getColor(context, R.color.colorCircleRedBg));
                } else if (page == 1) {
                    window.setStatusBarColor(ContextCompat.getColor(context, R.color.color_graph_green));
                } else if (page == 2) {
                    window.setStatusBarColor(ContextCompat.getColor(context, R.color.colorBlueGuide));
                } else if (page == 3) {
                    window.setStatusBarColor(ContextCompat.getColor(context, R.color.colorSnapGuideStitch));
                } else {
                    window.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
                }
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snapguide);
        context = this;
        getIntentData();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(context, R.color.colorCircleRedBg));
        }
        declareView();
        Log.d("snapguide", "density " + getResources().getDisplayMetrics().density);
    }

    private void getIntentData() {
        try {
            snapEarnGuideType = getIntent().getIntExtra(ConfigManager.Snap.SNAP_EARN_GUIDE, 0);
            brand = getIntent().getParcelableExtra(ConfigManager.BrandDetails.BRAND_DETAILS);
            interestsStore = getIntent().getParcelableExtra(ConfigManager.Store.INTEREST_STORE);
            store = getIntent().getParcelableExtra(ConfigManager.Store.STORE_DETAIL_TITLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void declareView() {
        deviceSession = new DeviceSession(context);

        arrayColor.add(ContextCompat.getColor(context, R.color.colorCircleRedBg));
        arrayColor.add(ContextCompat.getColor(context, R.color.color_graph_green));
        arrayColor.add(ContextCompat.getColor(context, R.color.colorBlueGuide));
        arrayColor.add(ContextCompat.getColor(context, R.color.colorSnapGuideStitch));

        mPager = (ViewPagerCustomDuration) findViewById(R.id.snap_guide_pager);
        mPager.setAdapter(new SnapGuideAdapter(getSupportFragmentManager(), getFragments()));
        mPager.setPageTransformer(false, new ColorTransformer(arrayColor));
        mPager.setOffscreenPageLimit(NUM_PAGES);
        mPager.setScrollDurationFactor(SCROLL_DURATION_FACTOR);

        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;
        mIndicator.setRadius(NUM_PAGES * density);
        mIndicator.setPageColor(colorGrey);
        mIndicator.setFillColor(colorRed);
        mIndicator.setStrokeColor(colorTransparent);
        mIndicator.setStrokeWidth(strokeWidth * density);
        mIndicator.setOnPageChangeListener(pageChangeListener);

    }

    private List<Fragment> getFragments() {
        SnapGuideFirstFragment page1 = new SnapGuideFirstFragment();
        SnapGuideSecondFragment page2 = new SnapGuideSecondFragment();
        SnapGuideThirdFragment page3 = new SnapGuideThirdFragment();
        SnapGuideFourthStichFragment page4 = new SnapGuideFourthStichFragment();

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(page1);
        fragmentList.add(page2);
        fragmentList.add(page3);
        fragmentList.add(page4);
        return fragmentList;
    }

    public void switchToFragment(int position) {
        mPager.setCurrentItem(position, true);
    }

    public void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {
        switch (snapEarnGuideType) {
            default: {
                finish();
                break;
            }
        }
    }

    @Override
    public void finish() {
        Intent intent = new Intent();
        if (snapEarnGuideType == ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_SNAP) {
            setResult(ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_SNAP);
        } else if (snapEarnGuideType == ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_BRAND) {
            intent.putExtra(ConfigManager.BrandDetails.BRAND_DETAILS, brand);
            setResult(ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_BRAND, intent);
        } else if (snapEarnGuideType == ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_STORE_DETAIL) {
            putIntentExtraReceiptDetail(intent);
            setResult(ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_STORE_DETAIL, intent);
        } else if (snapEarnGuideType == ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_STORE) {
            intent.putExtra(ConfigManager.Store.INTEREST_STORE, interestsStore);
            setResult(ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_STORE, intent);
        }
        new DeviceSession(this).completeShowSnapEarnGuide();
        super.finish();
    }

    private void putIntentExtraReceiptDetail(Intent intent) {
        boolean isLuckyDraw = MultiplierMenuManager.isLuckyDraw(store.getMerchantTier());
        intent.putExtra(ConfigManager.Snap.LUCKY_DRAW_INTENT_DATA, isLuckyDraw);
        intent.putExtra(ConfigManager.Snap.MERCHANT_DATA, createReceiptStore(store));
    }

    private ReceiptStore createReceiptStore(Store store) {
        ReceiptStore receiptStore = new ReceiptStore();
        receiptStore.setName(store.getName());
        receiptStore.setCategory(store.getCategory().getName());
        receiptStore.setId(String.valueOf(store.getId()));
        receiptStore.setLocation(store.getAddress());
        return receiptStore;
    }

}