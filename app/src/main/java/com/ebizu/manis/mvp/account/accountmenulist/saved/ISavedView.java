package com.ebizu.manis.mvp.account.accountmenulist.saved;

import com.ebizu.manis.model.saved.Saved;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by mac on 8/21/17.
 */

public interface ISavedView extends IBaseView{

    void setSaved(Saved saved);

    ISavedPresenter getSavedPresenter();

}
