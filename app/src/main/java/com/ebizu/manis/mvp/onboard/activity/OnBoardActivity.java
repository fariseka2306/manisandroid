package com.ebizu.manis.mvp.onboard.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.login.LoginActivity;
import com.ebizu.manis.mvp.onboard.fragment.OnBoardFragment;
import com.ebizu.manis.mvp.onboard.view.IOnBoard;
import com.ebizu.manis.mvp.onboard.view.OnBoardInterface;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.root.BaseActivity;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ebizu on 8/22/17.
 */

public class OnBoardActivity extends BaseActivity {


    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.button_skip)
    Button buttonSkip;
    @BindView(R.id.button_sign_up)
    Button buttonSignUp;
    @BindView(R.id.circle_pager_indicator)
    CirclePageIndicator circlePageIndicator;

    @Inject
    Context context;

    private OnBoardInterface onBoardInterface;
    private List<OnBoardFragment> onBoardFragments;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_board);
        ButterKnife.bind(this);
        setViewPagerView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent = DaggerActivityComponent
                .builder()
                .applicationComponent(getApplicationComponent())
                .build();
        activityComponent.inject(this);
    }

    private void setViewPagerView() {
        onBoardInterface = new OnBoardInterface(context);
        onBoardFragments = createOnBoardFragments(onBoardInterface);
        viewPager.setAdapter(new OnBoardFragmentPagerAdapter(getSupportFragmentManager(), onBoardInterface, onBoardFragments));
        circlePageIndicator.setViewPager(viewPager);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        viewPager.post(() -> onPageChangeListener.onPageSelected(viewPager.getCurrentItem()));
    }

    private ViewPager.OnPageChangeListener onPageChangeListener =
            new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    pageSelectedViewControl(position, onBoardFragments, onBoardInterface);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            };

    @OnClick(R.id.button_skip)
    void onClickSkip() {
        startActivityLogin();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.ON_BOARDING,
                ConfigManager.Analytic.Action.CLICK,
                "Button Skip"
        );
    }

    @OnClick(R.id.button_sign_up)
    void onClickSignUp() {
        startActivityLogin();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.ON_BOARDING,
                ConfigManager.Analytic.Action.CLICK,
                "Button Sign Up"
        );
    }

    private List<OnBoardFragment> createOnBoardFragments(OnBoardInterface onBoardInterface) {
        List<OnBoardFragment> onBoardFragments = new ArrayList<>();
        for (IOnBoard IOnBoard : onBoardInterface.getViews()) {
            OnBoardFragment onBoardFragment = new OnBoardFragment();
            onBoardFragments.add(onBoardFragment);
        }
        return onBoardFragments;
    }

    private void setVisibilityButton(OnBoardInterface onBoardInterface, int positionFragment) {
        if (onBoardInterface.isLast(positionFragment)) {
            buttonSkip.setVisibility(View.INVISIBLE);
            buttonSignUp.setVisibility(View.VISIBLE);
        } else {
            buttonSkip.setVisibility(View.VISIBLE);
            buttonSignUp.setVisibility(View.INVISIBLE);
        }
    }

    private void pageSelectedViewControl(int position, List<OnBoardFragment> onBoardFragments, OnBoardInterface onBoardInterface) {
        OnBoardFragment onBoardFragment = onBoardFragments.get(position);
        setVisibilityButton(onBoardInterface, position);
        onBoardFragment.setVisibilityText(onBoardInterface, position);
        onBoardFragment.startVideo();
        onBoardFragment.setPageControlListener(new OnBoardFragment.OnClickPageListener() {
            @Override
            public void onNext() {
                viewPager.setCurrentItem(position + 1);
            }

            @Override
            public void onPrev() {
                viewPager.setCurrentItem(position - 1);
            }
        });
    }

    private void startActivityLogin() {
        DeviceSession deviceSession = new DeviceSession(this);
        deviceSession.completeIntro();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

}