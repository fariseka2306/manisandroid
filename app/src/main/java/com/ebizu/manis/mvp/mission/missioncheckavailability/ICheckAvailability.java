package com.ebizu.manis.mvp.mission.missioncheckavailability;

import com.ebizu.manis.mvp.mission.MissionDetailActivity;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperienceCheckAvalaibilityRB;

public interface ICheckAvailability {

    void checkAvailability(MissionDetailActivity missionDetailActivity, ShareExperienceCheckAvalaibilityRB shareExperienceCheckAvalaibilityRB);
}
