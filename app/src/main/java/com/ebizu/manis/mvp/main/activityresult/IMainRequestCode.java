package com.ebizu.manis.mvp.main.activityresult;

import android.content.Intent;

/**
 * Created by ebizu on 10/11/17.
 */

public interface IMainRequestCode {

    int requestCode();

    void jobRequest(int resultCode, Intent data);

}
