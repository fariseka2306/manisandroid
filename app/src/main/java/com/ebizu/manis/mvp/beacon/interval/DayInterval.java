package com.ebizu.manis.mvp.beacon.interval;

/**
 * Created by ebizu on 9/7/17.
 */

public class DayInterval extends NotificationInterval implements IntervalInterface {

    @Override
    public String type() {
        return "day";
    }

    @Override
    public long interval(int interval) {
        return NotificationBeaconInterval.DAY * interval;
    }

}
