package com.ebizu.manis.mvp.phoneloginfacebookotp;

import com.ebizu.manis.model.Phone;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by FARIS_mac on 11/2/17.
 */

public class ConfirmCodeFacebookOtpHelper {

    public static void authManis(BaseActivity baseActivity, ConfirmCodeFacebookPresenter confirmCodePresenter, Phone phone, FacebookOtpLoginActivity facebookOtpLoginActivity) {
        if (baseActivity.getManisSession().isLoggedIn()) {
            confirmCodePresenter.signUpOtp(baseActivity.getManisSession().getAccountSession(), facebookOtpLoginActivity);
        } else {
            confirmCodePresenter.loginAccountManis(phone, facebookOtpLoginActivity.regIdGcm, facebookOtpLoginActivity);
        }
    }

}
