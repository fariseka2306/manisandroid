package com.ebizu.manis.mvp.luckydraw.luckydrawupload;

import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.LuckyDrawUploadBody;
import com.ebizu.manis.service.manis.requestbody.LuckyDrawUploadPhotoBody;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawUpload;
import com.google.gson.Gson;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class LuckyDrawUploadPresenter extends BaseViewPresenter implements ILuckyDrawUploadPresenter {

    private final String TAG = getClass().getSimpleName();

    private LuckyDrawUploadView luckyDrawUploadView;
    private Subscription subsLuckyDraw;

    @Override
    public void attachView(BaseView baseView) {
        luckyDrawUploadView = (LuckyDrawUploadView) baseView;
        super.attachView(baseView);
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsLuckyDraw != null) subsLuckyDraw.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadLuckyDrawUpload(ManisApi manisApi,
                                    LuckyDrawUploadBody luckyDrawUploadBody,
                                    LuckyDrawUploadPhotoBody luckyDrawUploadPhotoBody) {
        releaseSubscribe(0);
        subsLuckyDraw = manisApi.getLuckyDrawUpload(new Gson().toJson(luckyDrawUploadBody),
                new Gson().toJson(luckyDrawUploadPhotoBody)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperLuckyDrawUpload>(luckyDrawUploadView) {
                    @Override
                    public void onNext(WrapperLuckyDrawUpload wrapperLuckyDrawUpload) {
                        super.onNext(wrapperLuckyDrawUpload);
                    }
                });
    }
}
