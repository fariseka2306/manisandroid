package com.ebizu.manis.mvp.snapdetail.viewimage;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.snap.SnapData;
import com.ebizu.manis.root.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Raden on 7/21/17.
 */

public class ViewImageSnap extends BaseActivity {

    private static final String TAG = ViewImageSnap.class.getSimpleName();

    private SnapData snapData;
    private Context context;

    @BindView(R.id.img_receipt)
    ImageView imgReceipt;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.rel_left)
    RelativeLayout relBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideInHorizontalAnim();
        setContentView(R.layout.view_image_snap);
        ButterKnife.bind(this);
        context = this;

        snapData = getIntent().getParcelableExtra(ConfigManager.Snap.DATA);

        initDeclareView();
    }

    private void initDeclareView(){
        if(snapData != null){

            txtTitle.setText(snapData.getStore().getName());

            Glide.with(context)
                    .load(snapData.getReceipt().getImage())
                    .thumbnail(0.1f)
                    .animate(R.anim.fade_in_image)
                    .fitCenter()
                    .into(imgReceipt);
        }else{
            txtTitle.setText(getString(R.string.sh_txt_receipt_sample));
            Glide.with(context)
                    .load(R.drawable.receipt_sample)
                    .thumbnail(0.1f)
                    .animate(R.anim.fade_in_image)
                    .fitCenter()
                    .into(imgReceipt);
        }
    }

    @OnClick(R.id.rel_left)
    void backOnClick(){
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        slideOutHorizontalAnim();
    }
}
