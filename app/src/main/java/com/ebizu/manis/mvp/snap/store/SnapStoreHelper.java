package com.ebizu.manis.mvp.snap.store;

import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.snap.SnapStore;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;
import com.ebizu.manis.service.manis.requestbody.SearchMerchantStoreBody;
import com.ebizu.manis.service.manis.requestbody.SnapStoreBody;
import com.ebizu.manis.service.manis.requestbody.SnapStoreSuggestBody;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public class SnapStoreHelper {

    public static String getSnapStoreBody(int page) {
        SnapStoreBody snapStoreBody = new SnapStoreBody();
        snapStoreBody.setPage(page);
        snapStoreBody.setSize(20);
        snapStoreBody.setMultiplier(-1);
        return new Gson().toJson(snapStoreBody);
    }

    public static String getSnapStoreSuggest(int page) {
        SnapStoreSuggestBody snapStoreSuggestBody = new SnapStoreSuggestBody();
        snapStoreSuggestBody.setPage(page);
        return new Gson().toJson(snapStoreSuggestBody);
    }

    public static String getSearchMerchantStoreBody(String keyword, int page) {
        SearchMerchantStoreBody searchMerchantStoreBody = new SearchMerchantStoreBody();
        searchMerchantStoreBody.setPage(page);
        searchMerchantStoreBody.setKeyword(keyword);
        searchMerchantStoreBody.setSize(20);
        String storeBody = new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(searchMerchantStoreBody);
        return storeBody;
    }

    public static ReceiptStore createReceiptStore(Store store) {
        ReceiptStore ReceiptStore = new ReceiptStore();
        ReceiptStore.setCategory(String.valueOf(store.getCategory().getId()));
        ReceiptStore.setId(String.valueOf(store.getId()));
        ReceiptStore.setLocation(store.getAddress());
        ReceiptStore.setName(store.getName());
        return ReceiptStore;
    }
}
