package com.ebizu.manis.mvp.phonelogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.launcher.LauncherManager;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by ebizu on 9/28/17.
 */

public class PhoneOtpActivity extends BaseActivity {

    protected int requestCode;
    public String regIdGcm = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestCode = getIntent().getIntExtra(ConfigManager.Otp.REQUEST_CODE, 0);
        getIntentData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ConfigManager.Otp.FORCE_SIGN_UP_OTP: {
                if (resultCode == ConfigManager.Otp.SUCCESS_SIGN_UP_OTP) {
                    setResult(ConfigManager.Otp.SUCCESS_SIGN_UP_OTP);
                    finish();
                }
                break;
            }
            case ConfigManager.Otp.SIGN_UP_OTP_REQUEST_CODE: {
                if (resultCode == ConfigManager.Otp.SUCCESS_SIGN_UP_OTP) {
                    setResult(ConfigManager.Otp.SUCCESS_SIGN_UP_OTP);
                    finish();
                }
                break;
            }
        }
    }

    private void getIntentData() {
        try {
            if (!getManisSession().isLoggedIn()) {
                regIdGcm = getIntent().getStringExtra(ConfigManager.Auth.REGID_GCM_INTENT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onNextActivity() {
        dismissProgressBarDialog();
        Intent intent = LauncherManager.getIntentNextLoginPhone(
                this, this, requestCode);
        ActivityCompat.finishAffinity(this);
        this.startActivity(intent);
    }

    public int getRequestCode() {
        return requestCode;
    }
}
