package com.ebizu.manis.mvp.store.storedetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.InterestsStore;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.mvp.snap.SnapActivity;
import com.ebizu.manis.mvp.snap.form.storedetail.ReceiptDetailActivity;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by firef on 7/14/2017.
 */

public class StoreDetailActivity extends SnapActivity {

    @Inject
    Context context;
    @Inject
    ManisApi manisApi;
    @BindView(R.id.toolbar)
    ToolbarView toolbarView;
    @BindView(R.id.store_detail_header_view)
    StoreDetailView storeDetailView;

    private Store store;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        setContentView(R.layout.activity_store_detail);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        store = bundle.getParcelable(ConfigManager.Store.STORE_DETAIL_TITLE);

        if (!store.getName().isEmpty()) {
            String[] storeName = store.getName().split("@");
            RelativeLayout.LayoutParams params =
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
            params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            params.setMarginStart(20);
            toolbarView.setTitle(params, storeName[0], R.drawable.navigation_back_btn_black);
        }
        storeDetailView.setActivity(this);
        storeDetailView.attachPresenter(new StoreDetailPresenter());
        storeDetailView.getStoreDetailPresenter().loadStoreDetailData(manisApi, store);
        storeDetailView.initUber(store);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConfigManager.Snap.SNAP_EARN_GUIDE_REQUEST_CODE) {
            if (resultCode == ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_STORE_DETAIL) {
                Intent intent = new Intent(this, ReceiptDetailActivity.class);
                boolean isLuckyDraw = data.getBooleanExtra(ConfigManager.Snap.LUCKY_DRAW_INTENT_DATA, false);
                ReceiptStore receiptStore = data.getParcelableExtra(ConfigManager.Snap.MERCHANT_DATA);
                intent.putExtra(ConfigManager.Snap.LUCKY_DRAW_INTENT_DATA, isLuckyDraw);
                intent.putExtra(ConfigManager.Snap.MERCHANT_DATA, receiptStore);
                startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
            }
        }
    }
}