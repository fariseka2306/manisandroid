package com.ebizu.manis.mvp.notification.item;

import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.view.holder.NotificationSwipeViewHolder;

/**
 * Created by ebizu on 8/28/17.
 */

public class NotificationItem implements NotificationInterface {

    protected NotificationTableList notificationTableList;
    protected NotificationSwipeViewHolder notificationSwipeViewHolder;

    public NotificationItem(NotificationTableList notificationTableList, NotificationSwipeViewHolder notificationSwipeViewHolder) {
        this.notificationTableList = notificationTableList;
        this.notificationSwipeViewHolder = notificationSwipeViewHolder;
    }

    @Override
    public String notificationType() {
        return null;
    }

    @Override
    public void setNotificationView() {

    }
}
