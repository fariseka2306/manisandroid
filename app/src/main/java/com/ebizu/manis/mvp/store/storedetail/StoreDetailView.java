package com.ebizu.manis.mvp.store.storedetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.GPSTracker;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.manager.multiplier.MultiplierMenuManager;
import com.ebizu.manis.model.Offer;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.StoreDetail;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileActivity;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.SnapGuideActivity;
import com.ebizu.manis.mvp.snap.form.storedetail.ReceiptDetailActivity;
import com.ebizu.manis.mvp.store.storedetaillocation.StoreDetailLocationActivity;
import com.ebizu.manis.mvp.store.storedetailmore.StoreDetailMoreActivity;
import com.ebizu.manis.mvp.store.storeoffer.StoreOfferDetailActivity;
import com.ebizu.manis.mvp.store.storereward.StoreRewardDetailActivity;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;
import com.ebizu.manis.service.manis.response.WrapperPin;
import com.ebizu.manis.service.manis.response.WrapperUnpin;
import com.ebizu.manis.view.adapter.StoreOfferAdapter;
import com.uber.sdk.android.core.UberSdk;
import com.uber.sdk.android.rides.RideParameters;
import com.uber.sdk.android.rides.RideRequestButton;
import com.uber.sdk.android.rides.RideRequestButtonCallback;
import com.uber.sdk.core.auth.Scope;
import com.uber.sdk.rides.client.ServerTokenSession;
import com.uber.sdk.rides.client.SessionConfiguration;
import com.uber.sdk.rides.client.error.ApiError;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by firef on 7/20/2017.
 */

public class StoreDetailView extends BaseView implements IStoreDetailView,
        StoreOfferAdapter.StoreOfferListener, RideRequestButtonCallback {

    private Store store;
    private StoreDetail storeDetail;
    private IStoreDetailPresenter iStoreDetailPresenter;
    private StoreOfferAdapter storeOfferAdapter;
    private DeviceSession deviceSession;

    @BindView(R.id.sd_img_background)
    ImageView sdImgBackground;

    @BindView(R.id.sd_img_merchant)
    ImageView sdImgMerchant;

    @BindView(R.id.sd_txt_category)
    TextView sdTxtCategory;

    @BindView(R.id.sd_txt_near)
    TextView sdTxtNear;

    @BindView(R.id.sd_txt_follower)
    TextView sdTxtFollower;

    @BindView(R.id.sd_img_follow)
    ImageView sdImgFollow;

    @BindView(R.id.sd_title_rewards)
    LinearLayout sdTitleRewards;

    @BindView(R.id.sd_lin_rewardempty)
    LinearLayout sdLinRewardEmpty;

    @BindView(R.id.sd_title_offers)
    LinearLayout sdTittleOffers;

    @BindView(R.id.sd_lin_offerempty)
    LinearLayout sdLinOfferEmpty;

    @BindView(R.id.item_tab_store_multiplier)
    RelativeLayout itemTabStoreMultiplier;

    @BindView(R.id.image_view_multiplier)
    ImageView imageViewMultiplier;

    @BindView(R.id.text_view_multiplier)
    TextView textViewMultiplier;

    @BindView(R.id.sd_img_tier)
    ImageView sdImgTier;

    @BindView(R.id.rv_offer)
    ListView rvOffer;

    @BindView(R.id.sd_uberbtn)
    RideRequestButton sdUberBtn;

    public StoreDetailView(Context context) {
        super(context);
        createView(context);
    }

    public StoreDetailView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public StoreDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public StoreDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.store_detail_title_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new StoreDetailPresenter());
        initView();
        initListener();
        deviceSession = new DeviceSession(getContext());
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iStoreDetailPresenter = (IStoreDetailPresenter) iBaseViewPresenter;
        iStoreDetailPresenter.attachView(this);
    }

    @Override
    public void loadStoreDetailData(Store store, StoreDetail storeDetail) {
        this.store = store;
        this.storeDetail = storeDetail;
        sdTxtFollower.setText(storeDetail.getFollowers() + " " +
                getContext().getString(R.string.fs_txt_followers));
        sdTxtCategory.setText(store.getCategory().getName());
        sdTxtNear.setText(getContext().getString(R.string.fs_txt_nearest) + " : " +
                distanceToString(store.getCoordinate().getDistance()));
        ImageUtils.loadImage(getContext(), store.getAssets().getBanner(),
                ContextCompat.getDrawable(getContext(),
                        R.drawable.default_pic_store_banner), sdImgBackground);
        ImageUtils.loadImage(getContext(), store.getAssets().getPhoto(),
                ContextCompat.getDrawable(getContext(),
                        R.drawable.default_pic_store_logo), sdImgMerchant);
        checkMerchantTier(store);
        checkFollower(storeDetail);
        checkRewards(storeDetail);
        checkOffers(storeDetail);

    }

    @Override
    public void setFollowerData(WrapperPin wrapperPin) {
        Glide.with(getContext())
                .load(R.drawable.store_details_followed_icon)
                .into(sdImgFollow);
        storeDetail.setFollowers(storeDetail.getFollowers() + 1);
        sdTxtFollower.setText(storeDetail.getFollowers() + " " +
                getContext().getString(R.string.fs_txt_followers));
    }

    @Override
    public void setUnFollowerData(WrapperUnpin wrapperUnpin) {
        Glide.with(getContext())
                .load(R.drawable.store_details_follow_icon)
                .into(sdImgFollow);
        storeDetail.setFollowers(storeDetail.getFollowers() - 1);
        sdTxtFollower.setText(storeDetail.getFollowers() + " " +
                getContext().getString(R.string.fs_txt_followers));
    }

    @Override
    public IStoreDetailPresenter getStoreDetailPresenter() {
        return iStoreDetailPresenter;
    }

    @Override
    public void onStoreOfferListener(Offer offer) {
        Intent intent = new Intent(getContext(), StoreOfferDetailActivity.class);
        intent.putExtra(ConfigManager.Store.STORE_DETAIL_TITLE, store);
        intent.putExtra(ConfigManager.Store.STORE_OFFER, offer);
        getContext().startActivity(intent);
    }

    @OnClick(R.id.ll_more)
    public void onSdTxtMoreClick() {
        Intent intent = new Intent(getContext(), StoreDetailMoreActivity.class);
        intent.putExtra(ConfigManager.Store.STORE_DETAIL_TITLE, store);
        intent.putExtra(ConfigManager.Store.STORE_DETAIL_MORE, storeDetail);
        getContext().startActivity(intent);
    }

    @OnClick(R.id.sd_lin_follow)
    public void onSdImgFollowClick() {
        ManisApi manisApi = ManisApiGenerator.createServiceWithToken(getContext());
        if (storeDetail.getPinned()) {
            iStoreDetailPresenter.postStoreDetailUnfollow(manisApi, store);
            storeDetail.setPinned(false);
        } else {
            iStoreDetailPresenter.postStoreDetailFollower(manisApi, store);
            storeDetail.setPinned(true);
        }
    }

    @OnClick(R.id.sd_lin_location)
    public void onSdLinLocationClick() {
        Intent intent = new Intent(getContext(), StoreDetailLocationActivity.class);
        intent.putExtra(ConfigManager.Store.STORE_MAP_LOCATION, store);
        getContext().startActivity(intent);
    }

    @OnClick(R.id.sd_title_rewards)
    public void onSdTtitleRewardsClick() {
        Intent intent = new Intent(getContext(), StoreRewardDetailActivity.class);
        intent.putExtra(ConfigManager.Store.STORE_REWARD, storeDetail);
        getContext().startActivity(intent);
    }

    private String distanceToString(Double distance) {
        DecimalFormat df = new DecimalFormat("0.#");
        if (distance >= 1100) {
            return df.format(distance / 1000) + " km";
        } else {
            return df.format(distance) + " m";
        }
    }

    private void checkMerchantTier(Store store) {
        if (store.getMerchantTier().equalsIgnoreCase(ConfigManager.Store.MULTIPLIER_TIER_LONGTAIL)) {
            sdImgTier.setVisibility(VISIBLE);
        } else {
            itemTabStoreMultiplier.setVisibility(VISIBLE);
            Drawable multiplierDraw = ContextCompat.getDrawable(getContext(),
                    MultiplierMenuManager.getDrawble(store.getMultiplier(), store.getMerchantTier()));
            imageViewMultiplier.setImageDrawable(multiplierDraw);
            textViewMultiplier.setText(MultiplierMenuManager.getText(store.getMultiplier(), store.getMerchantTier()));
            textViewMultiplier.setBackground(multiplierDraw);
        }
    }

    private void checkFollower(StoreDetail storeDetail) {
        if (storeDetail.getPinned()) {
            Glide.with(getContext())
                    .load(R.drawable.store_details_followed_icon)
                    .into(sdImgFollow);
        } else {
            Glide.with(getContext())
                    .load(R.drawable.store_details_follow_icon)
                    .into(sdImgFollow);
        }
    }

    private void checkRewards(StoreDetail storeDetail) {
        if (storeDetail.getRewards().isEmpty()) {
            sdLinRewardEmpty.setVisibility(VISIBLE);
        } else {
            sdTitleRewards.setVisibility(VISIBLE);
        }
    }

    private void checkOffers(StoreDetail storeDetail) {
        if (storeDetail.getOffers().isEmpty()) {
            sdLinOfferEmpty.setVisibility(VISIBLE);
        } else {
            storeOfferAdapter.addAll(storeDetail.getOffers());
            sdTittleOffers.setVisibility(VISIBLE);
            rvOffer.setVisibility(VISIBLE);
        }
    }

    private void initView() {
        storeOfferAdapter = new StoreOfferAdapter(getContext(), new ArrayList<>());
        rvOffer.setAdapter(storeOfferAdapter);
    }

    private void initListener() {
        storeOfferAdapter.setStoreOfferListener(this);
    }

    @OnClick(R.id.sd_img_snap)
    void onClickImageSnap() {
        if (!deviceSession.hasShowSnapEarnGuide()) {
            Intent intent = new Intent(getContext(), SnapGuideActivity.class);
            intent.putExtra(ConfigManager.Snap.SNAP_EARN_GUIDE, ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_STORE_DETAIL);
            intent.putExtra(ConfigManager.Store.STORE_DETAIL_TITLE, store);
            getBaseActivity().startActivityForResult(intent, ConfigManager.Snap.SNAP_EARN_GUIDE_REQUEST_CODE);
        } else {
            if (null != store) {
                if (MultiplierMenuManager.isLuckyDraw(store.getMerchantTier())) {
                    getStoreDetailPresenter().isSnapable(store);
                } else {
                    startActivityReceiptDetail();
                }
            }
        }
    }

    @Override
    public void startActivityReceiptDetail() {
        Intent intent = new Intent(getContext(), ReceiptDetailActivity.class);
        putIntentExtraReceiptDetail(intent);
        getBaseActivity().startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
    }

    @Override
    public void startActivityProfile() {
        getBaseActivity().showAlertDialog("Lucky Draw", getContext().getString(R.string.ld_dialog_complete_profile),
                true, R.drawable.manis_logo,
                "Ok", (dialogInterface, i) -> {
                    Intent intent = new Intent(getContext(), ProfileActivity.class);
                    intent.putExtra(ConfigManager.UpdateProfile.UPDATE_PROFILE_TYPE_INTENT, ConfigManager.UpdateProfile.UPDATE_PROFILE_LUCKY_DRAW_TYPE);
                    putIntentExtraReceiptDetail(intent);
                    getBaseActivity().startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
                }, null, null);
    }

    @Override
    public void onRideInformationLoaded() {

    }

    @Override
    public void onError(ApiError apiError) {

    }

    @Override
    public void onError(Throwable throwable) {

    }

    public void initUber(Store store) {
        SessionConfiguration sessionConfiguration = new SessionConfiguration.Builder()
                .setClientId(BuildConfig.UBER_CLIENT_ID)
                .setServerToken(BuildConfig.UBER_SERVER_TOKEN)
                .setRedirectUri(BuildConfig.UBER_URI)
                .setEnvironment(SessionConfiguration.Environment.PRODUCTION)
                .setScopes(Arrays.asList(Scope.PROFILE, Scope.RIDE_WIDGETS))
                .build();

        UberSdk.initialize(sessionConfiguration);
        ServerTokenSession session = new ServerTokenSession(sessionConfiguration);

        String delims = "[@]";
        String[] parse = store.getName().split(delims);
        String nickname = parse[0];
        String address = "";
        if (parse.length == 2) {
            address = parse[1];
        }

        GPSTracker gpsTracker = new GPSTracker(getContext());
        gpsTracker.updateLocation();
        RideParameters rideParams = new RideParameters.Builder()
                .setPickupLocation(gpsTracker.getLatitude(), gpsTracker.getLongitude(), "My Location", "")
                .setDropoffLocation(store.getCoordinate().getLatitude(),
                        store.getCoordinate().getLongitude(), nickname, address)
                .build();

        sdUberBtn.setRideParameters(rideParams);
        sdUberBtn.setSession(session);
        sdUberBtn.setCallback(this);
        sdUberBtn.loadRideInformation();
    }

    private void putIntentExtraReceiptDetail(Intent intent) {
        boolean isLuckyDraw = MultiplierMenuManager.isLuckyDraw(store.getMerchantTier());
        intent.putExtra(ConfigManager.Snap.LUCKY_DRAW_INTENT_DATA, isLuckyDraw);
        intent.putExtra(ConfigManager.Snap.MERCHANT_DATA, createReceiptStore(store));
    }

    private ReceiptStore createReceiptStore(Store store) {
        ReceiptStore receiptStore = new ReceiptStore();
        receiptStore.setName(store.getName());
        receiptStore.setCategory(String.valueOf(store.getCategory().getId()));
        receiptStore.setId(String.valueOf(store.getId()));
        receiptStore.setLocation(store.getAddress());
        return receiptStore;
    }


}

