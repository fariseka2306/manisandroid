package com.ebizu.manis.mvp.main.activityresult;

import android.content.Intent;
import android.os.Handler;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.view.dialog.inviteterm.InviteTermDialog;

/**
 * Created by ebizu on 10/11/17.
 */

public class SnapRequest extends MainRequestCode
        implements IMainRequestCode {


    public SnapRequest(MainActivity mainActivity) {
        super(mainActivity);
    }

    @Override
    public int requestCode() {
        return ConfigManager.Snap.RECEIPT_REQUEST_CODE;
    }

    @Override
    public void jobRequest(int resultCode, Intent data) {
        super.jobRequest(resultCode, data);
        if (resultCode == ConfigManager.Snap.EARN_POINT_RECEIPT_SUCCESS_UPLOAD_RS) {
            mPager.setCurrentItem(POS_HOME_VIEW);
            mainActivity.homeFragment.getSnapPresenter().loadSnapData();
        } else if (resultCode == ConfigManager.Snap.EARN_TICKET_RECEIPT_SUCCESS_UPLOAD_RS) {
            boolean openLuckyDraw = data.getBooleanExtra(ConfigManager.Snap.OPEN_LUCKY_DRAW_FRAGMENT, false);
            if (openLuckyDraw) {
                mPager.setCurrentItem(POS_LUCKY_DRAW_VIEW);
            } else {
                mPager.setCurrentItem(POS_HOME_VIEW);
                mainActivity.homeFragment.getSnapPresenter().loadSnapData();
            }
        } else if (resultCode == ConfigManager.Snap.GET_MORE_POINTS_RS) {
            mPager.setCurrentItem(POS_HOME_VIEW);
            mainActivity.homeFragment.getSnapPresenter().loadSnapData();
            mainActivity.showInviteFriendDialog();
        }
    }
}
