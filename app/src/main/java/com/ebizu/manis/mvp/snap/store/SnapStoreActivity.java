package com.ebizu.manis.mvp.snap.store;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.snap.SnapStore;
import com.ebizu.manis.mvp.snap.SnapActivity;
import com.ebizu.manis.mvp.snap.form.receiptdetail.SnapStoreDetailActivity;
import com.ebizu.manis.mvp.snap.store.list.ListSnapStoreView;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.snaptnc.SnapEarnPointTncPresenter;
import com.ebizu.manis.view.dialog.snaptnc.SnapEarnTicketTncPresenter;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ebizu-User on 03/08/2017.
 */

public class SnapStoreActivity extends SnapActivity {

    @BindView(R.id.snap_store_view)
    ListSnapStoreView listSnapStoreView;
    @BindView(R.id.toolbar_view)
    ToolbarView toolbarView;

    private SnapStore snapStore;
    @Inject
    SnapEarnPointTncPresenter snapEarnPointTncPresenter;
    @Inject
    SnapEarnTicketTncPresenter snapEarnTicketTncPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideInVerticalAnim();
        setContentView(R.layout.activity_snap);
        ButterKnife.bind(this);
        listSnapStoreView.setActivity(this);
        toolbarView.setTitle(getResources().getString(R.string.se_title));
        loadAllPresenter();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    @Override
    public void onBackPressed() {
        if (listSnapStoreView.getNearStoreView().isShown()) {
            super.onBackPressed();
        } else {
            listSnapStoreView.visibleView(listSnapStoreView.getNearStoreView());
            getAnalyticManager().trackScreenView(ConfigManager.Analytic.ScreenView.FRAGMENT_SNAP_NEARBY);
        }
    }

    private void loadAllPresenter() {
        snapEarnPointTncPresenter.attachDialog(new BaseDialogManis(this));
        snapEarnTicketTncPresenter.attachDialog(new BaseDialogManis(this));
        snapEarnPointTncPresenter.getTermsAndSave();
        snapEarnTicketTncPresenter.getTermsAndSave();
        listSnapStoreView.getNearStoreView().getNearStorePresenter()
                .getSnapStoresNearby(1, IBaseView.LoadType.CLICK_LOAD);
        listSnapStoreView.getHistorySnapStoreView().loadAllPresenter();

    }

    @Override
    public void finish() {
        super.finish();
        slideOutVerticalAnim();
    }
}
