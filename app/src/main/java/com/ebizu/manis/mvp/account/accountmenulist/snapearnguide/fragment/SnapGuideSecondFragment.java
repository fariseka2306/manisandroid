package com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.fragment;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.SnapGuideActivity;

import java.util.HashMap;


public class SnapGuideSecondFragment extends Fragment {

    private boolean isCreated = false;
    private AnimatorSet mAnimatorSet;

    // Second Screen
    private TextView textViewTitleSecond;
    private TextView textViewDetailSecond;
    private TextView textViewDetailSecondChild;
    private ImageView imageViewMoonSecond;
    private ImageView imageViewSunSecond;
    private ImageView imageViewReceiptSecond;
    private View viewBlackTransparent;
    private ImageView imageViewIncorrectSecond;
    private ImageView imageViewCorrectSecond;
    private View viewOvalWhiteSecond;
    private Button buttonNextSecond;
    private TextView textViewExplanationSecond;

    private HashMap<View, Float> mOriginalYValuesMap = new HashMap<>();
    private HashMap<View, Float> mOriginalXValuesMap = new HashMap<>();
    private SnapGuideActivity snapGuideActivity;

    private View.OnClickListener nextListener = view -> snapGuideActivity.switchToFragment(2);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        snapGuideActivity = (SnapGuideActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_snapguide_second, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isCreated = true;
        Bundle args = getArguments();
        int position = args.getInt(UtilStatic.ARG_POSITION);
        view.setTag(position);
    }

    private void initView(View rootView) {
        textViewTitleSecond = (TextView) rootView.findViewById(R.id.textview_title);
        textViewDetailSecond = (TextView) rootView.findViewById(R.id.textview_detail);
        textViewDetailSecondChild = (TextView) rootView.findViewById(R.id.textview_detail_child);
        imageViewMoonSecond = (ImageView) rootView.findViewById(R.id.imageview_moon);
        imageViewSunSecond = (ImageView) rootView.findViewById(R.id.imageview_sun);
        imageViewReceiptSecond = (ImageView) rootView.findViewById(R.id.imageview_receipt);
        viewBlackTransparent = rootView.findViewById(R.id.view_dark);
        imageViewIncorrectSecond = (ImageView) rootView.findViewById(R.id.imageview_incorrect);
        imageViewCorrectSecond = (ImageView) rootView.findViewById(R.id.imageview_correct);
        viewOvalWhiteSecond = rootView.findViewById(R.id.view_oval_white);
        buttonNextSecond = (Button) rootView.findViewById(R.id.button_next);
        textViewExplanationSecond = (TextView) rootView.findViewById(R.id.textview_explanation);
        buttonNextSecond.setOnClickListener(nextListener);

        initializeFirstPosition();
        initializeStateView();
        rootView.post(() -> getOriginalYValues());
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && isCreated) {
            getOriginalYValues();
            viewBlackTransparent.setPivotX(viewBlackTransparent.getWidth());
            doAnimation();
        } else if (!menuVisible && isCreated) {
            mAnimatorSet.end();
            setViewsInOriginalPosition();
        }
    }

    private void getOriginalYValues() {
        mOriginalYValuesMap.put(imageViewReceiptSecond, imageViewReceiptSecond.getY());
        mOriginalYValuesMap.put(viewBlackTransparent, viewBlackTransparent.getY());
        mOriginalYValuesMap.put(imageViewMoonSecond, imageViewMoonSecond.getY());
        mOriginalYValuesMap.put(imageViewSunSecond, imageViewSunSecond.getY());
        mOriginalXValuesMap.put(viewBlackTransparent, viewBlackTransparent.getScaleX());
    }

    private void setViewsInOriginalPosition() {
        imageViewReceiptSecond.setY(mOriginalYValuesMap.get(imageViewReceiptSecond));
        viewBlackTransparent.setY(mOriginalYValuesMap.get(viewBlackTransparent));
        imageViewMoonSecond.setY(mOriginalYValuesMap.get(imageViewMoonSecond));
        imageViewSunSecond.setY(mOriginalYValuesMap.get(imageViewSunSecond));
        viewBlackTransparent.setScaleX(mOriginalXValuesMap.get(viewBlackTransparent));

        initializeStateView();
    }

    private void initializeStateView() {
        textViewTitleSecond.setAlpha(0f);
        textViewDetailSecond.setAlpha(0f);
        textViewDetailSecondChild.setAlpha(0f);
        imageViewCorrectSecond.setAlpha(0f);
        imageViewIncorrectSecond.setAlpha(0f);
        imageViewMoonSecond.setAlpha(1f);
        viewBlackTransparent.setAlpha(1f);
        viewBlackTransparent.setPivotX(viewBlackTransparent.getWidth());
    }

    private void initializeFirstPosition() {
        imageViewReceiptSecond.setY(imageViewReceiptSecond.getY() + getResources().getInteger(R.integer.moveYreceipt));
        viewBlackTransparent.setY(viewBlackTransparent.getY() + getResources().getInteger(R.integer.moveYreceipt));
        imageViewMoonSecond.setY(imageViewMoonSecond.getY() + getResources().getInteger(R.integer.moveYMoonSun));
        imageViewSunSecond.setY(imageViewSunSecond.getY() + getResources().getInteger(R.integer.moveYMoonSun));
    }


    public void doAnimation() {
        ObjectAnimator fadeTitleFirst = ObjectAnimator.ofFloat(textViewTitleSecond, "alpha", 0f, 1f);
        fadeTitleFirst.setDuration(700);

        ObjectAnimator fadeDetailFirst = ObjectAnimator.ofFloat(textViewDetailSecond, "alpha", 0f, 1f);
        fadeDetailFirst.setDuration(700);

        ObjectAnimator fadeDetail2First = ObjectAnimator.ofFloat(textViewDetailSecondChild, "alpha", 0f, 1f);
        fadeDetail2First.setDuration(700);

        ObjectAnimator moveReceipt = ObjectAnimator.ofFloat(imageViewReceiptSecond, "y", imageViewReceiptSecond.getY(), imageViewReceiptSecond.getY() - getResources().getInteger(R.integer.moveYreceipt));
        moveReceipt.setDuration(800);

        ObjectAnimator moveTransparentBlack = ObjectAnimator.ofFloat(viewBlackTransparent, "y", viewBlackTransparent.getY(), viewBlackTransparent.getY() - getResources().getInteger(R.integer.moveYreceipt));
        moveTransparentBlack.setDuration(800);

        ObjectAnimator moveMoon = ObjectAnimator.ofFloat(imageViewMoonSecond, "y", imageViewMoonSecond.getY(), imageViewMoonSecond.getY() - getResources().getInteger(R.integer.moveYMoonSun));
        moveMoon.setDuration(800);

        ObjectAnimator fadeIncorrectShow = ObjectAnimator.ofFloat(imageViewIncorrectSecond, "alpha", 0f, 1f);
        fadeIncorrectShow.setDuration(700);

        ObjectAnimator fadeIncorrectHidden = ObjectAnimator.ofFloat(imageViewIncorrectSecond, "alpha", 1f, 0f);
        fadeIncorrectHidden.setDuration(700);

        ObjectAnimator fadeTransparentBlack = ObjectAnimator.ofFloat(viewBlackTransparent, "scaleX", 0);
        fadeTransparentBlack.setDuration(700);

        ObjectAnimator fadeCorrectShow = ObjectAnimator.ofFloat(imageViewCorrectSecond, "alpha", 0f, 1f);
        fadeCorrectShow.setDuration(700);

        ObjectAnimator moveSun = ObjectAnimator.ofFloat(imageViewSunSecond, "y", imageViewSunSecond.getY(), imageViewSunSecond.getY() - getResources().getInteger(R.integer.moveYMoonSun));
        moveSun.setDuration(800);

        ObjectAnimator fadeMoon = ObjectAnimator.ofFloat(imageViewMoonSecond, "alpha", 1f, 0f);
        fadeMoon.setDuration(700);

        mAnimatorSet = new AnimatorSet();
        fadeIncorrectShow.setStartDelay(400);
        fadeTransparentBlack.setStartDelay(2000);
        fadeMoon.setStartDelay(2000);
        fadeIncorrectHidden.setStartDelay(2100);
        fadeCorrectShow.setStartDelay(2200);
        moveSun.setStartDelay(2000);

        mAnimatorSet.play(fadeTitleFirst)
                .with(fadeDetailFirst)
                .with(fadeDetail2First)
                .with(moveReceipt)
                .with(moveTransparentBlack)
                .with(moveMoon)
                .with(fadeIncorrectShow)
                .with(fadeIncorrectHidden)
                .with(fadeTransparentBlack)
                .with(fadeCorrectShow)
                .with(moveSun)
                .with(fadeMoon);
        mAnimatorSet.start();
    }
}