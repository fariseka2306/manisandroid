package com.ebizu.manis.mvp.main.activityresult;

import android.content.Intent;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.main.MainActivity;

/**
 * Created by ebizu on 10/11/17.
 */

public class SavedRequest extends MainRequestCode
        implements IMainRequestCode {

    public SavedRequest(MainActivity mainActivity) {
        super(mainActivity);
    }

    @Override
    public int requestCode() {
        return ConfigManager.Saved.SAVED_REQUEST_CODE;
    }

    @Override
    public void jobRequest(int resultCode, Intent data) {
        super.jobRequest(resultCode, data);
        if (resultCode == ConfigManager.Saved.STORES_RESULT_CODE) {
            mPager.setCurrentItem(POS_STORE_VIEW);
        } else if (resultCode == ConfigManager.Saved.REWARDS_RESULT_CODE) {
            mPager.setCurrentItem(POS_REWARD_VIEW);
        } else {
            mPager.setCurrentItem(POS_ACCOUNT_VIEW);
        }
    }
}
