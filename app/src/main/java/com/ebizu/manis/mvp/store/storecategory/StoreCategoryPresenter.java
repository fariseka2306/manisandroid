package com.ebizu.manis.mvp.store.storecategory;

import android.content.Context;
import android.util.SparseArray;

import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.response.WrapperInterestsStore;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by firef on 7/10/2017.
 */

public class StoreCategoryPresenter extends BaseViewPresenter implements IStoreCategoryPresenter {

    private static final String TAG = StoreCategoryPresenter.class.getSimpleName();
    private Context context;
    private StoreCategoryView storeCategoryView;
    private Subscription subsInterestStore;

    public StoreCategoryPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        storeCategoryView = (StoreCategoryView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsInterestStore != null) subsInterestStore.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadInterestData(ManisApi manisApi) {
        releaseSubscribe(0);
        storeCategoryView.showProgressBar();
        subsInterestStore = manisApi.getInterestsStore().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperInterestsStore>(storeCategoryView) {
                    @Override
                    public void onNext(WrapperInterestsStore wrapperInterestsStore) {
                        super.onNext(wrapperInterestsStore);
                        storeCategoryView.loadInterestsStore(wrapperInterestsStore.getInterestsStores());
                        storeCategoryView.dismissProgressBar();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        checkConnection();
                        storeCategoryView.dismissProgressBar();
                    }
                });
    }

    private void checkConnection() {
        if (!ConnectionDetector.isNetworkConnected(context)) {
            storeCategoryView.noInternetConnection();
        }
    }
}
