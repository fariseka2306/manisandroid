package com.ebizu.manis.mvp.store.storeoffer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.Offer;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.StoreDetail;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.OfferClaimBody;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class StoreOfferDetailActivity extends BaseActivity implements View.OnClickListener {

    @Inject
    Context context;

    @Inject
    ManisApi manisApi;

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;

    @BindView(R.id.store_offer_detail_view)
    StoreOfferDetailView storeOfferDetailView;

    @BindView(R.id.od_lin_redeem)
    LinearLayout odLinRedeem;

    @BindView(R.id.rf_txt_redeem)
    TextView rfTxtRedeem;

    private Offer offer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers_detail);
        ButterKnife.bind(this);
        initView();
        initListener();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        offer = bundle.getParcelable(ConfigManager.Store.STORE_OFFER);
        Store store = bundle.getParcelable(ConfigManager.Store.STORE_DETAIL_TITLE);
        StoreDetail storeDetail = bundle.getParcelable(ConfigManager.Store.STORE_DETAIL_MORE);
        if (offer != null) {
            String[] storeName = offer.getTitle().split("@");
            RelativeLayout.LayoutParams params =
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
            params.setMarginStart(20);
            toolbarView.setTitle(params, storeName[0], R.drawable.navigation_back_btn_black);
            storeOfferDetailView.setStoreOfferDetailView(offer, store, storeDetail);

            if (offer.getClaimDisabled()) {
                odLinRedeem.setVisibility(View.GONE);
            } else {
                odLinRedeem.setVisibility(View.VISIBLE);
            }
        }
    }

    private void initListener() {
        odLinRedeem.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.equals(odLinRedeem)) {
            storeOfferDetailView.getStoreOfferDetailPresenter().loadClaimOffer(manisApi, new OfferClaimBody(offer.getId()));
        }
    }

    @Override
    public void finish() {
        super.finish();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.OFFER,
                ConfigManager.Analytic.Action.CLICK,
                "Button Close"
        );
    }
}
