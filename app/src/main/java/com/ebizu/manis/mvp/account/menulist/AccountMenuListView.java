package com.ebizu.manis.mvp.account.menulist;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.manager.accountlistmenu.AccountListMenu;
import com.ebizu.manis.manager.accountlistmenu.AccountListMenuManager;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.view.adapter.account.AccountListMenuAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/14/2017.
 */

public class AccountMenuListView extends BaseView implements AccountListMenuAdapter.AccountMenuListener {

    @BindView(R.id.recyclerview_account)
    RecyclerView recyclerView;

    AccountListMenuAdapter accountListMenuAdapter;

    public AccountMenuListView(Context context) {
        super(context);
        createView(context);
    }

    public AccountMenuListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public AccountMenuListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AccountMenuListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        Activity activity = (Activity) context;
        View view = LayoutInflater.from(context).inflate(R.layout.account_menu_recycler, null, false);
        this.accountListMenuAdapter = new AccountListMenuAdapter(context, new ArrayList<>(), activity);
        accountListMenuAdapter.setAccountListener(this);
        addView(view);
        ButterKnife.bind(this, view);
        initialize();
    }

    private void initialize() {
        LinearLayoutManager linearLayoutManagerAccountMenu = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManagerAccountMenu);
        recyclerView.setAdapter(accountListMenuAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.hasFixedSize();
        recyclerView.setNestedScrollingEnabled(false);
        showViewAccountList();
    }

    private void showViewAccountList() {
        AccountListMenuManager.addAccountListMenu(accountListMenuAdapter);
    }

    @Override
    public void onClick(AccountListMenu accountListMenu) {
        AccountListMenuManager.onClickMenu(getBaseActivity(), accountListMenu);
    }
}