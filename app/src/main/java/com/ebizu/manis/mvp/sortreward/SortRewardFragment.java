package com.ebizu.manis.mvp.sortreward;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.solver.widgets.ConstraintAnchor;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import com.ebizu.manis.mvp.reward.ReedemFragment;
import com.ebizu.manis.mvp.reward.newreward.NewRewardFragment;
import com.ebizu.sdk.reward.models.Sorting;

import com.ebizu.manis.R;

import butterknife.BindView;

/**
 * Created by Raden on 8/3/17.
 */

public class SortRewardFragment extends BottomSheetDialogFragment implements View.OnClickListener{

    int sorting;

    CheckedTextView btnSortNew;
    CheckedTextView btnSortTrending;
    CheckedTextView btnVoucherPriceDesc;
    CheckedTextView btnVOucherPriceAsc;
    CheckedTextView btnVoucherPointDesc;
    CheckedTextView btnVoucherPointAsc;
    ImageView btnClose;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reward_sort, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        btnClose = (ImageView) view.findViewById(R.id.sort_btn_close);
        btnSortNew = (CheckedTextView) view.findViewById(R.id.btn_sort_new);
        btnSortTrending = (CheckedTextView) view.findViewById(R.id.btn_sort_trending);
        btnVoucherPriceDesc = (CheckedTextView) view.findViewById(R.id.btn_sort_voucher_price_desc);
        btnVOucherPriceAsc = (CheckedTextView) view.findViewById(R.id.btn_sort_voucher_price_asc);
        btnVoucherPointDesc = (CheckedTextView) view.findViewById(R.id.btn_sort_voucher_point_desc);
        btnVoucherPointAsc = (CheckedTextView) view.findViewById(R.id.btn_sort_voucher_point_asc);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateUI();
        setUICallbacks();
    }

    private void updateUI(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                switch (sorting){
                    case Sorting.NEW:
                        btnSortNew.setChecked(true);
                        break;
                    case Sorting.TRENDING:
                        btnSortTrending.setChecked(true);
                        break;
                    case Sorting.PRICE_HIGH_TO_LOW:
                        btnVoucherPriceDesc.setChecked(true);
                        break;
                    case Sorting.PRICE_LOW_TO_HIGH:
                        btnVOucherPriceAsc.setChecked(true);
                        break;
                    case Sorting.POINT_HIGH_TO_LOW:
                        btnVoucherPointDesc.setChecked(true);
                        break;
                    case Sorting.POINT_LOW_TO_HIGH:
                        btnVoucherPointAsc.setChecked(true);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void setUICallbacks() {
        btnClose.setOnClickListener(this);
        btnSortNew.setOnClickListener(this);
        btnSortTrending.setOnClickListener(this);
        btnVoucherPriceDesc.setOnClickListener(this);
        btnVOucherPriceAsc.setOnClickListener(this);
        btnVoucherPointDesc.setOnClickListener(this);
        btnVoucherPointAsc.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v != btnClose){
            Intent intent = new Intent(ReedemFragment.RECEIVER_REWARD);
            intent.putExtra(ReedemFragment.SORT_REWARD_TYPE, (String) v.getTag());
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        }
        dismiss();
    }

    public void setSorting(int sorting) {
        this.sorting = sorting;
    }
}
