package com.ebizu.manis.mvp.luckydraw.luckydrawscreen;

import android.content.Context;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubsriberLambda;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawScreen;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ebizu on 04/08/17.
 */

public class LuckyDrawPresenter extends BaseViewPresenter implements ILuckyDrawPresenter {

    private final String TAG = getClass().getSimpleName();

    private ILuckyDrawView iLuckyDrawView;
    private LuckyDrawView luckyDrawView;
    private BaseActivity baseActivity;
    private Context context;
    private Subscription subsLuckyDraw;

    public LuckyDrawPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        luckyDrawView = (LuckyDrawView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsLuckyDraw != null) subsLuckyDraw.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadLuckyDrawScreenData(ManisApi manisApi) {
        releaseSubscribe(0);
        subsLuckyDraw = manisApi.getLuckyDrawScreen().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperLuckyDrawScreen>(luckyDrawView) {
                    @Override
                    public void onNext(WrapperLuckyDrawScreen wrapperLuckyDrawScreen) {
                        super.onNext(wrapperLuckyDrawScreen);
                        luckyDrawView.setLuckyDrawScreen(wrapperLuckyDrawScreen.getLuckyDrawScreen());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        if (!ConnectionDetector.isNetworkConnected(context))
                            luckyDrawView.noConnection();
                    }
                });
    }

    @Override
    public void loadLuckyDrawScreenData() {
        releaseSubscribe(0);
        RequestBody requestBody = new RequestBodyBuilder(luckyDrawView.getContext())
                .setFunction(ConfigManager.LuckyDraw.FUNCTION_GET_SCREEN)
                .create();
        subsLuckyDraw = getManisApiV2().getLuckyDrawScreen(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperLuckyDrawScreen>(luckyDrawView) {
                    @Override
                    protected void onSuccess(WrapperLuckyDrawScreen wrapperLuckyDrawScreen) {
                        super.onSuccess(wrapperLuckyDrawScreen);
                        luckyDrawView.setLuckyDrawScreen(wrapperLuckyDrawScreen.getLuckyDrawScreen());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        if (!ConnectionDetector.isNetworkConnected(context))
                            luckyDrawView.noConnection();
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                    }
                });
    }

}
