package com.ebizu.manis.mvp.snap.store.list.suggested;

import com.ebizu.manis.mvp.snap.store.SnapStoreHelper;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.response.WrapperSnapStoreSuggest;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public class SuggestedPresenter extends BaseViewPresenter implements ISuggestedPresenter {

    private SuggestedView suggestedView;
    private Subscription subsSuggestedStore;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        suggestedView = (SuggestedView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsSuggestedStore != null) subsSuggestedStore.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void getSnapStoresSuggest(int page, IBaseView.LoadType loadType) {
        suggestedView.setLoading(true);
        releaseSubscribe(0);
        subsSuggestedStore = suggestedView.getManisApi().getSnapStoresSuggest(SnapStoreHelper.getSnapStoreSuggest(page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperSnapStoreSuggest>(suggestedView) {
                    @Override
                    public void onNext(WrapperSnapStoreSuggest wrapperSnapStoreSuggest) {
                        super.onNext(wrapperSnapStoreSuggest);
                        suggestedView.addStoreSuggest(wrapperSnapStoreSuggest.getSnapStoreSuggest(), loadType);
                        suggestedView.setLoading(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        suggestedView.setLoading(false);
                    }
                });
    }

}
