package com.ebizu.manis.mvp.branddetail;

import com.ebizu.manis.root.IBaseView;

import java.util.ArrayList;

/**
 * Created by Raden on 7/21/17.
 */

public interface IBrandDetailPresenter {
    void loadBrandDetailList(String keyword , int page, int brand, IBaseView.LoadType loadType);
}
