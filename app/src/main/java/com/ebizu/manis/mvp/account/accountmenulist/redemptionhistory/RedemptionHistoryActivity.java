package com.ebizu.manis.mvp.account.accountmenulist.redemptionhistory;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 8/7/2017.
 */

public class RedemptionHistoryActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;
    @BindView(R.id.redemption_history_view)
    RedemptionHistoryView redemptionHistoryView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    private Context context;
    private int page = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redemption_history);
        context = this;
        ButterKnife.bind(this);
        redemptionHistoryView.setActivity(this);
        swipeRefresh.setOnRefreshListener(this);
        swipeRefresh.setColorSchemeColors(ContextCompat.getColor(this, R.color.base_pink));
        initView();
    }

    private void initView() {
        toolbarView.setTitle(R.string.cr_title);
        redemptionHistoryView.attachPresenter(new RedemptionHistoryPresenter());
        redemptionHistoryView.getRedemptionHistoryPresenter().loadRedemptionHistory(ManisApiGenerator.createServiceWithToken(context), page);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finishActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void finish() {
        super.finish();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.REDEMPTION_HISTORY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Back"
        );
    }
    @Override
    public void onRefresh() {
        redemptionHistoryView.currentPage = page;
        redemptionHistoryView.getRedemptionHistoryPresenter().loadRedemptionHistory(ManisApiGenerator.createServiceWithToken(context), page);
        if (swipeRefresh.isRefreshing()) {
            swipeRefresh.setRefreshing(false);
        }
    }
}
