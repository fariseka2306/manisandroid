package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.PurchaseHistory;
import com.ebizu.manis.model.PurchaseHistoryPaging;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbody.PurchaseHistoryBody;
import com.ebizu.manis.view.adapter.ListAdapter;
import com.ebizu.manis.view.adapter.PurchaseHistoryAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public class PurchaseHistoryView extends BaseView implements IPurchaseHistoryView,
        SwipeRefreshLayout.OnRefreshListener, ListAdapter.OnClickListener<PurchaseHistory> {

    private IPurchaseHistoryPresenter iPurchaseHistoryPresenter;
    private PurchaseHistoryAdapter purchaseHistoryAdapter;
    private PurchaseHistoryBody.Data purchaseHistoryBodyData;
    private PurchaseHistoryBody purchaseHistoryBody;

    private boolean isCreatedView = false;

    @BindView(R.id.view_no_internet_connection)
    View viewNoInternetConnection;

    @BindView(R.id.sh_spin_sort)
    Spinner spinOrder;

    @BindView(R.id.sh_spin_filter)
    Spinner spinFilter;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.rv_purchased_history)
    RecyclerView rvPurchaseHistory;

    @BindView(R.id.view_empty_purchased_voucher)
    View viewEmptyPurchasedVoucher;

    public PurchaseHistoryView(Context context) {
        super(context);
        createView(context);
    }

    public PurchaseHistoryView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public PurchaseHistoryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PurchaseHistoryView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.purchase_history_view, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        attachPresenter(new PurchaseHistoryPresenter());
        initView();
        initListener();
        initPurchaseHistoryView();
        addSpinnerListener();
    }

    private void initListener() {
        swipeRefreshLayout.setOnRefreshListener(this);
        purchaseHistoryAdapter.setOnScrollListener(() -> {
            purchaseHistoryBodyData.setPage(purchaseHistoryBodyData.getPage() + 1);
            purchaseHistoryBody = new PurchaseHistoryBody(getContext(), purchaseHistoryBodyData);
            iPurchaseHistoryPresenter.loadPurchaseHistory(purchaseHistoryBody, LoadType.SCROLL_LOAD);
        });
    }

    @Override
    public void onRefresh() {
        purchaseHistoryBodyData.setPage(ConfigManager.Reward.PURCHASE_HISTORY_FIRST_PAGE);
        purchaseHistoryBody = new PurchaseHistoryBody(getContext(), purchaseHistoryBodyData);
        iPurchaseHistoryPresenter.loadPurchaseHistory(purchaseHistoryBody, LoadType.SWIPE_LOAD);
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iPurchaseHistoryPresenter = (IPurchaseHistoryPresenter) iBaseViewPresenter;
        iPurchaseHistoryPresenter.attachView(this);
    }

    @Override
    public void setPurchaseHistory(PurchaseHistoryPaging purchaseHistoryPaging, IBaseView.LoadType loadType) {
        if (purchaseHistoryPaging.getPurchaseHistories().isEmpty() || purchaseHistoryPaging.getPurchaseHistories() == null) {
            purchaseHistoryAdapter.setPagination(false);
            showEmptyPurchasedVouchers();
        } else {
            purchaseHistoryAdapter.setPagination(purchaseHistoryPaging.getMore());
            loadPurchaseHistoryView(purchaseHistoryPaging.getPurchaseHistories(), loadType);
            dismissEmptyPurchasedVouchers();
        }

    }

    @Override
    public void loadPurchaseHistoryViewFail(LoadType loadType) {
        switch (loadType) {
            case CLICK_LOAD: {
                checkConnection(loadType);
                break;
            }
            case SCROLL_LOAD: {
                checkConnection(loadType);
                break;
            }
            case SWIPE_LOAD: {
                checkConnection(loadType);
                break;
            }
        }
    }

    @Override
    public IPurchaseHistoryPresenter getPurchaseHistoryPresenter() {
        return iPurchaseHistoryPresenter;
    }

    private void initView() {
        isCreatedView = true;
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.base_pink));
        purchaseHistoryAdapter = new PurchaseHistoryAdapter(getContext(), rvPurchaseHistory);
        purchaseHistoryAdapter.setPaginationAble(true);
        purchaseHistoryAdapter.setOnClickListener(this);
    }

    private void initPurchaseHistoryView() {
        purchaseHistoryBodyData = new PurchaseHistoryBody.Data();
        purchaseHistoryBodyData.setPage(ConfigManager.Reward.PURCHASE_HISTORY_FIRST_PAGE);
        purchaseHistoryBodyData.setSize(ConfigManager.Reward.PURCHASE_HISTORY_MAX_PAGE_SIZE);
        purchaseHistoryBodyData.setOrder(PurchaseHistoryBody.ORDER_DATE);
        purchaseHistoryBodyData.setCondition(PurchaseHistoryBody.STATUS_ALL);
        purchaseHistoryBody = new PurchaseHistoryBody(getContext(), purchaseHistoryBodyData);
        iPurchaseHistoryPresenter.loadPurchaseHistory(purchaseHistoryBody, LoadType.CLICK_LOAD);
    }

    private void addSpinnerListener() {
        //*** Filter ***
        List<String> filterList = new ArrayList<>();
        filterList.add(getContext().getString(R.string.ph_all_text));
        filterList.add(getContext().getString(R.string.ph_paid_text));
        filterList.add(getContext().getString(R.string.ph_unpaid_text));
        filterList.add(getContext().getString(R.string.ph_pending_text));
        ArrayAdapter<String> filterAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, filterList) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextSize(16);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setGravity(Gravity.CENTER);
                return v;
            }
        };
        filterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinFilter.setAdapter(filterAdapter);
        spinFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, Integer> filterMap = new HashMap<>();
                filterMap.put(getContext().getString(R.string.ph_all_text), PurchaseHistoryBody.STATUS_ALL);
                filterMap.put(getContext().getString(R.string.ph_paid_text), PurchaseHistoryBody.STATUS_PAID);
                filterMap.put(getContext().getString(R.string.ph_unpaid_text), PurchaseHistoryBody.STATUS_UNPAID);
                filterMap.put(getContext().getString(R.string.ph_pending_text), PurchaseHistoryBody.STATUS_PENDING);
                int selectedFilter = filterMap.get(spinFilter.getSelectedItem().toString());
                if (purchaseHistoryBodyData.getCondition() != selectedFilter) {
                    purchaseHistoryBodyData.setPage(ConfigManager.Reward.PURCHASE_HISTORY_FIRST_PAGE);
                    purchaseHistoryBodyData.setCondition(selectedFilter);
                    purchaseHistoryBody = new PurchaseHistoryBody(getContext(), purchaseHistoryBodyData);
                    if (isCreatedView)
                        iPurchaseHistoryPresenter.loadPurchaseHistory(purchaseHistoryBody, LoadType.CLICK_LOAD);
                }
                getAnalyticManager().trackEvent(
                        ConfigManager.Analytic.Category.PURCHASED_HISTORY,
                        ConfigManager.Analytic.Action.CLICK,
                        "Filter ".concat(String.valueOf(spinFilter)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //*** Order ***
        List<String> orderList = new ArrayList<>();
        orderList.add(getContext().getString(R.string.ph_date_text));
        orderList.add(getContext().getString(R.string.ph_amount_text));

        ArrayAdapter<String> orderAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, orderList) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextSize(16);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setGravity(Gravity.CENTER);
                return v;
            }
        };
        orderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinOrder.setAdapter(orderAdapter);
        spinOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, Integer> filterMap = new HashMap<>();
                filterMap.put(getContext().getString(R.string.ph_date_text), PurchaseHistoryBody.ORDER_DATE);
                filterMap.put(getContext().getString(R.string.ph_amount_text), PurchaseHistoryBody.ORDER_AMOUNT);
                int selectedSort = filterMap.get(spinOrder.getSelectedItem().toString());
                if (purchaseHistoryBodyData.getOrder() != selectedSort) {
                    purchaseHistoryBodyData.setPage(ConfigManager.Reward.PURCHASE_HISTORY_FIRST_PAGE);
                    purchaseHistoryBodyData.setOrder(selectedSort);
                    purchaseHistoryBody = new PurchaseHistoryBody(getContext(), purchaseHistoryBodyData);
                    if (isCreatedView)
                        iPurchaseHistoryPresenter.loadPurchaseHistory(purchaseHistoryBody, LoadType.CLICK_LOAD);
                }
                getAnalyticManager().trackEvent(
                        ConfigManager.Analytic.Category.PURCHASED_HISTORY,
                        ConfigManager.Analytic.Action.CLICK,
                        "Order ".concat(String.valueOf(selectedSort)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void loadPurchaseHistoryView(List<PurchaseHistory> purchaseHistories, LoadType loadType) {
        switch (loadType) {
            case CLICK_LOAD: {
                purchaseHistoryAdapter.addAdapterList(purchaseHistories);
                break;
            }
            case SCROLL_LOAD: {
                purchaseHistoryAdapter.insertAdapterList(purchaseHistories);
                break;
            }
            case SWIPE_LOAD: {
                purchaseHistoryAdapter.addAdapterList(purchaseHistories);
                break;
            }
        }
        rvPurchaseHistory.setVisibility(View.VISIBLE);
        if (purchaseHistoryAdapter.isEmpty())
            showEmptyPurchasedVouchers();
    }

    private void checkConnection(LoadType loadType) {
        if (!UtilManis.isNetworkConnected(getContext())) {
            switch (loadType) {
                case CLICK_LOAD: {
                    viewNoInternetConnection.setVisibility(View.VISIBLE);
                    if (rvPurchaseHistory.isShown())
                        rvPurchaseHistory.setVisibility(View.GONE);
                    if (viewEmptyPurchasedVoucher.isShown())
                        viewEmptyPurchasedVoucher.setVisibility(View.GONE);
                    break;
                }
                case SCROLL_LOAD: {
                    viewNoInternetConnection.setVisibility(VISIBLE);
                    break;
                }
                case SWIPE_LOAD: {
                    viewNoInternetConnection.setVisibility(VISIBLE);
                    if (rvPurchaseHistory.isShown())
                        rvPurchaseHistory.setVisibility(View.GONE);
                    if (viewEmptyPurchasedVoucher.isShown())
                        viewEmptyPurchasedVoucher.setVisibility(View.GONE);
                    break;
                }
            }
        }
    }

    public void showProgressBar(LoadType loadType) {
        viewNoInternetConnection.setVisibility(View.GONE);
        viewEmptyPurchasedVoucher.setVisibility(View.GONE);
        if (loadType.equals(LoadType.CLICK_LOAD) || loadType.equals(LoadType.SWIPE_LOAD)) {
            showProgressBar();
            if (rvPurchaseHistory.isShown())
                rvPurchaseHistory.setVisibility(View.GONE);
        } else {
            purchaseHistoryAdapter.showListProgressBar();
        }
        purchaseHistoryAdapter.setLoading(true);
    }

    public void dismissProgressBar(LoadType loadType) {
        switch (loadType) {
            case CLICK_LOAD: {
                dismissProgressBar();
                break;
            }
            case SCROLL_LOAD: {
                purchaseHistoryAdapter.dismissListProgressBar();
                break;
            }
            case SWIPE_LOAD: {
                swipeRefreshLayout.setRefreshing(false);
                dismissProgressBar();
                break;
            }
            default:
                break;
        }
        purchaseHistoryAdapter.setLoading(false);
    }

    private void showEmptyPurchasedVouchers() {
        rvPurchaseHistory.setVisibility(View.GONE);
        viewEmptyPurchasedVoucher.setVisibility(View.VISIBLE);
    }

    private void dismissEmptyPurchasedVouchers() {
        if (viewEmptyPurchasedVoucher.isShown() || viewNoInternetConnection.isShown()) {
            rvPurchaseHistory.setVisibility(View.GONE);
            viewEmptyPurchasedVoucher.setVisibility(GONE);
            viewNoInternetConnection.setVisibility(GONE);
            rvPurchaseHistory.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onClick(PurchaseHistory purchaseHistory) {
        if (purchaseHistory != null) {
            if (purchaseHistory.getVoucherType().equals(ConfigManager.Reward.VOUCHER_TYPE_PHYSICAL) &&
                    purchaseHistory.getPaymentStatus().equals(ConfigManager.PurchaseHistory.STATUS_PAID)) {
                PurchasedVoucherPhysicalDialog purchasedVoucherPhysicalDialog = new PurchasedVoucherPhysicalDialog(getContext());
                purchasedVoucherPhysicalDialog.setPurchasedVoucherPhysicalDialogView(purchaseHistory);
                purchasedVoucherPhysicalDialog.show();
            } else {
                PurchasedVoucherDialog purchasedVoucherDialog = new PurchasedVoucherDialog(getContext());
                purchasedVoucherDialog.setPurchaseHistoryView(purchaseHistory);
                purchasedVoucherDialog.show();
            }
        }
    }
}
