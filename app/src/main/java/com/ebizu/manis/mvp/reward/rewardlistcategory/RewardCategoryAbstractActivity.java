package com.ebizu.manis.mvp.reward.rewardlistcategory;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.reward.rewardlistcategory.rewardbulk.RewardListBulkView;
import com.ebizu.manis.mvp.reward.rewardlistcategory.rewardregular.RewardListCategoryView;
import com.ebizu.manis.mvp.sortreward.SortRewardFragment;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.requestbody.RewardCategoryBody;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;
import com.ebizu.sdk.reward.models.Filter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public abstract class RewardCategoryAbstractActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.toolbar)
    protected ToolbarView toolbarView;

    @BindView(R.id.rel_right)
    protected RelativeLayout relRight;

    @BindView(R.id.reward_category_view)
    public RewardListCategoryView rewardCategoryView;

    @BindView(R.id.reward_bulk_view)
    public RewardListBulkView rewardBulkView;

    public static final String SORT_REWARD_TYPE = "sort_reward_type";
    public static final String RECEIVER_REWARD = "receiver_reward";
    private Filter filter, filterBrand;
    public int sorting = 0;
    private RewardListCategoryAbstractView baseView;
    protected String rewardCategoryId, rewardCategoryName, rewardBrandName, rewardSearchKeyword, requestRewardType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_abstract_category);
        ButterKnife.bind(this);
        baseView = getRewardCategoryAbstractView();
        baseView.setVisibility(View.VISIBLE);
        baseView.setActivity(this);
        baseView.attachPresenter(new RewardListCategoryPresenter(this));
        LocalBroadcastManager.getInstance(this).registerReceiver(rewardBroadcastReceiver, new IntentFilter(RECEIVER_REWARD));
        initView();
        initParameter();
        initListener();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    @Override
    public void onClick(View view) {
        if (view.equals(relRight)) {
            sortReward();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(rewardBroadcastReceiver);
    }

    public void initView() {
        Bundle bundle = getIntent().getExtras();
        rewardCategoryId = bundle.getString(ConfigManager.Reward.REWARD_CATEGORY_ID);
        rewardCategoryName = bundle.getString(ConfigManager.Reward.REWARD_CATEGORY_NAME);
        rewardSearchKeyword = bundle.getString(ConfigManager.Reward.REWARD_SEARCH_KEYWORD, "");
        requestRewardType = bundle.getString(ConfigManager.Reward.REQUEST_REWARD_TYPE);
        filter = new Filter();
        List<String> filterCategories = new ArrayList<>();
        filterCategories.add(rewardCategoryId);
        filter.setCategories(filterCategories);
        filter.setSorting(sorting);
        setSortView();
    }

    /**
     * Invisible sort view equals type_search
     * Visible sort view equals type_category and type_home
     */
    private void setSortView() {
        if (isSortAble()) {
            toolbarView.setRightImage(R.drawable.sort_icon);
        }
    }

    private boolean isSortAble() {
        return null != rewardCategoryId || null != rewardBrandName;
    }

    public void initParameter() {

    }

    private void initListener() {
        relRight.setOnClickListener(this);
    }

    private final BroadcastReceiver rewardBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(SORT_REWARD_TYPE)) {
                int sortingID = Integer.parseInt(intent.getStringExtra(SORT_REWARD_TYPE));
                filter.setSorting(sortingID);
                sorting = sortingID;

                filterBrand = new Filter();
                List<String> filterCategories = new ArrayList<>();
                filterCategories.add(rewardBrandName);
                filterBrand.setBrands(filterCategories);
                filterBrand.setSorting(sortingID);
                rewardCategoryView.getRewardCategoryPresenter().loadRewardVoucherList(
                        new RewardCategoryBody(ConfigManager.Reward.REWARD_CATEGORY_PAGE,
                                ConfigManager.Reward.REWARD_CATEGORY_MAX_PAGE), rewardCategoryId == null ? filterBrand : filter, "");
            }
        }
    };

    public void sortReward() {
        SortRewardFragment sortRewardFragment = new SortRewardFragment();
        sortRewardFragment.setSorting(sorting);
        sortRewardFragment.show(getSupportFragmentManager(), null);
    }

    public abstract RewardListCategoryAbstractView getRewardCategoryAbstractView();

}
