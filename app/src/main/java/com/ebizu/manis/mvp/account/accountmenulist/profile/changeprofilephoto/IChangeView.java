package com.ebizu.manis.mvp.account.accountmenulist.profile.changeprofilephoto;

import com.ebizu.manis.model.Account;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by abizu-alvio on 7/26/2017.
 */

public interface IChangeView extends IBaseView {

    void setProfilePhoto(Account account);

    IChangePresenter getChangePresenter();

    void successUploadPhoto();

}
