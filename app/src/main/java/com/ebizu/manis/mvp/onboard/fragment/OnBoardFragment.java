package com.ebizu.manis.mvp.onboard.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.mvp.onboard.view.OnBoardInterface;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 8/22/17.
 */

public class OnBoardFragment extends Fragment {

    @BindView(R.id.parent)
    ViewGroup viewGroupOnBoard;
    @BindView(R.id.video_view_on_board)
    VideoView videoView;
    @BindView(R.id.frame_layout_on_board)
    FrameLayout frameLayout;
    @BindView(R.id.text_view_on_board_title)
    AppCompatTextView textViewTitle;
    @BindView(R.id.text_view_on_board_body)
    AppCompatTextView textViewBody;

    @BindView(R.id.text_view_prev)
    TextView textViewPrev;
    @BindView(R.id.text_view_next)
    TextView textViewNext;

    private OnCreateViewListener onCreateViewListener;
    private int rawVideo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_onboard, container, false);
        ButterKnife.bind(this, view);
        onCreateViewListener.onCreateView();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        videoView.stopPlayback();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (videoView.isPlaying())
            videoView.pause();
    }

    public void setTitle(String title) {
        textViewTitle.setText(title);
    }

    public void setMessageBody(String messageBody) {
        textViewBody.setText(messageBody);
    }

    public void setBackgroundColor(int colorBackground) {
        viewGroupOnBoard.setBackgroundColor(colorBackground);
        frameLayout.setBackgroundColor(colorBackground);
    }

    public void setVideoRaw(int rawVideo) {
        this.rawVideo = rawVideo;
    }

    public void setOnCreateViewListener(OnCreateViewListener onCreateViewListener) {
        this.onCreateViewListener = onCreateViewListener;
    }

    public void setVisibilityText(OnBoardInterface onBoardInterface, int positionFragment) {
        if (onBoardInterface.isFirst(positionFragment)) {
            textViewPrev.setVisibility(View.INVISIBLE);
            textViewNext.setVisibility(View.VISIBLE);
        } else if (onBoardInterface.isLast(positionFragment)) {
            textViewPrev.setVisibility(View.VISIBLE);
            textViewNext.setVisibility(View.INVISIBLE);
        } else {
            textViewPrev.setVisibility(View.VISIBLE);
            textViewNext.setVisibility(View.VISIBLE);
        }
    }

    public void setPageControlListener(OnClickPageListener onClickPageListener) {
        textViewPrev.setOnClickListener((v) -> {
            onClickPageListener.onPrev();
            new AnalyticManager(getContext()).trackEvent(
                    ConfigManager.Analytic.Category.ON_BOARDING,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Prev"
            );
        });
        textViewNext.setOnClickListener((v) -> {
            onClickPageListener.onNext();
            new AnalyticManager(getContext()).trackEvent(
                    ConfigManager.Analytic.Category.ON_BOARDING,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Next"
            );
        });
    }

    public void startVideo() {
        try {
            videoView.setVideoURI(Uri.parse("android.resource://" + getContext().getPackageName() + "/" + rawVideo));
            frameLayout.setVisibility(View.VISIBLE);
            videoView.setOnPreparedListener(mp -> {
                new Handler().postDelayed(() -> {
                    if (frameLayout.isShown())
                        frameLayout.setVisibility(View.INVISIBLE);
                }, 300);
                videoView.start();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface OnCreateViewListener {
        void onCreateView();
    }

    public interface OnClickPageListener {
        void onNext();

        void onPrev();
    }

}