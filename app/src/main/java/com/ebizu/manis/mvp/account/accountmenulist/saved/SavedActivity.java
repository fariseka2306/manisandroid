package com.ebizu.manis.mvp.account.accountmenulist.saved;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mac on 8/21/17.
 */

public class SavedActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;
    @BindView(R.id.saved_view)
    SavedView savedView;

    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved);
        context = this;
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        toolbarView.setTitle(R.string.ss_title);
        savedView.setActivity(this);
        savedView.attachPresenter(new SavedPresenter());
        savedView.getSavedPresenter().loadSaved(ManisApiGenerator.createServiceWithToken(context));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            performBackAnimation();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void performBackAnimation() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.SAVED_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Back"
        );
    }

}
