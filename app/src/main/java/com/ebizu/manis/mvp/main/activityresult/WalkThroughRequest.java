package com.ebizu.manis.mvp.main.activityresult;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.mvp.main.MainActivity;

public class WalkThroughRequest extends MainRequestCode implements IMainRequestCode {

    public WalkThroughRequest(MainActivity mainActivity) {
        super(mainActivity);
    }

    @Override
    public int requestCode() {
        return ConfigManager.Walkthrough.WALKTHROUGH;
    }

    @Override
    public void jobRequest(int resultCode, Intent data) {
        super.jobRequest(resultCode, data);
        if (resultCode == ConfigManager.Walkthrough.NOTIFICATION_WALK_THROUGH) {
            mainActivity.initWalkThroughNotification();
        } else if (resultCode == ConfigManager.Walkthrough.FINISH_WALK_THROUGH) {
            SharedPreferences prefFirst =
                    mainActivity.getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
            UtilSessionFirstIn.setShowWalktrough(prefFirst, false);
            mainActivity.showLuckyDraw();
        }
    }
}
