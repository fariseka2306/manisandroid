package com.ebizu.manis.mvp.reward;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.ebizu.manis.helper.LoadType;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.sdk.reward.EbizuReward;
import com.ebizu.sdk.reward.core.interfaces.Callback;
import com.ebizu.sdk.reward.models.Filter;
import com.ebizu.sdk.reward.models.Response;
import com.ebizu.sdk.reward.models.Reward;
import com.ebizu.sdk.reward.models.SessionData;

import static java.security.AccessController.getContext;

/**
 * Created by Raden on 7/26/17.
 */

public class ReedemPresenter implements IReedemPresenter {

    private final String TAG = getClass().getSimpleName();

    private Context context;
    private Activity activity;
    private int page;
    private IReedemView iReedemView;


    public ReedemPresenter(Context context,Activity activity){
        this.context = context;
        this.activity = activity;
    }

    public void attachView(IReedemView iReedemView){
        this.iReedemView = iReedemView;
    }

    @Override
    public void loadReedem() {
        Account account = new ManisSession(context).getAccountSession();
        if (context != null && !activity.isDestroyed()) {
            EbizuReward.getSession(context).login(
                    String.valueOf(account.getAccId()),
                    account.getAccScreenName(),
                    account.getAccFacebookEmail(),
                    account.getAccCountry(),
                    account.getAccCreatedDateTime(),
                    new com.ebizu.sdk.reward.core.interfaces.Callback<SessionData>() {

                        @Override
                        public void onSuccess(SessionData sessionData) {
                            if (getContext() != null) {
                                Filter filter = new Filter();
                                EbizuReward.getProvider(activity).filterReward(
                                        iReedemView.getSearchView().getQuery().toString(),filter,iReedemView.getPage(),20,true,new Callback<Response.Data<Reward>>(){

                                            @Override
                                            public void onSuccess(Response.Data<Reward> rewardData) {
//                                                iReedemView.setRedeem(rewardData);
                                            }

                                            @Override
                                            public void onFailure(String s) {
                                                Log.d(TAG, "onFailure: " + s.toString());
                                            }
                                        });
                            }
                        }

                        @Override
                        public void onFailure(String s) {
//                            checkFailedCode(iReedemView);
                        }
                    });
        }
    }

    @Override
    public void loadRedem(Filter rewardFilter, LoadType loadType) {
        showProgress(loadType);
        EbizuReward.getProvider(activity).filterReward(
                iReedemView.getSearchView().getQuery().toString(),rewardFilter,iReedemView.getPage(),20,true,new Callback<Response.Data<Reward>>(){

                    @Override
                    public void onSuccess(Response.Data<Reward> rewardData) {
                        iReedemView.setRedeem(rewardData,loadType);
                        dissmissProgress(loadType);
                    }

                    @Override
                    public void onFailure(String s) {
                        dissmissProgress(loadType);
                        Log.d(TAG, "onFailure: " + s.toString());
                    }
                });
    }

    private void showProgress(LoadType loadType){
        switch (loadType){
            case SEARCH_LOAD:
            {
                iReedemView.showProgress();
                break;
            }
            case FIRST_LOAD:
            {
                iReedemView.showProgress();
                break;
            }
            case SWIPE_LOAD:
            {
                iReedemView.showProgress();
                break;
            }

        }
    }

    private void dissmissProgress(LoadType loadType){
        switch (loadType){
            case SEARCH_LOAD:
            {
                iReedemView.dissmissProgress();
                break;
            }
            case FIRST_LOAD:
            {
                iReedemView.dissmissProgress();
                break;
            }
            case SWIPE_LOAD:
            {
                iReedemView.dissmissSwipeProgress();
                break;
            }
        }

    }

    public void checkFailedCode(IReedemView iReedemView,int code){
        if(code == 600){
            iReedemView.failedConnected();
        }else{
            iReedemView.noData("no Data");
        }
    }
}
