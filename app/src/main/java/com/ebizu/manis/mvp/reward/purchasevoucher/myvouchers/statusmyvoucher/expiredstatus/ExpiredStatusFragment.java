package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.statusmyvoucher.expiredstatus;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

import static io.fabric.sdk.android.services.concurrency.AsyncTask.init;

/**
 * Created by Raden on 11/13/17.
 */

public class ExpiredStatusFragment extends BaseFragment {

    @BindView(R.id.my_voucher_expired_view)
    VoucherExpiredView voucherExpiredView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_voucher_expired,container,false);
        ButterKnife.bind(this,view);
        voucherExpiredView.setActivity((BaseActivity) getActivity());
        Init();
        return view;
    }

    private void Init(){
        voucherExpiredView.attachPresenter(new ExpiredStatusPresenter());
    }
}
