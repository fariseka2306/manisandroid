package com.ebizu.manis.mvp.snap.store.list.searchresult;

import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.mvp.snap.store.SnapStoreHelper;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.WrapperSnapStore;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ebizu-User on 08/08/2017.
 */

public class SearchResultPresenter extends BaseViewPresenter implements ISearchResultPresenter {

    private SearchResultView searchResultView;
    private Subscription subsSearchStore;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        searchResultView = (SearchResultView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsSearchStore != null) subsSearchStore.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void findMerchant(String keyword, int page, IBaseView.LoadType loadType) {
        showProgress(loadType);
        releaseSubscribe(0);
        subsSearchStore = searchResultView.getManisApi()
                .findMerchantStore(SnapStoreHelper.getSearchMerchantStoreBody(keyword, page))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperSnapStore>(searchResultView) {
                               @Override
                               public void onNext(WrapperSnapStore wrapperSnapStore) {
                                   super.onNext(wrapperSnapStore);
                                   dismissProgress(loadType);
                                   searchResultView.addSnapStores(wrapperSnapStore.getSnapStoreResult(), loadType);
                               }

                               @Override
                               public void onError(Throwable e) {
                                   super.onError(e);
                                   dismissProgress(loadType);
                                   if (!ConnectionDetector.isNetworkConnected(searchResultView.getContext())) {
                                       searchResultView.showNotificationMessage(
                                               searchResultView.getResources().getString(R.string.error_no_connection));
                                   }
                               }

                               @Override
                               public void onErrorFailure(ErrorResponse errorResponse) {
                                   super.onErrorFailure(errorResponse);
                                   searchResultView.invisibleTitle();
                                   searchResultView.getSnapStoreRecyclerView().setVisibility(View.GONE);
                                   searchResultView.showNoDataStoreView();
                               }
                           }
                );
    }

    private void showProgress(IBaseView.LoadType loadType) {
        searchResultView.setLoading(true);
        searchResultView.visibleTitle();
        searchResultView.dismissNoDataStoreView();
        switch (loadType) {
            case SEARCH_LOAD: {
                searchResultView.progressBar.setIndeterminate(true);
                searchResultView.progressBar.setVisibility(View.VISIBLE);
                break;
            }
            case SCROLL_LOAD: {
                searchResultView.getSnapStoreRecyclerView().showProgressitem();
                break;
            }
        }
    }

    private void dismissProgress(IBaseView.LoadType loadType) {
        searchResultView.setLoading(false);
        searchResultView.dismissNoDataStoreView();
        switch (loadType) {
            case SEARCH_LOAD: {
                searchResultView.progressBar.setIndeterminate(false);
                searchResultView.progressBar.setVisibility(View.GONE);
                break;
            }
            case SWIPE_LOAD: {
                searchResultView.swipeRefresh.dismissSwipe();
                break;
            }
            case SCROLL_LOAD: {
                searchResultView.getSnapStoreRecyclerView().dismissProgressItem();
                break;
            }
        }
    }
}
