package com.ebizu.manis.mvp.store.storeoffer;

import com.ebizu.manis.model.Offer;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.OfferClaimBody;

/**
 * Created by halim_ebizu on 8/23/17.
 */

public interface IStoreOfferDetailPresenter extends IBaseViewPresenter {

    void loadPinOffer(ManisApi manisApi, Offer offer);

    void loadUnpinOffer(ManisApi manisApi, Offer offer);

    void loadClaimOffer(ManisApi manisApi, OfferClaimBody offerClaimBody);
}
