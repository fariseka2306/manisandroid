package com.ebizu.manis.mvp.phoneloginfacebookotp;

import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.Phone;
import com.ebizu.manis.root.IBaseActivityPresenter;
import com.ebizu.manis.root.IBaseViewPresenter;

/**
 * Created by FARIS_mac on 11/1/17.
 */

public interface IConfirmCodeFacebookOtpPresenter extends IBaseActivityPresenter {

    void signInWithFacebookOtp(FacebookOtpLoginActivity facebookOtpLoginActivity);

    void loginAccountManis(Phone phone, String regIdGcm, FacebookOtpLoginActivity facebookOtpLoginActivity);

    void signUpOtp(Account account, FacebookOtpLoginActivity facebookOtpLoginActivity);

    void registerRewardSession(Account account, FacebookOtpLoginActivity facebookOtpLoginActivity);

    void loginRewardSdk(Account account, FacebookOtpLoginActivity facebookOtpLoginActivity);
}
