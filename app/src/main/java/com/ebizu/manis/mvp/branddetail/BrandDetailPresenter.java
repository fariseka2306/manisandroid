package com.ebizu.manis.mvp.branddetail;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.ebizu.manis.model.BrandDetailParam;
import com.ebizu.manis.root.BaseActivityPresenter;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.google.gson.Gson;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Raden on 7/21/17.
 */

public class BrandDetailPresenter extends BaseActivityPresenter implements IBrandDetailPresenter {

    private final String TAG = getClass().getSimpleName();

    private BrandDetailActivity brandDetailActivity;
    private Context context;
    private IBrandDetailView iBrandDetailView;
    private Subscription subsBrandDetail;

    public BrandDetailPresenter(Context context) {
        this.context = context;
    }

    public void attachBrandDetail(IBrandDetailView iBrandDetailView) {
        this.iBrandDetailView = iBrandDetailView;
        this.brandDetailActivity = (BrandDetailActivity) iBrandDetailView;
    }

    @Override
    public void loadBrandDetailList(String keyword, int page, int brand, IBaseView.LoadType loadType) {
        initializedLoad(loadType);

        BrandDetailParam brandDetailParam = new BrandDetailParam();

        brandDetailParam.setKeyword(keyword);
        brandDetailParam.setBrand(brand);
        brandDetailParam.setPage(page);
        brandDetailParam.setSize(20);

        releaseSubscribe(0);
        ManisApi manisApi = ManisApiGenerator.createServiceWithToken(context);
        subsBrandDetail = manisApi.getBrandDetailList(new Gson().toJson(brandDetailParam)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        wrapperSnapStore -> {
                            if (null != wrapperSnapStore.getSnapStoreResult() && null != wrapperSnapStore.getSnapStoreResult().getStores()
                                    && !wrapperSnapStore.getSnapStoreResult().getStores().isEmpty()) {
                                dismissTypeProgress(loadType);
                                brandDetailActivity.addViewBrandDetail(wrapperSnapStore.getSnapStoreResult(), loadType);
                                Log.d(TAG, "loadBrandDetails: " + wrapperSnapStore.toString());
                            } else {
                                brandDetailActivity.noDataBrandDetail();
                            }
                        },
                        throwable -> {
                            dismissTypeProgress(loadType);
                            Log.d(TAG, "loadBrandDetails: " + throwable.toString());
                        }
                );
    }

    private void initializedLoad(IBaseView.LoadType loadType) {
        dismissAllProgress();
        showTypeProgress(loadType);
    }

    private void showTypeProgress(IBaseView.LoadType loadType) {
        brandDetailActivity.setLoading(true);
        switch (loadType) {
            case CLICK_LOAD: {
                brandDetailActivity.progressLoad.setVisibility(View.VISIBLE);
                break;
            }
            case SCROLL_LOAD:
                brandDetailActivity.getSnapStoreAdapter().loadProgress();
                break;
            case SWIPE_LOAD:
                break;
        }
    }

    private void dismissTypeProgress(IBaseView.LoadType loadType) {
        brandDetailActivity.setLoading(false);
        switch (loadType) {
            case CLICK_LOAD:
                break;
            case SCROLL_LOAD:
                brandDetailActivity.getSnapStoreAdapter().dissProgress();
                break;
            case SWIPE_LOAD:
                break;
        }
    }

    public void dismissAllProgress() {
        brandDetailActivity.progressLoad.setVisibility(View.GONE);
        if (brandDetailActivity.swipeHome.isRefreshing()) {
            brandDetailActivity.swipeHome.setRefreshing(false);
        }
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsBrandDetail != null) subsBrandDetail.unsubscribe();
    }

    @Override
    public void releaseAllSubscribe() {
        releaseSubscribe(0);
    }
}
