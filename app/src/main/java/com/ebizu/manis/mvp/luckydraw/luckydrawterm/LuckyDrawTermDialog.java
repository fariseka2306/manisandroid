package com.ebizu.manis.mvp.luckydraw.luckydrawterm;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApi;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 8/22/17.
 */

public class LuckyDrawTermDialog extends BaseActivity{

    @Inject
    Context context;

    @Inject
    ManisApi manisApi;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_lucky_draw_tnc);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {
    }
}
