package com.ebizu.manis.mvp.snap.form.receiptdetail;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 09/08/2017.
 */

public class FormStoreDetailView extends BaseView {

    @BindView(R.id.text_input_store_name)
    TextInputEditText textInputStoreName;
    @BindView(R.id.text_input_location)
    TextInputEditText textInputLocation;
    @BindView(R.id.text_view_total_amount)
    TextInputEditText textInputTotalAmount;

    private TextInputEditText[] textInputForms;

    public FormStoreDetailView(Context context) {
        super(context);
        createView(context);
    }

    public FormStoreDetailView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public FormStoreDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public FormStoreDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_form_store_details, null, false);
        addView(view, new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
        textInputForms = new TextInputEditText[]{textInputStoreName, textInputLocation, textInputTotalAmount};
    }

    public boolean isValidInput() {
        for (TextInputEditText textInput : textInputForms) {
            if (textInput.getText().toString().isEmpty())
                YoYo.with(Techniques.Shake).duration(500).playOn(textInput);
        }
        return !storeName().isEmpty() && !storeLocation().isEmpty() && !totalAmount().isEmpty();
    }

    public String storeName() {
        return textInputStoreName.getText().toString();
    }

    public String storeLocation() {
        return textInputLocation.getText().toString();
    }

    public String totalAmount() {
        return textInputTotalAmount.getText().toString();
    }
}
