package com.ebizu.manis.mvp.filtercategories;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.ebizu.sdk.reward.EbizuReward;
import com.ebizu.sdk.reward.core.interfaces.Callback;
import com.ebizu.sdk.reward.models.Category;

import java.util.List;

/**
 * Created by Raden on 8/2/17.
 */

public class FilterCategoriesPresenter implements IFilterCategoriesPresenter {

    private final String TAG = getClass().getSimpleName();

    private Context context;
    private IFilterCategoriesView iFilterCategoriesView;

    public FilterCategoriesPresenter(Context context){
        this.context = context;
    }

    public void attachView(IFilterCategoriesView iFilterCategoriesView){
        this.iFilterCategoriesView = iFilterCategoriesView;
    }

    @Override
    public void loadFilterCategories() {
        EbizuReward.getProvider(context).rewardCategory(new Callback<List<Category>>() {

            @Override
            public void onSuccess(List<Category> categories) {
                iFilterCategoriesView.setFilterCategories(categories);
            }

            @Override
            public void onFailure(String s) {
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
