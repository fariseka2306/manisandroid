package com.ebizu.manis.mvp.main.activityresult;

import android.content.Intent;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.Brand;
import com.ebizu.manis.model.InterestsStore;
import com.ebizu.manis.mvp.branddetail.BrandDetailActivity;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.mvp.snap.store.SnapStoreActivity;
import com.ebizu.manis.mvp.store.storecategorydetail.StoreCategoryDetailActivity;

/**
 * Created by ebizu on 11/21/17.
 */

public class SnapEarnGuideRequest extends MainRequestCode
        implements IMainRequestCode {

    public SnapEarnGuideRequest(MainActivity mainActivity) {
        super(mainActivity);
    }

    @Override
    public int requestCode() {
        return ConfigManager.Snap.SNAP_EARN_GUIDE_REQUEST_CODE;
    }

    @Override
    public void jobRequest(int resultCode, Intent data) {
        super.jobRequest(resultCode, data);
        if (resultCode == ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_BRAND) {
            Intent intent = new Intent(mainActivity, BrandDetailActivity.class);
            Brand brand = data.getParcelableExtra(ConfigManager.BrandDetails.BRAND_DETAILS);
            intent.putExtra(ConfigManager.BrandDetails.BRAND_DETAILS, brand);
            mainActivity.startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
        } else if (resultCode == ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_SNAP) {
            mainActivity.startActivityForResult(new Intent(mainActivity, SnapStoreActivity.class),
                    ConfigManager.Snap.RECEIPT_REQUEST_CODE);
        } else if (resultCode == ConfigManager.Snap.SNAP_EARN_GUIDE_TYPE_STORE) {
            Intent intent = new Intent(mainActivity, StoreCategoryDetailActivity.class);
            InterestsStore interestsStore = data.getParcelableExtra(ConfigManager.Store.INTEREST_STORE);
            intent.putExtra(ConfigManager.Store.INTEREST_STORE, interestsStore);
            mainActivity.startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
        }
    }

}
