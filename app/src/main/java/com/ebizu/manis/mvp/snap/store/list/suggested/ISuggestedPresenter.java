package com.ebizu.manis.mvp.snap.store.list.suggested;

import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public interface ISuggestedPresenter extends IBaseViewPresenter {

    void getSnapStoresSuggest(int page, IBaseView.LoadType loadType);

}
