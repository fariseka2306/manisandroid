package com.ebizu.manis.mvp.onboard.view;

import android.content.Context;

/**
 * Created by ebizu on 8/22/17.
 */

public class OnBoardInterface {

    private IOnBoard[] IOnBoards;
    private Context context;

    public OnBoardInterface(Context context) {
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        this.IOnBoards = new IOnBoard[]{
                new OnBoardSnap(context),
                new OnBoardEarn(context),
                new OnBoardStuff(context)
        };
    }

    public IOnBoard[] getViews() {
        return IOnBoards;
    }

    public String getTitle(int index) {
        for (IOnBoard IOnBoard : IOnBoards) {
            if (IOnBoard.index() == index) {
                return IOnBoard.title();
            }
        }
        return new OnBoardSnap(context).title();
    }

    public String getMessageBody(int index) {
        for (IOnBoard IOnBoard : IOnBoards) {
            if (IOnBoard.index() == index) {
                return IOnBoard.messageBody();
            }
        }
        return new OnBoardSnap(context).messageBody();
    }

    public int getColorBackground(int index) {
        for (IOnBoard IOnBoard : IOnBoards) {
            if (IOnBoard.index() == index) {
                return IOnBoard.colorBackground();
            }
        }
        return new OnBoardSnap(context).colorBackground();
    }

    public int getRawVideo(int index) {
        for (IOnBoard IOnBoard : IOnBoards) {
            if (IOnBoard.index() == index) {
                return IOnBoard.rawVideo();
            }
        }
        return new OnBoardSnap(context).rawVideo();
    }

    public boolean isLast(int position) {
        return position == IOnBoards.length - 1;
    }

    public boolean isFirst(int position) {
        return position == 0;
    }

    public Context getContext() {
        return context;
    }
}
