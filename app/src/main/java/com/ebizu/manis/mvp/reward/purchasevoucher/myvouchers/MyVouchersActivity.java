package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.Constants;
import com.ebizu.manis.helper.MyVoucherConstant;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.mvp.reward.purchasevoucher.PurchaseRewardActivity;
import com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.statusmyvoucher.expiredstatus.ExpiredStatusFragment;
import com.ebizu.manis.view.adapter.AdapterHolder;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 10/2/17.
 */

public class MyVouchersActivity extends PurchaseRewardActivity {

    @Inject
    Context context;

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;

    @BindView(R.id.rel_tab_available)
    RelativeLayout relTabAvailable;

    @BindView(R.id.rel_tab_expired)
    RelativeLayout relTabExpired;

    @BindView(R.id.view_available_text)
    TextView viewAvailabelText;

    @BindView(R.id.view_expired_text)
    TextView viewExpiredText;

    @BindView(R.id.pager_tab_my_voucher)
    ViewPager viewPager;

    public AdapterHolder mAdapter;
    private MyVoucherNewFragment myVoucherNewFragment;
    private ExpiredStatusFragment expiredStatusFragment;
//    private ViewPager viewPager;

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int page) {
            setCurrentItemHeader(page);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

        @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_voucher_view);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {
        toolbarView.setTitle(context.getString(R.string.my_voucher_text), R.drawable.navigation_back_btn);
        myVoucherNewFragment = new MyVoucherNewFragment();
        expiredStatusFragment = new ExpiredStatusFragment();

        mAdapter = new AdapterHolder(getSupportFragmentManager(),getFragmentStatus());
        viewPager.setAdapter(mAdapter);

        viewPager.addOnPageChangeListener(pageChangeListener);
        viewPager.setOffscreenPageLimit(Constants.MAX_FRAGMENT_TO_DESTROY);

        relTabAvailable.setOnClickListener(new HeaderListener(MyVoucherConstant.POS_AVAILABLE));
        relTabExpired.setOnClickListener(new MyVouchersActivity.HeaderListener(MyVoucherConstant.POS_EXPIRED));

        relTabAvailable.setSelected(true);
    }

    private void setCurrentItemHeader(int position){
        relTabAvailable.setSelected(position == 0);
        relTabExpired.setSelected(position == 1);

    }

    private List<Fragment> getFragmentStatus(){
        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(myVoucherNewFragment);
        fragmentList.add(expiredStatusFragment);

        return fragmentList;
    }

    private class HeaderListener implements View.OnClickListener {
        private int position;

        HeaderListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            if (viewPager != null && viewPager.getCurrentItem() != position) {
                viewPager.setCurrentItem(position);
            }
            UtilManis.closeKeyboard(baseActivity(), v);
        }
    }

}
