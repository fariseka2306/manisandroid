package com.ebizu.manis.mvp.beacon;

import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.BeaconPromoBody;

/**
 * Created by ebizu on 8/23/17.
 */

public interface IBeaconPresenter {

    void getBeaconPromos(ManisApi manisApi, BeaconPromoBody beaconPromoBody);

    void getIntevalBeacon(ManisApi manisApi);
}
