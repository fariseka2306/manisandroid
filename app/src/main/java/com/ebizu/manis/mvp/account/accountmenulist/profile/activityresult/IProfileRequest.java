package com.ebizu.manis.mvp.account.accountmenulist.profile.activityresult;

import android.content.Intent;

/**
 * Created by ebizu on 10/13/17.
 */

public interface IProfileRequest {

    int requestCode();

    void doRequest(int resultCode, Intent data);
}
