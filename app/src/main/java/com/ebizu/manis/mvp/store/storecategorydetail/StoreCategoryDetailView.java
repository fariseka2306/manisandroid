package com.ebizu.manis.mvp.store.storecategorydetail;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.InterestsStore;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.StoreResults;
import com.ebizu.manis.mvp.store.storenearby.PaginationScrollListener;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.StoreInterestBody;
import com.ebizu.manis.view.adapter.StoreAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 7/29/17.
 */

public class StoreCategoryDetailView extends BaseView implements IStoreCategoryDetailView,
        SwipeRefreshLayout.OnRefreshListener, AppBarLayout.OnOffsetChangedListener {

    private LinearLayoutManager linearLayoutManager;
    private IStoreCategoryDetailPresenter iStoreCategoryDetailPresenter;
    private InterestsStore interestsStore;
    private StoreAdapter storeAdapter;
    private StoreInterestBody storeInterestBody;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    @BindView(R.id.sd_toolbar)
    Toolbar toolbar;

    @BindView(R.id.sd_collapsing)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.rv_stores)
    RecyclerView rvStores;

    @BindView(R.id.sd_img_background)
    ImageView sdImgBackground;

    @BindView(R.id.sd_txt_category)
    TextView sdTxtCategory;

    /*@BindView(R.id.progress_bar_circle)
    ProgressBar progressBarCircle;*/

    @BindView(R.id.swipe_store)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.sd_appbarlayout)
    AppBarLayout appBarLayout;

    private int lastChance;

    public StoreCategoryDetailView(Context context) {
        super(context);
        createView(context);
    }

    public StoreCategoryDetailView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public StoreCategoryDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public StoreCategoryDetailView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.store_category_detail_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new StoreCategoryDetailPresenter(context));
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iStoreCategoryDetailPresenter = (IStoreCategoryDetailPresenter) iBaseViewPresenter;
        iStoreCategoryDetailPresenter.attachView(this);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        initView();
        initListener();
        initActionBar();
    }

    @Override
    public void loadStoreCategory(StoreResults storeResults, StoreInterestBody storeInterestBody) {
        this.storeInterestBody = storeInterestBody;
        storeAdapter.addAll(storeResults.getStores());
        rvStores.setVisibility(VISIBLE);
        if (storeResults.getMore()) {
            storeAdapter.addLoadingFooter();
        } else {
            isLastPage = true;
        }
    }

    @Override
    public void loadStoreCategoryPaging(StoreResults storeResults, StoreInterestBody storeInterestBody) {
        storeAdapter.removeLoadingFooter();
        this.storeInterestBody = storeInterestBody;
        storeAdapter.addAll(storeResults.getStores());
        if (storeResults.getMore()) {
            storeAdapter.addLoadingFooter();
        } else {
            isLastPage = true;
        }
    }

    @Override
    public void showNotificationMessage(String message) {
        super.showNotificationMessage(message);
    }

    @Override
    public IStoreCategoryDetailPresenter getStoreCategoryDetailPresenter() {
        return iStoreCategoryDetailPresenter;
    }

    @Override
    public void onRefresh() {
        getStoreCategoryDetailPresenter().loadStoreCategoryDetail(ManisApiGenerator.createService(getContext()), storeInterestBody);
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_STORES_CATEGORIES,
                ConfigManager.Analytic.Action.REFRESH,
                "Swipe Refresh"
        );
    }

    public void setViewHeader(InterestsStore interestsStore, StoreInterestBody storeInterestBody) {
        this.storeInterestBody = storeInterestBody;
        this.interestsStore = interestsStore;
        Glide.with(getContext())
                .load(interestsStore.getAssets().getPhoto())
                .centerCrop()
                .placeholder(R.drawable.default_pic_store_logo)
                .into(sdImgBackground);
        sdTxtCategory.setText(interestsStore.getName());
    }


    private void initActionBar() {
        collapsingToolbarLayout.setTitle(interestsStore.getName());
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        StoreCategoryDetailActivity storeCategoryDetailActivity = (StoreCategoryDetailActivity) getContext();
        storeCategoryDetailActivity.setSupportActionBar(toolbar);
        storeCategoryDetailActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        storeCategoryDetailActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        storeCategoryDetailActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.navigation_back_btn_white);

        sdTxtCategory.setText(interestsStore.getName());
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (lastChance != verticalOffset) {
            sdTxtCategory.setWidth((int) (sdTxtCategory.getWidth() + (float) (-1 * verticalOffset) / appBarLayout.getTotalScrollRange() * sdTxtCategory.getX() * 2));
        }
        lastChance = verticalOffset;
    }

    private void initView() {
        storeAdapter = new StoreAdapter(getBaseActivity(), new ArrayList<>());
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvStores.setLayoutManager(linearLayoutManager);
        rvStores.setAdapter(storeAdapter);
        rvStores.setVisibility(VISIBLE);
        storeAdapter.setOnClickItemListener(store -> getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_STORES_CATEGORIES,
                ConfigManager.Analytic.Action.ITEM_CLICK,
                "Item Category " + store.getName()
        ));
        initCategoryPaging((LinearLayoutManager) rvStores.getLayoutManager());
    }

    private void initCategoryPaging(LinearLayoutManager linearLayoutManager) {
        rvStores.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                //luckyDrawEntryBody.setPage(luckyDrawEntryBody.getPage() + 1);
                new Handler().postDelayed(() -> getStoreCategoryDetailPresenter().loadStoreCategoryDetailPaging(
                        ManisApiGenerator.createServiceWithToken(getContext()), storeInterestBody), 1000);
            }

            @Override
            public int getTotalPageCount() {
                return storeInterestBody.getSize();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        new Handler().postDelayed(() -> getStoreCategoryDetailPresenter().loadStoreCategoryDetail(
                ManisApiGenerator.createServiceWithToken(getContext()), storeInterestBody), 1000);
    }

    private void initListener() {
        swipeRefreshLayout.setOnRefreshListener(this);
        appBarLayout.addOnOffsetChangedListener(this);
    }


}
