package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistorypyshicalfactory;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.ebizu.manis.model.PurchaseHistory;

/**
 * Created by halim_ebizu on 11/16/17.
 */

public class PurchasePyshicalFactory {

    private PurchaseHistory purchaseHistory;
    private PurchasePyshicalStatus[] purchasePyshicalStatuses;

    public PurchasePyshicalFactory(PurchaseHistory purchaseHistory, Context context) {
        this.purchaseHistory = purchaseHistory;
        instacePurchasePyshicalStatuses(context, purchaseHistory);
    }

    private void instacePurchasePyshicalStatuses(Context context, PurchaseHistory purchaseHistory) {
        purchasePyshicalStatuses = new PurchasePyshicalStatus[]{
                new PurchasePyshicalUnderReview(purchaseHistory, context),
                new PurchasePyshicalProcessing(purchaseHistory, context),
                new PurchasePyshicalPending(purchaseHistory, context),
                new PurchasePyshicalUndelivered(purchaseHistory, context),
                new PurchasePyshicalRejected(purchaseHistory, context),
                new PurchasePyshicalShipped(purchaseHistory, context)
        };
    }


    public String getStatus() {
        String status = "";
        for (PurchasePyshicalStatus purchasePyshicalStatus : purchasePyshicalStatuses) {
            if (purchaseHistory.getDeliveryStatus().equals(purchasePyshicalStatus.getDeliveryStatus())) {
                status = purchasePyshicalStatus.getStatus();
                break;
            }
        }
        return status;
    }

    public Drawable imageIcon() {
        Drawable drawable = null;
        for (PurchasePyshicalStatus purchasePyshicalStatus : purchasePyshicalStatuses) {
            if (purchaseHistory.getDeliveryStatus().equals(purchasePyshicalStatus.getDeliveryStatus())) {
                drawable = purchasePyshicalStatus.imageIcon();
                break;
            }
        }
        return drawable;
    }

    public String getInstruction() {
        String instructionMessage = "";
        for (PurchasePyshicalStatus purchasePyshicalStatus : purchasePyshicalStatuses) {
            if (purchaseHistory.getDeliveryStatus().equals(purchasePyshicalStatus.getDeliveryStatus())) {
                instructionMessage = purchasePyshicalStatus.getInstruction();
                break;
            }
        }
        return instructionMessage;
    }

    public int getInstructionVisibility() {
        int instructionVisibility = View.GONE;
        for (PurchasePyshicalStatus purchasePyshicalStatus : purchasePyshicalStatuses) {
            if (purchaseHistory.getDeliveryStatus().equals(purchasePyshicalStatus.getDeliveryStatus())) {
                instructionVisibility = purchasePyshicalStatus.getInstructionVisibility();
                break;
            }
        }

        return instructionVisibility;
    }
}
