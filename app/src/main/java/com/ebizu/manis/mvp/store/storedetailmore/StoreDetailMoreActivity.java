package com.ebizu.manis.mvp.store.storedetailmore;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.StoreDetail;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Halim on 7/22/17.
 */

public class StoreDetailMoreActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;

    @BindView(R.id.store_detail_more_view)
    StoreDetailMoreView storeDetailMoreView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stores_detail_more);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {

        Bundle bundle = getIntent().getExtras();
        Store store = bundle.getParcelable(ConfigManager.Store.STORE_DETAIL_TITLE);
        StoreDetail storeDetail = bundle.getParcelable(ConfigManager.Store.STORE_DETAIL_MORE);

        if (store != null) {
            if (!store.getName().isEmpty()) {
                String[] storeName = store.getName().split("@");
                RelativeLayout.LayoutParams params =
                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                params.setMarginStart(20);
                toolbarView.setTitle(params, storeName[0], R.drawable.navigation_back_btn_black);
            }
            storeDetailMoreView.setView(store, storeDetail);
        }
    }
}
