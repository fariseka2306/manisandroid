package com.ebizu.manis.mvp.reward.newreward;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ItemOffsetDecoration;
import com.ebizu.manis.manager.rewardcategory.RewardCategoryManager;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.adapter.NewRewardAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FARIS_mac on 9/22/17.
 */

public class NewRewardView extends BaseView implements INewRewardView, NewRewardAdapter.RewardCategoryListener, SwipeRefreshLayout.OnRefreshListener {

    private NewRewardAdapter newRewardAdapter;
    private INewRewardPresenter iNewRewardPresenter;

    @BindView(R.id.rv_reward_category)
    RecyclerView rvRewardCategory;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;

    public NewRewardView(Context context) {
        super(context);
        createView(context);
    }

    public NewRewardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public NewRewardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NewRewardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.reward_view, null, false);
        addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        attachPresenter(new NewRewardPresenter(context));
        swipeRefresh.setOnRefreshListener(this);
        initView();
        initListener();
    }

    public void initView() {
        newRewardAdapter = new NewRewardAdapter(getContext(), new ArrayList<Integer>(), new ArrayList<String>(), new ArrayList<String>());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        rvRewardCategory.setLayoutManager(gridLayoutManager);
        rvRewardCategory.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.five_dp));
        rvRewardCategory.setAdapter(newRewardAdapter);
    }

    private void initListener() {
        newRewardAdapter.setRewardCategoryListener(this);
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iNewRewardPresenter = (INewRewardPresenter) iBaseViewPresenter;
        iNewRewardPresenter.attachView(this);
    }

    @Override
    public void loadImageCategory(List<Integer> imageCategories, List<String> categoriesName, List<String> categoriesId) {
        rvRewardCategory.setVisibility(VISIBLE);
        newRewardAdapter.addCategories(imageCategories, categoriesName, categoriesId);
    }

    @Override
    public void onRetry() {
        super.onRetry();
        getNewRewardPresenter().loadCategoryImage();
    }

    @Override
    public INewRewardPresenter getNewRewardPresenter() {
        return iNewRewardPresenter;
    }

    @Override
    public void onRewardCategoryListener(String categoryName, String categoryId) {
        RewardCategoryManager.onRewardClick(getContext(),getBaseActivity(), categoryName, categoryId);
    }

    @Override
    public void onRefresh() {
        newRewardAdapter.clearReward();
        getNewRewardPresenter().loadCategoryImage();
        if (swipeRefresh.isRefreshing()) {
            swipeRefresh.setRefreshing(false);
        }
    }
}
