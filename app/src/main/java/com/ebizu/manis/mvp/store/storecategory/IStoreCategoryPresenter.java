package com.ebizu.manis.mvp.store.storecategory;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by firef on 7/10/2017.
 */

public interface IStoreCategoryPresenter extends IBaseViewPresenter {

    void loadInterestData(ManisApi manisApi);

}
