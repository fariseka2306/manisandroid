package com.ebizu.manis.mvp.onboard.view;

/**
 * Created by ebizu on 8/22/17.
 */

public interface IOnBoard {

    int index();

    String title();

    String messageBody();

    int colorBackground();

    int rawVideo();

}
