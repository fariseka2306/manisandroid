package com.ebizu.manis.mvp.luckydraw.luckydrawsubmit;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.AutoResizeTextView;
import com.ebizu.manis.model.LuckyDrawUpload;
import com.ebizu.manis.model.PrizeDetail;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by default on 19/10/17.
 */

public class LuckyDrawSubmitDialog extends BaseDialogManis {

    @BindView(R.id.image_view_base)
    ImageView imageViewBase;

    @BindView(R.id.image_view_ticket_icon)
    ImageView imageViewTicketIcon;

    @BindView(R.id.text_view_title_submitted)
    AutoResizeTextView textViewTitleSubmitted;

    @BindView(R.id.text_view_sub_title_submitted)
    TextView textViewSubTitleSubmitted;

    @BindView(R.id.text_view_submitted_desc)
    TextView textViewSubmittedDesc;

    private OnButtonClickListener onButtonClickListener;

    public LuckyDrawSubmitDialog(@NonNull Context context) {
        super(context);
        createView(context);
    }

    public LuckyDrawSubmitDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        createView(context);
    }

    public LuckyDrawSubmitDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        createView(context);
    }

    private void createView(Context context) {
        createDialogView();
    }

    private void createDialogView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_lucky_draw_submitted, null, false);
        addContentView(view, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
    }

    public void setOnButtonClickListener(OnButtonClickListener onButtonClickListener) {
        this.onButtonClickListener = onButtonClickListener;
    }


    @OnClick(R.id.button_lucky_draw)
    void onClickButtonLD() {
        if (null != onButtonClickListener)
            onButtonClickListener.onLuckyDraw();
    }

    @OnClick(R.id.button_ok)
    void onClickButtonOk() {
        if (null != onButtonClickListener)
            onButtonClickListener.onOk();
    }

    public void setLuckyDrawSubmitDialogView(LuckyDrawUpload luckyDrawUpload) {
        StringBuilder sbLuckyDrawDesc = new StringBuilder().append(getContext().getString(R.string.ld_submitted_desc))
                .append("\n")
                .append("(")
                .append(luckyDrawUpload.getPrizes().getPrizeTitle())
                .append(": ");
        if (luckyDrawUpload.getPrizes().getPrizeDetails().size() == 1) {
            sbLuckyDrawDesc.append(luckyDrawUpload.getPrizes().getPrizeDetails().get(0).getPrizeName());
        } else {
            for (int i = 0; i < luckyDrawUpload.getPrizes().getPrizeDetails().size(); i++) {
                if (i == luckyDrawUpload.getPrizes().getPrizeDetails().size() - 1) {
                    sbLuckyDrawDesc.append(getContext().getString(R.string.snap_luckydraw_submitted_and_separator))
                            .append(" ")
                            .append(luckyDrawUpload.getPrizes().getPrizeDetails().get(i).getPrizeName());
                } else {
                    sbLuckyDrawDesc.append(luckyDrawUpload.getPrizes().getPrizeDetails().get(i).getPrizeName())
                            .append(", ");
                }

            }
        }
        sbLuckyDrawDesc.append(")");
        textViewSubmittedDesc.setText(sbLuckyDrawDesc.toString());
    }

    public interface OnButtonClickListener {

        void onLuckyDraw();

        void onOk();
    }
}