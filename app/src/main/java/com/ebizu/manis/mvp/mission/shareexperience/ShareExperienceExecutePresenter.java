package com.ebizu.manis.mvp.mission.shareexperience;

import android.content.Context;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.manager.tracker.TrackerConstant;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseV2Subscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperiencePostRB;
import com.ebizu.manis.service.manis.responsev2.status.Status;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperMissionExecute;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ShareExperienceExecutePresenter extends BaseViewPresenter implements IShareExperienceExecute {

    private ManisApi manisApi;
    private Context context;
    private Subscription subsExecute;

    public ShareExperienceExecutePresenter(Context context) {
        this.context = context;
    }

    @Override
    public void executeShareExp(ShareExperienceDetailActivity shareExperienceDetailActivity, ShareExperiencePostRB shareExperiencePostRB) {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceMission(context);
        releaseSubscribe(0);
        subsExecute = manisApi.postShareExperience(BuildConfig.MISSION_URL_PARAM, shareExperiencePostRB)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseV2Subscriber<WrapperMissionExecute>(shareExperienceDetailActivity) {
                    @Override
                    public void onNext(WrapperMissionExecute wrapperMissionExecute) {
                        super.onNext(wrapperMissionExecute);
                        shareExperienceDetailActivity.dismissProgressBarDialog();
                        shareExperienceDetailActivity.showDialogSuccessMission(wrapperMissionExecute);
                    }

                    @Override
                    protected void onErrorFailure(Status status) {
                        super.onErrorFailure(status);
                        TrackerManager.getInstance().setTrackerData(
                                new TrackerStandartRequest(TrackerConstant.kTrackerOriginPageMissions, TrackerConstant.kTrackerPageMissionErrorPopup, "", "", String.valueOf(status.getCode()), status.getMessageClient())
                        );
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        shareExperienceDetailActivity.dismissProgressBarDialog();
                        NegativeScenarioManager.show(e, shareExperienceDetailActivity, NegativeScenarioManager.NegativeView.DIALOG);
                    }
                });
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsExecute != null) subsExecute.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }
}
