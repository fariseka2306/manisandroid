package com.ebizu.manis.mvp.store.storeoffer;

import com.ebizu.manis.model.Redeem;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.service.manis.response.ResponseData;

/**
 * Created by halim_ebizu on 8/23/17.
 */

public interface IStoreOfferDetailView extends IBaseView {

    void setPinOffer(ResponseData responseData);

    void setUnpiOffer(ResponseData responseData);

    void claimOffer(Redeem redeem);

    IStoreOfferDetailPresenter getStoreOfferDetailPresenter();
}
