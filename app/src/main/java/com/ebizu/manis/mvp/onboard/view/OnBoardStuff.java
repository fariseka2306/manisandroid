package com.ebizu.manis.mvp.onboard.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;

/**
 * Created by ebizu on 8/22/17.
 */

public class OnBoardStuff extends OnBoard
        implements IOnBoard {

    public OnBoardStuff(Context context) {
        super(context);
    }

    @Override
    public int index() {
        return 2;
    }

    @Override
    public String title() {
        return context.getResources().getString(R.string.exp_freestuff);
    }

    @Override
    public String messageBody() {
        return context.getResources().getString(R.string.exp_freestuff_detail);
    }

    @Override
    public int colorBackground() {
        return ContextCompat.getColor(context, R.color.on_board_yellow);
    }

    @Override
    public int rawVideo() {
        return R.raw.onboard3;
    }
}
