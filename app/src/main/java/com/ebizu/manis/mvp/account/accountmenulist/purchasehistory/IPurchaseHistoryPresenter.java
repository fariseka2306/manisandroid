package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory;

import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbody.PurchaseHistoryBody;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public interface IPurchaseHistoryPresenter extends IBaseViewPresenter {

    void loadPurchaseHistory(PurchaseHistoryBody purchaseHistoryBody, IBaseView.LoadType loadType);
}
