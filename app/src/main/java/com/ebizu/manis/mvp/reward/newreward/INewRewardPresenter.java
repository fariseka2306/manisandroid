package com.ebizu.manis.mvp.reward.newreward;

import com.ebizu.manis.root.IBaseViewPresenter;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public interface INewRewardPresenter extends IBaseViewPresenter {

    void loadCategoryImage();

}
