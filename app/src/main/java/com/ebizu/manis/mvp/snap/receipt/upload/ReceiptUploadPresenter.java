package com.ebizu.manis.mvp.snap.receipt.upload;

import android.graphics.Bitmap;
import android.util.Log;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.model.snap.Photo;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseActivityPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.rx.ResponseSubsriberLambda;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptDataBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawUpload;
import com.google.gson.Gson;

import okhttp3.MultipartBody;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ebizu on 8/21/17.
 */

public class ReceiptUploadPresenter extends BaseActivityPresenter
        implements IReceiptUploadPresenter {

    private ReceiptUploadActivity receiptUploadActivity;
    private Subscription subsUploadReceipt;

    @Override
    public void attachView(BaseActivity view) {
        super.attachView(view);
        receiptUploadActivity = (ReceiptUploadActivity) view;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsUploadReceipt != null) subsUploadReceipt.unsubscribe();
    }

    @Override
    public void releaseAllSubscribe() {
        releaseSubscribe(0);
    }

    @Override
    public void uploadReceiptImage(okhttp3.RequestBody requestBody, MultipartBody.Part multipartBody) {
        receiptUploadActivity.showProgressBarDialog("Upload Receipt...");
        releaseSubscribe(0);
        subsUploadReceipt = getManisApi().uploadReceiptImage(requestBody, multipartBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ResponseSubscriber<ResponseData>(receiptUploadActivity) {
                    @Override
                    public void onNext(ResponseData responseData) {
                        super.onNext(responseData);
                        receiptUploadActivity.showUploadSnapEarnPoint();
                        receiptUploadActivity.dismissProgressBarDialog();
                        receiptUploadActivity.onSuccessUpload(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        receiptUploadActivity.dismissProgressBarDialog();
                        if (!ConnectionDetector.isNetworkConnected(getContext()))
                            NegativeScenarioManager.show(e, receiptUploadActivity, NegativeScenarioManager.NegativeView.DIALOG);
                    }

                    @Override
                    public void onErrorFailure(ErrorResponse errorResponse) {
                        super.onErrorFailure(errorResponse);
                        NegativeScenarioManager.show(errorResponse, receiptUploadActivity, NegativeScenarioManager.NegativeView.DIALOG);
                    }
                });
    }

    @Override
    public void uploadReceiptImageLuckyDraw(okhttp3.RequestBody requestBody, MultipartBody.Part multipartBody) {
        receiptUploadActivity.showProgressBarDialog("Upload Receipt...");
        releaseSubscribe(0);
        subsUploadReceipt = getManisApi().uploadReceiptImageLuckyDraw(requestBody, multipartBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new ResponseSubscriber<WrapperLuckyDrawUpload>(receiptUploadActivity) {
                    @Override
                    public void onNext(WrapperLuckyDrawUpload wrapperLuckyDrawUpload) {
                        super.onNext(wrapperLuckyDrawUpload);
                        receiptUploadActivity.dismissProgressBarDialog();
                        receiptUploadActivity.onSuccessUpload(true, wrapperLuckyDrawUpload.getLuckyDrawUpload());
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        receiptUploadActivity.dismissProgressBarDialog();
                        if (!ConnectionDetector.isNetworkConnected(getContext()))
                            NegativeScenarioManager.show(e, receiptUploadActivity, NegativeScenarioManager.NegativeView.DIALOG);
                    }

                    @Override
                    public void onErrorFailure(ErrorResponse errorResponse) {
                        super.onErrorFailure(errorResponse);
                        NegativeScenarioManager.show(errorResponse, receiptUploadActivity, NegativeScenarioManager.NegativeView.DIALOG);
                    }
                });
    }

    @Override
    public void uploadReceipt(RequestBody requestBody) {
        receiptUploadActivity.showProgressBarDialog("Upload Receipt...");
    }

    @Override
    public void uploadReceiptImageLuckyDraw(RequestBody requestBody) {
        receiptUploadActivity.showProgressBarDialog("Upload Receipt...");
        getManisApiV2().uploadReceiptLuckyDraw(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperLuckyDrawUpload>(receiptUploadActivity) {
                    @Override
                    protected void onSuccess(WrapperLuckyDrawUpload wrapperLuckyDrawUpload) {
                        super.onSuccess(wrapperLuckyDrawUpload);
                        receiptUploadActivity.dismissProgressBarDialog();
                        receiptUploadActivity.onSuccessUpload(true, wrapperLuckyDrawUpload.getLuckyDrawUpload());
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                        receiptUploadActivity.dismissProgressBarDialog();
                        receiptUploadActivity.showManisAlertDialog(message);
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        receiptUploadActivity.dismissProgressBarDialog();
                        receiptUploadActivity.showManisAlertDialog(message);
                    }
                });
        Log.d(ConfigManager.API_TAG, "uploadReceiptImageLuckyDraw: ".concat(new Gson().toJson(requestBody)));
    }

    @Override
    public Photo createPhoto(Bitmap bitmap) {
        Photo photo = new Photo();
        String fileEncode64 = ImageUtils.convertBitmapToBase64Code(bitmap).replace("\n", "").replace("\r", "");
        photo.setFile(fileEncode64);
        return photo;
    }

    @Override
    public RequestBody createSnapReceiptUploadRB(ReceiptDataBody receiptDataBody, Photo photo) {
        return new RequestBodyBuilder(receiptUploadActivity)
                .setFunction(ConfigManager.Snap.FUNCTION_UPLOAD_RECEIPT)
                .setData(receiptDataBody)
                .setPhoto(photo)
                .create();
    }

    @Override
    public RequestBody createLuckyDrawUploadReceiptRB(ReceiptDataBody receiptDataBody, Photo photo) {
        return new RequestBodyBuilder(receiptUploadActivity)
                .setFunction(ConfigManager.LuckyDraw.FUNCTION_UPLOAD_LUCKY_DRAW)
                .setData(receiptDataBody)
                .setPhoto(photo)
                .create();
    }
}