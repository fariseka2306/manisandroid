package com.ebizu.manis.mvp.snap.form;

import com.ebizu.manis.mvp.snap.SnapActivity;
import com.ebizu.manis.mvp.snap.receipt.camera.CameraActivity;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptDataBody;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptSnap;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;

/**
 * Created by ebizu on 8/25/17.
 */

public class FormReceiptActivity extends SnapActivity {

    protected void startActivityCamera(ReceiptStore receiptStore, double totalAmount) {
        nextActivity(CameraActivity.class, createReceiptData(receiptStore, totalAmount));
    }

    protected ReceiptDataBody createReceiptData(ReceiptStore receiptStore, double totalAmount) {
        ReceiptDataBody receiptDataBody = new ReceiptDataBody();
        receiptDataBody.setReceiptStore(receiptStore);
        receiptDataBody.setReceiptSnap(createReceiptAmount(totalAmount));
        return receiptDataBody;
    }

    protected ReceiptSnap createReceiptAmount(double totalAmount) {
        ReceiptSnap receiptSnap = new ReceiptSnap();
        receiptSnap.setAmount(totalAmount);
        receiptSnap.setNumber("0");
        return receiptSnap;
    }

    protected ReceiptStore createReceiptStore(String storeName, String storeLocation) {
        ReceiptStore receiptStore = new ReceiptStore();
        receiptStore.setName(storeName);
        receiptStore.setLocation(storeLocation);
        return receiptStore;
    }
}
