package com.ebizu.manis.mvp.luckydraw.luckydrawscreen;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.view.linearlayout.FixedRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerOriginPageLuckyDraw;

/**
 * Created by ebizu on 04/08/17.
 */

public class LuckyDrawFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.lucky_draw_view)
    LuckyDrawView luckyDrawView;
    @BindView(R.id.swipe_refresh)
    FixedRefreshLayout swipeRefresh;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_luckydraw, container, false);
        ButterKnife.bind(this, view);
        swipeRefresh.setOnRefreshListener(this);
        swipeRefresh.setColorSchemeResources(R.color.base_pink);
        luckyDrawView.setActivity((BaseActivity) getActivity());
        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible && getContext() != null) {
            luckyDrawView.setActivity((BaseActivity) getActivity());
            luckyDrawView.attachFragment(this);
            luckyDrawView.getLuckyDrawPresenter().loadLuckyDrawScreenData();
            getAnalyticManager().trackScreenView(ConfigManager.Analytic.ScreenView.FRAGMENT_LUCKY_DRAW);
            TrackerManager.getInstance().setTrackerData(
                    new TrackerStandartRequest(kTrackerOriginPageLuckyDraw, kTrackerOriginPageLuckyDraw, "", "", "", "")
            );
        }
    }

    @Override
    public void onRefresh() {
        luckyDrawView.getLuckyDrawPresenter().loadLuckyDrawScreenData();
        if (swipeRefresh.isRefreshing()) {
            swipeRefresh.setRefreshing(false);
        }
    }
}
