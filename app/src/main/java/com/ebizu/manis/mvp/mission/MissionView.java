package com.ebizu.manis.mvp.mission;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.model.missions.Missions;
import com.ebizu.manis.mvp.mission.shareexperience.ShareExperienceDetailActivity;
import com.ebizu.manis.mvp.mission.thematic.ThematicDetailActivity;
import com.ebizu.manis.mvp.store.storenearby.PaginationScrollListener;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperienceMissionsRB;
import com.ebizu.manis.service.manis.requestbodyv2.data.ShareExperienceMissionData;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperMissions;
import com.ebizu.manis.view.adapter.MissionsListAdapter;
import com.ebizu.manis.view.dialog.inviteterm.InviteTermDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class MissionView extends BaseView implements IMissionView, MissionsListAdapter.OnClickListener {

    private String TAG = getClass().getSimpleName();

    @BindView(R.id.recyclerview_shareexperience)
    RecyclerView recyclerViewShareExperience;
    @BindView(R.id.textview_empty_missions)
    TextView textViewEmptyMissions;
    @BindView(R.id.layout_empty_missions)
    RelativeLayout layoutEmpty;

    private IMissionPresenter iMissionPresenter;
    private MissionsListAdapter missionsListAdapter;
    private LinearLayoutManager linearLayoutManager;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    public boolean isRefresh = false;
    private int TOTAL_PAGES = 0;
    private int page = 1;

    public MissionView(Context context) {
        super(context);
        createView(context);
    }

    public MissionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public MissionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MissionView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_share_experience, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        initView();
        initListener();
    }

    private void initView() {
        missionsListAdapter = new MissionsListAdapter(new ArrayList<>(), getContext());
        missionsListAdapter.setMissionClick(this);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerViewShareExperience.setLayoutManager(linearLayoutManager);
        recyclerViewShareExperience.setAdapter(missionsListAdapter);
    }

    private void initListener() {
        recyclerViewShareExperience.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (!isRefresh) {
                    loadNextPage();
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    public MissionsListAdapter getAdapter() {
        return this.missionsListAdapter;
    }

    public void resetMissionParam() {
        this.page = 1;
        this.isLastPage = false;
    }

    public void inviteFriends() {
        if (!ConnectionDetector.isNetworkConnected(getContext())) {
            getBaseActivity().showAlertDialog(getContext().getString(R.string.error),
                    "Can't Load Content.\n" +
                            "Check Your internet connection",
                    false,
                    R.drawable.manis_logo,
                    "OK", (dialog, which) -> dialog.dismiss());
        } else {
            SharedPreferences prefFirst = getContext().getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
            if (UtilSessionFirstIn.isShowInviteTerms(prefFirst)) {
                InviteTermDialog inviteTermDialog = new InviteTermDialog(getContext());
                inviteTermDialog.setActivity(getBaseActivity());
                inviteTermDialog.show();
                inviteTermDialog.getInviteTermPresenter().loadInviteTerm(getContext());
            } else {
                InviteTermDialog inviteTermDialog = new InviteTermDialog(getContext());
                inviteTermDialog.setActivity(getBaseActivity());
                inviteTermDialog.getInviteTermPresenter().loadInviteTerm(getContext());
                inviteTermDialog.generateBranch();
            }
        }
    }

    private void loadNextPage() {
        isLoading = true;
        page++;
        ShareExperienceMissionData data = new ShareExperienceMissionData(getContext());
        ShareExperienceMissionsRB requestBody = new ShareExperienceMissionsRB();
        data.setCurrentPage(page);
        requestBody.setData(data);
        getSharedExperiencePresenter().loadShareExperienceNextPage(requestBody);
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iMissionPresenter = (IMissionPresenter) iBaseViewPresenter;
        iMissionPresenter.attachView(this);
    }

    @Override
    public void showShareExperience(WrapperMissions wrapperShareExperience) {
        Log.i(TAG, "showShareExperience: " + wrapperShareExperience.getExperienceList().size());
        TOTAL_PAGES = wrapperShareExperience.getMeta().getPagination().getTotalPage();
        recyclerViewShareExperience.setVisibility(VISIBLE);
        missionsListAdapter.replaceShareExperience(wrapperShareExperience.getExperienceList());
        if (wrapperShareExperience.getMeta().getPagination().getCurrentPage() < TOTAL_PAGES) {
            missionsListAdapter.addLoadingFooter();
        } else {
            isLastPage = true;
        }

        int lastPos = linearLayoutManager.findLastVisibleItemPosition();
        if (lastPos == missionsListAdapter.getItemCount() - 2 && page != TOTAL_PAGES) {
            recyclerViewShareExperience.scrollToPosition(lastPos + 1);
            loadNextPage();
        }
        isRefresh = false;
    }

    @Override
    public void onRetry() {
        super.onRetry();
        resetMissionParam();
        ShareExperienceMissionData data = new ShareExperienceMissionData(getContext());
        ShareExperienceMissionsRB requestBody = new ShareExperienceMissionsRB();
        data.setCurrentPage(1);
        requestBody.setData(data);
        getSharedExperiencePresenter().loadShareExperienceMission(requestBody);
    }

    @Override
    public void showShareExperienceNextPage(WrapperMissions wrapperMissions) {
        missionsListAdapter.removeLoadingFooter();
        isLoading = false;
        recyclerViewShareExperience.setVisibility(VISIBLE);
        missionsListAdapter.addAll(wrapperMissions.getExperienceList());
        if (wrapperMissions.getMeta().getPagination().getCurrentPage() != wrapperMissions.getMeta().getPagination().getTotalPage())
            missionsListAdapter.addLoadingFooter();
        else isLastPage = true;
    }

    @Override
    public IMissionPresenter getSharedExperiencePresenter() {
        return iMissionPresenter;
    }

    @Override
    public void setOnClickListener(View view, Missions missions) {
        if (missions.getMissionTypeId() == 3) {
            inviteFriends();
        } else {
            Intent intent = new Intent(getContext(), missions.getMissionTypeId() == 1 ? ThematicDetailActivity.class : ShareExperienceDetailActivity.class);
            intent.putExtra(ShareExperienceDetailActivity.EXTRA_SHARE_EXPERIENCE_DETAIL, missions);
            getBaseActivity().startActivityForResult(intent, ConfigManager.Missions.MISSION_DETAIL_CODE);
        }
    }
}
