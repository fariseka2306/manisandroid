package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import com.ebizu.manis.R;
import com.ebizu.manis.model.purchasevoucher.CustomerDetails;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;

public class RegularShoppingCartView extends ShoppingCartView {

    EditText editTextShippingInfo;

    public RegularShoppingCartView(Context context) {
        super(context);
    }

    public RegularShoppingCartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RegularShoppingCartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public RegularShoppingCartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
    }

    @Override
    public int bottomLayoutId() {
        return R.layout.view_regular_shopping_cart;
    }

    @Override
    public void initBottomViewChildren(View view) {
        editTextShippingInfo = (EditText) view.findViewById(R.id.text_view_shipping_info);
    }

    @Override
    public void setShoppingCartView(RewardVoucher reward) {
        super.setShoppingCartView(reward);
        textInputPhoneNumber.setDoneImeAction();
    }

    @Override
    protected CustomerDetails getCustomerDetails() {
        CustomerDetails customerDetails = super.getCustomerDetails();
        customerDetails.setShipmentInfo(editTextShippingInfo.getText() == null ? "" :
                editTextShippingInfo.getText().toString());
        return customerDetails;
    }
}
