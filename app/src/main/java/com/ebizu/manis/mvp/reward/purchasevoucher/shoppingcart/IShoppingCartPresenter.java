package com.ebizu.manis.mvp.reward.purchasevoucher.shoppingcart;

import com.ebizu.manis.root.IBaseViewPresenter;

/**
 * Created by ebizu on 9/27/17.
 */

public interface IShoppingCartPresenter extends IBaseViewPresenter {

    void cancelPayment(String orderId);

}
