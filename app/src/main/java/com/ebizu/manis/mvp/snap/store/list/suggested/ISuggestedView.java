package com.ebizu.manis.mvp.snap.store.list.suggested;

import com.ebizu.manis.model.snap.SnapStoreSuggest;
import com.ebizu.manis.mvp.snap.store.list.IListSnapStorePresenter;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public interface ISuggestedView extends IBaseView {

    void addStoreSuggest(SnapStoreSuggest snapStoreSuggest, LoadType loadType);

    void setOnSnapAbleListener(IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener);

}
