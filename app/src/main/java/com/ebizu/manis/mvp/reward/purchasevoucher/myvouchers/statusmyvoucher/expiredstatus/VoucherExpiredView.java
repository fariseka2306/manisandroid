package com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.statusmyvoucher.expiredstatus;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.EndlessRecyclerOnScrollListener;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.rewardvoucher.Data;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.MyVouchersActivity;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.sdk.entities.MyVoucher;
import com.ebizu.manis.service.reward.requestbody.RewardMyVoucherBody;
import com.ebizu.manis.service.reward.response.WrapperRewardVoucherList;
import com.ebizu.manis.view.adapter.VoucherExpiredAdapter;
import com.ebizu.manis.view.dialog.expired.ExpiredDialog;
import com.ebizu.manis.view.holder.VoucherExpiredViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 11/14/17.
 */

public class VoucherExpiredView extends BaseView implements IExpiredStatusView, VoucherExpiredViewHolder.ExpiredVoucherListener
                                                        , SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.recycler_view_voucher_expired)
    RecyclerView recyclerVoucherExpired;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayoutExpired;

    @BindView(R.id.view_no_internet_connection)
    View viewNoInternetConnection;

    @BindView(R.id.view_empty_my_voucher)
    View viewMyVouchersEmpty;

    @BindView(R.id.progress_bar_voucher)
    ProgressBar progressBarVoucherExpired;

    private MyVouchersActivity myVouchersActivity;
    private RewardMyVoucherBody.Data rewardMyVoucherData;
    private IExpiredStatusPresenter  iExpiredStatusPresenter;
    private RewardMyVoucherBody rewardMyVoucherBody;
    private VoucherExpiredAdapter voucherExpiredAdapter;
    private GridLayoutManager gridLayoutManager;
    private boolean isLoading = false;
    public boolean isLastPage = false;
    private boolean isLoadMore = false;
    public boolean isOffline = false;
    public int page = 1;

    public VoucherExpiredView(Context context) {
        super(context);
        createView(context);
    }

    public VoucherExpiredView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public VoucherExpiredView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public VoucherExpiredView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.view_my_voucher_expired, null,false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        super.setActivity(baseActivity);
        attachPresenter(new ExpiredStatusPresenter());
        myVouchersActivity = (MyVouchersActivity) baseActivity;
        initExpired();
        initListener();
        initMyVouchers();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iExpiredStatusPresenter = (IExpiredStatusPresenter) iBaseViewPresenter;
        iExpiredStatusPresenter.attachView(this);
    }

    public VoucherExpiredAdapter getMyVoucherAdapter() {
        return voucherExpiredAdapter;
    }

    private void initExpired(){

        voucherExpiredAdapter = new VoucherExpiredAdapter(new ArrayList<MyVoucher>(),this,this);

        gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerVoucherExpired.setLayoutManager(gridLayoutManager);
        recyclerVoucherExpired.setAdapter(voucherExpiredAdapter);

        recyclerVoucherExpired.addOnScrollListener(new EndlessRecyclerOnScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                page++;
                rewardMyVoucherBody.getData().setPage(page);
                iExpiredStatusPresenter.getExpiredVouchers(rewardMyVoucherBody,LoadType.SCROLL_LOAD,ConfigManager.Reward.MY_VOUCHER_CONDITION_EXPIRED);
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (voucherExpiredAdapter.getItemViewType(position)) {
                    case 0:
                        return 1;
                    case 1:
                        return 2;
                    default:
                        return -1;
                }
            }
        });

    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    @Override
    public void showVouchersExpirerdView(WrapperRewardVoucherList wrapperRewardVoucherList, LoadType loadType) {
        dismissEmptyPurchasedVouchersExpired();
        isLoadMore = !wrapperRewardVoucherList.getData().getMore();
        loadMyVouchersView(wrapperRewardVoucherList.getData(), loadType);

    }

    @Override
    public void loadMyVouchersViewFail(LoadType loadType) {
        switch (loadType) {
            case CLICK_LOAD: {
                checkConnection(loadType);
                break;
            }
            case SCROLL_LOAD: {
                checkConnection(loadType);
                break;
            }
            case SWIPE_LOAD: {
                checkConnection(loadType);
                break;
            }
        }
    }

    @Override
    public void invisibleListVoucherView() {
        recyclerVoucherExpired.setVisibility(INVISIBLE);
    }

    @Override
    public void showProgressBarCircle() {
        progressBarVoucherExpired.setVisibility(VISIBLE);
    }

    @Override
    public void dismissProgressBarCircle() {
        progressBarVoucherExpired.setVisibility(INVISIBLE);
    }

    private void dismissEmptyPurchasedVouchersExpired(){
        if (viewMyVouchersEmpty.isShown()) {
            recyclerVoucherExpired.setVisibility(View.GONE);
            viewMyVouchersEmpty.setVisibility(View.GONE);
        }
    }

    private void loadMyVouchersView(Data dataResult, LoadType loadType) {
        isLastPage = !dataResult.getMore();
        switch (loadType) {
            case CLICK_LOAD: {
                voucherExpiredAdapter.replacePurchasedVouchers(dataResult.getRewardVoucherList());
                break;
            }
            case SCROLL_LOAD: {
                voucherExpiredAdapter.addAll(dataResult.getRewardVoucherList());
                break;
            }
            case SWIPE_LOAD: {
                voucherExpiredAdapter.replacePurchasedVouchers(dataResult.getRewardVoucherList());
                break;
            }
        }
        progressBarVoucherExpired.setVisibility(GONE);
        viewNoInternetConnection.setVisibility(GONE);
        recyclerVoucherExpired.setVisibility(View.VISIBLE);
        if (voucherExpiredAdapter.isEmpty())
            showEmptyPurchasedVouchers();
    }

    private void checkConnection(LoadType loadType) {
        if (!UtilManis.isNetworkConnected(getContext()) && !isOffline) {
            switch (loadType) {
                case CLICK_LOAD: {
                    viewNoInternetConnection.setVisibility(View.VISIBLE);
                    viewMyVouchersEmpty.setVisibility(VISIBLE);
                    break;
                }
                case SCROLL_LOAD: {
                    Toast.makeText(getContext(), R.string.error_connection, Toast.LENGTH_SHORT).show();
                    break;
                }
                case SWIPE_LOAD: {
                    if (voucherExpiredAdapter.isEmpty()) {
                        viewNoInternetConnection.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(getContext(), R.string.error_connection, Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

            isOffline = true;
        } else {
            switch (loadType) {
                case CLICK_LOAD: {
                    viewMyVouchersEmpty.setVisibility(View.GONE);
                    break;
                }
                case SCROLL_LOAD: {
                    break;
                }
                case SWIPE_LOAD: {
                    if (voucherExpiredAdapter.isEmpty()) {
                        viewMyVouchersEmpty.setVisibility(View.VISIBLE);
                    } else {
                    }
                    break;
                }
            }
        }
    }

    private void initMyVouchers() {
        rewardMyVoucherData = new RewardMyVoucherBody.Data();
        rewardMyVoucherData.setOrder(ConfigManager.Reward.MY_VOUCHER_ORDER);
        rewardMyVoucherData.setSize(ConfigManager.Reward.MY_VOUCHER_MAX_PAGE_SIZE);
        rewardMyVoucherData.setPage(ConfigManager.Reward.MY_VOUCHER_FIRST_PAGE);
        rewardMyVoucherData.setCondtion(ConfigManager.Reward.MY_VOUCHER_CONDITION_EXPIRED);

        rewardMyVoucherBody = new RewardMyVoucherBody(getContext(), rewardMyVoucherData);
        iExpiredStatusPresenter.getExpiredVouchers(rewardMyVoucherBody, LoadType.CLICK_LOAD, ConfigManager.Reward.MY_VOUCHER_CONDITION_EXPIRED);
        swipeRefreshLayoutExpired.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.base_pink));

    }

    private void initListener() {
        swipeRefreshLayoutExpired.setOnRefreshListener(this);
    }

    @Override
    public void onExpiredVoucherListener(RewardVoucher rewardVoucher) {

        ExpiredDialog expiredDialog = new ExpiredDialog(getContext());

        expiredDialog.setExpiredVoucher(rewardVoucher);
        expiredDialog.show();
    }

    private void showEmptyPurchasedVouchers() {
        if (!viewMyVouchersEmpty.isShown()) {
            recyclerVoucherExpired.setVisibility(View.GONE);
            viewNoInternetConnection.setVisibility(GONE);
            viewMyVouchersEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        page = ConfigManager.Reward.MY_VOUCHER_FIRST_PAGE;
        rewardMyVoucherData.setPage(page);
        rewardMyVoucherBody = new RewardMyVoucherBody(getContext(), rewardMyVoucherData);
        iExpiredStatusPresenter.getExpiredVouchers(rewardMyVoucherBody, LoadType.SWIPE_LOAD, ConfigManager.Reward.MY_VOUCHER_CONDITION_EXPIRED);
        if (swipeRefreshLayoutExpired.isRefreshing()) {
            swipeRefreshLayoutExpired.setRefreshing(false);
        }
    }
}
