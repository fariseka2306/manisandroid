package com.ebizu.manis.mvp.snap.store.list.recent;

import com.ebizu.manis.model.snap.SnapStoreResult;
import com.ebizu.manis.mvp.snap.store.list.IListSnapStorePresenter;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public interface IRecentStoreView extends IBaseView {

    void addRecentStores(SnapStoreResult snapStoreResult, LoadType loadType);

    void setOnSnapAbleListener(IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener);

}
