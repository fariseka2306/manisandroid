package com.ebizu.manis.mvp.account.accountmenulist.profile.changeprofilephoto;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.AvatarEditor;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.snap.Photo;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by abizu-alvio on 7/27/2017.
 */

public class ChangeProfilePhotoView extends BaseView implements IChangeView {

    @BindView(R.id.imageview_photo_change)
    AvatarEditor imageViewPhoto;
    @BindView(R.id.button_gallery)
    Button buttonGallery;
    @BindView(R.id.button_camera)
    Button buttonCamera;
    @BindView(R.id.button_facebook)
    Button buttonFacebook;
    @BindView(R.id.button_cancel)
    Button buttonCancel;
    @BindView(R.id.button_save)
    Button buttonSave;

    private LoginManager loginManager;
    CallbackManager callbackManager;
    private Account account;
    private IChangePresenter iChangePresenter;
    private Context mContext;
    private ChangeProfilePhotoActivity changeProfilePhotoActivity;

    String mImagePath;
    Uri mImageUri;

    public ChangeProfilePhotoView(Context context) {
        super(context);
        createView(context);
    }

    public ChangeProfilePhotoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ChangeProfilePhotoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ChangeProfilePhotoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        this.iChangePresenter = (IChangePresenter) iBaseViewPresenter;
        iChangePresenter.attachView(this);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_change_profile_photo, null, false);
        account = new Account();
        callbackManager = CallbackManager.Factory.create();
        addView(view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void setProfilePhoto(Account account) {
        ImageUtils.loadImageChange(getContext(), account.getAccPhoto(), imageViewPhoto);
    }

    public void setProfilePhoto(Uri data) {
        ImageUtils.loadImageChange(getContext(), data, imageViewPhoto);
    }

    @Override
    public IChangePresenter getChangePresenter() {
        return iChangePresenter;
    }

    @OnClick(R.id.button_gallery)
    void clickGallery() {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.putExtra("sender", "/profil_picture");
            getBaseActivity().startActivityForResult(intent, ConfigManager.UpdateProfile.GALLERY);
            new AnalyticManager(getContext()).trackEvent(
                    ConfigManager.Analytic.Category.DIALOG_CHANGE_PROFILEACTIVITY_PICTURE,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Gallery"
            );
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
            Log.e(ChangeProfilePhotoView.this.getClass().getName(), ex.getMessage());
        }
    }

    @OnClick(R.id.button_camera)
    void clickCamera() {
        try {
            mImageUri = FileProvider.getUriForFile(getContext(), "com.ebizu.manis.provider", createImageFile());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
            List<ResolveInfo> resInfoList = getContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                getContext().grantUriPermission(packageName, mImageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            getBaseActivity().startActivityForResult(intent, ConfigManager.UpdateProfile.CAMERA);
            new AnalyticManager(getContext()).trackEvent(
                    ConfigManager.Analytic.Category.DIALOG_CHANGE_PROFILEACTIVITY_PICTURE,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Camera"
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File createImageFile() throws IOException {
        final String dir = Environment.getExternalStorageDirectory() + "/Manis";
        File newdir = new File(dir);
        if (!newdir.exists()) newdir.mkdirs();
        String imageFileName = newdir + "/ProfilePicture.jpg";
        File image = new File(imageFileName);
        if (!image.exists()) image.createNewFile();
        mImagePath = "file:" + image.getAbsolutePath();
        return image;
    }

    @OnClick(R.id.button_facebook)
    void clickFacebook() {
        final ProgressDialog progressDialog = new ProgressDialog(getBaseActivity());
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setMessage(getBaseActivity().getString(R.string.txt_loading));
        progressDialog.show();

        FacebookSdk.sdkInitialize(getContext());
        new AnalyticManager(getContext()).trackEvent(
                ConfigManager.Analytic.Category.DIALOG_CHANGE_PROFILEACTIVITY_PICTURE,
                ConfigManager.Analytic.Action.CLICK,
                "Button Facebook"
        );
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(final LoginResult loginResult) {
                if (progressDialog.isShowing()) progressDialog.dismiss();
                String facebookPhoto = "https://graph.facebook.com/me/picture?width=600&height=600&access_token=" + loginResult.getAccessToken().getToken();
                ImageUtils.loadImageChange(getContext(), facebookPhoto, imageViewPhoto);
            }

            @Override
            public void onCancel() {
                if (progressDialog.isShowing()) progressDialog.dismiss();
            }

            @Override
            public void onError(FacebookException error) {
                if (progressDialog.isShowing()) progressDialog.dismiss();
            }
        });
        try {
            LoginManager.getInstance().logOut();
        } catch (Exception e) {
            Log.e(getBaseActivity().getString(R.string.app_name), e.getMessage(), e);

        }
        LoginManager.getInstance().logInWithReadPermissions((Activity) getContext(), Arrays.asList("email", "user_birthday", "user_location"));
    }

    @OnClick(R.id.button_cancel)
    void clickCancel() {
        new AnalyticManager(getContext()).trackEvent(
                ConfigManager.Analytic.Category.DIALOG_CHANGE_PROFILEACTIVITY_PICTURE,
                ConfigManager.Analytic.Action.CLICK,
                "Button Cancel"
        );
        getBaseActivity().finish();
    }

    @TargetApi(Build.VERSION_CODES.N)
    @OnClick(R.id.button_save)
    void clickSave() {
        try {
            imageViewPhoto.setImageDrawable(new BitmapDrawable(getResources(), imageViewPhoto.getBitmap()));
            new AnalyticManager(getContext()).trackEvent(
                    ConfigManager.Analytic.Category.DIALOG_CHANGE_PROFILEACTIVITY_PICTURE,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Save"
            );
        } catch (Exception e) {
            e.printStackTrace();
            UtilManis.info(getContext(), getBaseActivity().getString(R.string.profile_picture_title), getBaseActivity().getString(R.string.profile_picture_not_cropable));
            imageViewPhoto.resetMatrix();
            return;
        }
        Photo photo = new Photo();
        photo.setFile(ImageUtils.convertBitmapToBase64Code(imageViewPhoto.getBitmap()));
        RequestBody requestBody = new RequestBodyBuilder(getContext())
                .setFunction(ConfigManager.Account.FUNCTION_UPDATE_PROFILE_PICT)
                .setPhoto(photo)
                .create();
        getChangePresenter().savePhotoProfile(requestBody);
    }

    @Override
    public void successUploadPhoto() {
        getBaseActivity().setResult(ConfigManager.UpdateProfile.SUCCESS_UPDATE_PICTURE_RES_CODE);
        getBaseActivity().finish();
    }

}