package com.ebizu.manis.mvp.notification.item;

import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.view.holder.NotificationSwipeViewHolder;

/**
 * Created by ebizu on 8/28/17.
 */

public class NotificationView {

    private NotificationItem[] getNotificationItems(NotificationTableList notificationTableList, NotificationSwipeViewHolder notificationSwipeViewHolder) {
        return new NotificationItem[]{
                new NotificationBeaconPromo(notificationTableList, notificationSwipeViewHolder),
                new NotificationSnap(notificationTableList, notificationSwipeViewHolder)
        };
    }

    public void setNotificationView(NotificationTableList notificationTableList, NotificationSwipeViewHolder notificationSwipeViewHolder) {
        for (NotificationInterface notificationItem : getNotificationItems(notificationTableList, notificationSwipeViewHolder)) {
            if (notificationItem.notificationType().equals(notificationTableList.getType())) {
                notificationItem.setNotificationView();
            }
        }
    }



}
