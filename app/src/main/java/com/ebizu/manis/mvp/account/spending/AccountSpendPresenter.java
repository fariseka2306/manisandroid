package com.ebizu.manis.mvp.account.spending;

import com.ebizu.manis.model.Statistic;
import com.ebizu.manis.model.StatisticBody;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.response.WrapperDataStatistic;
import com.google.gson.Gson;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by abizu-alvio on 7/14/2017.
 */

public class AccountSpendPresenter extends BaseViewPresenter implements IAccountSpendPresenter {

    private final String TAG = getClass().getSimpleName();

    private BaseActivity baseActivity;
    private AccountSpendView accountSpendView;
    private Subscription subsSpend;

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        accountSpendView = (AccountSpendView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subsSpend != null) subsSpend.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadAccountSpend(ManisApi manisApi, StatisticBody statisticBody) {
        releaseSubscribe(0);
        accountSpendView.startAnimationShimmer();
        subsSpend = manisApi.getStatistic(new Gson().toJson(statisticBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperDataStatistic>(accountSpendView) {
                    @Override
                    public void onNext(WrapperDataStatistic wrapperDataStatistic) {
                        super.onNext(wrapperDataStatistic);
                        List<Statistic> statistics = wrapperDataStatistic.getWrapperStatistic().getData().getStatisticList();
                        accountSpendView.stopAnimationShimmer();
                        accountSpendView.setSpendingBarView(statistics);
                        accountSpendView.setSpendingBarViewTextView(statistics);
                        accountSpendView.showViewAccountSpend(statistics);
                        accountSpendView.showViewAccountTotalSpend(wrapperDataStatistic.getWrapperStatistic().getData());
                        accountSpendView.showCurrenctyAccount();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        accountSpendView.stopAnimationShimmer();
                    }
                });
    }
}
