package com.ebizu.manis.mvp.store.storenearby;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.mvp.store.storecategory.StoreCategoryFragment;
import com.ebizu.manis.root.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerOriginPageStores;

/**
 * Created by Halim on 7/5/17.
 */

public class StoreFragment extends BaseFragment {

    private boolean stateFragment;
    private StoreNearbyFragment storeNearbyFragment;
    private StoreCategoryFragment storeCategoryFragment;

    @BindView(R.id.fs_rel_nearby)
    RelativeLayout relNearby;

    @BindView(R.id.fs_rel_categories)
    RelativeLayout relCategories;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_store, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initView();
    }

    private void initView() {
        relNearby.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPink));
        relCategories.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.circle_border));
        storeNearbyFragment = new StoreNearbyFragment();
        storeCategoryFragment = new StoreCategoryFragment();
        stateFragment = false;
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.fs_frame_content, storeNearbyFragment)
                .commitAllowingStateLoss();
    }

    @OnClick(R.id.fs_rel_nearby)
    public void onRelNearbyClick() {
        if (stateFragment) {
            stateFragment = false;
            relNearby.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPink));
            relCategories.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.circle_border));
            storeNearbyFragment = new StoreNearbyFragment();
            switchContent(storeNearbyFragment);
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.FRAGMENT_STORES,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Nearby"
            );
        }
    }

    @OnClick(R.id.fs_rel_categories)
    public void onRelCategoryClick() {
        if (!stateFragment) {
            stateFragment = true;
            relNearby.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.circle_border));
            relCategories.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPink));
            storeCategoryFragment = new StoreCategoryFragment();
            switchContent(storeCategoryFragment);
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.FRAGMENT_STORES,
                    ConfigManager.Analytic.Action.CLICK,
                    "Button Categories"
            );
        }
    }

    private void switchContent(final Fragment fragment) {
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.fs_frame_content, fragment)
                .commit();
    }

    public StoreNearbyFragment getStoreNearbyFragment() {
        return storeNearbyFragment;
    }

    public StoreCategoryFragment getStoreCategoryFragment() {
        return storeCategoryFragment;
    }

    public void hideNavigationButton() {
        relNearby.setVisibility(View.GONE);
        relCategories.setVisibility(View.GONE);
    }

    public void showNavigationButton() {
        relNearby.setVisibility(View.VISIBLE);
        relCategories.setVisibility(View.VISIBLE);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            TrackerManager.getInstance().setTrackerData(
                    new TrackerStandartRequest(kTrackerOriginPageStores, kTrackerOriginPageStores, "", "", "", "")
            );
        }
    }
}
