package com.ebizu.manis.mvp.reward.rewardlistcategory;

import com.ebizu.manis.model.rewardvoucher.Brand;
import com.ebizu.manis.model.rewardvoucher.Category;
import com.ebizu.manis.model.rewardvoucher.Input;
import com.ebizu.manis.model.rewardvoucher.Provider;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.model.rewardvoucher.Term;
import com.ebizu.sdk.reward.models.Reward;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARIS_mac on 17/11/17.
 */

public class RewardConverterAdapter {

    public static RewardVoucher convertToRewardVoucer(Reward reward) {
        RewardVoucher result = new RewardVoucher();

        List<Term> terms = new ArrayList<>();
        for (com.ebizu.sdk.reward.models.Term rewardTerm : reward.getTerms()) {
            Term term = new Term();
            term.setId(rewardTerm.getId());
            term.setText(rewardTerm.getText());
            terms.add(term);
        }

        List<Input> inputs = new ArrayList<>();
        for (com.ebizu.sdk.reward.models.Input rewardInput : reward.getInputs()) {
            Input input = new Input();
            input.setFilledByApplication(rewardInput.getFilledByApplication());
            input.setId(rewardInput.getId());
            input.setText(rewardInput.getText());
            input.setType(rewardInput.getType());
            inputs.add(input);
        }

        List<Category> categories = new ArrayList<>();
        for (com.ebizu.sdk.reward.models.Category category : reward.getProvider().getCategory()) {
            Category categorys = new Category();
            categorys.setId(category.getId());
            categorys.setName(category.getName());
            categories.add(categorys);
        }

        Brand brand = new Brand();
        brand.setId(reward.getBrand().getId());
        brand.setName(reward.getBrand().getName());

        Provider provider = new Provider();
        provider.setAddress(reward.getProvider().getAddress());
        provider.setId(reward.getProvider().getId());
        provider.setImage(reward.getProvider().getImage());
        provider.setLat(reward.getProvider().getLat());
        provider.setLon(reward.getProvider().getLon());
        provider.setName(reward.getProvider().getName());
        provider.setPhone(reward.getProvider().getPhone());
        provider.setWebsite(reward.getProvider().getWebsite());
        provider.setCategory(categories);

        result.setCurrency(reward.getCurrency());
        result.setDescription(reward.getDescription());
        result.setExpired(reward.getExpired());
        result.setId(reward.getId());
        result.setType(reward.getType());
        result.setImage(reward.getSmallImage());
        result.setImage64(reward.getMediumImage());
        result.setImage128(reward.getLargeImage());
        result.setName(reward.getName());
        result.setPoint(reward.getPoint());
        result.setValue(reward.getValue());
        result.setStock(reward.getStock());
        result.setVoucherRedeemType(reward.getVoucherRedeemType());
        result.setRedeemLabel(reward.getRedeemLabel());
        result.setVoucherTransactionType(reward.getVoucherTransactionType());
        result.setVoucherPurchaseValue(reward.getVoucherPurchaseValue());
        result.setVoucherCostValue(reward.getVoucherCostValue());
        result.setProvider(provider);
        result.setBrand(brand);
        result.setTerms(terms);
        result.setInputs(inputs);
        result.setRedeemInstruction(reward.getRedeemInstruction());
        result.setTrackLink(reward.getTrackLink());
        return result;
    }
}
