package com.ebizu.manis.mvp.reward.purchasevoucher.validation;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;

/**
 * Created by ebizu on 10/4/17.
 */

public class TransactionSuccess extends TransactionStatus
        implements ITransactionStatus {

    public TransactionSuccess(Context context) {
        super(context);
    }

    @Override
    public String statusMolPay() {
        return "00";
    }

    @Override
    public String statusMidTrans() {
        return TransactionResult.STATUS_SUCCESS;
    }

    @Override
    public String statusPayment() {
        return "paid";
    }

    @Override
    public String statusTransaction() {
        return context.getString(R.string.payment_success_status);
    }

    @Override
    public Drawable iconStatus() {
        return ContextCompat.getDrawable(context, R.drawable.ic_payment_success);
    }
}
