package com.ebizu.manis.mvp.account.accountmenulist.saved;

import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by mac on 8/21/17.
 */

public interface ISavedPresenter extends IBaseViewPresenter{

    void loadSaved(ManisApi manisApi);

}
