package com.ebizu.manis.mvp.snap.receipt.camera;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.view.camera.CameraPreview;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ebizu-User on 10/08/2017.
 */

public class CameraActivity extends ReceiptActivity implements SensorEventListener {

    private static final int SHAKE_THRESHOLD = 600;

    @Inject
    Context context;
    @Inject
    CameraActivityPresenter cameraActivityPresenter;

    @BindView(R.id.camera_preview)
    CameraPreview cameraPreview;
    @BindView(R.id.imageView_recent)
    ImageView imageViewRecent;
    @BindView(R.id.vDashedLine)
    View viewDashed;
    @BindView(R.id.layout_snap_controller)
    RelativeLayout snapController;
    @BindView(R.id.textView_focus)
    View textViewFocus;
    @BindView(R.id.textView_topReceipt)
    TextView textViewTopReceipt;
    @BindView(R.id.textView_counter)
    TextView textViewCounter;
    @BindView(R.id.textView_camera_desc)
    TextView textViewDesc;
    @BindView(R.id.view_recent)
    RelativeLayout layoutRecent;
    @BindView(R.id.rootView_camera)
    RelativeLayout layoutRoot;
    @BindView(R.id.rotationAlertHolder)
    AlertCameraHolder alertCameraHolder;
    @BindView(R.id.snap_button)
    Button snapButton;
    ArrayList<Bitmap> bitmapsList = new ArrayList<>();
    Uri uri;
    int croppedHeight;
    private boolean hasTakePicture = false;
    private long lastUpdate = 0;
    private float lastX;
    private float lastY;
    private float lastZ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snap_receipt);
        cameraActivityPresenter.attachView(this);
        attachPresenter(cameraActivityPresenter);
        ButterKnife.bind(this);
        getExtrasData();
        cameraPreview.setBaseActivity(this);
        cameraPreview.setOnPreview((data, camera) -> {
            // not implemented
        });
        cameraPreview.setPictureCallback((data, camera) ->
                cameraActivityPresenter.processData(data));
        accelerometer();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent())
                .build();
        activityComponent.inject(this);
    }

    private void accelerometer() {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    private void getExtrasData() {
        receiptDataBody = getIntent().getExtras().getParcelable(ConfigManager.Snap.RECEIPT_DATA_PARCEL);
    }

    @OnClick(R.id.snap_button)
    void snapReceipt() {
        if (!hasTakePicture && cameraPreview.isReady()) {
            cameraPreview.takePicture();
            hasTakePicture = true;
        }
    }

    @OnClick(R.id.textView_cancel)
    void snapCancel() {
        if (bitmapsList.isEmpty()) {
            onBackPressed();
        } else {
            cameraActivityPresenter.stitchImage();
            nextActivity(ConfirmActivity.class, bitmapId, receiptDataBody, bitmapsList.size());
        }
    }

    @OnClick(R.id.imageView_help)
    void snapHelp() {
        // not implemented yet
    }

    @OnClick(R.id.textView_focus)
    void snapFocus() {
        hideFocusTextView();
    }

    @Override
    protected void onResume() {
        if (!isFinishing()) {
            cameraPreview.resume();
            cameraPreview.setVisibility(View.VISIBLE);
        }
        hasTakePicture = false;
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (!isFinishing()) {
            cameraPreview.pause();
            if (!hasTakePicture)
                cameraPreview.setVisibility(View.GONE);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (!isFinishing())
            cameraPreview.stop();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        slideOutHorizontalAnim();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (uri != null) ImageUtils.deleteRecentFile(uri);
        if (requestCode == ConfigManager.Snap.RECEIPT_REQUEST_CODE)
            if (resultCode == ConfigManager.Snap.LONG_RECEIPT_RETAKE_RESULT_CODE) {
                int lastPos = bitmapsList.size() - 1;
                bitmapsList.remove(lastPos);
                if (!bitmapsList.isEmpty())
                    setRecentBitmap(bitmapsList.get(bitmapsList.size() - 1));
                else
                    hideRecentLayout();
            } else if (resultCode == ConfigManager.Snap.LONG_RECEIPT_ADD_RESULT_CODE) {
                setRecentBitmap(bitmapsList.get(bitmapsList.size() - 1));
            } else if (resultCode == ConfigManager.Snap.RECEIPT_RETAKE_RESULT_CODE) {
                bitmapsList.clear();
                hideRecentLayout();
            }
        hideFocusTextView();
    }

    private void hideRecentLayout() {
        textViewDesc.setText(getString(R.string.stitch_description));
        textViewTopReceipt.setVisibility(View.VISIBLE);
        viewDashed.setVisibility(View.GONE);
        layoutRecent.setVisibility(View.GONE);
    }

    private void hideFocusTextView() {
        if (textViewFocus.getVisibility() == View.VISIBLE)
            textViewFocus.setVisibility(View.GONE);
    }

    private void showRecentLayout() {
        textViewDesc.setText(getString(R.string.stitch_next_description));
        textViewCounter.setText(String.valueOf(bitmapsList.size()));
        textViewTopReceipt.setVisibility(View.GONE);
        viewDashed.setVisibility(View.VISIBLE);
        layoutRecent.setVisibility(View.VISIBLE);
    }

    private void setRecentBitmap(Bitmap bitmap) {
        Bitmap croppedBitmap = Bitmap.createBitmap(bitmap, 0, bitmap.getHeight() * 75 / 100,
                bitmap.getWidth(), bitmap.getHeight() * 25 / 100);
        croppedHeight = croppedBitmap.getHeight();

        layoutRecent.getViewTreeObserver().addOnGlobalLayoutListener(
                () -> setNewLayoutParam(croppedBitmap));
        imageViewRecent.setImageBitmap(croppedBitmap);
        showRecentLayout();
    }

    private void setNewLayoutParam(Bitmap croppedBitmap) {
        int intendedWidth = layoutRecent.getWidth();
        int originalWidth = croppedBitmap.getWidth();
        int originalHeight = croppedBitmap.getHeight();

        float scale = (float) intendedWidth / originalWidth;
        int newHeight = Math.round(originalHeight * scale);

        RelativeLayout.LayoutParams rootParam = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutRecent.getLayoutParams().width = intendedWidth;
        layoutRecent.getLayoutParams().height = newHeight;
        int marginHorizontal = getResources().getDimensionPixelSize(R.dimen.twenty_dp);
        rootParam.setMargins(marginHorizontal, 0, marginHorizontal, 0);
        layoutRecent.setLayoutParams(rootParam);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        imageViewRecent.setLayoutParams(layoutParams);
        imageViewRecent.getLayoutParams().width = intendedWidth;
        imageViewRecent.getLayoutParams().height = newHeight;
        imageViewRecent.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ConfigManager.Permission.CAMERA_REQUEST_CODE)
            if (getPermissionManager().checkPermissionCamera(false)) {
                cameraPreview.initialize();
                onPause();
                onResume();
            } else {
                finish();
            }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mSensor = sensorEvent.sensor;
        if (mSensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();
            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;
                float speed = Math.abs(x + y + z - lastX - lastY - lastZ) / diffTime * 10000;
                if (speed > SHAKE_THRESHOLD) {
                    // checking if phone shaked
                }
                lastX = x;
                lastY = y;
                lastZ = z;
            }
        }
        setAlertCameraHolder();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // not implemented method
    }

    private void setAlertCameraHolder() {
        try {
            if (lastX < 2.5 && lastX > -2.5 && lastY < 2.5 && lastY > -2.5 && lastZ < 15) {
                alertCameraHolder.setVisibility(View.GONE);
                snapButton.setEnabled(true);
                textViewFocus.setEnabled(true);
            } else {
                if (hasTakePicture) {
                    alertCameraHolder.setVisibility(View.GONE);
                    snapButton.setEnabled(true);
                } else {
                    alertCameraHolder.setVisibility(View.VISIBLE);
                    snapButton.setEnabled(false);
                    textViewFocus.setEnabled(false);
                }
            }
        } catch (NullPointerException ignored) {
            Log.e(CameraActivity.class.getSimpleName(), ignored.getMessage());
        }
    }
}
