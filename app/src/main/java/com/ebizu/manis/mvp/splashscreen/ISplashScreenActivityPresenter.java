package com.ebizu.manis.mvp.splashscreen;

import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.IBaseActivityPresenter;

/**
 * Created by Ebizu-User on 06/07/2017.
 */

public interface ISplashScreenActivityPresenter<View> extends IBaseActivityPresenter {
    void checkDeviceEmulator();

    void checkDeviceRooted();

    void checkExManisApp();

    boolean checkAllRequire();
}