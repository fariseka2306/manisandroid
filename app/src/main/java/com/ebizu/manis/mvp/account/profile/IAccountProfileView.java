package com.ebizu.manis.mvp.account.profile;

import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.Point;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by abizu-alvio on 7/14/2017.
 */

public interface IAccountProfileView extends IBaseView {
    void setAccountProfile(Account account);

    void setPointProfile(Point point);

    IAccountProfilePresenter getProfilePresenter();

    void setOnClickSignOutSccess(OnClickListener onClickSignOutSccess);

    interface OnClickListener {
        void onClickSignOut();
    }
}
