package com.ebizu.manis.mvp.reward.newreward;

import com.ebizu.manis.root.IBaseView;

import java.util.List;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public interface INewRewardView extends IBaseView {

    void loadImageCategory(List<Integer> imageCategories, List<String> categoriesName, List<String> categoriesId);

    INewRewardPresenter getNewRewardPresenter();
}
