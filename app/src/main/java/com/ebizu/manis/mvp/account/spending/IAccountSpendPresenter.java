package com.ebizu.manis.mvp.account.spending;

import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.StatisticBody;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.ManisApi;

/**
 * Created by abizu-alvio on 7/14/2017.
 */

public interface IAccountSpendPresenter extends IBaseViewPresenter {

    void loadAccountSpend(ManisApi manisApi, StatisticBody statisticBody);

}
