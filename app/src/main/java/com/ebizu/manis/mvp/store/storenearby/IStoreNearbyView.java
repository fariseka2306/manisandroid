package com.ebizu.manis.mvp.store.storenearby;

import com.ebizu.manis.model.StoreResults;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by ebizu on 9/11/17.
 */

public interface IStoreNearbyView {

    void addViewStores(StoreResults storeResults, IBaseView.LoadType loadType);

    void failLoadViewStores(IBaseView.LoadType loadType, Throwable e);

    void invisibleListStoreView();

    void visibleListStoreView();

    void showProgressBarCircle();

    void dismissProgressBarCircle();

}