package com.ebizu.manis.mvp.mission.thematic;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.manager.tracker.TrackerConstant;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.mvp.mission.MissionDetailActivity;
import com.ebizu.manis.view.dialog.missionsdialog.ThematicMissionsDialog;
import com.ebizu.manis.view.dialog.missionsdialog.ThematicPresenter;

import javax.inject.Inject;

/**
 * Created by FARIS_mac on 10/17/17.
 */

public class ThematicDetailActivity extends MissionDetailActivity {

    private String TAG = getClass().getSimpleName();

    @Inject
    Context context;
    @Inject
    ThematicPresenter thematicPresenter;

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .applicationComponent(getApplicationComponent())
                .build();
        activityComponent.inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setButtonText(getString(R.string.button_thematic));
    }

    @Override
    public void executeMission() {
        super.executeMission();
        ThematicMissionsDialog thematicMissionsDialog = new ThematicMissionsDialog(this);
        thematicMissionsDialog.setActivity(this);
        thematicMissionsDialog.setMissions(mission);
        thematicMissionsDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        thematicMissionsDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        thematicMissionsDialog.show();
    }

    @Override
    public void finish() {
        super.finish();
        TrackerManager.getInstance().setTrackerData(
                new TrackerStandartRequest(TrackerConstant.kTrackerOriginPageMissions, TrackerConstant.kTrackerOriginPageMissions, "", "", "", "")
        );
    }
}
