package com.ebizu.manis.mvp.mission.shareexperience;

import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperiencePostRB;

public interface IShareExperienceExecute {
    void executeShareExp(ShareExperienceDetailActivity shareExperienceDetailActivity, ShareExperiencePostRB shareExperiencePostRB);
}
