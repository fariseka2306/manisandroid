package com.ebizu.manis.mvp.viewhistory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.filterhistory.SnapHistoryManager;
import com.ebizu.manis.manager.orderhistory.OrderHistoryManager;
import com.ebizu.manis.model.ViewHistoryParam;
import com.ebizu.manis.model.ViewHistoryResult;
import com.ebizu.manis.model.snap.SnapData;
import com.ebizu.manis.mvp.snapdetail.SnapDetailsActivity;
import com.ebizu.manis.mvp.store.storenearby.PaginationScrollListener;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.view.adapter.ViewHistoryAdapter;
import com.ebizu.manis.view.linearlayout.FixedLinearLayoutManager;
import com.ebizu.manis.view.manis.notification.NotificationMessageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 7/24/17.
 */

public class SnapViewHistoryActivity extends BaseActivity
        implements IHistoryView, SwipeRefreshLayout.OnRefreshListener, ViewHistoryAdapter.OnClickListener {

    private Context context;
    private ViewHistoryPresenter viewHistoryPresenter;
    private int page = 1;
    private int filterSpinnerPosition = 0;
    private int orderSpinnerPosition = 0;
    public ViewHistoryAdapter viewHistoryAdapter;
    private LinearLayoutManager layoutManager;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    private boolean isFirstOrderSpinnerListener = true;
    private boolean isFirstFilterSpinnerListener = true;
    private ViewHistoryParam viewHistoryParam;

    @BindView(R.id.view_history_list)
    RecyclerView recyclerViewHistory;
    @BindView(R.id.spin_filter)
    Spinner spinFilter;
    @BindView(R.id.sh_spin_sort)
    Spinner spinOrder;
    @BindView(R.id.layout_empty)
    LinearLayout layoutEmpty;
    @BindView(R.id.sh_toolbar)
    Toolbar shToolbar;
    @BindView(R.id.layout_empty_img)
    ImageView layoutEmptyImg;
    @BindView(R.id.layout_empty_txt)
    TextView layoutEmptyText;
    @BindView(R.id.sh_swiper)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.notification_view)
    NotificationMessageView notificationMessageView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideInHorizontalAnim();
        setContentView(R.layout.activity_snap_view_history);
        context = this;
        ButterKnife.bind(this);

        viewHistoryPresenter = new ViewHistoryPresenter(context);

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        viewHistoryParam = new ViewHistoryParam();
        viewHistoryParam.setFilter(ConfigManager.SnapViewHistory.STATUS_ALL);
        viewHistoryParam.setOrder(ConfigManager.SnapViewHistory.ORDER_DATE);
        viewHistoryParam.setSort(ConfigManager.SnapViewHistory.SORT_DESC);
        viewHistoryParam.setPage(ConfigManager.SnapViewHistory.PAGE);
        viewHistoryParam.setSize(ConfigManager.SnapViewHistory.SIZE);
        viewHistoryPresenter.attachViewHistory(this, this);
        viewHistoryPresenter.loadViewHistory(viewHistoryParam, IBaseView.LoadType.CLICK_LOAD);
        showBaseProgressBar();

        initialize();
        initFilter();
    }

    private void initialize() {

        shToolbar.setContentInsetsAbsolute(0, 0);
        setSupportActionBar(shToolbar);
        ActionBar actionBar = getSupportActionBar();

        View view = getLayoutInflater().inflate(R.layout.actionbar_back, null);
        TextView txtTitle = (TextView) view.findViewById(R.id.txt_title_action);
        txtTitle.setText(getString(R.string.sh_title));
        RelativeLayout relBack = (RelativeLayout) view.findViewById(R.id.rel_left);
        relBack.setOnClickListener(v -> finish());

        actionBar.setCustomView(view, new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.WRAP_CONTENT,
                        ActionBar.LayoutParams.MATCH_PARENT,
                        Gravity.CENTER
                )
        );
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        viewHistoryAdapter = new ViewHistoryAdapter(context, this);
        swipeRefresh.setOnRefreshListener(this);
        FixedLinearLayoutManager fixedLinearLayoutManager = new FixedLinearLayoutManager(context);
        fixedLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewHistory.setLayoutManager(layoutManager);
        recyclerViewHistory.setAdapter(viewHistoryAdapter);
        recyclerViewHistory.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                viewHistoryParam.setPage(page);
                viewHistoryPresenter.loadViewHistory(viewHistoryParam, IBaseView.LoadType.SCROLL_LOAD);
            }

            @Override
            public int getTotalPageCount() {
                return viewHistoryAdapter.getItemCount();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void initFilter() {
        spinFilter.setAdapter(SnapHistoryManager.getFilterSpinnerAdapter(context));
        spinFilter.setPrompt(getString(R.string.sh_txt_filter));
        spinFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onClickFilterSpinner(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing because nothing selected.
            }
        });

        spinOrder.setAdapter(OrderHistoryManager.getOrderSpinnerAdapter(context));
        spinOrder.setPrompt(getString(R.string.sh_txt_sort_by));
        spinOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onClickOrderSpinner(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing because nothing selected.
            }
        });
    }

    @Override
    public void onClickFilterSpinner(int position) {
        if (!isFirstFilterSpinnerListener) {
            filterSpinnerPosition = position;
            setPage(1);
            viewHistoryAdapter.deleteAll();
            viewHistoryParam = new ViewHistoryParam();
            viewHistoryParam.setFilter(SnapHistoryManager.getFilterValueInt(filterSpinnerPosition));
            viewHistoryParam.setSort(ConfigManager.SnapViewHistory.SORT_DESC);
            viewHistoryParam.setOrder(OrderHistoryManager.getOrderValueInt(orderSpinnerPosition));
            viewHistoryParam.setPage(page);
            viewHistoryParam.setSize(ConfigManager.SnapViewHistory.SIZE);
            viewHistoryPresenter.loadViewHistory(viewHistoryParam, IBaseView.LoadType.CLICK_LOAD);
            showBaseProgressBar();
        } else {
            isFirstFilterSpinnerListener = false;
        }
    }

    @Override
    public void onClickOrderSpinner(int position) {
        if (!isFirstOrderSpinnerListener) {
            orderSpinnerPosition = position;
            setPage(1);
            viewHistoryAdapter.deleteAll();
            viewHistoryParam = new ViewHistoryParam();
            viewHistoryParam.setFilter(SnapHistoryManager.getFilterValueInt(filterSpinnerPosition));
            viewHistoryParam.setSort(ConfigManager.SnapViewHistory.SORT_DESC);
            viewHistoryParam.setOrder(OrderHistoryManager.getOrderValueInt(orderSpinnerPosition));
            viewHistoryParam.setPage(page);
            viewHistoryParam.setSize(ConfigManager.SnapViewHistory.SIZE);
            viewHistoryPresenter.loadViewHistory(viewHistoryParam, IBaseView.LoadType.CLICK_LOAD);
            showBaseProgressBar();
            getAnalyticManager().trackEvent(
                    ConfigManager.Analytic.Category.SNAP_HISTORY,
                    ConfigManager.Analytic.Action.ITEM_CLICK,
                    "Filter ".concat(String.valueOf(orderSpinnerPosition))
            );
        } else {
            isFirstOrderSpinnerListener = false;
        }
    }

    @Override
    public void setViewHistory(ViewHistoryResult viewHistoryResult, IBaseView.LoadType loadType) {
        isLastPage = !viewHistoryResult.getMore();
        ArrayList<SnapData> snapDataHistories = viewHistoryResult.getSnapDatas();
        switch (loadType) {
            case CLICK_LOAD: {
                this.viewHistoryAdapter.replaceViewHistory(snapDataHistories);
                break;
            }
            case SWIPE_LOAD: {
                this.viewHistoryAdapter.replaceViewHistory(snapDataHistories);
                break;
            }
            case SCROLL_LOAD: {
                this.viewHistoryAdapter.addAllViewHistory(snapDataHistories);
                break;
            }
            default: {
                this.viewHistoryAdapter.replaceViewHistory(snapDataHistories);
                break;
            }
        }
        if (viewHistoryAdapter.isEmpty()) {
            noDataFound();
        } else {
            dataFound();
        }
        if (!isLastPage)
            page = page + 1;
    }

    @Override
    public void noDataFound() {
        layoutEmpty.setVisibility(View.VISIBLE);
        recyclerViewHistory.setVisibility(View.GONE);
        Glide.with(context)
                .load(R.drawable.empty_states_snap_history_no_data)
                .into(layoutEmptyImg);
        layoutEmptyText.setText(getString(R.string.error_snap_history));
    }

    @Override
    public void dataFound() {
        layoutEmpty.setVisibility(View.GONE);
        recyclerViewHistory.setVisibility(View.VISIBLE);
    }

    @Override
    public void noConnectionError() {
        layoutEmpty.setVisibility(View.VISIBLE);
        Glide.with(context)
                .load(R.drawable.empty_states_no_connection)
                .into(layoutEmptyImg);
        recyclerViewHistory.setVisibility(View.INVISIBLE);
        layoutEmptyText.setText(getString(R.string.error_no_connection));
    }

    @Override
    public void onRefresh() {
        viewHistoryParam = new ViewHistoryParam();
        setPage(1);
        viewHistoryAdapter.deleteAll();
        viewHistoryParam.setFilter(SnapHistoryManager.getFilterValueInt(filterSpinnerPosition));
        viewHistoryParam.setOrder(OrderHistoryManager.getOrderValueInt(orderSpinnerPosition));
        viewHistoryParam.setSort(ConfigManager.SnapViewHistory.SORT_DESC);
        viewHistoryParam.setPage(page);
        viewHistoryParam.setSize(ConfigManager.SnapViewHistory.SIZE);
        viewHistoryPresenter.loadViewHistory(viewHistoryParam, IBaseView.LoadType.SWIPE_LOAD);
        if (swipeRefresh.isRefreshing()) {
            swipeRefresh.setRefreshing(false);
        }
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.SNAP_HISTORY,
                ConfigManager.Analytic.Action.REFRESH,
                "Swipe Refresh"
        );
    }

    @Override
    public void onSnapHistoryClickListener(SnapData snapData) {
        Intent intent = new Intent(context, SnapDetailsActivity.class);
        intent.putExtra(ConfigManager.Snap.DATA, snapData);
        context.startActivity(intent);
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
    }

    @Override
    public void showNotification(String message) {
        notificationMessageView.showNotif(message);
    }

    @Override
    public void showProgressBarRecycler() {
        viewHistoryAdapter.addProgressBar();
    }

    @Override
    public void dismissProgressBarRecycler() {
        viewHistoryAdapter.removeProgressView();
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public void finish() {
        super.finish();
        slideOutHorizontalAnim();
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }
}
