package com.ebizu.manis.mvp.login;

import android.content.Intent;

import com.ebizu.manis.manager.facebook.FacebookManager;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.Google;
import com.ebizu.manis.root.IBaseActivityPresenter;
import com.ebizu.manis.sdk.rest.data.AuthSignIn;

import io.intercom.android.sdk.models.User;

/**
 * Created by Ebizu-User on 10/07/2017.
 */

public interface ILoginActivityPresenter extends IBaseActivityPresenter {

    void loginByGoogle(String regIdGCM);

    void getGooglePerson(Intent data);

    void loginByFacebook(String regIdGCM);

    void loginByPhone(String regIdGCM);

    void loginAccountManis(Google google);

    void loginAccountManis(AuthSignIn.RequestBody.Facebook facebook);

    void registerRewardSession(Account account);

    void loginRewardSdk(Account account);

    FacebookManager getFacebookManager();
}
