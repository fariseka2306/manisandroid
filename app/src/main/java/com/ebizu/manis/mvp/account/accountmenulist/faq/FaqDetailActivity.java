package com.ebizu.manis.mvp.account.accountmenulist.faq;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.FaqChild;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 8/4/2017.
 */

public class FaqDetailActivity extends BaseActivity {

    public static String EXTRA_FAQ = "faq";

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;
    @BindView(R.id.text_view_html)
    TextView textViewHtml;
    FaqChild faqChild;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_detail);
        ButterKnife.bind(this);
        if (getIntent().hasExtra(EXTRA_FAQ)) {
            faqChild = getIntent().getParcelableExtra(EXTRA_FAQ);
        } else {
            throw new IllegalArgumentException("Detail activity must receive a movie parcelable");
        }

        initView();
    }

    private void initView() {
        toolbarView.setTitle(R.string.fa_txt_faq);
        textViewHtml.setText(UtilManis.fromHtml(faqChild.getContent()));
    }

    @Override
    public void finish() {
        super.finish();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.HELP_ACTIVITY_DETAIL,
                ConfigManager.Analytic.Action.CLICK,
                "Button Back"
        );
    }
}
