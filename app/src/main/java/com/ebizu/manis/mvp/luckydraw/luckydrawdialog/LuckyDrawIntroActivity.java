package com.ebizu.manis.mvp.luckydraw.luckydrawdialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.mvp.luckydraw.luckydrawhelp.LuckyDrawHelpView;
import com.ebizu.manis.root.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LuckyDrawIntroActivity extends BaseActivity {

    @BindView(R.id.lucky_draw_help_view)
    LuckyDrawHelpView luckyDrawHelpView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lucky_draw_intro);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    private void initView() {
        SharedPreferences prefFirst = getSharedPreferences(
                UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
        UtilSessionFirstIn.setShowLuckyDrawIntro(prefFirst, false);
        luckyDrawHelpView.setLuckyDrawHelpView(R.string.ld_first_title,
                R.string.ld_first_desc, R.drawable.ticket_icon_white);
    }

    @OnClick(R.id.luckydraw_btn_ok)
    public void onBtnAwesomeClick() {
        finish();
    }
}