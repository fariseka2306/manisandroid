package com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistoryfactory;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.PurchaseHistory;

/**
 * Created by halim_ebizu on 10/17/17.
 */

public class PurchaseStatusPending extends PurchaseHistoryAbstract implements PurchaseStatus {

    public PurchaseStatusPending(PurchaseHistory purchaseHistory, Context context) {
        super(purchaseHistory, context);
    }

    @Override
    public String getStatus() {
        return ConfigManager.PurchaseHistory.STATUS_PENDING;
    }

    @Override
    public Drawable imageIcon() {
        return ContextCompat.getDrawable(context, R.drawable.ic_purchase_status_on_process);
    }

    @Override
    public int color() {
        return ContextCompat.getColor(context, R.color.purchase_process);
    }

    @Override
    public boolean isShowInfo() {
        return true;
    }
}
