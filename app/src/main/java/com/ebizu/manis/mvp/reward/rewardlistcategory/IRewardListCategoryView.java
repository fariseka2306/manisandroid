package com.ebizu.manis.mvp.reward.rewardlistcategory;

import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.sdk.reward.models.Reward;

import java.util.List;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public interface IRewardListCategoryView extends IBaseView {

    void loadRewarVoucherList(List<Reward> rewardVoucherList, String keyword);

    void loadRewardVoucherListNext(List<Reward> rewardVoucherList);

    void loadRewardBulkList(List<RewardVoucher> rewardVouchers);

    void loadRewardBulkListNext(List<RewardVoucher> rewardVouchers);

    void handleConnectionFailure();

    IRewardListCategoryPresenter getRewardCategoryPresenter();
}
