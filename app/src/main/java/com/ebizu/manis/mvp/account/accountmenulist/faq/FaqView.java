package com.ebizu.manis.mvp.account.accountmenulist.faq;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.model.Faq;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.view.adapter.FaqAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/31/2017.
 */

public class FaqView extends BaseView implements IFaqView {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private LinearLayoutManager linearLayoutManager;
    FaqAdapter faqAdapter;
    IFaqPresenter iFaqPresenter;

    public FaqView(Context context) {
        super(context);
        createView(context);
    }

    public FaqView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public FaqView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public FaqView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_faq, null, false);
        faqAdapter = new FaqAdapter(context, new ArrayList<>());
        addView(view);
        ButterKnife.bind(this, view);
        attachPresenter(new FaqPresenter());
        initialize();
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        this.iFaqPresenter = (IFaqPresenter) iBaseViewPresenter;
        this.iFaqPresenter.attachView(this);
    }

    public void initialize() {
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(faqAdapter);
    }

    @Override
    public void showFaqs(List<Faq> faqses) {
        faqAdapter.addFaqList(faqses);
    }

    @Override
    public IFaqPresenter getFaqPresenter() {
        return iFaqPresenter;
    }
}
