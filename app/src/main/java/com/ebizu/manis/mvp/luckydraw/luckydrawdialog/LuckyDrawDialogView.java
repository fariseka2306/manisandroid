package com.ebizu.manis.mvp.luckydraw.luckydrawdialog;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.LuckyDrawEntryData;
import com.ebizu.manis.model.LuckyDrawWinnerData;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.view.adapter.luckydraw.LuckyDrawEntryAdapter;
import com.ebizu.manis.view.adapter.luckydraw.LuckyDrawWinnerAdapter;
import com.ebizu.manis.view.linearlayout.FixedRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 08/08/17.
 */

public class LuckyDrawDialogView extends BaseView implements ILuckyDrawDialogView {

    private ViewType viewType;

    private ILuckyDrawDialogPresenter iLuckyDrawDialogPresenter;
    public LuckyDrawEntryAdapter luckyDrawEntryAdapter;
    public LuckyDrawWinnerAdapter luckyDrawWinnerAdapter;

    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.rv_lucky_draw_winner)
    RecyclerView rvLuckyDrawWinner;
    @BindView(R.id.rv_lucky_draw_entry)
    RecyclerView rvLuckyDrawEntry;
    @BindView(R.id.layout_empty_txt)
    TextView textViewMessage;
    @BindView(R.id.swipe_refresh)
    FixedRefreshLayout swipeRefresh;

    public LuckyDrawDialogView(Context context) {
        super(context);
        createView(context);
    }

    public LuckyDrawDialogView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public LuckyDrawDialogView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LuckyDrawDialogView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.lucky_draw_dialog_view, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        initLuckyDrawWinnerView();
        initLuckyDrawEntryView();
        initSwipeRefreshView();
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
        switch (viewType) {
            case LUCKY_DRAW_ENTRY: {
                rvLuckyDrawEntry.setVisibility(VISIBLE);
                break;
            }
            case LUCKY_DRAW_WINNER: {
                rvLuckyDrawWinner.setVisibility(VISIBLE);
                break;
            }
        }
    }

    private void initLuckyDrawEntryView() {
        luckyDrawEntryAdapter = new LuckyDrawEntryAdapter(getContext(), rvLuckyDrawEntry);
        luckyDrawEntryAdapter.setOnScrollListener(() -> {
            RequestBody requestBody = getLuckyDrawDialogPresenter().createLuckyDrawEntriesRB(getPagePresenter());
            getLuckyDrawDialogPresenter().loadLuckyDrawEntryData(LoadType.SCROLL_LOAD, requestBody);
        });
    }

    private void initLuckyDrawWinnerView() {
        luckyDrawWinnerAdapter = new LuckyDrawWinnerAdapter(getContext(), rvLuckyDrawWinner);
        luckyDrawWinnerAdapter.setOnScrollListener(() -> {
            RequestBody requestBody = getLuckyDrawDialogPresenter().createLuckyDrawWinnersRB(getPagePresenter());
            getLuckyDrawDialogPresenter().loadLuckyDrawWinner(LoadType.SCROLL_LOAD, requestBody);
        });
    }

    public void initSwipeRefreshView() {
        swipeRefresh.setColorSchemeResources(R.color.color_graph_green);
        swipeRefresh.setOnRefreshListener(() -> {
            if (swipeRefresh.isRefreshing()) {
                swipeRefresh.setRefreshing(false);
                setPagePresenter(1);
                switch (viewType) {
                    case LUCKY_DRAW_WINNER: {
                        RequestBody requestBody = getLuckyDrawDialogPresenter().createLuckyDrawWinnersRB(getPagePresenter());
                        getLuckyDrawDialogPresenter().loadLuckyDrawWinner(LoadType.SWIPE_LOAD, requestBody);
                        break;
                    }
                    case LUCKY_DRAW_ENTRY:
                        RequestBody requestBody = getLuckyDrawDialogPresenter().createLuckyDrawEntriesRB(getPagePresenter());
                        getLuckyDrawDialogPresenter().loadLuckyDrawEntryData(LoadType.SWIPE_LOAD, requestBody);
                        break;
                }
            }
        });
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        super.attachPresenter(iBaseViewPresenter);
        iLuckyDrawDialogPresenter = (ILuckyDrawDialogPresenter) iBaseViewPresenter;
        iLuckyDrawDialogPresenter.attachView(this);
    }

    @Override
    public void setLuckyDrawEntryData(LoadType loadType, LuckyDrawEntryData luckyDrawEntryData) {
        txtTitle.setText(luckyDrawEntryData.getEntriesTitle());
        setPaginationPresenter(luckyDrawEntryData.isMore());
        switch (loadType) {
            case CLICK_LOAD: {
                luckyDrawEntryAdapter.replaceAdapterList(luckyDrawEntryData.getTickets());
                break;
            }
            case SWIPE_LOAD: {
                luckyDrawEntryAdapter.replaceAdapterList(luckyDrawEntryData.getTickets());
                break;
            }
            case SCROLL_LOAD: {
                luckyDrawEntryAdapter.insertAdapterList(luckyDrawEntryData.getTickets());
                break;
            }
            default: {
                break;
            }
        }
        if (isEmptyList()) {
            showNegativeScenario(getResources().getString(R.string.error_no_data_ld_entries));
        } else {
            setPagePresenter(getPagePresenter() + 1);
        }
    }

    @Override
    public void setLuckyDrawWinnerData(LoadType loadType, LuckyDrawWinnerData luckyDrawWinnerData) {
        txtTitle.setText(luckyDrawWinnerData.getWinnerTitle());
        setPaginationPresenter(luckyDrawWinnerData.isMore());
        switch (loadType) {
            case CLICK_LOAD: {
                luckyDrawWinnerAdapter.replaceAdapterList(luckyDrawWinnerData.getLuckyDrawWinners());
                break;
            }
            case SWIPE_LOAD: {
                luckyDrawWinnerAdapter.replaceAdapterList(luckyDrawWinnerData.getLuckyDrawWinners());
                break;
            }
            case SCROLL_LOAD: {
                luckyDrawWinnerAdapter.insertAdapterList(luckyDrawWinnerData.getLuckyDrawWinners());
                break;
            }
            default: {
                break;
            }
        }
        if (isEmptyList())
            showNegativeScenario(getResources().getString(R.string.error_no_data_ld_winners));
    }

    @Override
    public void showProgressView(IBaseView.LoadType loadType, ViewType viewType) {
        setLoadingPresenter();
        textViewMessage.setVisibility(GONE);
        switch (loadType) {
            case CLICK_LOAD: {
                showProgressBar();
                break;
            }
            case SCROLL_LOAD: {
                showListProgressBar();
                break;
            }
            case SWIPE_LOAD: {
                showProgressBar();
            }
        }
    }

    @Override
    public void dismissProgressView(IBaseView.LoadType loadType, ViewType viewType) {
        switch (loadType) {
            case CLICK_LOAD: {
                dismissProgressBar();
                break;
            }
            case SCROLL_LOAD: {
                dismissListProgressBar();
                break;
            }
            case SWIPE_LOAD: {
                dismissProgressBar();
                break;
            }
        }
        setUnloadingPresenter();
    }

    @Override
    public void showNegativeScenario(String message) {
        if (isEmptyList()) {
            textViewMessage.setVisibility(VISIBLE);
            textViewMessage.setText(message);
        } else {
            getBaseActivity().showManisAlertDialog(message);
        }
    }

    @Override
    public ILuckyDrawDialogPresenter getLuckyDrawDialogPresenter() {
        return iLuckyDrawDialogPresenter;
    }

    @Override
    public boolean isEmptyList() {
        switch (viewType) {
            case LUCKY_DRAW_ENTRY: {
                return luckyDrawEntryAdapter.isEmpty();
            }
            case LUCKY_DRAW_WINNER: {
                return luckyDrawWinnerAdapter.isEmpty();
            }
            default: {
                return true;
            }
        }
    }

    @Override
    public void setLoadingPresenter() {
        switch (viewType) {
            case LUCKY_DRAW_ENTRY: {
                luckyDrawEntryAdapter.setLoading(true);
                break;
            }
            case LUCKY_DRAW_WINNER: {
                luckyDrawWinnerAdapter.setLoading(true);
                break;
            }
        }
    }

    @Override
    public void setUnloadingPresenter() {
        switch (viewType) {
            case LUCKY_DRAW_ENTRY: {
                luckyDrawEntryAdapter.setLoading(false);
                break;
            }
            case LUCKY_DRAW_WINNER: {
                luckyDrawWinnerAdapter.setLoading(false);
                break;
            }
        }
    }

    @Override
    public void setPaginationPresenter(boolean isMore) {
        switch (viewType) {
            case LUCKY_DRAW_ENTRY: {
                luckyDrawEntryAdapter.setPagination(isMore);
                break;
            }
            case LUCKY_DRAW_WINNER: {
                luckyDrawWinnerAdapter.setPagination(isMore);
                break;
            }
        }
    }

    @Override
    public void setPagePresenter(int page) {
        switch (viewType) {
            case LUCKY_DRAW_ENTRY: {
                luckyDrawEntryAdapter.setPage(page);
                break;
            }
            case LUCKY_DRAW_WINNER: {
                luckyDrawWinnerAdapter.setPage(page);
                break;
            }
        }
    }

    @Override
    public int getPagePresenter() {
        switch (viewType) {
            case LUCKY_DRAW_ENTRY: {
                return luckyDrawEntryAdapter.getPage();
            }
            case LUCKY_DRAW_WINNER: {
                return luckyDrawWinnerAdapter.getPage();
            }
            default: {
                return 1;
            }
        }
    }

    @Override
    public void showListProgressBar() {
        switch (viewType) {
            case LUCKY_DRAW_ENTRY: {
                luckyDrawEntryAdapter.showListProgressBar();
                break;
            }
            case LUCKY_DRAW_WINNER: {
                luckyDrawWinnerAdapter.showListProgressBar();
                break;
            }
        }
    }

    @Override
    public void dismissListProgressBar() {
        switch (viewType) {
            case LUCKY_DRAW_ENTRY: {
                luckyDrawEntryAdapter.dismissListProgressBar();
                break;
            }
            case LUCKY_DRAW_WINNER: {
                luckyDrawWinnerAdapter.dismissListProgressBar();
                break;
            }
        }
    }
}
