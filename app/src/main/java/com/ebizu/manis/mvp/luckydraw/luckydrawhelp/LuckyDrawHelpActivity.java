package com.ebizu.manis.mvp.luckydraw.luckydrawhelp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;
import com.ebizu.manis.di.component.ActivityComponent;
import com.ebizu.manis.di.component.DaggerActivityComponent;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.mvp.luckydraw.ColorTransformer;
import com.ebizu.manis.mvp.luckydraw.ViewPagerCustomDuration;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.adapter.luckydraw.LuckyDrawHelpAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ebizu.manis.helper.UtilStatic.REQUEST_SNAP;
import static com.ebizu.manis.manager.tracker.TrackerConstant.kTrackerOriginPageLuckyDraw;

/**
 * Created by Halim on 8/6/17.
 */

public class LuckyDrawHelpActivity extends BaseActivity {

    @BindView(R.id.indicator)
    CirclePageIndicator circlePageIndicator;

    @BindView(R.id.pager)
    ViewPagerCustomDuration viewPagerCustomDuration;

    @Inject
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_luckydraw_help);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    protected void prepareInjection() {
        super.prepareInjection();
        ActivityComponent activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).build();
        activityComponent.inject(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        performBackAnimation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SNAP) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                performBackAnimation();
            } else {
                performBackAnimation();
            }
        }
    }

    private void initView() {
        List<Integer> arrayColor = new ArrayList<>();
        arrayColor.add(ContextCompat.getColor(context, R.color.colorCircleRedBg));
        arrayColor.add(ContextCompat.getColor(context, R.color.color_graph_green));
        arrayColor.add(ContextCompat.getColor(context, R.color.colorBlueGuide));
        arrayColor.add(ContextCompat.getColor(context, R.color.colorSlateGuide));

        viewPagerCustomDuration.setAdapter(new LuckyDrawHelpAdapter(getSupportFragmentManager(), getFragments()));
        viewPagerCustomDuration.setPageTransformer(false, new ColorTransformer((ArrayList<Integer>) arrayColor));
        viewPagerCustomDuration.setOffscreenPageLimit(ConfigManager.LuckyDraw.NUM_PAGES);
        viewPagerCustomDuration.setScrollDurationFactor(ConfigManager.LuckyDraw.SCROLL_DURATION_FACTOR);

        circlePageIndicator.setViewPager(viewPagerCustomDuration);
        circlePageIndicator.setRadius(ConfigManager.LuckyDraw.NUM_PAGES * getResources().getDisplayMetrics().density);
        circlePageIndicator.setPageColor(ContextCompat.getColor(context, R.color.colorGrey));
        circlePageIndicator.setFillColor(ContextCompat.getColor(context, R.color.colorRed));
        circlePageIndicator.setStrokeColor(ContextCompat.getColor(context, R.color.colorTransparent));
        circlePageIndicator.setStrokeWidth(ConfigManager.LuckyDraw.STROKE_WIDTH * getResources().getDisplayMetrics().density);
    }

    private List<Fragment> getFragments() {
        LuckyDrawHelpFirstFragment page1 = new LuckyDrawHelpFirstFragment();
        LuckyDrawHelpSecondFragment page2 = new LuckyDrawHelpSecondFragment();
        LuckyDrawHelpThirdFragment page3 = new LuckyDrawHelpThirdFragment();

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(page1);
        fragmentList.add(page2);
        fragmentList.add(page3);
        return fragmentList;
    }

    public void switchToFragment(int position) {
        viewPagerCustomDuration.setCurrentItem(position, true);
    }

    private void performBackAnimation() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void finish() {
        super.finish();
        TrackerManager.getInstance().setTrackerData(
                new TrackerStandartRequest(kTrackerOriginPageLuckyDraw, kTrackerOriginPageLuckyDraw, "", "", "", ""));
    }
}
