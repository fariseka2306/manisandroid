package com.ebizu.manis.mvp.notification.listnotification;

import com.ebizu.manis.root.IBaseViewPresenter;

/**
 * Created by Ebizu on 8/22/17.
 */

public interface IListNotificationPresenter extends IBaseViewPresenter {

    void getNotificationList();

    void deleteNotificationList();

}
