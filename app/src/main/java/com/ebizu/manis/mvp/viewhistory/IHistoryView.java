package com.ebizu.manis.mvp.viewhistory;

import com.ebizu.manis.model.ViewHistoryResult;
import com.ebizu.manis.root.IBaseView;

/**
 * Created by Raden on 7/24/17.
 */

public interface IHistoryView {

    void setViewHistory(ViewHistoryResult viewHistoryResult, IBaseView.LoadType loadType);

    void noDataFound();

    void dataFound();

    void noConnectionError();

    void showProgressBarRecycler();

    void dismissProgressBarRecycler();

    void showNotification(String message);

    void onClickFilterSpinner(int position);

    void onClickOrderSpinner(int position);
}
