package com.ebizu.manis.mvp.reward.rewardlistcategory;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.root.BaseViewPresenter;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.service.manis.requestbody.RewardCategoryBody;
import com.ebizu.manis.service.reward.RewardApi;
import com.ebizu.manis.service.reward.RewardApiGenerator;
import com.ebizu.manis.service.reward.requestbody.RewardBulkListBody;
import com.ebizu.manis.service.reward.response.ResponseRewardApi;
import com.ebizu.manis.service.reward.response.WrapperRewardVoucherList;
import com.ebizu.sdk.reward.EbizuReward;
import com.ebizu.sdk.reward.core.interfaces.Callback;
import com.ebizu.sdk.reward.models.Filter;
import com.ebizu.sdk.reward.models.Response;
import com.ebizu.sdk.reward.models.Reward;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public class RewardListCategoryPresenter extends BaseViewPresenter implements IRewardListCategoryPresenter {

    private final String TAG = getClass().getSimpleName();

    private Context context;

    private RewardListCategoryAbstractView rewardCategoryAbstractView;

    private RewardApi rewardApi;

    private Subscription subRewardList;

    public RewardListCategoryPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachView(BaseView baseView) {
        super.attachView(baseView);
        rewardCategoryAbstractView = (RewardListCategoryAbstractView) baseView;
    }

    @Override
    public void releaseSubscribe(int key) {
        if (subRewardList != null)
            subRewardList.unsubscribe();
    }

    @Override
    public void releaseAllSubscribes() {
        releaseSubscribe(0);
    }

    @Override
    public void loadRewardVoucherList(RewardCategoryBody rewardCategoryBody, Filter filter, String keyword) {
        rewardCategoryAbstractView.showProgressBar();
        EbizuReward.getProvider(context).filterReward(keyword, filter, rewardCategoryBody.getPage(), rewardCategoryBody.getSize(), true,
                new Callback<Response.Data<Reward>>() {
                    @Override
                    public void onSuccess(Response.Data<Reward> rewardData) {
                        rewardCategoryAbstractView.dismissNoInternetConnection();
                        rewardCategoryAbstractView.isLastPage = !rewardData.isMore();
                        rewardCategoryAbstractView.loadRewarVoucherList(rewardData.getList(), keyword);
                        if (rewardData.getList().isEmpty()) {
                            rewardCategoryAbstractView.showEmptyState(keyword);
                        }
                    }

                    @Override
                    public void onFailure(String s) {
                        Log.d(TAG, "Failure Retrieve Data " + s);
                        if (!ConnectionDetector.isNetworkConnected(context)) {
                            rewardCategoryAbstractView.dismissProgressBar();
                            rewardCategoryAbstractView.handleConnectionFailure();
                        }
                    }
                });
    }

    @Override
    public void loadRewardVoucherListNext(RewardCategoryBody rewardCategoryBody, Filter filter, String keyword) {
        EbizuReward.getProvider(context).filterReward("", filter, rewardCategoryBody.getPage(), rewardCategoryBody.getSize(), true,
                new Callback<Response.Data<Reward>>() {
                    @Override
                    public void onSuccess(Response.Data<Reward> rewardData) {
                        rewardCategoryAbstractView.isLastPage = !rewardData.isMore();
                        rewardCategoryAbstractView.loadRewardVoucherListNext(rewardData.getList());
                        if (rewardData.getList().isEmpty()) {
                            rewardCategoryAbstractView.layoutEmpty.setVisibility(View.VISIBLE);
                            rewardCategoryAbstractView.textViewEmpty.setText(R.string.error_reward_notfound);
                        }
                    }

                    @Override
                    public void onFailure(String s) {
                        Log.d(TAG, "Failure Retrieve Data " + s);
                        if (!ConnectionDetector.isNetworkConnected(context)) {
                            rewardCategoryAbstractView.dismissProgressBar();
                            rewardCategoryAbstractView.handleConnectionFailure();
                        }
                    }
                });
    }

    @Override
    public void loadRewardsBulkList(RewardBulkListBody rewardBulkListBody) {
        releaseSubscribe(0);
        rewardCategoryAbstractView.showProgressBar();
        if (rewardApi == null)
            rewardApi = RewardApiGenerator.createService(context);
        rewardApi.getRewardBulkList(rewardBulkListBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperRewardVoucherList>(rewardCategoryAbstractView) {
                    @Override
                    public void onNext(WrapperRewardVoucherList wrapperRewardVoucherList) {
                        super.onNext(wrapperRewardVoucherList);
                        rewardCategoryAbstractView.isLastPage = !wrapperRewardVoucherList.getData().getMore();
                        rewardCategoryAbstractView.loadRewardBulkList(wrapperRewardVoucherList.getData().getRewardVoucherList());
                        if (wrapperRewardVoucherList.getData().getRewardVoucherList().isEmpty()) {
                            rewardCategoryAbstractView.layoutEmpty.setVisibility(View.VISIBLE);
                            rewardCategoryAbstractView.textViewEmpty.setText(R.string.error_reward_notfound);
                        }
                    }

                    @Override
                    public void onFailure(ResponseRewardApi responseRewardApi) {
                        super.onFailure(responseRewardApi);
                    }
                });
    }

    @Override
    public void loadRewardBulkListNext(RewardBulkListBody rewardBulkListBody) {
        releaseSubscribe(0);
        if (rewardApi == null)
            rewardApi = RewardApiGenerator.createService(context);
        rewardApi.getRewardBulkList(rewardBulkListBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperRewardVoucherList>(rewardCategoryAbstractView) {
                    @Override
                    public void onNext(WrapperRewardVoucherList wrapperRewardVoucherList) {
                        super.onNext(wrapperRewardVoucherList);
                        rewardCategoryAbstractView.isLastPage = !wrapperRewardVoucherList.getData().getMore();
                        rewardCategoryAbstractView.loadRewardBulkListNext(wrapperRewardVoucherList.getData().getRewardVoucherList());
                        if (wrapperRewardVoucherList.getData().getRewardVoucherList().isEmpty()) {
                            rewardCategoryAbstractView.layoutEmpty.setVisibility(View.VISIBLE);
                            rewardCategoryAbstractView.textViewEmpty.setText(R.string.error_reward_notfound);
                        }
                    }

                    @Override
                    public void onFailure(ResponseRewardApi responseRewardApi) {
                        super.onFailure(responseRewardApi);
                        Log.d(TAG, "Failure Retrieve Data " + responseRewardApi.getErrorMessage());
                        if (!ConnectionDetector.isNetworkConnected(context)) {
                            rewardCategoryAbstractView.dismissProgressBar();
                            rewardCategoryAbstractView.showNoInternetConnection();
                        }
                    }
                });
    }
}
