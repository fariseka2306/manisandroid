package com.ebizu.manis.mvp.reward.rewarddetail;

import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.root.IBaseViewPresenter;
import com.ebizu.manis.service.manis.requestbody.RewardRedeemBody;
import com.ebizu.sdk.reward.models.Reward;
import com.ebizu.sdk.reward.models.VoucherInput;

import java.util.List;

/**
 * Created by Raden on 8/1/17.
 */

public interface IRewardDetailPresenter extends IBaseViewPresenter{
    void loadReward(RewardRedeemBody rewardRedeemBody);

    void setRewardUser(final String refCode, RewardVoucher reward);

    void getShoppingCart(RewardVoucher reward, int qty);

    void setVoucherInputList(List<VoucherInput> voucherInputs);
}
