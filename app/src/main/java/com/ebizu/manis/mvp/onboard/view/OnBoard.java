package com.ebizu.manis.mvp.onboard.view;

import android.content.Context;

/**
 * Created by ebizu on 8/22/17.
 */

public class OnBoard implements IOnBoard {

    protected Context context;

    public OnBoard(Context context) {
        this.context = context;
    }

    @Override
    public int index() {
        return 0;
    }

    @Override
    public String title() {
        return null;
    }

    @Override
    public String messageBody() {
        return null;
    }

    @Override
    public int colorBackground() {
        return 0;
    }

    @Override
    public int rawVideo() {
        return 0;
    }
}
