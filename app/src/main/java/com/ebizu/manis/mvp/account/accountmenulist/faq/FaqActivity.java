package com.ebizu.manis.mvp.account.accountmenulist.faq;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.Faq;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.view.manis.toolbar.ToolbarView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FaqActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    ToolbarView toolbarView;
    @BindView(R.id.faq_view)
    FaqView faqView;

    Faq faq;
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_menulist_faq);
        faq = new Faq();
        context = this;
        ButterKnife.bind(this);
        initView();
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void initView() {
        toolbarView.setTitle(R.string.fa_txt_faq);
        faqView.attachPresenter(new FaqPresenter());
        faqView.getFaqPresenter().loadFaq(ManisApiGenerator.createServiceWithToken(context), faq);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            finishActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void finish() {
        super.finish();
        getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.HELP_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Back"
        );
    }
}
