package com.ebizu.manis.mvp.mission;

import com.ebizu.manis.root.IBaseView;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperMissions;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public interface IMissionView extends IBaseView {

    void showShareExperience(WrapperMissions wrapperMissions);

    void showShareExperienceNextPage(WrapperMissions wrapperMissions);

    IMissionPresenter getSharedExperiencePresenter();

}
