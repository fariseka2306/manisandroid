package com.ebizu.manis.mvp.reward.rewarddetail.rewarddetailregular;

import com.ebizu.manis.mvp.reward.rewarddetail.RewardDetailAbstractActivity;

/**
 * Created by FARIS_mac on 15/11/17.
 */

public class RewardRegularDetailActivity extends RewardDetailAbstractActivity {

    @Override
    public void onCLickBuy() {
        super.onCLickBuy();
    }

    @Override
    public void onClickRedeem() {
        super.onClickRedeem();
        initAnalyticTrack();
    }
}
