package com.ebizu.manis.mvp.beacon;

import android.content.Context;

import com.ebizu.manis.model.notification.beacon.BeaconPromo;
import com.ebizu.manis.view.manis.notification.LocalNotification;

/**
 * Created by ebizu on 9/6/17.
 */

public class BeaconNotification extends LocalNotification {

    private static long SECOND = 1000;
    private static long MINUTE = SECOND * 60;
    private static long HOUR = MINUTE * 60;
    private static long DAY = HOUR * 24;

    public BeaconNotification(Context context) {
        super(context);
    }

    public void showBeaconNotification(BeaconPromo beaconPromo){
        notification.setContentTitle(beaconPromo.getName());
        notification.setContentText(beaconPromo.getDescription());
        notificationManager.notify(beaconPromo.getCompanyId(), notification.build());
    }

}
