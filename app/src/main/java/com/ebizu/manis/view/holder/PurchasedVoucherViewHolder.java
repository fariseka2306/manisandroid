package com.ebizu.manis.view.holder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.PurchaseHistory;
import com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.purchasehistoryfactory.PurchaseHistoryFactory;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 9/14/17.
 */

public class PurchasedVoucherViewHolder extends BaseHolder<PurchaseHistory> {

    private PurchaseHistory purchaseHistory;

    private PurchaseHistoryFactory purchaseHistoryFactory;

    @BindView(R.id.view_group_purchase_voucher)
    ViewGroup viewGroupPurchaseVoucher;

    @BindView(R.id.image_view_status_purchase)
    ImageView imageViewPurchaseStatus;

    @BindView(R.id.text_view_status)
    TextView textViewStatus;

    @BindView(R.id.text_view_voucher_amount)
    TextView textViewVoucherAmount;

    @BindView(R.id.text_view_voucher_brand)
    TextView textViewVoucherBrand;

    @BindView(R.id.text_view_trans_time)
    TextView textViewTransTime;

    @BindView(R.id.text_view_gross_amount)
    TextView textViewGrossAmount;

    @BindView(R.id.view_group_info)
    ViewGroup viewGroupInfo;

    @BindView(R.id.text_view_info)
    TextView textViewInfo;

    @BindView(R.id.text_view_bulk_info)
    TextView textViewBulkInfo;

    @BindView(R.id.view_divider_bottom_info)
    View dividerInfoBottom;

    public PurchasedVoucherViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void setHolderView(PurchaseHistory purchaseHistory) {
        try {
            textViewVoucherAmount.setText(purchaseHistory.getVoucherName());
            textViewVoucherBrand.setText(purchaseHistory.getPurchaseHistoryBrand().getName());
            textViewTransTime.setText(purchaseHistory.getTransactionTime());
            textViewGrossAmount.setText(purchaseHistory.getCurrency().concat(" ")
                    .concat(String.valueOf(purchaseHistory.getAmount())));
            setViewStatus(purchaseHistory);
            this.purchaseHistory = purchaseHistory;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setViewStatus(PurchaseHistory purchaseHistory) {
        purchaseHistoryFactory = new PurchaseHistoryFactory(purchaseHistory, itemView.getContext());
        imageViewPurchaseStatus.setImageDrawable(purchaseHistoryFactory.getImageDrawable());
        textViewStatus.setTextColor(purchaseHistoryFactory.getColor());
        if (!purchaseHistory.getPurchaseDescription().isEmpty() || purchaseHistory.getPurchaseDescription() != null) {
            textViewBulkInfo.setText(purchaseHistory.getPurchaseDescription());
        }
        setInfo(purchaseHistory);
    }

    private void setInfo(PurchaseHistory purchaseHistory) {
        if (purchaseHistoryFactory.isShowInfo()) {
            showInfo(purchaseHistory.getPaymentSubtitle());
        } else {
            hideInfo();
        }
    }

    public void showInfo(String info) {
        textViewInfo.setText(info);
        dividerInfoBottom.setVisibility(View.VISIBLE);
        viewGroupInfo.setVisibility(View.VISIBLE);
        textViewInfo.setVisibility(View.VISIBLE);
    }

    public void hideInfo() {
        dividerInfoBottom.setVisibility(View.GONE);
        viewGroupInfo.setVisibility(View.GONE);
        textViewInfo.setVisibility(View.GONE);
    }

}

