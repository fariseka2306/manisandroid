package com.ebizu.manis.view.manis.textview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 11/17/17.
 */

public class ShippingDetailsFormText extends RelativeLayout {

    private String titleText = "";
    private String inputText = "";
    private int backgroundColor;
    private int maxLine;

    @BindView(R.id.divider_top)
    View dividerViewTop;
    @BindView(R.id.divider_bottom)
    View dividerViewBottom;
    @BindView(R.id.text_view_title_input)
    TextView textViewTitle;
    @BindView(R.id.text_view_input)
    TextView textViewInput;

    public ShippingDetailsFormText(Context context) {
        super(context);
        createView(context);
    }

    public ShippingDetailsFormText(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context, attrs);
    }

    public ShippingDetailsFormText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ShippingDetailsFormText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context, attrs);
    }

    private void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.text_view_order_details, null, false);
        addView(view, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    private void createView(Context context, AttributeSet attrs) {
        createView(context);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ShippingDetailsFormText, 0, 0);
        getAttrValues(a, context);
        setDividerView(a);
        setView();
    }

    private void getAttrValues(TypedArray a, Context context) {
        titleText = a.getString(R.styleable.ShippingDetailsFormText_textTitle);
        inputText = a.getString(R.styleable.ShippingDetailsFormText_textInput);
        backgroundColor = a.getColor(R.styleable.ShippingDetailsFormText_backgroundColor,
                ContextCompat.getColor(context, R.color.white));
        maxLine = a.getInt(R.styleable.ShippingDetailsFormText_maxLine, Integer.MAX_VALUE);
    }

    private void setView() {
        textViewTitle.setText(titleText);
        textViewInput.setText(inputText);
        textViewInput.setMaxLines(maxLine);
        setBackgroundColor(backgroundColor);
        setMaxLine(maxLine);
    }

    private void setDividerView(TypedArray a) {
        boolean showDividerTop = a.getBoolean(R.styleable.ShippingDetailsFormText_dividerTop, false);
        boolean showDividerBottom = a.getBoolean(R.styleable.ShippingDetailsFormText_dividerBottom, false);
        if (showDividerTop) dividerViewTop.setVisibility(VISIBLE);
        if (showDividerBottom) dividerViewBottom.setVisibility(VISIBLE);
    }

    public void setTitle(String title) {
        textViewTitle.setText(title);
    }

    public void setInputText(String inputText) {
        textViewInput.setText(inputText);
    }

    @Override
    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        super.setBackgroundColor(backgroundColor);
    }

    public void setBackgroundView(int drawable) {
        setBackground(ContextCompat.getDrawable(getContext(), drawable));
    }

    public void setMaxLine(int maxLine) {
        this.maxLine = maxLine;
        if (maxLine >= 1) {
            textViewInput.setSingleLine(false);
            textViewInput.setMaxLines(maxLine);
        } else {
            textViewInput.setSingleLine(true);
        }
    }

}
