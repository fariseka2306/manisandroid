package com.ebizu.manis.view.manis.nodatastorefound;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 10/16/17.
 */

public class NoDataStoreFoundView extends RelativeLayout {

    @BindView(R.id.image_view_no_data_store)
    ImageView imageViewNoDataStore;

    @BindView(R.id.text_view_message)
    TextView textViewMessage;

    @BindView(R.id.text_view_message_top)
    TextView textViewMessageTop;

    public enum Type {
        SEARCH_VIEW,
        NEAR_VIEW
    }

    public NoDataStoreFoundView(Context context) {
        super(context);
        createView(context);
    }

    public NoDataStoreFoundView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public NoDataStoreFoundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NoDataStoreFoundView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    public void setSearchView() {
        textViewMessageTop.setVisibility(VISIBLE);
        textViewMessageTop.setText(getContext().getString(R.string.error_store_search));
        imageViewNoDataStore.setVisibility(GONE);
        textViewMessage.setVisibility(GONE);
    }

    public void setNearView() {
        textViewMessageTop.setVisibility(GONE);
        imageViewNoDataStore.setVisibility(VISIBLE);
        textViewMessage.setVisibility(VISIBLE);
        Glide.with(getContext())
                .load(R.drawable.empty_states_snap_recent_no_data)
                .into(imageViewNoDataStore);
        textViewMessage.setText(getContext().getString(R.string.error_no_store_nearby_multiplier));
    }

    private void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_no_data_store, null, false);
        addView(view, new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
    }

    public void setTypeView(Type typeView) {
        if (typeView == Type.NEAR_VIEW) {
            setNearView();
        } else if (typeView == Type.SEARCH_VIEW) {
            setSearchView();
        }
    }
}
