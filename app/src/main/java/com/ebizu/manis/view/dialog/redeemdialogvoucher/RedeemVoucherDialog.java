package com.ebizu.manis.view.dialog.redeemdialogvoucher;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.service.manis.requestbody.RewardRedeemBody;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;
import com.ebizu.manis.view.dialog.redeemdialogsuccess.RedeemDialogSuccessSecond;
import com.ebizu.sdk.reward.models.Redeem;
import com.ebizu.sdk.reward.models.Reward;
import com.ebizu.sdk.reward.models.VoucherInput;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARIS_mac on 10/2/17.
 */

public class RedeemVoucherDialog extends BaseDialogManis implements IRedeemRewardDialog {

    private String TAG = getClass().getName();
    private RewardVoucher reward;
    private RedeemVoucherPresenterPresenter redeemVoucherPresenter;
    private RewardRedeemBody rewardRedeemBody;
    private List<VoucherInput> voucherInputs = new ArrayList<>();

    private ImageView imageViewCompany;
    private WebView webViewRedeem;
    private TextView textViewRewards, textViewMerchantName;
    private Button buttonShowCode, buttonClose;
    private double latitude, longitude;


    public void setRedeemVoucherView(List<VoucherInput> voucherInputs) {

    }

    public void setReward(RewardVoucher reward) {
        this.reward = reward;
        setViewDialog(reward);
    }

    public RedeemVoucherDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public RedeemVoucherDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected RedeemVoucherDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_redeem_voucher, null, false);
        getParent().addView(view);
        imageViewCompany = (ImageView) findViewById(R.id.imageview_reward);
        webViewRedeem = (WebView) findViewById(R.id.webview_redeem);
        textViewRewards = (TextView) findViewById(R.id.textview_reward);
        textViewMerchantName = (TextView) findViewById(R.id.textview_merchant);
        buttonClose = (Button) findViewById(R.id.button_cancel);
        buttonShowCode = (Button) findViewById(R.id.button_ok);
        buttonClose.setOnClickListener(v -> dismiss());
        buttonShowCode.setOnClickListener(v -> {
            attachDialogPresenter(new RedeemVoucherPresenterPresenter(getContext()));
            rewardRedeemBody = new RewardRedeemBody();
            rewardRedeemBody.setId(reward.getId());
            rewardRedeemBody.setPoint(reward.getPoint());
            rewardRedeemBody.setVoucher_type(reward.getType());
            getRedeemVoucherPresenter().loadReward(rewardRedeemBody);
        });
    }

    @Override
    public void attachDialogPresenter(BaseDialogPresenter baseDialogPresenter) {
        super.attachDialogPresenter(baseDialogPresenter);
        redeemVoucherPresenter = (RedeemVoucherPresenterPresenter) baseDialogPresenter;
        redeemVoucherPresenter.attachDialog(this);
    }

    public RedeemVoucherPresenterPresenter getRedeemVoucherPresenter() {
        return redeemVoucherPresenter;
    }

    private void setViewDialog(RewardVoucher reward) {
        showDialogRedeem();
    }

    private void showDialogRedeem() {
        ImageUtils.loadImage(getContext(), reward.getImage128(), ContextCompat.getDrawable(getContext(), R.drawable.default_pic_rewards_tile_img), imageViewCompany);
        textViewMerchantName.setText(reward.getProvider().getName());
        textViewRewards.setText(reward.getName());
        String[] terms = getContext().getResources().getStringArray(R.array.terms_invalidate);
        String text = "<html><body style=\"padding: 10; margin: 0; font-size: 12px; color: #747474;\">" + getContext().getString(R.string.dr_txt_tc) + "<ul style=\"padding-left: 10;padding-right: 10; margin: 10;\">";
        for (String s : terms) {
            s = String.format(s, ((reward.getProvider().getName() != null && !reward.getProvider().getName().equalsIgnoreCase(""))) ? reward.getProvider().getName() : "");
            text += "<li>" + s + "</li>";
        }
        text += "</ul></body></html>";
        if (terms.length > 0) {
            webViewRedeem.loadData(text, "text/html", "utf-8");
        }
    }

    @Override
    public void setViewRedeem(String refcode) {
        dismissBaseProgressBar();
        Log.i(TAG, "setViewRedeem: refCode" + refcode);
        getRedeemVoucherPresenter().setRewardUser(refcode, reward);
    }

    public void showDialogSuccess(Redeem response) {
        getBaseActivity().dismissProgressBarDialog();
        RedeemDialogSuccessSecond redeemDialogSuccessSecond = new RedeemDialogSuccessSecond(getContext());
        redeemDialogSuccessSecond.setActivity(getBaseActivity());
        redeemDialogSuccessSecond.setRedeem(response);
        redeemDialogSuccessSecond.setReward(reward);
        redeemDialogSuccessSecond.show();
    }

}
