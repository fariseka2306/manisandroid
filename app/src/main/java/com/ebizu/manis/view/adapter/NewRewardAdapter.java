package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.helper.UtilManis;

import java.util.List;

/**
 * Created by halim_ebizu on 8/23/17.
 */

public class NewRewardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Integer> pictures;
    private List<String> categories;
    private List<String> categoriesId;
    private RewardCategoryListener rewardCategoryListener;

    public NewRewardAdapter(Context context, List<Integer> pictures, List<String> categories, List<String> categoriesId) {
        this.context = context;
        this.pictures = pictures;
        this.categories = categories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reward_category, parent, false);
        return new RewardCategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final Integer assetRewardCategory = pictures.get(position);
        final String assetCategory = categories.get(position);
        final String categoryId = categoriesId.get(position);

        RewardCategoryViewHolder holder = (RewardCategoryViewHolder) viewHolder;
        ImageUtils.loadImage(context, assetRewardCategory, ContextCompat.getDrawable(context, R.drawable.default_pic_store_logo), holder.imgItemCategory);
        int squarePx = UtilManis.getScreenWidth(context) / 3;
        float elevation = context.getResources().getDimension(R.dimen.elevation_category_view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            holder.cardView.setElevation(context.getResources().getDimension(R.dimen.elevation_category_view));
        holder.cardView.setLayoutParams(new CardView.LayoutParams(squarePx - (int) elevation, squarePx - (int) elevation));
        holder.tvCategory.setText(assetCategory);
        holder.rlCategoryItem.setOnClickListener(view -> rewardCategoryListener.onRewardCategoryListener(assetCategory, categoryId));
    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

    public void addCategories(List<Integer> pictures, List<String> categories, List<String> categoriesId) {
        this.pictures = pictures;
        this.categories = categories;
        this.categoriesId = categoriesId;
        notifyDataSetChanged();
    }

    public void clearReward() {
        if (this.pictures != null) this.pictures.clear();
        if (this.categories != null) this.categories.clear();
        if (this.categoriesId != null) this.categoriesId.clear();
        notifyDataSetChanged();
    }

    public void setRewardCategoryListener(RewardCategoryListener rewardCategoryListener) {
        this.rewardCategoryListener = rewardCategoryListener;
    }

    private class RewardCategoryViewHolder extends RecyclerView.ViewHolder {

        ImageView imgItemCategory;
        TextView tvCategory;
        RelativeLayout rlCategoryItem;
        CardView cardView;

        private RewardCategoryViewHolder(View itemView) {
            super(itemView);
            imgItemCategory = (ImageView) itemView.findViewById(R.id.img_item_category);
            tvCategory = (TextView) itemView.findViewById(R.id.tv_category);
            rlCategoryItem = (RelativeLayout) itemView.findViewById(R.id.rl_cateory_item);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }

    public interface RewardCategoryListener {
        void onRewardCategoryListener(String categoryName, String categoryId);
    }
}
