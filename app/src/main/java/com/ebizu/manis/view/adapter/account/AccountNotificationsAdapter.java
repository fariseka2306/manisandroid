package com.ebizu.manis.view.adapter.account;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import com.ebizu.manis.R;
import com.ebizu.manis.model.NotificationsSetting;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/24/2017.
 */

public class AccountNotificationsAdapter extends RecyclerView.Adapter<AccountNotificationsAdapter.ViewHolder> {

    private final String TAG = getClass().getSimpleName();

    private Context context;
    private List<NotificationsSetting> modelNotificationsSettings;
    private SwitchNotificationListener switchNotificationListener;

    public AccountNotificationsAdapter(Context context, List<NotificationsSetting> notificationsSettings) {
        this.context = context;
        this.modelNotificationsSettings = notificationsSettings;
    }

    public void setSwitchListener(SwitchNotificationListener switchNotificationListener) {
        this.switchNotificationListener = switchNotificationListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_item_list_notifications, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        NotificationsSetting notificationsSetting = this.modelNotificationsSettings.get(position);
        String title = notificationsSetting.getTitle();
        if (notificationsSetting.getTitle().equalsIgnoreCase("sms")) {
            title = context.getString(R.string.ns_txt_sms);
        } else if (notificationsSetting.getTitle().equalsIgnoreCase("push")) {
            title = context.getString(R.string.ns_txt_pn);
        } else if (notificationsSetting.getTitle().equalsIgnoreCase("email")) {
            title = context.getString(R.string.ns_txt_email);
        }
        viewHolder.switchNotifications.setText(title);
        viewHolder.switchNotifications.setChecked(notificationsSetting.getValue());
        viewHolder.switchNotifications.setOnCheckedChangeListener((buttonView, isChecked) -> {
            switchNotificationListener.onSwitch(notificationsSetting);
            notificationsSetting.setValue(isChecked);
        });
    }

    public List<NotificationsSetting> getModelNotificationsSettings() {
        return modelNotificationsSettings;
    }

    @Override
    public int getItemCount() {
        return modelNotificationsSettings.size();
    }

    public void addNotificationsList(List<NotificationsSetting> notificationsSettings) {
        modelNotificationsSettings.addAll(notificationsSettings);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.switch_notifications)
        Switch switchNotifications;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface SwitchNotificationListener {
        void onSwitch(NotificationsSetting notificationsSetting);
    }
}