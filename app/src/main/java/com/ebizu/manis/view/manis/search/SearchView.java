package com.ebizu.manis.view.manis.search;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.rx.RxHelper;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Ebizu-User on 03/08/2017.
 */

public class SearchView extends RelativeLayout {

    @BindView(R.id.text_input_search)
    EditText inputTextSearch;
    @BindView(R.id.image_view_search)
    ImageView imageViewSearch;
    @BindView(R.id.text_view_hint)
    TextView textViewHint;
    @BindView(R.id.close_search_reward)
    ImageView closeSearchReward;
    @BindView(R.id.image_view_clear)
    RelativeLayout imageViewClear;

    private String keywordChanged = "";
    private boolean isSearch = false;

    private OnTextChangedListener onTextChangedListener;
    private OnSearchListener onSearchListener;
    private OnDeleteListener onDeleteListener;
    private OnSearchClickListener onSearchClickListener;

    public SearchView(Context context) {
        super(context);
        createView(context);
    }

    public SearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public SearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SearchView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    private void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_search, null, false);
        addView(view, new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
        inputTextSearch.setImeOptions(EditorInfo.IME_ACTION_DONE);
        initializeListener();
    }

    private void initializeListener() {
        imageViewClear.setOnClickListener((view1 -> {
            inputTextSearch.setText("");
            checkDeleteListener();
        }));
        inputTextSearch.setOnKeyListener((view, i, keyEvent) -> {
            int keyCode = keyEvent.getKeyCode();
            if (!isSearch && keyCode == KeyEvent.KEYCODE_ENTER) {
                checkSearchListener();
                isSearch = true;
            } else {
                isSearch = false;
            }
            return false;
        });
        inputTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Do nothing because we don't use it.
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkBackspaceDeleteListener(charSequence.toString());
                updateViewIndicator(charSequence.toString());
                keywordChanged = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // Do nothing because we don't use it.
            }
        });
        RxHelper.setTextWatcherObservable(inputTextSearch)
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> textChangedListener(onTextChangedListener, s));
        inputTextSearch.setOnClickListener(view -> {
            if (null != onSearchClickListener)
                onSearchClickListener.onClick(view);
        });
    }

    public boolean isEmpty() {
        return inputTextSearch.getText().toString().isEmpty();
    }

    public void setInputText(String input) {
        inputTextSearch.setText(input);
    }

    public void clearInputText() {
        inputTextSearch.setText("");
    }

    public void updateViewIndicator(String s) {
        if (s.isEmpty()) {
            showIndicatorKeyword();
            imageViewClear.setVisibility(INVISIBLE);
        } else {
            hideIndicatorKeyword();
            imageViewClear.setVisibility(VISIBLE);
        }
    }

    private void showIndicatorKeyword() {
        imageViewSearch.setVisibility(VISIBLE);
        textViewHint.setVisibility(VISIBLE);
    }

    private void hideIndicatorKeyword() {
        imageViewSearch.setVisibility(GONE);
        textViewHint.setVisibility(GONE);
    }

    public void setHint(String text) {
        textViewHint.setText(text);
    }

    public void textChangedListener(OnTextChangedListener onTextChangedListener, String s) {
        checkTextChangedListener(s);
    }

    private void checkDeleteListener() {
        if (null != onDeleteListener)
            if (inputTextSearch.getText().length() >= 0) {
                keywordChanged = "";
                onDeleteListener.onDeleteAllText();
            } else {
                onDeleteListener.onDeleteText();
            }
    }

    private void checkBackspaceDeleteListener(String keyword) {
        if (null != onDeleteListener && (keywordChanged.length() == 1 && keyword.length() == 0)) {
            onDeleteListener.onDeleteAllText();
        }
    }

    private void checkTextChangedListener(String s) {
        if (null != onTextChangedListener)
            new Handler().postDelayed(() -> {
                if (s.length() > 2) {
                    onTextChangedListener.onValid(s);
                } else if (s.isEmpty()) {
                    onTextChangedListener.onEmptyText();
                } else {
                    onTextChangedListener.onInvalid();
                }
            }, 500);
    }

    private void checkSearchListener() {
        if (null != onSearchListener && !isEmpty())
            onSearchListener.onSearch(inputTextSearch.getText().toString());
    }

    public void setOnImeiSearch() {
        inputTextSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
    }

    public void setOnImeiDone() {
        inputTextSearch.setImeOptions(EditorInfo.IME_ACTION_DONE);
    }

    public void setOnDeleteListener(OnDeleteListener onDeleteListener) {
        this.onDeleteListener = onDeleteListener;
    }

    public void setOnSearchListener(OnSearchListener onSearchListener) {
        this.onSearchListener = onSearchListener;
    }


    public void setOnTextChange(OnTextChangedListener onTextChangedListener) {
        this.onTextChangedListener = onTextChangedListener;
    }

    public void setOnSearchClickListener(OnSearchClickListener onSearchClickListener) {
        this.onSearchClickListener = onSearchClickListener;
    }

    public interface OnTextChangedListener {
        void onValid(String keyword);

        void onInvalid();

        void onEmptyText();
    }

    public interface OnSearchListener {
        void onSearch(String keyword);
    }

    public interface OnDeleteListener {
        void onDeleteText();

        void onDeleteAllText();
    }

    public interface OnSearchClickListener {
        void onClick(View view);
    }
}
