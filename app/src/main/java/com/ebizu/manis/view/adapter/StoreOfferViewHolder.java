package com.ebizu.manis.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 8/22/17.
 */

public class StoreOfferViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.sdi_img_offer)
    ImageView sdiImgOffer;

    @BindView(R.id.sdi_img_merchant)
    ImageView sdiImgMerchant;

    @BindView(R.id.sdi_txt_title)
    TextView sdiTxtTitle;

    @BindView(R.id.sdi_txt_detail)
    TextView sdiTxtDetail;

    @BindView(R.id.sdi_txt_time)
    TextView sdiTxtTime;

    public StoreOfferViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
