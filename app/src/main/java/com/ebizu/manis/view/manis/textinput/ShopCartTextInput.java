package com.ebizu.manis.view.manis.textinput;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 9/28/17.
 */

public class ShopCartTextInput extends RelativeLayout {

    @BindView(R.id.text_view_title_input)
    TextView textViewTitleInput;
    @BindView(R.id.text_view_input)
    EditText inputText;
    @BindView(R.id.root_input_cart)
    ConstraintLayout rootLayout;

    public ShopCartTextInput(Context context) {
        super(context);
        createView(context);
    }

    public ShopCartTextInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context, attrs);
    }

    public ShopCartTextInput(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context, attrs);
    }

    protected void createView(Context context, AttributeSet attrs) {
        createView(context);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ShopCartTextInput, 0, 0);
        String titleText = a.getString(R.styleable.ShopCartTextInput_textShopTitle);
        int backgroundTextColor = a.getColor(R.styleable.ShopCartTextInput_backgroundTextInput,
                ContextCompat.getColor(context, R.color.black));
        int lineInput = a.getInt(R.styleable.ShopCartTextInput_lineShopInput, 1);
        int maxLength = a.getInt(R.styleable.ShopCartTextInput_maximalLength, 0);
        String regexInput = a.getString(R.styleable.ShopCartTextInput_regexInput);
        Drawable drawable = a.getDrawable(R.styleable.ShopCartTextInput_backgroundDrawable);
        if (drawable == null) drawable = ContextCompat.getDrawable(getContext(), R.drawable.bg_round_white);

        if (regexInput != null && !regexInput.isEmpty()) {
            if (maxLength > 0) setInputFilterWithLength(regexInput, maxLength);
            else setInputFilters(regexInput);
        }
        setTitle(titleText);
        setInputBackgroundDrawable(drawable);
        setInputTextColor(backgroundTextColor);
        setInputLine(lineInput);
        a.recycle();
        addDivider();
    }

    protected void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.text_input_shopping_cart, null, false);
        addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
        inputText.setRawInputType(InputType.TYPE_CLASS_TEXT);
    }

    public void setTitle(String title) {
        textViewTitleInput.setText(title);
    }

    public void setInput(String text) {
        inputText.setText(text);
    }

    public void setBackgroundViewColor(int color) {
        setBackgroundColor(color);
    }

    public void setInputBackground(int color) {
        inputText.setBackgroundColor(color);
    }

    public void setInputBackgroundDrawable(Drawable drawable) {
        inputText.setBackground(drawable);
    }

    public void setInputTextColor(int color) {
        inputText.setTextColor(color);
    }

    public String getText() {
        return inputText.getText().toString();
    }

    public String getType() {
        return textViewTitleInput.getText().toString();
    }

    public void setInputLine(int line) {
        inputText.setMaxLines(line);
        inputText.setMinLines(line);
    }

    public void setInputFilterWithLength(String regex, int maxLength) {
        inputText.setFilters(new InputFilter[]{getInputFilter(regex),
                new InputFilter.LengthFilter(maxLength)});
    }

    public void setInputType(int inputType) {
        inputText.setRawInputType(inputType);
    }

    public void setInputFilters(String regex) {
        inputText.setFilters(new InputFilter[]{getInputFilter(regex)});
    }

    public void setDoneImeAction() {
        inputText.setImeOptions(EditorInfo.IME_ACTION_DONE);
    }

    protected void addDivider() {
        View view = new View(getContext());
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                1
        );
        params.setMargins(0, (int) getResources().getDimension(R.dimen.activity_vertical_margin), 0, 0);
        view.setLayoutParams(params);
        view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        view.setAlpha((float) 0.4);

        rootLayout.addView(view);

        ConstraintSet set = new ConstraintSet();
        set.clone(rootLayout);
        set.connect(view.getId(), ConstraintSet.TOP, isConnectToTextView() ?
                textViewTitleInput.getId() : inputText.getId(), ConstraintSet.BOTTOM);
        set.applyTo(rootLayout);
    }

    private boolean isConnectToTextView() {
        int tvHeight = textViewTitleInput.getHeight();
        int etHeight = inputText.getHeight();
        return tvHeight > etHeight;
    }

    private InputFilter getInputFilter(String regex) {
        InputFilter filter;
        filter = (source, start, end, dest, dstart, dend) -> {
            if (source.toString().matches(regex)) {
                return source;
            } else {
                String exclude = regex.replace("[", "[^");
                return source.toString().replaceAll(exclude, "");
            }
        };
        return filter;
    }
}
