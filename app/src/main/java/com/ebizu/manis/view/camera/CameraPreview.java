package com.ebizu.manis.view.camera;

import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.permission.PermissionManager;
import com.ebizu.manis.root.BaseActivity;

import java.util.List;

/**
 * Created by Ebizu-User on 11/08/2017.
 */

public class CameraPreview extends SurfaceView
        implements ICameraPreview {

    private final String TAG = getClass().getSimpleName();
    private Camera camera;
    private SurfaceHolder surfaceHolder;
    private PreviewCallback previewCallback;
    private Camera.PictureCallback pictureCallback;
    private BaseActivity baseActivity;

    public CameraPreview(Context context) {
        super(context);
    }

    public CameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraPreview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CameraPreview(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void initialize() {
        instanceCamera();
        instanceSurfaceHolder();
    }

    @Override
    public Camera getCamera() {
        if (camera == null)
            instanceCamera();
        return camera;
    }

    @Override
    public void instanceCamera() {
        if (PermissionManager.getInstance(getBaseActivity()).checkPermissionCamera(true))
            try {
                camera = Camera.open(getCameraId());
            } catch (Exception e) {
                Log.e(TAG, "instanceCamera: ".concat(e.getMessage()));
                baseActivity.setResult(ConfigManager.Snap.CANT_ACCESS_CAMERA);
                baseActivity.finish();
            }
    }

    @Override
    public SurfaceHolder getSurfaceHolder() {
        if (surfaceHolder == null)
            instanceSurfaceHolder();
        return surfaceHolder;
    }

    @Override
    public void instanceSurfaceHolder() {
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (PermissionManager.getInstance(getBaseActivity()).checkPermissionCamera(false)) {
                    try {
                        if (null != camera) {
                            camera.setPreviewDisplay(surfaceHolder);
                            camera.setPreviewCallback((data, cameraPreview) ->
                                    previewCallback.onPreviewFrame(data, cameraPreview));
                        }
                    } catch (Exception e) {
                        getCamera().release();
                        camera = null;
                        Log.e(TAG, "surfaceCreated: ".concat(e.getMessage()));
                    }
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                if (PermissionManager.getInstance(getBaseActivity()).checkPermissionCamera(false)) {
                    if (camera == null)
                        instanceCamera();
                    camera.stopPreview();
                    Camera.Parameters cameraParams = camera.getParameters();

                    Camera.Size previewSize = CameraHelper.determinePreviewSize(true, width, height, getPreviewSizeList());
                    Camera.Size pictureSize = CameraHelper.determinePictureSize(previewSize, getPictureSizeList());
                    cameraParams.setPreviewSize(previewSize.width, previewSize.height);
                    cameraParams.setPictureSize(pictureSize.width, pictureSize.height);
                    CameraHelper.adjustSurfaceLayoutSize(previewSize, true, width, height, CameraPreview.this);
                    // Continue executing this method if this method is called recursively.
                    // Recursive call of surfaceChanged is very special case, which is a path from
                    // the catch clause at the end of this method.
                    // The later part of this method should be executed as well in the recursive
                    // invocation of this method, because the layout change made in this recursive
                    // call will not trigger another invocation of this method.
                    CameraHelper.configureCameraParameters(getContext(), camera, cameraParams);
                    startPreview(cameraParams);
                }
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                stop();
            }
        });
    }

    private void startPreview(Camera.Parameters cameraParams) {
        try {
            byte[] previewBuffer = new byte[
                    (cameraParams.getPreviewSize().height * cameraParams.getPreviewSize().width * 3) / 2];
            camera.addCallbackBuffer(previewBuffer);
            camera.setPreviewCallback((data, camera) -> previewCallback.onPreviewFrame(data, camera));
            camera.startPreview();
        } catch (Exception e) {
            Log.e(TAG, "startPreview: ".concat(e.getMessage()));
        }
    }

    @Override
    public List<Camera.Size> getPreviewSizeList() {
        return camera.getParameters().getSupportedPreviewSizes();
    }

    @Override
    public List<Camera.Size> getPictureSizeList() {
        return camera.getParameters().getSupportedPictureSizes();
    }

    @Override
    public void setOnPreview(PreviewCallback previewCallback) {
        this.previewCallback = previewCallback;
    }

    @Override
    public void setPictureCallback(Camera.PictureCallback pictureCallback) {
        this.pictureCallback = pictureCallback;
    }

    @Override
    public void takePicture() {
        camera.takePicture(null, null, pictureCallback);
    }

    @Override
    public void focus() {
        if (PermissionManager.getInstance(getBaseActivity()).checkPermissionCamera(false)) {
            if (camera == null)
                instanceCamera();
            camera.autoFocus((success, cameraFocus) -> cameraFocus.autoFocus(null));
        }
    }

    @Override
    public void pause() {
        if (PermissionManager.getInstance(getBaseActivity()).checkPermissionCamera(false)) {
            if (camera == null)
                instanceCamera();
            camera.stopPreview();
            camera.setPreviewCallback(null);
        }
    }

    @Override
    public void stop() {
        if (PermissionManager.getInstance(getBaseActivity()).checkPermissionCamera(false)) {
            if (camera == null)
                instanceCamera();
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
        }
    }

    @Override
    public void resume() {
        if (PermissionManager.getInstance(getBaseActivity()).checkPermissionCamera(false)) {
            if (camera == null)
                instanceCamera();
            camera.startPreview();
        }
    }

    public BaseActivity getBaseActivity() {
        return baseActivity;
    }

    public void setBaseActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
        initialize();
    }

    private int getCameraId() {
        int cameraId = 0;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                return i;
            }
        }
        return cameraId;
    }

    public boolean isReady() {
        return null != camera;
    }


}
