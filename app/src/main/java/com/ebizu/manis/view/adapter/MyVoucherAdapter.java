package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.MyVouchersView;
import com.ebizu.manis.sdk.entities.MyVoucher;
import com.ebizu.manis.view.holder.MyVoucherViewHolder;
import com.ebizu.manis.view.holder.ProgessBarHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ebizu on 9/18/17.
 */

public class MyVoucherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<RewardVoucher> rewards;
    public final int VIEW_TYPE_LOADING = 1;
    public final int VIEW_TYPE_ITEM = 0;
    private boolean isLoadingAdded = false;

    private MyVouchersView myVouchersView;
    private MyVoucherViewHolder.MyVoucherListener myVoucherListener;


    public MyVoucherAdapter(List<MyVoucher> rewards, MyVouchersView myVouchersView,
                            MyVoucherViewHolder.MyVoucherListener myVoucherListener) {
        this.rewards = new ArrayList<>();
        this.myVouchersView = myVouchersView;
        this.myVoucherListener = myVoucherListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case VIEW_TYPE_ITEM: {
                viewHolder = getViewHolder(parent,layoutInflater);
                break;
            }
            case VIEW_TYPE_LOADING: {
                return new ProgessBarHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_progress, parent, false));
            }
            default: {
                return null;
            }
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyVoucherViewHolder) {
            RewardVoucher reward = rewards.get(position);
            MyVoucherViewHolder myVoucherViewHolder = (MyVoucherViewHolder) holder;
            myVoucherViewHolder.setMyVoucherView(reward,myVouchersView);
        }
    }

    @Override
    public int getItemCount() {
        return rewards.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == rewards.size() - 1 && isLoadingAdded) ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public List<RewardVoucher> getRewards() {
        return rewards;
    }

    public void add(RewardVoucher r) {
        rewards.add(r);
    }

    public void addAll(List<RewardVoucher> moveResults) {
        for (RewardVoucher result : moveResults) {
            add(result);
        }
        notifyDataSetChanged();
    }

    public void addPurchasedVouchers(List<RewardVoucher> rewards) {
        this.rewards = rewards;
        notifyItemInserted(this.rewards.size() + 1);
    }

    public void replacePurchasedVouchers(List<RewardVoucher> rewards) {
        this.rewards = rewards;
        notifyDataSetChanged();
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new RewardVoucher());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = rewards.size() - 1;
        RewardVoucher result = getItem(position);

        if (result != null && result.getName().isEmpty()) {
            rewards.remove(position);
            notifyItemRemoved(position);
        }
    }

    public RewardVoucher getItem(int position) {
        return rewards.get(position);
    }

    public void showProgress() {
        this.rewards.add(null);
        notifyItemInserted(this.rewards.size());
    }

    public void dismissProgress() {
        this.rewards.removeAll(Collections.singleton(null));
        notifyItemRemoved(this.rewards.size());
    }

    public boolean isEmpty() {
        return rewards.size() < 1;
    }

    private RelativeLayout createViewProgress(Context context) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        ProgressBar progressBar = new ProgressBar(context);
        relativeLayout.addView(progressBar, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return relativeLayout;
    }

    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater layoutInflater){
        final RecyclerView.ViewHolder myVoucherGridViewHolder;
        View view = layoutInflater.inflate(R.layout.item_my_voucher, parent,false);
        myVoucherGridViewHolder = rewards.get(0) instanceof RewardVoucher ?
                new MyVoucherViewHolder(view,myVoucherListener):
                new MyVoucherViewHolder(view,myVoucherListener);
        return myVoucherGridViewHolder;
    }
}
