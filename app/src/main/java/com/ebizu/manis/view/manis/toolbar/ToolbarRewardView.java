package com.ebizu.manis.view.manis.toolbar;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.model.rewardbulk.RewardBulk;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.root.BaseView;
import com.ebizu.sdk.reward.models.Reward;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by halim_ebizu on 9/12/17.
 */

public class ToolbarRewardView extends BaseView implements IToolbarView {

    @BindView(R.id.img_store)
    ImageView imgStore;

    @BindView(R.id.txt_title)
    TextView txtTitle;

    @BindView(R.id.txt_detail)
    TextView txtDetail;

    public ToolbarRewardView(Context context) {
        super(context);
        createView(context);
    }

    public ToolbarRewardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ToolbarRewardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ToolbarRewardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.actionbar_back_reward, null, false);
        addView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    @Override
    public void performBackAnimation(Activity activity) {
        activity.finish();
    }

    @OnClick(R.id.rel_left)
    void onBackPressed() {
        try {
            AppCompatActivity appCompatActivity = (AppCompatActivity) getContext();
            appCompatActivity.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setToolbarView(Reward reward) {
        Glide.with(getContext())
                .load(reward.getProvider().getImage())
                .thumbnail(0.1f)
                .fitCenter()
                .placeholder(R.drawable.default_pic_store_logo)
                .animate(R.anim.fade_in_image)
                .into(imgStore);
        txtTitle.setText(reward.getName());
        txtDetail.setText(reward.getProvider().getName());
    }

    public void setToolbarView(RewardVoucher reward) {
        Glide.with(getContext())
                .load(reward.getProvider().getImage())
                .thumbnail(0.1f)
                .fitCenter()
                .placeholder(R.drawable.default_pic_store_logo)
                .animate(R.anim.fade_in_image)
                .into(imgStore);
        txtTitle.setText(reward.getName());
        txtDetail.setText(reward.getProvider().getName());
    }
}
