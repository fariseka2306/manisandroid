package com.ebizu.manis.view.dialog.snaptnc;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.snapdetail.viewimage.ViewImageSnap;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.requestbodyv2.data.ManisTncData;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.term.TermDialog;

import butterknife.OnClick;

/**
 * Created by ebizu on 10/12/17.
 */

public class SnapTncDialog extends BaseDialogManis {

    private TermDialog termDialog;

    @OnClick(R.id.button_tnc)
    void onClickTterms() {
        if (null == termDialog)
            termDialog = new TermDialog(getBaseActivity());
        termDialog.setIsManisLegal(false);
        termDialog.show();
        termDialog.setActivity(getBaseActivity());
        termDialog.getTermPresenter().loadTerm(setRequestBody(getContext()), ConfigManager.ManisLegal.TNC_TOU);
    }

    @OnClick(R.id.button_sample)
    void onClickSample() {
        getBaseActivity().startActivity(new Intent(getContext(), ViewImageSnap.class));
    }

    protected OnAcceptTncListener onAcceptTncListener;

    public SnapTncDialog(@NonNull Context context) {
        super(context);
    }

    public SnapTncDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected SnapTncDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public void setOnAcceptListener(OnAcceptTncListener onAcceptListener) {
        this.onAcceptTncListener = onAcceptListener;
    }

    public interface OnAcceptTncListener {
        void onAccept();
    }

    private RequestBody setRequestBody(Context context) {
        ManisTncData manisTncData = new ManisTncData();
        manisTncData.setType(ConfigManager.ManisLegal.TNC_TYPE_SNE);
        RequestBody requestBody = new RequestBodyBuilder(context)
                .setCommand(ConfigManager.ManisLegal.COMMAND_MANIS_LEGAL)
                .setFunction(ConfigManager.ManisLegal.FUNCTION_MANIS_LEGAL)
                .setData(manisTncData)
                .create();
        return requestBody;
    }
}
