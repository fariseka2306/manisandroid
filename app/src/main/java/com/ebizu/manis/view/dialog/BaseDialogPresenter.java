package com.ebizu.manis.view.dialog;

import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.ManisApiV2;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class BaseDialogPresenter implements IDialogPresenter {

    private BaseDialogManis baseDialogManis;
    private ManisApi manisApi;
    private ManisSession manisSession;
    private ManisApiV2 manisApiV2;

    @Override
    public void attachDialog(BaseDialogManis baseDialogManis) {
        this.baseDialogManis = baseDialogManis;
    }

    public ManisSession getManisSession() {
        if (null == manisSession)
            manisSession = new ManisSession(baseDialogManis.getContext());
        return manisSession;
    }

    public ManisApi getManisApi() {
        if (null == manisApi)
            if (getManisSession().isLoggedIn()) {
                manisApi = ManisApiGenerator.createServiceWithToken(baseDialogManis.getContext());
            } else {
                manisApi = ManisApiGenerator.createService(baseDialogManis.getContext());
            }
        return manisApi;
    }

    @Override
    public ManisApiV2 getManisApiV2() {
        if (null == manisApiV2)
            manisApiV2 = ManisApiGenerator.createServiceV2(baseDialogManis.getContext());
        return manisApiV2;
    }
}
