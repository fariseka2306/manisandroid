package com.ebizu.manis.view.dialog.review;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.ebizu.manis.R;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.review.reviewdialog.DialogReview;

/**
 * Created by mac on 8/23/17.
 */

public class ReviewPopUpDialog extends BaseDialogManis implements View.OnClickListener {

    private Button buttonYes;
    private Button buttonNo;

    public ReviewPopUpDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public ReviewPopUpDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected ReviewPopUpDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_review, null, false);
        getParent().addView(view);
        buttonYes = (Button) view.findViewById(R.id.button_yes);
        buttonNo = (Button) view.findViewById(R.id.button_no);
        buttonYes.setOnClickListener(this);
        buttonNo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(buttonYes)) {
            dismiss();
            ReviewHappyDialog reviewHappyDialog = new ReviewHappyDialog(getBaseActivity());
            reviewHappyDialog.show();
            reviewHappyDialog.setActivity(getBaseActivity());
        } else {
            dismiss();
            ReviewFeedbackDialog reviewFeedbackDialog = new ReviewFeedbackDialog(getBaseActivity());
            reviewFeedbackDialog.show();
            reviewFeedbackDialog.setActivity(getBaseActivity());
        }
    }
}
