package com.ebizu.manis.view.adapter.account;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.manager.accountlistmenu.AccountListMenu;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/6/2017.
 */

public class AccountListMenuAdapter extends RecyclerView.Adapter<AccountListMenuAdapter.ViewHolder> {

    Context mContext;
    Activity activity;
    private AccountMenuListener accountListner;
    private List<AccountListMenu> modelAccountList;

    public AccountListMenuAdapter(Context mContext, List<AccountListMenu> modelAccountList, Activity activity) {
        this.mContext = mContext;
        this.modelAccountList = modelAccountList;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_menulist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        AccountListMenu accountList = modelAccountList.get(position);
        viewHolder.icon.setImageResource(accountList.thumbnail());
        viewHolder.name.setText(accountList.name());
        viewHolder.parentAccount.setOnClickListener((v -> accountListner.onClick(accountList)));
    }

    public void addAccountList(AccountListMenu accountListMenu) {
        this.modelAccountList.add(accountListMenu);
        notifyDataSetChanged();
    }

    public void setAccountListener(AccountMenuListener accountListener) {
        this.accountListner = accountListener;
    }

    @Override
    public int getItemCount() {
        return modelAccountList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_name_list)
        TextView name;
        @BindView(R.id.image_view_icon_list)
        ImageView icon;
        @BindView(R.id.parent_account_list)
        LinearLayout parentAccount;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface AccountMenuListener {
        void onClick(AccountListMenu accountList);
    }
}
