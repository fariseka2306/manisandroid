package com.ebizu.manis.view.walkthrough.walkthroughpage;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mac on 8/29/17.
 */

public class WalkthroughPageThree extends WalkthroughPageView {

    @BindView(R.id.parent)
    RelativeLayout relativeLayoutParent;
    @BindView(R.id.edittext_amount)
    EditText editTextAmount;
    @BindView(R.id.lin_startsnap)
    LinearLayout linearLayoutStartSnapButton;

    @BindView(R.id.walkthrough_fifth_page)
    RelativeLayout walkthroughFifthPage;
    @BindView(R.id.imageview_button_ok)
    ImageView imageViewButtonOk;

    @BindView(R.id.walkthrough_sixth_page)
    LinearLayout walkthroughSixthPage;
    @BindView(R.id.selected_input)
    View viewSelectedInput;

    @BindView(R.id.walkthrough_seventh_page)
    RelativeLayout walkthroughSeventhPage;
    @BindView(R.id.selected_snap)
    View viewSelectedSnap;


    public WalkthroughPageThree(Context context) {
        super(context);
        createView(context);
    }

    public WalkthroughPageThree(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public WalkthroughPageThree(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WalkthroughPageThree(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    protected void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.activity_walkthrough_page_three, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
    }

    @Override
    public int page() {
        return 3;
    }

    @Override
    public void startPage() {
        super.startPage();
        startAnimeSlideRight(this);
        relativeLayoutParent.setVisibility(VISIBLE);
        new Handler().postDelayed(() -> walkthroughFifthPage.setVisibility(VISIBLE), 1000);
    }

    @Override
    public void finishPage() {
        super.finishPage();
        startAnimSlideOutLeft(this);
        onNextPageListener.onNextPage(page());
    }

    @Override
    public void setOnNextPageListener(OnNextPageListener onNextPageListener) {
        super.setOnNextPageListener(onNextPageListener);
        this.onNextPageListener = onNextPageListener;
        imageViewButtonOk.setOnClickListener(v -> {
            walkthroughFifthPage.setVisibility(GONE);
            walkthroughSixthPage.setVisibility(VISIBLE);
        });
        viewSelectedInput.setOnClickListener(v -> {
            walkthroughSixthPage.setVisibility(GONE);
            editTextAmount.setText("20000");
            new Handler().postDelayed(() -> walkthroughSeventhPage.setVisibility(VISIBLE), 1000);
        });
        viewSelectedSnap.setOnClickListener(v -> {
            walkthroughSeventhPage.setVisibility(GONE);
            finishPage();
        });
    }
}
