package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.model.Reward;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.sdk.entities.MyVoucher;
import com.ebizu.manis.view.holder.MyVoucherViewHolder;
import com.ebizu.manis.view.holder.ProgessBarHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Raden on 11/13/17.
 */

public class NewMyVoucherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Reward> rewards;
    public final int VIEW_TYPE_LOADING = 0;
    public final int VIEW_TYPE_ITEM = 1;

    private Context context;

    public NewMyVoucherAdapter(Context context, List<MyVoucher> rewards) {
        this.context = context;
        this.rewards = new ArrayList<>();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
//        switch (viewType) {
//            case VIEW_TYPE_LOADING: {
//                return new ProgessBarHolder(createViewProgress(context));
//            }
//            case VIEW_TYPE_ITEM: {
//                View view = LayoutInflater.from(context).inflate(R.layout.item_my_voucher, parent, false);
////                return new MyVoucherViewHolder(view);
//            }
//            default: {
//                View view = LayoutInflater.from(context).inflate(R.layout.item_my_voucher, parent, false);
////                return new MyVoucherViewHolder(view);
//            }
//        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyVoucherViewHolder) {
            Reward reward = rewards.get(position);
            MyVoucherViewHolder myVoucherViewHolder = (MyVoucherViewHolder) holder;
//            myVoucherViewHolder.setMyVoucherView(baseActivity, reward);
        }
    }

    @Override
    public int getItemCount() {
        return rewards.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (null == rewards.get(position)) {
            return VIEW_TYPE_LOADING;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    public void add(Reward r) {
        rewards.add(r);
        notifyItemInserted(rewards.size() - 1);
    }

    public void addAll(List<Reward> moveResults) {
        for (Reward result : moveResults) {
            add(result);
        }
    }

    public void addPurchasedVouchers(List<Reward> rewards) {
        this.rewards = rewards;
        notifyItemInserted(this.rewards.size() + 1);
    }

    public void replacePurchasedVouchers(List<Reward> rewards) {
        this.rewards = rewards;
        notifyDataSetChanged();
    }

    public void showProgress() {
        this.rewards.add(null);
        notifyItemInserted(this.rewards.size());
    }

    public void dismissProgress() {
        this.rewards.removeAll(Collections.singleton(null));
        notifyItemRemoved(this.rewards.size());
    }

    public boolean isEmpty() {
        return rewards.size() < 1;
    }

    private RelativeLayout createViewProgress(Context context) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        ProgressBar progressBar = new ProgressBar(context);
        relativeLayout.addView(progressBar, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return relativeLayout;
    }
}
