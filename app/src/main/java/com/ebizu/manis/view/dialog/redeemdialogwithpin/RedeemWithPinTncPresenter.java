package com.ebizu.manis.view.dialog.redeemdialogwithpin;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.service.manis.response.WrapperBrandTerms;
import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.RedeemPinTncBody;
import com.ebizu.manis.service.reward.RewardApi;
import com.ebizu.manis.service.reward.RewardApiGenerator;
import com.ebizu.manis.service.reward.requestbody.require.RewardApiBody;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by FARIS_mac on 10/4/17.
 */

public class RedeemWithPinTncPresenter extends BaseDialogPresenter implements IRedeemWithPinTncPresenter {

    private Context context;
    private RedeemWithPinTncDialog redeemWithPinTncDialog;
    private RedeemWithPinDialog redeemWithPinDialog;
    private RewardApi rewardApi;

    @Override
    public void attachDialog(BaseDialogManis baseDialogManis) {
        super.attachDialog(baseDialogManis);
        redeemWithPinTncDialog = (RedeemWithPinTncDialog) baseDialogManis;
    }

    public RedeemWithPinTncPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void loadBrandTerm(RedeemPinTncBody redeemPinTncBody) {
        if (rewardApi == null)
            rewardApi = RewardApiGenerator.createService(context);
        rewardApi.getBrandTerms(redeemPinTncBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperBrandTerms>(redeemWithPinTncDialog) {
                    @Override
                    public void onNext(WrapperBrandTerms wrapperBrandTerms) {
                        super.onNext(wrapperBrandTerms);
                        if (wrapperBrandTerms.getErrorCode() == RewardApiConfig.ResponseCode.SESSION_EMPTY ||
                                wrapperBrandTerms.getErrorCode() == RewardApiConfig.ResponseCode.SESSION_INVALID) {
                            redeemWithPinTncDialog.setViewReddemWithPinInvalid(context.getString(R.string.error_reward_session));
                        }  else {
                            redeemWithPinTncDialog.setViewRedeemWithPin(wrapperBrandTerms.getBrandTerms().getTerms());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                });
    }
}
