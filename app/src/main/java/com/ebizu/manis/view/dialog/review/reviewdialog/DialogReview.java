package com.ebizu.manis.view.dialog.review.reviewdialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.review.SendReviewPresenter;
import com.ebizu.manis.view.manis.nointernetconnection.NoInternetConnectionView;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class DialogReview extends BaseDialogManis {

    private BaseActivity baseActivity;
    protected SendReviewPresenter baseDialogPresenter;
    private ConstraintLayout noInternetConnectionView;
    private ProgressBar progressBar;
    private RelativeLayout parent;

    public RelativeLayout getParent() {
        return parent;
    }

    public DialogReview(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public DialogReview(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected DialogReview(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_manis, null, false);
        addContentView(view, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        parent = (RelativeLayout) view.findViewById(R.id.parent);
    }

    @Override
    public void showNoInternetConnection() {
        if (noInternetConnectionView == null)
            addViewInternetConnection();
        if (!noInternetConnectionView.isShown())
            noInternetConnectionView.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissNoInternetConnection() {
        if (noInternetConnectionView != null)
            noInternetConnectionView.setVisibility(View.GONE);
    }

    @Override
    public void showBaseProgressBar() {
        if (progressBar == null)
            addViewProgressBar();
        if (!progressBar.isShown()) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void dismissBaseProgressBar() {
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onRetry() {
        noInternetConnectionView.setVisibility(View.GONE);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    @Override
    public BaseActivity getBaseActivity() {
        return baseActivity;
    }

    private void addViewProgressBar() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.progress_bar, null, false);
        parent.addView(view, getParent().getLayoutParams());
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
    }

    private void addViewInternetConnection() {
        NoInternetConnectionView view = new NoInternetConnectionView(getContext());
        parent.addView(view, getParent().getLayoutParams());
        noInternetConnectionView = (ConstraintLayout) parent.findViewById(R.id.no_network_view);
        centerOnViewGroup(this.noInternetConnectionView);
        noInternetConnectionView.setVisibility(View.GONE);
        noInternetConnectionView.setOnClickListener(v -> onRetry());
    }

    private void centerOnViewGroup(View view) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        view.setLayoutParams(layoutParams);
    }
}