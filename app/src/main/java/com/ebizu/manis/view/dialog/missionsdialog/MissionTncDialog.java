package com.ebizu.manis.view.dialog.missionsdialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.manager.tracker.TrackerConstant;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.missions.Missions;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class MissionTncDialog extends BaseDialogManis {

    private Missions missions;

    private TextView textViewTitle;
    private WebView webViewTerm;
    private Button buttonClose;

    public void setMissions(Missions missions) {
        this.missions = missions;
        setViewTerm(missions);
    }

    public MissionTncDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public MissionTncDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected MissionTncDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_term, null, false);
        getParent().addView(view);
        textViewTitle = (TextView) view.findViewById(R.id.text_view_title);
        webViewTerm = (WebView) view.findViewById(R.id.web_view_terms);
        buttonClose = (Button) view.findViewById(R.id.button_close);
        webViewTerm.setPadding(16, 16, 16, 16);
        buttonClose.setOnClickListener(v -> dismiss());
        setWebViewSetting();
    }

    public void setViewTerm(Missions missions) {
        dismissBaseProgressBar();
        try {
            textViewTitle.setVisibility(View.GONE);
            webViewTerm.loadData(URLEncoder.encode(missions.getTnc(), "utf-8").replaceAll("\\+", "%20"), "text/html", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void setWebViewSetting() {
        final WebSettings webSettings = webViewTerm.getSettings();
        webSettings.setDefaultFontSize(12);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setAppCacheEnabled(false);
        webSettings.setBlockNetworkImage(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setGeolocationEnabled(false);
        webSettings.setNeedInitialFocus(false);
        webSettings.setSaveFormData(false);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        TrackerManager.getInstance()
                .setTrackerData(new TrackerStandartRequest(
                        TrackerConstant.kTrackerPageMissionTandC, TrackerConstant.kTrackerOriginPageMissions, "", "", "", "")
                );
    }
}