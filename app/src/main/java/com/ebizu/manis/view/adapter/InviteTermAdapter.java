package com.ebizu.manis.view.adapter;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.view.holder.InviteTermHolder;

import java.util.List;

/**
 * Created by ebizu on 8/16/17.
 */

public class InviteTermAdapter extends ListAdapter<String, InviteTermHolder> {

    public InviteTermAdapter(Context context, List<String> arrayData) {
        super(R.layout.item_dialog_invite_term, context, arrayData);
    }

}
