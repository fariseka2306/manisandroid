package com.ebizu.manis.view.manis.notification;

import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;
import com.ebizu.manis.model.notification.beacon.BeaconPromo;

/**
 * Created by ebizu on 8/31/17.
 */

public abstract class LocalNotification {

    protected Context context;
    protected NotificationCompat.Builder notification;
    protected NotificationManager notificationManager;

    public LocalNotification(Context context) {
        this.context = context;
        initializeNotification();
        initializeNotificationManager();
    }

    private void initializeNotification() {
        notification = new NotificationCompat.Builder(context);
        notification.setSmallIcon(R.drawable.ic_launcher);
        notification.setAutoCancel(true);
        notification.setColor(ContextCompat.getColor(context, R.color.base_pink));
        notification.setGroup("MANIS");
        notification.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        notification.setContentText("Manis");
    }

    private void initializeNotificationManager() {
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private NotificationCompat.BigPictureStyle getBigPictureStyle(BeaconPromo beaconPromo) {
        NotificationCompat.BigPictureStyle notifStyle = new NotificationCompat.BigPictureStyle();
        notifStyle.setBigContentTitle(beaconPromo.getName());
        notifStyle.setSummaryText(beaconPromo.getCompany());
        return notifStyle;
    }

}