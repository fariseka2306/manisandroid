package com.ebizu.manis.view.manis.notification;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;

import com.ebizu.manis.R;
import com.ebizu.manis.database.litepal.NotificationDatabase;
import com.ebizu.manis.model.notification.snap.SnapHistory;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.mvp.main.toolbar.ToolbarMainMainView;
import com.ebizu.manis.mvp.viewhistory.SnapViewHistoryActivity;

public class GCMNotification extends LocalNotification {

    public GCMNotification(Context context) {
        super(context);
    }

    public void showSnapNotification(SnapHistory snapHistory) {
        Intent intent = new Intent(context, SnapViewHistoryActivity.class);
        Intent backIntent = new Intent(context, MainActivity.class);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivities(context, (int) snapHistory.getId(),
                new Intent[]{backIntent, intent}, PendingIntent.FLAG_UPDATE_CURRENT);

        notification.setContentTitle(context.getResources().getString(R.string.app_name));
        notification.setContentText(snapHistory.getMessage());
        notification.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notif_new));
        notification.setContentIntent(contentIntent);

        notificationManager.notify((int) snapHistory.getId(), notification.build());
        NotificationDatabase.getInstance().insert(snapHistory);
        loadNotifUnread();
    }


    private void loadNotifUnread() {
        if (context instanceof MainActivity)
            try {
                MainActivity mainActivity = ((MainActivity) context);
                ToolbarMainMainView toolbarMainMainView = (ToolbarMainMainView) mainActivity.findViewById(R.id.toolbar_main);
                toolbarMainMainView.getToolbarPresenter().loadTotalNotifUnread();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}