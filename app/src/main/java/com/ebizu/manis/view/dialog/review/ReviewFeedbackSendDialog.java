package com.ebizu.manis.view.dialog.review;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import io.intercom.android.sdk.Intercom;

/**
 * Created by mac on 8/23/17.
 */

public class ReviewFeedbackSendDialog extends BaseDialogManis implements View.OnClickListener {

    private Button buttonOK;

    public ReviewFeedbackSendDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public ReviewFeedbackSendDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected ReviewFeedbackSendDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_review_feedback_send, null, false);
        getParent().addView(view);
        buttonOK = (Button) view.findViewById(R.id.button_ok);
        buttonOK.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
        Intercom.client().displayConversationsList();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        new AnalyticManager(getContext()).trackEvent(
                ConfigManager.Analytic.Category.DIALOG_REVIEW,
                ConfigManager.Analytic.Action.CLICK,
                "Button Yes"
        );
    }
}
