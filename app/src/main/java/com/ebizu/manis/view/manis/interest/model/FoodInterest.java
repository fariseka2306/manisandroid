package com.ebizu.manis.view.manis.interest.model;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public class FoodInterest implements InterestModelView {

    @Override
    public int id() {
        return 1;
    }

    @Override
    public Drawable drawable(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.interest_selection_food);
    }

    @Override
    public int color(Context context) {
        return ContextCompat.getColor(context, R.color.interest_food);
    }

}