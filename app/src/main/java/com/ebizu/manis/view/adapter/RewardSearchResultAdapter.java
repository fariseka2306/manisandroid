package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.sdk.utils.ImageUtil;
import com.ebizu.manis.view.holder.RewardGridViewHolder;
import com.ebizu.manis.view.holder.RewardSearchResultViewHolder;
import com.ebizu.sdk.reward.models.Reward;

import java.util.List;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public class RewardSearchResultAdapter extends RecyclerView.Adapter<RewardSearchResultViewHolder> {

    private Context context;
    private List<Reward> rewards;
    private RewardSearchResultListener rewardSearchResultListener;

    public RewardSearchResultAdapter(Context context, List<Reward> rewards) {
        this.context = context;
        this.rewards = rewards;
    }

    @Override
    public RewardSearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reward_search_result, parent, false);
        return new RewardSearchResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RewardSearchResultViewHolder holder, int position) {
        Reward reward = rewards.get(position);
        holder.tvRewardName.setText(reward.getName());
        holder.tvProviderName.setText(reward.getProvider().getName());
        int dataValue = reward.getPoint();
        holder.tvPoint.setText((dataValue == 0) ? context.getString(R.string.rd_txt_free) : dataValue + " " + context.getString(R.string.rd_txt_pts));
        if (reward.isHidePrice())
            holder.tvRewardValue.setVisibility(View.GONE);
        else
            holder.tvRewardValue.setText(reward.getCurrency() + " " + reward.getValue());
        if (reward.getStock() < 1)
            holder.tvOutOfStock.setVisibility(View.VISIBLE);
        else
            holder.tvOutOfStock.setVisibility(View.GONE);
        ImageUtils.loadImage(context, reward.getLargeImage(), ContextCompat.getDrawable(context, R.drawable.default_pic_promo_details_pic_small), holder.ivReward);
        holder.itemView.setOnClickListener(view -> rewardSearchResultListener.onRewardSearchResultListener(reward));
    }

    @Override
    public int getItemCount() {
        return rewards.size();
    }

    public void setOnRewardSearchResultListener(RewardSearchResultListener rewardGridListener) {
        this.rewardSearchResultListener = rewardGridListener;
    }

    public interface RewardSearchResultListener {
        void onRewardSearchResultListener(Reward reward);
    }
}
