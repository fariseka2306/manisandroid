package com.ebizu.manis.view.dialog.redemptionhistorydetail;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.customtabs.CustomTabsIntent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ClipBoard_Utils;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.RedemptionHistoryResult;
import com.ebizu.manis.root.ManisApplication;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by FARIS_mac on 9/8/17.
 */

public class RedemptionHistoryDetailDialog extends BaseDialogManis implements IRedemptionHistoryDetailDialog, View.OnClickListener {

    private ImageView imageViewMerchant;
    private TextView textViewTime;
    private TextView textViewDate;
    private TextView textViewTitle;
    private TextView textViewMerchant;
    private TextView textViewType;
    private TextView textViewCopy;
    private TextView textViewMerchantLocation;
    private TextView textViewExpired;
    private TextView textViewSN;
    private LinearLayout linVoucherCode;
    private Button buttonClose;

    RedemptionHistoryResult redemptionHistoryResult;

    public void setRedemptionHistoryResult(RedemptionHistoryResult redemptionHistoryResult) {
        this.redemptionHistoryResult = redemptionHistoryResult;
        setRedemptionHistoryDetail(redemptionHistoryResult);
    }

    public RedemptionHistoryDetailDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public RedemptionHistoryDetailDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected RedemptionHistoryDetailDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_redemption_history_detail, null, false);
        getParent().addView(view, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        imageViewMerchant = (ImageView) findViewById(R.id.imageview_merchant);
        textViewTime = (TextView) findViewById(R.id.textview_time);
        textViewDate = (TextView) findViewById(R.id.textview_date);
        textViewTitle = (TextView) findViewById(R.id.textview_title);
        textViewMerchant = (TextView) findViewById(R.id.textview_merchant);
        textViewType = (TextView) findViewById(R.id.textview_type);
        textViewCopy = (TextView) findViewById(R.id.textview_copy);
        textViewMerchantLocation = (TextView) findViewById(R.id.textview_location);
        textViewExpired = (TextView) findViewById(R.id.textview_expired);
        textViewSN = (TextView) findViewById(R.id.textview_sn);
        linVoucherCode = (LinearLayout) findViewById(R.id.ly_voucher_code);
        buttonClose = (Button) findViewById(R.id.button_close);
        buttonClose.setOnClickListener((v -> dismiss()));
        textViewCopy.setOnClickListener(this);
    }

    @Override
    public void setRedemptionHistoryDetail(RedemptionHistoryResult redemptionHistoryResult) {
        Glide.with(getContext())
                .load(redemptionHistoryResult.getAssets().getLogo())
                .thumbnail(0.1f)
                .fitCenter()
                .animate(R.anim.fade_in_image)
                .placeholder(R.drawable.default_pic_promo_details_pic_large)
                .into(imageViewMerchant);

        SimpleDateFormat timeFomat = new SimpleDateFormat("hh:mm a");
        Date dateTime = new Date(redemptionHistoryResult.getRedeemed() * 1000L);
        textViewTime.setText(timeFomat.format(dateTime));

        SimpleDateFormat dateFomat = new SimpleDateFormat("dd MMM yyyy");
        Date date = new Date(redemptionHistoryResult.getRedeemed() * 1000L);
        textViewDate.setText(dateFomat.format(date));

        String delims = "[@]";
        String[] parse = redemptionHistoryResult.getStore().split(delims);

        textViewMerchant.setText(parse[0].trim());
        if (parse.length == 2) {
            textViewMerchantLocation.setText("@" + parse[1].trim());
        } else {
            textViewMerchantLocation.setText("");
        }

        textViewTitle.setText(redemptionHistoryResult.getName());
        textViewType.setText(redemptionHistoryResult.getRewardType());

        Date dateExpired = new Date(redemptionHistoryResult.getExpired() * 1000L);
        textViewExpired.setText(dateFomat.format(dateExpired));

        textViewSN.setText(redemptionHistoryResult.getCode());
        if (redemptionHistoryResult.getRewardType().equals(UtilStatic.REWARD_INPUT_TYPE_ETU)) {
            linVoucherCode.setVisibility(View.GONE);
        } else if (redemptionHistoryResult.getRewardType().equals(UtilStatic.REWARD_INPUT_TYPE_THIRDPARTY)) {
            linVoucherCode.setVisibility(View.VISIBLE);
            textViewCopy.setText(R.string.rwd_point_dtl_use_it);
        } else {
            linVoucherCode.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textViewCopy)) {
            String[] urlTrackLink = redemptionHistoryResult.getTrackLink().split(",");
            ClipData clip = ClipData.newPlainText(getContext().getString(R.string.rwd_point_dtl_redeemption_code), redemptionHistoryResult.getCode());
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder.build();

            if (redemptionHistoryResult.getRewardType().equalsIgnoreCase("Third Party Reward")) {
                //validate URL
//                clip = ClipData.newPlainText(getContext().getString(R.string.rwd_point_dtl_redeemption_code), urlTrackLink[1]);
                if (TextUtils.isEmpty(redemptionHistoryResult.getTrackLink())) {
                    Toast.makeText(getContext(), getContext().getString(R.string.rwd_point_dtl_empty_url), Toast.LENGTH_SHORT).show();
                } else {
                    if (URLUtil.isValidUrl(redemptionHistoryResult.getTrackLink())) {
                        customTabsIntent.launchUrl(getContext(), Uri.parse(urlTrackLink[0]));
                    } else {
                        Toast.makeText(getContext(), getContext().getString(R.string.rwd_point_dtl_invalid_url), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            ClipBoard_Utils.copyToClipboard(getContext(), redemptionHistoryResult.getCode());
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        new AnalyticManager(getContext()).trackEvent(
                ConfigManager.Analytic.Category.DIALOG_REDEMPTION_HISTORY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Close"
        );
    }
}
