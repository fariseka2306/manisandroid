package com.ebizu.manis.view.dialog.receipt;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.mvp.luckydraw.luckydrawsubmit.LuckyDrawSubmitDialog;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ebizu on 8/25/17.
 */

public class ReceiptUploadDialog extends BaseDialogManis {

    @BindView(R.id.image_view_receipt)
    RoundedImageView imageViewReceipt;

    private OnBackPressedListener onBackPressedListener;

    public ReceiptUploadDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public ReceiptUploadDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected ReceiptUploadDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_success_upload, null, false);
        getParent().addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    public void setBitmapReceipt(Bitmap bitmapReceipt) {
        imageViewReceipt.setImageBitmap(bitmapReceipt);
    }

    public void setOnBackpressedListener(OnBackPressedListener onBackpressedListener) {
        this.onBackPressedListener = onBackpressedListener;
    }

    @OnClick(R.id.button_ok)
    void onClickOk() {
        if (null != onBackPressedListener)
            onBackPressedListener.onBackPressed();
    }

    @OnClick(R.id.button_get_points)
    void onClickGetMorePoints() {
        if (null != onBackPressedListener)
            onBackPressedListener.onMorePoints();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (null != onBackPressedListener)
            onBackPressedListener.onBackPressed();
    }

    public interface OnBackPressedListener {
        void onBackPressed();

        void onMorePoints();
    }
}