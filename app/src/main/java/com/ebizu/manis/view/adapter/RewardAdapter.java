package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ebizu.manis.R;
import com.ebizu.manis.mvp.reward.newreward.NewRewardFragment;
import com.ebizu.sdk.reward.models.Reward;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Raden on 7/8/17.
 */

public class RewardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = RewardAdapter.class.getSimpleName();

    private final int VIEW_ITEM = 1;
    private Context context;
    private ArrayList<Reward> rewards;
    private RewardAdapter.onclickListener onclickListener;

    public RewardAdapter(Context context, ArrayList<Reward> rewards) {
        this.context = context;
        this.rewards = rewards;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reward_home, parent, false);
        vh = new RedeemHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Reward reward = rewards.get(position);
        RedeemHolder redeemHolder = (RedeemHolder) holder;

        Glide.with(context)
                .load(reward.getImageLarge())
                .asBitmap()
                .thumbnail(0.1f)
                .animate(R.anim.fade_in_image)
                .placeholder(R.drawable.store_noimage)
                .centerCrop()
                .into(new BitmapImageViewTarget(redeemHolder.redeemPosterImg) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        redeemHolder.redeemPosterImg.setImageDrawable(circularBitmapDrawable);
                    }
                });

        redeemHolder.txtPointRewardBrand.setVisibility(View.VISIBLE);
        redeemHolder.txtPointRewardBrand.setText(String.valueOf(reward.getPoint()));

        redeemHolder.relReward.setOnClickListener(v -> onclickListener.onRewardClickListener(reward));
    }

    @Override
    public int getItemCount() {
        return rewards.size();
    }

    public Object getItem(int position) {
        return rewards.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_ITEM;
    }

    public void addReward(ArrayList<Reward> rewards) {
        this.rewards = rewards;
        notifyDataSetChanged();
    }

    public void setRewardOnClick(RewardAdapter.onclickListener onclickListener){
        this.onclickListener = onclickListener;
    }

    public class RedeemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgViewBrandReward)
        CircleImageView redeemPosterImg;
        @BindView(R.id.txtPointReward)
        TextView txtPointRewardBrand;
        @BindView(R.id.rel_brand)
        RelativeLayout relReward;

        public RedeemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface onclickListener{
        void onRewardClickListener (Reward reward);
    }
}
