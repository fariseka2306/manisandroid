package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.manager.multiplier.MultiplierMenuManager;
import com.ebizu.manis.model.Store;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 31/07/17.
 */

public class StoreCategoryDetailViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.parent)
    ConstraintLayout parent;

    @BindView(R.id.image_view_store)
    ImageView imageViewStore;

    @BindView(R.id.text_view_distance)
    TextView textViewDistance;

    @BindView(R.id.text_view_name_store)
    TextView textViewStore;

    @BindView(R.id.text_view_place)
    TextView textViewPlace;

    @BindView(R.id.text_view_category)
    TextView textViewCategory;

    @BindView(R.id.text_view_multiplier)
    TextView textViewMultiplier;

    private Context context;
    private Store store;

    public StoreCategoryDetailViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        context = itemView.getContext();
    }

    public void setStore(Store store) {
        this.store = store;
        ImageUtils.loadImage(context, store.getAssets().getPhoto(),
                ContextCompat.getDrawable(context, R.drawable.default_pic_store_logo), imageViewStore);

        String[] splitStoreName = store.getName().split("@");
        String storeName = splitStoreName[0];
        String storeAddress = "";
        if (splitStoreName.length > 1) {
            storeAddress = "@" + splitStoreName[1];
        }
        textViewStore.setText(storeName);
        textViewPlace.setText(storeAddress);
        textViewCategory.setText(store.getCategory().getName());
        textViewDistance.setText(distanceToString(store.getCoordinate().getDistance()));
        setMultiplierView(store);
    }

    private void setMultiplierView(Store store) {
        if (store.getMultiplier() != -1) {
            Drawable multiplierDraw = ContextCompat.getDrawable(context,
                    MultiplierMenuManager.getDrawble(store.getMultiplier(), store.getMerchantTier()));
            textViewMultiplier.setText(MultiplierMenuManager.getText(store.getMultiplier(), store.getMerchantTier()));
            textViewMultiplier.setBackground(multiplierDraw);
            textViewMultiplier.setVisibility(View.VISIBLE);
        }
    }

    private String distanceToString(Double distance) {
        DecimalFormat df = new DecimalFormat("0.#");
        if (distance >= 1100) {
            return df.format(distance / 1000) + " km";
        } else {
            return df.format(distance) + " m";
        }
    }

}
