package com.ebizu.manis.view.dialog.redeemdialogwithpin;

import com.ebizu.manis.view.dialog.IBaseDialog;

/**
 * Created by FARIS_mac on 10/5/17.
 */

public interface IRedeemWithPinTncDialog extends IBaseDialog {

    void setViewRedeemWithPin(String terms);

    void setViewReddemWithPinInvalid(String message);

}
