package com.ebizu.manis.view.manis.toolbar;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ebizu on 10/3/17.
 */

public class ToolbarOrderSummary extends BaseView {

    public ToolbarOrderSummary(Context context) {
        super(context);
        createView(context);
    }

    public ToolbarOrderSummary(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ToolbarOrderSummary(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ToolbarOrderSummary(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.toolbar_order_summary, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.button_back)
    void onClickBack() {
        getBaseActivity().finish();
    }
}
