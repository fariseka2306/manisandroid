package com.ebizu.manis.view.dialog.review;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.response.ResponseData;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by mac on 8/23/17.
 */

public class GetReviewPresenter implements IReviewStatus {

    private final String TAG = getClass().getSimpleName();

    private Context context;
    private ManisApi manisApi;

    public GetReviewPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void getReviewStatus(MainActivity mainActivity) {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceWithToken(context);
        manisApi.getReviewStatus()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<ResponseData>(mainActivity) {
                    @Override
                    public void onNext(ResponseData responseData) {
                        super.onNext(responseData);
                        mainActivity.setReview(responseData);
                        Log.i(TAG, "sendReview: " + responseData.getSuccess().toString());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                    }
                });
    }
}
