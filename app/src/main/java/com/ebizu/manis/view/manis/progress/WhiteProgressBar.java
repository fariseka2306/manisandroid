package com.ebizu.manis.view.manis.progress;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.ebizu.manis.R;

/**
 * Created by ebizu on 10/17/17.
 */

public class WhiteProgressBar extends ProgressBar {

    public WhiteProgressBar(Context context) {
        super(context);
        customBar(context);
    }

    public WhiteProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        customBar(context);
    }

    public WhiteProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        customBar(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WhiteProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        customBar(context);
    }

    private void customBar(Context context) {
        getIndeterminateDrawable().setColorFilter(
                ContextCompat.getColor(getContext(), R.color.white),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }
}