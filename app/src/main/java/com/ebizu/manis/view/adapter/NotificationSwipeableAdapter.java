package com.ebizu.manis.view.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.view.holder.NotificationSwipeViewHolder;
import com.ebizu.manis.view.swipe.SwipeRightNotification;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu on 8/30/17.
 */

public class NotificationSwipeableAdapter extends RecyclerView.Adapter<NotificationSwipeViewHolder>
        implements SwipeableItemAdapter<NotificationSwipeViewHolder> {

    private List<NotificationTableList> notificationTableList;
    public EventListener eventListener;

    public NotificationSwipeableAdapter() {
        notificationTableList = new ArrayList<>();
    }

    @Override
    public NotificationSwipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationSwipeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fragment_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(NotificationSwipeViewHolder holder, int position) {
        holder.setNotificationView(notificationTableList.get(position));
    }

    @Override
    public int getItemCount() {
        return notificationTableList.size();
    }

    @Override
    public int onGetSwipeReactionType(NotificationSwipeViewHolder holder, int position, int x, int y) {
        return SwipeableItemConstants.REACTION_CAN_SWIPE_BOTH_H;
    }

    @Override
    public void onSetSwipeBackground(NotificationSwipeViewHolder holder, int position, int type) {
        holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        holder.itemView.setOnClickListener(view -> new AnalyticManager(holder.context).trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_NOTIFICATION,
                ConfigManager.Analytic.Action.ITEM_CLICK,
                "Item Notification"
        ));
    }

    @Override
    public SwipeResultAction onSwipeItem(NotificationSwipeViewHolder holder, int position, int result) {
        switch (result) {
            case SwipeableItemConstants.RESULT_SWIPED_RIGHT: {
                SwipeRightNotification swipeRightNotification = new SwipeRightNotification(this, notificationTableList.get(position), position);
                swipeRightNotification.setOnClickItemRemoveListener(() -> {
                    new AnalyticManager(holder.context).trackEvent(
                            ConfigManager.Analytic.Category.FRAGMENT_NOTIFICATION,
                            ConfigManager.Analytic.Action.SWIPE,
                            "Delete item click"
                    );
                });
                return swipeRightNotification;
            }
            case SwipeableItemConstants.RESULT_SWIPED_LEFT: {
                //Do nothing;
                break;
            }
        }
        return null;
    }

    public void addNotificationList(List<NotificationTableList> notificationTableList) {
        this.notificationTableList.addAll(notificationTableList);
        notifyItemRangeInserted(this.notificationTableList.size() + 1,
                notificationTableList.size());
    }

    public void replaceNotificationList(List<NotificationTableList> notificationTableList) {
        this.notificationTableList = notificationTableList;
        notifyDataSetChanged();
    }

    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }

    public void deleteAll() {
        this.notificationTableList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public interface EventListener {
        void onItemRemoved(int position);
    }
}