package com.ebizu.manis.view.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ebizu.manis.R;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.statusmyvoucher.expiredstatus.VoucherExpiredView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 11/24/17.
 */

public class VoucherExpiredViewHolder extends RecyclerView.ViewHolder {

    private Context context;
    private VoucherExpiredView voucherExpiredView;
    private ExpiredVoucherListener expiredVoucherListener;

    @BindView(R.id.frpi_img_redeem)
    ImageView imgRedeemExpired;

    @BindView(R.id.frpi_txt_title)
    TextView txtTitle;

    @BindView(R.id.frpi_txt_category)
    TextView txtCategory;

    public VoucherExpiredViewHolder(View itemView, final ExpiredVoucherListener expiredVoucherListener) {
        super(itemView);
        this.context = itemView.getContext();
        setOnExpiredVoucherListener(expiredVoucherListener);
        ButterKnife.bind(this, itemView);
    }

    public void setVoucherExpiredView (final RewardVoucher voucher, VoucherExpiredView voucherExpiredView){
        try{
            this.voucherExpiredView = voucherExpiredView;
            txtTitle.setText(voucher.getName());
            txtCategory.setText(voucher.getProvider().getName());
            loadImage(voucher.getImage(), R.drawable.default_pic_promo_details_pic_small, imgRedeemExpired);
            itemView.setOnClickListener(v -> expiredVoucherListener.onExpiredVoucherListener(voucher));

        }catch (Exception e){
            Log.e("context", String.valueOf(e));
        }
    }

    private void loadImage(Object image, int placeHolder, ImageView target){
        Glide.with(context)
                .load(image)
                .thumbnail(0.1f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .placeholder(placeHolder)
                .animate(R.anim.fade_in_image)
                .into(target);
    }

    private void setOnExpiredVoucherListener(ExpiredVoucherListener onExpiredVoucherListener){
        this.expiredVoucherListener = onExpiredVoucherListener;
    }

    public interface ExpiredVoucherListener{
        void onExpiredVoucherListener(RewardVoucher rewardVoucher);
    }
}
