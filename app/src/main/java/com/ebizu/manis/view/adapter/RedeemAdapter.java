package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ebizu.manis.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 7/26/17.
 */

public class RedeemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context context;
    public List<com.ebizu.sdk.reward.models.Reward> rewards;
    private RedeemAdapter.OnclickListener onclickListener;

    public RedeemAdapter(Context context){
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh ;

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reward,parent,false);
        vh = new RewardHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        com.ebizu.sdk.reward.models.Reward reward = rewards.get(position);
        RewardHolder rewardHolder = (RewardHolder) holder;

        rewardHolder.txtTitle.setText(reward.getName());
        rewardHolder.txtCategory.setText(reward.getProvider().getName());
        int dataValue = reward.getPoint();
        rewardHolder.txtPoints.setText((dataValue == 0) ? context.getString(R.string.rd_txt_free) : dataValue + " " + context.getString(R.string.rd_txt_pts));
        Glide.with(context)
                .load(reward.getLargeImage())
                .thumbnail(0.1f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .placeholder(R.drawable.default_pic_store_logo)
                .animate(R.anim.fade_in_image)
                .into(rewardHolder.imgRedeem);

        if (reward.isHidePrice())
            rewardHolder.txtValue.setVisibility(View.GONE);
        else
            rewardHolder.txtValue.setText(reward.getCurrency() + " " + reward.getValue());

        if(reward.getStock() < 1){
            rewardHolder.txtOutOfStock.setVisibility(View.VISIBLE);
        }else{
            rewardHolder.txtOutOfStock.setVisibility(View.GONE);
        }
        rewardHolder.relativeReward.setOnClickListener(v -> onclickListener.onRewardDetailsClickListener(reward));

    }

    public void setOnclickListener(OnclickListener onclickListener){
        if(this.onclickListener == null){
            this.onclickListener = onclickListener;
        }
    }

    public void addRedeem(List<com.ebizu.sdk.reward.models.Reward> rewards){
        this.rewards = rewards;
        notifyDataSetChanged();
    }

    public boolean isEmptyReward(){
        return rewards.size() == 0;
    }

    @Override
    public int getItemCount() {
//        return rewards.size();
        if(rewards == null){
            return 0;
        }else{
            return rewards.size();
        }
    }

    public class RewardHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_redeem)
        ImageView imgRedeem;
        @BindView(R.id.txt_value)
        TextView txtValue;
        @BindView(R.id.txt_outofstock)
        TextView txtOutOfStock;
        @BindView(R.id.txt_points)
        TextView txtPoints;
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_category)
        TextView txtCategory;
        @BindView(R.id.rl_reward)
        RelativeLayout relativeReward;

        public RewardHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public interface OnclickListener{
        void onRewardDetailsClickListener (com.ebizu.sdk.reward.models.Reward reward);
    }
}
