package com.ebizu.manis.view.dialog.inviteterm;

import android.content.Context;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.InviteTerm;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.rx.ResponseSubsriberLambda;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.response.WrapperInviteTerm;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by abizu-alvio on 8/14/2017.
 */

public class InviteTermPresenter extends BaseDialogPresenter implements IInviteTermPresenter {

    private final String TAG = getClass().getSimpleName();

    private InviteTermDialog inviteTermDialog;
    private ManisApi manisApi;

    @Override
    public void attachDialog(BaseDialogManis baseDialogManis) {
        super.attachDialog(baseDialogManis);
        inviteTermDialog = (InviteTermDialog) baseDialogManis;
    }

    @Override
    public void loadInviteTerm(Context context) {
        inviteTermDialog.showBaseProgressBar();
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceWithToken(inviteTermDialog.getBaseActivity());
        manisApi.getInviteTerm()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperInviteTerm>(inviteTermDialog) {
                    @Override
                    public void onNext(WrapperInviteTerm wrapperInviteTerm) {
                        super.onNext(wrapperInviteTerm);
                        InviteTerm inviteTerms = wrapperInviteTerm.getInviteTerm();
                        inviteTermDialog.setViewInvite(inviteTerms);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        inviteTermDialog.dismissBaseProgressBar();
                    }
                });
    }

    @Override
    public void loadInviteTerm() {
        inviteTermDialog.showBaseProgressBar();
        RequestBody requestBody = new RequestBodyBuilder(inviteTermDialog.getContext())
                .setFunction(ConfigManager.Account.FUNCTION_GET_TERMS)
                .create();
        getManisApiV2().getInviteTerms(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperInviteTerm>(inviteTermDialog) {

                    @Override
                    protected void onSuccess(WrapperInviteTerm wrapperInviteTerm) {
                        super.onSuccess(wrapperInviteTerm);
                        InviteTerm inviteTerms = wrapperInviteTerm.getInviteTerm();
                        inviteTermDialog.setViewInvite(inviteTerms);
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                        inviteTermDialog.dismissBaseProgressBar();
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        inviteTermDialog.dismissBaseProgressBar();
                    }
                });
    }
}
