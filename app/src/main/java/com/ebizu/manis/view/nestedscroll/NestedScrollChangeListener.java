package com.ebizu.manis.view.nestedscroll;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ebizu.manis.view.manis.swiperefresh.SwipeRefreshManis;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public abstract class NestedScrollChangeListener implements NestedScrollView.OnScrollChangeListener {

    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerView;
    private SwipeRefreshManis swipeRefresh;

    public NestedScrollChangeListener(LinearLayoutManager linearLayoutManager, RecyclerView recyclerView, SwipeRefreshManis swipeRefresh) {
        this.linearLayoutManager = linearLayoutManager;
        this.recyclerView = recyclerView;
        this.swipeRefresh = swipeRefresh;
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        int childHeight = 0;
        if (recyclerView.getChildAt(0) != null)
            childHeight = recyclerView.getChildAt(0).getHeight();
        int totalDy = (linearLayoutManager.getChildCount() - 1) * childHeight;
        int totalScrollY = (scrollY + swipeRefresh.getNestedScrollView().getHeight());
        if (!isLoading() && !isLastPage()) {
            if (totalDy <= totalScrollY) {
                onLoadMore();
            }
        }
    }

    protected abstract void onLoadMore();

    protected abstract boolean isLoading();

    protected abstract boolean isLastPage();
}
