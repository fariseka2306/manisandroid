package com.ebizu.manis.view.walkthrough.walkthroughpage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.mvp.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by FARIS_mac on 8/31/17.
 */

public class WalkthroughPageFour extends WalkthroughPageView {

    @BindView(R.id.parent)
    RelativeLayout relativeLayoutParent;
    @BindView(R.id.imageview_walkthrough_snap)
    ImageView imageViewWalkthroughSnap;

    private int animatorPosition;

    public WalkthroughPageFour(Context context) {
        super(context);
    }

    public WalkthroughPageFour(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WalkthroughPageFour(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WalkthroughPageFour(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.activity_walkthrough_page_four, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        animatorPosition = 0;
        setImage(animatorPosition);
        animatorPosition++;
    }

    @Override
    public int page() {
        return 4;
    }

    @Override
    public void startPage() {
        super.startPage();
        startAnimeSlideRight(this);
        relativeLayoutParent.setVisibility(VISIBLE);
    }

    @Override
    public void finishPage() {
        super.finishPage();
        onNextPageListener.onNextPage(page());
        startAnimFadeOut(this);
    }

    @Override
    public void setOnNextPageListener(OnNextPageListener onNextPageListener) {
        super.setOnNextPageListener(onNextPageListener);
        this.onNextPageListener = onNextPageListener;
    }

    private void setImage(int positionImage) {
        Glide.with(getContext())
                .load(getImage(positionImage))
                .fitCenter()
                .placeholder(getImage(positionImage - 1))
                .crossFade(200)
                .into(imageViewWalkthroughSnap);
    }

    private int getImage(int positionImage) {
        switch (positionImage) {
            case 0:
                return R.drawable.snapviewoverlay;
            case 1:
                return R.drawable.snapviewcrop;
            case 2:
                return R.drawable.snapviewcropdone;
            default:
                return R.drawable.snapviewoverlay;
        }
    }

    @OnClick(R.id.imageview_walkthrough_snap)
    public void snapReceipt() {
        if (animatorPosition % 2 != 0) {
            if (animatorPosition >= 3) {
                SharedPreferences prefFirst = getBaseActivity().getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
                UtilSessionFirstIn.setShowWalktrough(prefFirst, false);
                UtilSessionFirstIn.setShowWalktroughNotif(prefFirst, true);
                getBaseActivity().setResult(ConfigManager.Walkthrough.NOTIFICATION_WALK_THROUGH);
                getBaseActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                getBaseActivity().finish();
            } else {
                setImage(animatorPosition);
                animatorPosition++;
                new Handler().postDelayed(() -> {
                    setImage(animatorPosition);
                    animatorPosition++;
                }, 1000);
            }
        }
    }
}
