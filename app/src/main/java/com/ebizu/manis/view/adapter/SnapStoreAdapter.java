package com.ebizu.manis.view.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.multiplier.MultiplierMenuManager;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileActivity;
import com.ebizu.manis.mvp.snap.form.storedetail.ReceiptDetailActivity;
import com.ebizu.manis.mvp.snap.store.list.IListSnapStorePresenter;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;
import com.ebizu.manis.view.holder.ProgessBarHolder;
import com.ebizu.manis.view.holder.SnapStoreHolder;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public class SnapStoreAdapter extends RecyclerView.Adapter {

    private BaseActivity activity;
    private ArrayList<Store> stores;
    private final int STORE_NEARBY_VIEW_TYPE = 0;
    private final int PROGRESS_VIEW_TYPE = 1;
    private OnItemClickListener onItemClickListener;
    private IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener;

    public SnapStoreAdapter(BaseActivity activity, ArrayList<Store> stores) {
        this.activity = activity;
        this.stores = stores;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case STORE_NEARBY_VIEW_TYPE: {
                return new SnapStoreHolder(LayoutInflater.from(activity).inflate(R.layout.item_holder_store_list, parent, false));
            }
            case PROGRESS_VIEW_TYPE: {
                return new ProgessBarHolder(LayoutInflater.from(activity).inflate(R.layout.pagination_progress, parent, false));
            }
            default: {
                return null;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SnapStoreHolder) {
            SnapStoreHolder snapStoreHolder = (SnapStoreHolder) holder;
            snapStoreHolder.setSnapStore(stores.get(position));
            snapStoreHolder.setOnClickListener((receiptStore) -> {
                if (MultiplierMenuManager.isLuckyDraw(stores.get(position).getMerchantTier())) {
                    if (null != onCheckSnapAbleListener)
                        onCheckSnapAbleListener.onCheckSnapAble(stores.get(position));
                } else {
                    startActivityReceiptDetail(position, receiptStore);
                }
                if (null != onItemClickListener)
                    onItemClickListener.onClick(stores.get(position));
            });
        }
    }

    private void startEventLuckyDraw(int position, ReceiptStore receiptStore) {
        if (activity.getManisSession().isNotAvailableNameUser()) {
            activity.showAlertDialog("Lucky Draw", activity.getString(R.string.ld_dialog_complete_profile),
                    true, R.drawable.manis_logo,
                    "Ok", (dialogInterface, i) -> {
                        Intent intent = new Intent(activity, ProfileActivity.class);
                        intent.putExtra(ConfigManager.UpdateProfile.UPDATE_PROFILE_TYPE_INTENT, ConfigManager.UpdateProfile.UPDATE_PROFILE_LUCKY_DRAW_TYPE);
                        putIntentExtraReceiptDetail(position, intent, receiptStore);
                        activity.startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
                    }, null, null);
        } else {
            startActivityReceiptDetail(position, receiptStore);
        }
    }

    private void startActivityReceiptDetail(int position, ReceiptStore receiptStore) {
        Intent intent = new Intent(activity, ReceiptDetailActivity.class);
        putIntentExtraReceiptDetail(position, intent, receiptStore);
        activity.startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
    }

    private void putIntentExtraReceiptDetail(int position, Intent intent, ReceiptStore receiptStore) {
        intent.putExtra(ConfigManager.Snap.MERCHANT_DATA, receiptStore);
        boolean isLuckyDraw = MultiplierMenuManager.isLuckyDraw(stores.get(position).getMerchantTier());
        intent.putExtra(ConfigManager.Snap.LUCKY_DRAW_INTENT_DATA, isLuckyDraw);
    }

    @Override
    public int getItemViewType(int position) {
        if (stores.get(position) == null) {
            return PROGRESS_VIEW_TYPE;
        } else {
            return STORE_NEARBY_VIEW_TYPE;
        }
    }

    public void setOnCheckSnapAbleListener(IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener) {
        this.onCheckSnapAbleListener = onCheckSnapAbleListener;
    }

    public void addNearStores(ArrayList<Store> stores) {
        this.stores.addAll(stores);
        notifyItemRangeInserted(this.stores.size() + 1,
                stores.size());
    }

    public void replaceNearStores(ArrayList<Store> stores) {
        this.stores.clear();
        this.stores = stores;
        notifyDataSetChanged();
    }

    public ArrayList<Store> getSnapStores() {
        return stores;
    }

    public boolean isEmptyStores() {
        return stores.size() == 0;
    }

    public void loadProgress() {
        stores.add(null);
        notifyItemInserted(stores.size());
    }

    public void dissProgress() {
        stores.removeAll(Collections.singleton(null));
        notifyItemRemoved(stores.size());
    }

    @Override
    public int getItemCount() {
        return stores.size();
    }

    public void setOnClickItemListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onClick(Store store);
    }

}