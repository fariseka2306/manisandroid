package com.ebizu.manis.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.statusmyvoucher.expiredstatus.VoucherExpiredView;
import com.ebizu.manis.sdk.entities.MyVoucher;
import com.ebizu.manis.view.holder.MyVoucherViewHolder;
import com.ebizu.manis.view.holder.ProgessBarHolder;
import com.ebizu.manis.view.holder.VoucherExpiredViewHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 11/14/17.
 */

public class VoucherExpiredAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<RewardVoucher> rewards;
    public final int VIEW_TYPE_LOADING = 1;
    public final int VIEW_TYPE_ITEM = 0;
    private boolean isLoadingAdded = false;

    private VoucherExpiredView voucherExpiredView;
    private VoucherExpiredViewHolder.ExpiredVoucherListener voucherListener;


    public VoucherExpiredAdapter(List<MyVoucher> myVouchers,
                                 VoucherExpiredView voucherExpiredView, VoucherExpiredViewHolder.ExpiredVoucherListener voucherListener){
                this.rewards        = new ArrayList<>();
        this.voucherExpiredView = voucherExpiredView;
        this.voucherListener = voucherListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case VIEW_TYPE_ITEM: {
                viewHolder = getViewHolder(parent,layoutInflater);
                break;
            }
            case VIEW_TYPE_LOADING: {
                return new ProgessBarHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_progress, parent, false));
            }
            default: {
                return null;
            }
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VoucherExpiredViewHolder){
            RewardVoucher reward = rewards.get(position);
            VoucherExpiredViewHolder voucherExpiredViewHolder = (VoucherExpiredViewHolder) holder;
            voucherExpiredViewHolder.setVoucherExpiredView(reward,voucherExpiredView);
        }
    }

    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater layoutInflater){
        final RecyclerView.ViewHolder expiredVoucherGridView;
        View view = layoutInflater.inflate(R.layout.item_my_voucher_expired,parent,false);
        expiredVoucherGridView = rewards.get(0) instanceof RewardVoucher ?
                new VoucherExpiredViewHolder(view,voucherListener):
                new VoucherExpiredViewHolder(view,voucherListener);

        return expiredVoucherGridView;
    }

    @Override
    public int getItemCount() {
        return rewards.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == rewards.size() - 1 && isLoadingAdded) ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public List<RewardVoucher> getRewards() {
        return rewards;
    }

    public void add(RewardVoucher r) {
        rewards.add(r);
    }

    public void addAll(List<RewardVoucher> moveResults) {
        for (RewardVoucher result : moveResults) {
            add(result);
        }
        notifyDataSetChanged();
    }

    public void addPurchasedVouchers(List<RewardVoucher> rewards) {
        this.rewards = rewards;
        notifyItemInserted(this.rewards.size() + 1);
    }

    public void replacePurchasedVouchers(List<RewardVoucher> rewards) {
        this.rewards = rewards;
        notifyDataSetChanged();
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new RewardVoucher());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = rewards.size() - 1;
        RewardVoucher result = getItem(position);

        if (result != null && result.getName().isEmpty()) {
            rewards.remove(position);
            notifyItemRemoved(position);
        }
    }

    public RewardVoucher getItem(int position) {
        return rewards.get(position);
    }

    public void showProgress() {
        this.rewards.add(null);
        notifyItemInserted(this.rewards.size());
    }

    public void dismissProgress() {
        this.rewards.removeAll(Collections.singleton(null));
        notifyItemRemoved(this.rewards.size());
    }

    public boolean isEmpty() {
        return rewards.size() < 1;
    }


    public class voucherExpiredHolder extends RecyclerView.ViewHolder {

        public voucherExpiredHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
