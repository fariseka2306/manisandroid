package com.ebizu.manis.view.dialog.snaptnc;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.snap.SnapTermLuckyDraw;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ebizu on 10/11/17.
 */

public class SnapEarnTicketTncDialog extends SnapTncDialog {

    @BindView(R.id.text_view_earning_ticket)
    TextView textViewEarningTicket;
    @BindView(R.id.text_view_validity)
    TextView textViewValidity;
    @BindView(R.id.text_view_receipt_clarity)
    TextView textViewReceiptClarity;
    @BindView(R.id.text_view_limit)
    TextView textViewLimit;
    @BindView(R.id.text_view_earning_title)
    TextView textViewEarningTitle;

    private SnapTermLuckyDraw snapTermLuckyDraw;

    public SnapEarnTicketTncDialog(@NonNull Context context) {
        super(context);
    }

    public SnapEarnTicketTncDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected SnapEarnTicketTncDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void addDialogView(Context context) {
        super.addDialogView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_snap_earn_ticket_tnc, null, false);
        getParent().addView(view);
        ButterKnife.bind(this, view);
        setSnapEarnTicketTncView(getSnapTermLuckyDraw());
    }

    private void setSnapEarnTicketTncView(SnapTermLuckyDraw snapTermLuckyDraw) {
        if (null != snapTermLuckyDraw) {
            textViewEarningTicket.setText(snapTermLuckyDraw.getPoint());
            textViewLimit.setText(snapTermLuckyDraw.getLimit());
            textViewValidity.setText(snapTermLuckyDraw.getValidity());
            textViewEarningTitle.setText(snapTermLuckyDraw.getShortDescription());
        }
    }

    @OnClick(R.id.btn_ok)
    void onCLickOk() {
        if (null != onAcceptTncListener)
            onAcceptTncListener.onAccept();
        dismiss();
    }

    @OnClick(R.id.btn_always_accept)
    void onClickAlwaysAccept() {
        if (null != onAcceptTncListener)
            onAcceptTncListener.onAccept();
        getManisSession().acceptAlwaysTncSnapEarnTicket();
        dismiss();
    }

    public SnapTermLuckyDraw getSnapTermLuckyDraw() {
        if (snapTermLuckyDraw == null)
            snapTermLuckyDraw = new Gson().fromJson(getManisSession().getTermsSnapEarnTickets(), SnapTermLuckyDraw.class);
        return snapTermLuckyDraw;
    }
}
