package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ebizu.manis.R;

import java.util.List;

/**
 * Created by halim_ebizu on 8/23/17.
 */

public class RewardCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Integer> pictures;
    private List<String> categories;
    private RewardCategoryListener rewardCategoryListener;

    public RewardCategoryAdapter(Context context, List<Integer> pictures, List<String> categories) {
        this.context = context;
        this.pictures = pictures;
        this.categories = categories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reward_category, parent, false);
        return new RewardCategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final Integer assetRewardCategory = pictures.get(position);
        final String assetCategory = categories.get(position);

        RewardCategoryViewHolder holder = (RewardCategoryViewHolder) viewHolder;
        Glide.with(context)
                .load(assetRewardCategory)
                .thumbnail(0.1f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .placeholder(R.drawable.default_pic_store_logo)
                .animate(R.anim.fade_in_image)
                .into(holder.imgItemCategory);
        holder.tvCategory.setText(assetCategory);
        holder.rlCategoryItem.setOnClickListener(view -> rewardCategoryListener.onRewardCategoryListener(assetCategory));
    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

    public void addCategories(List<Integer> pictures, List<String> categories) {
        this.pictures = pictures;
        this.categories = categories;
        notifyDataSetChanged();
    }

    public void setRewardCategoryListener(RewardCategoryListener rewardCategoryListener) {
        this.rewardCategoryListener = rewardCategoryListener;
    }

    public class RewardCategoryViewHolder extends RecyclerView.ViewHolder {

        ImageView imgItemCategory;
        TextView tvCategory;
        RelativeLayout rlCategoryItem;

        public RewardCategoryViewHolder(View itemView) {
            super(itemView);
            imgItemCategory = (ImageView) itemView.findViewById(R.id.img_item_category);
            tvCategory = (TextView) itemView.findViewById(R.id.tv_category);
            rlCategoryItem = (RelativeLayout) itemView.findViewById(R.id.rl_cateory_item);
        }
    }

    public interface RewardCategoryListener {
        void onRewardCategoryListener(String categoryName);
    }
}
