package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.model.InterestsStore;

import java.util.List;

/**
 * Created by firef on 7/10/2017.
 */

public class StoreCategoryAdapter extends ListAdapter<InterestsStore, StoreCategoryViewHolder> {

    public StoreCategoryAdapter(Context context, List<InterestsStore> interestsStores) {
        super(R.layout.item_fragment_stores_categories, context, interestsStores);
    }
}
