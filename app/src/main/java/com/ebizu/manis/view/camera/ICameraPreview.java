package com.ebizu.manis.view.camera;

import android.hardware.Camera;
import android.view.SurfaceHolder;

import java.util.List;

/**
 * Created by Ebizu-User on 11/08/2017.
 */

public interface ICameraPreview {

    void initialize();

    Camera getCamera();

    void instanceCamera();

    SurfaceHolder getSurfaceHolder();

    void instanceSurfaceHolder();

    List<Camera.Size> getPreviewSizeList();

    List<Camera.Size> getPictureSizeList();

    void focus();

    void pause();

    void stop();

    void resume();

    void setOnPreview(PreviewCallback previewCallback);

    void setPictureCallback(Camera.PictureCallback pictureCallback);

    void takePicture();

    interface PreviewCallback {
        void onPreviewFrame(byte[] data, Camera camera);
    }
}
