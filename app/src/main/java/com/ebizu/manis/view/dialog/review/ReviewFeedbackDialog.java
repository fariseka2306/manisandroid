package com.ebizu.manis.view.dialog.review;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ebizu.manis.R;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.ReviewBody;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;

/**
 * Created by mac on 8/23/17.
 */

public class ReviewFeedbackDialog extends BaseDialogManis implements View.OnClickListener {

    private EditText editTextFeedback;
    private Button buttonOk;
    private Button buttonCancel;

    SendReviewPresenter sendReviewPresenter;

    public ReviewFeedbackDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public ReviewFeedbackDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected ReviewFeedbackDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_review_feedback, null, false);
        getParent().addView(view);
        editTextFeedback = (EditText) view.findViewById(R.id.edittext_feedback);
        buttonOk = (Button) view.findViewById(R.id.button_ok);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);
        this.sendReviewPresenter = new SendReviewPresenter(getContext());
        editTextFeedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enableYesButton();
            }
        });
        buttonOk.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
    }

    private void enableYesButton() {
        new Handler().post(() -> {
            if (editTextFeedback.getText().toString().trim().length() > 0) {
                buttonOk.setEnabled(true);
                buttonOk.setBackgroundResource(R.drawable.bg_btn_rounded_green_full);
            } else {
                buttonOk.setEnabled(false);
                buttonOk.setBackgroundResource(R.drawable.bg_btn_rounded_grey);
            }
        });
    }

    @Override
    public void onClick(View v) {
        ReviewBody reviewBody = new ReviewBody();
        if (v.equals(buttonOk)) {
            reviewBody.setReview("0");
            reviewBody.setHappy("0");
            reviewBody.setComment(editTextFeedback.getText().toString());
            sendReviewPresenter.sendReview(reviewBody);
            dismiss();
            ReviewFeedbackSendDialog reviewFeedbackSendDialog = new ReviewFeedbackSendDialog(getBaseActivity());
            reviewFeedbackSendDialog.show();
            reviewFeedbackSendDialog.setActivity(getBaseActivity());
        } else {
            reviewBody.setComment("");
            reviewBody.setHappy("0");
            reviewBody.setReview("0");
            sendReviewPresenter.sendReview(reviewBody);
            dismiss();
        }
    }
}
