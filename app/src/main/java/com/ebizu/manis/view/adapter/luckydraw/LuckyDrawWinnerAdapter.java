package com.ebizu.manis.view.adapter.luckydraw;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.LuckyDrawWinner;
import com.ebizu.manis.view.adapter.ListAdapter;
import com.ebizu.manis.view.holder.luckydraw.LuckyDrawWinnerViewHolder;

/**
 * Created by ebizu on 12/12/17.
 */

public class LuckyDrawWinnerAdapter extends ListAdapter<LuckyDrawWinner, LuckyDrawWinnerViewHolder> {

    public LuckyDrawWinnerAdapter(Context context) {
        super(R.layout.item_lucky_draw_winner, context);
    }

    public LuckyDrawWinnerAdapter(Context context, RecyclerView recyclerView) {
        super(R.layout.item_lucky_draw_winner, context, recyclerView);
    }
}
