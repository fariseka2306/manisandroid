package com.ebizu.manis.view.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ebizu.manis.preference.ManisSession;

/**
 * Created by halim_ebizu on 11/29/17.
 */

public abstract class BaseHolder<T> extends RecyclerView.ViewHolder {

    protected Context context;

    public BaseHolder(View view) {
        super(view);
        context = view.getContext();
    }

    public abstract void setHolderView(T model);

    public ManisSession getManisSession() {
        return ManisSession.getInstance(context);
    }

}