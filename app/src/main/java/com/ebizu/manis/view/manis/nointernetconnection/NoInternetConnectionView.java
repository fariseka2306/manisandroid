package com.ebizu.manis.view.manis.nointernetconnection;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 21/07/2017.
 */

public class NoInternetConnectionView extends RelativeLayout {

    @BindView(R.id.constraint_group)
    ConstraintLayout viewRetry;

    public NoInternetConnectionView(Context context) {
        super(context);
        createView(context);
    }

    public NoInternetConnectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public NoInternetConnectionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NoInternetConnectionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    private void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_no_internet_connection, null, false);
        addView(view);
        ButterKnife.bind(this, view);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        viewRetry.setOnClickListener((v) -> {
            onClickListener.onClickRetry();
        });
    }

    public interface OnClickListener {
        void onClickRetry();
    }
}
