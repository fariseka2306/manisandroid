package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Offer;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 8/22/17.
 */

public class StoreOfferAdapter extends ArrayAdapter<Offer> {

    private Context context;
    private List<Offer> offers;
    private StoreOfferListener storeOfferListener;

    @BindView(R.id.sdi_img_offer)
    ImageView sdiImgOffer;

    @BindView(R.id.sdi_img_merchant)
    ImageView sdiImgMerchant;

    @BindView(R.id.sdi_txt_title)
    TextView sdiTxtTitle;

    @BindView(R.id.sdi_txt_detail)
    TextView sdiTxtDetail;

    @BindView(R.id.sdi_txt_time)
    TextView sdiTxtTime;

    @BindView(R.id.rl_offer)
    RelativeLayout rlOffer;

    public StoreOfferAdapter(Context context, List<Offer> offers) {
        super(context, R.layout.recycle_view_offer, offers);
        this.context = context;
        this.offers = offers;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.recycle_view_offer, parent, false);
        }

        ButterKnife.bind(this, convertView);
        Offer offer = offers.get(position);
        ImageUtils.loadImage(context, offer.getPhoto(),
                ContextCompat.getDrawable(context, R.drawable.default_pic_store_logo), sdiImgOffer);
        ImageUtils.loadImage(context, offer.getPhoto(),
                ContextCompat.getDrawable(context, R.drawable.default_pic_store_logo), sdiImgMerchant);
        sdiTxtTitle.setText(offer.getTitle());
        sdiTxtDetail.setText(offer.getDescription());
        sdiTxtTime.setText(UtilManis.getTimeAgo(offer.getOfferDate().getStart(), context));
        rlOffer.setOnClickListener(view -> storeOfferListener.onStoreOfferListener(offer));
        return convertView;
    }

    public void setStoreOfferListener(StoreOfferListener storeOfferListener) {
        this.storeOfferListener = storeOfferListener;
    }

    public interface StoreOfferListener {
        void onStoreOfferListener(Offer offer);
    }
}
