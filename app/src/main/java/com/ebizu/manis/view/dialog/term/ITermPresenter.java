package com.ebizu.manis.view.dialog.term;

import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.view.dialog.IDialogPresenter;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public interface ITermPresenter extends IDialogPresenter {

    void loadTerm(RequestBody requestBody,String contentType);
}
