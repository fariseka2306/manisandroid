package com.ebizu.manis.view.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.FaqChild;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 8/4/2017.
 */

public class FaqChildViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.faq_item)
    LinearLayout faqItem;
    @BindView(R.id.text_view_title)
    TextView textViewTitle;

    public FaqChildViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setFaqChildItemView(FaqChild faqChild) {
        textViewTitle.setText(faqChild.getTitle());
    }
}
