package com.ebizu.manis.view.adapter.rewardvoucher;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.AutoResizeTextView;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BulkRewardViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.frpi_img_redeem)
    ImageView ivReward;
    @BindView(R.id.frpi_img_bucket)
    ImageView frpiImgBucket;
    @BindView(R.id.frpi_img_manis)
    ImageView frpiImgManis;
    @BindView(R.id.frpi_txt_title)
    TextView tvRewardName;
    @BindView(R.id.frpi_txt_category)
    TextView tvProviderName;
    @BindView(R.id.frpi_txt_points)
    AutoResizeTextView tvPoint;
    @BindView(R.id.frpi_txt_value)
    AutoResizeTextView tvRewardValue;
    @BindView(R.id.frpi_txt_outofstock)
    TextView tvOutOfStock;
    @BindView(R.id.rl_img_point)
    LinearLayout rlImgPoint;
    @BindView(R.id.layout_point)
    public LinearLayout llPoint;
    @BindView(R.id.layout_value)
    public LinearLayout llValue;
    @BindView(R.id.rl_btn_free)
    RelativeLayout rlBtnFree;

    private RewardVoucher item;
    private Context context;
    private BulkRewardListener bulkRewardListener;

    public BulkRewardViewHolder(View itemView, final BulkRewardListener listener) {
        super(itemView);
        context = itemView.getContext();
        setOnBulkRewardListener(listener);
        ButterKnife.bind(this, itemView);
    }

    public void setItem(RewardVoucher item) {
        this.item = item;
        tvRewardName.setText(item.getName());
        ImageUtils.loadImage(context, item.getImage128(), ContextCompat.getDrawable(context, R.drawable.default_pic_promo_details_pic_small), ivReward);
        tvOutOfStock.setVisibility(View.GONE);
        tvProviderName.setText(item.getProvider().getName());
        tvRewardValue.setText(item.getCurrency() + " " + (int) item.getVoucherPurchaseValue());
        tvPoint.setText(item.getPoint() + " " + context.getString(R.string.rd_txt_pts));

        // free voucher
        if (item.getPoint() == 0 &&
                item.getVoucherTransactionType().equalsIgnoreCase(RewardVoucher.VOUCHER_TRANSACTION_REDEEMABLE) &&
                item.getVoucherPurchaseValue() == 0) {
            tvPoint.setText(context.getString(R.string.rd_txt_free));
            llPoint.setVisibility(View.INVISIBLE);
            llValue.setVisibility(View.INVISIBLE);
            rlBtnFree.setVisibility(View.VISIBLE);
        }

        // redeemable only
        else if (item.getPoint() >= 0 && item.getVoucherPurchaseValue() == 0 &&
                item.getVoucherTransactionType().equalsIgnoreCase(RewardVoucher.VOUCHER_TRANSACTION_REDEEMABLE)) {
            rlBtnFree.setVisibility(View.INVISIBLE);
            llPoint.setVisibility(View.VISIBLE);
            llValue.setVisibility(View.INVISIBLE);
        }

        // purchaseable only
        else if (item.getPoint() == 0 &&
                item.getVoucherTransactionType().equalsIgnoreCase(RewardVoucher.VOUCHER_TRANSACTION_PURCHASABLE) &&
                item.getVoucherPurchaseValue() > 0.0) {
            rlBtnFree.setVisibility(View.INVISIBLE);
            llPoint.setVisibility(View.INVISIBLE);
            llValue.setVisibility(View.VISIBLE);
        }

        //both
        else if (item.getPoint() > 0 &&
                item.getVoucherPurchaseValue() > 0) {
            rlBtnFree.setVisibility(View.INVISIBLE);
            llPoint.setVisibility(View.VISIBLE);
            llValue.setVisibility(View.VISIBLE);
        } else {
            llPoint.setVisibility(View.INVISIBLE);
            llValue.setVisibility(View.INVISIBLE);
            rlBtnFree.setVisibility(View.INVISIBLE);
        }

        if (item.getStock() < 1) {
            tvOutOfStock.setVisibility(View.VISIBLE);
        } else {
            rlImgPoint.setVisibility(View.VISIBLE);
            tvOutOfStock.setVisibility(View.GONE);
        }

        itemView.setOnClickListener(view -> {
            bulkRewardListener.onBulkRewardListener(this.item);
            new AnalyticManager(context).trackEvent(
                    ConfigManager.Analytic.Category.FRAGMENT_REWARD_POINT,
                    ConfigManager.Analytic.Action.ITEM_CLICK,
                    "Item Click".concat(" Item ").concat(item.getName()));
        });
    }

    private void setOnBulkRewardListener(BulkRewardListener rewardBulkListener) {
        this.bulkRewardListener = rewardBulkListener;
    }

    public interface BulkRewardListener {
        void onBulkRewardListener(RewardVoucher reward);
    }
}
