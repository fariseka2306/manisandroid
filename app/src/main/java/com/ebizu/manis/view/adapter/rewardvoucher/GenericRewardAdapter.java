package com.ebizu.manis.view.adapter.rewardvoucher;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.sdk.reward.models.Reward;

import java.util.ArrayList;
import java.util.List;

public class GenericRewardAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected Context context;

    private List<T> objects = new ArrayList<>();
    private View.OnClickListener onClickListener;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private BulkRewardViewHolder.BulkRewardListener bulkRewardListener;
    private SingleRewardViewHolder.RewardListener rewardListener;

    public GenericRewardAdapter(Context context, List<T> objects,
                                BulkRewardViewHolder.BulkRewardListener bulkRewardListener,
                                SingleRewardViewHolder.RewardListener rewardListener) {
        this.context = context;
        this.objects = objects;
        this.bulkRewardListener = bulkRewardListener;
        this.rewardListener = rewardListener;
    }

    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, layoutInflater);
                break;
            case LOADING:
                View view = layoutInflater.inflate(R.layout.pagination_progress, parent, false);
                viewHolder = new loadingViewHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object rewardVoucher = objects.get(position);
        switch (getItemViewType(position)) {
            case ITEM:
                if (rewardVoucher instanceof Reward) {
                    SingleRewardViewHolder singleRewardViewHolder = (SingleRewardViewHolder) holder;
                    singleRewardViewHolder.setItem((Reward) rewardVoucher);
                } else {
                    BulkRewardViewHolder bulkRewardViewHolder = (BulkRewardViewHolder) holder;
                    bulkRewardViewHolder.setItem((RewardVoucher) rewardVoucher);
                }
                break;
            case LOADING:
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == objects.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public View.OnClickListener getOnClickListener(){
        return onClickListener;
    }

    public void setOnClickListener(View.OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    private class loadingViewHolder extends RecyclerView.ViewHolder {
        public loadingViewHolder(View view) {
            super(view);
        }
    }

    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater layoutInflater) {
        final RecyclerView.ViewHolder rewardGridViewHolder;
        View view = layoutInflater.inflate(R.layout.item_new_reward_category, parent, false);
        rewardGridViewHolder = objects.get(0) instanceof Reward ?
                new SingleRewardViewHolder(view, rewardListener) :
                new BulkRewardViewHolder(view, bulkRewardListener);
        return rewardGridViewHolder;
    }

    public void add(T r) {
        objects.add(r);
        notifyItemInserted(objects.size() - 1);
    }

    public void addAll(List<T> moveResults) {
        for (T result : moveResults) {
            add(result);
        }
    }

    public void replaceReward(List<T> rewards) {
        this.objects.clear();
        this.objects = rewards;
        notifyDataSetChanged();
    }

    public void remove(Object r) {
        int position = objects.indexOf(r);
        if (position > -1) {
            objects.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add((T) new Object());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = objects.size() - 1;
        Object result = getItem(position);

        if (result != null) {
            objects.remove(position);
            notifyItemRemoved(position);
        }
    }
}