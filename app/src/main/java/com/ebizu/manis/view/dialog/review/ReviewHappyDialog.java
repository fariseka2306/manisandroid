package com.ebizu.manis.view.dialog.review;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.BuildConfig;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.ReviewBody;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;
import com.ebizu.manis.view.dialog.review.reviewdialog.DialogReview;

/**
 * Created by mac on 8/23/17.
 */

public class ReviewHappyDialog extends BaseDialogManis implements View.OnClickListener {

    private Button buttonYes;
    private Button buttonNotNow;

    ReviewBody reviewBody;
    SendReviewPresenter sendReviewPresenter;

    public ReviewHappyDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public ReviewHappyDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected ReviewHappyDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_review_happy, null, false);
        getParent().addView(view);
        buttonYes = (Button) view.findViewById(R.id.button_yes);
        buttonNotNow = (Button) view.findViewById(R.id.button_not_now);
        buttonYes.setOnClickListener(this);
        buttonNotNow.setOnClickListener(this);
        this.sendReviewPresenter = new SendReviewPresenter(getContext());
    }

    @Override
    public void onClick(View v) {
        ReviewBody reviewBody = new ReviewBody();
        if (v.equals(buttonYes)) {
            reviewBody.setComment("");
            reviewBody.setHappy("1");
            reviewBody.setReview("1");
            try {
                getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getContext().getString(R.string.market_id) + BuildConfig.APPLICATION_ID)));
            } catch (android.content.ActivityNotFoundException anfe) {
                getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getContext().getString(R.string.playstore_id) + BuildConfig.APPLICATION_ID)));
            }
            sendReviewPresenter.sendReview(reviewBody);
            dismiss();
        } else {
            reviewBody.setComment("");
            reviewBody.setHappy("1");
            reviewBody.setReview("0");
            sendReviewPresenter.sendReview(reviewBody);
            dismiss();
        }
    }
}
