package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Currency;
import com.ebizu.manis.model.snap.SnapData;
import com.ebizu.manis.preference.ManisSession;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 7/24/17.
 */

public class ViewHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = ViewHistoryAdapter.class.getSimpleName();

    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_PROGRESS = 1;
    public Context context;
    public ArrayList<SnapData> snapDatas;
    private Currency currency;
    private OnClickListener onClickListener;


    public ViewHistoryAdapter(Context context, OnClickListener onClickListener) {
        this.snapDatas = new ArrayList<>();
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_view_history, parent, false);
            vh = new ViewHistoryHolder(view);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.pagination_progress, parent, false);
            vh = new ProgressHolder(v);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SnapData viewHistoryDetail = this.snapDatas.get(position);
        switch (getItemViewType(position)) {
            case VIEW_TYPE_ITEM:
                ViewHistoryHolder viewHistoryHolder = (ViewHistoryHolder) holder;
                viewHistoryHolder.txtTime.setText(UtilManis.getTimeAgoMiddle(viewHistoryDetail.getUploaded(), context));
                viewHistoryHolder.txtMerchant.setText(UtilManis.getStoreName(viewHistoryDetail.getStore().getName()));
                viewHistoryHolder.txtLocation.setText(UtilManis.getStoreLocation(viewHistoryDetail.getStore().getName(), viewHistoryDetail.getStore().getAddress()));
                Log.d(TAG, "onBindViewHolderViewHistory: " + UtilManis.getStoreName(viewHistoryDetail.getStore().getName()));

                currency = new ManisSession(context).getAccountSession().getCurrency();
                viewHistoryHolder.txtCurrency.setText(currency.getIso2());
                if (currency != null && currency.getIso2().trim().equalsIgnoreCase("RM")) {
                    viewHistoryHolder.txtMoney.setText(UtilManis.formatMoney(viewHistoryDetail.getReceipt().getAmount()));
                } else {
                    viewHistoryHolder.txtMoney.setText(UtilManis.formatMoney(Math.round(viewHistoryDetail.getReceipt().getAmount())));
                }
                viewHistoryHolder.mainReason.setVisibility(View.GONE);
                viewHistoryHolder.mainReason.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBackgroundLayout));
                viewHistoryHolder.txtStatus.setText(viewHistoryDetail.getStatus().toLowerCase().split(" ")[0]);
                if (viewHistoryDetail.getStatusType() == SnapData.STATUS_PENDING) {
                    viewHistoryHolder.imgStatus.setImageResource(R.drawable.snap_history_pending);
                    viewHistoryHolder.linearAddpoint.setVisibility(View.GONE);
                } else if (viewHistoryDetail.getStatusType() == SnapData.STATUS_APPROVED) {
                    viewHistoryHolder.linearAddpoint.setVisibility(View.VISIBLE);
                    viewHistoryHolder.txtPoint.setText("+ " + viewHistoryDetail.getPoint() + " " + context.getString(R.string.menu_pts));
                    viewHistoryHolder.imgStatus.setImageResource(R.drawable.snap_history_accepted);
                } else if (viewHistoryDetail.getStatusType() == SnapData.STATUS_REJECTED) {
                    viewHistoryHolder.mainReason.setVisibility(View.VISIBLE);
                    viewHistoryHolder.txtReason.setText(viewHistoryDetail.getRemark());
                    viewHistoryHolder.imgStatus.setImageResource(R.drawable.snap_history_rejected);
                    viewHistoryHolder.linearAddpoint.setVisibility(View.GONE);
                } else if (viewHistoryDetail.getStatusType() == SnapData.STATUS_PENDING_APPEAL) {
                    viewHistoryHolder.mainReason.setVisibility(View.VISIBLE);
                    viewHistoryHolder.mainReason.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
                    viewHistoryHolder.txtReason.setText(context.getString(R.string.ds_txt_appeal));
                }
                viewHistoryHolder.itemView.setOnClickListener(v -> onClickListener.onSnapHistoryClickListener(viewHistoryDetail));
            case VIEW_TYPE_PROGRESS:
                break;
        }


    }

    @Override
    public int getItemCount() {
        return snapDatas.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (snapDatas.get(position) == null) {
            return VIEW_TYPE_PROGRESS;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    public void replaceViewHistory(ArrayList<SnapData> snapDatas) {
        this.snapDatas = snapDatas;
        notifyDataSetChanged();
    }

    public void addViewHistory(ArrayList<SnapData> snapDatas) {
        removeProgressView();
        this.snapDatas = snapDatas;
        notifyDataSetChanged();
    }

    public void addAllViewHistory(ArrayList<SnapData> snapDatas) {
        removeProgressView();
        this.snapDatas.addAll(snapDatas);
        notifyDataSetChanged();
    }

    public void addProgressBar() {
        snapDatas.add(null);
        notifyItemChanged(snapDatas.size());
    }

    public void removeProgressView() {
        this.snapDatas.removeAll(Collections.singleton(null));
        notifyDataSetChanged();
    }

    public void deleteAll() {
        this.snapDatas = new ArrayList<>();
        notifyDataSetChanged();
    }


    public boolean isEmpty() {
        return this.snapDatas.isEmpty();
    }

    public class ViewHistoryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lin_main)
        LinearLayout linearMain;
        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.txt_merchant)
        TextView txtMerchant;
        @BindView(R.id.txt_location)
        TextView txtLocation;
        @BindView(R.id.txt_currency)
        TextView txtCurrency;
        @BindView(R.id.txt_money)
        TextView txtMoney;
        @BindView(R.id.lin_addpoint)
        LinearLayout linearAddpoint;
        @BindView(R.id.txt_point)
        TextView txtPoint;
        @BindView(R.id.container_status)
        LinearLayout containerStatus;
        @BindView(R.id.img_status)
        ImageView imgStatus;
        @BindView(R.id.txt_status)
        TextView txtStatus;
        @BindView(R.id.main_reason)
        RelativeLayout mainReason;
        @BindView(R.id.txt_reason)
        TextView txtReason;
        @BindView(R.id.btn_appeal)
        Button btnAppeal;


        public ViewHistoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public class ProgressHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progress_Bar)
        ProgressBar progressBar;

        public ProgressHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnClickListener {
        void onSnapHistoryClickListener(SnapData snapData);
    }
}
