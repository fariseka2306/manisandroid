package com.ebizu.manis.view.manis.recyclerview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.mvp.snap.store.list.IListSnapStorePresenter;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.adapter.SnapStoreAdapter;

import java.util.ArrayList;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public class SnapStoreRecyclerView extends RecyclerView {

    private LinearLayoutManager linearLayoutManager;
    private SnapStoreAdapter snapStoreAdapter;

    public SnapStoreRecyclerView(Context context) {
        super(context);
    }

    public SnapStoreRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SnapStoreRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void initialize(BaseActivity baseActivity) {
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        snapStoreAdapter = new SnapStoreAdapter(baseActivity, new ArrayList<>());
        snapStoreAdapter.setOnClickItemListener(snapStore -> new AnalyticManager(getContext()).trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_SNAP_STORE,
                ConfigManager.Analytic.Action.ITEM_STORE,
                "Item Click ".concat("Item Store ").concat(snapStore.getName())
        ));
        setLayoutManager(linearLayoutManager);
        setAdapter(snapStoreAdapter);
        setHasFixedSize(false);
        setNestedScrollingEnabled(false);
    }

    public void setOnSnapAbleListener(IListSnapStorePresenter.OnCheckSnapAbleListener onCheckSnapAbleListener) {
        snapStoreAdapter.setOnCheckSnapAbleListener(onCheckSnapAbleListener);
    }

    public void addData(ArrayList<Store> snapStores) {
        snapStoreAdapter.addNearStores(snapStores);
    }

    public void replaceData(ArrayList<Store> snapStores) {
        snapStoreAdapter.replaceNearStores(snapStores);
    }

    public void showProgressitem() {
        snapStoreAdapter.loadProgress();
    }

    public void dismissProgressItem() {
        snapStoreAdapter.dissProgress();
    }

    public LinearLayoutManager getLinearLayoutManager() {
        return linearLayoutManager;
    }

    public SnapStoreAdapter getSnapStoreAdapter() {
        return snapStoreAdapter;
    }

    public boolean isEmptyStores() {
        return getSnapStoreAdapter().isEmptyStores();
    }

}
