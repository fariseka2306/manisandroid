package com.ebizu.manis.view.walkthrough.walkthroughpage;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by mac on 8/29/17.
 */

public abstract class WalkthroughPageView extends RelativeLayout implements IWalkthroughPage {

    protected OnNextPageListener onNextPageListener;
    private BaseActivity baseActivity;

    public WalkthroughPageView(Context context) {
        super(context);
        createView(context);
    }

    public WalkthroughPageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public WalkthroughPageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WalkthroughPageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    protected void createView(Context context) {

    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    @Override
    public BaseActivity getBaseActivity() {
        return baseActivity;
    }

    protected void setVisibleLayout(View view) {
        view.setVisibility(VISIBLE);
    }

    protected void startAnimeSlideRight(View view) {
        view.setVisibility(VISIBLE);
        Animation slideInRight = (AnimationUtils.loadAnimation(getContext(),
                R.anim.slide_in_right));
        slideInRight.setInterpolator(new AccelerateDecelerateInterpolator());
        view.startAnimation(slideInRight);
    }

    protected void startAnimFadeIn(View view) {
        view.setVisibility(VISIBLE);
        Animation fadeIn = AnimationUtils.loadAnimation(getContext(),
                R.anim.fade_in);
        fadeIn.setInterpolator(new AccelerateDecelerateInterpolator());
        view.startAnimation(fadeIn);
    }

    protected void startAnimFadeOut(View view) {
        Animation fadeOut = AnimationUtils.loadAnimation(getContext(),
                R.anim.fade_out);
        fadeOut.setInterpolator(new AccelerateDecelerateInterpolator());
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(fadeOut);
    }

    protected void startAnimSlideOutLeft(View view) {
        Animation slideOutLeft = AnimationUtils.loadAnimation(getContext(),
                R.anim.slide_out_left);
        slideOutLeft.setInterpolator(new AccelerateDecelerateInterpolator());
        slideOutLeft.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(slideOutLeft);
    }

    protected Animation animationFadeIn() {
        Animation fadeIn = AnimationUtils.loadAnimation(getContext(),
                R.anim.fade_in);
        fadeIn.setInterpolator(new AccelerateDecelerateInterpolator());
        return fadeIn;
    }

    protected Animation animationFadeOut() {
        Animation fadeOut = AnimationUtils.loadAnimation(getContext(),
                R.anim.fade_out);
        fadeOut.setInterpolator(new AccelerateDecelerateInterpolator());
        return fadeOut;
    }

    @Override
    public void onWalkthroughClick() {

    }

    @Override
    public void startPage() {

    }

    @Override
    public void finishPage() {

    }

    @Override
    public int page() {
        return 0;
    }

    @Override
    public void setOnNextPageListener(OnNextPageListener onNextPageListener) {
        this.onNextPageListener = onNextPageListener;
    }
}
