package com.ebizu.manis.view.dialog.term;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.Term;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.requestbodyv2.data.ManisTncData;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class TermDialog extends BaseDialogManis implements ITermDialog {

    @BindView(R.id.text_view_title)
    TextView textViewTitle;

    @BindView(R.id.text_view_negative_scenario)
    TextView textViewNegativeScenario;

    @BindView(R.id.web_view_terms)
    WebView webViewTerm;

    @BindView(R.id.button_close)
    Button buttonClose;

    private TermPresenter termPresenter;
    private boolean isManisLegal;

    public TermDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public TermDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected TermDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_term, null, false);
        ButterKnife.bind(this, view);
        getParent().addView(view);
        buttonClose.setOnClickListener(v -> dismiss());
        attachDialogPresenter(new TermPresenter());
        setWebViewSetting();
    }

    @Override
    public void attachDialogPresenter(BaseDialogPresenter baseDialogPresenter) {
        super.attachDialogPresenter(baseDialogPresenter);
        termPresenter = (TermPresenter) baseDialogPresenter;
        termPresenter.attachDialog(this);
    }

    @Override
    public void setViewTerm(Term term) {
        try {
            textViewNegativeScenario.setVisibility(View.GONE);
            textViewTitle.setVisibility(View.VISIBLE);
            webViewTerm.setVisibility(View.VISIBLE);
            textViewTitle.setText(term.getTitle());
            webViewTerm.loadData(URLEncoder.encode(term.getContent(), "utf-8").replaceAll("\\+", "%20"), "text/html", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setNegativeScenarioView(String message) {
        textViewNegativeScenario.setVisibility(View.VISIBLE);
        textViewTitle.setVisibility(View.GONE);
        webViewTerm.setVisibility(View.GONE);
        textViewNegativeScenario.setText(message);
        textViewNegativeScenario.setOnClickListener(view -> {
            onRetry();
        });
    }

    @Override
    public void onRetry() {
        super.onRetry();
        textViewNegativeScenario.setVisibility(View.GONE);
        getTermPresenter().loadTerm(setRequestBody(getContext()), isManisLegal
                ? ConfigManager.ManisLegal.TNC_PP
                : ConfigManager.ManisLegal.TNC_TOU);
    }

    public TermPresenter getTermPresenter() {
        return termPresenter;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        new AnalyticManager(getContext()).trackEvent(ConfigManager.Analytic.Category.DIALOG_TERMS,
                ConfigManager.Analytic.Action.CLICK, "Button Close");
    }

    private void setWebViewSetting() {
        final WebSettings webSettings = webViewTerm.getSettings();
        webSettings.setDefaultFontSize(10);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setAppCacheEnabled(false);
        webSettings.setBlockNetworkImage(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setGeolocationEnabled(false);
        webSettings.setNeedInitialFocus(false);
        webSettings.setSaveFormData(false);
    }

    public void setIsManisLegal(boolean isManisLegal) {
        this.isManisLegal = isManisLegal;
    }

    private RequestBody setRequestBody(Context context) {
        ManisTncData manisTncData = new ManisTncData();
        manisTncData.setType(isManisLegal
                ? ConfigManager.ManisLegal.TNC_TYPE_NON_SNE
                : ConfigManager.ManisLegal.TNC_TYPE_SNE);
        RequestBody requestBody = new RequestBodyBuilder(context)
                .setCommand(ConfigManager.ManisLegal.COMMAND_MANIS_LEGAL)
                .setFunction(ConfigManager.ManisLegal.FUNCTION_MANIS_LEGAL)
                .setData(manisTncData)
                .create();
        return requestBody;
    }
}