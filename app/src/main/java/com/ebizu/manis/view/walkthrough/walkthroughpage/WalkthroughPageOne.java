package com.ebizu.manis.view.walkthrough.walkthroughpage;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mac on 8/29/17.
 */

public class WalkthroughPageOne extends WalkthroughPageView {

    @BindView(R.id.walkthrough_first_page)
    RelativeLayout walkthroughFirstPage;
    @BindView(R.id.walkthrough_second_page)
    RelativeLayout walkthroughSecondPage;
    @BindView(R.id.imageview_phone_view)
    ImageView imageViewPhoneView;
    @BindView(R.id.imageview_snap_button_view)
    ImageView imageViewSnapButtonView;
    @BindView(R.id.imageview_fab_button)
    ImageView imageViewFabSnapButton;

    private AnimatorSet animatorSet;

    @BindView(R.id.button_ok)
    Button buttonOk;

    public WalkthroughPageOne(Context context) {
        super(context);
        createView(context);
    }

    public WalkthroughPageOne(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public WalkthroughPageOne(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WalkthroughPageOne(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.activity_walkthrough_page_one, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
    }

    @Override
    public int page() {
        return 1;
    }

    @Override
    public void startPage() {
        super.startPage();
        setVisibleLayout(this);
        walkthroughFirstPage.setVisibility(VISIBLE);
    }

    @Override
    public void finishPage() {
        super.finishPage();
        startAnimSlideOutLeft(this);
        onNextPageListener.onNextPage(page());
    }

    @Override
    public void setOnNextPageListener(OnNextPageListener onNextPageListener) {
        super.setOnNextPageListener(onNextPageListener);
        this.onNextPageListener = onNextPageListener;
        buttonOk.setOnClickListener((v -> {
            walkthroughFirstPage.setVisibility(GONE);
            walkthroughSecondPage.setVisibility(VISIBLE);
            showAnimation();
        }));
        imageViewFabSnapButton.setOnClickListener((v -> finishPage()));
    }

    private void showAnimation() {
        imageViewPhoneView.setY(imageViewPhoneView.getY() - getResources().getDimension(R.dimen.move_down_margin));
        ObjectAnimator movePhone = ObjectAnimator.ofFloat(imageViewPhoneView, "y", imageViewPhoneView.getY() - getResources().getDimension(R.dimen.move_down_margin), imageViewPhoneView.getY());
        movePhone.setDuration(700);
        ObjectAnimator scaleXPhone = ObjectAnimator.ofFloat(imageViewSnapButtonView, "scaleX", 0f);
        scaleXPhone.setDuration(700);
        ObjectAnimator scaleYPhone = ObjectAnimator.ofFloat(imageViewSnapButtonView, "scaleY", 0f);
        scaleYPhone.setDuration(700);
        animatorSet = new AnimatorSet();
        animatorSet.play(scaleXPhone).with(scaleYPhone).after(movePhone);
        animatorSet.start();
    }
}
