package com.ebizu.manis.view.dialog.missionsdialog;

import android.content.Context;
import android.view.View;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.manager.negativescenario.NegativeScenarioManager;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.rx.ResponseV2Subscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbodyv2.body.ThematicPostRB;
import com.ebizu.manis.service.manis.responsev2.status.Status;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperMissionExecute;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by FARIS_mac on 10/24/17.
 */

public class ThematicPresenter implements IThematicPresenter {

    private ManisApi manisApi;
    private Context context;
    private BaseActivity baseActivity;

    @Inject
    public ThematicPresenter() {

    }

    public ThematicPresenter(Context context, BaseActivity baseActivity) {
        this.context = context;
        this.baseActivity = baseActivity;
    }

    @Override
    public void executeMission(ThematicMissionsDialog thematicMissionsDialog, ThematicPostRB thematicPostRB) {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceMission(context);
        thematicMissionsDialog.progressBar.setVisibility(View.VISIBLE);
        thematicMissionsDialog.textViewPin.setVisibility(View.INVISIBLE);
        manisApi.postThematic(BuildConfig.MISSION_URL_PARAM, thematicPostRB)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseV2Subscriber<WrapperMissionExecute>(thematicMissionsDialog) {
                    @Override
                    protected void onErrorFailure(Status status) {
                        super.onErrorFailure(status);
                        thematicMissionsDialog.progressBar.setVisibility(View.INVISIBLE);
                        thematicMissionsDialog.executeFailed(status.getMessageClient());
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        thematicMissionsDialog.dismiss();
                        NegativeScenarioManager.show(message, baseActivity, NegativeScenarioManager.NegativeView.DIALOG);
                    }

                    @Override
                    protected void onSuccess(WrapperMissionExecute wrapperMissionExecute) {
                        super.onSuccess(wrapperMissionExecute);
                        thematicMissionsDialog.progressBar.setVisibility(View.INVISIBLE);
                        thematicMissionsDialog.showDialogSuccess(wrapperMissionExecute);
                    }
                });
    }
}
