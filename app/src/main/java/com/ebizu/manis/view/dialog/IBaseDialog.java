package com.ebizu.manis.view.dialog;

import android.content.DialogInterface;

import com.ebizu.manis.root.BaseActivity;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public interface IBaseDialog {

    void attachDialogPresenter(BaseDialogPresenter baseDialogPresenter);

    void showNoInternetConnection();

    void dismissNoInternetConnection();

    void showBaseProgressBar();

    void dismissBaseProgressBar();

    void onRetry();

    void setActivity(BaseActivity baseActivity);

    void showAlertDialog(String title,
                         String message,
                         boolean cancelAble,
                         int drawable,
                         String positive,
                         DialogInterface.OnClickListener positiveListener);

    BaseActivity getBaseActivity();

}
