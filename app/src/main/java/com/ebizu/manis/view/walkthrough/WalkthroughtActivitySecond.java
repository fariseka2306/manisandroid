package com.ebizu.manis.view.walkthrough;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.walkthrough.walkthroughpage.WalkthroughPageSix;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mac on 8/25/17.
 */

public class WalkthroughtActivitySecond extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.drawer)
    DrawerLayout drawerNotification;
    @BindView(R.id.walkthrough_eighth_page)
    RelativeLayout walthroughtEightPage;
    @BindView(R.id.imageview_corner_notification)
    ImageView imageViewNotification;
    @BindView(R.id.textView_notification_skip)
    TextView textViewSkipNotification;
    @BindView(R.id.base_background)
    View baseBackground;
    WalkthroughPageSix walkthroughPageSix;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough_page_five);
        ButterKnife.bind(this);
        imageViewNotification.setOnClickListener(this);
        textViewSkipNotification.setOnClickListener(this);
        walkthroughPageSix = (WalkthroughPageSix) getSupportFragmentManager().findFragmentById(R.id.right_slider);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && resultCode == ConfigManager.Walkthrough.WALKTHROUGH) ;
    }

    public void closeDrawer() {
        if (drawerNotification != null) {
            if (drawerNotification.isDrawerOpen(Gravity.END)) {
                drawerNotification.closeDrawer(Gravity.END);
                setResult(ConfigManager.Walkthrough.FINISH_WALK_THROUGH);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
            } else {
                drawerNotification.openDrawer(Gravity.END);
            }
        }
    }

    @Override
    public void onBackPressed() {
        //Do Nothing
    }

    @Override
    public void onClick(View v) {
        if (v.equals(imageViewNotification)) {
            if (drawerNotification != null && !drawerNotification.isDrawerOpen(Gravity.END)) {
                drawerNotification.openDrawer(Gravity.END);
                drawerNotification.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
                walthroughtEightPage.setVisibility(View.GONE);
                baseBackground.setVisibility(View.VISIBLE);
                walkthroughPageSix.showNinethPage();
            }
        } else if (v.equals(textViewSkipNotification)) {
            setResult(ConfigManager.Walkthrough.FINISH_WALK_THROUGH);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            finish();
        }
    }
}
