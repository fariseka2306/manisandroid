package com.ebizu.manis.view.dialog.transactionstatus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.model.purchasevoucher.Purchase;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.purchasevoucher.validation.TransactionValidation;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ebizu on 10/4/17.
 */

public class TransactionStatusDialog extends BaseDialogManis {

    @BindView(R.id.image_view_brand)
    ImageView imageViewBrand;
    @BindView(R.id.text_view_voucher_name)
    TextView textViewVoucherName;
    @BindView(R.id.text_view_payment_status)
    TextView textViewPaymentStatus;
    @BindView(R.id.image_view_payment_status)
    ImageView imageViewPaymentStatus;
    @BindView(R.id.text_view_voucher_id)
    TextView textViewVoucherId;
    @BindView(R.id.button_dismiss)
    Button buttonDismiss;

    private TransactionValidation transactionValidation;
    private OnActionListener onActionListener;
    private Purchase purchase;

    public TransactionStatusDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public TransactionStatusDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected TransactionStatusDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_transaction_payment, null, false);
        getParent().addView(view);
        ButterKnife.bind(this, view);
    }

    @Nullable
    public void setTransactionValidation(String statusPayment) {
        transactionValidation = new TransactionValidation(getContext(), statusPayment);
    }

    @Nullable
    public void setTransactionStatusView(RewardVoucher reward, Purchase purchase) {
        this.purchase = purchase;
        buttonDismiss.setText(getContext().getString(transactionValidation.successTrans() ?
                R.string.das_btn_awesome : R.string.ok));
        Glide.with(getContext())
                .load(reward.getImage128())
                .centerCrop()
                .into(imageViewBrand);
        textViewVoucherName.setText(reward.getName());
        textViewPaymentStatus.setText(
                transactionValidation.getTransactionStatus()
        );
        imageViewPaymentStatus.setImageDrawable(
                transactionValidation.getTransactionIcon()
        );
        if (null != purchase)
            textViewVoucherId.setText(purchase.getVoucherCode());
    }

    @OnClick(R.id.button_dismiss)
    void onClickDismiss() {
        if (transactionValidation.successTrans()) {
            onSuccessTrans(purchase);
        } else if (transactionValidation.pendingTrans()) {
            onPendingTrans(purchase);
        } else {
            dismissDialog();
        }
    }

    public void setOnActionListener(OnActionListener onActionListener) {
        this.onActionListener = onActionListener;
    }

    @Override
    public void cancel() {
        super.cancel();
        dismissDialog();
    }

    private void dismissDialog() {
        dismiss();
        if (onActionListener != null)
            onActionListener.onDismissDialog();
    }

    private void onSuccessTrans(Purchase purchase) {
        dismiss();
        if (onActionListener != null && purchase != null) {
            onActionListener.onSuccessTrans(purchase);
        } else {
            onActionListener.onDismissDialog();
        }
    }

    private void onPendingTrans(Purchase purchase) {
        dismiss();
        if (onActionListener != null && purchase != null) {
            onActionListener.onPendingTrans(purchase);
        } else {
            onActionListener.onDismissDialog();
        }
    }

    public interface OnActionListener {
        void onDismissDialog();

        void onSuccessTrans(Purchase purchase);

        void onPendingTrans(Purchase purchase);
    }
}
