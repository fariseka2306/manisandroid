package com.ebizu.manis.view.dialog.redeemdialogvoucher;

import com.ebizu.manis.service.manis.requestbody.RewardRedeemBody;

/**
 * Created by FARIS_mac on 10/3/17.
 */

public interface IRedeemRewardVoucherPresenter {

    void loadReward(RewardRedeemBody rewardRedeemBody);

}
