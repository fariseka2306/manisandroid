package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ebizu.manis.R;
import com.ebizu.manis.manager.multiplier.MultiplierMenuManager;
import com.ebizu.manis.model.Brand;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Raden on 7/6/17.
 */

public class BrandAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = BrandAdapter.class.getSimpleName();

    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_PROGRESS = 0;
    private Context context;
    private ArrayList<Brand> brands;
    private BrandAdapter.OnClickListener onClickListener;

    public BrandAdapter(Context context) {
        this.brands = new ArrayList<>();
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_brand_reward, parent, false);
        vh = new BrandHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Brand brand = brands.get(position);
        BrandHolder brandHolder = (BrandHolder) holder;


        Glide.with(context)
                .load(brand.image)
                .asBitmap()
                .thumbnail(0.1f)
                .animate(R.anim.fade_in_image)
                .placeholder(R.drawable.store_noimage)
                .centerCrop()
                .into(new BitmapImageViewTarget(brandHolder.brandPosterImg) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        brandHolder.brandPosterImg.setImageDrawable(circularBitmapDrawable);
                    }
                });

        if (brand.getMultiplier() != -1) {
            Drawable multiplierDraw = ContextCompat.getDrawable(context, MultiplierMenuManager.getDrawble(brand.getMultiplier()));
            brandHolder.txtFeatured.setText(MultiplierMenuManager.getText(brand.getMultiplier()));
            brandHolder.txtFeatured.setBackground(multiplierDraw);
            brandHolder.txtFeatured.setVisibility(View.VISIBLE);
        }

        brandHolder.relBrand.setOnClickListener(v -> {
            onClickListener.onBrandClickListener(brand);
        });

    }

    public void setOnClickListener(OnClickListener onClickListener) {
        if (this.onClickListener == null) {
            this.onClickListener = onClickListener;
        }
    }

    @Override
    public int getItemCount() {
        if (null == brands)
            return 0;
        return brands.size();
    }

    public void addBrands(ArrayList<Brand> brands) {
        this.brands = brands;
        notifyDataSetChanged();
    }

    public class BrandHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgViewBrandReward)
        CircleImageView brandPosterImg;
        @BindView(R.id.rel_brand)
        RelativeLayout relBrand;
        @BindView(R.id.txt_featured)
        TextView txtFeatured;


        public BrandHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface OnClickListener {
        void onBrandClickListener(Brand brand);
    }

}
