package com.ebizu.manis.view.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 8/2/17.
 */

public class FilterContentViewHolder extends RecyclerView.ViewHolder {

//    @BindView(R.id.ctv_content)
//    public CheckedTextView cekTxtContent;
    @BindView(R.id.img_reward_categories)
    public ImageView imgRewardCategories;
    public TextView txtCheck;
    public FrameLayout viewCheck;
    public ImageView imageViewPlus;
    public RelativeLayout relViewCheck;

    private Context context;

    public FilterContentViewHolder(View itemView, Context context) {
        super(itemView);
        ButterKnife.bind(itemView);
        this.context = context;

        viewCheck = (FrameLayout) itemView.findViewById(R.id.view_check);
        imageViewPlus = (ImageView) itemView.findViewById(R.id.image_view_plus);
        txtCheck = (TextView) itemView.findViewById(R.id.txt_check);
        relViewCheck = (RelativeLayout)itemView.findViewById(R.id.rel_view_check);
//        cekTxtContent = (CheckedTextView) itemView.findViewById(R.id.ctv_content);
        imgRewardCategories = (ImageView) itemView.findViewById(R.id.img_reward_categories);

    }
}
