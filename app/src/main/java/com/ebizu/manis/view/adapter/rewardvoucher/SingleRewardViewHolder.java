package com.ebizu.manis.view.adapter.rewardvoucher;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.AutoResizeTextView;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.manager.vouchertransactiontype.VoucherTransactionTypeManager;
import com.ebizu.sdk.reward.models.Reward;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SingleRewardViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.frpi_img_redeem)
    public ImageView ivReward;
    @BindView(R.id.frpi_img_bucket)
    public ImageView frpiImgBucket;
    @BindView(R.id.frpi_img_manis)
    public ImageView frpiImgManis;
    @BindView(R.id.frpi_txt_title)
    public TextView tvRewardName;
    @BindView(R.id.frpi_txt_category)
    public TextView tvProviderName;
    @BindView(R.id.frpi_txt_points)
    public AutoResizeTextView tvPoint;
    @BindView(R.id.frpi_txt_value)
    public AutoResizeTextView tvRewardValue;
    @BindView(R.id.frpi_txt_outofstock)
    public TextView tvOutOfStock;
    @BindView(R.id.rl_img_point)
    public LinearLayout rlImgPoint;
    @BindView(R.id.layout_point)
    public LinearLayout llPoint;
    @BindView(R.id.layout_value)
    public LinearLayout llValue;
    @BindView(R.id.rl_btn_free)
    public RelativeLayout rlBtnFree;

    private Reward item;
    private Context context;
    private RewardListener rewardListener;

    public SingleRewardViewHolder(View itemView, final RewardListener listener) {
        super(itemView);
        context = itemView.getContext();
        setOnRewardListener(listener);
        ButterKnife.bind(this, itemView);
    }

    public void setItem(Reward item) {
        this.item = item;
        tvRewardName.setText(item.getName());
        ImageUtils.loadImage(context, item.getImageLarge(), ContextCompat.getDrawable(context, R.drawable.default_pic_promo_details_pic_small), ivReward);
        tvOutOfStock.setVisibility(View.GONE);
        tvProviderName.setText(item.getProvider().getName());
        tvRewardValue.setText(item.getCurrency() + " " + (int) item.getVoucherPurchaseValue());
        tvPoint.setText(item.getPoint() + " " + context.getString(R.string.rd_txt_pts));
        VoucherTransactionTypeManager.checkCategoryVoucherTransactionType(this, item, context);

        if (item.getStock() < 1) {
            tvOutOfStock.setVisibility(View.VISIBLE);
        } else {
            rlImgPoint.setVisibility(View.VISIBLE);
            tvOutOfStock.setVisibility(View.GONE);
        }

        itemView.setOnClickListener(view -> {
            rewardListener.onRewardListener(this.item);
            new AnalyticManager(context).trackEvent(
                    ConfigManager.Analytic.Category.FRAGMENT_REWARD_POINT,
                    ConfigManager.Analytic.Action.ITEM_CLICK,
                    "Item Click".concat(" Item ").concat(item.getName()));
        });
    }

    private void setOnRewardListener(RewardListener rewardVoucherListener) {
        this.rewardListener = rewardVoucherListener;
    }

    public interface RewardListener {
        void onRewardListener(Reward reward);
    }
}
