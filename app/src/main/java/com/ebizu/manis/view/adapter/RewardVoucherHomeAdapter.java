package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ebizu.manis.R;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.sdk.reward.models.Reward;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Raden on 7/8/17.
 */

public class RewardVoucherHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = RewardVoucherHomeAdapter.class.getSimpleName();

    private final int VIEW_ITEM = 1;
    private Context context;
    private List<RewardVoucher> rewardVoucherList;
    private RewardVoucherListener rewardVoucherListener;

    public RewardVoucherHomeAdapter(Context context, List<RewardVoucher> rewardVoucherList) {
        this.context = context;
        this.rewardVoucherList = rewardVoucherList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reward_home, parent, false);
        vh = new RedeemHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RewardVoucher rewardVoucher = rewardVoucherList.get(position);
        RedeemHolder redeemHolder = (RedeemHolder) holder;

        Glide.with(context)
                .load(rewardVoucher.getImage128())
                .asBitmap()
                .thumbnail(0.1f)
                .animate(R.anim.fade_in_image)
                .placeholder(R.drawable.store_noimage)
                .centerCrop()
                .into(new BitmapImageViewTarget(redeemHolder.redeemPosterImg) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        redeemHolder.redeemPosterImg.setImageDrawable(circularBitmapDrawable);
                    }
                });

        redeemHolder.txtPointRewardBrand.setVisibility(View.VISIBLE);
        redeemHolder.txtPointRewardBrand.setText(String.valueOf(rewardVoucher.getPoint()));

        redeemHolder.relReward.setOnClickListener(v -> rewardVoucherListener.onRewardClickListener(rewardVoucher));
    }

    @Override
    public int getItemCount() {
        return rewardVoucherList.size();
    }

    public Object getItem(int position) {
        return rewardVoucherList.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_ITEM;
    }

    public void addRewardVoucher(List<RewardVoucher> rewardVoucher) {
        this.rewardVoucherList = rewardVoucher;
        notifyDataSetChanged();
    }

    public void setRewardOnClick(RewardVoucherListener rewardVoucherListener){
        this.rewardVoucherListener = rewardVoucherListener;
    }

    public class RedeemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgViewBrandReward)
        CircleImageView redeemPosterImg;
        @BindView(R.id.txtPointReward)
        TextView txtPointRewardBrand;
        @BindView(R.id.rel_brand)
        RelativeLayout relReward;

        public RedeemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface RewardVoucherListener{
        void onRewardClickListener(RewardVoucher rewardVoucher);
    }
}
