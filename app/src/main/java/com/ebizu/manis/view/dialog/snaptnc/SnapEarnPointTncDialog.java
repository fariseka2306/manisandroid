package com.ebizu.manis.view.dialog.snaptnc;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.snap.SnapTerm;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ebizu on 10/11/17.
 */

public class SnapEarnPointTncDialog extends SnapTncDialog {

    @BindView(R.id.text_view_point)
    TextView textViewPoint;
    @BindView(R.id.text_view_validity)
    TextView textViewValidity;
    @BindView(R.id.text_view_receipt_clarity)
    TextView textViewReceiptClarity;
    @BindView(R.id.text_view_limit)
    TextView textViewLimit;

    private SnapTerm snapTerm;

    public SnapEarnPointTncDialog(@NonNull Context context) {
        super(context);
    }

    public SnapEarnPointTncDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected SnapEarnPointTncDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void addDialogView(Context context) {
        super.addDialogView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_snap_earn_point_tnc, null, false);
        getParent().addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
        setSnapEarnPointTncView(getSnapTerm());
    }

    @OnClick(R.id.btn_ok)
    void onCLickOk() {
        if (null != onAcceptTncListener)
            onAcceptTncListener.onAccept();
        dismiss();
    }

    @OnClick(R.id.btn_always_accept)
    void onClickAlwaysAccept() {
        if (null != onAcceptTncListener)
            onAcceptTncListener.onAccept();
        getManisSession().acceptAlwaysTncSnapEarnPoint();
        dismiss();
    }

    private void setSnapEarnPointTncView(SnapTerm snapTerm) {
        if (null != snapTerm) {
            textViewPoint.setText(snapTerm.getPoint());
            textViewLimit.setText(snapTerm.getLimit());
            textViewValidity.setText(snapTerm.getValidity());
        }
    }

    public SnapTerm getSnapTerm() {
        if (null == snapTerm)
            snapTerm = new Gson().fromJson(getManisSession().getTermsSnapEarnPoints(), SnapTerm.class);
        return snapTerm;
    }
}