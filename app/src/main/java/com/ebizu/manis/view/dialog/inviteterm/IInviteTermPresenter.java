package com.ebizu.manis.view.dialog.inviteterm;

import android.content.Context;

import com.ebizu.manis.view.dialog.IDialogPresenter;

/**
 * Created by abizu-alvio on 8/14/2017.
 */

public interface IInviteTermPresenter extends IDialogPresenter {

    void loadInviteTerm(Context context);

    void loadInviteTerm();
}
