package com.ebizu.manis.view.dialog.review.reviewdialog;

import com.ebizu.manis.root.IBaseActivity;
import com.ebizu.manis.service.manis.response.ResponseData;

/**
 * Created by mac on 8/24/17.
 */

public interface IReviewDialog extends IBaseActivity{

    void setReview(ResponseData responseData);

}
