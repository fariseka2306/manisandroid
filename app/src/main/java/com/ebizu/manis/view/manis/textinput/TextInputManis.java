package com.ebizu.manis.view.manis.textinput;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.view.KeyEvent;

/**
 * Created by Ebizu-User on 10/08/2017.
 */

public class TextInputManis extends TextInputEditText {

    private OnSoftInputDismiss onSoftInputDismiss;

    public TextInputManis(Context context) {
        super(context);
    }

    public TextInputManis(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextInputManis(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (onSoftInputDismiss != null)
                onSoftInputDismiss.onDismiss(keyCode, event);
            return false;
        }
        return super.dispatchKeyEvent(event);
    }

    public void setOnSoftInputListener(OnSoftInputDismiss onSoftInputListener) {
        this.onSoftInputDismiss = onSoftInputListener;
    }

    public interface OnSoftInputDismiss {
        void onDismiss(int keyCode, KeyEvent keyEvent);
    }
}
