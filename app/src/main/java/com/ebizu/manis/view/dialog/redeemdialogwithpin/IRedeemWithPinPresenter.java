package com.ebizu.manis.view.dialog.redeemdialogwithpin;

import com.ebizu.manis.service.reward.requestbody.PinValidationBody;

/**
 * Created by FARIS_mac on 10/9/17.
 */

public interface IRedeemWithPinPresenter {

    void pinValidationPost(PinValidationBody pinValidationBody);

}
