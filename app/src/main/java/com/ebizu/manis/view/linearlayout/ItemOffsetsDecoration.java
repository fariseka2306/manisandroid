package com.ebizu.manis.view.linearlayout;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Raden on 7/26/17.
 */

public class ItemOffsetsDecoration extends RecyclerView.ItemDecoration  {
    public int mItemOffset;

    ItemOffsetsDecoration(int itemOffset) {
        mItemOffset = itemOffset;
    }

    public ItemOffsetsDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId));
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
    }
}
