package com.ebizu.manis.view.dialog.redeemdialogvoucher;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.RedeemCloseBody;
import com.ebizu.manis.service.manis.requestbody.RedeemCloseFailedBody;
import com.ebizu.manis.service.manis.requestbody.RewardRedeemBody;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.service.manis.response.WrapperRedeemCloseFailed;
import com.ebizu.manis.service.manis.response.WrapperRewardRedeem;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;
import com.ebizu.sdk.reward.EbizuReward;
import com.ebizu.sdk.reward.models.Redeem;
import com.ebizu.sdk.reward.models.Reward;
import com.ebizu.sdk.reward.models.SessionData;
import com.ebizu.sdk.reward.models.VoucherInput;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by FARIS_mac on 10/3/17.
 */

public class RedeemVoucherPresenterPresenter extends BaseDialogPresenter implements IRedeemRewardVoucherPresenter {

    private String TAG = getClass().getSimpleName();
    private RedeemVoucherDialog redeemVoucherDialog;
    private ManisApi manisApi;
    private List<VoucherInput> voucherInputs = new ArrayList<>();
    private Context context;

    public RedeemVoucherPresenterPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void attachDialog(BaseDialogManis baseDialogManis) {
        super.attachDialog(baseDialogManis);
        redeemVoucherDialog = (RedeemVoucherDialog) baseDialogManis;
    }


    @Override
    public void loadReward(RewardRedeemBody rewardRedeemBody) {
        redeemVoucherDialog.getBaseActivity().showProgressBarDialog(context.getString(R.string.please_wait));
        redeemVoucherDialog.showBaseProgressBar();
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceWithToken(redeemVoucherDialog.getContext());
        manisApi.getRewardRedeem(new Gson().toJson(rewardRedeemBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperRewardRedeem>(redeemVoucherDialog) {
                    @Override
                    public void onNext(WrapperRewardRedeem wrapperRewardRedeem) {
                        super.onNext(wrapperRewardRedeem);
                        redeemVoucherDialog.setViewRedeem(wrapperRewardRedeem.getRewardRedeem().getRefcode());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        redeemVoucherDialog.dismissBaseProgressBar();
                        if (!ConnectionDetector.isNetworkConnected(redeemVoucherDialog.getContext()))
                            redeemVoucherDialog.showNoInternetConnection();
                    }
                });
    }

    public void setRewardUser(final String refCode, RewardVoucher reward) {
        ManisSession manisSession = new ManisSession(context);
        EbizuReward.getSession(context).login(manisSession.getAccountSession().getAccId(), manisSession.getAccountSession().getAccScreenName(),
                manisSession.getAccountSession().getAccFacebookEmail(), manisSession.getAccountSession().getAccCountry(), manisSession.getAccountSession().getAccCreatedDateTime(),
                new com.ebizu.sdk.reward.core.interfaces.Callback<SessionData>() {

                    @Override
                    public void onSuccess(SessionData sessionData) {
                        Log.i("SetRewardUser", "Reward set up user success");
                        callRewardRedeem(refCode, reward);
                    }

                    @Override
                    public void onFailure(String s) {
                        UtilManis.info(context, context.getString(R.string.text_error_occured), s);
                        redeemVoucherDialog.dismissBaseProgressBar();
                        Log.e("SetRewardUser", "Reward set up user failed because " + s);
                    }
                });
    }

    private void callRewardRedeem(String refCode, RewardVoucher reward) {
        EbizuReward.getRedemption(context).redeem(reward.getId(), refCode, voucherInputs, new com.ebizu.sdk.reward.core.interfaces.Callback<Redeem>() {
            @Override
            public void onSuccess(Redeem response) {
                if (response != null) {
                    ManisSession manisSession = new ManisSession(context);
                    int point = manisSession.getPointSession().getPoint() - reward.getPoint();
                    manisSession.setPoint(point);
                    redeemVoucherDialog.showDialogSuccess(response);
                    redeemClose(refCode, response, reward);

                    HashMap<String, String> eventData = new HashMap<>();
                    eventData.put(UtilStatic.MANIS_KEY_TYPE, UtilStatic.MANIS_TYPE_REWARD);
                    eventData.put(UtilStatic.MANIS_KEY_ITEM_ID, reward.getId());
                    eventData.put(UtilStatic.MANIS_KEY_POINT, (Long.valueOf(point)).toString());
                    eventData.put(UtilStatic.MANIS_KEY_STORE_ID, UtilStatic.MANIS_EBIZU_STORE_ID);
                    eventData.put(UtilStatic.MANIS_KEY_STORE_NAME, UtilStatic.MANIS_EBIZU_STORE_NAME);
                    eventData.put(UtilStatic.MANIS_KEY_STORE_CATEGORY, UtilStatic.MANIS_EBIZU_STORE_CATEGORY);
                    eventData.put(UtilStatic.MANIS_KEY_STORE_CATEGORY_ID, UtilStatic.MANIS_EBIZU_STORE_CATEGORY_ID);
                    UtilManis.ebizuTrackCustomEvent(context, UtilStatic.MANIS_EVENT_REDEEMED, eventData);
                }
            }

            @Override
            public void onFailure(String s) {
                Log.e(TAG, "Redeemption : " + s);
                redeemCloseFailed(reward.getId(), refCode, reward.getPoint());
                UtilManis.info(context, context.getString(R.string.text_error_occured), s);
                redeemVoucherDialog.getBaseActivity().dismissProgressBarDialog();
            }
        });
    }

    private void redeemClose(String refCode, Redeem respose, RewardVoucher reward) {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceWithToken(redeemVoucherDialog.getContext());

        RedeemCloseBody redeemCloseBody = new RedeemCloseBody();
        redeemCloseBody.setId(reward.getId());
        redeemCloseBody.setRef(refCode);
        redeemCloseBody.setCode(respose.getCode());
        redeemCloseBody.setSn(respose.getSn());
        redeemCloseBody.setTrx(respose.getTransactionId());
        redeemCloseBody.setExpired(reward.getExpired());
        redeemCloseBody.setVoucher_image(reward.getImage128());
        redeemCloseBody.setReward_name(reward.getName());
        redeemCloseBody.setCom_name(reward.getProvider().getName());
        redeemCloseBody.setReward_type(reward.getType());
        redeemCloseBody.setPoint(reward.getPoint());
        redeemCloseBody.setTrackLink(respose.getTrackLink());

        manisApi.getRedeemClose(new Gson().toJson(redeemCloseBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<ResponseData>(redeemVoucherDialog) {
                    @Override
                    public void onNext(ResponseData responseData) {
                        super.onNext(responseData);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        redeemVoucherDialog.dismissBaseProgressBar();
                        if (!ConnectionDetector.isNetworkConnected(redeemVoucherDialog.getContext()))
                            redeemVoucherDialog.showNoInternetConnection();
                    }
                });
    }

    private void redeemCloseFailed(final String id, final String ref, final long point) {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceWithToken(redeemVoucherDialog.getContext());

        RedeemCloseFailedBody redeemCloseFailedBody = new RedeemCloseFailedBody();
        redeemCloseFailedBody.setId(id);
        redeemCloseFailedBody.setRef(ref);
        redeemCloseFailedBody.setPoint(point);

        manisApi.getRedeemCloseFailed(new Gson().toJson(redeemCloseFailedBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<WrapperRedeemCloseFailed>(redeemVoucherDialog) {
                    @Override
                    public void onNext(WrapperRedeemCloseFailed wrapperRedeemCloseFailed) {
                        super.onNext(wrapperRedeemCloseFailed);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        redeemVoucherDialog.dismissBaseProgressBar();
                        if (!ConnectionDetector.isNetworkConnected(redeemVoucherDialog.getContext()))
                            redeemVoucherDialog.showNoInternetConnection();
                    }
                });
    }

}
