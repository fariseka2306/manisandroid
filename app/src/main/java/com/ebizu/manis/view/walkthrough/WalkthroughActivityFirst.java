package com.ebizu.manis.view.walkthrough;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.walkthrough.walkthroughpage.WalkthroughPageFour;
import com.ebizu.manis.view.walkthrough.walkthroughpage.WalkthroughPageManager;
import com.ebizu.manis.view.walkthrough.walkthroughpage.WalkthroughPageOne;
import com.ebizu.manis.view.walkthrough.walkthroughpage.WalkthroughPageThree;
import com.ebizu.manis.view.walkthrough.walkthroughpage.WalkthroughPageTwo;
import com.ebizu.manis.view.walkthrough.walkthroughpage.WalkthroughPageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mac on 8/29/17.
 */

public class WalkthroughActivityFirst extends BaseActivity {


    @BindView(R.id.walkthrough_page_one_view)
    WalkthroughPageOne walkthroughPageOne;
    @BindView(R.id.walkthrough_page_two_view)
    WalkthroughPageTwo walkthroughPageTwo;
    @BindView(R.id.walkthrough_page_three_view)
    WalkthroughPageThree walkthroughPageThree;
    @BindView(R.id.walkthrough_page_four_view)
    WalkthroughPageFour walkthroughPageFour;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough_main);
        ButterKnife.bind(this);
        walkthroughPageFour.setActivity(this);
        WalkthroughPageView[] walkThroughPageViews = new WalkthroughPageView[]{
                walkthroughPageOne, walkthroughPageTwo, walkthroughPageThree, walkthroughPageFour
        };
        WalkthroughPageManager walkthroughPageManager = new WalkthroughPageManager(walkThroughPageViews);
        walkthroughPageManager.startTutorial(1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && resultCode == ConfigManager.Walkthrough.WALKTHROUGH) ;
    }

    @Override
    public void onBackPressed() {
        // Do Nothing
    }

    @OnClick(R.id.textView_skip)
    void onClickSkip() {
        setResult(ConfigManager.Walkthrough.FINISH_WALK_THROUGH);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }
}
