package com.ebizu.manis.view.holder;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 11/29/17.
 */

public class InviteTermHolder extends BaseHolder<String> {

    @BindView(R.id.textview_term)
    TextView txtTerm;

    public InviteTermHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void setHolderView(String model) {
        try {
            if (model != null) txtTerm.setText(model);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "setHolderView: ".concat(e.getMessage()));
        }
    }
}
