package com.ebizu.manis.view.dialog.offerclaim;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Offer;
import com.ebizu.manis.model.Redeem;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.model.StoreDetail;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 10/26/17.
 */

public class OfferClaimDialog extends BaseDialogManis implements View.OnClickListener {

    private ViewGroup viewGroupDialog;

    @BindView(R.id.dr_img_merchant)
    ImageView drImgMerchant;

    @BindView(R.id.dr_txt_title)
    TextView drTxtTitle;

    @BindView(R.id.dr_txt_sn)
    TextView drTxtSn;

    @BindView(R.id.dr_img_barcode)
    ImageView drImgBarcode;

    @BindView(R.id.dr_btn_mark)
    Button drBtnMark;

    @BindView(R.id.dr_btn_close)
    Button drBtnClose;

    public OfferClaimDialog(@NonNull Context context) {
        super(context);
        createView(context);
    }

    public OfferClaimDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        createView(context);
    }

    protected OfferClaimDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        createView(context);
    }

    public void setOfferClaimDialogView(Redeem redeem, Offer offer, Store store) {
        UtilManis.createBarCode(redeem.getCode(), drImgBarcode);
        drTxtSn.setText(redeem.getCode());
        Glide.with(getContext())
                .load(store.getAssets().getPhoto())
                .thumbnail(0.1f)
                .fitCenter()
                .placeholder(R.drawable.default_pic_rewards_tile_img)
                .animate(R.anim.fade_in_image)
                .into(drImgMerchant);
        drTxtTitle.setText(offer.getTitle());
    }

    private void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_offer_claim, null, false);
        getParent().addView(view);
        ButterKnife.bind(this, view);
        drBtnMark.setVisibility(View.GONE);
        initListener();
    }

    private void initListener() {
        drBtnMark.setOnClickListener(this);
        drBtnClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        dismiss();
    }
}
