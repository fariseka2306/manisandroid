package com.ebizu.manis.view.holder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.GPSTracker;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.manager.multiplier.MultiplierMenuManager;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.mvp.snap.store.SnapStoreHelper;
import com.ebizu.manis.preference.LocationSession;
import com.ebizu.manis.service.manis.requestbody.snap.ReceiptStore;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public class SnapStoreHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.parent)
    ConstraintLayout parent;
    @BindView(R.id.image_view_store)
    ImageView imageViewStore;
    @BindView(R.id.text_view_distance)
    TextView textViewDistance;
    @BindView(R.id.text_view_name_store)
    TextView textViewStore;
    @BindView(R.id.text_view_place)
    TextView textViewPlace;
    @BindView(R.id.text_view_category)
    TextView textViewCategory;
    @BindView(R.id.text_view_multiplier)
    TextView textViewMultiplier;

    private Context context;
    private Store store;
    private LocationSession locationSession;

    public SnapStoreHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        context = itemView.getContext();
        locationSession = new LocationSession(itemView.getContext());
    }

    public void setSnapStore(Store store) {
        this.store = store;
        ImageUtils.loadImage(context, store.getAssets().getPhoto(),
                ContextCompat.getDrawable(context, R.drawable.default_pic_store_logo), imageViewStore);

        String[] splitStoreName = store.getName().split("@");
        String storeName = splitStoreName[0];
        String storeAddress = "";
        if (splitStoreName.length > 1) {
            storeAddress = "@" + splitStoreName[1];
        }
        textViewStore.setText(storeName);
        textViewPlace.setText(storeAddress);
        textViewCategory.setText(store.getCategory().getName());
        setDistanceView();
        setMultiplierView(store.getMerchantTier());
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        parent.setOnClickListener((v) -> onClickListener.onClick(SnapStoreHelper.createReceiptStore(store)));
    }

    public interface OnClickListener {
        void onClick(ReceiptStore receiptStore);
    }

    private void setMultiplierView(String merchantTier) {
        if (store.getMultiplier() != -1) {
            Drawable multiplierDraw = ContextCompat.getDrawable(context, MultiplierMenuManager.getDrawble(store.getMultiplier(), merchantTier));
            textViewMultiplier.setText(MultiplierMenuManager.getText(store.getMultiplier(), merchantTier));
            textViewMultiplier.setBackground(multiplierDraw);
            textViewMultiplier.setVisibility(View.VISIBLE);
        }
    }

    private void setDistanceView() {
        if (store.getCoordinate().getDistance() == 0) {
            double distance = GPSTracker.distance(locationSession.getLat(), locationSession.getLong(),
                    store.getCoordinate().getLatitude(), store.getCoordinate().getLongitude());
            textViewDistance.setText(distanceToString(distance));
        } else {
            textViewDistance.setText(distanceToString(store.getCoordinate().getDistance()));
        }
    }

    private String distanceToString(Double distance) {
        DecimalFormat df = new DecimalFormat("0.#");
        if (distance >= 1100) {
            return df.format(distance / 1000) + " km";
        } else {
            return df.format(distance) + " m";
        }
    }
}
