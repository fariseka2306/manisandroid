package com.ebizu.manis.view.adapter.account;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.manager.accountlistsettingsabout.AccountListSettingsAbout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/21/2017.
 */

public class AccountListSettingAboutAdapter extends RecyclerView.Adapter<AccountListSettingAboutAdapter.ViewHolder> {

    Context context;
    Activity activity;
    private AccountMenuListener accountListner;
    private List<AccountListSettingsAbout> modelAccountList;

    public AccountListSettingAboutAdapter(Context context, List<AccountListSettingsAbout> modelAccountList, Activity activity) {
        this.context = context;
        this.modelAccountList = modelAccountList;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_settings_about, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        AccountListSettingsAbout accountList = modelAccountList.get(position);
        viewHolder.name.setText(accountList.name());
        viewHolder.build.setText(accountList.build(activity));
        viewHolder.parentAccount.setOnClickListener((v -> accountListner.onClick(accountList)));

    }

    public void addAccountList(AccountListSettingsAbout accountListSettingsAbout) {
        this.modelAccountList.add(accountListSettingsAbout);
        notifyDataSetChanged();
    }

    public void setAccountListener(AccountMenuListener accountListener) {
        this.accountListner = accountListener;
    }

    @Override
    public int getItemCount() {
        return modelAccountList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_name)
        TextView name;
        @BindView(R.id.text_view_build)
        TextView build;
        @BindView(R.id.parent_account_list_about)
        LinearLayout parentAccount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AccountMenuListener {
        void onClick(AccountListSettingsAbout accountListSettingsAbout);
    }
}