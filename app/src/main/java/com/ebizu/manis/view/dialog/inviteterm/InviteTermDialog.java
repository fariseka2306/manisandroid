package com.ebizu.manis.view.dialog.inviteterm;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.model.InviteTerm;
import com.ebizu.manis.view.adapter.InviteTermAdapter;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;

import org.json.JSONException;
import org.json.JSONObject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;

/**
 * Created by abizu-alvio on 8/14/2017.
 */

public class InviteTermDialog extends BaseDialogManis implements IInviteTermDialog, View.OnClickListener {

    private RelativeLayout parent;
    private TextView textViewDescription;
    private RecyclerView listViewTerm;
    private Button buttonOk;
    private Button buttonGotIt;

    private String BRANCH_TEST_BAD = "BranchTestBad";
    private InviteTermPresenter inviteTermPresenter;

    public InviteTermDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }


    public InviteTermDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected InviteTermDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_invite_term, null, false);
        getParent().addView(view, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        parent = (RelativeLayout) view.findViewById(R.id.parent);
        textViewDescription = (TextView) view.findViewById(R.id.textview_desc);
        listViewTerm = (RecyclerView) view.findViewById(R.id.listview_item);
        buttonOk = (Button) view.findViewById(R.id.button_ok);
        buttonGotIt = (Button) view.findViewById(R.id.button_gotit);
        buttonOk.setOnClickListener(this);
        buttonGotIt.setOnClickListener(this);
        attachDialogPresenter(new InviteTermPresenter());
    }

    @Override
    public void attachDialogPresenter(BaseDialogPresenter baseDialogPresenter) {
        super.attachDialogPresenter(baseDialogPresenter);
        inviteTermPresenter = (InviteTermPresenter) baseDialogPresenter;
        inviteTermPresenter.attachDialog(this);
    }

    @Override
    public void setViewInvite(InviteTerm inviteTerm) {
        dismissBaseProgressBar();
        textViewDescription.setText(UtilManis.fromHtml(inviteTerm.getDescription()));
        InviteTermAdapter adapterInviteTerms = new InviteTermAdapter(getContext(), inviteTerm.getTerms());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        adapterInviteTerms.addAdapterList(inviteTerm.getTerms());
        listViewTerm.setAdapter(adapterInviteTerms);
        listViewTerm.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onRetry() {
        super.onRetry();
        getInviteTermPresenter().loadInviteTerm(getContext());
    }

    public InviteTermPresenter getInviteTermPresenter() {
        return inviteTermPresenter;
    }

    public void generateBranch() {
        JSONObject obj = new JSONObject();
        try {
            obj.put(getBaseActivity().getString(R.string.branch_key_name), getBaseActivity().getString(R.string.branch_value_name));
            obj.put(getBaseActivity().getString(R.string.branch_key_auto_deeplink_key1), getBaseActivity().getString(R.string.branch_value_auto_deeplink_key_1));
            obj.put(getBaseActivity().getString(R.string.branch_key_message), getBaseActivity().getString(R.string.branch_key_message));
            obj.put(getBaseActivity().getString(R.string.branch_key_og_title), getBaseActivity().getString(R.string.branch_value_og_title));
            obj.put(getBaseActivity().getString(R.string.branch_key_og_desc), getBaseActivity().getString(R.string.branch_value_og_desc));
            obj.put(getBaseActivity().getString(R.string.branch_key_og_image_url), getBaseActivity().getString(R.string.branch_value_og_image_url));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        new Branch.ShareLinkBuilder(getBaseActivity(), obj)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.MESSAGE)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.TWITTER)
                .setMessage(getBaseActivity().getString(R.string.branch_message))
                .setSubject(getBaseActivity().getString(R.string.branch_subject))
                .setStage(getBaseActivity().getString(R.string.branch_stage))
                .setFeature(getBaseActivity().getString(R.string.branch_feature))
                .addTag(getBaseActivity().getString(R.string.branch_tag1))
                .addTag(getBaseActivity().getString(R.string.branch_tag2))
                .setDefaultURL(getBaseActivity().getString(R.string.branch_default_url))
                .setCallback(new Branch.BranchLinkShareListener() {
                    @Override
                    public void onShareLinkDialogLaunched() {
                        Log.i(BRANCH_TEST_BAD, "onShareLinkDialogLaunched()");
                    }

                    @Override
                    public void onShareLinkDialogDismissed() {
                        Log.i(BRANCH_TEST_BAD, "onShareLinkDialogDismissed()");
                    }

                    @Override
                    public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {
                        if (error != null) {
                            Log.i(BRANCH_TEST_BAD, "onLinkShareResponse... " + sharedLink + " " + sharedChannel + " " + error.getMessage());
                        } else {
                            Log.i(BRANCH_TEST_BAD, "onLinkShareResponse... " + sharedLink + " " + sharedChannel);
                        }
                    }

                    @Override
                    public void onChannelSelected(String channelName) {
                        Log.i(BRANCH_TEST_BAD, "onChannelSelected... " + channelName);
                    }
                })
                .shareLink();
    }

    @Override
    public void onClick(View v) {
        if (v.equals(buttonOk)) {
            dismiss();
            new Handler().postDelayed(() -> generateBranch(), 500);
        } else {
            SharedPreferences prefFirst = getContext().getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
            UtilSessionFirstIn.setShowInviteTerms(prefFirst, false);
            dismiss();
            new Handler().postDelayed(() -> generateBranch(), 500);
        }
    }
}
