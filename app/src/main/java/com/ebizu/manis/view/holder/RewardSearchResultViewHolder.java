package com.ebizu.manis.view.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;

/**
 * Created by halim_ebizu on 9/11/17.
 */

public class RewardSearchResultViewHolder extends RecyclerView.ViewHolder {
    public TextView tvRewardName;
    public TextView tvProviderName;
    public TextView tvPoint;
    public TextView tvRewardValue;
    public TextView tvOutOfStock;
    public ImageView ivReward;

    public RewardSearchResultViewHolder(View itemView) {
        super(itemView);
        ivReward = (ImageView) itemView.findViewById(R.id.frpi_img_redeem);
        tvRewardName = (TextView) itemView.findViewById(R.id.frpi_txt_title);
        tvProviderName = (TextView) itemView.findViewById(R.id.frpi_txt_category);
        tvPoint = (TextView) itemView.findViewById(R.id.frpi_txt_points);
        tvRewardValue = (TextView) itemView.findViewById(R.id.frpi_txt_value);
        tvOutOfStock = (TextView) itemView.findViewById(R.id.frpi_txt_outofstock);
    }
}
