package com.ebizu.manis.view.dialog.review.reviewdialog;

import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.requestbody.ReviewBody;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public interface IDialogReviewPresenter {

    void attachDialog(DialogReview dialogReview);

    void sendReview(ReviewBody reviewBody);
}
