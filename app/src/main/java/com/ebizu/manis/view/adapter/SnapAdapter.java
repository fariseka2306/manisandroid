package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.Currency;
import com.ebizu.manis.model.snap.SnapData;
import com.ebizu.manis.preference.ManisSession;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 7/6/17.
 */

public class SnapAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = SnapAdapter.class.getSimpleName();

    private Context context;
    private ArrayList<SnapData> snapDatas;
    private SnapAdapter.onClickListener onClickListener;

    public SnapAdapter(Context context) {
        this.snapDatas = new ArrayList<>();
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_snap, parent, false);
        vh = new SnapAdapter.SnapHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SnapData snapData = snapDatas.get(position);
        SnapHolder snapHolder = (SnapHolder) holder;

        //snapHolder.txtMerchantCategory.setText(snapData.store.category.name);
        //Log.d(TAG, "onBindViewHolder: " + snapData.store.category.name);
        snapHolder.txtMerchantCategory.setText(snapData.getStore().getCategory().getName());
        Log.d(TAG, "onBindViewHolder: " + snapData.getStore().getCategory().getName());
        String delims = "[@]";
        String[] parse = snapData.getStore().getName().split(delims);
        snapHolder.txtMerchantName.setText(parse[0].trim());


        if (snapData.statusType == com.ebizu.manis.sdk.entities.SnapData.STATUS_APPROVED) {
            snapHolder.imgStatSnap.setImageResource(R.drawable.snap_history_accepted);
        } else if (snapData.statusType == com.ebizu.manis.sdk.entities.SnapData.STATUS_REJECTED) {
            snapHolder.imgStatSnap.setImageResource(R.drawable.snap_history_rejected);
        } else {
            snapHolder.imgStatSnap.setImageResource(R.drawable.snap_history_pending);
        }
        snapHolder.txtTime.setText(UtilManis.getTimeAgo(snapData.uploaded, context));

        Currency currency = new ManisSession(context).getAccountSession().getCurrency();
        snapHolder.txtCurrentSnap.setText(currency.getIso2());
        if (currency != null && currency.getIso2().trim().equalsIgnoreCase("RM")) {
            snapHolder.txtPointSnap.setText(UtilManis.formatMoney(snapData.getReceipt().getAmount()));
//            snapHolder.txtPointSnap.setText(String.valueOf(snapData.receipt.amount));
//            snapHolder.txtPointSnap.setText(Long.toString(Math.round(snapData.receipt.amount)));
//            snapHolder.txtPointSnap.setText(UtilManis.longToLocaleNumberFormat(Math.round(snapData.receipt.amount)));
        } else {
            snapHolder.txtPointSnap.setText(UtilManis.formatMoney(currency.getIso2().trim(), snapData.getReceipt().getAmount()));
        }

        snapHolder.viewSnapGroup.setOnClickListener(v -> onClickListener.OnSnapClickListener(snapData));

    }

    @Override
    public int getItemCount() {
        if (snapDatas != null) {
            return snapDatas.size();
        } else {
            return 0;
        }
    }

    public void addSnaps(ArrayList<SnapData> snapDatas) {
        this.snapDatas = snapDatas;
        notifyDataSetChanged();
    }

    public void setSnapOnClick(SnapAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public boolean isEmpty() {
        return snapDatas.isEmpty();
    }

    class SnapHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.img_statssnap)
        ImageView imgStatSnap;
        @BindView(R.id.txt_pointsnap)
        TextView txtPointSnap;
        @BindView(R.id.txt_currencysnap)
        TextView txtCurrentSnap;
        @BindView(R.id.txt_merchantname)
        TextView txtMerchantName;
        @BindView(R.id.txt_merchantcategory)
        TextView txtMerchantCategory;
        @BindView(R.id.view_snap_group)
        RelativeLayout viewSnapGroup;

        public SnapHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface onClickListener {
        void OnSnapClickListener(SnapData snapData);
    }
}


