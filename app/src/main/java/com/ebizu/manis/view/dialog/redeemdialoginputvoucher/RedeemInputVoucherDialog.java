package com.ebizu.manis.view.dialog.redeemdialoginputvoucher;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.manager.input.InputTypeManager;
import com.ebizu.manis.model.rewardvoucher.Input;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.sdk.ManisLocalData;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.sdk.reward.models.Reward;
import com.ebizu.sdk.reward.models.VoucherInput;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.ebizu.manis.helper.UtilStatic.REWARD_INPUT_TEXT_POINTS;
import static com.ebizu.manis.helper.UtilStatic.REWARD_INPUT_TEXT_TOTAL_POINTS;
import static com.ebizu.manis.helper.UtilStatic.REWARD_INPUT_TYPE_MULTILINE;

/**
 * Created by FARIS_mac on 9/29/17.
 */

public class RedeemInputVoucherDialog extends BaseDialogManis {

    private RewardVoucher reward;
    private com.ebizu.manis.model.Reward rewardModel;
    private boolean isPurchaseable;

    private TextView textViewTitle, textViewDescription;
    private ImageView imageViewStore;
    private Button buttonCancel, buttonContinue;
    private LinearLayout voucherInputContainer;
    private List<EditText> editTextInput = new ArrayList<>();
    private HashMap<EditText, Input> editTextInputHashMap = new HashMap<>();
    private RedeemInputListener mRedeemInputListener;

    public RedeemInputVoucherDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public RedeemInputVoucherDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected RedeemInputVoucherDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    public void setReward(RewardVoucher reward, boolean isPurchaseable) {
        this.reward = reward;
        this.isPurchaseable = isPurchaseable;
        setViewDialog(reward);
    }

    public void setRewardModel(com.ebizu.manis.model.Reward rewardModel, boolean isPurchaseable) {
        this.rewardModel = rewardModel;
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_redeem_input, null, false);
        getParent().addView(view);
        voucherInputContainer = (LinearLayout) view.findViewById(R.id.voucher_input_container);
        textViewTitle = (TextView) view.findViewById(R.id.textview_title);
        textViewDescription = (TextView) view.findViewById(R.id.textview_desc);
        imageViewStore = (ImageView) view.findViewById(R.id.imageview_stores);
        buttonCancel = (Button) view.findViewById(R.id.button_cancel);
        buttonContinue = (Button) view.findViewById(R.id.button_continue);
    }

    private void setViewDialog(RewardVoucher reward) {
        textViewTitle.setText(reward.getRedeemInstruction());
        textViewDescription.setText(reward.getName());
        ManisSession manisSession = new ManisSession(getContext());
        ImageUtils.loadImage(getContext(), reward.getImage(), ContextCompat.getDrawable(getContext(), R.drawable.default_pic_store_logo), imageViewStore);
        LayoutInflater inflate = LayoutInflater.from(getContext());
        if (reward != null && !reward.getInputs().isEmpty()) {
            int sizeOfFilledUser = 0;
            for (Input input : reward.getInputs()) {
                if (!input.isFilledByApplication()) {
                    sizeOfFilledUser += 1;
                }
            }

            for (int i = 0; i < reward.getInputs().size(); i++) {
                Input input = reward.getInputs().get(i);
                View view = inflate.inflate(R.layout.view_voucher_dialog_input, voucherInputContainer, false);
                EditText editTextInput = (EditText) view.findViewById(R.id.edittext_voucher_input);
                InputTypeManager.checkInputType(input, editTextInput, manisSession);

                editTextInput.setInputType(input.getInputType());
                if (input.getType().equalsIgnoreCase(REWARD_INPUT_TYPE_MULTILINE)) {
                    editTextInput.setSingleLine(false);
                    editTextInput.setMinLines(3);
                }

                editTextInput.setImeOptions(EditorInfo.IME_ACTION_DONE);
                if (!input.isFilledByApplication() && sizeOfFilledUser != 1) {
                    editTextInput.setImeOptions(EditorInfo.IME_ACTION_NEXT);
                    sizeOfFilledUser--;
                }

                //check input for physical voucher
                if (input.getText().equalsIgnoreCase(REWARD_INPUT_TEXT_TOTAL_POINTS)) {
                    editTextInput.setText(String.valueOf(ManisLocalData.getAccountPoint()));
                    editTextInput.setFocusable(false);
                    view.setVisibility(View.GONE);
                }
                if (input.getText().equalsIgnoreCase(REWARD_INPUT_TEXT_POINTS)) {
                    //this mean voucher point
                    editTextInput.setText(String.valueOf(reward.getPoint()));
                    editTextInput.setFocusable(false);
                    view.setVisibility(View.GONE);
                }
                //end of physical voucher input

                editTextInput.setTag(input.getId());
                this.editTextInput.add(editTextInput);
                editTextInputHashMap.put(editTextInput, input);
                voucherInputContainer.addView(view);
            }
        }
        voucherInputContainer.requestFocus();
        buttonCancel.setOnClickListener(v -> {
            for (EditText etInput : editTextInput) {
                etInput.setText("");
                etInput.clearFocus();
            }
            voucherInputContainer.requestFocus();
            dismiss();
        });
        buttonContinue.setOnClickListener(v -> continueAction());
    }

    private void continueAction() {
        List<VoucherInput> voucherInputs = new ArrayList<>();
        for (EditText editText : editTextInput) {
            if (isEmptyEditText(editText)) {
                voucherInputs.add(getVoucherInput(editText));
            } else {
                return;
            }
        }
        if (mRedeemInputListener != null)
            mRedeemInputListener.finish(voucherInputs, isPurchaseable);
        dismiss();
    }

    private boolean isEmptyEditText(EditText editText) {
        boolean isEmpty;
        Input input = editTextInputHashMap.get(editText);
        if (input.getText().equalsIgnoreCase(ConfigManager.RedeemPhysicalInput.INPUT_PHONE_NUMBER) ||
                input.getText().equalsIgnoreCase(ConfigManager.RedeemPhysicalInput.INPUT_MOBILE)) {
            if (TextUtils.isEmpty(editText.getText())) {
                showMessageInputValidation(getContext().getString(R.string.text_error_occured),
                        getContext().getString(R.string.error_empty_spesific_field, input.getText()));
                isEmpty = false;
            } else if (editText.length() < 8) {
                showMessageInputValidation(getContext().getString(R.string.error),
                        getContext().getString(R.string.error_reward_phone_number_length));
                isEmpty = false;
            } else {
                isEmpty = true;
            }

        } else {
            if (TextUtils.isEmpty(editText.getText())) {
                showMessageInputValidation(getContext().getString(R.string.text_error_occured),
                        getContext().getString(R.string.error_empty_spesific_field, input.getText()));
                isEmpty = false;
            } else {
                isEmpty = true;
            }
        }
        return isEmpty;
    }

    private VoucherInput getVoucherInput(EditText editText) {
        VoucherInput voucherInput = new VoucherInput();
        voucherInput.setInputId(editText.getTag().toString());
        voucherInput.setValue(editText.getText().toString());
        return voucherInput;
    }

    private void showMessageInputValidation(String messageTitle, String errorMessage) {
        showAlertDialog(messageTitle, errorMessage,
                false, R.drawable.manis_logo, getContext().getString(R.string.text_ok), (dialog, which) -> dialog.dismiss());
    }

    public void setRedeemInputListener(RedeemInputListener redeemInputListener) {
        this.mRedeemInputListener = redeemInputListener;
    }

    public interface RedeemInputListener {
        void finish(List<VoucherInput> result, boolean isPurchaseable);
    }
}
