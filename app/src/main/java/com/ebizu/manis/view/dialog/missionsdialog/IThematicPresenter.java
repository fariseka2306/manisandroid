package com.ebizu.manis.view.dialog.missionsdialog;

import com.ebizu.manis.mvp.mission.MissionDetailActivity;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperiencePostRB;
import com.ebizu.manis.service.manis.requestbodyv2.body.ThematicPostRB;
import com.ebizu.manis.view.dialog.missionsdialog.ThematicMissionsDialog;

/**
 * Created by FARIS_mac on 10/24/17.
 */

public interface IThematicPresenter {

    void executeMission(ThematicMissionsDialog thematicMissionsDialog, ThematicPostRB thematicPostRB);

}
