package com.ebizu.manis.view.swipe;

import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.view.adapter.NotificationSwipeableAdapter;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionRemoveItem;

/**
 * Created by ebizu on 8/30/17.
 */

public class SwipeRightNotification extends SwipeResultActionRemoveItem {

    private NotificationSwipeableAdapter mAdapter;
    private NotificationTableList notificationTableList;
    private final int mPosition;
    private OnClickItemRemoveListener onClickItemRemoveListener;

    public SwipeRightNotification(NotificationSwipeableAdapter adapter, NotificationTableList notificationTableList, int position) {
        mAdapter = adapter;
        mPosition = position;
        this.notificationTableList = notificationTableList;
    }

    @Override
    protected void onPerformAction() {
        super.onPerformAction();
        notificationTableList.delete();
        mAdapter.eventListener.onItemRemoved(mPosition);
        mAdapter.notifyItemRemoved(mPosition);
        onClickItemRemoveListener.onClick();
    }

    @Override
    protected void onSlideAnimationEnd() {
        super.onSlideAnimationEnd();
        if (mAdapter.eventListener != null) {
            mAdapter.eventListener.onItemRemoved(mPosition);
            onClickItemRemoveListener.onClick();
        }
    }

    @Override
    protected void onCleanUp() {
        super.onCleanUp();
        // clear the references
        mAdapter = null;
    }

    public void setOnClickItemRemoveListener(OnClickItemRemoveListener onClickItemRemoveListener){
        this.onClickItemRemoveListener = onClickItemRemoveListener;
    }

    public interface OnClickItemRemoveListener {

        void onClick();
    }
}