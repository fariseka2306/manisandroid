package com.ebizu.manis.view.dialog.missionsdialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.AsteriskPasswordTransformationMethod;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.missions.Missions;
import com.ebizu.manis.service.manis.requestbodyv2.body.ThematicPostRB;
import com.ebizu.manis.service.manis.requestbodyv2.data.ThematicPostData;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperMissionExecute;
import com.ebizu.manis.view.dialog.BaseDialogManis;

/**
 * Created by FARIS_mac on 10/5/17.
 */

public class ThematicMissionsDialog extends BaseDialogManis implements View.OnKeyListener, View.OnTouchListener, TextWatcher {

    private ImageView imageViewMerchant;
    private TextView textViewPoint;
    private TextView textViewDesc;
    private EditText editTextFirstpin;
    private EditText editTextSecondpin;
    private EditText editTextThirdpin;
    private EditText editTextFourthpin;
    private EditText editTextFifthpin;
    private EditText editTextSixthpin;
    private EditText[] editTextPin;
    public ProgressBar progressBar;
    public TextView textViewPin;
    private Button buttonOK;
    private int editTextPosition = 0;
    private Missions missions;

    public void setMissions(Missions missions) {
        this.missions = missions;
        initView(missions);
    }

    public ThematicMissionsDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public ThematicMissionsDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected ThematicMissionsDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_mission_thematic, null, false);
        getParent().addView(view);
        imageViewMerchant = (ImageView) findViewById(R.id.dm_img_merchant);
        textViewPoint = (TextView) findViewById(R.id.dm_txt_point);
        textViewDesc = (TextView) findViewById(R.id.dm_txt_desc);
        buttonOK = (Button) findViewById(R.id.dm_btn_ok);
        editTextFirstpin = (EditText) findViewById(R.id.dm_edt_firstpin);
        editTextSecondpin = (EditText) findViewById(R.id.dm_edt_secondpin);
        editTextThirdpin = (EditText) findViewById(R.id.dm_edt_thirdpin);
        editTextFourthpin = (EditText) findViewById(R.id.dm_edt_fourthpin);
        editTextFifthpin = (EditText) findViewById(R.id.dm_edt_fifthpin);
        editTextSixthpin = (EditText) findViewById(R.id.dm_edt_sixpin);
        textViewPin = (TextView) findViewById(R.id.dr_txt_pin);

        editTextFirstpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextSecondpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextThirdpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextFourthpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextFifthpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextSixthpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        editTextPin = new EditText[]{
                editTextFirstpin,
                editTextSecondpin,
                editTextThirdpin,
                editTextFourthpin,
                editTextFifthpin,
                editTextSixthpin
        };

        editTextFirstpin.requestFocus();
        buttonOK.setOnClickListener(v -> executeWithPin());
        initListener();
    }

    private void executeWithPin() {
        ThematicPresenter thematicPresenter = new ThematicPresenter(getContext(), getBaseActivity());
        ThematicPostData data = new ThematicPostData(getContext());
        data.setMissionId(missions.getMissionId());
        data.setPin(getEnteredPin());
        data.setVoucherId(missions.getVoucherId());
        ThematicPostRB requestBody = new ThematicPostRB();
        requestBody.setData(data);
        thematicPresenter.executeMission(this, requestBody);
    }

    private String getEnteredPin() {
        StringBuilder stringBuilder = new StringBuilder();
        for (EditText editText : editTextPin) {
            stringBuilder.append(editText.getText().toString());
        }
        return stringBuilder.toString();
    }

    private void initView(Missions missions) {
        ImageUtils.loadImage(getContext(), missions.getAsset().getLogo(),
                ContextCompat.getDrawable(getContext(), R.drawable.default_pic_store_logo), imageViewMerchant);
        textViewPoint.setText("+ " + missions.getIncentive().getAmount().toString().concat(" "));
        textViewDesc.setText(missions.getIncentive().getForm());
    }

    public void showDialogSuccess(WrapperMissionExecute wrapperMissionExecute) {
        dismiss();
        MissionSuccessDialog missionSuccessDialog = new MissionSuccessDialog(getContext());
        missionSuccessDialog.setWrapperMissionPost(wrapperMissionExecute);
        missionSuccessDialog.setMissions(missions);
        missionSuccessDialog.setActivity(getBaseActivity());
        missionSuccessDialog.show();
    }

    public void executeFailed(String messageClient) {
        textViewPin.setVisibility(View.VISIBLE);
        textViewPin.setText(messageClient);
        textViewPin.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
        clearEditTextPin();
    }

    private void initListener() {
        for (EditText editText : editTextPin) {
            editText.setOnKeyListener(this);
            editText.setOnTouchListener(this);
            editText.addTextChangedListener(this);
        }
    }

    private void clearEditTextPin() {
        editTextPosition = 0;
        for (EditText editText : editTextPin) {
            editText.getText().clear();
        }
        editTextFirstpin.requestFocus();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!editTextPin[editTextPosition].getText().toString().isEmpty()) {
            if (editTextPosition < editTextPin.length - 1) {
                editTextPosition++;
                editTextPin[editTextPosition].requestFocus();
            } else if (editTextPosition == editTextPin.length - 1) {
                //UtilManis.closeKeyboard(this, );
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            clearEditTextPin();
        } else {
            if (!editTextPin[editTextPosition].getText().toString().isEmpty()) {
                if (editTextPosition < editTextPin.length - 1) {
                    editTextPosition++;
                    editTextPin[editTextPosition].requestFocus();
                } else if (editTextPosition == editTextPin.length - 1) {
                    UtilManis.closeKeyboard(getBaseActivity(), view);
                }
            }
        }
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        int pos = 0;
        for (EditText editText : editTextPin) {
            if (view.equals(editText)) {
                editTextPosition = pos;
                editTextPin[editTextPosition].requestFocus();
            }
            pos++;
        }
        return false;
    }
}
