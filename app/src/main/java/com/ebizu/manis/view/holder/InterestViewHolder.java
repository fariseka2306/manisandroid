package com.ebizu.manis.view.holder;

import android.content.Context;
import android.support.percent.PercentLayoutHelper;
import android.support.percent.PercentRelativeLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.Interest;
import com.ebizu.manis.view.manis.interest.model.InterestValidation;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public class InterestViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.percent_relative_layout)
    PercentRelativeLayout percentRelativeLayout;
    @BindView(R.id.image_view_category_interest)
    ImageView imageViewInterest;
    @BindView(R.id.view_check)
    View checkView;
    @BindView(R.id.text_view_interest)
    TextView textViewInterest;
    @BindView(R.id.frame_add_interest)
    FrameLayout frameLayoutAddInterest;
    @BindView(R.id.linear_interest_text_background)
    LinearLayout linInterestTextBackground;

    private Context context;
    private Interest interest;

    public InterestViewHolder(View itemView, Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }

    public void setInterestItemView(Interest interest) {
        setInterest(interest);
        if (interest.getPinned()) {
            checkView.setVisibility(View.VISIBLE);
        } else {
            checkView.setVisibility(View.GONE);
        }
        textViewInterest.setText(interest.getName());

        imageViewInterest.setImageDrawable(InterestValidation.getInterestDrawable(context, interest.getId()));

        linInterestTextBackground.setBackgroundColor(InterestValidation.getColor(context, interest.getId()));
    }

    public void setOnClickListener(InterestViewHolder.OnClickListener onClickListener) {
        percentRelativeLayout.setOnClickListener((v) -> {
            if (!checkView.isShown()) {
                checkView.setVisibility(View.VISIBLE);
                frameLayoutAddInterest.setVisibility(View.GONE);
                new AnalyticManager(context).trackEvent(
                        ConfigManager.Analytic.Category.INTEREST,
                        ConfigManager.Analytic.Action.CLICK,
                        "Image ".concat(interest.getName()).concat("Select"));
                getInterest().setPinned(true);
            } else if (checkView.isShown()) {
                checkView.setVisibility(View.GONE);
                frameLayoutAddInterest.setVisibility(View.VISIBLE);
                getInterest().setPinned(false);
                new AnalyticManager(context).trackEvent(
                        ConfigManager.Analytic.Category.INTEREST,
                        ConfigManager.Analytic.Action.CLICK,
                        "Image ".concat(interest.getName()).concat("Unselect"));
            }
            onClickListener.checkedInterest();
        });
    }

    public Interest getInterest() {
        return interest;
    }

    public void setInterest(Interest interest) {
        this.interest = interest;
    }

    public interface OnClickListener {
        void checkedInterest();
    }
}
