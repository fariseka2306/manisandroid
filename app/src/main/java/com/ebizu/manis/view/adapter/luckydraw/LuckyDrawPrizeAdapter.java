package com.ebizu.manis.view.adapter.luckydraw;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.model.PrizeDetail;
import com.ebizu.manis.view.holder.luckydraw.LuckyDrawPrizeViewHolder;

import java.util.List;

/**
 * Created by ebizu on 04/08/17.
 */

public class LuckyDrawPrizeAdapter extends RecyclerView.Adapter<LuckyDrawPrizeViewHolder> {

    private Context context;
    private List<PrizeDetail> prizeDetails;

    public LuckyDrawPrizeAdapter(Context context, List<PrizeDetail> prizeDetails) {
        this.context = context;
        this.prizeDetails = prizeDetails;
    }

    @Override
    public LuckyDrawPrizeViewHolder onCreateViewHolder(ViewGroup viewHolder, int viewType) {
        View view = LayoutInflater.from(viewHolder.getContext()).inflate(R.layout.item_prize, viewHolder, false);
        return new LuckyDrawPrizeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LuckyDrawPrizeViewHolder viewHolder, int position) {
        PrizeDetail prizeDetail = prizeDetails.get(position);
        viewHolder.setPrizeDetailView(prizeDetail);
    }

    @Override
    public int getItemCount() {
        return prizeDetails != null ? prizeDetails.size() : 0;
    }

    public void addPrizes(List<PrizeDetail> prizeDetails) {
        this.prizeDetails = prizeDetails;
        notifyDataSetChanged();
    }

}
