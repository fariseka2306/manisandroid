package com.ebizu.manis.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ebizu on 14/08/17.
 */

public class LoadingViewHolder extends RecyclerView.ViewHolder {
    public LoadingViewHolder(View view) {
        super(view);
    }
}
