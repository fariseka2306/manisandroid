package com.ebizu.manis.view.walkthrough.walkthroughpage;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.view.walkthrough.WalkthroughtActivitySecond;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FARIS_mac on 8/31/17.
 */

public class WalkthroughPageSix extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.txt_title_action)
    TextView textViewTitle;

    @BindView(R.id.highlight_up)
    RelativeLayout highlightUp;
    @BindView(R.id.highlight_down)
    RelativeLayout highlightDown;
    @BindView(R.id.imageview_button_ok)
    ImageView imageViewButtonOk;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_walkthrough_page_six, container, false);
        ButterKnife.bind(this, view);
        imageViewButtonOk.setOnClickListener(this);
        return view;
    }

    public void showNinethPage() {
        setHeaderTitle(2);
        new Handler().postDelayed(() -> {
            highlightUp.setVisibility(View.VISIBLE);
            highlightDown.setVisibility(View.VISIBLE);
        }, 1000);
    }

    private void setHeaderTitle(final int countIsntRead) {
        if (isAdded()) {
            new Handler().post(() -> {
                try {
                    switch (countIsntRead) {
                        case 0:
                            textViewTitle.setText(getString(R.string.notifications));
                            break;
                        case 1:
                            textViewTitle.setText(countIsntRead + " " + getString(R.string.new_notification));
                            break;
                        default:
                            textViewTitle.setText(countIsntRead + " " + getString(R.string.new_notifications));
                            break;
                    }
                } catch (Exception e) {
                    Log.e("notification", e.getMessage());
                }
            });
        }
    }


    @Override
    public void onClick(View v) {
        if (v.equals(imageViewButtonOk)) {
            ((WalkthroughtActivitySecond) getActivity()).closeDrawer();
        }
    }
}
