package com.ebizu.manis.view.videoview;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import com.ebizu.manis.R;

/**
 * Created by Ebizu-User on 13/07/2017.
 */

public class VideoManis {

    private VideoView videoView;
    private View viewBackground;
    private Context context;

    private int seekPosition;

    public VideoManis(ViewGroup viewGroup, Context context) {
        this.viewBackground = viewGroup.findViewById(R.id.view_background);
        this.videoView = (VideoView) viewGroup.findViewById(R.id.video_view_manis);
        this.context = context;
        setListener();
    }

    private void setListener() {
        videoView.setVideoURI(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.manis_login_video_converted));
        videoView.setOnCompletionListener(arg0 -> {
            viewBackground.setVisibility(View.GONE);
            videoView.start();
        });
    }

    public void startPlayVideo() {
        new Handler().postDelayed(() -> viewBackground.setVisibility(View.GONE), 300);
        videoView.seekTo(seekPosition);
        videoView.start();
        videoView.resume();
    }

    public void pauseVideo() {
        viewBackground.setVisibility(View.VISIBLE);
        seekPosition = videoView.getCurrentPosition();
        videoView.pause();
    }

    public boolean isPlaying() {
        return videoView.isPlaying();
    }

}