package com.ebizu.manis.view.adapter.luckydraw;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.LuckyDrawTicket;
import com.ebizu.manis.view.adapter.ListAdapter;
import com.ebizu.manis.view.holder.luckydraw.LuckyDrawEntryViewHolder;

/**
 * Created by ebizu on 12/12/17.
 */

public class LuckyDrawEntryAdapter extends ListAdapter<LuckyDrawTicket, LuckyDrawEntryViewHolder> {

    public LuckyDrawEntryAdapter(Context context) {
        super(R.layout.item_lucky_draw_entries, context);
    }

    public LuckyDrawEntryAdapter(Context context, RecyclerView recyclerView) {
        super(R.layout.item_lucky_draw_entries, context, recyclerView);
    }

}