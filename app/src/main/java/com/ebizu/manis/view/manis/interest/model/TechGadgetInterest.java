package com.ebizu.manis.view.manis.interest.model;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public class TechGadgetInterest implements InterestModelView {
    @Override
    public int id() {
        return 4;
    }

    @Override
    public Drawable drawable(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.interest_selection_gadgets);
    }

    @Override
    public int color(Context context) {
        return ContextCompat.getColor(context, R.color.interest_tech_n_gadgets);
    }
}
