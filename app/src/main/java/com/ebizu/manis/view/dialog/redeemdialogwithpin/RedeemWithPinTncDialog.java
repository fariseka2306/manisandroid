package com.ebizu.manis.view.dialog.redeemdialogwithpin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.model.purchasevoucher.Purchase;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.login.LoginActivity;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.service.reward.requestbody.RedeemPinTncBody;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;
import com.ebizu.sdk.reward.models.Reward;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FARIS_mac on 10/4/17.
 */

public class RedeemWithPinTncDialog extends BaseDialogManis implements IRedeemWithPinTncDialog, View.OnClickListener {

    @BindView(R.id.web_view)
    WebView webView;

    @BindView(R.id.imageview_reward)
    ImageView imageViewReward;

    @BindView(R.id.textview_reward)
    TextView textViewReward;

    @BindView(R.id.textview_merchant)
    TextView textViewMerchant;

    @BindView(R.id.button_useitnow)
    Button buttonUseItNow;

    @BindView(R.id.button_useitlater)
    Button buttonUseItLater;

    private String refCode;

    private RewardVoucher reward;
    private com.ebizu.manis.model.Reward myVoucher;
    private Purchase purchase;
    private RedeemWithPinTncPresenter redeemWithPinTncPresenter;

    public void setReward(RewardVoucher reward) {
        this.reward = reward;
        initVoucherTerm(reward, null);
    }

    public void setMyVoucher(com.ebizu.manis.model.Reward myVoucher) {
        this.myVoucher = myVoucher;
        initVoucherTerm(null, myVoucher);

    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public RedeemWithPinTncDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public RedeemWithPinTncDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected RedeemWithPinTncDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    @Override
    public void attachDialogPresenter(BaseDialogPresenter baseDialogPresenter) {
        super.attachDialogPresenter(baseDialogPresenter);
        redeemWithPinTncPresenter = (RedeemWithPinTncPresenter) baseDialogPresenter;
        redeemWithPinTncPresenter.attachDialog(this);
    }

    @Override
    public void setViewRedeemWithPin(String terms) {
        ImageUtils.loadImage(getContext(), reward != null ? reward.getProvider().getImage() : myVoucher.getProvider().getImage(),
                ContextCompat.getDrawable(getContext(), R.drawable.default_pic_store_logo), imageViewReward);
        textViewReward.setText(reward != null ? reward.getBrand().getName() : myVoucher.getBrand().getName());
        textViewMerchant.setText(reward != null ? reward.getName() : myVoucher.getName());
        if (terms != null) {
            webView.loadData(terms, "text/html; charset=utf-8", "utf-8");
        }
    }

    @Override
    public void setViewReddemWithPinInvalid(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(
                "Yes",
                (dialog, id) -> doLogout());

        AlertDialog alert = builder.create();
        alert.show();
    }
    private void doLogout() {
        new ManisSession(getContext()).clearSession();
        getContext().startActivity(new Intent(getContext(), LoginActivity.class));
    }

    @Override
    public void onClick(View view) {
        if (view.equals(buttonUseItNow)) {
            showDialogRedeemWithPin();
        } else {
            dismiss();
        }
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_redeem_tnc_with_pin, null, false);
        ButterKnife.bind(this, view);
        getParent().addView(view);
        initListener();
    }

    private void initListener() {
        buttonUseItLater.setOnClickListener(this);
        buttonUseItNow.setOnClickListener(this);
    }

    private void showDialogRedeemWithPin() {
        RedeemWithPinDialog redeemWithPinDialog = new RedeemWithPinDialog(getContext());
        redeemWithPinDialog.setActivity(getBaseActivity());
        if (reward != null) {
            redeemWithPinDialog.setReward(reward);
        } else {
            redeemWithPinDialog.setMyVoucher(myVoucher);
        }
        redeemWithPinDialog.setRefCode(refCode);
        redeemWithPinDialog.setPurchase(purchase);
        redeemWithPinDialog.show();
        dismiss();
    }

    private void initVoucherTerm(RewardVoucher reward, com.ebizu.manis.model.Reward myvoucher) {
        RedeemPinTncBody.Data data = new RedeemPinTncBody.Data();
        data.setVoucherId(reward != null ? reward.getId() : myvoucher.getId());
        RedeemPinTncBody redeemPinTncBody = new RedeemPinTncBody(getContext(), data);
        attachDialogPresenter(new RedeemWithPinTncPresenter(getContext()));
        getRedeemWithPinTncPresenter().loadBrandTerm(redeemPinTncBody);
    }

    public RedeemWithPinTncPresenter getRedeemWithPinTncPresenter() {
        return redeemWithPinTncPresenter;
    }
}
