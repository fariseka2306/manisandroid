package com.ebizu.manis.view.dialog.term;

import com.ebizu.manis.model.Term;
import com.ebizu.manis.rx.ResponseSubsriberLambda;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.response.WrapperTerm;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class TermPresenter extends BaseDialogPresenter implements ITermPresenter {

    private TermDialog termDialog;

    @Override
    public void attachDialog(BaseDialogManis baseDialogManis) {
        super.attachDialog(baseDialogManis);
        termDialog = (TermDialog) baseDialogManis;
    }

    @Override
    public void loadTerm(RequestBody requestBody, String contentType) {
        termDialog.showBaseProgressBar();
        getManisApiV2().getManisTerm(requestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubsriberLambda<WrapperTerm>(termDialog) {
                    @Override
                    protected void onSuccess(WrapperTerm wrapperTerm) {
                        super.onSuccess(wrapperTerm);
                        setViewTerm(wrapperTerm, contentType);
                        termDialog.dismissBaseProgressBar();
                    }

                    @Override
                    protected void onErrorFailure(String message) {
                        super.onErrorFailure(message);
                        termDialog.setNegativeScenarioView(message);
                        termDialog.dismissBaseProgressBar();
                    }

                    @Override
                    protected void onNetworkFailed(String message) {
                        super.onNetworkFailed(message);
                        termDialog.setNegativeScenarioView(message);
                        termDialog.dismissBaseProgressBar();
                    }
                });
    }

    private void setViewTerm(WrapperTerm wrapperTerm, String contentType) {
        for (Term term : wrapperTerm.getTerm()) {
            if (term.getType().equals(contentType)) {
                termDialog.setViewTerm(term);
            }
        }
    }
}
