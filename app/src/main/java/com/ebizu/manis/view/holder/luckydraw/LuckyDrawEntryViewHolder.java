package com.ebizu.manis.view.holder.luckydraw;

import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.LuckyDrawTicket;
import com.ebizu.manis.view.holder.BaseHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 12/12/17.
 */

public class LuckyDrawEntryViewHolder extends BaseHolder<LuckyDrawTicket> {

    @BindView(R.id.ld_entries_txt_time)
    TextView ldEntriesTxtTime;
    @BindView(R.id.ld_entries_txt_desc)
    TextView ldEntriesTxtDesc;

    public LuckyDrawEntryViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void setHolderView(LuckyDrawTicket model) {
        ldEntriesTxtTime.setText(UtilManis.getTimeAgoMiddle(model.getdDay(), itemView.getContext()));
        ldEntriesTxtDesc.setText(Html.fromHtml(model.getWordingPrefix() + " <b>" + model.getWordingObject() + "</b>"));
    }
}