package com.ebizu.manis.view.dialog;

import com.ebizu.manis.service.manis.ManisApiV2;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public interface IDialogPresenter {

    void attachDialog(BaseDialogManis baseDialogManis);

    ManisApiV2 getManisApiV2();
}
