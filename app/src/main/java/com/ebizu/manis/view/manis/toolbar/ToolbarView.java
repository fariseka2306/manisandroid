package com.ebizu.manis.view.manis.toolbar;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.root.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by abizu-alvio on 7/21/2017.
 */

public class ToolbarView extends BaseView implements IToolbarView {

    @BindView(R.id.txt_title_action)
    TextView textViewTitle;
    @BindView(R.id.img_left)
    ImageView imageView;
    @BindView(R.id.img_right)
    ImageView imgRight;

    public ToolbarView(Context context) {
        super(context);
        createView(context);
    }

    public ToolbarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ToolbarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ToolbarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.actionbar_back, null, false);
        addView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    public void setTitle(int title) {
        textViewTitle.setText(title);
    }

    public void setTitle(String title) {
        textViewTitle.setText(title);
    }

    public void setTitle(int title, int drawable) {
        textViewTitle.setText(title);
        imageView.setImageResource(drawable);
    }

    public void setTitle(String title, int drawable) {
        textViewTitle.setText(title);
        imageView.setVisibility(VISIBLE);
        imageView.setImageResource(drawable);
    }

    public void setTitle(RelativeLayout.LayoutParams params, String title, int drawable) {
        textViewTitle.setLayoutParams(params);
        textViewTitle.setText(title);
        imageView.setVisibility(VISIBLE);
        imageView.setImageResource(drawable);
    }

    public void setRightImage(int drawable) {
        imgRight.setVisibility(VISIBLE);
        Glide.with(getContext())
                .load(drawable)
                .into(imgRight);
    }

    @OnClick(R.id.rel_left)
    void onBackPressed() {
        try {
            AppCompatActivity appCompatActivity = (AppCompatActivity) getContext();
            appCompatActivity.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void performBackAnimation(Activity activity) {

    }
}
