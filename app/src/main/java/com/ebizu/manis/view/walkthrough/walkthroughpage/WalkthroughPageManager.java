package com.ebizu.manis.view.walkthrough.walkthroughpage;

/**
 * Created by mac on 8/29/17.
 */

public class WalkthroughPageManager {

    private WalkthroughPageView[] walkthroughPageViews;

    public WalkthroughPageManager(WalkthroughPageView[] walkthroughPageViews) {
        this.walkthroughPageViews = walkthroughPageViews;
    }

    public void startTutorial(int page) {
        WalkthroughPageView walkthroughPageView = getWalkthroughPageView(page);
        walkthroughPageView.startPage();
        setWalkthroughNextPage(walkthroughPageView);
    }

    private void setWalkthroughNextPage(WalkthroughPageView walkthroughPageView) {
        if (walkthroughPageView.page() < walkthroughPageViews.length) {
            WalkthroughPageView nextPageView = walkthroughPageViews[walkthroughPageView.page()];
            walkthroughPageView.setOnNextPageListener((currentPage) -> {
                nextPageView.startPage();
                startTutorial(currentPage + 1);
            });
        }
    }

    private WalkthroughPageView getWalkthroughPageView(int page) {
        for (WalkthroughPageView walkthroughPageView : walkthroughPageViews) {
            if (walkthroughPageView.page() == page) {
                return walkthroughPageView;
            }
        }
        return new WalkthroughPageOne(walkthroughPageViews[0].getContext());
    }

}
