package com.ebizu.manis.view.dialog.expired;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Raden on 11/16/17.
 */

public class ExpiredDialog extends BaseDialogManis  {

    private static final String TAG = ExpiredDialog.class.getSimpleName();

    private RewardVoucher reward;

    @BindView(R.id.imageview_reward)
    ImageView imgRewardVoucher;

    @BindView(R.id.textview_reward)
    TextView txtRewardVoucher;

    @BindView(R.id.textview_merchant)
    TextView txtMerchantVoucher;

    @BindView(R.id.imageview_check)
    ImageView imgCheckVoucher;

    @BindView(R.id.button_useitnow)
    Button btnUseItNowVoucher;

    @BindView(R.id.txt_transaction)
    TextView txtTransaction;

    @BindView(R.id.txt_start_time)
    TextView txtStartTime;

    @BindView(R.id.txt_end_time)
    TextView txtEndTime;

    @BindView(R.id.txt_voucher_validity)
    TextView txtVoucherCherValidity;

    @BindView(R.id.txt_voucher_code)
    TextView txtVoucherCodeName;

    public ExpiredDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public ExpiredDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected ExpiredDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    public void setExpiredVoucher(RewardVoucher expiredVoucher){
        this.reward = expiredVoucher;
        setViewDialog(expiredVoucher);

    }

    private void customDialog(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_voucher_expired, null, false);
        getParent().addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    private void setViewDialog(RewardVoucher reward){
        ImageUtils.loadImage(getContext(), reward.getProvider().getImage(), ContextCompat.getDrawable(getContext(), R.drawable.default_pic_store_logo), imgRewardVoucher);
        txtRewardVoucher.setText(reward.getName());
        txtMerchantVoucher.setText(reward.getProvider().getName());
        txtTransaction.setText(reward.getOrderId());
        txtEndTime.setText(reward.getExpired());
//        txtVoucherCherValidity.setText(reward.getValidityTitle());
//        txtVoucherCodeName.setText(reward.getOrderIdTitle());

        txtStartTime.setText(reward.getStartDate());
    }

    @OnClick(R.id.button_useitnow)
        void clickOke(){
            dismiss();
        }
}
