package com.ebizu.manis.view.adapter;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.model.saved.Offer;
import com.ebizu.manis.view.holder.SavedOfferHolder;

import java.util.List;

/**
 * Created by mac on 8/21/17.
 */

public class SavedOfferAdapter extends ListAdapter<Offer, SavedOfferHolder> {

    public SavedOfferAdapter(Context context, List<Offer> arrayData) {
        super(R.layout.item_saved_all, context, arrayData);
    }

}
