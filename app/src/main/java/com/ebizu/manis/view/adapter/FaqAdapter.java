package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.Faq;
import com.ebizu.manis.view.holder.FaqViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abizu-alvio on 7/31/2017.
 */

public class FaqAdapter extends RecyclerView.Adapter<FaqViewHolder> {

    private final String TAG = getClass().getSimpleName();

    private Context mContext;
    private List<Faq> faqList;

    public FaqAdapter(Context mContext, ArrayList<Faq> faqList) {
        this.mContext = mContext;
        this.faqList = faqList;
    }

    @Override
    public FaqViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.faq_item_list, parent, false);
        return new FaqViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FaqViewHolder holder, int position) {
        Faq faq = this.faqList.get(position);
        holder.setFaqItemView(faq);
        holder.setOnClickListener(() -> {
            new AnalyticManager(mContext).trackEvent(
                    ConfigManager.Analytic.Category.HELP_ACTIVITY,
                    ConfigManager.Analytic.Action.CLICK,
                    faq.getTitle()
            );
        });
    }

    @Override
    public int getItemCount() {
        return faqList.size();
    }

    public void addFaqList(List<Faq> faqses) {
        faqList.clear();
        faqList.addAll(faqses);
        notifyDataSetChanged();
    }
}
