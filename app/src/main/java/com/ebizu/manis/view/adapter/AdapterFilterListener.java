package com.ebizu.manis.view.adapter;

import android.support.v7.widget.RecyclerView;

import com.ebizu.manis.helper.FilterHolder;
import com.ebizu.manis.view.holder.FilterContentViewHolder;

/**
 * Created by Raden on 8/2/17.
 */

public interface AdapterFilterListener<T> {
    void onCreateListItem(T data, FilterContentViewHolder holder, int position);

    void onSelectItem(T data, boolean isSelected);
}
