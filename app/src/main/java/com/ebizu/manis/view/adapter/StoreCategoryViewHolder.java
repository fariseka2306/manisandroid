package com.ebizu.manis.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.model.InterestsStore;
import com.ebizu.manis.view.holder.BaseHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 31/07/17.
 */

public class StoreCategoryViewHolder extends BaseHolder<InterestsStore> {

    @BindView(R.id.rl_stores_categories)
    RelativeLayout rlStoresCategories;

    @BindView(R.id.fsni_img_merchant)
    ImageView fsniImgMerchant;

    @BindView(R.id.fsni_txt_category)
    TextView fsniTxtCategory;

    @BindView(R.id.fsni_txt_count)
    TextView fsniTxtCount;

    public StoreCategoryViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void setHolderView(InterestsStore interestsStore) {
        Glide.with(context)
                .load(interestsStore.getAssets().getPhoto())
                .into(fsniImgMerchant);
        fsniTxtCategory.setText(interestsStore.getName());
        fsniTxtCount.setText(interestsStore.getCount().toString());
    }
}
