package com.ebizu.manis.view.manis.toolbar;

import android.app.Activity;

/**
 * Created by halim_ebizu on 9/12/17.
 */

public interface IToolbarRewardView {
    void performBackAnimation(Activity activity);
}
