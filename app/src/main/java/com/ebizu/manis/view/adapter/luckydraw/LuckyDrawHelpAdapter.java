package com.ebizu.manis.view.adapter.luckydraw;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ebizu.manis.helper.UtilStatic;

import java.util.List;

/**
 * Created by Halim on 8/6/17.
 */

public class LuckyDrawHelpAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragments;

    public LuckyDrawHelpAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle args = new Bundle();
        args.putInt(UtilStatic.ARG_POSITION, position);
        this.fragments.get(position).setArguments(args);
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }
}
