package com.ebizu.manis.view.holder;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.ebizu.manis.R;
import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.mvp.notification.item.NotificationView;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractSwipeableItemViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 8/30/17.
 */

public class NotificationSwipeViewHolder extends AbstractSwipeableItemViewHolder {

    @BindView(R.id.fni_txt_title)
    TextView txtTitle;
    @BindView(R.id.fni_txt_location)
    TextView txtLocation;
    @BindView(R.id.fni_txt_time)
    TextView txtTime;
    @BindView(R.id.fni_img_icon)
    ImageView imgIcon;
    @BindView(R.id.fni_delete)
    ImageView imgDelete;
    @BindView(R.id.fni_swipe_layout)
    SwipeLayout swipeLayout;

    public Context context;

    public NotificationSwipeViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        context = itemView.getContext();
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        swipeLayout.addDrag(SwipeLayout.DragEdge.Left, imgDelete);
    }

    @Override
    public View getSwipeableContainerView() {
        return null;
    }

    public void setNotificationView(NotificationTableList notificationTableList) {
        NotificationView notificationView = new NotificationView();
        notificationView.setNotificationView(notificationTableList, this);
    }

    public void setOnClickListener(NotificationSwipeViewHolder.OnClickListener onClickListener) {
        imgDelete.setOnClickListener(view -> onClickListener.removeItem());
        swipeLayout.setOnClickListener(view -> {
            if (swipeLayout.getOpenStatus() == SwipeLayout.Status.Close)
                onClickListener.viewItem();
        });
    }

    public void setItemIconDrawable(Drawable drawable) {
        imgIcon.setImageDrawable(drawable);
    }

    public void setTitle(String title) {
        txtTitle.setText(title);
    }

    public void setTime(String time) {
        txtTime.setText(time);
    }

    public void setLocation(String location) {
        txtLocation.setText(location);
    }

    public Context getContext() {
        return context;
    }

    public interface OnClickListener {
        void removeItem();
        void viewItem();
    }
}
