package com.ebizu.manis.view.manis.toolbar;

import android.app.Activity;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public interface IToolbarView {
    void performBackAnimation(Activity activity);
}
