package com.ebizu.manis.view.dialog.redeemdialogwithpin;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.AsteriskPasswordTransformationMethod;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.model.purchasevoucher.Purchase;
import com.ebizu.manis.model.redeempinvalidation.RedeemPinValidation;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.preference.RewardSession;
import com.ebizu.manis.service.reward.requestbody.PinValidationBody;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;
import com.ebizu.sdk.reward.models.Reward;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FARIS_mac on 10/5/17.
 */

public class RedeemWithPinDialog extends BaseDialogManis implements View.OnKeyListener, View.OnTouchListener, TextWatcher {

    private RewardVoucher reward;
    private com.ebizu.manis.model.Reward myVoucher;
    private Purchase purchase;
    private RedeemWithPinPresenter redeemWithPinPresenter;

    private EditText[] editTextPin;
    private String refCode;
    private int editTextPosition = 0;

    @BindView(R.id.dr_img_merchant)
    ImageView imageViewMerchant;

    @BindView(R.id.dr_txt_desc)
    TextView textViewDesc;

    @BindView(R.id.dr_btn_awesome)
    Button buttonAwesome;

    @BindView(R.id.dr_edt_firstpin)
    EditText editTextFirstpin;

    @BindView(R.id.dr_edt_secondpin)
    EditText editTextSecondpin;

    @BindView(R.id.dr_edt_thirdpin)
    EditText editTextThirdpin;

    @BindView(R.id.dr_edt_fourthpin)
    EditText editTextFourthpin;

    @BindView(R.id.dr_edt_fifthpin)
    EditText editTextFifthpin;

    @BindView(R.id.dr_edt_sixpin)
    EditText editTextSixthpin;

    @BindView(R.id.dr_txt_message)
    TextView drTxtMessage;

    @BindView(R.id.dr_ll_redeem_pin)
    LinearLayout drLlRedeemPin;

    @BindView(R.id.dr_tv_redeem_voucher_code)
    TextView textViewVoucherCode;

    @BindView(R.id.dr_ll_redeem_success)
    LinearLayout drLlRedeemSuccess;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.dr_tv_redeem_success)
    TextView drTvRedeemSuccess;

    public void setReward(RewardVoucher reward) {
        this.reward = reward;
        initData(reward, null);
    }

    public void setMyVoucher(com.ebizu.manis.model.Reward myVoucher) {
        this.myVoucher = myVoucher;
        initData(null, myVoucher);
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public RedeemWithPinDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public RedeemWithPinDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected RedeemWithPinDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_redeem_pin, null, false);
        ButterKnife.bind(this, view);
        getParent().addView(view);
        editTextPin = new EditText[]{
                editTextFirstpin,
                editTextSecondpin,
                editTextThirdpin,
                editTextFourthpin,
                editTextFifthpin,
                editTextSixthpin
        };

        editTextFirstpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextSecondpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextThirdpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextFourthpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextFifthpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextSixthpin.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        editTextFirstpin.requestFocus();
        buttonAwesome.setOnClickListener(v -> pinValidation());
        initListener();
    }

    private void pinValidation() {
        StringBuilder stringBuilder = new StringBuilder();
        List<String> inputPins = new ArrayList<>();
        for (EditText editText : editTextPin) {
            stringBuilder.append(editText.getText().toString());
            inputPins.add(editText.getText().toString());
        }
        String pin = stringBuilder.toString();
        String purchaseId = null;
        String transactionId = null;
        if (refCode != null) {
            purchaseId = "";
            transactionId = refCode;
        } else {
            purchaseId = purchase != null ? purchase.getPurchaseId() : myVoucher.getPurchaseId();
            transactionId = myVoucher != null ? myVoucher.getTransactionId() : "";
        }

        PinValidationBody.Data data = new PinValidationBody.Data();
        data.setPin(pin);
        data.setPurchaseId(purchaseId);
        data.setTransactionId(transactionId);
        data.setVoucherCode(reward != null ? reward.getBrand().getName() : myVoucher.getBrand().getName());
        PinValidationBody pinValidationBody = new PinValidationBody(getContext(), data);
        getRedeemWithPinPresenter().pinValidationPost(pinValidationBody);
    }

    private void initData(RewardVoucher reward, com.ebizu.manis.model.Reward myVoucher) {
        ImageUtils.loadImage(getContext(), reward != null ? reward.getProvider().getImage() : myVoucher.getProvider().getImage(),
                ContextCompat.getDrawable(getContext(), R.drawable.default_pic_store_logo), imageViewMerchant);
        textViewDesc.setText(reward != null ? reward.getName() : myVoucher.getName());
        progressBar.setVisibility(View.GONE);
    }

    private void initListener() {
        for (EditText editText : editTextPin) {
            editText.setOnKeyListener(this);
            editText.setOnTouchListener(this);
            editText.addTextChangedListener(this);
        }
    }

    @Override
    public void attachDialogPresenter(BaseDialogPresenter baseDialogPresenter) {
        super.attachDialogPresenter(baseDialogPresenter);
        redeemWithPinPresenter = (RedeemWithPinPresenter) baseDialogPresenter;
        redeemWithPinPresenter.attachDialog(this);
    }

    public RedeemWithPinPresenter getRedeemWithPinPresenter() {
        if (null == redeemWithPinPresenter)
            attachDialogPresenter(new RedeemWithPinPresenter(getContext()));
        return redeemWithPinPresenter;
    }

    public void showDialogPinSuccess(RedeemPinValidation redeemPinValidation) {
        buttonAwesome.setText(getContext().getString(R.string.dr_btn_awesome));
        drTvRedeemSuccess.setText(getContext().getString(R.string.redeemption_status_pin_success));
        drLlRedeemPin.setVisibility(View.GONE);
        drLlRedeemSuccess.setVisibility(View.VISIBLE);
        textViewDesc.setText(reward != null ? reward.getName() : myVoucher.getName());
        textViewVoucherCode.setTypeface(Typeface.DEFAULT_BOLD);
        textViewVoucherCode.setText(getContext().getString(R.string.txt_voucher_redeem_code) + redeemPinValidation.getVoucherCode());
        ImageUtils.loadImage(getContext(), reward != null ? reward.getProvider().getImage() : myVoucher.getProvider().getImage(),
                ContextCompat.getDrawable(getContext(), R.drawable.default_pic_store_logo), imageViewMerchant);
        buttonAwesome.setOnClickListener(v -> dismiss());
    }

    public void showFailureMessage(String message) {
        drTxtMessage.setText(message);
        drTxtMessage.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
        clearEditTextPin();
    }

    private void clearEditTextPin() {
        editTextPosition = 0;
        for (EditText editText : editTextPin) {
            editText.getText().clear();
        }
        editTextFirstpin.requestFocus();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!editTextPin[editTextPosition].getText().toString().isEmpty()) {
            if (editTextPosition < editTextPin.length - 1) {
                editTextPosition++;
                editTextPin[editTextPosition].requestFocus();
            } else if (editTextPosition == editTextPin.length - 1) {
                //UtilManis.closeKeyboard(this, vi);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            clearEditTextPin();
        } else {
            if (!editTextPin[editTextPosition].getText().toString().isEmpty()) {
                if (editTextPosition < editTextPin.length - 1) {
                    editTextPosition++;
                    editTextPin[editTextPosition].requestFocus();
                } else if (editTextPosition == editTextPin.length - 1) {
                    UtilManis.closeKeyboard(getBaseActivity(), view);
                }
            }
        }
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        int pos = 0;
        for (EditText editText : editTextPin) {
            if (view.equals(editText)) {
                editTextPosition = pos;
                editTextPin[editTextPosition].requestFocus();
            }
            pos++;
        }
        return false;
    }


}
