package com.ebizu.manis.view.dialog.redeemdialogsuccess;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.inviteterm.InviteTermDialog;
import com.ebizu.sdk.reward.models.Redeem;
import com.ebizu.sdk.reward.models.Reward;

import butterknife.OnClick;

import static com.ebizu.manis.view.dialog.redeemdialogsuccess.RedeemDialogSuccess.COMMA_SEPARATOR;

/**
 * Created by FARIS_mac on 10/4/17.
 */

public class RedeemDialogSuccessSecond extends BaseDialogManis implements View.OnClickListener{

    private ImageView imageViewMerchant, imageViewCheckTop, imageViewCheck;
    private LinearLayout linCode;
    private TextView textViewDesc, textViewCopy, textViewCode, textViewSN, textViewInstructionTitle, textViewInstruction;
    private Button buttonAwesome;

    private Redeem redeem;
    private RewardVoucher reward;

    private String code, sn, trackLink;

    public void setRedeem(Redeem redeem) {
        this.redeem = redeem;
        code = redeem.getCode();
        sn = redeem.getSn();
        trackLink = redeem.getTrackLink();
    }

    public void setReward(RewardVoucher reward) {
        this.reward = reward;
        setDialogView(reward);
    }

    public RedeemDialogSuccessSecond(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public RedeemDialogSuccessSecond(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected RedeemDialogSuccessSecond(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_redeem_point_success, null, false);
        getParent().addView(view);
        imageViewMerchant = (ImageView) findViewById(R.id.imageview_merchant);
        imageViewCheckTop = (ImageView) findViewById(R.id.imageview_check_top);
        imageViewCheck = (ImageView) findViewById(R.id.imageview_check);
        textViewDesc = (TextView) findViewById(R.id.textview_desc);
        linCode = (LinearLayout) findViewById(R.id.lin_code);
        textViewCopy = (TextView) findViewById(R.id.textview_copy);
        textViewCode = (TextView) findViewById(R.id.textview_redemption_code);
        textViewSN = (TextView) findViewById(R.id.textview_sn);
        textViewInstructionTitle = (TextView) findViewById(R.id.textview_instruction_title);
        textViewInstruction = (TextView) findViewById(R.id.textview_instruction);
        buttonAwesome = (Button) findViewById(R.id.button_close);
        buttonAwesome.setOnClickListener(v -> dismiss());
        textViewCopy.setOnClickListener(this);
        initViewMorePoints(view);
    }


    private void initViewMorePoints(View view) {
        Button buttonMorePoints = (Button) view.findViewById(R.id.button_more_points);
        buttonMorePoints.setOnClickListener((view1 -> {
            dismiss();
            getBaseActivity().showInviteFriendDialog();
        }));
    }

    private void setDialogView(RewardVoucher reward) {
        textViewDesc.setText(reward.getName());
        textViewCode.setText(code);
        textViewSN.setText(getContext().getString(R.string.serial_number) + " : " + sn);

        if (reward.getType().equalsIgnoreCase(UtilStatic.REWARD_INPUT_TYPE_ECODE) ||
                reward.getType().equalsIgnoreCase(UtilStatic.REWARD_INPUT_TYPE_ETU)) {
            imageViewCheck.setVisibility(View.GONE);
            imageViewCheckTop.setVisibility(View.VISIBLE);
            linCode.setVisibility(View.VISIBLE);
            textViewInstructionTitle.setText(R.string.redeemption_instruction);
            if (reward.getType().equalsIgnoreCase(UtilStatic.REWARD_INPUT_TYPE_ETU))
                textViewInstruction.setText(getContext().getString(R.string.redeemption_status_detail_etu));
            else
                textViewInstruction.setText(reward.getRedeemInstruction());
        } else {
            imageViewCheck.setVisibility(View.VISIBLE);
            imageViewCheckTop.setVisibility(View.GONE);
            linCode.setVisibility(View.GONE);
            textViewInstructionTitle.setText(R.string.redeemption_status);
            textViewInstruction.setText(getContext().getString(R.string.redeemption_status_detail));
        }
        ImageUtils.loadImage(getContext(), reward.getImage128(), ContextCompat.getDrawable(getContext(), R.drawable.default_pic_rewards_tile_img), imageViewMerchant);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        new AnalyticManager(getContext()).trackEvent(
                ConfigManager.Analytic.Category.DIALOG_CLOSE,
                ConfigManager.Analytic.Action.CLICK,
                "Button Close"
        );
    }

    @Override
    public void onClick(View v) {
        if (v.equals(textViewCopy)) {
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder.build();

            ClipboardManager clipboard = (ClipboardManager) getBaseActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(getContext().getString(R.string.rwd_point_dtl_redeemption_code), code);
            String[] urlTrackLink = trackLink.split(COMMA_SEPARATOR);
            if (reward.getType().equalsIgnoreCase(UtilStatic.REWARD_INPUT_TYPE_THIRDPARTY)) {
                //Validate URL
                clip = ClipData.newPlainText(getContext().getString(R.string.rwd_point_dtl_redeemption_code), urlTrackLink[1]);
                clipboard.setPrimaryClip(clip);
                if (TextUtils.isEmpty(trackLink)) {
                    Toast.makeText(getContext(), getContext().getString(R.string.rwd_point_dtl_empty_url), Toast.LENGTH_SHORT).show();
                } else {
                    if (URLUtil.isValidUrl(trackLink)) {
                        customTabsIntent.launchUrl(getContext(), Uri.parse(urlTrackLink[0]));
                    } else {
                        Toast.makeText(getContext(), getContext().getString(R.string.rwd_point_dtl_invalid_url), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            clipboard.setPrimaryClip(clip);
            Toast.makeText(getContext(), getContext().getString(R.string.rwd_point_dtl_copied), Toast.LENGTH_SHORT).show();
        }
    }
}
