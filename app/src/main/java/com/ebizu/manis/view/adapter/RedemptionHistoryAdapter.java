package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.RedemptionHistoryResult;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.sdk.entities.RewardsClaimedItem;
import com.ebizu.manis.view.dialog.redemptionhistorydetail.RedemptionHistoryDetailDialog;
import com.ebizu.manis.view.dialog.redemptionhistorydetail.RedemptionHistoryPhysicalDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 8/8/2017.
 */

public class RedemptionHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = getClass().getSimpleName();

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;

    private BaseActivity baseActivity;
    private Context mContext;
    private List<RedemptionHistoryResult> redemptionHistoryResults;

    public RedemptionHistoryAdapter(Context mContext, BaseActivity baseActivity, ArrayList<RedemptionHistoryResult> redemptionHistoryResults) {
        this.baseActivity = baseActivity;
        this.redemptionHistoryResults = redemptionHistoryResults;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, layoutInflater);
                break;
            case LOADING:
                View view = layoutInflater.inflate(R.layout.pagination_progress, parent, false);
                viewHolder = new loadingViewHolder(view);
                break;
        }
        return viewHolder;
    }

    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater layoutInflater) {
        final RecyclerView.ViewHolder viewHolder;
        View view = layoutInflater.inflate(R.layout.item_redemption_history, parent, false);
        viewHolder = new ViewHolder(view);
        view.setOnClickListener((v -> {
            int position = viewHolder.getAdapterPosition();
            RedemptionHistoryResult redemptionHistoryResult = this.redemptionHistoryResults.get(position);
            if (redemptionHistoryResult.getRewardType().equalsIgnoreCase("Physical Voucher")) {
                RedemptionHistoryPhysicalDialog redemptionHistoryDetailDialog = new RedemptionHistoryPhysicalDialog(mContext);
                redemptionHistoryDetailDialog.setRedemptionHistoryResult(redemptionHistoryResults.get(position));
                redemptionHistoryDetailDialog.setActivity(baseActivity);
                redemptionHistoryDetailDialog.show();
            } else {
                RedemptionHistoryDetailDialog redemptionHistoryDetailDialog = new RedemptionHistoryDetailDialog(mContext);
                redemptionHistoryDetailDialog.setRedemptionHistoryResult(redemptionHistoryResults.get(position));
                redemptionHistoryDetailDialog.setActivity(baseActivity);
                redemptionHistoryDetailDialog.show();
            }
            new AnalyticManager(mContext).trackEvent(
                    ConfigManager.Analytic.Category.REDEMPTION_HISTORY,
                    ConfigManager.Analytic.Action.ITEM_CLICK,
                    redemptionHistoryResult.getName()
            );
        }));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RedemptionHistoryResult redemptionHistoryResult = this.redemptionHistoryResults.get(position);
        switch (getItemViewType(position)) {
            case ITEM:
                RedemptionHistoryAdapter.ViewHolder redemptionHistoryViewHolder = (RedemptionHistoryAdapter.ViewHolder) holder;
                String point = redemptionHistoryResult.getPoint();
                ImageUtils.loadImage(mContext, redemptionHistoryResult.getAssets().getLogo(), ContextCompat.getDrawable(mContext, R.drawable.default_pic_promo_details_pic_small), redemptionHistoryViewHolder.imageViewRedemption);
                redemptionHistoryViewHolder.textViewTitle.setText(redemptionHistoryResult.getName());
                redemptionHistoryViewHolder.textViewPoints.setText(point);
                redemptionHistoryViewHolder.textViewPoints.setText(mContext.getString(R.string.text_pts, point));
                if (point.equalsIgnoreCase(RewardsClaimedItem.REWARD_FREE_POINT) || point.equalsIgnoreCase(RewardsClaimedItem.REWARD_ZERO_POINT)) {
                    redemptionHistoryViewHolder.textViewPoints.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGreen));
                    redemptionHistoryViewHolder.textViewPoints.setTextColor(ContextCompat.getColor(mContext, R.color.colorGreenDark));
                    redemptionHistoryViewHolder.textViewPoints.setText(mContext.getString(R.string.rd_txt_free));
                } else {
                    redemptionHistoryViewHolder.textViewPoints.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPink));
                    redemptionHistoryViewHolder.textViewPoints.setTextColor(ContextCompat.getColor(mContext, android.R.color.white));
                }
                redemptionHistoryViewHolder.textViewCategory.setText(redemptionHistoryResult.getStore());
                break;
            case LOADING:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return redemptionHistoryResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == redemptionHistoryResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    public void add(RedemptionHistoryResult r) {
        redemptionHistoryResults.add(r);
        notifyItemInserted(redemptionHistoryResults.size() - 1);
    }

    public void addAll(List<RedemptionHistoryResult> moveResults) {
        for (RedemptionHistoryResult result : moveResults) {
            add(result);
        }
    }

    public void replaceRedemptionHistory(List<RedemptionHistoryResult> redemptionHistoryResults) {
        this.redemptionHistoryResults.clear();
        this.redemptionHistoryResults = redemptionHistoryResults;
        notifyDataSetChanged();
    }

    public void remove(RedemptionHistoryResult r) {
        int position = redemptionHistoryResults.indexOf(r);
        if (position > -1) {
            redemptionHistoryResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new RedemptionHistoryResult());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = redemptionHistoryResults.size() - 1;
        RedemptionHistoryResult result = getItem(position);

        if (result != null) {
            redemptionHistoryResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public RedemptionHistoryResult getItem(int position) {
        return redemptionHistoryResults.get(position);
    }


    private class loadingViewHolder extends RecyclerView.ViewHolder {
        public loadingViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageview_redeem)
        ImageView imageViewRedemption;
        @BindView(R.id.textview_points)
        TextView textViewPoints;
        @BindView(R.id.textview_title)
        TextView textViewTitle;
        @BindView(R.id.textview_merchant)
        TextView textViewCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
