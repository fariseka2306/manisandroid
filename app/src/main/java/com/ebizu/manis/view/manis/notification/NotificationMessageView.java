package com.ebizu.manis.view.manis.notification;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 12/07/2017.
 */

public class NotificationMessageView extends RelativeLayout {

    @BindView(R.id.popup_layout)
    RelativeLayout popupLayout;
    @BindView(R.id.text_message)
    TextView textViewMessage;
    @BindView(R.id.image_close)
    ImageView imageViewClose;

    private static final int POPUP_TIMEOUT = 3500;

    private Handler myHandler = new Handler();
    private Runnable myRunnable = this::dismissNotif;

    public NotificationMessageView(Context context) {
        super(context);
        createView(context);
    }

    public NotificationMessageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public NotificationMessageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NotificationMessageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    private void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_notification_message, null, false);
        addView(view);
        ButterKnife.bind(this, view);
        imageViewClose.setOnClickListener((v -> dismissNotif()));
    }

    public void showNotif(String message) {
        if (!popupLayout.isShown()) {
            popupLayout.setVisibility(View.VISIBLE);
            Animation slide_down = AnimationUtils.loadAnimation(getContext(),
                    R.anim.slide_in_down_from_top);
            slide_down.setInterpolator(new AccelerateDecelerateInterpolator());
            popupLayout.startAnimation(slide_down);
            textViewMessage.setText(message);
            myHandler.postDelayed(myRunnable, POPUP_TIMEOUT);
        }
    }

    public void showNotif(String message, int colorBackground, int textColor) {
        popupLayout.setBackgroundColor(colorBackground);
        textViewMessage.setTextColor(textColor);
        showNotif(message);
    }

    public void dismissNotif() {
        if (popupLayout.isShown()) {
            Animation slide_up = AnimationUtils.loadAnimation(getContext(),
                    R.anim.slide_out_up_from_top);
            slide_up.setInterpolator(new AccelerateDecelerateInterpolator());
            slide_up.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    popupLayout.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            popupLayout.startAnimation(slide_up);
            myHandler.removeCallbacks(myRunnable);
        }
    }
}
