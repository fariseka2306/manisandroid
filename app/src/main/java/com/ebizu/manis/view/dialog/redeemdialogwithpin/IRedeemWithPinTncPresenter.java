package com.ebizu.manis.view.dialog.redeemdialogwithpin;

import com.ebizu.manis.service.reward.requestbody.RedeemPinTncBody;

/**
 * Created by FARIS_mac on 10/4/17.
 */

public interface IRedeemWithPinTncPresenter {

    void loadBrandTerm(RedeemPinTncBody redeemPinTncBody);

}
