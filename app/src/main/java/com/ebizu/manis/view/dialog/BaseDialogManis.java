package com.ebizu.manis.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.manis.blur.BlurDrawable;
import com.ebizu.manis.view.manis.nointernetconnection.NoInternetConnectionView;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class BaseDialogManis extends Dialog implements IBaseDialog {

    private BaseActivity baseActivity;
    private BaseDialogPresenter baseDialogPresenter;
    private ConstraintLayout noInternetConnectionView;
    private ProgressBar progressBar;
    private RelativeLayout parent;
    private ManisSession manisSession;
    private DeviceSession deviceSession;

    public RelativeLayout getParent() {
        return parent;
    }

    public BaseDialogManis(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public BaseDialogManis(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected BaseDialogManis(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        getWindow().getAttributes().dimAmount =  1f;
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_manis, null, false);
        addContentView(view, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        parent = (RelativeLayout) view.findViewById(R.id.parent);
        addDialogView(context);
    }

    protected void addDialogView(Context context) {

    }

    public void addDialogView(View view) {
        parent.addView(view);
        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        view.setLayoutParams(layoutParams);
    }

    @Override
    public void attachDialogPresenter(BaseDialogPresenter baseDialogPresenter) {
        this.baseDialogPresenter = baseDialogPresenter;
    }

    @Override
    public void showNoInternetConnection() {
        if (noInternetConnectionView == null)
            addViewInternetConnection();
        if (!noInternetConnectionView.isShown())
            noInternetConnectionView.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissNoInternetConnection() {
        if (noInternetConnectionView != null)
            noInternetConnectionView.setVisibility(View.GONE);
    }

    @Override
    public void showBaseProgressBar() {
        if (progressBar == null)
            addViewProgressBar();
        if (!progressBar.isShown()) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void dismissBaseProgressBar() {
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onRetry() {
        noInternetConnectionView.setVisibility(View.GONE);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    @Override
    public void showAlertDialog(String title, String message, boolean cancelAble, int drawable, String positive, OnClickListener positiveListener) {
        new AlertDialog.Builder(getContext()).setTitle(title).setMessage(message).setCancelable(cancelAble).setIcon(drawable)
                .setPositiveButton(positive, positiveListener)
                .create().show();
    }

    @Override
    public BaseActivity getBaseActivity() {
        return baseActivity;
    }

    private void addViewProgressBar() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.progress_bar, null, false);
        parent.addView(view, getParent().getLayoutParams());
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
    }

    private void addViewInternetConnection() {
        NoInternetConnectionView view = new NoInternetConnectionView(getContext());
        parent.addView(view, getParent().getLayoutParams());
        noInternetConnectionView = (ConstraintLayout) parent.findViewById(R.id.no_network_view);
        centerOnViewGroup(this.noInternetConnectionView);
        noInternetConnectionView.setVisibility(View.GONE);
        noInternetConnectionView.setOnClickListener(v -> onRetry());
    }

    private void centerOnViewGroup(View view) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        view.setLayoutParams(layoutParams);
    }

    protected ManisSession getManisSession() {
        if (manisSession == null) {
            manisSession = new ManisSession(getContext());
        }
        return manisSession;
    }

    protected DeviceSession getDeviceSession() {
        if (deviceSession == null) {
            deviceSession = new DeviceSession(getContext());
        }
        return deviceSession;
    }
}