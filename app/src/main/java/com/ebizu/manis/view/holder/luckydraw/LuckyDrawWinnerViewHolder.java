package com.ebizu.manis.view.holder.luckydraw;

import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.model.LuckyDrawWinner;
import com.ebizu.manis.view.holder.BaseHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 12/12/17.
 */

public class LuckyDrawWinnerViewHolder extends BaseHolder<LuckyDrawWinner> {


    @BindView(R.id.ld_winner_txt_user)
    TextView txtUserName;
    @BindView(R.id.ld_winner_txt_desc)
    TextView txtDescription;
    @BindView(R.id.ld_winner_txt_time)
    TextView txtTime;

    public LuckyDrawWinnerViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void setHolderView(LuckyDrawWinner model) {
        txtUserName.setText(model.getAccScreenName());
        txtDescription.setText(Html.fromHtml(model.getPrizePrefix() + " <b>" + model.getPrizeObject() + "</b>"));
        txtTime.setText(UtilManis.getTimeAgoMiddle(model.getdDay(), itemView.getContext()));
    }
}
