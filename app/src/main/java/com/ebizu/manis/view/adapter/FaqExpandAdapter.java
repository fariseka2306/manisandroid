package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.model.FaqChild;
import com.ebizu.manis.mvp.account.accountmenulist.faq.FaqDetailActivity;
import com.ebizu.manis.view.holder.FaqChildViewHolder;

import java.util.List;

/**
 * Created by abizu-alvio on 8/3/2017.
 */

public class FaqExpandAdapter extends RecyclerView.Adapter<FaqChildViewHolder> {

    private final String TAG = getClass().getSimpleName();

    private Context mContext;
    private List<FaqChild> faqChildList;

    public FaqExpandAdapter(Context mContext, List<FaqChild> faqChildList) {
        this.mContext = mContext;
        this.faqChildList = faqChildList;
    }

    @Override
    public FaqChildViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.faq_child_item_list, parent, false);
        final FaqChildViewHolder viewHolder = new FaqChildViewHolder(itemView);
        itemView.setOnClickListener(view -> {
            int position = viewHolder.getAdapterPosition();
            Intent intent = new Intent(mContext, FaqDetailActivity.class);
            intent.putExtra(FaqDetailActivity.EXTRA_FAQ, faqChildList.get(position));
            mContext.startActivity(intent);
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(FaqChildViewHolder holder, int position) {
        FaqChild faqChild = this.faqChildList.get(position);
        holder.setFaqChildItemView(faqChild);
    }

    @Override
    public int getItemCount() {
        return faqChildList.size();
    }

    public void addFaqChildList(List<FaqChild> faqChildren) {
        faqChildList.clear();
        faqChildList.addAll(faqChildren);
        notifyDataSetChanged();
    }
}
