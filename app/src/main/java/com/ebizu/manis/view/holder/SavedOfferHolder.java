package com.ebizu.manis.view.holder;

import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.model.saved.Offer;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 11/29/17.
 */

public class SavedOfferHolder extends BaseHolder<Offer> {

    @BindView(R.id.textview_company)
    TextView textViewTitle;
    @BindView(R.id.textview_category)
    TextView textViewDesc;
    @BindView(R.id.imageview_promo)
    ImageView imageViewPromo;
    @BindView(R.id.imageview_merchant)
    ImageView imageViewMerchant;

    public SavedOfferHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void setHolderView(Offer model) {
        try {
            textViewTitle.setText(model.getStore().getName());
            textViewDesc.setText(model.getTitle());
            ImageUtils.loadImage(context,
                    model.getPhoto(),
                    ContextCompat.getDrawable(context, R.drawable.default_pic_store_logo),
                    imageViewPromo);
            ImageUtils.loadImage(itemView.getContext(),
                    model.getStore().getAssets().getPhoto(),
                    ContextCompat.getDrawable(context, R.drawable.default_pic_store_logo),
                    imageViewMerchant);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "setHolderView: ".concat(e.getMessage()));
        }
    }
}
