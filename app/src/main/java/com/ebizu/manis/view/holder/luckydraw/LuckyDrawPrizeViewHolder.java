package com.ebizu.manis.view.holder.luckydraw;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.manager.tracker.TrackerConstant;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.PrizeDetail;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 04/08/17.
 */

public class LuckyDrawPrizeViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_price_image)
    RoundedImageView ivPriceImage;

    private PrizeDetail prizeDetail;

    public LuckyDrawPrizeViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        view.setOnClickListener(
                v -> TrackerManager.getInstance().setTrackerData(
                        new TrackerStandartRequest(TrackerConstant.kTrackerOriginPageLuckyDraw, TrackerConstant.kTrackerOriginPageLuckyDraw, TrackerConstant.kTrackerComponentList, TrackerConstant.kTrackerSubFeatureImagePrizesThisWeek, String.valueOf(prizeDetail.getPrizeId()), prizeDetail.getPrizeName())
                ));
    }

    public void setPrizeDetailView(PrizeDetail prizeDetail) {
        Glide.with(itemView.getContext())
                .load(prizeDetail.getPrizeImage())
                .fitCenter()
                .into(ivPriceImage);
        this.prizeDetail = prizeDetail;
    }
}
