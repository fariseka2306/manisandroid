package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.model.missions.Missions;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class MissionsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Missions> shareExperiencesList;
    private Context context;
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private OnClickListener mListener;

    public MissionsListAdapter(ArrayList<Missions> shareExperiencesList, Context context) {
        this.shareExperiencesList = shareExperiencesList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, layoutInflater);
                break;
            case LOADING:
                View view = layoutInflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new loadingViewHolder(view);
        }
        return viewHolder;
    }

    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater layoutInflater) {
        final RecyclerView.ViewHolder viewHolder;
        View view = layoutInflater.inflate(R.layout.item_holder_missions_list, parent, false);
        viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        Missions missions = shareExperiencesList.get(position);
        switch (getItemViewType(position)) {
            case ITEM:
                final ViewHolder holder = (ViewHolder) viewHolder;
                ImageUtils.loadImage(context, missions.getAsset().getIconMission(), ContextCompat.getDrawable(context, R.drawable.default_pic_store_logo), holder.imageViewMissionType);
                ImageUtils.loadImage(context, missions.getAsset().getLogo(), ContextCompat.getDrawable(context, R.drawable.default_pic_store_logo), holder.imageViewMerchantBanner);
                ImageUtils.loadImage(context, missions.getAsset().getIcon(), ContextCompat.getDrawable(context, R.drawable.default_pic_store_logo), holder.imageViewPointValue);
                holder.textViewMissionsName.setText(missions.getMissionName());
                holder.textViewPointValue.setText(String.valueOf(missions.getIncentive().getAmount()));
                holder.textViewMissionsType.setText(missions.getMissionTypeName());
                holder.textViewPoint.setText(missions.getIncentive().getForm());
                holder.itemView.setOnClickListener(v -> {
                    if (mListener != null) mListener.setOnClickListener(v, missions);
                });
                break;
            case LOADING:
                //    DO NOTHING
                break;
        }
    }

    @Override
    public int getItemCount() {
        return shareExperiencesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == shareExperiencesList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void addShareExperienceList(List<Missions> missionses) {
        shareExperiencesList.clear();
        shareExperiencesList.addAll(missionses);
        notifyDataSetChanged();
    }

    public void addShareExperience(List<Missions> missionses) {
        this.shareExperiencesList.addAll(missionses);
        notifyItemRangeInserted(this.shareExperiencesList.size() + 1, missionses.size());
    }

    public void add(Missions r) {
        shareExperiencesList.add(r);
        notifyItemInserted(shareExperiencesList.size() - 1);
    }

    public void addAll(List<Missions> moveResults) {
        for (Missions result : moveResults) {
            add(result);
        }
    }

    public void replaceShareExperience(List<Missions> missionses) {
        this.shareExperiencesList = missionses;
        notifyDataSetChanged();
    }

    public void remove(Missions r) {
        int position = shareExperiencesList.indexOf(r);
        if (position > -1) {
            shareExperiencesList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Missions());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = shareExperiencesList.size() - 1;
        Missions result = getItem(position);

        if (result != null) {
            shareExperiencesList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Missions getItem(int position) {
        return shareExperiencesList.get(position);
    }

    private class loadingViewHolder extends RecyclerView.ViewHolder {
        public loadingViewHolder(View view) {
            super(view);
        }
    }

    public void setMissionClick(OnClickListener listener) {
        this.mListener = listener;
    }

    public interface OnClickListener {
        void setOnClickListener(View view, Missions missions);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageview_merchant)
        RoundedImageView imageViewMerchantBanner;
        @BindView(R.id.textview_missions_name)
        TextView textViewMissionsName;
        @BindView(R.id.textview_missions_type)
        TextView textViewMissionsType;
        @BindView(R.id.textview_point_value)
        TextView textViewPointValue;
        @BindView(R.id.textview_point)
        TextView textViewPoint;
        @BindView(R.id.iv_mission_type_icon)
        ImageView imageViewMissionType;
        @BindView(R.id.iv_point_value)
        ImageView imageViewPointValue;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
