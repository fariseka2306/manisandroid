package com.ebizu.manis.view.manis.toolbar;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.root.BaseView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ebizu on 10/9/17.
 */

public class ToolbarNotification extends BaseView {

    @BindView(R.id.txt_title_action)
    TextView textViewTitle;
    @BindView(R.id.img_left)
    ImageView imageView;

    public ToolbarNotification(Context context) {
        super(context);
        createView(context);
    }

    public ToolbarNotification(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ToolbarNotification(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ToolbarNotification(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    public void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.actionbar_back, null, false);
        addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
    }

    public void setTitle(int title) {
        textViewTitle.setText(title);
    }

    public void setTitle(String title) {
        textViewTitle.setText(title);
    }

    public void setTitle(int title, int drawable) {
        textViewTitle.setText(title);
        imageView.setImageResource(drawable);
    }

    public void setTitle(String title, int drawable) {
        textViewTitle.setText(title);
        imageView.setImageAlpha(drawable);
    }

    @OnClick(R.id.rel_left)
    void onBackPressed() {
        ((MainActivity) getContext()).closeDrawer();
    }

}
