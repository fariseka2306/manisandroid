package com.ebizu.manis.view.manis.textinput;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.ebizu.manis.R;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by default on 14/11/17.
 */

public class AccountProfileAddressTextInput extends LinearLayout implements InputFilter {

    @BindView(R.id.edittext_account_address_input)
    EditText editTextAddressInput;

    private boolean enableSpecialChar = false;

    public AccountProfileAddressTextInput(Context context) {
        super(context);
        CreateView(context);
    }

    public AccountProfileAddressTextInput(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        CreateView(context);
    }

    public AccountProfileAddressTextInput(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CreateView(context);
    }

    public void CreateView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.text_input_account_address_profile,null);
        addView(view, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this, view);
        editTextAddressInput.setRawInputType(InputType.TYPE_CLASS_TEXT);
    }

    public void setInputAccountAdress(String text){
        editTextAddressInput.setText(text);
    }

    public void setHintAccountAdrress(String hintAccountAdrress) {
        editTextAddressInput.setHint(hintAccountAdrress);
    }

    public void setKeyboardAccountAddress(int inputType){
        editTextAddressInput.setInputType(inputType);
    }

    public void setImeOptionAccountAddress(int imeOption){
        editTextAddressInput.setImeOptions(imeOption);
    }

    public void setLengthAccountAddress (int length, boolean enableSpecialChar){
        this.enableSpecialChar= enableSpecialChar;
        editTextAddressInput.setFilters(new InputFilter[]{
                this, new InputFilter.LengthFilter(length)
        });
    }

    public void setMaxLength(int maxLength){

    }

    public String getText(){
        return editTextAddressInput.getText().toString();
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        for (int i = start; i < end; ++i)
        {
            if(enableSpecialChar)
                if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890]*").matcher(String.valueOf(source.charAt(i))).matches())
                {
                    return "";
                }
        }
        return null;
    }

}
