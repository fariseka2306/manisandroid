package com.ebizu.manis.view.manis.interest.model;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public interface InterestModelView {

    int id();

    Drawable drawable(Context context);

    int color(Context context);

}
