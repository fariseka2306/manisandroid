package com.ebizu.manis.view.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public class RewardGridViewHolder extends RecyclerView.ViewHolder {

    public TextView tvRewardName;
    public TextView tvProviderName;
    public TextView tvPoint;
    public TextView tvRewardValue;
    public TextView tvOutOfStock;
    public ImageView ivReward;
    public ImageView frpiImgBucket;
    public ImageView frpiImgManis;
    public LinearLayout rlImgPoint;
    public RelativeLayout rlBtnFree;

    public RewardGridViewHolder(View itemView) {
        super(itemView);
        ivReward = (ImageView) itemView.findViewById(R.id.frpi_img_redeem);
        frpiImgBucket = (ImageView) itemView.findViewById(R.id.frpi_img_bucket);
        frpiImgManis = (ImageView) itemView.findViewById(R.id.frpi_img_manis);
        tvRewardName = (TextView) itemView.findViewById(R.id.frpi_txt_title);
        tvProviderName = (TextView) itemView.findViewById(R.id.frpi_txt_category);
        tvPoint = (TextView) itemView.findViewById(R.id.frpi_txt_points);
        tvRewardValue = (TextView) itemView.findViewById(R.id.frpi_txt_value);
        tvOutOfStock = (TextView) itemView.findViewById(R.id.frpi_txt_outofstock);
        rlImgPoint = (LinearLayout) itemView.findViewById(R.id.rl_img_point);
        rlBtnFree = (RelativeLayout) itemView.findViewById(R.id.rl_btn_free);
    }

    public class loadingViewHolder extends RecyclerView.ViewHolder {
        public loadingViewHolder(View view) {
            super(view);
        }
    }

}
