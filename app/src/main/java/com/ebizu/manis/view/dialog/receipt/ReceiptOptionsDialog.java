package com.ebizu.manis.view.dialog.receipt;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;

import com.ebizu.manis.view.dialog.BaseDialogManis;

/**
 * Created by andrifashbir on 07/11/17.
 */

public class ReceiptOptionsDialog extends BaseDialogManis {

    public ReceiptOptionsDialog(@NonNull Context context) {
        super(context);
    }

    public ReceiptOptionsDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected ReceiptOptionsDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }
}
