package com.ebizu.manis.view.walkthrough.walkthroughpage;

import com.ebizu.manis.root.BaseActivity;

/**
 * Created by mac on 8/29/17.
 */

public interface IWalkthroughPage {

    void onWalkthroughClick();

    void setActivity(BaseActivity appCompatActivity);

    BaseActivity getBaseActivity();

    void startPage();

    void finishPage();

    int page();

    void setOnNextPageListener(OnNextPageListener onNextPageListener);

    interface OnNextPageListener {
        void onNextPage(int currentPage);
    }

}
