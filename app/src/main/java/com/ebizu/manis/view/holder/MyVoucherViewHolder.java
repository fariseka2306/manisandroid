package com.ebizu.manis.view.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ebizu.manis.R;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.MyVouchersView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 9/18/17.
 */

public class MyVoucherViewHolder extends RecyclerView.ViewHolder{

    private Context context;

    private MyVouchersView myVouchersView;

    @BindView(R.id.frpi_rl_voucher)
    RelativeLayout frpiRlVoucher;

    @BindView(R.id.frpi_img_redeem)
    ImageView imageViewReward;

    @BindView(R.id.frpi_txt_title)
    TextView textViewRewardName;

    @BindView(R.id.frpi_txt_category)
    TextView textViewProviderName;

    @BindView(R.id.frpi_txt_points)
    TextView textViewPoint;

    @BindView(R.id.frpi_txt_value)
    TextView textViewRewardValue;

    @BindView(R.id.frpi_txt_outofstock)
    TextView textViewOutOfStock;

    @BindView(R.id.view_group_info_redeem)
    View viewInfoRedeem;

    RewardVoucher rewardVoucher = new RewardVoucher();
    private MyVoucherListener voucherListener;

    public MyVoucherViewHolder(View itemView, final MyVoucherListener listener) {
        super(itemView);
        this.context = itemView.getContext();
        setOnMyVoucherListener(listener);
        ButterKnife.bind(this,itemView);
    }

    public void setMyVoucherView(final RewardVoucher reward, MyVouchersView myVouchersView) {

        try {
            this.myVouchersView = myVouchersView;
            textViewRewardName.setText(reward.getName());
            textViewProviderName.setText(reward.getProvider().getName());
            int dataValue = reward.getPoint();
            textViewPoint.setText((dataValue == 0) ? context.getString(R.string.rd_txt_free) : dataValue + " " + context.getString(R.string.rd_txt_pts));
            setPurchaseAction(reward);
            textViewOutOfStock.setVisibility(View.INVISIBLE);

            itemView.setOnClickListener(v -> voucherListener.onMyVoucherListener(reward));
            loadImage(reward.getImage128(), R.drawable.default_pic_promo_details_pic_small, imageViewReward);
        } catch (Exception e) {
            Log.e("context", String.valueOf(e));
        }

    }


    private void setPurchaseAction(RewardVoucher reward) {
        if (reward.isHidePrice()) {
            textViewRewardValue.setVisibility(View.GONE);
        } else {
            textViewRewardValue.setVisibility(View.VISIBLE);
            textViewRewardValue.setText(reward.getCurrency() + " " + reward.getValue());
        }
    }

    private void loadImage(Object image, int placeHolder, ImageView target) {
        Glide.with(context)
                .load(image)
                .thumbnail(0.1f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .placeholder(placeHolder)
                .animate(R.anim.fade_in_image)
                .into(target);
    }

    private void setOnMyVoucherListener(MyVoucherListener myVoucherListener){
        this.voucherListener = myVoucherListener;
    }

    public interface MyVoucherListener {
        void onMyVoucherListener(RewardVoucher rewardVoucher);
    }
}
