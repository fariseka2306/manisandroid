package com.ebizu.manis.view.manis.textinput;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileView;
import com.ebizu.manis.service.manis.requestbody.AccountBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by default on 13/11/17.
 */

public class AccountProfileTextInput extends LinearLayout implements View.OnFocusChangeListener{

    @BindView(R.id.text_view_title_account_input)
    TextView textViewTitleAccountInput;
    @BindView(R.id.edittext_account_input)
    EditText editTextAccountInput;

    private Calendar calendar = Calendar.getInstance();
    private AnalyticManager analyticManager;
    private AccountBody accountBody;
    private ProfileView profileView;

    public AccountProfileTextInput(Context context) {
        super(context);
        createView(context);
    }

    public AccountProfileTextInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public AccountProfileTextInput(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    protected void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.text_input_account_profile,null,false);
        addView(view, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
        ButterKnife.bind(this,view);
        editTextAccountInput.setRawInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
    }

    public void setTitleAccount(String title) {
        textViewTitleAccountInput.setText(title);
        textViewTitleAccountInput.setAllCaps(true);
    }

    public void setHintAccount(String hint){
        editTextAccountInput.setHint(hint);
    }

    public void setKeyboardAccount(int inputType){
        editTextAccountInput.setInputType(inputType);
    }

    public void setInputAccount (String text) {
        editTextAccountInput.setText(text);
    }

    public void setOnAcccountProfileListener (AnalyticManager analyticManager, ProfileView profileView) {
        this.analyticManager = analyticManager;
        this.profileView = profileView;
        editTextAccountInput.setOnFocusChangeListener(this);
    }

    public String getText(){
        return editTextAccountInput.getText().toString();
    }

    public void setImeOptionAccount(int imeOption){
        editTextAccountInput.setImeOptions(imeOption);
    }

    public void setEnableAccount (boolean enableAccount) {
        editTextAccountInput.setEnabled(enableAccount);
    }

    private void setBirthdate() {
        DatePickerDialog.OnDateSetListener date = (view, year1, monthOfYear, dayOfMonth) -> {
            accountBody = new AccountBody();
            calendar.set(Calendar.YEAR, year1);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat formatterStr = new SimpleDateFormat("dd MMM yyyy");
            String strDate = formatterStr.format(new Date(calendar.getTimeInMillis()));
            String strBirth = strDate.trim();
            editTextAccountInput.setText(strDate);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
            Date dateBirthday = null;
            try {
                dateBirthday = dateFormat.parse(strBirth);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long timeBirthdate = dateBirthday.getTime() / 1000L;
            accountBody.setBirthdate(Long.toString(timeBirthdate));
            profileView.setBirthDate(accountBody.getBirthdate());
        };
        new DatePickerDialog(getContext(), date, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
        analyticManager.trackEvent(
                ConfigManager.Analytic.Category.PROFILE_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "EditText Birthdate");
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v.equals(editTextAccountInput)) {
            if (hasFocus) {
                setBirthdate();
            }
        }

    }

}
