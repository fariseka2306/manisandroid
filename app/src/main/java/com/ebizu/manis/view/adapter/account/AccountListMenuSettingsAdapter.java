package com.ebizu.manis.view.adapter.account;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.manager.accountlistmenusettings.AccountListSettings;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/20/2017.
 */

public class AccountListMenuSettingsAdapter extends RecyclerView.Adapter<AccountListMenuSettingsAdapter.ViewHolder> {

    Context mContext;
    private AccountMenuListener accountMenuListener;
    private List<AccountListSettings> modelAccountList;

    public AccountListMenuSettingsAdapter(Context mContext, List<AccountListSettings> modelAccountList) {
        this.mContext = mContext;
        this.modelAccountList = modelAccountList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_menulist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        AccountListSettings accountList = modelAccountList.get(position);
        viewHolder.icon.setImageResource(accountList.thumbnail());
        viewHolder.name.setText(accountList.name());
        viewHolder.parentAccount.setOnClickListener((v -> accountMenuListener.onClick(accountList)));
    }

    public void addAccountList(AccountListSettings accountListMenuSettings) {
        this.modelAccountList.add(accountListMenuSettings);
        notifyDataSetChanged();
    }

    public void setAccountListener(AccountMenuListener accountListener) {
        this.accountMenuListener = accountListener;
    }

    @Override
    public int getItemCount() {
        return modelAccountList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_name_list)
        TextView name;
        @BindView(R.id.image_view_icon_list)
        ImageView icon;
        @BindView(R.id.parent_account_list)
        LinearLayout parentAccount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AccountMenuListener {
        void onClick(AccountListSettings accountList);
    }
}