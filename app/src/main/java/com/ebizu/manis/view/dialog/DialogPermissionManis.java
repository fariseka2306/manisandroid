package com.ebizu.manis.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.R;

/**
 * Created by Ebizu-User on 11/07/2017.
 */

public class DialogPermissionManis extends Dialog {

    private Button btnOnPermission;
    private Button btnCancelPermission;

    public DialogPermissionManis(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public DialogPermissionManis(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected DialogPermissionManis(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, (DialogInterface.OnCancelListener) cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_permission_marshmallow);

        TextView txtGps = (TextView) findViewById(R.id.dp_txt_gps);
        TextView txtStorage = (TextView) findViewById(R.id.dp_txt_storage);
        TextView txtCamera = (TextView) findViewById(R.id.dp_txt_camera);
        TextView txtPhone = (TextView) findViewById(R.id.dp_txt_phone);
        btnOnPermission = (Button) findViewById(R.id.dp_btn_on);
        btnCancelPermission = (Button) findViewById(R.id.dp_btn_cancel);

        txtGps.setText(UtilManis.fromHtml(context.getString(R.string.dp_txt_gps)));
        txtStorage.setText(UtilManis.fromHtml(context.getString(R.string.dp_txt_storage)));
        txtCamera.setText(UtilManis.fromHtml(context.getString(R.string.dp_txt_camera)));
        txtPhone.setText(UtilManis.fromHtml(context.getString(R.string.dp_txt_phone)));
    }

    public void setNextListener(OnNextlListener onNextlListener) {
        btnOnPermission.setOnClickListener((view) -> onNextlListener.clickNext());
    }

    public void setCancelListener(OnCancelListener onCancelListener) {
        btnCancelPermission.setOnClickListener((view) -> onCancelListener.clickCancel());
    }

    public interface OnCancelListener {
        void clickCancel();
    }

    public interface OnNextlListener {
        void clickNext();
    }
}