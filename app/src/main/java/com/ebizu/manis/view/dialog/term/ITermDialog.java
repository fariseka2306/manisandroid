package com.ebizu.manis.view.dialog.term;

import com.ebizu.manis.model.Term;
import com.ebizu.manis.view.dialog.IBaseDialog;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public interface ITermDialog extends IBaseDialog {

    void setViewTerm(Term term);

    void setNegativeScenarioView(String message);
}
