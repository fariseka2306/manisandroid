package com.ebizu.manis.view.spendingbar;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.Statistic;

import java.util.List;

/**
 * Created by abizu-alvio on 7/18/2017.
 */

public class SpendingBarTextView extends LinearLayout {

    private double maxWidthPercenteage = 80;
    private double minWidthPercenteage = 5;

    public SpendingBarTextView(Context context) {
        super(context);
        setView(context);
    }

    public SpendingBarTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setView(context);
    }

    public SpendingBarTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SpendingBarTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setView(context);
    }

    private void setView(Context context) {
        setOrientation(HORIZONTAL);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.text_view_spending_bar, null, false);
        addView(view);
    }

    public void setSpendingBarTextView(List<Statistic> statistics) {
        removeAllViews();
        for (Statistic statistic : statistics) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.text_view_spending_bar, null, false);
            addView(view);
            TextView textViewStatistic = (TextView) view.findViewById(R.id.text_view_spending_bar);
            textViewStatistic.setText(String.valueOf(statistic.getPercentage()) + "%");
            setChildTextViewLayoutParam(textViewStatistic, statistic.getPercentage());
        }
    }

    private void setChildTextViewLayoutParam(TextView textView, double percentage) {
        double totalWitdh = getWidth();
        double textViewWidth = 0;
        if (percentage < minWidthPercenteage) {
            textViewWidth = totalWitdh * minWidthPercenteage / 100;
        } else if (percentage > maxWidthPercenteage && percentage < 99) {
            textViewWidth = totalWitdh * maxWidthPercenteage / 100;
        } else if (percentage > 40) {
            textViewWidth = totalWitdh * (percentage - 4) / 100;
        } else {
            textViewWidth = totalWitdh * percentage / 100;
        }
        textView.setWidth((int) textViewWidth);
    }
}
