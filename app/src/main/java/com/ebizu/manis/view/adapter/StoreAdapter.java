package com.ebizu.manis.view.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.Store;
import com.ebizu.manis.mvp.store.storedetail.StoreDetailActivity;
import com.ebizu.manis.root.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firef on 7/10/2017.
 */

public class StoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private BaseActivity activity;
    private List<Store> stores;
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private OnItemClickListener onItemClickListener;

    public StoreAdapter(BaseActivity activity, List<Store> stores) {
        this.activity = activity;
        this.stores = stores;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        switch (position) {
            case ITEM:
                viewHolder = getViewHolder(parent, layoutInflater);
                break;
            case LOADING:
                View view = layoutInflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        Store store = stores.get(position);
        switch (getItemViewType(position)) {
            case ITEM:
                StoreViewHolder storeViewHolder = (StoreViewHolder) viewHolder;
                storeViewHolder.setStore(stores.get(position));
                storeViewHolder.parent.setOnClickListener(view -> {
                    Intent intent = new Intent(activity, StoreDetailActivity.class);
                    intent.putExtra(ConfigManager.Store.STORE_DETAIL_TITLE, store);
                    activity.startActivityForResult(intent, ConfigManager.Snap.RECEIPT_REQUEST_CODE);
                    if (null != onItemClickListener)
                        onItemClickListener.onClick(store);
                });
                break;
            case LOADING:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return stores != null ? stores.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == stores.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void addStores(List<Store> stores) {
        this.stores.addAll(stores);
        notifyItemRangeInserted(this.stores.size() + 1, stores.size());
    }

    public void add(Store r) {
        stores.add(r);
        notifyItemInserted(stores.size() - 1);
    }

    public void replaceStores(List<Store> stores) {
        this.stores = stores;
        notifyDataSetChanged();
    }

    public void addAll(List<Store> moveResults) {
        for (Store result : moveResults) {
            add(result);
        }
    }

    public void remove(Store r) {
        int position = stores.indexOf(r);
        if (position > -1) {
            stores.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Store());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = stores.size() - 1;
        Store result = getItem(position);

        if (result != null) {
            stores.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clearStores() {
        this.stores = new ArrayList<>();
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        return this.stores.size() < 1;
    }

    public Store getItem(int position) {
        return stores.get(position);
    }

    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater layoutInflater) {
        final RecyclerView.ViewHolder viewHolder;
        View view = layoutInflater.inflate(R.layout.item_holder_store_list, parent, false);
        viewHolder = new StoreViewHolder(view);
        return viewHolder;
    }

    public List<Store> getStores() {
        return stores;
    }

    public void setOnClickItemListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onClick(Store store);
    }
}
