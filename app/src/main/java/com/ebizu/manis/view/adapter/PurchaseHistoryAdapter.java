package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.PurchaseHistory;
import com.ebizu.manis.view.holder.PurchasedVoucherViewHolder;

import java.util.List;

/**
 * Created by ebizu on 9/14/17.
 */

public class PurchaseHistoryAdapter extends ListAdapter<PurchaseHistory, PurchasedVoucherViewHolder> {

    public PurchaseHistoryAdapter(Context context, RecyclerView recyclerView) {
        super(R.layout.item_purchase_voucher_history, context, recyclerView);
    }
}
