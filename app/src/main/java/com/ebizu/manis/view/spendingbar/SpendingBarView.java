package com.ebizu.manis.view.spendingbar;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.manager.categorycolor.CategoryColorManager;
import com.ebizu.manis.model.Statistic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abizu-alvio on 7/18/2017.
 */

public class SpendingBarView extends LinearLayout {

    private float radiusCornerBar = 12.0f;

    public SpendingBarView(Context context) {
        super(context);
        setView(context);
    }

    public SpendingBarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setView(context);
    }

    public SpendingBarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SpendingBarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setView(context);
    }

    private void setView(Context context) {
        setOrientation(HORIZONTAL);
    }

    public void setSpendingBarView(List<Statistic> statistics) {
        removeAllViews();
        List<View> viewStatistics = new ArrayList<>();
        for (Statistic statistic : statistics) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.view_spending_bar, null, false);
            addView(view);
            View viewStatistic = view.findViewById(R.id.view_spending_bar);
            viewStatistics.add(viewStatistic);
            setChildViewLayoutParam(viewStatistic, statistic.getPercentage());
        }
        setColorRadius(viewStatistics);
    }

    private void setChildViewLayoutParam(View viewStatistic, double percentage) {
        double totalWidth = getWidth();
        double viewWidhth = totalWidth * percentage / 100;
        LayoutParams layoutParams = new LayoutParams((int) viewWidhth, 50);
        viewStatistic.setLayoutParams(layoutParams);
    }

    private void setColorRadius(List<View> viewStatistics) {
        for (int index = 0; viewStatistics.size() > index; index++) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            if (viewStatistics.size() == 1) {
                gradientDrawable.setCornerRadii(new float[]{radiusCornerBar, radiusCornerBar, radiusCornerBar, radiusCornerBar, radiusCornerBar, radiusCornerBar, radiusCornerBar, radiusCornerBar});
                viewStatistics.get(0).setBackground(gradientDrawable);
            } else if (viewStatistics.size() > 1) {
                if (index == 0) {
                    gradientDrawable.setCornerRadii(new float[]{radiusCornerBar, radiusCornerBar, 0.0f, 0.0f, 0.0f, 0.0f, radiusCornerBar, radiusCornerBar});
                } else if (index == viewStatistics.size() - 1) {
                    gradientDrawable.setCornerRadii(new float[]{0.0f, 0.0f, radiusCornerBar, radiusCornerBar, radiusCornerBar, radiusCornerBar, 0.0f, 0.0f});
                }
            }
            gradientDrawable.setColor(CategoryColorManager.getColor(getContext(), index));
            viewStatistics.get(index).setBackground(gradientDrawable);
        }
    }
}