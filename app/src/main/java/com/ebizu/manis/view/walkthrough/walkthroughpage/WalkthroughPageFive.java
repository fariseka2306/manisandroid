package com.ebizu.manis.view.walkthrough.walkthroughpage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.manis.root.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FARIS_mac on 8/31/17.
 */

public class WalkthroughPageFive extends WalkthroughPageView implements View.OnClickListener {

    @BindView(R.id.drawer)
    DrawerLayout drawerNotification;
    @BindView(R.id.walkthrough_eighth_page)
    RelativeLayout walthroughtEightPage;
    @BindView(R.id.imageview_corner_notification)
    ImageView imageViewNotification;

    public WalkthroughPageFive(Context context) {
        super(context);
    }

    public WalkthroughPageFive(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WalkthroughPageFive(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WalkthroughPageFive(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.activity_walkthrough_page_five, null, false);
        addView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        getBaseActivity();
//        walkthroughPageSix = (WalkthroughPageSix) getBaseActivity().getSupportFragmentManager().findFragmentById(R.id.right_slider);
        imageViewNotification.setOnClickListener(this);
    }

    @Override
    public int page() {
        return 5;
    }

    @Override
    public void startPage() {
        super.startPage();
        startAnimeSlideRight(this);
    }

    @Override
    public void finishPage() {
        super.finishPage();
        startAnimFadeOut(this);
    }

    @Override
    public void setOnNextPageListener(OnNextPageListener onNextPageListener) {
        this.onNextPageListener = onNextPageListener;
        super.setOnNextPageListener(onNextPageListener);
    }

    public void closeDrawer() {
        if (drawerNotification != null) {
            if (drawerNotification.isDrawerOpen(Gravity.RIGHT)) {
                drawerNotification.closeDrawer(Gravity.RIGHT);
                finishPage();
            } else {
                drawerNotification.openDrawer(Gravity.RIGHT);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(imageViewNotification)) {
            if (drawerNotification != null && !drawerNotification.isDrawerOpen(Gravity.RIGHT)) {
                drawerNotification.openDrawer(Gravity.RIGHT);
//                walthroughtEightPage.setVisibility(INVISIBLE);
//                walkthroughPageSix.showNinethPage();
            }
        }
    }
}
