package com.ebizu.manis.view.holder;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.model.Faq;
import com.ebizu.manis.view.adapter.FaqExpandAdapter;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 8/3/2017.
 */

public class FaqViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.expandable_layout)
    ExpandableLayout expandableLayout;
    @BindView(R.id.text_view_title)
    TextView textViewTitle;
    @BindView(R.id.text_view_title_count)
    TextView textViewTitleCount;
    @BindView(R.id.faq_item)
    RelativeLayout faqItem;
    @BindView(R.id.recycler_view_expand)
    RecyclerView recyclerViewExpand;

    FaqExpandAdapter faqExpandAdapter;

    public FaqViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        expandableLayout.setInterpolator(new OvershootInterpolator());
        faqExpandAdapter = new FaqExpandAdapter(itemView.getContext(), new ArrayList<>());
        recyclerViewExpand.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        recyclerViewExpand.setAdapter(faqExpandAdapter);
    }

    public void setFaqItemView(Faq faq) {
        textViewTitle.setText(faq.getTitle());
        textViewTitleCount.setText(String.valueOf(faq.getChildren().size()));
        textViewTitle.setSelected(false);
        expandableLayout.collapse(false);
        faqExpandAdapter.addFaqChildList(faq.getChildren());
    }

    public void setOnClickListener(FaqViewHolder.OnClickListener onClickListener) {
        faqItem.setOnClickListener((v) -> {
            if (!expandableLayout.isExpanded()) {
                textViewTitle.setSelected(true);
                expandableLayout.expand();
            } else if (expandableLayout.isExpanded()) {
                textViewTitle.setSelected(false);
                expandableLayout.collapse();
            }
        });
        onClickListener.clickToExpand();
    }
    public interface OnClickListener {
        void clickToExpand();
    }
}
