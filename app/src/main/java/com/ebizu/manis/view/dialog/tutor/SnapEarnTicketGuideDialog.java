package com.ebizu.manis.view.dialog.tutor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ebizu on 10/16/17.
 */

public class SnapEarnTicketGuideDialog extends BaseDialogManis {

    public SnapEarnTicketGuideDialog(@NonNull Context context) {
        super(context);
        addDialogView(context);
    }

    public SnapEarnTicketGuideDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        addDialogView(context);
    }

    protected SnapEarnTicketGuideDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        addDialogView(context);
    }

    @Override
    protected void addDialogView(Context context) {
        super.addDialogView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_snap_lucky_draw_tutor, null, false);
        ButterKnife.bind(this, view);
        getParent().addView(view, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
    }

    @OnClick(R.id.ds_btn_ok)
    void onClickOk() {
        dismiss();
        new AnalyticManager(getContext()).trackEvent(
                ConfigManager.Analytic.Category.DIALOG_OK,
                ConfigManager.Analytic.Action.CLICK,
                "Button OK"
        );
    }

    @OnClick(R.id.ds_btn_igot)
    void onClickGot() {
        getDeviceSession().completeShowSnapEarnTicketGuide();
        dismiss();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        new AnalyticManager(getContext()).trackEvent(
                ConfigManager.Analytic.Category.DIALOG_OK,
                ConfigManager.Analytic.Action.CLICK,
                "Button Close"
        );
    }
}