package com.ebizu.manis.view.manis.textinput;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;

/**
 * Created by ebizu on 10/3/17.
 */

public class ShopCartNumberTextInput extends ShopCartTextInput {

    private String textTitle;
    private int textColor;

    public ShopCartNumberTextInput(Context context) {
        super(context);
        createView(context);
    }

    public ShopCartNumberTextInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public ShopCartNumberTextInput(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @Override
    protected void createView(Context context, AttributeSet attrs) {
        super.createView(context, attrs);
    }

    @Override
    public void setInput(String text) {
        super.setInput(text);
        inputText.setTextColor(textColor);
        textViewTitleInput.setText(textTitle);
        addDivider();
    }

    @Override
    public void setTitle(String title) {
        textTitle = title;
    }

    @Override
    public void setInputTextColor(int color) {
        textColor = color;
    }
}
