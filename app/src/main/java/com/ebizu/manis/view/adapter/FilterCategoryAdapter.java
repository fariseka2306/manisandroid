package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.view.holder.FilterContentViewHolder;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 8/2/17.
 */

public class FilterCategoryAdapter<T> extends RecyclerView.Adapter<FilterContentViewHolder> {

    private Context context;
    private List<T> data;
    private HashMap<String, Integer> mMapIndex;
    private AdapterFilterListener adapterFilterListener;

    public FilterCategoryAdapter(Context context, List<T> data){
        this.data = data;
        this.context = context;
        mMapIndex = new HashMap<>();
    }

    public void setAdapterFilterListener(AdapterFilterListener<T> adapterFilterListener) {
        this.adapterFilterListener = adapterFilterListener;
    }

    @Override
    public FilterContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_filter_content, parent, false);
        return new FilterContentViewHolder(view,context);
    }

    @Override
    public void onBindViewHolder(FilterContentViewHolder holder, int position) {
        adapterFilterListener.onCreateListItem(data.get(position),holder,position);
        holder.relViewCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.viewCheck.isShown()) {
                    holder.viewCheck.setVisibility(View.VISIBLE);
                    adapterFilterListener.onSelectItem(data.get(holder.getAdapterPosition()), false);
                } else if (holder.viewCheck.isShown()){
                    holder.viewCheck.setVisibility(View.GONE);
                    adapterFilterListener.onSelectItem(data.get(holder.getAdapterPosition()), true);
                }

            }
        });

        Glide.with(context)
           .load(R.drawable.ic_manis_login)
           .asBitmap()
           .thumbnail(0.1f)
           .animate(R.anim.fade_in_image)
           .placeholder(R.drawable.ic_manis_login)
           .centerCrop()
           .into(holder.imgRewardCategories);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public void setmMapIndex(HashMap<String, Integer> mMapIndex) {
        this.mMapIndex = mMapIndex;
    }
}
