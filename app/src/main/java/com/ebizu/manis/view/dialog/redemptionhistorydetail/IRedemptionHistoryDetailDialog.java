package com.ebizu.manis.view.dialog.redemptionhistorydetail;

import com.ebizu.manis.model.RedemptionHistoryResult;
import com.ebizu.manis.view.dialog.IBaseDialog;

/**
 * Created by FARIS_mac on 9/8/17.
 */

public interface IRedemptionHistoryDetailDialog extends IBaseDialog {

    void setRedemptionHistoryDetail(RedemptionHistoryResult redemptionHistoryResult);

}
