package com.ebizu.manis.view.walkthrough.walkthroughpage;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mac on 8/29/17.
 */

public class WalkthroughPageTwo extends WalkthroughPageView {

    @BindView(R.id.parent)
    RelativeLayout relativeLayoutParent;

    @BindView(R.id.walkthrough_third_page)
    RelativeLayout walkthroughThirdPage;
    @BindView(R.id.highlight_up)
    RelativeLayout relativeLayoutHighLightUp;
    @BindView(R.id.highlight_down)
    RelativeLayout relativeLayoutHighLighDown;
    @BindView(R.id.selected)
    RelativeLayout relativeLayoutSelected;

    @BindView(R.id.walkthrough_four_page)
    RelativeLayout walkthroughFourthPage;
    @BindView(R.id.imageview_button_gotit)
    ImageView imageViewButtonGotIt;

    public WalkthroughPageTwo(Context context) {
        super(context);
        createView(context);
    }

    public WalkthroughPageTwo(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public WalkthroughPageTwo(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public WalkthroughPageTwo(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    @Override
    protected void createView(Context context) {
        super.createView(context);
        View view = LayoutInflater.from(context).inflate(R.layout.activity_walkthrough_page_two, null, false);
        addView(view, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
    }

    @Override
    public int page() {
        return 2;
    }

    @Override
    public void startPage() {
        super.startPage();
        startAnimeSlideRight(this);
        relativeLayoutParent.setVisibility(VISIBLE);
        walkthroughThirdPage.setVisibility(VISIBLE);
        new Handler().postDelayed(() -> {
            walkthroughFourthPage.setVisibility(VISIBLE);
        }, 1000);
    }

    @Override
    public void finishPage() {
        super.finishPage();
        startAnimSlideOutLeft(this);
        onNextPageListener.onNextPage(page());
    }

    @Override
    public void setOnNextPageListener(OnNextPageListener onNextPageListener) {
        super.setOnNextPageListener(onNextPageListener);
        this.onNextPageListener = onNextPageListener;
        imageViewButtonGotIt.setOnClickListener(v -> {
            walkthroughFourthPage.setVisibility(GONE);
            new Handler().postDelayed(() ->{
                relativeLayoutHighLightUp.setVisibility(VISIBLE);
                relativeLayoutHighLighDown.setVisibility(VISIBLE);
            }, 1000);
        });
        relativeLayoutSelected.setOnClickListener(v -> {
            relativeLayoutParent.setVisibility(GONE);
            finishPage();
        });
    }
}
