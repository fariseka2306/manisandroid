package com.ebizu.manis.view.dialog.redeemdialogvoucher;

import com.ebizu.manis.view.dialog.IBaseDialog;

/**
 * Created by FARIS_mac on 10/3/17.
 */

public interface IRedeemRewardDialog extends IBaseDialog {

    void setViewRedeem(String refcode);

}
