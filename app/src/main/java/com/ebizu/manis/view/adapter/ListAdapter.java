package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.mvp.store.storenearby.PaginationScrollListener;
import com.ebizu.manis.view.holder.BaseHolder;
import com.ebizu.manis.view.holder.ProgressBarHolder;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ListAdapter<T, E extends BaseHolder> extends RecyclerView.Adapter<E> {

    private static final String TAG = "GAdapter";
    private Map<Integer, Constructor<?>> cached = new HashMap<>();
    private int layoutId = 0;
    protected int viewType;

    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_ITEM = 1;

    protected Context context;
    private List<T> adapterList = new ArrayList<>();

    //*** Variable validation ****
    private boolean isPagination = false;
    private boolean isLoading = false;
    private int page = 1;

    //*** Adapter interface listener ****
    private OnClickListener onClickListener;
    private OnScrollListener onScrollListener;


    public ListAdapter(int layoutId, Context context) {
        this.layoutId = layoutId;
        this.context = context;
        this.adapterList = new ArrayList<>();
    }

    public ListAdapter(int layoutId, Context context, List<T> adapterList) {
        this.layoutId = layoutId;
        this.context = context;
        this.adapterList = adapterList;
    }

    public ListAdapter(int layoutId, Context context, RecyclerView recyclerView) {
        this.layoutId = layoutId;
        this.context = context;
        this.adapterList = new ArrayList<>();
        initializeLayoutManager(recyclerView);
    }

    public ListAdapter(int layoutId, Context context, List<T> adapterList, RecyclerView recyclerView) {
        this.layoutId = layoutId;
        this.context = context;
        this.adapterList = adapterList;
        initializeLayoutManager(recyclerView);
    }

    private void initializeLayoutManager(RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setAdapter(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (null != onScrollListener) {
                    onScrollListener.onLoadMore();
                }
            }

            @Override
            public int getTotalPageCount() {
                return adapterList.size();
            }

            @Override
            public boolean isLastPage() {
                return !isPagination;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    @Override
    public E onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = new View(context);
        this.viewType = viewType;
        if (isPagination) {
            switch (viewType) {
                case VIEW_TYPE_LOADING: {
                    convertView = LayoutInflater.from(context).inflate(R.layout.item_progress, parent, false);
                    return (E) new ProgressBarHolder(convertView);
                }
                case VIEW_TYPE_ITEM: {
                    convertView = LayoutInflater.from(context).inflate(this.layoutId, parent, false);
                    break;
                }
            }
        } else {
            convertView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        }
        Type type = ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
        return holderGenerator(type, convertView);
    }

    private E holderGenerator(Type type, View convertView) {
        E viewHolder = null;
        try {
            if (null == cached.get(type.hashCode())) {
                viewHolder = (E) ((Class<?>) type).getConstructor(View.class).newInstance(convertView);
                cached.put(type.hashCode(), ((Class<?>) type).getConstructor(View.class));
            } else {
                viewHolder = (E) cached.get(type.hashCode()).newInstance(convertView);
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            Log.e(TAG, "Class : ".concat(e.getMessage()));
        } finally {
            Log.d(TAG, "Class : " + String.valueOf(viewHolder.getClass().getName()));
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        if (getItemViewType(position) != VIEW_TYPE_LOADING)
            setViewHolder(holder, position);
    }

    private void setViewHolder(BaseHolder holder, int position) {
        try {
            holder.setHolderView(adapterList.get(position));
            if (null != onClickListener) {
                holder.itemView.setOnClickListener(view -> onClickListener.onClick(adapterList.get(position)));
            }
        } catch (IndexOutOfBoundsException e) {
            Log.e(TAG, "onBindViewHolder : ".concat(e.getMessage()));
        }
    }

    @Override
    public int getItemCount() {
        return adapterList.size();
    }

    @Override
    public int getItemViewType(int position) {
        int typeItem = VIEW_TYPE_ITEM;
        if (isPagination) {
            if (null == adapterList.get(position)) {
                typeItem = VIEW_TYPE_LOADING;
            } else {
                typeItem = VIEW_TYPE_ITEM;
            }
        }
        return typeItem;
    }

    public void replaceAdapterList(List<T> adapterList) {
        this.adapterList = adapterList;
        notifyDataSetChanged();
    }

    public void addAdapterList(List<T> adapterList) {
        this.adapterList = adapterList;
        notifyDataSetChanged();
    }

    public void insertAdapterList(List<T> adapterList) {
        this.adapterList.addAll(adapterList);
        notifyItemInserted(this.adapterList.size() + 1);
    }

    public void showListProgressBar() {
        if (isPagination) {
            adapterList.add(null);
            notifyItemInserted(adapterList.size());
        }
    }

    public void dismissListProgressBar() {
        if (null == adapterList.get(adapterList.size() - 1)) {
            adapterList.remove(adapterList.size() - 1);
            notifyItemRemoved(adapterList.size());
        }
    }

    public void deleteAll() {
        adapterList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        if (null != adapterList)
            return adapterList.isEmpty();
        return true;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * OnClickListener function
     * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * * * *
     */

    public interface OnClickListener<T> {
        void onClick(T object);
    }

    /**
     * Listen function onClick when the interface called it.
     *
     * @param onClickListener
     */
    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    /**
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * OnScrollListener function
     * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * * * *
     */
    public interface OnScrollListener {
        void onLoadMore();
    }

    /**
     * Blocking load more function setPagination(false);
     *
     * @param pagination
     */
    public void setPagination(boolean pagination) {
        isPagination = pagination;
    }

    public void setPaginationAble(boolean isPagination) {
        this.isPagination = isPagination;
    }


    /**
     * Blocking load more function setLoading(false);
     *
     * @param loading
     */
    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    /**
     * Listen function onLoadMore when the interface called it.
     *
     * @param onScrollListener
     */
    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void resetPage() {
        this.page = 1;
    }

    public void setMinPage() {
        this.page = page - 1;
    }

    public int getPage() {
        return page;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public boolean isLastPage() {
        return !isPagination;
    }
}