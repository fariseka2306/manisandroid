package com.ebizu.manis.view.dialog.missionsdialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.tracker.TrackerConstant;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.missions.Missions;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.manager.tracker.TrackerConstant;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.missionspost.Incentive;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperMissionExecute;
import com.ebizu.manis.view.dialog.BaseDialogManis;

public class MissionSuccessDialog extends BaseDialogManis {

    private TextView textEarnPoints, textDescription;
    private Context context;

    private WrapperMissionExecute wrapperMissionExecute;
    private Missions missions;

    public void setMissions(Missions missions) {
        this.missions = missions;
    }

    public void setWrapperMissionPost(WrapperMissionExecute wrapperMissionExecute) {
        initView(wrapperMissionExecute);
    }

    public MissionSuccessDialog(@NonNull Context context) {
        super(context);
        this.context = context;
        customDialog();
    }

    public MissionSuccessDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        this.context = context;
        customDialog();
    }

    protected MissionSuccessDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
        customDialog();
    }

    private void customDialog() {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_mission_success, null, false);
        getParent().addView(view);
        textEarnPoints = (TextView) findViewById(R.id.text_view_earned_points);
        textDescription = (TextView) findViewById(R.id.text_view_description);
        Button buttonAwesome = (Button) findViewById(R.id.btn_awesome);
        Button buttonMorePoints = (Button) findViewById(R.id.btn_more_points);

        buttonAwesome.setOnClickListener(v -> backToList());
        buttonMorePoints.setOnClickListener(v -> {
            getBaseActivity().setResult(ConfigManager.Missions.MISSION_DETAIL_CODE);
            backToList();
            Incentive incentive = wrapperMissionExecute.getMissionsPost().getIncentive();
            String identifierName = wrapperMissionExecute.getMissionsPost().getDescription().concat(String.valueOf(incentive.getAmount())).concat(incentive.getForm());
            TrackerManager.getInstance().setTrackerData(
                    new TrackerStandartRequest(TrackerConstant.kTrackerOriginPageMissions, TrackerConstant.kTrackerPageMissionSuccessPopup, "", "", "", identifierName)
            );
        });
    }

    private void initView(WrapperMissionExecute wrapperMissionExecute) {
        textDescription.setText(wrapperMissionExecute.getMissionsPost().getDescription());
        textEarnPoints.setText(context.getResources().getString(R.string.you_earned) +
                wrapperMissionExecute.getMissionsPost().getIncentive().getAmount() + " " +
                wrapperMissionExecute.getMissionsPost().getIncentive().getForm());
        this.wrapperMissionExecute = wrapperMissionExecute;
    }

    private void backToList() {
        TrackerManager.getInstance().setTrackerData(new TrackerStandartRequest(TrackerConstant.kTrackerOriginPageMissions, TrackerConstant.kTrackerPageMissionDetail, TrackerConstant.kTrackerComponentButton, "", wrapperMissionExecute.getMissionsPost().getIncentive().getId(), missions.getMissionName()));
        dismiss();
        getBaseActivity().finish();
    }
}
