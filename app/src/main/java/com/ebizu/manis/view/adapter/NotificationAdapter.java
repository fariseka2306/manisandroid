package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.database.litepal.NotificationDatabase;
import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.mvp.viewhistory.SnapViewHistoryActivity;
import com.ebizu.manis.view.holder.NotificationSwipeViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu on 8/31/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationSwipeViewHolder> {

    private List<NotificationTableList> notificationTableList;
    private NotificationSwipeViewHolder notificationSwipeViewHolder;
    private EventListener eventListener;

    public NotificationAdapter() {
        notificationTableList = new ArrayList<>();
    }

    @Override
    public NotificationSwipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        notificationSwipeViewHolder = new NotificationSwipeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fragment_notification, parent, false));
        return notificationSwipeViewHolder;
    }

    @Override
    public void onBindViewHolder(NotificationSwipeViewHolder holder, int position) {
        NotificationTableList notification = notificationTableList.get(position);
        holder.setNotificationView(notification);
        int pos = holder.getAdapterPosition();
        holder.setOnClickListener(new NotificationSwipeViewHolder.OnClickListener() {
            @Override
            public void removeItem() {
                removeNotificationItem(notification, pos);
                if (notificationTableList.isEmpty())
                    eventListener.onEmptyNotificationList();
            }

            @Override
            public void viewItem() {
                if (notification.getType().equals(NotificationDatabase.TYPE_SNAP)) {
                    Context context = notificationSwipeViewHolder.context;
                    Intent intent = new Intent(context, SnapViewHistoryActivity.class);
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationTableList.size();
    }

    public void addNotificationList(List<NotificationTableList> notificationTableList) {
        this.notificationTableList.addAll(notificationTableList);
        notifyItemRangeInserted(this.notificationTableList.size() + 1,
                notificationTableList.size());
    }

    public void replaceNotificationList(List<NotificationTableList> notificationTableList) {
        this.notificationTableList = notificationTableList;
        notifyDataSetChanged();
    }

    public void removeNotificationItem(NotificationTableList notification, int position) {
        this.notificationTableList.remove(position);
        notification.delete();
        notifyDataSetChanged();
    }

    public void deleteAll() {
        this.notificationTableList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }

    public interface EventListener {
        void onEmptyNotificationList();
    }
}
