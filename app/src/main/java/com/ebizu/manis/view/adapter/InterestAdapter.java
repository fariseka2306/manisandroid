package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.R;
import com.ebizu.manis.model.Interest;
import com.ebizu.manis.mvp.interest.InterestView;
import com.ebizu.manis.view.holder.InterestViewHolder;
import com.ebizu.manis.view.manis.interest.model.InterestValidation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public class InterestAdapter extends RecyclerView.Adapter<InterestViewHolder> {

    private Context context;
    private InterestView interestView;
    private ArrayList<Interest> interests;

    public InterestAdapter(Context context, InterestView interestView) {
        this.context = context;
        this.interestView = interestView;
        interests = new ArrayList<>();
    }

    @Override
    public InterestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.interest_item_view, parent, false);
        return new InterestViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(InterestViewHolder holder, int position) {
        Interest interest = interests.get(position);
        holder.setInterestItemView(interest);
        holder.setOnClickListener(() -> {
            if (InterestValidation.isValidPinned(interests)) {
                interestView.enableSaveButton();
            } else {
                interestView.unableSaveButton(InterestValidation.getSizePinned());
            }
        });
    }

    @Override
    public int getItemCount() {
        return interests.size();
    }

    public void setInterests(ArrayList<Interest> interests) {
        this.interests = interests;
        Comparator<Interest> comparator = (o1, o2) -> o1.getId() - o2.getId();
        Collections.sort(this.interests, comparator);
        notifyDataSetChanged();
    }

    public ArrayList<Interest> getInterests() {
        return interests;
    }
}