package com.ebizu.manis.view.dialog.review;

import com.ebizu.manis.mvp.main.MainActivity;

/**
 * Created by mac on 8/23/17.
 */

public interface IReviewStatus {

    void getReviewStatus(MainActivity mainActivity);

}
