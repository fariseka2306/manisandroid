package com.ebizu.manis.view.manis.location;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;

/**
 * Created by Ebizu-User on 08/08/2017.
 */

public class GpsLocationInactiveView extends RelativeLayout {
    public GpsLocationInactiveView(Context context) {
        super(context);
        createView(context);
    }

    public GpsLocationInactiveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    public GpsLocationInactiveView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        createView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public GpsLocationInactiveView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        createView(context);
    }

    private void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_inactive_location, null, false);
        addView(view);
    }
}
