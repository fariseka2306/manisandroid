package com.ebizu.manis.view.dialog.redeemdialogwithpin;

import android.content.Context;
import android.widget.Toast;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.rx.ResponseRewardSubscriber;
import com.ebizu.manis.service.reward.RewardApi;
import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.RewardApiGenerator;
import com.ebizu.manis.service.reward.requestbody.PinValidationBody;
import com.ebizu.manis.service.reward.response.ResponseRewardApi;
import com.ebizu.manis.service.reward.response.WrapperPinValidation;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by FARIS_mac on 10/4/17.
 */

public class RedeemWithPinPresenter extends BaseDialogPresenter implements IRedeemWithPinPresenter {

    private Context context;
    private RedeemWithPinDialog redeemWithPinDialog;
    private RewardApi rewardApi;

    @Override
    public void attachDialog(BaseDialogManis baseDialogManis) {
        super.attachDialog(baseDialogManis);
        redeemWithPinDialog = (RedeemWithPinDialog) baseDialogManis;
    }

    public RedeemWithPinPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void pinValidationPost(PinValidationBody pinValidationBody) {
        if (rewardApi == null)
            rewardApi = RewardApiGenerator.createService(context);
        rewardApi.getPinValidation(pinValidationBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseRewardSubscriber<WrapperPinValidation>(redeemWithPinDialog) {
                    @Override
                    public void onNext(WrapperPinValidation wrapperPinValidation) {
                        super.onNext(wrapperPinValidation);
                        if (wrapperPinValidation.getErrorCode() != RewardApiConfig.ResponseCode.SUCCESS) {
                            redeemWithPinDialog.showFailureMessage(wrapperPinValidation.getErrorMessage());
                        } else {
                            redeemWithPinDialog.showDialogPinSuccess(wrapperPinValidation.getRedeemPinValidation());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        if (!ConnectionDetector.isNetworkConnected(redeemWithPinDialog.getContext())) {
                            Toast.makeText(context, context.getString(R.string.error_no_connection), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getString(R.string.server_busy), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
