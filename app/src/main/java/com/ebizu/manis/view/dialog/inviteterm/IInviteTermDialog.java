package com.ebizu.manis.view.dialog.inviteterm;

import com.ebizu.manis.model.InviteTerm;
import com.ebizu.manis.view.dialog.IBaseDialog;

/**
 * Created by abizu-alvio on 8/14/2017.
 */

public interface IInviteTermDialog extends IBaseDialog {

    void setViewInvite(InviteTerm viewInvite);

}
