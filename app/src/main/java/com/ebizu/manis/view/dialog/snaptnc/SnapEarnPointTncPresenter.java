package com.ebizu.manis.view.dialog.snaptnc;

import android.util.Log;

import com.ebizu.manis.model.snap.SnapTerm;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.makeramen.roundedimageview.RoundedImageView.TAG;

/**
 * Created by ebizu on 10/12/17.
 */

public class SnapEarnPointTncPresenter extends BaseDialogPresenter {

    @Inject
    public SnapEarnPointTncPresenter() {
    }

    public void getTermsAndSave() {
        getManisApi().getTermsSnapEarnPoint()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(wrapperSnapTerms -> {
                    SnapTerm snapTerm = wrapperSnapTerms.getSnapTerm();
                    getManisSession().setTermsSnapEarnPoints(snapTerm);
                }, throwable -> {
                    Log.e(TAG, "getTermsAndSave: ".concat(throwable.getMessage()));
                });
    }

}
