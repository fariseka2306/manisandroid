package com.ebizu.manis.view.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ImageUtils;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.rewardbulk.RewardBulk;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public class RewardBulkAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;

    private Context context;
    private List<RewardVoucher> rewardBulkList;
    private RewardBulkListener rewardBulkListener;

    public RewardBulkAdapter(Context context, List<RewardVoucher> rewardBulkList) {
        this.context = context;
        this.rewardBulkList = rewardBulkList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, layoutInflater);
                break;
            case LOADING:
                View view = layoutInflater.inflate(R.layout.pagination_progress, parent, false);
                viewHolder = new loadingViewHolder(view);
                break;
        }
        return viewHolder;
    }

    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater layoutInflater) {
        final RecyclerView.ViewHolder rewardGridViewHolder;
        View view = layoutInflater.inflate(R.layout.item_new_reward_category, parent, false);
        rewardGridViewHolder = new ViewHolder(view);
        return rewardGridViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RewardVoucher rewardVoucher = rewardBulkList.get(position);
        switch (getItemViewType(position)) {
            case ITEM:
                RewardBulkAdapter.ViewHolder rewardCategoryViewHolder = (RewardBulkAdapter.ViewHolder) holder;
                rewardCategoryViewHolder.tvRewardName.setText(rewardVoucher.getName());
                ImageUtils.loadImage(context, rewardVoucher.getImage128(), ContextCompat.getDrawable(context, R.drawable.default_pic_promo_details_pic_small), rewardCategoryViewHolder.ivReward);
                rewardCategoryViewHolder.tvOutOfStock.setVisibility(View.GONE);
                rewardCategoryViewHolder.tvProviderName.setText(rewardVoucher.getProvider().getName());
                rewardCategoryViewHolder.tvRewardValue.setText(rewardVoucher.getCurrency() + " " + rewardVoucher.getVoucherPurchaseValue());
                rewardCategoryViewHolder.tvPoint.setText(rewardVoucher.getPoint() + " " + context.getString(R.string.rd_txt_pts));


                // free voucher
                if (rewardVoucher.getPoint() == 0 &&
                        rewardVoucher.getVoucherTransactionType().equalsIgnoreCase(RewardVoucher.VOUCHER_TRANSACTION_REDEEMABLE) &&
                        rewardVoucher.getVoucherPurchaseValue() == 0) {
                    rewardCategoryViewHolder.tvPoint.setText(context.getString(R.string.rd_txt_free));
                    rewardCategoryViewHolder.frpiImgManis.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.tvPoint.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.frpiImgBucket.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.tvRewardValue.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.rlImgPoint.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.rlBtnFree.setVisibility(View.VISIBLE);
                }

                // redeemable only
                else if (rewardVoucher.getPoint() >= 0 && rewardVoucher.getVoucherPurchaseValue() == 0 &&
                        rewardVoucher.getVoucherTransactionType().equalsIgnoreCase(RewardVoucher.VOUCHER_TRANSACTION_REDEEMABLE)) {
                    rewardCategoryViewHolder.frpiImgManis.setVisibility(View.VISIBLE);
                    rewardCategoryViewHolder.tvPoint.setVisibility(View.VISIBLE);
                    rewardCategoryViewHolder.frpiImgBucket.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.tvRewardValue.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.rlBtnFree.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.rlImgPoint.setVisibility(View.VISIBLE);
                }

                // purchaseable only
                else if (rewardVoucher.getPoint() == 0 &&
                        rewardVoucher.getVoucherTransactionType().equalsIgnoreCase(RewardVoucher.VOUCHER_TRANSACTION_PURCHASABLE) &&
                        rewardVoucher.getVoucherPurchaseValue() > 0.0) {
                    rewardCategoryViewHolder.frpiImgManis.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.tvPoint.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.frpiImgBucket.setVisibility(View.VISIBLE);
                    rewardCategoryViewHolder.tvRewardValue.setVisibility(View.VISIBLE);
                    rewardCategoryViewHolder.rlBtnFree.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.rlImgPoint.setVisibility(View.VISIBLE);
                }

                //both
                else if (rewardVoucher.getPoint() > 0 &&
                        rewardVoucher.getVoucherPurchaseValue() > 0) {
                    rewardCategoryViewHolder.frpiImgManis.setVisibility(View.VISIBLE);
                    rewardCategoryViewHolder.tvPoint.setVisibility(View.VISIBLE);
                    rewardCategoryViewHolder.frpiImgBucket.setVisibility(View.VISIBLE);
                    rewardCategoryViewHolder.tvRewardValue.setVisibility(View.VISIBLE);
                    rewardCategoryViewHolder.rlBtnFree.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.rlImgPoint.setVisibility(View.VISIBLE);
                } else {
                    rewardCategoryViewHolder.frpiImgManis.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.tvPoint.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.rlImgPoint.setVisibility(View.INVISIBLE);
                    rewardCategoryViewHolder.rlBtnFree.setVisibility(View.INVISIBLE);
                }

                if (rewardVoucher.getStock() < 1) {
                    rewardCategoryViewHolder.tvOutOfStock.setVisibility(View.VISIBLE);
                } else {
                    rewardCategoryViewHolder.rlImgPoint.setVisibility(View.VISIBLE);
                    rewardCategoryViewHolder.tvOutOfStock.setVisibility(View.GONE);
                }

                rewardCategoryViewHolder.itemView.setOnClickListener(view -> {
                    rewardBulkListener.onRewardBulkListener(rewardVoucher);
                    new AnalyticManager(context).trackEvent(
                            ConfigManager.Analytic.Category.FRAGMENT_REWARD_POINT,
                            ConfigManager.Analytic.Action.ITEM_CLICK,
                            "Item Click".concat(" Item ").concat(rewardVoucher.getName()));
                });
                break;
            case LOADING:
                break;
        }
    }

    public void clearRewards() {
        this.rewardBulkList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rewardBulkList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == rewardBulkList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    public void add(RewardVoucher r) {
        rewardBulkList.add(r);
        notifyItemInserted(rewardBulkList.size() - 1);
    }

    public void addAll(List<RewardVoucher> moveResults) {
        for (RewardVoucher result : moveResults) {
            add(result);
        }
    }

    public void replaceReward(List<RewardVoucher> rewards) {
        this.rewardBulkList.clear();
        this.rewardBulkList = rewards;
        notifyDataSetChanged();
    }

    public void remove(RewardVoucher r) {
        int position = rewardBulkList.indexOf(r);
        if (position > -1) {
            rewardBulkList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new RewardVoucher());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = rewardBulkList.size() - 1;
        RewardVoucher result = getItem(position);

        if (result != null) {
            rewardBulkList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public RewardVoucher getItem(int position) {
        return rewardBulkList.get(position);
    }


    public void setOnRewardBulkListener(RewardBulkListener rewardBulkListener) {
        this.rewardBulkListener = rewardBulkListener;
    }

    public interface RewardBulkListener {
        void onRewardBulkListener(RewardVoucher reward);
    }

    private class loadingViewHolder extends RecyclerView.ViewHolder {
        public loadingViewHolder(View view) {
            super(view);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.frpi_img_redeem)
        ImageView ivReward;
        @BindView(R.id.frpi_img_bucket)
        public ImageView frpiImgBucket;
        @BindView(R.id.frpi_img_manis)
        ImageView frpiImgManis;
        @BindView(R.id.frpi_txt_title)
        TextView tvRewardName;
        @BindView(R.id.frpi_txt_category)
        TextView tvProviderName;
        @BindView(R.id.frpi_txt_points)
        TextView tvPoint;
        @BindView(R.id.frpi_txt_value)
        TextView tvRewardValue;
        @BindView(R.id.frpi_txt_outofstock)
        TextView tvOutOfStock;
        @BindView(R.id.rl_img_point)
        LinearLayout rlImgPoint;
        @BindView(R.id.rl_btn_free)
        RelativeLayout rlBtnFree;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
