package com.ebizu.manis.view.manis.interest.model;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.ebizu.manis.model.Interest;
import com.ebizu.manis.service.manis.requestbody.interest.PinnedBody;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ebizu-User on 20/07/2017.
 */

public class InterestValidation {

    private static InterestModelView[] interestModelViews = {
            new FoodInterest(),
            new FashionInterest(),
            new EntertainmentInterest(),
            new TechGadgetInterest(),
            new EventsInterest(),
            new HomeLivingInterest(),
            new HealthBeutyInterest(),
            new TravelInterest(),
            new ShoppingInterest(),
            new SportsInterest(),
            new FilmMusicInterest(),
            new BusinessInterest()
    };

    private static int sizePinned;


    public static Drawable getInterestDrawable(Context context, int id) {
        for (InterestModelView interestModelView : interestModelViews) {
            if (interestModelView.id() == id) {
                return interestModelView.drawable(context);
            }
        }
        return new FoodInterest().drawable(context);
    }

    public static int getColor(Context context, int id) {
        for (InterestModelView interestModelView : interestModelViews) {
            if (interestModelView.id() == id) {
                return interestModelView.color(context);
            }
        }
        return new FoodInterest().color(context);
    }

    public static String getJsonIds(ArrayList<Interest> interests) {
        PinnedBody pinnedBody = new PinnedBody();
        pinnedBody.setIds(getIds(interests));
        return new Gson().toJson(pinnedBody);
    }

    private static List<Integer> getIds(ArrayList<Interest> interests) {
        List<Integer> ids = new ArrayList<>();
        for (Interest interest : interests) {
            if (interest.getPinned()) {
                ids.add(interest.getId());
            }
        }
        return ids;
    }

    public static boolean isValidPinned(ArrayList<Interest> interests) {
        sizePinned = 0;
        for (Interest interest : interests) {
            if (interest.getPinned()) {
                sizePinned++;
            }
        }
        return sizePinned > 2;
    }

    public static int getSizePinned() {
        return sizePinned;
    }
}