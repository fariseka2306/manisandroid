package com.ebizu.manis.view.dialog.snaptnc;

import android.util.Log;

import com.ebizu.manis.model.snap.SnapTermLuckyDraw;
import com.ebizu.manis.view.dialog.BaseDialogPresenter;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ebizu on 10/12/17.
 */

public class SnapEarnTicketTncPresenter extends BaseDialogPresenter {

    private final String TAG = getClass().getSimpleName();

    @Inject
    public SnapEarnTicketTncPresenter() {
    }

    public void getTermsAndSave() {
        getManisApi().getTermsSnapEarnTicket()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(wrapperSnapEarnTicketTerms -> {
                    SnapTermLuckyDraw snapTermLuckyDraw = wrapperSnapEarnTicketTerms.getSnapTermLuckyDraw();
                    getManisSession().setTermsSnapEarnTickets(snapTermLuckyDraw);
                }, throwable -> {
                    Log.e(TAG, "getTermsAndSave: ".concat(throwable.getMessage()));
                });
    }

}
