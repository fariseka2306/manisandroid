package com.ebizu.manis.view.dialog.review;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.rx.ResponseSubscriber;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.ReviewBody;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.view.dialog.review.reviewdialog.DialogReview;
import com.ebizu.manis.view.dialog.review.reviewdialog.IDialogReviewPresenter;
import com.google.gson.Gson;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class SendReviewPresenter implements IDialogReviewPresenter {

    private final String TAG = getClass().getSimpleName();

    private Context context;
    private ManisApi manisApi;
    private DialogReview baseDialogManis;

    @Override
    public void attachDialog(DialogReview baseDialogManis) {
        this.baseDialogManis = baseDialogManis;
    }

    public SendReviewPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void sendReview(ReviewBody reviewBody) {
        if (manisApi == null)
            manisApi = ManisApiGenerator.createServiceWithToken(context);
        manisApi.saveReview(new Gson().toJson(reviewBody))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResponseSubscriber<ResponseData>(baseDialogManis) {
                    @Override
                    public void onNext(ResponseData responseData) {
                        super.onNext(responseData);
                        Log.i(TAG, "sendPostReviewSuccessStatus: " + responseData.getSuccess().toString());
                        Log.i(TAG, "sendPostReviewBody: " + reviewBody.toString());
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);

                    }
                });
    }

}
