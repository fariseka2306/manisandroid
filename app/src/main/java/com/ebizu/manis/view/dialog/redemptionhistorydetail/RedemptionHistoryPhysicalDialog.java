package com.ebizu.manis.view.dialog.redemptionhistorydetail;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.RedemptionHistoryResult;
import com.ebizu.manis.sdk.ManisLocalData;
import com.ebizu.manis.sdk.entities.LoginData;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by FARIS_mac on 9/11/17.
 */

public class RedemptionHistoryPhysicalDialog extends BaseDialogManis implements IRedemptionHistoryDetailDialog {

    private ImageView imageViewMerchant;
    private ImageView imageViewStatus;
    private TextView textViewDate;
    private TextView textViewTitle;
    private TextView textViewMerchant;
    private TextView textViewStatus;
    private TextView textViewInstruction;
    private TextView textViewInstructionBottom;
    private Button buttonClose;

    RedemptionHistoryResult redemptionHistoryResult;

    public void setRedemptionHistoryResult(RedemptionHistoryResult redemptionHistoryResult) {
        this.redemptionHistoryResult = redemptionHistoryResult;
        setRedemptionHistoryDetail(redemptionHistoryResult);
    }

    public RedemptionHistoryPhysicalDialog(@NonNull Context context) {
        super(context);
        customDialog(context);
    }

    public RedemptionHistoryPhysicalDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        customDialog(context);
    }

    protected RedemptionHistoryPhysicalDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        customDialog(context);
    }

    private void customDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_redemption_history_physical, null, false);
        getParent().addView(view, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        imageViewMerchant = (ImageView) findViewById(R.id.imageview_merchant);
        imageViewStatus = (ImageView) findViewById(R.id.imageview_status);
        textViewTitle = (TextView) findViewById(R.id.textview_title);
        textViewDate = (TextView) findViewById(R.id.textview_date);
        textViewMerchant = (TextView) findViewById(R.id.textview_merchant);
        textViewStatus = (TextView) findViewById(R.id.textview_status);
        textViewInstruction = (TextView) findViewById(R.id.textview_instruction);
        textViewInstructionBottom = (TextView) findViewById(R.id.textview_instruction_bottom);
        buttonClose = (Button) findViewById(R.id.button_close);
        buttonClose.setOnClickListener((v -> dismiss()));
    }

    @Override
    public void setRedemptionHistoryDetail(RedemptionHistoryResult redemptionHistoryResult) {
        Glide.with(getContext())
                .load(redemptionHistoryResult.getAssets().getLogo())
                .thumbnail(0.1f)
                .fitCenter()
                .animate(R.anim.fade_in_image)
                .placeholder(R.drawable.default_pic_promo_details_pic_large)
                .into(imageViewMerchant);

        SimpleDateFormat dateFomat = new SimpleDateFormat("hh.mm a, dd MMM yyyy");
        Date date = new Date(redemptionHistoryResult.getRedeemed() * 1000L);
        textViewDate.setText(dateFomat.format(date));

        String delims = "[@]";
        String[] parse = redemptionHistoryResult.getStore().split(delims);

        textViewMerchant.setText(parse[0].trim());
        textViewTitle.setText(redemptionHistoryResult.getName());
        textViewInstruction.setText(redemptionHistoryResult.getCode());
        generateStatus(redemptionHistoryResult.getDeliveryStatus());
    }

    private void generateStatus(int status) {
        LoginData loginData = ManisLocalData.getLoginData();
        if (loginData != null) {
            String country = loginData.accCountry;
            if (country != null && country.equals(getContext().getString(R.string.localization_IDN_tag)))
                textViewInstructionBottom.setText("redemption.id@ebizu.com");
        }
        switch (status) {
            case 1:
                textViewStatus.setText(R.string.under_review);
                imageViewStatus.setImageResource(R.drawable.under_review_icon);
                textViewInstruction.setText(R.string.redeemption_status_under_review);
                textViewInstructionBottom.setVisibility(View.GONE);
                break;
            case 2:
                textViewStatus.setText(R.string.pending);
                imageViewStatus.setImageResource(R.drawable.on_hold_icon);
                textViewInstruction.setText(R.string.redeemption_status_pending);
                textViewInstructionBottom.setVisibility(View.VISIBLE);
                break;
            case 3:
                textViewStatus.setText(R.string.rejected);
                imageViewStatus.setImageResource(R.drawable.rejected_icon);
                textViewInstruction.setText(R.string.redeemption_status_reject);
                textViewInstructionBottom.setVisibility(View.VISIBLE);
                break;
            case 4:
                textViewStatus.setText(R.string.processing);
                imageViewStatus.setImageResource(R.drawable.processing_icon);
                textViewInstruction.setText(getContext().getString(R.string.redeemption_status_processing, redemptionHistoryResult.getNotes()));
                textViewInstructionBottom.setVisibility(View.GONE);
                break;
            case 6:
                textViewStatus.setText(R.string.undelivered);
                imageViewStatus.setImageResource(R.drawable.undelivered_icon);
                textViewInstruction.setText(R.string.redeemption_status_undelivered);
                textViewInstructionBottom.setVisibility(View.VISIBLE);
                break;
            default:
                textViewStatus.setText(R.string.shipped);
                imageViewStatus.setImageResource(R.drawable.shipped_icon);
                String instruction = getContext().getString(R.string.redeemption_status_shipped);
                instruction = instruction + redemptionHistoryResult.getShipmentNumber();
                textViewInstruction.setText(instruction);
                textViewInstructionBottom.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        new AnalyticManager(getContext()).trackEvent(
                ConfigManager.Analytic.Category.DIALOG_REDEMPTION_HISTORY_PHYSICAL,
                ConfigManager.Analytic.Action.CLICK,
                "Button Close"
        );
    }
}
