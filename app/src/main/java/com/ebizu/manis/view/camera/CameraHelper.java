package com.ebizu.manis.view.camera;

import android.app.Activity;
import android.content.Context;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.support.constraint.ConstraintLayout;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import java.util.List;

/**
 * Created by Ebizu-User on 12/08/2017.
 */

public class CameraHelper {

    private CameraHelper() {
    }

    public static Camera.Size determinePreviewSize(boolean portrait, int reqWidth, int reqHeight, List<Camera.Size> previewSizes) {
        // Meaning of width and height is switched for preview when portrait,
        // while it is the same as user's view for surface and metrics.
        // That is, width must always be larger than height for setPreviewSize.
        int reqPreviewWidth; // requested width in terms of camera hardware
        int reqPreviewHeight; // requested height in terms of camera hardware
        if (portrait) {
            reqPreviewWidth = reqHeight;
            reqPreviewHeight = reqWidth;
        } else {
            reqPreviewWidth = reqWidth;
            reqPreviewHeight = reqHeight;
        }

        // Adjust surface size with the closest aspect-ratio
        float reqRatio = ((float) reqPreviewWidth) / reqPreviewHeight;
        float curRatio;
        float deltaRatio;
        float deltaRatioMin = Float.MAX_VALUE;
        Camera.Size retSize = null;
        for (Camera.Size size : previewSizes) {
            curRatio = ((float) size.width) / size.height;
            deltaRatio = Math.abs(reqRatio - curRatio);
            if (deltaRatio < deltaRatioMin) {
                deltaRatioMin = deltaRatio;
                retSize = size;
            }
        }
        return retSize;
    }

    public static Camera.Size determinePictureSize(Camera.Size previewSize, List<Camera.Size> pictureSizes) {
        Camera.Size retSize = null;
        for (Camera.Size size : pictureSizes) {
            if (size.equals(previewSize)) {
                return size;
            }
        }

        // if the preview size is not supported as a picture size
        float reqRatio = ((float) previewSize.width) / previewSize.height;
        float curRatio;
        float deltaRatio;
        float deltaRatioMin = Float.MAX_VALUE;
        for (Camera.Size size : pictureSizes) {
            curRatio = ((float) size.width) / size.height;
            deltaRatio = Math.abs(reqRatio - curRatio);
            if (deltaRatio < deltaRatioMin) {
                deltaRatioMin = deltaRatio;
                retSize = size;
            }
        }

        return retSize;
    }

    public static boolean adjustSurfaceLayoutSize(Camera.Size previewSize, boolean portrait,
                                                  int availableWidth, int availableHeight,
                                                  SurfaceView surfaceView) {
        float tmpLayoutHeight;
        float tmpLayoutWidth;
        if (portrait) {
            tmpLayoutHeight = previewSize.width;
            tmpLayoutWidth = previewSize.height;
        } else {
            tmpLayoutHeight = previewSize.height;
            tmpLayoutWidth = previewSize.width;
        }

        float factH;
        float factW;
        float fact;
        factH = availableHeight / tmpLayoutHeight;
        factW = availableWidth / tmpLayoutWidth;
        if (factH < factW) {
            fact = factW;
        } else {
            fact = factH;
        }

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) surfaceView.getLayoutParams();

        int layoutHeight = (int) (tmpLayoutHeight * fact);
        int layoutWidth = (int) (tmpLayoutWidth * fact);

        boolean layoutChanged;
        if ((layoutWidth != surfaceView.getWidth()) || (layoutHeight != surfaceView.getHeight())) {
            layoutParams.height = layoutHeight;
            layoutParams.width = layoutWidth;
            surfaceView.setLayoutParams(layoutParams); // this will trigger another surfaceChanged invocation.
            layoutChanged = true;
        } else {
            layoutChanged = false;
        }
        return layoutChanged;
    }

    public static void configureCameraParameters(Context context, Camera camera, Camera.Parameters cameraParams) {
        // for 2.2 and later
        int angle;
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        switch (display.getRotation()) {
            case Surface.ROTATION_0: // This is display orientation
                angle = 90; // This is camera orientation
                break;
            case Surface.ROTATION_90:
                angle = 0;
                break;
            case Surface.ROTATION_180:
                angle = 270;
                break;
            case Surface.ROTATION_270:
                angle = 180;
                break;
            default:
                angle = 90;
                break;
        }
        camera.setDisplayOrientation(angle);
        cameraParams.setPreviewSize(cameraParams.getPreviewSize().width, cameraParams.getPreviewSize().height);
        cameraParams.set("jpeg-quality", 100);
        cameraParams.setPictureFormat(PixelFormat.JPEG);
        cameraParams.setPictureSize(cameraParams.getPictureSize().width, cameraParams.getPictureSize().height);
        List<String> focusModes = cameraParams.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
            cameraParams.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        camera.setParameters(cameraParams);
    }
}
