package com.ebizu.manis.view.adapter.account;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.manager.categorycolor.CategoryColorManager;
import com.ebizu.manis.model.Statistic;
import com.ebizu.manis.preference.ManisSession;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class AccountCategoryAdapter extends RecyclerView.Adapter<AccountCategoryAdapter.ViewHolder> {

    private final String TAG = getClass().getSimpleName();

    private Context mContext;
    private List<Statistic> modelStatisticList;
    private String currency;
    private ManisSession manisSession;

    public AccountCategoryAdapter(Context mContext, ArrayList<Statistic> modelStatisticList) {
        this.mContext = mContext;
        this.modelStatisticList = modelStatisticList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        manisSession = new ManisSession(mContext);
        Statistic statistic = this.modelStatisticList.get(position);
        String name = statistic.getName().toUpperCase();
        String categoryName = modelStatisticList.size() > 1 ? name.split(" ")[0] : name;
        viewHolder.textViewAccountCategory.setText(categoryName);
        viewHolder.textViewTotalCategory.setText(getMoney(Math.round(statistic.getAmount())));
        viewHolder.textViewAccountCategory.setTextColor(CategoryColorManager.getColor(mContext, position));
    }

    @Override
    public int getItemCount() {
        return modelStatisticList.size();
    }

    public void addAccountList(List<Statistic> statistic) {
        modelStatisticList.clear();
        modelStatisticList.addAll(statistic);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_account_category)
        TextView textViewAccountCategory;
        @BindView(R.id.text_view_amount_category)
        TextView textViewTotalCategory;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private String getMoney(Long amount) {
        if (manisSession.getAccountSession().getCurrency().getIso2() != null) {
            String iso2 = manisSession.getAccountSession().getCurrency().getIso2();
            return iso2 + " " + UtilManis.formatMoney(iso2, amount);
        } else {
            return "";
        }
    }

}