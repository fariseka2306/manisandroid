package com.ebizu.manis.view.manis.swiperefresh;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public class SwipeRefreshManis extends SwipeRefreshLayout {

    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.parent_group_swipe)
    LinearLayout parentGroupSwipe;

    public SwipeRefreshManis(Context context) {
        super(context);
        createView(context);
    }

    public SwipeRefreshManis(Context context, AttributeSet attrs) {
        super(context, attrs);
        createView(context);
    }

    private void createView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.swipe_refresh, null, false);
        addView(view, new NestedScrollView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        ButterKnife.bind(this, view);
        setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorPink));
    }

    public void addViewToSwipe(View view, ConstraintLayout.LayoutParams layoutParams) {
        parentGroupSwipe.addView(view, layoutParams);
    }

    public void addViewToSwipe(RecyclerView recyclerView, RecyclerView.LayoutParams layoutParams) {
        parentGroupSwipe.addView(recyclerView, layoutParams);
    }

    public void addViewToSwipe(View view) {
        parentGroupSwipe.addView(view);
    }

    public NestedScrollView getNestedScrollView() {
        return nestedScrollView;
    }

    public void dismissSwipe() {
        setRefreshing(false);
    }
}
