package com.ebizu.manis.model.rewardredeem;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/3/17.
 */

public class RewardRedeem {

    @SerializedName("refcode")
    @Expose
    private String refcode;
    @SerializedName("point")
    @Expose
    private Integer point;

    public String getRefcode() {
        return refcode;
    }

    public Integer getPoint() {
        return point;
    }
}
