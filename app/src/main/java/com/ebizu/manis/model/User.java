package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by Raden on 7/3/17.
 */

public class User {

    @SerializedName("acc_id")
    @Expose
    private Integer accId;
    @SerializedName("acc_facebook_id")
    @Expose
    private Integer accFacebookId;
    @SerializedName("acc_facebook_email")
    @Expose
    private String accFacebookEmail;
    @SerializedName("acc_google_id")
    @Expose
    private String accGoogleId;
    @SerializedName("acc_google_email")
    @Expose
    private Integer accGoogleEmail;
    @SerializedName("acc_screen_name")
    @Expose
    private String accScreenName;
    @SerializedName("acc_cty_id")
    @Expose
    private String accCtyId;
    @SerializedName("acc_created_datetime")
    @Expose
    private Integer accCreatedDatetime;
    @SerializedName("acc_photo")
    @Expose
    private String accPhoto;
    @SerializedName("acc_status")
    @Expose
    private Integer accStatus;
    @SerializedName("acc_birthdate")
    @Expose
    private Integer accBirthdate;
    @SerializedName("acc_address")
    @Expose
    private String accAddress;
    @SerializedName("acc_gender")
    @Expose
    private String accGender;
    @SerializedName("tmz_name")
    @Expose
    private String tmzName;
    @SerializedName("acc_msisdn")
    @Expose
    private String accMsisdn;
    @SerializedName("acc_mem_id")
    @Expose
    private Integer accMemId;
    @SerializedName("acc_otp_status")
    @Expose
    private Integer accOtpStatus;
    @SerializedName("acc_last_login")
    @Expose
    private Integer accLastLogin;
    @SerializedName("currency")
    @Expose
    private Currency currency;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("first_login")
    @Expose
    private Boolean firstLogin;

    public Integer getAccId() {
        return UserParams.ACC_ID;
    }

    public void setAccId(Integer accId) {
        this.accId = accId;
    }

    public Integer getAccFacebookId() {
        return UserParams.ACC_FACEBOOK_ID;
    }

    public void setAccFacebookId(Integer accFacebookId) {
        this.accFacebookId = accFacebookId;
    }

    public String getAccFacebookEmail() {
        return UserParams.ACC_FACEBOOK_EMAIL;
    }

    public void setAccFacebookEmail(String accFacebookEmail) {
        this.accFacebookEmail = accFacebookEmail;
    }

    public String getAccGoogleId() {
        return UserParams.ACC_GOOLGLE_ID;
    }

    public void setAccGoogleId(String accGoogleId) {
        this.accGoogleId = accGoogleId;
    }

    public Integer getAccGoogleEmail() {
        return UserParams.ACC_GOOLGLE_EMAIL;
    }

    public void setAccGoogleEmail(Integer accGoogleEmail) {
        this.accGoogleEmail = accGoogleEmail;
    }

    public String getAccScreenName() {
        return "";
    }

    public void setAccScreenName(String accScreenName) {
        this.accScreenName = accScreenName;
    }

    public String getAccCtyId() {
        return UserParams.ACC_CTY_ID;
    }

    public void setAccCtyId(String accCtyId) {
        this.accCtyId = accCtyId;
    }

    public Integer getAccCreatedDatetime() {
        return UserParams.ACC_CREATED_DATETIME;
    }

    public void setAccCreatedDatetime(Integer accCreatedDatetime) {
        this.accCreatedDatetime = accCreatedDatetime;
    }

    public String getAccPhoto() {
        return UserParams.ACC_PHOTO;
    }

    public void setAccPhoto(String accPhoto) {
        this.accPhoto = accPhoto;
    }

    public Integer getAccStatus() {
        return UserParams.ACC_STATUS;
    }

    public void setAccStatus(Integer accStatus) {
        this.accStatus = accStatus;
    }

    public Integer getAccBirthdate() {
        return UserParams.ACC_BIRTHDATE;
    }

    public void setAccBirthdate(Integer accBirthdate) {
        this.accBirthdate = accBirthdate;
    }

    public String getAccAddress() {
        return UserParams.ACC_ADDRESS;
    }

    public void setAccAddress(String accAddress) {
        this.accAddress = accAddress;
    }

    public String getAccGender() {
        return UserParams.ACC_GENDER;
    }

    public void setAccGender(String accGender) {
        this.accGender = accGender;
    }

    public String getTmzName() {
        return UserParams.TMZ_NAME;
    }

    public void setTmzName(String tmzName) {
        this.tmzName = tmzName;
    }

    public String getAccMsisdn() {
        return UserParams.ACC_MSISDN;
    }

    public void setAccMsisdn(String accMsisdn) {
        this.accMsisdn = accMsisdn;
    }

    public Integer getAccMemId() {
        return UserParams.ACC_MEM_ID;
    }

    public void setAccMemId(Integer accMemId) {
        this.accMemId = accMemId;
    }

    public Integer getAccOtpStatus() {
        return UserParams.ACC_OTP_STATUS;
    }

    public void setAccOtpStatus(Integer accOtpStatus) {
        this.accOtpStatus = accOtpStatus;
    }

    public Integer getAccLastLogin() {
        return UserParams.ACC_LAST_LOGIN;
    }

    public void setAccLastLogin(Integer accLastLogin) {
        this.accLastLogin = accLastLogin;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Boolean getFirstLogin() {
        return UserParams.FIRST_LOGIN;
    }

    public void setFirstLogin(Boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

}
