package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class Term {

    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("title")
    @Expose
    String title;

    @SerializedName("content")
    @Expose
    String content;

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
