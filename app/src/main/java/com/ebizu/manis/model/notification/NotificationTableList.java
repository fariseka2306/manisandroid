package com.ebizu.manis.model.notification;

import org.litepal.annotation.Column;
import org.litepal.crud.DataSupport;

/**
 * Created by ebizu on 8/30/17.
 */

public class NotificationTableList extends DataSupport {

    @Column(unique = true, defaultValue = "unknown")
    private long id;

    @Column(unique = true, defaultValue = "unknown")
    private long notificationId;

    @Column(defaultValue = "unknown")
    private boolean read;

    @Column(defaultValue = "unknown")
    private long savedTime;

    @Column(defaultValue = "unknown")
    private String type;

    /**
     * Beacon Type
     **/
    @Column(defaultValue = "unknown")
    private String beaconPromoDesc;
    @Column(defaultValue = "unknown")
    private Integer beaconCompanyId;

    /**
     * Snap Type
     **/
    @Column(defaultValue = "unknown")
    private String snapMessage;
    @Column(defaultValue = "unknown")
    private String snapStatus;

    public String getBeaconPromoDesc() {
        return beaconPromoDesc;
    }

    public void setBeaconPromoDesc(String beaconPromoDesc) {
        this.beaconPromoDesc = beaconPromoDesc;
    }

    public Integer getBeaconCompanyId() {
        return beaconCompanyId;
    }

    public void setBeaconCompanyId(Integer beaconCompanyId) {
        this.beaconCompanyId = beaconCompanyId;
    }

    public String getSnapMessage() {
        return snapMessage;
    }

    public void setSnapMessage(String snapMessage) {
        this.snapMessage = snapMessage;
    }

    public String getSnapStatus() {
        return snapStatus;
    }

    public void setSnapStatus(String snapStatus) {
        this.snapStatus = snapStatus;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public long getSavedTime() {
        return savedTime;
    }

    public void setSavedTime(long savedTime) {
        this.savedTime = savedTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(long notificationId) {
        this.notificationId = notificationId;
    }

    @Override
    public String toString() {
        return "NotificationTableList{" +
                "id=" + id +
                ", notificationId=" + notificationId +
                ", read=" + read +
                ", savedTime=" + savedTime +
                ", type='" + type + '\'' +
                ", beaconPromoDesc='" + beaconPromoDesc + '\'' +
                ", beaconCompanyId=" + beaconCompanyId +
                ", snapMessage='" + snapMessage + '\'' +
                ", snapStatus='" + snapStatus + '\'' +
                '}';
    }
}
