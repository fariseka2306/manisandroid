package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 7/31/2017.
 */

public class FaqChild implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("content")
    @Expose
    private String content;

    protected FaqChild(Parcel in) {
        title = in.readString();
        content = in.readString();
    }

    public static final Creator<FaqChild> CREATOR = new Creator<FaqChild>() {
        @Override
        public FaqChild createFromParcel(Parcel in) {
            return new FaqChild(in);
        }

        @Override
        public FaqChild[] newArray(int size) {
            return new FaqChild[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(content);
    }
}
