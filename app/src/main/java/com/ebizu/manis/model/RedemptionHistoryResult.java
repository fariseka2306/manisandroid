package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.manis.sdk.entities.RewardsClaimedItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 8/7/2017.
 */

public class RedemptionHistoryResult implements Parcelable{

    @SerializedName("redeemed")
    @Expose
    private Integer redeemed;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("expired")
    @Expose
    private Integer expired;
    @SerializedName("store")
    @Expose
    private String store;
    @SerializedName("assets")
    @Expose
    private RedemptionHistoryAssets assets;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("reward_type")
    @Expose
    private String rewardType;
    @SerializedName("sn")
    @Expose
    private String sn;
    @SerializedName("point")
    @Expose
    private String point;
    @SerializedName("delivery_status")
    @Expose
    private Integer deliveryStatus;
    @SerializedName("shipment_date")
    @Expose
    private String shipmentDate;
    @SerializedName("shipment_number")
    @Expose
    private String shipmentNumber;
    @SerializedName("shipment_expedition")
    @Expose
    private String shipmentExpedition;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("track_link")
    @Expose
    private String trackLink;

    public RedemptionHistoryResult() {

    }

    public Integer getRedeemed() {
        return redeemed;
    }

    public RedemptionHistoryResult(Parcel in) {
        this.redeemed = in.readInt();
        this.datetime = in.readString();
        this.name = in.readString();
        this.expired = in.readInt();
        this.store = in.readString();
        this.assets = in.readParcelable(RewardsClaimedItem.Assets.class.getClassLoader());
        this.type = in.readString();
        this.description = in.readString();
        this.code = in.readString();
        this.sn = in.readString();
        this.point = in.readString();
        this.deliveryStatus = in.readInt();
        this.shipmentDate = in.readString();
        this.shipmentNumber = in.readString();
        this.shipmentExpedition = in.readString();
        this.notes = in.readString();
        this.rewardType = in.readString();
        this.trackLink = in.readString();
    }

    public static final Creator<RedemptionHistoryResult> CREATOR = new Creator<RedemptionHistoryResult>() {
        @Override
        public RedemptionHistoryResult createFromParcel(Parcel in) {
            return new RedemptionHistoryResult(in);
        }

        @Override
        public RedemptionHistoryResult[] newArray(int size) {
            return new RedemptionHistoryResult[size];
        }
    };

    public String getDatetime() {
        return datetime;
    }

    public String getName() {
        return name;
    }

    public Integer getExpired() {
        return expired;
    }

    public String getStore() {
        return store;
    }

    public RedemptionHistoryAssets getAssets() {
        return assets;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }

    public String getRewardType() {
        return rewardType;
    }

    public String getSn() {
        return sn;
    }

    public String getPoint() {
        return point;
    }

    public Integer getDeliveryStatus() {
        return deliveryStatus;
    }

    public String getShipmentDate() {
        return shipmentDate;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    public String getShipmentExpedition() {
        return shipmentExpedition;
    }

    public String getNotes() {
        return notes;
    }

    public String getTrackLink() {
        return trackLink;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.redeemed);
        dest.writeString(this.datetime);
        dest.writeString(this.name);
        dest.writeInt(this.expired);
        dest.writeString(this.store);
        dest.writeString(this.type);
        dest.writeString(this.description);
        dest.writeString(this.code);
        dest.writeString(this.sn);
        dest.writeString(this.point);
        dest.writeInt(this.deliveryStatus);
        dest.writeString(this.shipmentDate);
        dest.writeString(this.shipmentNumber);
        dest.writeString(this.shipmentExpedition);
        dest.writeString(this.notes);
        dest.writeString(this.rewardType);
        dest.writeString(this.trackLink);
    }
}
