package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firef on 7/10/2017.
 */

public class InterestsStore implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("pinned")
    @Expose
    private Boolean pinned;

    @SerializedName("icon")
    @Expose
    private String icon;

    @SerializedName("assets")
    @Expose
    private Assets assets;

    @SerializedName("count")
    @Expose
    private Integer count;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getPinned() {
        return pinned;
    }

    public void setPinned(Boolean pinned) {
        this.pinned = pinned;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Assets getAssets() {
        return assets;
    }

    public void setAssets(Assets assets) {
        this.assets = assets;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeValue(this.pinned);
        dest.writeString(this.icon);
        dest.writeParcelable(this.assets, flags);
        dest.writeValue(this.count);
    }

    public InterestsStore() {
    }

    protected InterestsStore(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.pinned = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.icon = in.readString();
        this.assets = in.readParcelable(Assets.class.getClassLoader());
        this.count = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<InterestsStore> CREATOR = new Creator<InterestsStore>() {
        @Override
        public InterestsStore createFromParcel(Parcel source) {
            return new InterestsStore(source);
        }

        @Override
        public InterestsStore[] newArray(int size) {
            return new InterestsStore[size];
        }
    };
}
