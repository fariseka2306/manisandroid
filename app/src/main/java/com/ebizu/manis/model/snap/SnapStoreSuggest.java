package com.ebizu.manis.model.snap;

import com.ebizu.manis.model.Store;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public class SnapStoreSuggest {

    @SerializedName("more")
    @Expose
    private Boolean more;
    @SerializedName("suggestions")
    @Expose
    private ArrayList<Store> stores = null;

    public Boolean getMore() {
        return more;
    }

    public ArrayList<Store> getStores() {
        return stores;
    }
}
