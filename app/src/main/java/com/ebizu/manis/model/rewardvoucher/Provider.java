package com.ebizu.manis.model.rewardvoucher;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class Provider implements Parcelable {
    @SerializedName("image")
    @Expose
    private String image = "";
    @SerializedName("website")
    @Expose
    private String website = "";
    @SerializedName("address")
    @Expose
    private String address = "";
    @SerializedName("phone")
    @Expose
    private String phone = "";
    @SerializedName("name")
    @Expose
    private String name = "";
    @SerializedName("lon")
    @Expose
    private Double lon = 0.0;
    @SerializedName("id")
    @Expose
    private String id = "";
    @SerializedName("category")
    @Expose
    private List<Category> category = new ArrayList<>();
    @SerializedName("lat")
    @Expose
    private Double lat = 0.0;

    public Provider() {}


    protected Provider(Parcel in) {
        image = in.readString();
        website = in.readString();
        address = in.readString();
        phone = in.readString();
        name = in.readString();
        if (in.readByte() == 0) {
            lon = null;
        } else {
            lon = in.readDouble();
        }
        id = in.readString();
        category = in.createTypedArrayList(Category.CREATOR);
        if (in.readByte() == 0) {
            lat = null;
        } else {
            lat = in.readDouble();
        }
    }

    public static final Creator<Provider> CREATOR = new Creator<Provider>() {
        @Override
        public Provider createFromParcel(Parcel in) {
            return new Provider(in);
        }

        @Override
        public Provider[] newArray(int size) {
            return new Provider[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeString(website);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeString(name);
        if (lon == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(lon);
        }
        dest.writeString(id);
        dest.writeTypedList(category);
        if (lat == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(lat);
        }
    }

    public String getImage() {
        return image;
    }

    public String getWebsite() {
        return website;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public Double getLon() {
        return lon;
    }

    public String getId() {
        return id;
    }

    public List<Category> getCategory() {
        return category;
    }

    public Double getLat() {
        return lat;
    }

    public static Creator<Provider> getCREATOR() {
        return CREATOR;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }
}
