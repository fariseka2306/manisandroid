package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 7/7/2017.
 */

public class Account {

    public static final String  GENDER_MALE = "male";
    public static final String GENDER_FEMALE = "female";

    @SerializedName("acc_id")
    @Expose
    private String accId;
    @SerializedName("acc_facebook_id")
    @Expose
    private long accFacebookId;
    @SerializedName("acc_facebook_email")
    @Expose
    private String accFacebookEmail;
    @SerializedName("acc_google_id")
    @Expose
    private String accGoogleId;
    @SerializedName("acc_google_email")
    @Expose
    private String accGoogleEmail;
    @SerializedName("acc_screen_name")
    @Expose
    private String accScreenName;
    @SerializedName("acc_cty_id")
    @Expose
    private String accCountry;
    @SerializedName("acc_photo")
    @Expose
    private String accPhoto;
    @SerializedName("acc_last_login")
    @Expose
    private long accLastLogin;
    @SerializedName("acc_device_id")
    @Expose
    private String accDeviceId;
    @SerializedName("acc_status")
    @Expose
    private int accStatus;
    @SerializedName("tmz_name")
    @Expose
    private String tmzName;
    @SerializedName("acc_gender")
    @Expose
    private String accGender;
    @SerializedName("acc_msisdn")
    @Expose
    private String accMsisdn;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("currency")
    @Expose
    private Currency currency;
    @SerializedName("acc_birthdate")
    @Expose
    public long accBirthdate;
    @SerializedName("acc_address")
    @Expose
    private String accAddress;
    @SerializedName("aci_postcode")
    @Expose
    private String aciPostcode;
    @SerializedName("aci_city")
    @Expose
    private String aciCity;
    @SerializedName("aci_state")
    @Expose
    private String aciState;
    @SerializedName("aci_country")
    @Expose
    private String aciCountry;
    @SerializedName("acc_otp_status")
    @Expose
    private long accOtpStatus;
    @SerializedName("first_login")
    @Expose
    private boolean firstLogin;
    @SerializedName("point")
    @Expose
    private long point;
    @SerializedName("acc_created_datetime")
    @Expose
    private long accCreatedDateTime;
    @SerializedName("updateable_email")
    @Expose
    private boolean updateableEmail = false;
    @SerializedName("updateable_phone")
    @Expose
    private boolean updateablePhone = false;

    public String getAccId() {
        return accId;
    }

    public long getAccFacebookId() {
        return accFacebookId;
    }

    public String getAccFacebookEmail() {
        return accFacebookEmail;
    }

    public String getAccGoogleId() {
        return accGoogleId;
    }

    public String getAccGoogleEmail() {
        return accGoogleEmail;
    }

    public String getAccScreenName() {
        return accScreenName;
    }

    public String getAccCountry() {
        return accCountry;
    }

    public String getAccPhoto() {
        return accPhoto;
    }

    public long getAccLastLogin() {
        return accLastLogin;
    }

    public String getAccDeviceId() {
        return accDeviceId;
    }

    public int getAccStatus() {
        return accStatus;
    }

    public String getTmzName() {
        return tmzName;
    }

    public String getAccGender() {
        return accGender;
    }

    public String getAccMsisdn() {
        return accMsisdn;
    }

    public String getToken() {
        return token;
    }

    public Currency getCurrency() {
        return currency;
    }

    public long getAccBirthdate() {
        return accBirthdate;
    }

    public String getAccAddress() {
        return accAddress;
    }

    public long getAccOtpStatus() {
        return accOtpStatus;
    }

    public boolean isFirstLogin() {
        return firstLogin;
    }

    public long getPoint() {
        return point;
    }

    public long getAccCreatedDateTime() {
        return accCreatedDateTime;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public void setAccFacebookId(long accFacebookId) {
        this.accFacebookId = accFacebookId;
    }

    public void setAccFacebookEmail(String accFacebookEmail) {
        this.accFacebookEmail = accFacebookEmail;
    }

    public void setAccGoogleId(String accGoogleId) {
        this.accGoogleId = accGoogleId;
    }

    public void setAccGoogleEmail(String accGoogleEmail) {
        this.accGoogleEmail = accGoogleEmail;
    }

    public void setAccScreenName(String accScreenName) {
        this.accScreenName = accScreenName;
    }

    public void setAccCountry(String accCountry) {
        this.accCountry = accCountry;
    }

    public void setAccPhoto(String accPhoto) {
        this.accPhoto = accPhoto;
    }

    public void setAccLastLogin(long accLastLogin) {
        this.accLastLogin = accLastLogin;
    }

    public void setAccDeviceId(String accDeviceId) {
        this.accDeviceId = accDeviceId;
    }

    public void setAccStatus(int accStatus) {
        this.accStatus = accStatus;
    }

    public void setTmzName(String tmzName) {
        this.tmzName = tmzName;
    }

    public void setAccGender(String accGender) {
        this.accGender = accGender;
    }

    public void setAccMsisdn(String accMsisdn) {
        this.accMsisdn = accMsisdn;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public void setAccBirthdate(long accBirthdate) {
        this.accBirthdate = accBirthdate;
    }

    public void setAccAddress(String accAddress) {
        this.accAddress = accAddress;
    }

    public void setAccOtpStatus(long accOtpStatus) {
        this.accOtpStatus = accOtpStatus;
    }

    public void setFirstLogin(boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

    public void setPoint(long point) {
        this.point = point;
    }

    public void setAccCreatedDateTime(long accCreatedDateTime) {
        this.accCreatedDateTime = accCreatedDateTime;
    }

    public String getAciPostcode() {
        return aciPostcode;
    }

    public void setAciPostcode(String aciPostcode) {
        this.aciPostcode = aciPostcode;
    }

    public String getAciCity() {
        return aciCity;
    }

    public void setAciCity(String aciCity) {
        this.aciCity = aciCity;
    }

    public String getAciState() {
        return aciState;
    }

    public void setAciState(String aciState) {
        this.aciState = aciState;
    }

    public String getAciCountry() {
        return aciCountry;
    }

    public void setAciCountry(String aciCountry) {
        this.aciCountry = aciCountry;
    }

    public String getEmail() {
        if (null != this.accGoogleEmail && this.accGoogleEmail.length() > 3) {
            return accGoogleEmail;
        } else if (null != this.accFacebookEmail && this.accFacebookEmail.length() > 3) {
            return accFacebookEmail;
        } else {
            return "No email";
        }
    }

    public boolean isUpdateableEmail() {
        return updateableEmail;
    }

    public boolean isUpdateablePhone() {
        return updateablePhone;
    }

    public void setUpdateableEmail(boolean updateableEmail) {
        this.updateableEmail = updateableEmail;
    }

    public void setUpdateablePhone(boolean updateablePhone) {
        this.updateablePhone = updateablePhone;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accId='" + accId + '\'' +
                ", accFacebookId=" + accFacebookId +
                ", accFacebookEmail='" + accFacebookEmail + '\'' +
                ", accGoogleId='" + accGoogleId + '\'' +
                ", accGoogleEmail='" + accGoogleEmail + '\'' +
                ", accScreenName='" + accScreenName + '\'' +
                ", accCountry='" + accCountry + '\'' +
                ", accPhoto='" + accPhoto + '\'' +
                ", accLastLogin=" + accLastLogin +
                ", accDeviceId='" + accDeviceId + '\'' +
                ", accStatus=" + accStatus +
                ", tmzName='" + tmzName + '\'' +
                ", accGender='" + accGender + '\'' +
                ", accMsisdn='" + accMsisdn + '\'' +
                ", token='" + token + '\'' +
                ", currency=" + currency +
                ", accBirthdate=" + accBirthdate +
                ", accAddress='" + accAddress + '\'' +
                ", aciPostcode='" + aciPostcode + '\'' +
                ", aciCity='" + aciCity + '\'' +
                ", aciState='" + aciState + '\'' +
                ", aciCountry='" + aciCountry + '\'' +
                ", accOtpStatus=" + accOtpStatus +
                ", firstLogin=" + firstLogin +
                ", point=" + point +
                ", accCreatedDateTime=" + accCreatedDateTime +
                ", updateableEmail=" + updateableEmail +
                ", updateablePhone=" + updateablePhone +
                '}';
    }
}