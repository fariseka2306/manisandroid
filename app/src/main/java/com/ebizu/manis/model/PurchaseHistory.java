package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public class PurchaseHistory implements Parcelable {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("voucherName")
    @Expose
    private String voucherName;
    @SerializedName("voucherType")
    @Expose
    private String voucherType;
    @SerializedName("brand")
    @Expose
    private PurchaseHistoryBrand purchaseHistoryBrand;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("paymentTitle")
    @Expose
    private String paymentTitle;
    @SerializedName("paymentSubtitle")
    @Expose
    private String paymentSubtitle;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("amount")
    @Expose
    private double amount;
    @SerializedName("paidTitle")
    @Expose
    private String paidTitle;
    @SerializedName("transactionTime")
    @Expose
    private String transactionTime;
    @SerializedName("voucherIcon")
    @Expose
    private String voucherIcon;
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;
    @SerializedName("paymentStatus")
    @Expose
    private String paymentStatus;
    @SerializedName("customer")
    @Expose
    private PurchaseHistoryCustomer purchaseHistoryCustomer;
    @SerializedName("deliveryStatus")
    @Expose
    private String deliveryStatus;
    @SerializedName("shipmentDate")
    @Expose
    private String shipmentDate;
    @SerializedName("shipmentNumber")
    @Expose
    private String shipmentNumber;
    @SerializedName("shipmentExpedition")
    @Expose
    private String shipmentExpedition;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("purchaseDescription")
    @Expose
    private String purchaseDescription;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public void setVoucherName(String voucherName) {
        this.voucherName = voucherName;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public PurchaseHistoryBrand getPurchaseHistoryBrand() {
        return purchaseHistoryBrand;
    }

    public void setPurchaseHistoryBrand(PurchaseHistoryBrand purchaseHistoryBrand) {
        this.purchaseHistoryBrand = purchaseHistoryBrand;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTitle() {
        return paymentTitle;
    }

    public void setPaymentTitle(String paymentTitle) {
        this.paymentTitle = paymentTitle;
    }

    public String getPaymentSubtitle() {
        return paymentSubtitle;
    }

    public void setPaymentSubtitle(String paymentSubtitle) {
        this.paymentSubtitle = paymentSubtitle;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPaidTitle() {
        return paidTitle;
    }

    public void setPaidTitle(String paidTitle) {
        this.paidTitle = paidTitle;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getVoucherIcon() {
        return voucherIcon;
    }

    public void setVoucherIcon(String voucherIcon) {
        this.voucherIcon = voucherIcon;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public PurchaseHistoryCustomer getPurchaseHistoryCustomer() {
        return purchaseHistoryCustomer;
    }

    public void setPurchaseHistoryCustomer(PurchaseHistoryCustomer purchaseHistoryCustomer) {
        this.purchaseHistoryCustomer = purchaseHistoryCustomer;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(String shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public String getShipmentExpedition() {
        return shipmentExpedition;
    }

    public void setShipmentExpedition(String shipmentExpedition) {
        this.shipmentExpedition = shipmentExpedition;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPurchaseDescription() {
        return purchaseDescription;
    }

    public void setPurchaseDescription(String purchaseDescription) {
        this.purchaseDescription = purchaseDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.orderId);
        dest.writeString(this.voucherName);
        dest.writeString(this.voucherType);
        dest.writeParcelable(this.purchaseHistoryBrand, flags);
        dest.writeString(this.paymentType);
        dest.writeString(this.paymentTitle);
        dest.writeString(this.paymentSubtitle);
        dest.writeString(this.currency);
        dest.writeDouble(this.amount);
        dest.writeString(this.paidTitle);
        dest.writeString(this.transactionTime);
        dest.writeString(this.voucherIcon);
        dest.writeString(this.voucherCode);
        dest.writeString(this.paymentStatus);
        dest.writeParcelable(this.purchaseHistoryCustomer, flags);
        dest.writeString(this.deliveryStatus);
        dest.writeString(this.shipmentDate);
        dest.writeString(this.shipmentNumber);
        dest.writeString(this.shipmentExpedition);
        dest.writeString(this.notes);
        dest.writeString(this.purchaseDescription);
    }

    public PurchaseHistory() {
    }

    protected PurchaseHistory(Parcel in) {
        this.orderId = in.readString();
        this.voucherName = in.readString();
        this.voucherType = in.readString();
        this.purchaseHistoryBrand = in.readParcelable(PurchaseHistoryBrand.class.getClassLoader());
        this.paymentType = in.readString();
        this.paymentTitle = in.readString();
        this.paymentSubtitle = in.readString();
        this.currency = in.readString();
        this.amount = in.readInt();
        this.paidTitle = in.readString();
        this.transactionTime = in.readString();
        this.voucherIcon = in.readString();
        this.voucherCode = in.readString();
        this.paymentStatus = in.readString();
        this.purchaseHistoryCustomer = in.readParcelable(PurchaseHistoryCustomer.class.getClassLoader());
        this.deliveryStatus = in.readString();
        this.shipmentDate = in.readString();
        this.shipmentNumber = in.readString();
        this.shipmentExpedition = in.readString();
        this.notes = in.readString();
        this.purchaseDescription = in.readString();
    }

    public static final Creator<PurchaseHistory> CREATOR = new Creator<PurchaseHistory>() {
        @Override
        public PurchaseHistory createFromParcel(Parcel source) {
            return new PurchaseHistory(source);
        }

        @Override
        public PurchaseHistory[] newArray(int size) {
            return new PurchaseHistory[size];
        }
    };
}
