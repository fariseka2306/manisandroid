package com.ebizu.manis.model.snap;

import com.ebizu.manis.model.Store;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public class SnapStoreResult {
    @SerializedName("more")
    @Expose
    private Boolean more;
    @SerializedName("results")
    @Expose
    private ArrayList<Store> stores = null;

    public Boolean getMore() {
        return more;
    }

    public void setMore(Boolean more) {
        this.more = more;
    }

    public ArrayList<Store> getStores() {
        return stores;
    }
}
