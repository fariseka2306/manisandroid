package com.ebizu.manis.model.rewardbulk;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 11/14/17.
 */

public class Input implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("required")
    @Expose
    private String required;
    @SerializedName("filledByApplication")
    @Expose
    private String filledByApplication;

    protected Input(Parcel in) {
        id = in.readString();
        text = in.readString();
        type = in.readString();
        required = in.readString();
        filledByApplication = in.readString();
    }

    public static final Creator<Input> CREATOR = new Creator<Input>() {
        @Override
        public Input createFromParcel(Parcel in) {
            return new Input(in);
        }

        @Override
        public Input[] newArray(int size) {
            return new Input[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getType() {
        return type;
    }

    public String getRequired() {
        return required;
    }

    public String getFilledByApplication() {
        return filledByApplication;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(text);
        dest.writeString(type);
        dest.writeString(required);
        dest.writeString(filledByApplication);
    }
}
