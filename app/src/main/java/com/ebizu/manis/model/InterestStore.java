package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 7/23/17.
 */

public class InterestStore implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("assets")
    @Expose
    private Assets_ assets;
    @SerializedName("coordinate")
    @Expose
    private Coordinate coordinate;
    @SerializedName("premium")
    @Expose
    private Boolean premium;
    @SerializedName("multiplier")
    @Expose
    private Integer multiplier;
    @SerializedName("has")
    @Expose
    private Has has;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Assets_ getAssets() {
        return assets;
    }

    public void setAssets(Assets_ assets) {
        this.assets = assets;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Boolean getPremium() {
        return premium;
    }

    public void setPremium(Boolean premium) {
        this.premium = premium;
    }

    public Integer getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }

    public Has getHas() {
        return has;
    }

    public void setHas(Has has) {
        this.has = has;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.name);
        dest.writeString(this.address);
        dest.writeParcelable(this.category, flags);
        dest.writeParcelable(this.assets, flags);
        dest.writeParcelable(this.coordinate, flags);
        dest.writeValue(this.premium);
        dest.writeValue(this.multiplier);
        dest.writeParcelable(this.has, flags);
    }

    public InterestStore() {
    }

    protected InterestStore(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.address = in.readString();
        this.category = in.readParcelable(Category.class.getClassLoader());
        this.assets = in.readParcelable(Assets_.class.getClassLoader());
        this.coordinate = in.readParcelable(Coordinate.class.getClassLoader());
        this.premium = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.multiplier = (Integer) in.readValue(Integer.class.getClassLoader());
        this.has = in.readParcelable(Has.class.getClassLoader());
    }

    public static final Creator<InterestStore> CREATOR = new Creator<InterestStore>() {
        @Override
        public InterestStore createFromParcel(Parcel source) {
            return new InterestStore(source);
        }

        @Override
        public InterestStore[] newArray(int size) {
            return new InterestStore[size];
        }
    };
}
