package com.ebizu.manis.model.saved;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 8/21/17.
 */

public class Assets_ {

    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("banner")
    @Expose
    private String banner;

    public String getPhoto() {
        return photo;
    }

    public String getBanner() {
        return banner;
    }
}
