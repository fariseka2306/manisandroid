package com.ebizu.manis.model.rewardbulk;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.sdk.reward.models.Reward;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARIS_mac on 11/14/17.
 */

public class RewardBulk extends Reward {

    public static final String VOUCHER_TRANSACTION_REDEEMABLE = "Redeemable";
    public static final String VOUCHER_TRANSACTION_PURCHASABLE = "Purchasable";
    public static final String VOUCHER_TRANSACTION_BOTH = "Both";
    public static final String VOUCHER_REDEEM_CODE = "Code";
    public static final String VOUCHER_REDEEM_PIN = "Pin";

    @SerializedName("bulkId")
    @Expose
    private String bulkId = "";
    @SerializedName("bulkType")
    @Expose
    private String bulkType = "";
    @SerializedName("image")
    @Expose
    private String image = "";
    @SerializedName("image128")
    @Expose
    private String image128 = "";

    @SerializedName("image64")
    @Expose
    private String image64 = "";

    public RewardBulk() {
    }

    protected RewardBulk(Parcel in) {
        bulkId = in.readString();
        bulkType = in.readString();
        image = in.readString();
        image64 = in.readString();
        image128 = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bulkId);
        dest.writeString(bulkType);
        dest.writeString(image);
        dest.writeString(image64);
        dest.writeString(image128);
    }

    public String getBulkId() {
        return bulkId;
    }

    public String getBulkType() {
        return bulkType;
    }

    public String getImage() {
        return image;
    }

    public String getImage64() {
        return image64;
    }

    public String getImage128() {
        return image128;
    }
}