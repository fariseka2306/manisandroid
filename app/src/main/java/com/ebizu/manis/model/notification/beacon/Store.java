package com.ebizu.manis.model.notification.beacon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.litepal.crud.DataSupport;

/**
 * Created by ebizu on 8/23/17.
 */

public class Store {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("assets")
    @Expose
    private Assets assets;
    @SerializedName("premium")
    @Expose
    private Boolean premium;
    @SerializedName("multiplier")
    @Expose
    private Integer multiplier;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Category getCategory() {
        return category;
    }

    public Assets getAssets() {
        return assets;
    }

    public Boolean getPremium() {
        return premium;
    }

    public Integer getMultiplier() {
        return multiplier;
    }
}
