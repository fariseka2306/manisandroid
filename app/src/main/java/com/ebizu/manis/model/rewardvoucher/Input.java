package com.ebizu.manis.model.rewardvoucher;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.InputType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class Input implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id = "";
    @SerializedName("text")
    @Expose
    private String text = "";
    @SerializedName("type")
    @Expose
    private String type = "";
    @SerializedName("filledByApplication")
    @Expose
    private String filledByApplication = "";
    @SerializedName("required")
    @Expose
    private String required = "";

    protected Input(Parcel in) {
        id = in.readString();
        text = in.readString();
        type = in.readString();
        filledByApplication = in.readString();
        required = in.readString();
    }

    public Input() {
    }

    public static final Creator<Input> CREATOR = new Creator<Input>() {
        @Override
        public Input createFromParcel(Parcel in) {
            return new Input(in);
        }

        @Override
        public Input[] newArray(int size) {
            return new Input[size];
        }
    };

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getFilledByApplication() {
        return filledByApplication;
    }

    public void setFilledByApplication(String filledByApplication) {
        this.filledByApplication = filledByApplication;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public boolean isRequired() {
        return required != null && required.equalsIgnoreCase("Y");
    }

    public boolean isFilledByApplication() {
        return filledByApplication != null && filledByApplication.equalsIgnoreCase("Y");
    }

    /**
     * @return android.widget.EditText {@link InputType}
     */
    public int getInputType() {
        if (type.equalsIgnoreCase("N")) {
            return InputType.TYPE_CLASS_NUMBER;
        } else if (type.equalsIgnoreCase("P")) {
            return InputType.TYPE_CLASS_PHONE;
        } else if (type.equalsIgnoreCase("M")) {
            return InputType.TYPE_TEXT_FLAG_MULTI_LINE;
        } else {
            return InputType.TYPE_CLASS_TEXT;
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(text);
        dest.writeString(type);
        dest.writeString(filledByApplication);
        dest.writeString(required);
    }
}
