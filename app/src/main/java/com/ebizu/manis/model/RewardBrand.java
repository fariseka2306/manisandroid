package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 9/23/17.
 */

public class RewardBrand implements Parcelable {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.id);
    }

    public RewardBrand() {
    }

    protected RewardBrand(Parcel in) {
        this.name = in.readString();
        this.id = in.readString();
    }

    public static final Creator<RewardBrand> CREATOR = new Creator<RewardBrand>() {
        @Override
        public RewardBrand createFromParcel(Parcel source) {
            return new RewardBrand(source);
        }

        @Override
        public RewardBrand[] newArray(int size) {
            return new RewardBrand[size];
        }
    };
}
