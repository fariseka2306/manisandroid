package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Halim on 9/23/17.
 */

public class RewardPaging implements Parcelable {

    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("pageCount")
    @Expose
    private Integer pageCount;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("data")
    @Expose
    private List<Reward> rewards;
    @SerializedName("more")
    @Expose
    private boolean more;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("page")
    @Expose
    private Integer page;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<Reward> getRewards() {
        return rewards;
    }

    public void setRewards(List<Reward> rewards) {
        this.rewards = rewards;
    }

    public Boolean getMore() {
        return more;
    }

    public void setMore(Boolean more) {
        this.more = more;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.total);
        dest.writeValue(this.pageCount);
        dest.writeValue(this.size);
        dest.writeList(this.rewards);
        dest.writeValue(this.more);
        dest.writeValue(this.count);
        dest.writeValue(this.page);
    }

    public RewardPaging() {
    }

    protected RewardPaging(Parcel in) {
        this.total = (Integer) in.readValue(Integer.class.getClassLoader());
        this.pageCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.size = (Integer) in.readValue(Integer.class.getClassLoader());
        this.rewards = new ArrayList<Reward>();
        in.readList(this.rewards, Reward.class.getClassLoader());
        this.more = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.count = (Integer) in.readValue(Integer.class.getClassLoader());
        this.page = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<RewardPaging> CREATOR = new Creator<RewardPaging>() {
        @Override
        public RewardPaging createFromParcel(Parcel source) {
            return new RewardPaging(source);
        }

        @Override
        public RewardPaging[] newArray(int size) {
            return new RewardPaging[size];
        }
    };
}

