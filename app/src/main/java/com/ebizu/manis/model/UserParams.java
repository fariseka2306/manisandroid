package com.ebizu.manis.model;

/**
 * Created by Raden on 7/3/17.
 */

public class UserParams {
    public static boolean SUCCESS = true;
    public static String MESSAGE = "";
    public static int ACC_ID = 100303;
    public static int ACC_FACEBOOK_ID = 0;
    public static String ACC_FACEBOOK_EMAIL = "eolle@gmail.com";
    public static String ACC_GOOLGLE_ID = "104418207620175366925";
    public static int ACC_GOOLGLE_EMAIL = 0;
    public static String ACC_GOOLGLE_NAME = "Elle Ong";
    public static String ACC_CTY_ID = "MY";
    public static int ACC_CREATED_DATETIME = 1497413228;
    public static String ACC_PHOTO = "";
    public static int ACC_STATUS = 1;
    public static int ACC_BIRTHDATE = 0;
    public static String ACC_ADDRESS = "";
    public static String ACC_GENDER = "female";
    public static String TMZ_NAME = "Asia/Kuala_Lumpur";
    public static String ACC_MSISDN = "";
    public static int ACC_MEM_ID = 31692;
    public static int ACC_OTP_STATUS = 0;
    public static int ACC_LAST_LOGIN = 1499082779;
    public static String CURRENCY_ISO2 = "RMssa";
    public static String CURRENCY_ISO3 = "MYR";
    public static String TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6IlpJbmhkcE83UGIxIn0.eyJpc3MiOiJiYTJhY2EzYzIzMDM5OTgxY2Q2MTM2NWU4YTZiYjkxZTgxZDkyM2YzNDVmZDNhODA2OTI5YzYxMDMxNjBlYzkxIiwiYXVkIjoiNzEyMzRiOTFjMTUzZjJlOTE5NjE0NjkxNzY3N2IwZWNmZGMyMDc5NTQwNjQxY2JlMTE0ZDBlZWQ5YTU2OGQ3MSIsImp0aSI6IlpJbmhkcE83UGIxIiwiaWF0IjoxNTAyNzcxNDU5LCJuYmYiOjE1MDI3NzE0NTksImV4cCI6MTY2MDQ1MTQ1OSwiZGF0YSI6InJEdG9reGFBdHNiUW5SRko1VWlUbXozZTc3ZTBhZDFlZDIzMmYyNmEyM2Q0ZTllMmE4NTMxOTQ5ZjhmNGZhMTIxYTg5OWIyZjQ1YTBmMTI1MGM5YWE1YjZiNGE2MjRmZTczMTQxY2JmODAwYTNjNDAwNmQ1MTEwYjE5NzA0YjUzYmI2NWRkYzQ2ZmRlZWRmZWQxZmU0OGY2MGNjYjU4YmU4OTg5OWYzNTdlNzc5MTZmYzMwY2FhOTE1Y2RkMjlkMjJkNzIwYjJhNzNhZGI3ZTAwY2MxNzljNGZiYjEwYzE5NjkwMzIwYmNkMDc5MWYwMjNmODliZiJ9.HCacaLx_bllPGtmaa9AaRfMEPgcUb-pqut6R1UswdFg";
    public static boolean FIRST_LOGIN = true;

}
