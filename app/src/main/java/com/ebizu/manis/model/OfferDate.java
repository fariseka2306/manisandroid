package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 8/22/17.
 */

public class OfferDate implements Parcelable {

    @SerializedName("start")
    @Expose
    private long start;
    @SerializedName("end")
    @Expose
    private long end;
    @SerializedName("expired")
    @Expose
    private long expired;

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public long getExpired() {
        return expired;
    }

    public void setExpired(long expired) {
        this.expired = expired;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.start);
        dest.writeLong(this.end);
        dest.writeLong(this.expired);
    }

    public OfferDate() {
    }

    protected OfferDate(Parcel in) {
        this.start = in.readLong();
        this.end = in.readLong();
        this.expired = in.readLong();
    }

    public static final Creator<OfferDate> CREATOR = new Creator<OfferDate>() {
        @Override
        public OfferDate createFromParcel(Parcel source) {
            return new OfferDate(source);
        }

        @Override
        public OfferDate[] newArray(int size) {
            return new OfferDate[size];
        }
    };
}
