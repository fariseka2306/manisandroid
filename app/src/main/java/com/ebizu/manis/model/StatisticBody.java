package com.ebizu.manis.model;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class StatisticBody {
    private String limit;

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }
}
