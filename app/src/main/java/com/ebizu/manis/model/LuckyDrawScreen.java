package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 04/08/17.
 */

public class LuckyDrawScreen implements Parcelable {

    @SerializedName("user_tickets_alltime")
    @Expose
    private Integer userTicketsAlltime;

    @SerializedName("user_tickets_current_period")
    @Expose
    private Integer userTicketsCurrentPeriod;

    @SerializedName("last_winner_prefix")
    @Expose
    private String lastWinnerPrefix;

    @SerializedName("last_winner_object")
    @Expose
    private String lastWinnerObject;

    @SerializedName("currency")
    @Expose
    private Currency currency;

    @SerializedName("total_price")
    @Expose
    private Integer totalPrice;

    @SerializedName("prizes")
    @Expose
    private Prizes prizes;

    @SerializedName("lucky_draw_expired_date")
    @Expose
    private Integer luckyDrawExpiredDate;

    @SerializedName("lucky_draw_msg_prefix")
    @Expose
    private String luckyDrawMsgPrefix;

    @SerializedName("lucky_draw_msg_object")
    @Expose
    private String luckyDrawMsgObject;

    public Integer getUserTicketsAlltime() {
        return userTicketsAlltime;
    }

    public void setUserTicketsAlltime(Integer userTicketsAlltime) {
        this.userTicketsAlltime = userTicketsAlltime;
    }

    public Integer getUserTicketsCurrentPeriod() {
        return userTicketsCurrentPeriod;
    }

    public void setUserTicketsCurrentPeriod(Integer userTicketsCurrentPeriod) {
        this.userTicketsCurrentPeriod = userTicketsCurrentPeriod;
    }

    public String getLastWinnerPrefix() {
        return lastWinnerPrefix;
    }

    public void setLastWinnerPrefix(String lastWinnerPrefix) {
        this.lastWinnerPrefix = lastWinnerPrefix;
    }

    public String getLastWinnerObject() {
        return lastWinnerObject;
    }

    public void setLastWinnerObject(String lastWinnerObject) {
        this.lastWinnerObject = lastWinnerObject;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Prizes getPrizes() {
        return prizes;
    }

    public void setPrizes(Prizes prizes) {
        this.prizes = prizes;
    }

    public Integer getLuckyDrawExpiredDate() {
        return luckyDrawExpiredDate;
    }

    public void setLuckyDrawExpiredDate(Integer luckyDrawExpiredDate) {
        this.luckyDrawExpiredDate = luckyDrawExpiredDate;
    }

    public String getLuckyDrawMsgPrefix() {
        return luckyDrawMsgPrefix;
    }

    public void setLuckyDrawMsgPrefix(String luckyDrawMsgPrefix) {
        this.luckyDrawMsgPrefix = luckyDrawMsgPrefix;
    }

    public String getLuckyDrawMsgObject() {
        return luckyDrawMsgObject;
    }

    public void setLuckyDrawMsgObject(String luckyDrawMsgObject) {
        this.luckyDrawMsgObject = luckyDrawMsgObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.userTicketsAlltime);
        dest.writeValue(this.userTicketsCurrentPeriod);
        dest.writeString(this.lastWinnerPrefix);
        dest.writeString(this.lastWinnerObject);
        dest.writeParcelable(this.currency, flags);
        dest.writeValue(this.totalPrice);
        dest.writeParcelable(this.prizes, flags);
        dest.writeValue(this.luckyDrawExpiredDate);
        dest.writeString(this.luckyDrawMsgPrefix);
        dest.writeString(this.luckyDrawMsgObject);
    }

    public LuckyDrawScreen() {
    }

    protected LuckyDrawScreen(Parcel in) {
        this.userTicketsAlltime = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userTicketsCurrentPeriod = (Integer) in.readValue(Integer.class.getClassLoader());
        this.lastWinnerPrefix = in.readString();
        this.lastWinnerObject = in.readString();
        this.currency = in.readParcelable(Currency.class.getClassLoader());
        this.totalPrice = (Integer) in.readValue(Integer.class.getClassLoader());
        this.prizes = in.readParcelable(Prizes.class.getClassLoader());
        this.luckyDrawExpiredDate = (Integer) in.readValue(Integer.class.getClassLoader());
        this.luckyDrawMsgPrefix = in.readString();
        this.luckyDrawMsgObject = in.readString();
    }

    public static final Creator<LuckyDrawScreen> CREATOR = new Creator<LuckyDrawScreen>() {
        @Override
        public LuckyDrawScreen createFromParcel(Parcel source) {
            return new LuckyDrawScreen(source);
        }

        @Override
        public LuckyDrawScreen[] newArray(int size) {
            return new LuckyDrawScreen[size];
        }
    };
}
