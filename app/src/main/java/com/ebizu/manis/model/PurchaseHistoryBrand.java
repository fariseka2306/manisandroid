package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public class PurchaseHistoryBrand implements Parcelable {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("count")
    private int count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.count);
    }

    public PurchaseHistoryBrand() {
    }

    protected PurchaseHistoryBrand(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.count = in.readInt();
    }

    public static final Creator<PurchaseHistoryBrand> CREATOR = new Creator<PurchaseHistoryBrand>() {
        @Override
        public PurchaseHistoryBrand createFromParcel(Parcel source) {
            return new PurchaseHistoryBrand(source);
        }

        @Override
        public PurchaseHistoryBrand[] newArray(int size) {
            return new PurchaseHistoryBrand[size];
        }
    };
}
