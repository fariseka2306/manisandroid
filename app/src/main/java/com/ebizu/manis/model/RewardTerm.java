package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 9/23/17.
 */

public class RewardTerm implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("text")
    @Expose
    private String text;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.text);
    }

    public RewardTerm() {
    }

    protected RewardTerm(Parcel in) {
        this.id = in.readString();
        this.text = in.readString();
    }

    public static final Creator<RewardTerm> CREATOR = new Creator<RewardTerm>() {
        @Override
        public RewardTerm createFromParcel(Parcel source) {
            return new RewardTerm(source);
        }

        @Override
        public RewardTerm[] newArray(int size) {
            return new RewardTerm[size];
        }
    };
}
