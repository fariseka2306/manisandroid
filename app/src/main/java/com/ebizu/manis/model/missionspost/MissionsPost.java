package com.ebizu.manis.model.missionspost;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/12/17.
 */

public class MissionsPost {

    @SerializedName("incentive")
    @Expose
    private Incentive incentive;
    @SerializedName("description")
    @Expose
    private String description;

    public Incentive getIncentive() {
        return incentive;
    }

    public String getDescription() {
        return description;
    }
}
