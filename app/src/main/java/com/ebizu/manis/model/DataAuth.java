package com.ebizu.manis.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 16/07/2017.
 */

public class DataAuth {

    @SerializedName("device")
    Device device;

    public void setDataDevice(Device device) {
        this.device = device;
    }
}
