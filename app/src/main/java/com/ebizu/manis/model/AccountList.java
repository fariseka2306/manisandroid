package com.ebizu.manis.model;

/**
 * Created by abizu-alvio on 7/5/2017.
 */

public class AccountList {

    private int name;
    private int thumbnail;

    public AccountList (int name, int thumbnail) {
        this.name = name;
        this.thumbnail = thumbnail;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}
