package com.ebizu.manis.model.saved;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 8/21/17.
 */

public class Category {

    @SerializedName("parent")
    @Expose
    private Integer parent;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("assets")
    @Expose
    private Assets assets;

    public Integer getParent() {
        return parent;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIcon() {
        return icon;
    }

    public Assets getAssets() {
        return assets;
    }
}
