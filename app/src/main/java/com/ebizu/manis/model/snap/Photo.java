package com.ebizu.manis.model.snap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 12/7/17.
 */

public class Photo {

    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("ext")
    @Expose
    private Boolean ext;
    @SerializedName("mime")
    @Expose
    private String mime;

    public Photo() {
        this.ext = true;
        this.mime = "image/jpeg";
    }

    public void setFile(String file) {
        this.file = file;
    }

}
