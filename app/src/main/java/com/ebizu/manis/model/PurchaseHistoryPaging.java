package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public class PurchaseHistoryPaging implements Parcelable {

    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("pageCount")
    @Expose
    private Integer pageCount;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("data")
    @Expose
    private List<PurchaseHistory> purchaseHistories;
    @SerializedName("more")
    @Expose
    private Boolean more;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("page")
    @Expose
    private Integer page;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<PurchaseHistory> getPurchaseHistories() {
        return purchaseHistories;
    }

    public void setPurchaseHistories(List<PurchaseHistory> purchaseHistories) {
        this.purchaseHistories = purchaseHistories;
    }

    public Boolean getMore() {
        return more;
    }

    public void setMore(Boolean more) {
        this.more = more;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.total);
        dest.writeValue(this.pageCount);
        dest.writeValue(this.size);
        dest.writeTypedList(this.purchaseHistories);
        dest.writeValue(this.more);
        dest.writeValue(this.count);
        dest.writeValue(this.page);
    }

    public PurchaseHistoryPaging() {
    }

    protected PurchaseHistoryPaging(Parcel in) {
        this.total = (Integer) in.readValue(Integer.class.getClassLoader());
        this.pageCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.size = (Integer) in.readValue(Integer.class.getClassLoader());
        this.purchaseHistories = in.createTypedArrayList(PurchaseHistory.CREATOR);
        this.more = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.count = (Integer) in.readValue(Integer.class.getClassLoader());
        this.page = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<PurchaseHistoryPaging> CREATOR = new Creator<PurchaseHistoryPaging>() {
        @Override
        public PurchaseHistoryPaging createFromParcel(Parcel source) {
            return new PurchaseHistoryPaging(source);
        }

        @Override
        public PurchaseHistoryPaging[] newArray(int size) {
            return new PurchaseHistoryPaging[size];
        }
    };
}
