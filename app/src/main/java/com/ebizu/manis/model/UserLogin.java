package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 14/07/2017.
 */

public class UserLogin {
    @SerializedName("acc_id")
    @Expose
    private int accId;
    @SerializedName("acc_facebook_id")
    @Expose
    private long accFacebookId;
    @SerializedName("acc_facebook_email")
    @Expose
    private String accFacebookEmail;
    @SerializedName("acc_google_id")
    @Expose
    private String accGoogleId;
    @SerializedName("acc_google_email")
    @Expose
    private String accGoogleEmail;
    @SerializedName("acc_screen_name")
    @Expose
    private String accScreenName;
    @SerializedName("acc_cty_id")
    @Expose
    private String accCountry;
    @SerializedName("acc_photo")
    @Expose
    private String accPhoto;
    @SerializedName("acc_last_login")
    @Expose
    private long accLastLogin;
    @SerializedName("acc_device_id")
    @Expose
    private String accDeviceId;
    @SerializedName("acc_status")
    @Expose
    private int accStatus;
    @SerializedName("tmz_name")
    @Expose
    private String tmzName;
    @SerializedName("acc_gender")
    @Expose
    private String accGender;
    @SerializedName("acc_msisdn")
    @Expose
    private String accMsisdn;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("currency")
    @Expose
    private Currency currency;
    @SerializedName("acc_birthdate")
    @Expose
    private long accBirthdate;
    @SerializedName("acc_address")
    @Expose
    private String accAddress;
    @SerializedName("acc_otp_status")
    @Expose
    private long accOtpStatus;
    @SerializedName("first_login")
    @Expose
    private boolean firstLogin;
    @SerializedName("point")
    @Expose
    private long point;
    @SerializedName("acc_created_datetime")
    @Expose
    private long accCreatedDateTime;

    public int getAccId() {
        return accId;
    }

    public long getAccFacebookId() {
        return accFacebookId;
    }

    public String getAccFacebookEmail() {
        return accFacebookEmail;
    }

    public String getAccGoogleId() {
        return accGoogleId;
    }

    public String getAccGoogleEmail() {
        return accGoogleEmail;
    }

    public String getAccScreenName() {
        return accScreenName;
    }

    public String getAccCountry() {
        return accCountry;
    }

    public String getAccPhoto() {
        return accPhoto;
    }

    public long getAccLastLogin() {
        return accLastLogin;
    }

    public String getAccDeviceId() {
        return accDeviceId;
    }

    public int getAccStatus() {
        return accStatus;
    }

    public String getTmzName() {
        return tmzName;
    }

    public String getAccGender() {
        return accGender;
    }

    public String getAccMsisdn() {
        return accMsisdn;
    }

    public String getToken() {
        return token;
    }

    public Currency getCurrency() {
        return currency;
    }

    public long getAccBirthdate() {
        return accBirthdate;
    }

    public String getAccAddress() {
        return accAddress;
    }

    public long getAccOtpStatus() {
        return accOtpStatus;
    }

    public boolean isFirstLogin() {
        return firstLogin;
    }

    public long getPoint() {
        return point;
    }

    public long getAccCreatedDateTime() {
        return accCreatedDateTime;
    }
}
