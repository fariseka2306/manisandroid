package com.ebizu.manis.model;

/**
 * Created by firef on 7/21/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Avaibility implements Parcelable {

    @SerializedName("today")
    @Expose
    private Today today;
    @SerializedName("daily")
    @Expose
    private List<Daily> daily = null;

    public Today getToday() {
        return today;
    }

    public void setToday(Today today) {
        this.today = today;
    }

    public List<Daily> getDaily() {
        return daily;
    }

    public void setDaily(List<Daily> daily) {
        this.daily = daily;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.today, flags);
        dest.writeTypedList(this.daily);
    }

    public Avaibility() {
    }

    protected Avaibility(Parcel in) {
        this.today = in.readParcelable(Today.class.getClassLoader());
        this.daily = in.createTypedArrayList(Daily.CREATOR);
    }

    public static final Creator<Avaibility> CREATOR = new Creator<Avaibility>() {
        @Override
        public Avaibility createFromParcel(Parcel source) {
            return new Avaibility(source);
        }

        @Override
        public Avaibility[] newArray(int size) {
            return new Avaibility[size];
        }
    };
}