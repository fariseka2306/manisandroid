package com.ebizu.manis.model.purchasevoucher;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrifashbir on 16/11/17.
 */

public class ShoppingCart implements Parcelable {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("voucherName")
    @Expose
    private String voucherName;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("qty")
    @Expose
    private int quantity;
    @SerializedName("vouchers")
    @Expose
    private List<RewardVoucher> vouchers = new ArrayList<>();

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public void setVoucherName(String voucherName) {
        this.voucherName = voucherName;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<RewardVoucher> getVouchers() {
        return vouchers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.orderId);
        dest.writeString(this.voucherName);
        dest.writeString(this.currency);
        dest.writeString(this.amount);
        dest.writeString(this.total);
        dest.writeInt(this.quantity);
        dest.writeTypedList(this.vouchers);
    }

    public ShoppingCart() {
    }

    protected ShoppingCart(Parcel in) {
        this.orderId = in.readString();
        this.voucherName = in.readString();
        this.currency = in.readString();
        this.amount = in.readString();
        this.total = in.readString();
        this.quantity = in.readInt();
        this.vouchers = in.createTypedArrayList(RewardVoucher.CREATOR);
    }

    public static final Creator<ShoppingCart> CREATOR = new Creator<ShoppingCart>() {
        @Override
        public ShoppingCart createFromParcel(Parcel source) {
            return new ShoppingCart(source);
        }

        @Override
        public ShoppingCart[] newArray(int size) {
            return new ShoppingCart[size];
        }
    };
}
