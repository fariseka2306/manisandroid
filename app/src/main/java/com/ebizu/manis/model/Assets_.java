package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firef on 7/7/2017.
 */

public class Assets_ implements Parcelable {

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("banner")
    @Expose
    private String banner;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.photo);
        dest.writeString(this.banner);
    }

    public Assets_() {
    }

    protected Assets_(Parcel in) {
        this.photo = in.readString();
        this.banner = in.readString();
    }

    public static final Parcelable.Creator<Assets_> CREATOR = new Parcelable.Creator<Assets_>() {
        @Override
        public Assets_ createFromParcel(Parcel source) {
            return new Assets_(source);
        }

        @Override
        public Assets_[] newArray(int size) {
            return new Assets_[size];
        }
    };
}
