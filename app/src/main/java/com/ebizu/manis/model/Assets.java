package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firef on 7/7/2017.
 */

public class Assets implements Parcelable {

    @SerializedName("icon")
    @Expose
    private String icon;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("banner")
    @Expose
    private String banner;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.icon);
        dest.writeString(this.photo);
        dest.writeString(this.banner);
    }

    public Assets() {
    }

    protected Assets(Parcel in) {
        this.icon = in.readString();
        this.photo = in.readString();
        this.banner = in.readString();
    }

    public static final Parcelable.Creator<Assets> CREATOR = new Parcelable.Creator<Assets>() {
        @Override
        public Assets createFromParcel(Parcel source) {
            return new Assets(source);
        }

        @Override
        public Assets[] newArray(int size) {
            return new Assets[size];
        }
    };
}
