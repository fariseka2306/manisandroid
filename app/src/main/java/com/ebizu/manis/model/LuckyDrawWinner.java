package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 8/6/17.
 */

public class LuckyDrawWinner implements Parcelable {

    @SerializedName("acc_id")
    @Expose
    private int accId;
    @SerializedName("acc_screen_name")
    @Expose
    private String accScreenName;
    @SerializedName("acc_photo")
    @Expose
    private String accPhoto;
    @SerializedName("prize_prefix")
    @Expose
    private String prizePrefix;
    @SerializedName("prize_object")
    @Expose
    private String prizeObject;
    @SerializedName("d_day")
    @Expose
    private int dDay;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    public int getAccId() {
        return accId;
    }

    public void setAccId(int accId) {
        this.accId = accId;
    }

    public String getAccScreenName() {
        return accScreenName;
    }

    public void setAccScreenName(String accScreenName) {
        this.accScreenName = accScreenName;
    }

    public String getAccPhoto() {
        return accPhoto;
    }

    public void setAccPhoto(String accPhoto) {
        this.accPhoto = accPhoto;
    }

    public String getPrizePrefix() {
        return prizePrefix;
    }

    public void setPrizePrefix(String prizePrefix) {
        this.prizePrefix = prizePrefix;
    }

    public String getPrizeObject() {
        return prizeObject;
    }

    public void setPrizeObject(String prizeObject) {
        this.prizeObject = prizeObject;
    }

    public int getdDay() {
        return dDay;
    }

    public void setdDay(int dDay) {
        this.dDay = dDay;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.accId);
        dest.writeString(this.accScreenName);
        dest.writeString(this.accPhoto);
        dest.writeString(this.prizePrefix);
        dest.writeString(this.prizeObject);
        dest.writeInt(this.dDay);
        dest.writeString(this.timestamp);
    }

    public LuckyDrawWinner() {
    }

    protected LuckyDrawWinner(Parcel in) {
        this.accId = in.readInt();
        this.accScreenName = in.readString();
        this.accPhoto = in.readString();
        this.prizePrefix = in.readString();
        this.prizeObject = in.readString();
        this.dDay = in.readInt();
        this.timestamp = in.readString();
    }

    public static final Creator<LuckyDrawWinner> CREATOR = new Creator<LuckyDrawWinner>() {
        @Override
        public LuckyDrawWinner createFromParcel(Parcel source) {
            return new LuckyDrawWinner(source);
        }

        @Override
        public LuckyDrawWinner[] newArray(int size) {
            return new LuckyDrawWinner[size];
        }
    };
}
