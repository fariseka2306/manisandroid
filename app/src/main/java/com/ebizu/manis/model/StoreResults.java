package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firef on 7/7/2017.
 */

public class StoreResults {

    @SerializedName("more")
    @Expose
    private Boolean more;

    @SerializedName("results")
    @Expose
    private ArrayList<Store> stores;

    public Boolean getMore() {
        return more;
    }

    public void setMore(Boolean more) {
        this.more = more;
    }

    public ArrayList<Store> getStores() {
        return stores;
    }

    public void setStores(ArrayList<Store> stores) {
        this.stores = stores;
    }

}