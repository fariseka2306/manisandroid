package com.ebizu.manis.model.snap;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.manis.model.Receipt;
import com.ebizu.manis.model.Store;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 7/5/17.
 */

public class SnapData implements Parcelable {

    public static final int STATUS_PENDING = 0;
    public static final int STATUS_APPROVED = 1;
    public static final int STATUS_REJECTED = 2;
    public static final int STATUS_PENDING_APPEAL = 3;
    public static final int STATUS_ALL = 4;
    public static final int ORDER_DATE = 0;
    public static final int ORDER_AMOUNT = 1;
    public static final int SORT_ASC = 0;
    public static final int SORT_DESC = 1;

    @SerializedName("id")
    @Expose
    public long id;
    @SerializedName("appealable")
    @Expose
    public boolean appealable;
    @SerializedName("receipt")
    @Expose
    public Receipt receipt;
    @SerializedName("store")
    @Expose
    public Store store;
    @SerializedName("point")
    @Expose
    public long point;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("remark")
    @Expose
    public String remark;
    @SerializedName("uploaded")
    @Expose
    public long uploaded;
    @SerializedName("status_type")
    @Expose
    public int statusType;

    public SnapData() {
    }

    public static int getStatusPending() {
        return STATUS_PENDING;
    }

    public static int getStatusApproved() {
        return STATUS_APPROVED;
    }

    public static int getStatusRejected() {
        return STATUS_REJECTED;
    }

    public static int getStatusPendingAppeal() {
        return STATUS_PENDING_APPEAL;
    }

    public static int getStatusAll() {
        return STATUS_ALL;
    }

    public static int getOrderDate() {
        return ORDER_DATE;
    }

    public static int getOrderAmount() {
        return ORDER_AMOUNT;
    }

    public static int getSortAsc() {
        return SORT_ASC;
    }

    public static int getSortDesc() {
        return SORT_DESC;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAppealable() {
        return appealable;
    }

    public void setAppealable(boolean appealable) {
        this.appealable = appealable;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public long getPoint() {
        return point;
    }

    public void setPoint(long point) {
        this.point = point;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public long getUploaded() {
        return uploaded;
    }

    public void setUploaded(long uploaded) {
        this.uploaded = uploaded;
    }

    public int getStatusType() {
        return statusType;
    }

    public void setStatusType(int statusType) {
        this.statusType = statusType;
    }

    public static Creator<SnapData> getCREATOR() {
        return CREATOR;
    }

    protected SnapData(Parcel in) {
        this.id = in.readLong();
        this.appealable = in.readByte() != 0;
        this.receipt = in.readParcelable(Receipt.class.getClassLoader());
        this.store = in.readParcelable(Store.class.getClassLoader());
        this.point = in.readLong();
        this.status = in.readString();
        this.remark = in.readString();
        this.uploaded = in.readLong();
        this.statusType = in.readInt();
    }

    public static final Creator<SnapData> CREATOR = new Creator<SnapData>() {
        @Override
        public SnapData createFromParcel(Parcel in) {
            return new SnapData(in);
        }

        @Override
        public SnapData[] newArray(int size) {
            return new SnapData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeByte(this.appealable ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.receipt, flags);
        dest.writeParcelable(this.store, flags);
        dest.writeLong(this.point);
        dest.writeString(this.status);
        dest.writeString(this.remark);
        dest.writeLong(this.uploaded);
        dest.writeInt(this.statusType);
    }
}
