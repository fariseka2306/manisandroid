package com.ebizu.manis.model.tracker;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.litepal.annotation.Column;

/**
 * Created by ebizu on 12/13/17.
 */

public class ManisTracker {

    @SerializedName("localDatetime")
    @Expose
    private String localDatetime;
    @SerializedName("ipAddress")
    @Expose
    private String ipAddress;
    @SerializedName("accountId")
    @Expose
    private String accountId;
    @SerializedName("originPage")
    @Expose
    private String originPage;
    @SerializedName("lastPage")
    @Expose
    private String lastPage;
    @SerializedName("currentPage")
    @Expose
    private String currentPage;
    @SerializedName("component")
    @Expose
    private String component;
    @SerializedName("subFeature")
    @Expose
    private String subFeature;
    @SerializedName("identifierNumber")
    @Expose
    private String identifierNumber;
    @SerializedName("identifierName")
    @Expose
    private String identifierName;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("countryId")
    @Expose
    private String countryId;

    public void setLocalDatetime(String localDatetime) {
        this.localDatetime = localDatetime;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setOriginPage(String originPage) {
        this.originPage = originPage;
    }

    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public void setSubFeature(String subFeature) {
        this.subFeature = subFeature;
    }

    public void setIdentifierNumber(String identifierNumber) {
        this.identifierNumber = identifierNumber;
    }

    public void setIdentifierName(String identifierName) {
        this.identifierName = identifierName;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }
}
