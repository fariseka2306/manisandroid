package com.ebizu.manis.model.snap;

import com.ebizu.manis.model.Assets_;
import com.ebizu.manis.model.Category;
import com.ebizu.manis.model.Coordinate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public class SnapStore {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("assets")
    @Expose
    private Assets_ assets;
    @SerializedName("coordinate")
    @Expose
    private Coordinate coordinate;
    @SerializedName("premium")
    @Expose
    private Boolean premium;
    @SerializedName("multiplier")
    @Expose
    private Integer multiplier;
    @SerializedName("merchant_tier")
    @Expose
    private String merchantTier;

    public String getMerchantTier() {
        return merchantTier;
    }

    public void setMerchantTier(String merchantTier) {
        this.merchantTier = merchantTier;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Assets_ getAssets() {
        return assets;
    }

    public void setAssets(Assets_ assets) {
        this.assets = assets;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Boolean getPremium() {
        return premium;
    }

    public void setPremium(Boolean premium) {
        this.premium = premium;
    }

    public Integer getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }
}
