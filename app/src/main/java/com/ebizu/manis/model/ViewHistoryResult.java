package com.ebizu.manis.model;

import com.ebizu.manis.model.snap.SnapData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Raden on 7/24/17.
 */

public class ViewHistoryResult {

    @SerializedName("more")
    @Expose
    private Boolean more;

    @SerializedName("results")
    @Expose
    private ArrayList<SnapData> snapDatas;

    public Boolean getMore() {
        return more;
    }

    public void setMore(Boolean more) {
        this.more = more;
    }

    public ArrayList<SnapData> getSnapDatas() {
        return snapDatas;
    }

    public void setSnapDatas(ArrayList<SnapData> snapDatas) {
        this.snapDatas = snapDatas;
    }
}
