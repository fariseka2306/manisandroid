package com.ebizu.manis.model;

import com.google.gson.annotations.SerializedName;

import io.intercom.com.google.gson.annotations.Expose;

/**
 * Created by Raden on 7/24/17.
 */

public class ViewHistoryDetail {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("appealable")
    @Expose
    private Boolean appealable;
    @SerializedName("receipt")
    @Expose
    private Receipt receipt;
    @SerializedName("store")
    @Expose
    private Store store;
    @SerializedName("point")
    @Expose
    private Integer point;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_type")
    @Expose
    private Integer statusType;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("uploaded")
    @Expose
    private Integer uploaded;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getAppealable() {
        return appealable;
    }

    public void setAppealable(Boolean appealable) {
        this.appealable = appealable;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusType() {
        return statusType;
    }

    public void setStatusType(Integer statusType) {
        this.statusType = statusType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getUploaded() {
        return uploaded;
    }

    public void setUploaded(Integer uploaded) {
        this.uploaded = uploaded;
    }
}
