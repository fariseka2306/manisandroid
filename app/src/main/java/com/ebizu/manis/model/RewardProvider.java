package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Halim on 9/23/17.
 */

public class RewardProvider implements Parcelable {

    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lon")
    @Expose
    private double lon;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category")
    @Expose
    private List<RewardCategory> rewardCategories;
    @SerializedName("lat")
    @Expose
    private double lat;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<RewardCategory> getRewardCategories() {
        return rewardCategories;
    }

    public void setRewardCategories(List<RewardCategory> rewardCategories) {
        this.rewardCategories = rewardCategories;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.image);
        dest.writeString(this.website);
        dest.writeString(this.address);
        dest.writeString(this.phone);
        dest.writeString(this.name);
        dest.writeDouble(this.lon);
        dest.writeString(this.id);
        dest.writeTypedList(this.rewardCategories);
        dest.writeDouble(this.lat);
    }

    public RewardProvider() {
    }

    protected RewardProvider(Parcel in) {
        this.image = in.readString();
        this.website = in.readString();
        this.address = in.readString();
        this.phone = in.readString();
        this.name = in.readString();
        this.lon = in.readDouble();
        this.id = in.readString();
        this.rewardCategories = in.createTypedArrayList(RewardCategory.CREATOR);
        this.lat = in.readDouble();
    }

    public static final Creator<RewardProvider> CREATOR = new Creator<RewardProvider>() {
        @Override
        public RewardProvider createFromParcel(Parcel source) {
            return new RewardProvider(source);
        }

        @Override
        public RewardProvider[] newArray(int size) {
            return new RewardProvider[size];
        }
    };
}
