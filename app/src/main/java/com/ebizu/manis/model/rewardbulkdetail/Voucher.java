package com.ebizu.manis.model.rewardbulkdetail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 15/11/17.
 */

public class Voucher implements Parcelable{

    @SerializedName("voucherId")
    @Expose
    private String voucherId = "";
    @SerializedName("voucherName")
    @Expose
    private String voucherName = "";
    @SerializedName("qty")
    @Expose
    private int qty = 0;

    protected Voucher(Parcel in) {
        voucherId = in.readString();
        voucherName = in.readString();
        qty = in.readInt();
    }

    public static final Creator<Voucher> CREATOR = new Creator<Voucher>() {
        @Override
        public Voucher createFromParcel(Parcel in) {
            return new Voucher(in);
        }

        @Override
        public Voucher[] newArray(int size) {
            return new Voucher[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(voucherId);
        dest.writeString(voucherName);
        dest.writeInt(qty);
    }

    public String getVoucherId() {
        return voucherId;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public int getQty() {
        return qty;
    }

    public static Creator<Voucher> getCREATOR() {
        return CREATOR;
    }
}
