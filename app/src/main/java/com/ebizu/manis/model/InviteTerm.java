package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abizu-alvio on 8/14/2017.
 */

public class InviteTerm {
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("terms")
    @Expose
    private ArrayList<String> terms = null;

    public String getDescription() {
        return description;
    }

    public ArrayList<String> getTerms() {
        return terms;
    }
}
