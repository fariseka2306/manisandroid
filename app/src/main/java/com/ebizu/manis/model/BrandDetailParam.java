package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 7/21/17.
 */

public class BrandDetailParam {

    @SerializedName("keyword")
    @Expose
    public String keyword;

    @SerializedName("brand")
    @Expose
    public int brand;

    @SerializedName("page")
    @Expose
    public int page;

    @SerializedName("size")
    @Expose
    public int size;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getBrand() {
        return brand;
    }

    public void setBrand(int brand) {
        this.brand = brand;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
