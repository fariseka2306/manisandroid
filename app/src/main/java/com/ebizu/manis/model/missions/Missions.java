package com.ebizu.manis.model.missions;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class Missions implements Parcelable {

    @SerializedName("missionId")
    @Expose
    private int missionId = 0;
    @SerializedName("missionTypeId")
    @Expose
    private int missionTypeId = 0;
    @SerializedName("missionName")
    @Expose
    private String missionName = "";
    @SerializedName("voucherId")
    @Expose
    private String voucherId = "";
    @SerializedName("missionTypeName")
    @Expose
    private String missionTypeName = "";
    @SerializedName("missionDescription")
    @Expose
    private String missionDescription = "";
    @SerializedName("shareExperienceTypeId")
    @Expose
    private int shareExperienceTypeId = 0;
    @SerializedName("shareExperinceMinimumFriends")
    @Expose
    private int shareExperinceMinimumFriends = 0;
    @SerializedName("incentive")
    @Expose
    private Incentive incentive;
    @SerializedName("asset")
    @Expose
    private Asset asset;
    @SerializedName("tnc")
    @Expose
    private String tnc;
    @SerializedName("hashTag")
    @Expose
    private String hashTag;
    @SerializedName("missionStatus")
    @Expose
    private String missionStatus;
    @SerializedName("startDate")
    @Expose
    private int startDate = 0;
    @SerializedName("endDate")
    @Expose
    private int endDate = 0;
    @SerializedName("startDateDescription")
    @Expose
    private String startDateDescription;
    @SerializedName("endDateDescription")
    @Expose
    private String endDateDescription;

    protected Missions(Parcel in) {
        this.missionTypeName = in.readString();
        this.missionDescription = in.readString();
        this.missionName = in.readString();
        this.incentive = in.readParcelable(Incentive.class.getClassLoader());
        this.asset = in.readParcelable(Asset.class.getClassLoader());
        this.tnc = in.readString();
        this.hashTag = in.readString();
        this.missionStatus = in.readString();
        this.startDateDescription = in.readString();
        this.endDateDescription = in.readString();
        this.missionId = in.readInt();
        this.missionTypeId = in.readInt();
        this.shareExperienceTypeId = in.readInt();
        this.shareExperinceMinimumFriends = in.readInt();
        this.startDate = in.readInt();
        this.endDate = in.readInt();
        this.voucherId = in.readString();
    }

    public Missions() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.missionTypeName);
        dest.writeString(this.missionDescription);
        dest.writeString(this.missionName);
        dest.writeParcelable(this.incentive, flags);
        dest.writeParcelable(this.asset, flags);
        dest.writeString(this.tnc);
        dest.writeString(this.hashTag);
        dest.writeString(this.missionStatus);
        dest.writeString(this.startDateDescription);
        dest.writeString(this.endDateDescription);
        dest.writeInt(this.missionId);
        dest.writeInt(this.missionTypeId);
        dest.writeInt(this.shareExperienceTypeId);
        dest.writeInt(this.shareExperinceMinimumFriends);
        dest.writeInt(this.startDate);
        dest.writeInt(this.endDate);
        dest.writeString(this.voucherId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Missions> CREATOR = new Creator<Missions>() {
        @Override
        public Missions createFromParcel(Parcel in) {
            return new Missions(in);
        }

        @Override
        public Missions[] newArray(int size) {
            return new Missions[size];
        }
    };

    public String getMissionTypeName() {
        return missionTypeName;
    }

    public Integer getMissionId() {
        return missionId;
    }

    public Integer getMissionTypeId() {
        return missionTypeId;
    }

    public String getMissionName() {
        return missionName;
    }

    public int getShareExperienceTypeId() {
        return shareExperienceTypeId;
    }

    public int getShareExperinceMinimumFriends() {
        return shareExperinceMinimumFriends;
    }

    public Incentive getIncentive() {
        return incentive;
    }

    public Asset getAsset() {
        return asset;
    }

    public String getTnc() {
        return tnc;
    }

    public String getHashTag() {
        return hashTag;
    }

    public String getMissionStatus() {
        return missionStatus;
    }

    public Integer getStartDate() {
        return startDate;
    }

    public Integer getEndDate() {
        return endDate;
    }

    public String getStartDateDescription() {
        return startDateDescription;
    }

    public String getEndDateDescription() {
        return endDateDescription;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public String getMissionDescription() {
        return missionDescription;
    }
}
