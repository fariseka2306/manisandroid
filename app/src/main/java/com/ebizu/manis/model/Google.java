package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 17/07/2017.
 */

public class Google {

    @SerializedName("google_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("verified")
    @Expose
    private boolean verified;

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(int gender) {
        final int MALE = 0;
        final int FEMALE = 1;
        final int OTHER = 2;
        switch (gender) {
            case MALE: {
                this.gender = "male";
            }
            case FEMALE: {
                this.gender = "female";
            }
            case OTHER: {
                this.gender = "other";
            }
            default: {
                this.gender = "none";
            }
        }
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
