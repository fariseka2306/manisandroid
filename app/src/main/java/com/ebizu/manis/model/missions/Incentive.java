package com.ebizu.manis.model.missions;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class Incentive implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("form")
    @Expose
    private String form;
    @SerializedName("amount")
    @Expose
    private Integer amount;

    protected Incentive(Parcel in) {
        id = in.readString();
        form = in.readString();
        amount = in.readInt();
    }

    public static final Creator<Incentive> CREATOR = new Creator<Incentive>() {
        @Override
        public Incentive createFromParcel(Parcel in) {
            return new Incentive(in);
        }

        @Override
        public Incentive[] newArray(int size) {
            return new Incentive[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getForm() {
        return form;
    }

    public Integer getAmount() {
        return amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(form);
        dest.writeInt(amount);
    }
}
