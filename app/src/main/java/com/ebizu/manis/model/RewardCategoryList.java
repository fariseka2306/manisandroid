package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class RewardCategoryList implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("id")
    @Expose
    private String id;

    protected RewardCategoryList(Parcel in) {
        name = in.readString();
        if (in.readByte() == 0) {
            count = null;
        } else {
            count = in.readInt();
        }
        id = in.readString();
    }

    public static final Creator<RewardCategoryList> CREATOR = new Creator<RewardCategoryList>() {
        @Override
        public RewardCategoryList createFromParcel(Parcel in) {
            return new RewardCategoryList(in);
        }

        @Override
        public RewardCategoryList[] newArray(int size) {
            return new RewardCategoryList[size];
        }
    };

    public String getName() {
        return name;
    }

    public Integer getCount() {
        return count;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        if (count == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(count);
        }
        dest.writeString(id);
    }
}
