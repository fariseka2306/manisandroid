package com.ebizu.manis.model.tracker;

/**
 * Created by ebizu on 12/15/17.
 */

public class TrackerStandartRequest {

    private String originPage = "";
    private String currentPage = "";
    private String component = "";
    private String subFeature = "";
    private String identifierNumber = "";
    private String identifierName = "";

    public TrackerStandartRequest(String originPage, String currentPage, String component, String subFeature, String identifierNumber, String identifierName) {
        this.originPage = originPage;
        this.currentPage = currentPage;
        this.component = component;
        this.subFeature = subFeature;
        this.identifierNumber = identifierNumber;
        this.identifierName = identifierName;
    }

    public String getOriginPage() {
        return originPage;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public String getComponent() {
        return component;
    }

    public String getSubFeature() {
        return subFeature;
    }

    public String getIdentifierNumber() {
        return identifierNumber;
    }

    public String getIdentifierName() {
        return identifierName;
    }
}
