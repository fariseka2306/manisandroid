package com.ebizu.manis.model.missionspost;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/12/17.
 */

public class Incentive {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("form")
    @Expose
    private String form;
    @SerializedName("amount")
    @Expose
    private Integer amount;

    public String getId() {
        return id;
    }

    public String getForm() {
        return form;
    }

    public Integer getAmount() {
        return amount;
    }
}
