package com.ebizu.manis.model.shareexpeeriencecheckavailability;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/12/17.
 */

public class ShareExperienceCheckAvalaibility implements Parcelable{

    @SerializedName("availability")
    @Expose
    private int availability = 0;
    @SerializedName("description")
    @Expose
    private String description = "";


    protected ShareExperienceCheckAvalaibility(Parcel in) {
        description = in.readString();
        availability = in.readInt();
    }

    public static final Creator<ShareExperienceCheckAvalaibility> CREATOR = new Creator<ShareExperienceCheckAvalaibility>() {
        @Override
        public ShareExperienceCheckAvalaibility createFromParcel(Parcel in) {
            return new ShareExperienceCheckAvalaibility(in);
        }

        @Override
        public ShareExperienceCheckAvalaibility[] newArray(int size) {
            return new ShareExperienceCheckAvalaibility[size];
        }
    };

    public int getAvailability() {
        return availability;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(description);
        dest.writeInt(availability);
    }
}
