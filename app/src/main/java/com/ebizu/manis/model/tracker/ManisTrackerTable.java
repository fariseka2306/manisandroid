package com.ebizu.manis.model.tracker;

import org.litepal.annotation.Column;
import org.litepal.crud.DataSupport;

/**
 * Created by ebizu on 12/13/17.
 */

public class ManisTrackerTable extends DataSupport {

    @Column(unique = true, defaultValue = "unknown")
    private long id;

    @Column(defaultValue = "")
    private String localDatetime;
    @Column(defaultValue = "")
    private String ipAddress;
    @Column(defaultValue = "")
    private String accountId;
    @Column(defaultValue = "")
    private String originPage;
    @Column(defaultValue = "")
    private String lastPage;
    @Column(defaultValue = "")
    private String currentPage;
    @Column(defaultValue = "")
    private String component;
    @Column(defaultValue = "")
    private String subFeature;
    @Column(defaultValue = "")
    private String identifierNumber;
    @Column(defaultValue = "")
    private String identifierName;
    @Column(defaultValue = "")
    private String longitude;
    @Column(defaultValue = "")
    private String latitude;
    @Column(defaultValue = "")
    private String countryId;

    public long getId() {
        return id;
    }

    public String getLocalDatetime() {
        return localDatetime;
    }

    public void setLocalDatetime(String localDatetime) {
        this.localDatetime = localDatetime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOriginPage() {
        return originPage;
    }

    public void setOriginPage(String originPage) {
        this.originPage = originPage;
    }

    public String getLastPage() {
        return lastPage;
    }

    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getSubFeature() {
        return subFeature;
    }

    public void setSubFeature(String subFeature) {
        this.subFeature = subFeature;
    }

    public String getIdentifierNumber() {
        return identifierNumber;
    }

    public void setIdentifierNumber(String identifierNumber) {
        this.identifierNumber = identifierNumber;
    }

    public String getIdentifierName() {
        return identifierName;
    }

    public void setIdentifierName(String identifierName) {
        this.identifierName = identifierName;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    @Override
    public String toString() {
        return "ManisTrackerTable{" + "\n" +
                "id=" + id + "\n" +
                "localDatetime='" + localDatetime + "\n" +
                "ipAddress='" + ipAddress + "\n" +
                "accountId='" + accountId + "\n" +
                "originPage='" + originPage + "\n" +
                "lastPage='" + lastPage + "\n" +
                "currentPage='" + currentPage + "\n" +
                "component='" + component + "\n" +
                "subFeature='" + subFeature + "\n" +
                "identifierNumber='" + identifierNumber + "\n" +
                "identifierName='" + identifierName + "\n" +
                "longitude='" + longitude + "\n" +
                "latitude='" + latitude + "\n" +
                "countryId='" + countryId + "\n" +
                '}';
    }
}
