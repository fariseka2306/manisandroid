package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Halim on 8/6/17.
 */

public class LuckyDrawWinnerData implements Parcelable {

    @SerializedName("more")
    @Expose
    private boolean more;
    @SerializedName("winner_title")
    @Expose
    private String winnerTitle;
    @SerializedName("winners")
    @Expose
    private List<LuckyDrawWinner> luckyDrawWinners;

    public boolean isMore() {
        return more;
    }

    public void setMore(boolean more) {
        this.more = more;
    }

    public String getWinnerTitle() {
        return winnerTitle;
    }

    public void setWinnerTitle(String winnerTitle) {
        this.winnerTitle = winnerTitle;
    }

    public List<LuckyDrawWinner> getLuckyDrawWinners() {
        return luckyDrawWinners;
    }

    public void setLuckyDrawWinners(List<LuckyDrawWinner> luckyDrawWinners) {
        this.luckyDrawWinners = luckyDrawWinners;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.more ? (byte) 1 : (byte) 0);
        dest.writeString(this.winnerTitle);
        dest.writeList(this.luckyDrawWinners);
    }

    public LuckyDrawWinnerData() {
    }

    protected LuckyDrawWinnerData(Parcel in) {
        this.more = in.readByte() != 0;
        this.winnerTitle = in.readString();
        this.luckyDrawWinners = new ArrayList<LuckyDrawWinner>();
        in.readList(this.luckyDrawWinners, LuckyDrawWinner.class.getClassLoader());
    }

    public static final Creator<LuckyDrawWinnerData> CREATOR = new Creator<LuckyDrawWinnerData>() {
        @Override
        public LuckyDrawWinnerData createFromParcel(Parcel source) {
            return new LuckyDrawWinnerData(source);
        }

        @Override
        public LuckyDrawWinnerData[] newArray(int size) {
            return new LuckyDrawWinnerData[size];
        }
    };
}
