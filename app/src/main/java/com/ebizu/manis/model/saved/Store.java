package com.ebizu.manis.model.saved;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 8/21/17.
 */

public class Store {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("assets")
    @Expose
    private Assets_ assets;
    @SerializedName("premium")
    @Expose
    private Boolean premium;
    @SerializedName("multiplier")
    @Expose
    private Integer multiplier;


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Category getCategory() {
        return category;
    }

    public Assets_ getAssets() {
        return assets;
    }

    public Boolean getPremium() {
        return premium;
    }

    public Integer getMultiplier() {
        return multiplier;
    }
}
