package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by halim_ebizu on 8/22/17.
 */

public class Offer implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("claim_disabled")
    @Expose
    private Boolean claimDisabled;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("date")
    @Expose
    private OfferDate offerDate;
    @SerializedName("tos")
    @Expose
    private String[] tos = new String[]{};
    @SerializedName("saved")
    @Expose
    private Boolean saved;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getClaimDisabled() {
        return claimDisabled;
    }

    public void setClaimDisabled(Boolean claimDisabled) {
        this.claimDisabled = claimDisabled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public OfferDate getOfferDate() {
        return offerDate;
    }

    public void setOfferDate(OfferDate offerDate) {
        this.offerDate = offerDate;
    }

    public String[] getTos() {
        return tos;
    }

    public void setTos(String[] tos) {
        this.tos = tos;
    }

    public Boolean getSaved() {
        return saved;
    }

    public void setSaved(Boolean saved) {
        this.saved = saved;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeValue(this.claimDisabled);
        dest.writeString(this.description);
        dest.writeString(this.photo);
        dest.writeParcelable(this.offerDate, flags);
        dest.writeStringArray(this.tos);
        dest.writeValue(this.saved);
    }

    public Offer() {
    }

    protected Offer(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.claimDisabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.description = in.readString();
        this.photo = in.readString();
        this.offerDate = in.readParcelable(OfferDate.class.getClassLoader());
        this.tos = in.createStringArray();
        this.saved = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<Offer> CREATOR = new Creator<Offer>() {
        @Override
        public Offer createFromParcel(Parcel source) {
            return new Offer(source);
        }

        @Override
        public Offer[] newArray(int size) {
            return new Offer[size];
        }
    };
}
