package com.ebizu.manis.model.saved;

import com.ebizu.manis.model.*;
import com.ebizu.manis.model.saved.Offer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mac on 8/21/17.
 */

public class Saved {

    @SerializedName("offers")
    @Expose
    private List<Offer> offers = null;
    @SerializedName("rewards")
    @Expose
    private List<com.ebizu.manis.model.Reward> rewards = null;

    public List<Offer> getOffers() {
        return offers;
    }

    public List<com.ebizu.manis.model.Reward> getRewards() {
        return rewards;
    }
}
