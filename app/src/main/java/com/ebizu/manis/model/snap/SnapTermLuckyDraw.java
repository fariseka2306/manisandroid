package com.ebizu.manis.model.snap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/12/17.
 */

public class SnapTermLuckyDraw extends SnapTerm {

    @SerializedName("short_description")
    @Expose
    private String shortDescription;

    public String getShortDescription() {
        return shortDescription;
    }

    @Override
    public String toString() {
        return "SnapTermLuckyDraw{" +
                "shortDescription='" + shortDescription + '\'' +
                '}';
    }
}
