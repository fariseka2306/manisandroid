package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 9/23/17.
 */

public class RewardInput implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("required")
    @Expose
    private String required;
    @SerializedName("filledByApplication")
    @Expose
    private String filledByApplication;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getFilledByApplication() {
        return filledByApplication;
    }

    public void setFilledByApplication(String filledByApplication) {
        this.filledByApplication = filledByApplication;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.text);
        dest.writeString(this.type);
        dest.writeString(this.required);
        dest.writeString(this.filledByApplication);
    }

    public RewardInput() {
    }

    protected RewardInput(Parcel in) {
        this.id = in.readString();
        this.text = in.readString();
        this.type = in.readString();
        this.required = in.readString();
        this.filledByApplication = in.readString();
    }

    public static final Creator<RewardInput> CREATOR = new Creator<RewardInput>() {
        @Override
        public RewardInput createFromParcel(Parcel source) {
            return new RewardInput(source);
        }

        @Override
        public RewardInput[] newArray(int size) {
            return new RewardInput[size];
        }
    };
}
