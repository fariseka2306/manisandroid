package com.ebizu.manis.model.missions;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class Asset implements Parcelable{

    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("banner")
    @Expose
    private String banner;
    @SerializedName("iconMission")
    @Expose
    private String iconMission;

    protected Asset(Parcel in) {
        this.logo = in.readString();
        this.icon = in.readString();
        this.banner = in.readString();
        this.iconMission = in.readString();
    }

    public static final Creator<Asset> CREATOR = new Creator<Asset>() {
        @Override
        public Asset createFromParcel(Parcel in) {
            return new Asset(in);
        }

        @Override
        public Asset[] newArray(int size) {
            return new Asset[size];
        }
    };

    public String getLogo() {
        return logo;
    }

    public String getIcon() {
        return icon;
    }

    public String getBanner() {
        return banner;
    }

    public String getIconMission() {
        return iconMission;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.logo);
        dest.writeString(this.icon);
        dest.writeString(this.banner);
        dest.writeString(this.iconMission);
    }
}
