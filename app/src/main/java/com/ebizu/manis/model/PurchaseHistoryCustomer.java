package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public class PurchaseHistoryCustomer implements Parcelable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("shipmentInfo")
    @Expose
    private String shipmentInfo;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getShipmentInfo() {
        return shipmentInfo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.shipmentInfo);
    }

    public PurchaseHistoryCustomer() {
    }

    protected PurchaseHistoryCustomer(Parcel in) {
        this.name = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.shipmentInfo = in.readString();
    }

    public static final Creator<PurchaseHistoryCustomer> CREATOR = new Creator<PurchaseHistoryCustomer>() {
        @Override
        public PurchaseHistoryCustomer createFromParcel(Parcel source) {
            return new PurchaseHistoryCustomer(source);
        }

        @Override
        public PurchaseHistoryCustomer[] newArray(int size) {
            return new PurchaseHistoryCustomer[size];
        }
    };
}
