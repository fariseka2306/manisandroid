package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 8/22/17.
 */

public class Versioning {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("version_platform")
    @Expose
    private String versionPlatform;
    @SerializedName("version_code")
    @Expose
    private String versionCode;
    @SerializedName("version_name")
    @Expose
    private String versionName;
    @SerializedName("version_status")
    @Expose
    private String versionStatus;
    @SerializedName("version_message")
    @Expose
    private String versionMessage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersionPlatform() {
        return versionPlatform;
    }

    public void setVersionPlatform(String versionPlatform) {
        this.versionPlatform = versionPlatform;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getVersionStatus() {
        return versionStatus;
    }

    public void setVersionStatus(String versionStatus) {
        this.versionStatus = versionStatus;
    }

    public String getVersionMessage() {
        return versionMessage;
    }

    public void setVersionMessage(String versionMessage) {
        this.versionMessage = versionMessage;
    }

    @Override
    public String toString() {
        return "Versioning{" +
                "id='" + id + '\'' +
                ", versionPlatform='" + versionPlatform + '\'' +
                ", versionCode='" + versionCode + '\'' +
                ", versionName='" + versionName + '\'' +
                ", versionStatus='" + versionStatus + '\'' +
                ", versionMessage='" + versionMessage + '\'' +
                '}';
    }
}
