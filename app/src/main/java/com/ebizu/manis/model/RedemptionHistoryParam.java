package com.ebizu.manis.model;

/**
 * Created by abizu-alvio on 8/7/2017.
 */

public class RedemptionHistoryParam {

    private int page;

    private int size;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
