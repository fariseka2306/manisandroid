package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raden on 7/26/17.
 */

public class Reward implements Parcelable {

    @SerializedName("voucherTransactionType")
    @Expose
    private String voucherTransactionType;
    @SerializedName("voucherCostValue")
    @Expose
    private Integer voucherCostValue;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("inputs")
    @Expose
    private List<Object> inputs = null;
    @SerializedName("purchaseId")
    @Expose
    private String purchaseId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("redeemInstruction")
    @Expose
    private String redeemInstruction;
    @SerializedName("redeemCount")
    @Expose
    private Integer redeemCount;
    @SerializedName("point")
    @Expose
    private Integer point;
    @SerializedName("halalStatus")
    @Expose
    private String halalStatus;
    @SerializedName("expired")
    @Expose
    private String expired;
    @SerializedName("provider")
    @Expose
    private RewardProvider provider;
    @SerializedName("terms")
    @Expose
    private List<Term> terms = null;
    @SerializedName("voucherRedeemType")
    @Expose
    private String voucherRedeemType;
    @SerializedName("trackLink")
    @Expose
    private String trackLink;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("stock")
    @Expose
    private Integer stock;
    @SerializedName("value")
    @Expose
    private Integer value;
    @SerializedName("brand")
    @Expose
    private RewardBrand brand;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("redeemLimit")
    @Expose
    private String redeemLimit;
    @SerializedName("redeemId")
    @Expose
    private String redeemId;
    @SerializedName("hidePrice")
    @Expose
    private String hidePrice;
    @SerializedName("image64")
    @Expose
    private String image64;
    @SerializedName("redeemLabel")
    @Expose
    private String redeemLabel;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("currency3")
    @Expose
    private String currency3;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("needConfirmation")
    @Expose
    private String needConfirmation;
    @SerializedName("image128")
    @Expose
    private String image128;
    @SerializedName("voucherPurchaseValue")
    @Expose
    private Integer voucherPurchaseValue;

    public String getVoucherTransactionType() {
        return voucherTransactionType;
    }

    public void setVoucherTransactionType(String voucherTransactionType) {
        this.voucherTransactionType = voucherTransactionType;
    }

    public Integer getVoucherCostValue() {
        return voucherCostValue;
    }

    public void setVoucherCostValue(Integer voucherCostValue) {
        this.voucherCostValue = voucherCostValue;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<Object> getInputs() {
        return inputs;
    }

    public void setInputs(List<Object> inputs) {
        this.inputs = inputs;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRedeemInstruction() {
        return redeemInstruction;
    }

    public void setRedeemInstruction(String redeemInstruction) {
        this.redeemInstruction = redeemInstruction;
    }

    public Integer getRedeemCount() {
        return redeemCount;
    }

    public void setRedeemCount(Integer redeemCount) {
        this.redeemCount = redeemCount;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getHalalStatus() {
        return halalStatus;
    }

    public void setHalalStatus(String halalStatus) {
        this.halalStatus = halalStatus;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public RewardProvider getProvider() {
        return provider;
    }

    public void setProvider(RewardProvider provider) {
        this.provider = provider;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }

    public String getVoucherRedeemType() {
        return voucherRedeemType;
    }

    public void setVoucherRedeemType(String voucherRedeemType) {
        this.voucherRedeemType = voucherRedeemType;
    }

    public String getTrackLink() {
        return trackLink;
    }

    public void setTrackLink(String trackLink) {
        this.trackLink = trackLink;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public RewardBrand getBrand() {
        return brand;
    }

    public void setBrand(RewardBrand brand) {
        this.brand = brand;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRedeemLimit() {
        return redeemLimit;
    }

    public void setRedeemLimit(String redeemLimit) {
        this.redeemLimit = redeemLimit;
    }

    public String getRedeemId() {
        return redeemId;
    }

    public void setRedeemId(String redeemId) {
        this.redeemId = redeemId;
    }

    public String getHidePrice() {
        return hidePrice;
    }

    public void setHidePrice(String hidePrice) {
        this.hidePrice = hidePrice;
    }

    public String getImage64() {
        return image64;
    }

    public void setImage64(String image64) {
        this.image64 = image64;
    }

    public String getRedeemLabel() {
        return redeemLabel;
    }

    public void setRedeemLabel(String redeemLabel) {
        this.redeemLabel = redeemLabel;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCurrency3() {
        return currency3;
    }

    public void setCurrency3(String currency3) {
        this.currency3 = currency3;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNeedConfirmation() {
        return needConfirmation;
    }

    public void setNeedConfirmation(String needConfirmation) {
        this.needConfirmation = needConfirmation;
    }

    public String getImage128() {
        return image128;
    }

    public void setImage128(String image128) {
        this.image128 = image128;
    }

    public Integer getVoucherPurchaseValue() {
        return voucherPurchaseValue;
    }

    public void setVoucherPurchaseValue(Integer voucherPurchaseValue) {
        this.voucherPurchaseValue = voucherPurchaseValue;
    }

    public boolean isHidePrice() {
        return hidePrice != null && hidePrice.equalsIgnoreCase("Y");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.voucherTransactionType);
        dest.writeValue(this.voucherCostValue);
        dest.writeString(this.orderId);
        dest.writeList(this.inputs);
        dest.writeString(this.purchaseId);
        dest.writeString(this.description);
        dest.writeString(this.type);
        dest.writeString(this.redeemInstruction);
        dest.writeValue(this.redeemCount);
        dest.writeValue(this.point);
        dest.writeString(this.halalStatus);
        dest.writeString(this.expired);
        dest.writeParcelable(this.provider, flags);
        dest.writeList(this.terms);
        dest.writeString(this.voucherRedeemType);
        dest.writeString(this.trackLink);
        dest.writeString(this.currency);
        dest.writeString(this.id);
        dest.writeValue(this.stock);
        dest.writeValue(this.value);
        dest.writeParcelable(this.brand, flags);
        dest.writeString(this.image);
        dest.writeString(this.redeemLimit);
        dest.writeString(this.redeemId);
        dest.writeString(this.hidePrice);
        dest.writeString(this.image64);
        dest.writeString(this.redeemLabel);
        dest.writeString(this.transactionId);
        dest.writeString(this.tags);
        dest.writeString(this.currency3);
        dest.writeString(this.name);
        dest.writeString(this.needConfirmation);
        dest.writeString(this.image128);
        dest.writeValue(this.voucherPurchaseValue);
    }

    public Reward() {
    }

    protected Reward(Parcel in) {
        this.voucherTransactionType = in.readString();
        this.voucherCostValue = (Integer) in.readValue(Integer.class.getClassLoader());
        this.orderId = in.readString();
        this.inputs = new ArrayList<Object>();
        in.readList(this.inputs, Object.class.getClassLoader());
        this.purchaseId = in.readString();
        this.description = in.readString();
        this.type = in.readString();
        this.redeemInstruction = in.readString();
        this.redeemCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.point = (Integer) in.readValue(Integer.class.getClassLoader());
        this.halalStatus = in.readString();
        this.expired = in.readString();
        this.provider = in.readParcelable(RewardProvider.class.getClassLoader());
        this.terms = new ArrayList<Term>();
        in.readList(this.terms, Term.class.getClassLoader());
        this.voucherRedeemType = in.readString();
        this.trackLink = in.readString();
        this.currency = in.readString();
        this.id = in.readString();
        this.stock = (Integer) in.readValue(Integer.class.getClassLoader());
        this.value = (Integer) in.readValue(Integer.class.getClassLoader());
        this.brand = in.readParcelable(RewardBrand.class.getClassLoader());
        this.image = in.readString();
        this.redeemLimit = in.readString();
        this.redeemId = in.readString();
        this.hidePrice = in.readString();
        this.image64 = in.readString();
        this.redeemLabel = in.readString();
        this.transactionId = in.readString();
        this.tags = in.readString();
        this.currency3 = in.readString();
        this.name = in.readString();
        this.needConfirmation = in.readString();
        this.image128 = in.readString();
        this.voucherPurchaseValue = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<Reward> CREATOR = new Creator<Reward>() {
        @Override
        public Reward createFromParcel(Parcel source) {
            return new Reward(source);
        }

        @Override
        public Reward[] newArray(int size) {
            return new Reward[size];
        }
    };
}
