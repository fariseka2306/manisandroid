package com.ebizu.manis.model.snap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Raden on 7/17/17.
 */

public class SnapResult {

    @SerializedName("more")
    @Expose
    private Boolean more;

    @SerializedName("results")
    @Expose
    private ArrayList<SnapDetailsData> snapDetailsDatas;

    public Boolean getMore() {
        return more;
    }

    public void setMore(Boolean more) {
        this.more = more;
    }

    public ArrayList<SnapDetailsData> getSnapDetailsDatas() {
        return snapDetailsDatas;
    }

    public void setSnapDetailsDatas(ArrayList<SnapDetailsData> snapDetailsDatas) {
        this.snapDetailsDatas = snapDetailsDatas;
    }
}
