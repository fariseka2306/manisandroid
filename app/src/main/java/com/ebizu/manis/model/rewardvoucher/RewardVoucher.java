package com.ebizu.manis.model.rewardvoucher;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class RewardVoucher implements Parcelable {

    public static final String VOUCHER_TRANSACTION_REDEEMABLE = "Redeemable";
    public static final String VOUCHER_TRANSACTION_PURCHASABLE = "Purchasable";
    public static final String VOUCHER_TRANSACTION_BOTH = "Both";
    public static final String VOUCHER_REDEEM_CODE = "Code";
    public static final String VOUCHER_REDEEM_PIN = "Pin";

    @SerializedName("bulkId")
    @Expose
    private String bulkId = "";
    @SerializedName("bulkType")
    @Expose
    private String bulkType = "";
    @SerializedName("validityTitle")
    @Expose
    private String validityTitle = "";
    @SerializedName("voucherTransactionType")
    @Expose
    private String voucherTransactionType = "";
    @SerializedName("voucherCostValue")
    @Expose
    private double voucherCostValue = 0.0;
    @SerializedName("orderId")
    @Expose
    private String orderId = "";
    @SerializedName("inputs")
    @Expose
    private List<Input> inputs = new ArrayList<>();
    @SerializedName("description")
    @Expose
    private String description = "";
    @SerializedName("type")
    @Expose
    private String type = "";
    @SerializedName("redeemInstruction")
    @Expose
    private String redeemInstruction = "";
    @SerializedName("redeemCount")
    @Expose
    private int redeemCount = 0;
    @SerializedName("point")
    @Expose
    private int point = 0;
    @SerializedName("halalStatus")
    @Expose
    private String halalStatus = "";
    @SerializedName("expired")
    @Expose
    private String expired = "";
    @SerializedName("provider")
    @Expose
    private Provider provider = new Provider();
    @SerializedName("terms")
    @Expose
    private List<Term> terms = new ArrayList<>();
    @SerializedName("voucherRedeemType")
    @Expose
    private String voucherRedeemType = "";
    @SerializedName("trackLink")
    @Expose
    private String trackLink = "";
    @SerializedName("currency")
    @Expose
    private String currency = "";
    @SerializedName("id")
    @Expose
    private String id = "";
    @SerializedName("stock")
    @Expose
    private int stock = 0;
    @SerializedName("value")
    @Expose
    private int value;
    @SerializedName("brand")
    @Expose
    private Brand brand = new Brand();
    @SerializedName("orderIdValue")
    @Expose
    private String orderIdValue = "";
    @SerializedName("image")
    @Expose
    private String image = "";
    @SerializedName("redeemLimit")
    @Expose
    private String redeemLimit = "";
    @SerializedName("hidePrice")
    @Expose
    private String hidePrice = "";
    @SerializedName("image64")
    @Expose
    private String image64 = "";
    @SerializedName("redeemLabel")
    @Expose
    private String redeemLabel = "";
    @SerializedName("transactionId")
    @Expose
    private String transactionId = "";
    @SerializedName("tags")
    @Expose
    private String tags = "";
    @SerializedName("currency3")
    @Expose
    private String currency3 = "";
    @SerializedName("name")
    @Expose
    private String name = "";
    @SerializedName("needConfirmation")
    @Expose
    private String needConfirmation = "";
    @SerializedName("startDate")
    @Expose
    private String startDate = "";
    @SerializedName("image128")
    @Expose
    private String image128 = "";
    @SerializedName("orderIdTitle")
    @Expose
    private String orderIdTitle = "";
    @SerializedName("voucherPurchaseValue")
    @Expose
    private double voucherPurchaseValue = 0.0;
    @SerializedName("qty")
    @Expose
    private int quantity;

    public RewardVoucher() {
    }

    public RewardVoucher(String id, String name, int quantity) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
    }

    protected RewardVoucher(Parcel in) {
        bulkId = in.readString();
        bulkType = in.readString();
        voucherTransactionType = in.readString();
        validityTitle = in.readString();
        voucherCostValue = in.readDouble();
        orderId = in.readString();
        inputs = in.createTypedArrayList(Input.CREATOR);
        description = in.readString();
        type = in.readString();
        redeemInstruction = in.readString();
        redeemCount = in.readInt();
        point = in.readInt();
        halalStatus = in.readString();
        expired = in.readString();
        provider = in.readParcelable(Provider.class.getClassLoader());
        terms = in.createTypedArrayList(Term.CREATOR);
        voucherRedeemType = in.readString();
        trackLink = in.readString();
        currency = in.readString();
        id = in.readString();
        stock = in.readInt();
        value = in.readInt();
        brand = in.readParcelable(Brand.class.getClassLoader());
        orderIdValue = in.readString();
        image = in.readString();
        redeemLimit = in.readString();
        hidePrice = in.readString();
        image64 = in.readString();
        redeemLabel = in.readString();
        transactionId = in.readString();
        tags = in.readString();
        currency3 = in.readString();
        name = in.readString();
        needConfirmation = in.readString();
        startDate = in.readString();
        image128 = in.readString();
        orderIdTitle = in.readString();
        voucherPurchaseValue = in.readDouble();
        quantity = in.readInt();
    }

    public static final Creator<RewardVoucher> CREATOR = new Creator<RewardVoucher>() {
        @Override
        public RewardVoucher createFromParcel(Parcel in) {
            return new RewardVoucher(in);
        }

        @Override
        public RewardVoucher[] newArray(int size) {
            return new RewardVoucher[size];
        }
    };

    public boolean isNeedConfirmationPage() {
        return needConfirmation != null && needConfirmation.equalsIgnoreCase("Y");
    }

    public void setNeedConfirmationPage(String needConfirmationPage) {
        this.needConfirmation = needConfirmationPage;
    }

    public boolean isHidePrice() {
        return hidePrice != null && hidePrice.equalsIgnoreCase("Y");
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bulkId);
        dest.writeString(bulkType);
        dest.writeString(voucherTransactionType);
        dest.writeString(validityTitle);
        dest.writeDouble(voucherCostValue);
        dest.writeString(orderId);
        dest.writeTypedList(inputs);
        dest.writeString(description);
        dest.writeString(type);
        dest.writeString(redeemInstruction);
        dest.writeInt(redeemCount);
        dest.writeInt(point);
        dest.writeString(halalStatus);
        dest.writeString(expired);
        dest.writeParcelable(provider, flags);
        dest.writeTypedList(terms);
        dest.writeString(voucherRedeemType);
        dest.writeString(trackLink);
        dest.writeString(currency);
        dest.writeString(id);
        dest.writeInt(stock);
        dest.writeInt(value);
        dest.writeParcelable(brand, flags);
        dest.writeString(orderIdValue);
        dest.writeString(image);
        dest.writeString(redeemLimit);
        dest.writeString(hidePrice);
        dest.writeString(image64);
        dest.writeString(redeemLabel);
        dest.writeString(transactionId);
        dest.writeString(tags);
        dest.writeString(currency3);
        dest.writeString(name);
        dest.writeString(needConfirmation);
        dest.writeString(startDate);
        dest.writeString(image128);
        dest.writeString(orderIdTitle);
        dest.writeDouble(voucherPurchaseValue);
        dest.writeInt(this.quantity);
    }

    public String getBulkId() {
        return bulkId;
    }

    public String getBulkType() {
        return bulkType;
    }

    public String getValidityTitle() {
        return validityTitle;
    }

    public String getVoucherTransactionType() {
        return voucherTransactionType;
    }

    public double getVoucherCostValue() {
        return voucherCostValue;
    }

    public String getOrderId() {
        return orderId;
    }

    public List<Input> getInputs() {
        return inputs;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public String getRedeemInstruction() {
        return redeemInstruction;
    }

    public int getRedeemCount() {
        return redeemCount;
    }

    public int getPoint() {
        return point;
    }

    public String getHalalStatus() {
        return halalStatus;
    }

    public String getExpired() {
        return expired;
    }

    public Provider getProvider() {
        return provider;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public String getVoucherRedeemType() {
        return voucherRedeemType;
    }

    public String getTrackLink() {
        return trackLink;
    }

    public String getCurrency() {
        return currency;
    }

    public String getId() {
        return id;
    }

    public int getStock() {
        return stock;
    }

    public int getValue() {
        return value;
    }

    public Brand getBrand() {
        return brand;
    }

    public String getImage() {
        return image;
    }

    public String getOrderIdValue() {
        return orderIdValue;
    }

    public String getRedeemLimit() {
        return redeemLimit;
    }

    public String getHidePrice() {
        return hidePrice;
    }

    public String getImage64() {
        return image64;
    }

    public String getRedeemLabel() {
        return redeemLabel;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getTags() {
        return tags;
    }

    public String getCurrency3() {
        return currency3;
    }

    public String getName() {
        return name;
    }

    public String getNeedConfirmation() {
        return needConfirmation;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getImage128() {
        return image128;
    }

    public String getOrderIdTitle() {
        return orderIdTitle;
    }

    public double getVoucherPurchaseValue() {
        return voucherPurchaseValue;
    }

    public int getQuantity() {
        return quantity;
    }

    public static Creator<RewardVoucher> getCREATOR() {
        return CREATOR;
    }

    public void setBulkId(String bulkId) {
        this.bulkId = bulkId;
    }

    public void setBulkType(String bulkType) {
        this.bulkType = bulkType;
    }

    public void setValidityTitle(String validityTitle) {
        this.validityTitle = validityTitle;
    }

    public void setVoucherTransactionType(String voucherTransactionType) {
        this.voucherTransactionType = voucherTransactionType;
    }

    public void setVoucherCostValue(double voucherCostValue) {
        this.voucherCostValue = voucherCostValue;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setInputs(List<Input> inputs) {
        this.inputs = inputs;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRedeemInstruction(String redeemInstruction) {
        this.redeemInstruction = redeemInstruction;
    }

    public void setRedeemCount(int redeemCount) {
        this.redeemCount = redeemCount;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setHalalStatus(String halalStatus) {
        this.halalStatus = halalStatus;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }

    public void setVoucherRedeemType(String voucherRedeemType) {
        this.voucherRedeemType = voucherRedeemType;
    }

    public void setTrackLink(String trackLink) {
        this.trackLink = trackLink;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public void setOrderIdValue(String orderIdValue) {
        this.orderIdValue = orderIdValue;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setRedeemLimit(String redeemLimit) {
        this.redeemLimit = redeemLimit;
    }

    public void setHidePrice(String hidePrice) {
        this.hidePrice = hidePrice;
    }

    public void setImage64(String image64) {
        this.image64 = image64;
    }

    public void setRedeemLabel(String redeemLabel) {
        this.redeemLabel = redeemLabel;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public void setCurrency3(String currency3) {
        this.currency3 = currency3;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNeedConfirmation(String needConfirmation) {
        this.needConfirmation = needConfirmation;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setImage128(String image128) {
        this.image128 = image128;
    }

    public void setOrderIdTitle(String orderIdTitle) {
        this.orderIdTitle = orderIdTitle;
    }

    public void setVoucherPurchaseValue(double voucherPurchaseValue) {
        this.voucherPurchaseValue = voucherPurchaseValue;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
