package com.ebizu.manis.model.saved;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mac on 8/21/17.
 */

public class Offer {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("claim_disabled")
    @Expose
    private Boolean claimDisabled;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("date")
    @Expose
    private Date date;
    @SerializedName("store")
    @Expose
    private Store store;
    @SerializedName("saved")
    @Expose
    private Boolean saved;
    @SerializedName("tos")
    @Expose
    private List<String> tos = null;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Boolean getClaimDisabled() {
        return claimDisabled;
    }

    public String getDescription() {
        return description;
    }

    public String getPhoto() {
        return photo;
    }

    public Date getDate() {
        return date;
    }

    public Store getStore() {
        return store;
    }

    public Boolean getSaved() {
        return saved;
    }

    public List<String> getTos() {
        return tos;
    }
}
