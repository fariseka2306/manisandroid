package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.manis.sdk.entities.RewardsClaimedItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 8/7/2017.
 */

public class RedemptionHistoryAssets implements Parcelable {

    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("logo")
    @Expose
    private String logo;

    public RedemptionHistoryAssets(Parcel in) {
        thumbnail = in.readString();
        logo = in.readString();
    }

    public static final Creator<RedemptionHistoryAssets> CREATOR = new Creator<RedemptionHistoryAssets>() {
        @Override
        public RedemptionHistoryAssets createFromParcel(Parcel in) {
            return new RedemptionHistoryAssets(in);
        }

        @Override
        public RedemptionHistoryAssets[] newArray(int size) {
            return new RedemptionHistoryAssets[size];
        }
    };

    public String getThumbnail() {
        return thumbnail;
    }

    public String getLogo() {
        return logo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(thumbnail);
        dest.writeString(logo);
    }
}
