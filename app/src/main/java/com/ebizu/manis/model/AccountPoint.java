package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 7/11/17.
 */

public class AccountPoint {

    @SerializedName("point")
    @Expose
    public int point;

    public int getPoint() {
        return point;
    }

//            public static class RequestBody {
//    }
//    public static class Request extends BaseRequest<BaseHeaderRequest,com.ebizu.manis.sdk.rest.data.AccountPoint.RequestBody> {
//
//        public Request(double latitude, double longitude) {
//            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
//            this.body = new com.ebizu.manis.sdk.rest.data.AccountPoint.RequestBody();
//        }
//    }
//
//    public static class Response {
//        @SerializedName("point")
//        @Expose
//        public long point;
//
//
//    }
}
