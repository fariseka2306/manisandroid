package com.ebizu.manis.model.snap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/12/17.
 */

public class SnapTerm {

    @SerializedName("validity")
    @Expose
    private String validity;
    @SerializedName("limit")
    @Expose
    private String limit;
    @SerializedName("point")
    @Expose
    private String point;

    public String getValidity() {
        return validity;
    }

    public String getLimit() {
        return limit;
    }

    public String getPoint() {
        return point;
    }

    @Override
    public String toString() {
        return "SnapTerm{" +
                "validity='" + validity + '\'' +
                ", limit='" + limit + '\'' +
                ", point='" + point + '\'' +
                '}';
    }
}
