package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 16/07/2017.
 */

public class Device {

    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("imsi")
    @Expose
    private String imsi;
    @SerializedName("manufacture")
    @Expose
    private String manufacture;
    @SerializedName("model")
    @Expose
    private String model = "";
    @SerializedName("msisdn")
    @Expose
    private String msisdn;
    @SerializedName("operator")
    @Expose
    private String operator;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("version")
    @Expose
    private String version;

    public String getImei() {
        return imei;
    }

    public String getImsi() {
        return imsi;
    }

    public String getManufacture() {
        return manufacture;
    }

    public String getModel() {
        return model;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getOperator() {
        return operator;
    }

    public String getToken() {
        return token;
    }

    public String getVersion() {
        return version;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
