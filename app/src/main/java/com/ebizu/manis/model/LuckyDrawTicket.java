package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 08/08/17.
 */

public class LuckyDrawTicket implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("d_day")
    @Expose
    private long dDay;
    @SerializedName("wording_prefix")
    @Expose
    private String wordingPrefix;
    @SerializedName("wording_object")
    @Expose
    private String wordingObject;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getdDay() {
        return dDay;
    }

    public void setdDay(int dDay) {
        this.dDay = dDay;
    }

    public String getWordingPrefix() {
        return wordingPrefix;
    }

    public void setWordingPrefix(String wordingPrefix) {
        this.wordingPrefix = wordingPrefix;
    }

    public String getWordingObject() {
        return wordingObject;
    }

    public void setWordingObject(String wordingObject) {
        this.wordingObject = wordingObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeLong(this.dDay);
        dest.writeString(this.wordingPrefix);
        dest.writeString(this.wordingObject);
    }

    public LuckyDrawTicket() {
    }

    protected LuckyDrawTicket(Parcel in) {
        this.id = in.readInt();
        this.dDay = in.readLong();
        this.wordingPrefix = in.readString();
        this.wordingObject = in.readString();
    }

    public static final Creator<LuckyDrawTicket> CREATOR = new Creator<LuckyDrawTicket>() {
        @Override
        public LuckyDrawTicket createFromParcel(Parcel source) {
            return new LuckyDrawTicket(source);
        }

        @Override
        public LuckyDrawTicket[] newArray(int size) {
            return new LuckyDrawTicket[size];
        }
    };
}
