package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 7/24/17.
 */

public class ViewHistoryParam {

    @SerializedName("filter")
    @Expose
    public int filter;
    @SerializedName("order")
    @Expose
    public int order;
    @SerializedName("sort")
    @Expose
    public int sort;
    @SerializedName("page")
    @Expose
    public int page;
    @SerializedName("size")
    @Expose
    public int size;

    public int getFilter() {
        return filter;
    }

    public void setFilter(int filter) {
        this.filter = filter;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
