package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class LuckyDrawTerm implements Parcelable {

    @SerializedName("validity")
    @Expose
    private String validity;

    @SerializedName("limit")
    @Expose
    private String limit;

    @SerializedName("point")
    @Expose
    private String point;

    @SerializedName("short_description")
    @Expose
    private String shortDescription;

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.validity);
        dest.writeString(this.limit);
        dest.writeString(this.point);
        dest.writeString(this.shortDescription);
    }

    public LuckyDrawTerm() {
    }

    protected LuckyDrawTerm(Parcel in) {
        this.validity = in.readString();
        this.limit = in.readString();
        this.point = in.readString();
        this.shortDescription = in.readString();
    }

    public static final Creator<LuckyDrawTerm> CREATOR = new Creator<LuckyDrawTerm>() {
        @Override
        public LuckyDrawTerm createFromParcel(Parcel source) {
            return new LuckyDrawTerm(source);
        }

        @Override
        public LuckyDrawTerm[] newArray(int size) {
            return new LuckyDrawTerm[size];
        }
    };
}
