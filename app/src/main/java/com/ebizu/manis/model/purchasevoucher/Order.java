package com.ebizu.manis.model.purchasevoucher;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ebizu on 10/2/17.
 */

public class Order implements Parcelable {

    private Product product;
    private CustomerDetails customerDetails;

    public Product getProduct() {
        return product;
    }

    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.product, flags);
        dest.writeParcelable(this.customerDetails, flags);
    }

    public Order() {
    }

    protected Order(Parcel in) {
        this.product = in.readParcelable(Product.class.getClassLoader());
        this.customerDetails = in.readParcelable(CustomerDetails.class.getClassLoader());
    }

    public static final Parcelable.Creator<Order> CREATOR = new Parcelable.Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel source) {
            return new Order(source);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
}
