package com.ebizu.manis.model;

/**
 * Created by firef on 7/21/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StoreDetail implements Parcelable {

    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("contact")
    @Expose
    private Contact contact;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("point")
    @Expose
    private Integer point;
    @SerializedName("premium")
    @Expose
    private Boolean premium;
    @SerializedName("multiplier")
    @Expose
    private Integer multiplier;
    @SerializedName("pinned")
    @Expose
    private Boolean pinned;
    @SerializedName("offers")
    @Expose
    private List<Offer> offers = null;
    @SerializedName("rewards")
    @Expose
    private List<Object> rewards = null;
    @SerializedName("avaibility")
    @Expose
    private Avaibility avaibility;
    @SerializedName("followers")
    @Expose
    private Integer followers;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Boolean getPremium() {
        return premium;
    }

    public void setPremium(Boolean premium) {
        this.premium = premium;
    }

    public Integer getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }

    public Boolean getPinned() {
        return pinned;
    }

    public void setPinned(Boolean pinned) {
        this.pinned = pinned;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public List<Object> getRewards() {
        return rewards;
    }

    public void setRewards(List<Object> rewards) {
        this.rewards = rewards;
    }

    public Avaibility getAvaibility() {
        return avaibility;
    }

    public void setAvaibility(Avaibility avaibility) {
        this.avaibility = avaibility;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.description);
        dest.writeParcelable(this.address, flags);
        dest.writeParcelable(this.contact, flags);
        dest.writeString(this.website);
        dest.writeValue(this.point);
        dest.writeValue(this.premium);
        dest.writeValue(this.multiplier);
        dest.writeValue(this.pinned);
        dest.writeTypedList(this.offers);
        dest.writeList(this.rewards);
        dest.writeParcelable(this.avaibility, flags);
        dest.writeValue(this.followers);
    }

    public StoreDetail() {
    }

    protected StoreDetail(Parcel in) {
        this.description = in.readString();
        this.address = in.readParcelable(Address.class.getClassLoader());
        this.contact = in.readParcelable(Contact.class.getClassLoader());
        this.website = in.readString();
        this.point = (Integer) in.readValue(Integer.class.getClassLoader());
        this.premium = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.multiplier = (Integer) in.readValue(Integer.class.getClassLoader());
        this.pinned = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.offers = in.createTypedArrayList(Offer.CREATOR);
        this.rewards = new ArrayList<Object>();
        in.readList(this.rewards, Object.class.getClassLoader());
        this.avaibility = in.readParcelable(Avaibility.class.getClassLoader());
        this.followers = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<StoreDetail> CREATOR = new Creator<StoreDetail>() {
        @Override
        public StoreDetail createFromParcel(Parcel source) {
            return new StoreDetail(source);
        }

        @Override
        public StoreDetail[] newArray(int size) {
            return new StoreDetail[size];
        }
    };
}
