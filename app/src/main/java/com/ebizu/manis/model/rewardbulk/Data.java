package com.ebizu.manis.model.rewardbulk;

import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by FARIS_mac on 11/14/17.
 */

public class Data {

    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("pageCount")
    @Expose
    private Integer pageCount;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("data")
    @Expose
    private List<RewardBulk> rewardBulks = null;
    @SerializedName("more")
    @Expose
    private Boolean more;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("page")
    @Expose
    private Integer page;

    public Integer getTotal() {
        return total;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public Integer getSize() {
        return size;
    }

    public List<RewardBulk> getRewardBulks() {
        return rewardBulks;
    }

    public Boolean getMore() {
        return more;
    }

    public Integer getCount() {
        return count;
    }

    public Integer getPage() {
        return page;
    }
}
