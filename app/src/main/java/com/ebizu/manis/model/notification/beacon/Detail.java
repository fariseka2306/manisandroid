package com.ebizu.manis.model.notification.beacon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.litepal.crud.DataSupport;

/**
 * Created by ebizu on 8/23/17.
 */

public class Detail {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("claim_disabled")
    @Expose
    private Boolean claimDisabled;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("valid_start")
    @Expose
    private Integer validStart;
    @SerializedName("valid_end")
    @Expose
    private Integer validEnd;
    @SerializedName("expired")
    @Expose
    private Integer expired;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("limit_one_per_customer")
    @Expose
    private String limitOnePerCustomer;
    @SerializedName("allow_redeem")
    @Expose
    private String allowRedeem;
    @SerializedName("store")
    @Expose
    private Store store;
    @SerializedName("share_url")
    @Expose
    private String shareUrl;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Boolean getClaimDisabled() {
        return claimDisabled;
    }

    public String getDescription() {
        return description;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public String getCompany() {
        return company;
    }

    public String getCategory() {
        return category;
    }

    public String getPicture() {
        return picture;
    }

    public Integer getValidStart() {
        return validStart;
    }

    public Integer getValidEnd() {
        return validEnd;
    }

    public Integer getExpired() {
        return expired;
    }

    public Integer getRating() {
        return rating;
    }

    public String getLimitOnePerCustomer() {
        return limitOnePerCustomer;
    }

    public String getAllowRedeem() {
        return allowRedeem;
    }

    public Store getStore() {
        return store;
    }

    public String getShareUrl() {
        return shareUrl;
    }

}
