package com.ebizu.manis.model.rewardbulkdetail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARIS_mac on 15/11/17.
 */

public class ShoppingCart implements Parcelable {

    @SerializedName("orderId")
    @Expose
    private String orderId = "";
    @SerializedName("name")
    @Expose
    private String name = "";
    @SerializedName("description")
    @Expose
    private String description = "";
    @SerializedName("currency")
    @Expose
    private String currency = "";
    @SerializedName("totalPrice")
    @Expose
    private int totalPrice = 0;
    @SerializedName("qty")
    @Expose
    private int qty = 0;
    @SerializedName("vouchers")
    @Expose
    private List<Voucher> vouchers = new ArrayList<>();

    public ShoppingCart() {
    }

    protected ShoppingCart(Parcel in) {
        orderId = in.readString();
        name = in.readString();
        description = in.readString();
        currency = in.readString();
        totalPrice = in.readInt();
        qty = in.readInt();
    }

    public static final Creator<ShoppingCart> CREATOR = new Creator<ShoppingCart>() {
        @Override
        public ShoppingCart createFromParcel(Parcel in) {
            return new ShoppingCart(in);
        }

        @Override
        public ShoppingCart[] newArray(int size) {
            return new ShoppingCart[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderId);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(currency);
        dest.writeInt(totalPrice);
        dest.writeInt(qty);
    }

    public String getOrderId() {
        return orderId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCurrency() {
        return currency;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public int getQty() {
        return qty;
    }

    public List<Voucher> getVouchers() {
        return vouchers;
    }

    public static Creator<ShoppingCart> getCREATOR() {
        return CREATOR;
    }
}
