package com.ebizu.manis.model.notification.beacon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.litepal.crud.DataSupport;

/**
 * Created by ebizu on 8/23/17.
 */

public class Assets {

    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("banner")
    @Expose
    private String banner;

    public String getPhoto() {
        return photo;
    }

    public String getBanner() {
        return banner;
    }
}
