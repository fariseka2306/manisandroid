package com.ebizu.manis.model;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class Privacy {

    String type;
    String title;
    String content;

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
