package com.ebizu.manis.model.rewardvoucher;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class Data {
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("pageCount")
    @Expose
    private Integer pageCount;
    @SerializedName("size")
    @Expose
    private Integer size;
    @SerializedName("data")
    @Expose
    private List<RewardVoucher> rewardVoucherList = null;
    @SerializedName("more")
    @Expose

    private boolean more;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("page")
    @Expose
    private Integer page;

    public Integer getTotal() {
        return total;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public Integer getSize() {
        return size;
    }

    public List<RewardVoucher> getRewardVoucherList() {
        return rewardVoucherList;
    }

    public Boolean getMore() {
        return more;
    }

    public Integer getCount() {
        return count;
    }

    public Integer getPage() {
        return page;
    }

}
