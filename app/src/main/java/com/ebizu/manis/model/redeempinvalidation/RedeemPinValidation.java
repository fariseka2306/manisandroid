package com.ebizu.manis.model.redeempinvalidation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 20/11/17.
 */

public class RedeemPinValidation {

    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;

    public String getVoucherCode() {
        return voucherCode;
    }
}
