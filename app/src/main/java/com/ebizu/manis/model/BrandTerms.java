package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/4/17.
 */

public class BrandTerms {

    @SerializedName("terms")
    @Expose
    private String terms;

    public String getTerms() {
        return terms;
    }
}
