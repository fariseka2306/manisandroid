package com.ebizu.manis.model.notification.snap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.litepal.crud.DataSupport;

/**
 * Created by ebizu on 8/28/17.
 */

public class SnapPoint {

    @SerializedName("snap")
    @Expose
    private String snap;
    @SerializedName("current")
    @Expose
    private String current;

    public String getSnap() {
        return snap;
    }

    public String getCurrent() {
        return current;
    }
}
