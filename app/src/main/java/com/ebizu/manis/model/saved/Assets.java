package com.ebizu.manis.model.saved;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 8/21/17.
 */

public class Assets {

    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("banner")
    @Expose
    private String banner;

    public String getIcon() {
        return icon;
    }

    public String getPhoto() {
        return photo;
    }

    public String getBanner() {
        return banner;
    }
}
