package com.ebizu.manis.model.purchasevoucher;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/2/17.
 */

public class CustomerDetails implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phoneNo;
    @SerializedName("shipmentInfo")
    @Expose
    private String shipmentInfo = "";
    @SerializedName("address")
    @Expose
    private String address = "";
    @SerializedName("postcode")
    @Expose
    private String postCode = "";
    @SerializedName("town")
    @Expose
    private String town = "";
    @SerializedName("state")
    @Expose
    private String state = "";
    @SerializedName("country")
    @Expose
    private String country = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getShipmentInfo() {
        return shipmentInfo;
    }

    public void setShipmentInfo(String shipmentInfo) {
        this.shipmentInfo = shipmentInfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public CustomerDetails() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.phoneNo);
        dest.writeString(this.shipmentInfo);
        dest.writeString(this.address);
        dest.writeString(this.postCode);
        dest.writeString(this.town);
        dest.writeString(this.state);
        dest.writeString(this.country);
    }

    protected CustomerDetails(Parcel in) {
        this.name = in.readString();
        this.email = in.readString();
        this.phoneNo = in.readString();
        this.shipmentInfo = in.readString();
        this.address = in.readString();
        this.postCode = in.readString();
        this.town = in.readString();
        this.state = in.readString();
        this.country = in.readString();
    }

    public static final Creator<CustomerDetails> CREATOR = new Creator<CustomerDetails>() {
        @Override
        public CustomerDetails createFromParcel(Parcel source) {
            return new CustomerDetails(source);
        }

        @Override
        public CustomerDetails[] newArray(int size) {
            return new CustomerDetails[size];
        }
    };
}
