package com.ebizu.manis.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 27/07/2017.
 */

public class Phone {

    @SerializedName("number")
    private String number;

    public void setNumber(String number) {
        this.number = number;
    }
}
