package com.ebizu.manis.model.purchasevoucher;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ebizu on 10/2/17.
 */

public class Product implements Parcelable {

    private String name;
    private int qty;
    private double amount;
    private double totalAmount;
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.qty);
        dest.writeDouble(this.amount);
        dest.writeDouble(this.totalAmount);
        dest.writeString(this.orderId);
    }

    public Product() {
    }

    protected Product(Parcel in) {
        this.name = in.readString();
        this.qty = in.readInt();
        this.amount = in.readDouble();
        this.totalAmount = in.readDouble();
        this.orderId = in.readString();
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
