package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class LuckyDrawCheckSnapable {

    @SerializedName("user_allowed_snap")
    @Expose
    private Boolean userAllowedSnap;

    @SerializedName("restriction_message")
    @Expose
    private String restrictionMessage;

    public Boolean getUserAllowedSnap() {
        return userAllowedSnap;
    }

    public void setUserAllowedSnap(Boolean userAllowedSnap) {
        this.userAllowedSnap = userAllowedSnap;
    }

    public String getRestrictionMessage() {
        return restrictionMessage;
    }

    public void setRestrictionMessage(String restrictionMessage) {
        this.restrictionMessage = restrictionMessage;
    }
}
