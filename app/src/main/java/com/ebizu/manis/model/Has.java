package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firef on 7/7/2017.
 */

public class Has implements Parcelable {

    @SerializedName("offers")
    @Expose
    private Boolean offers;

    @SerializedName("rewards")
    @Expose
    private Boolean rewards;

    public Boolean getOffers() {
        return offers;
    }

    public void setOffers(Boolean offers) {
        this.offers = offers;
    }

    public Boolean getRewards() {
        return rewards;
    }

    public void setRewards(Boolean rewards) {
        this.rewards = rewards;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.offers);
        dest.writeValue(this.rewards);
    }

    public Has() {
    }

    protected Has(Parcel in) {
        this.offers = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.rewards = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<Has> CREATOR = new Parcelable.Creator<Has>() {
        @Override
        public Has createFromParcel(Parcel source) {
            return new Has(source);
        }

        @Override
        public Has[] newArray(int size) {
            return new Has[size];
        }
    };
}
