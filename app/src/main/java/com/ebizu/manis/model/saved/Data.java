package com.ebizu.manis.model.saved;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mac on 8/21/17.
 */

public class Data {

    @SerializedName("offers")
    @Expose
    private List<Offer> offers = null;
    @SerializedName("rewards")
    @Expose
    private List<Object> rewards = null;

}
