package com.ebizu.manis.model.notification.beacon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.litepal.crud.DataSupport;

/**
 * Created by ebizu on 8/23/17.
 */

public class BeaconPromo {

    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("company_id")
    @Expose
    private Integer companyId;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("detail")
    @Expose
    private Detail detail;
    @SerializedName("sn")
    @Expose
    private String sn;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCompany() {
        return company;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public String getPicture() {
        return picture;
    }

    public String getType() {
        return type;
    }

    public Detail getDetail() {
        return detail;
    }

    public String getSn() {
        return sn;
    }

    @Override
    public String toString() {
        return "BeaconPromo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", company='" + company + '\'' +
                ", companyId=" + companyId +
                ", picture='" + picture + '\'' +
                ", type='" + type + '\'' +
                ", detail=" + detail +
                ", sn='" + sn + '\'' +
                '}';
    }
}
