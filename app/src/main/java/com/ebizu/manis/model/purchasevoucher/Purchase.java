package com.ebizu.manis.model.purchasevoucher;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/7/17.
 */

public class Purchase {

    @SerializedName("purchaseId")
    @Expose
    private String purchaseId = "";
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage = "";
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode = "";

    public String getPurchaseId() {
        return purchaseId;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public String getVoucherCode() {
        return voucherCode;
    }
}
