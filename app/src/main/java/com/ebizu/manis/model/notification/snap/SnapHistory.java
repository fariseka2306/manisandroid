package com.ebizu.manis.model.notification.snap;

import com.ebizu.manis.model.Point;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.litepal.crud.DataSupport;

/**
 * Created by ebizu on 8/28/17.
 */

public class SnapHistory {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("app")
    @Expose
    private String app;
    @SerializedName("silent")
    @Expose
    private boolean silent;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("point")
    @Expose
    private SnapPoint snapPoint;

    public SnapHistory(long id, String message, String uri, String status) {
        this.id = id;
        this.message = message;
        this.uri = uri;
        this.status = status;
        this.app = "manis";
        this.silent = false;
        this.type = "snap";
    }

    public long getId() {
        return id;
    }

    public String getApp() {
        return app;
    }

    public boolean isSilent() {
        return silent;
    }

    public String getMessage() {
        return message;
    }

    public String getType() {
        return type;
    }

    public String getUri() {
        return uri;
    }

    public String getStatus() {
        return status;
    }

    public SnapPoint getSnapPoint() {
        return snapPoint;
    }
}
