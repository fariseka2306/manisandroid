package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by abizu-alvio on 8/7/2017.
 */

public class RedemptionHistoryData {

    @SerializedName("more")
    @Expose
    private Boolean more;
    @SerializedName("results")
    @Expose
    private List<RedemptionHistoryResult> getRedemptionHistoryResultList = null;

    public Boolean getMore() {
        return more;
    }

    public List<RedemptionHistoryResult> getGetRedemptionHistoryResultList() {
        return getRedemptionHistoryResultList;
    }
}
