package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 04/08/17.
 */

public class PrizeDetail implements Parcelable {

    @SerializedName("prize_id")
    @Expose
    private Integer prizeId;

    @SerializedName("prize_name")
    @Expose
    private String prizeName;

    @SerializedName("prize_image")
    @Expose
    private String prizeImage;

    public Integer getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(Integer prizeId) {
        this.prizeId = prizeId;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public String getPrizeImage() {
        return prizeImage;
    }

    public void setPrizeImage(String prizeImage) {
        this.prizeImage = prizeImage;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.prizeId);
        dest.writeString(this.prizeName);
        dest.writeString(this.prizeImage);
    }

    public PrizeDetail() {
    }

    protected PrizeDetail(Parcel in) {
        this.prizeId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.prizeName = in.readString();
        this.prizeImage = in.readString();
    }

    public static final Creator<PrizeDetail> CREATOR = new Creator<PrizeDetail>() {
        @Override
        public PrizeDetail createFromParcel(Parcel source) {
            return new PrizeDetail(source);
        }

        @Override
        public PrizeDetail[] newArray(int size) {
            return new PrizeDetail[size];
        }
    };
}
