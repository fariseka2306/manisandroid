package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 7/24/2017.
 */

public class NotificationsSetting {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("value")
    @Expose
    private Boolean value;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "NotificationsSetting{" +
                "title='" + title + '\'' +
                ", value=" + value +
                '}';
    }
}
