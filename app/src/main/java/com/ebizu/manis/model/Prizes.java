package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu on 04/08/17.
 */

public class Prizes implements Parcelable {

    @SerializedName("prize_title")
    @Expose
    private String prizeTitle;

    @SerializedName("prize_details")
    @Expose
    private List<PrizeDetail> prizeDetails = null;

    public String getPrizeTitle() {
        return prizeTitle;
    }

    public void setPrizeTitle(String prizeTitle) {
        this.prizeTitle = prizeTitle;
    }

    public List<PrizeDetail> getPrizeDetails() {
        return prizeDetails;
    }

    public void setPrizeDetails(List<PrizeDetail> prizeDetails) {
        this.prizeDetails = prizeDetails;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.prizeTitle);
        dest.writeTypedList(this.prizeDetails);
    }

    public Prizes() {
    }

    protected Prizes(Parcel in) {
        this.prizeTitle = in.readString();
        this.prizeDetails = in.createTypedArrayList(PrizeDetail.CREATOR);
    }

    public static final Creator<Prizes> CREATOR = new Creator<Prizes>() {
        @Override
        public Prizes createFromParcel(Parcel source) {
            return new Prizes(source);
        }

        @Override
        public Prizes[] newArray(int size) {
            return new Prizes[size];
        }
    };
}
