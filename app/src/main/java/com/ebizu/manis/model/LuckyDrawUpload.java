package com.ebizu.manis.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class LuckyDrawUpload implements Parcelable {

    @SerializedName("snap_luckydraw_id")
    @Expose
    private Integer snapLuckydrawId;

    @SerializedName("is_luckydraw")
    @Expose
    private Boolean isLuckydraw;

    @SerializedName("prizes")
    @Expose
    private Prizes prizes;

    public Integer getSnapLuckydrawId() {
        return snapLuckydrawId;
    }

    public void setSnapLuckydrawId(Integer snapLuckydrawId) {
        this.snapLuckydrawId = snapLuckydrawId;
    }

    public Boolean getIsLuckydraw() {
        return isLuckydraw;
    }

    public void setIsLuckydraw(Boolean isLuckydraw) {
        this.isLuckydraw = isLuckydraw;
    }

    public Prizes getPrizes() {
        return prizes;
    }

    public void setPrizes(Prizes prizes) {
        this.prizes = prizes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.snapLuckydrawId);
        dest.writeValue(this.isLuckydraw);
        dest.writeParcelable(this.prizes, flags);
    }

    public LuckyDrawUpload() {
    }

    protected LuckyDrawUpload(Parcel in) {
        this.snapLuckydrawId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isLuckydraw = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.prizes = in.readParcelable(Prizes.class.getClassLoader());
    }

    public static final Creator<LuckyDrawUpload> CREATOR = new Creator<LuckyDrawUpload>() {
        @Override
        public LuckyDrawUpload createFromParcel(Parcel source) {
            return new LuckyDrawUpload(source);
        }

        @Override
        public LuckyDrawUpload[] newArray(int size) {
            return new LuckyDrawUpload[size];
        }
    };
}
