package com.ebizu.manis.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by abizu-alvio on 7/31/2017.
 */

public class Faq {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("children")
    @Expose
    private List<FaqChild> children = null;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<FaqChild> getChildren() {
        return children;
    }
}
