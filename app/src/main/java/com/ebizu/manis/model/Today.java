package com.ebizu.manis.model;

/**
 * Created by firef on 7/21/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Today implements Parcelable {

    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("open")
    @Expose
    private String open;
    @SerializedName("close")
    @Expose
    private String close;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.day);
        dest.writeString(this.status);
        dest.writeString(this.open);
        dest.writeString(this.close);
    }

    public Today() {
    }

    protected Today(Parcel in) {
        this.day = in.readString();
        this.status = in.readString();
        this.open = in.readString();
        this.close = in.readString();
    }

    public static final Creator<Today> CREATOR = new Creator<Today>() {
        @Override
        public Today createFromParcel(Parcel source) {
            return new Today(source);
        }

        @Override
        public Today[] newArray(int size) {
            return new Today[size];
        }
    };
}
