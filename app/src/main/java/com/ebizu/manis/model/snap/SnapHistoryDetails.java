package com.ebizu.manis.model.snap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 7/17/17.
 */

public class SnapHistoryDetails {


        @SerializedName("filter")
        @Expose
        public String filter;

        @SerializedName("order")
        @Expose
        public String order;

        @SerializedName("sort")
        @Expose
        public String sort;

        @SerializedName("page")
        @Expose
        public int page;
        @SerializedName("size")
        @Expose
        public int size;

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public String getOrder() {
            return order;
        }

        public void setOrder(String order) {
            this.order = order;
        }

        public String getSort() {
            return sort;
        }

        public void setSort(String sort) {
            this.sort = sort;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

//        public RequestBody(int filter, int order, int sort, int page, int size) {
//            this.filter = filter;
//            this.order = order;
//            this.sort = sort;
//            this.page = page;
//            this.size = size;
//        }

//    public static class Request extends BaseRequest<BaseHeaderRequest,com.ebizu.manis.sdk.rest.data.SnapHistory.RequestBody> {
//
//        public Request(double latitude, double longitude, int filter, int order, int sort, int page, int size) {
//            this.header = new BaseHeaderRequest(new Coordinate(latitude,longitude));
//            this.body = new com.ebizu.manis.sdk.rest.data.SnapHistory.RequestBody(filter, order, sort, page, size);
//        }
//    }
//
//    public static class Response {
//        @SerializedName("more")
//        @Expose
//        public boolean more;
//        @SerializedName("results")
//        @Expose
//        public ArrayList<com.ebizu.manis.model.snap.SnapData> results = new ArrayList<>();
//    }
}
