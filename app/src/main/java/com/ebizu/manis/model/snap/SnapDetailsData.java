package com.ebizu.manis.model.snap;

import com.ebizu.manis.model.Store;
import com.ebizu.manis.service.manis.requestbody.ReceiptSnap;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 7/17/17.
 */

public class SnapDetailsData {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("appealable")
    @Expose
    private Boolean appealable;
    @SerializedName("receiptSnap")
    @Expose
    private ReceiptSnap receiptSnap;
    @SerializedName("store")
    @Expose
    private Store store;
    @SerializedName("point")
    @Expose
    private int point;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_type")
    @Expose
    private int statusType;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("uploaded")
    @Expose
    private int uploaded;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getAppealable() {
        return appealable;
    }

    public void setAppealable(Boolean appealable) {
        this.appealable = appealable;
    }

    public ReceiptSnap getReceiptSnap() {
        return receiptSnap;
    }

    public void setReceiptSnap(ReceiptSnap receiptSnap) {
        this.receiptSnap = receiptSnap;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusType() {
        return statusType;
    }

    public void setStatusType(Integer statusType) {
        this.statusType = statusType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getUploaded() {
        return uploaded;
    }

    public void setUploaded(Integer uploaded) {
        this.uploaded = uploaded;
    }
}
