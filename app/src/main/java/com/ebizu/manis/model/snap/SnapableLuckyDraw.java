package com.ebizu.manis.model.snap;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/10/17.
 */

public class SnapableLuckyDraw {

    @SerializedName("user_allowed_snap")
    private boolean userAllowedSnap;
    @SerializedName("restriction_message")
    private String restrictionMessage;

    public boolean isUserAllowedSnap() {
        return userAllowedSnap;
    }

    public String getRestrictionMessage() {
        return restrictionMessage;
    }
}
