package com.ebizu.manis.model.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/4/17.
 */

public class IntervalBeaconPromo {

    @Expose
    private String type;
    @SerializedName("interval")
    @Expose
    private Integer interval;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

}
