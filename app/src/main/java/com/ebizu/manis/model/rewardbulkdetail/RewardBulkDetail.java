package com.ebizu.manis.model.rewardbulkdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 15/11/17.
 */

public class RewardBulkDetail {

    @SerializedName("orderDetailTimer")
    @Expose
    private int orderDetailTimer = 0;
    @SerializedName("shoppingCart")
    @Expose
    private ShoppingCart shoppingCart = new ShoppingCart();

    public int getOrderDetailTimer() {
        return orderDetailTimer;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }
}
