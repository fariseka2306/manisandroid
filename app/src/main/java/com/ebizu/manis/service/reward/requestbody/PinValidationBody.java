package com.ebizu.manis.service.reward.requestbody;

import android.content.Context;

import com.ebizu.manis.sdk.rest.data.PinValidationPost;
import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/9/17.
 */

public class PinValidationBody extends RewardSessionApiBody {

    @SerializedName("data")
    @Expose
    private PinValidationBody.Data data;

    public PinValidationBody(Context context, PinValidationBody.Data data) {
        super(context);
        this.data = data;
        this.module = RewardApiConfig.Module.VOUCHER;
        this.action = RewardApiConfig.Action.PIN_VALIDATION;
    }

    public static class Data {
        private String purchaseId;
        private String transactionId;
        private String pin;
        private String voucherCode;

        public String getPurchaseId() {
            return purchaseId;
        }

        public void setPurchaseId(String purchaseId) {
            this.purchaseId = purchaseId;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getPin() {
            return pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }

        public String getVoucherCode() {
            return voucherCode;
        }

        public void setVoucherCode(String voucherCode) {
            this.voucherCode = voucherCode;
        }
    }
}
