package com.ebizu.manis.service.reward.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class WrapperGetSessionNew extends ResponseRewardApi {

    @SerializedName("data")
    @Expose
    private RewardGetSessionNew rewardGetSessionNew;

    public RewardGetSessionNew getRewardGetSessionNew() {
        return rewardGetSessionNew;
    }

    public void setRewardGetSessionNew(RewardGetSessionNew rewardGetSessionNew) {
        this.rewardGetSessionNew = rewardGetSessionNew;
    }
}
