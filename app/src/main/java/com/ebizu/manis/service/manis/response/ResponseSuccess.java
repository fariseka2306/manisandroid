package com.ebizu.manis.service.manis.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 07/07/2017.
 */

public class ResponseSuccess {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("elapsed")
    @Expose
    private Integer elapsed;

    public Boolean getSuccess() {
        return success;
    }

    public Object getMessage() {
        return message;
    }

    public Integer getElapsed() {
        return elapsed;
    }
}
