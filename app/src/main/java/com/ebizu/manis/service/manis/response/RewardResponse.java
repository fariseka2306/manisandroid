package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.RewardPaging;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 9/23/17.
 */

public class RewardResponse {
    @SerializedName("errorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("time")
    @Expose
    private Double time;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }
}
