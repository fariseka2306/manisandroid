package com.ebizu.manis.service.manis.twitter;

import com.twitter.sdk.android.core.models.User;

import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.Call;

public interface CustomTwitterApi {
    @GET("1.1/users/show.json")
    Call<User> show(@Query("user_id") long id);
}