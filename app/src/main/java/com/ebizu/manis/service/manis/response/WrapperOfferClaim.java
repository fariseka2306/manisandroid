package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Redeem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 10/26/17.
 */

public class WrapperOfferClaim extends ResponseData{

    @SerializedName("data")
    @Expose
    private Redeem redeem;

    public Redeem getRedeem() {
        return redeem;
    }

    public void setRedeem(Redeem redeem) {
        this.redeem = redeem;
    }
}
