package com.ebizu.manis.service.manis;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.ManisTrackerRequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.service.manis.response.WrapperAccount;
import com.ebizu.manis.service.manis.response.WrapperInviteTerm;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawEntry;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawScreen;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawUpload;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawWinner;
import com.ebizu.manis.service.manis.response.WrapperNotifications;
import com.ebizu.manis.service.manis.response.WrapperPoint;
import com.ebizu.manis.service.manis.response.WrapperSnapable;
import com.ebizu.manis.service.manis.response.WrapperTerm;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by ebizu on 10/31/17.
 */

public interface ManisApiV2 {

    @POST(ConfigManager.LuckyDraw.URL_PATH)
    Observable<WrapperLuckyDrawWinner> getWinners(@Body RequestBody requestBody);

    @POST(ConfigManager.LuckyDraw.URL_PATH)
    Observable<WrapperLuckyDrawEntry> getEntries(@Body RequestBody requestBody);

    @POST(ConfigManager.LuckyDraw.URL_PATH)
    Observable<WrapperLuckyDrawScreen> getLuckyDrawScreen(@Body RequestBody requestBody);

    @POST(ConfigManager.LuckyDraw.URL_PATH)
    Observable<WrapperLuckyDrawUpload> uploadReceiptLuckyDraw(@Body RequestBody requestBody);

    @POST("LuckyDrawAPI")
    Observable<WrapperSnapable> checkSnapAbleLuckyDraw(@Body RequestBody requestBody);

    @POST(ConfigManager.Account.URL_PATH)
    Observable<WrapperAccount> getAccount(@Body RequestBody requestBody);

    @POST(ConfigManager.Account.URL_PATH)
    Observable<WrapperPoint> getPoint(@Body RequestBody requestBody);

    @POST(ConfigManager.Account.URL_PATH)
    Observable<WrapperAccount> saveAccountProfile(@Body RequestBody requestBody);

    @POST(ConfigManager.Account.URL_PATH)
    Observable<WrapperAccount> updateAccountPhotoProfile(@Body RequestBody requestBody);

    @POST(ConfigManager.Account.URL_PATH)
    Observable<WrapperNotifications> getNotifications(@Body RequestBody requestBody);

    @POST(ConfigManager.Account.URL_PATH)
    Observable<WrapperNotifications> saveAccountNotifications(@Body RequestBody requestBody);

    @POST(ConfigManager.Account.URL_PATH)
    Observable<WrapperInviteTerm> getInviteTerms(@Body RequestBody requestBody);

    @POST("trackerclient")
    Observable<ResponseData> postingTracker(@Body ManisTrackerRequestBody manisTrackerRequestBody);

    @POST(ConfigManager.ManisLegal.URL_PATH)
    Observable<WrapperTerm> getManisTerm(@Body RequestBody requestBody);
}
