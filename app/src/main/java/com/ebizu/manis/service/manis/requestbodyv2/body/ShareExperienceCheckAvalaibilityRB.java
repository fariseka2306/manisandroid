package com.ebizu.manis.service.manis.requestbodyv2.body;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.ManisRequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.data.ShareExperienceCheckAvalaibilityData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/12/17.
 */

public class ShareExperienceCheckAvalaibilityRB extends ManisRequestBody {

    @SerializedName("data")
    private ShareExperienceCheckAvalaibilityData data;

    public void setData(ShareExperienceCheckAvalaibilityData data) {
        this.data = data;
    }

    public ShareExperienceCheckAvalaibilityRB() {
        module = ConfigManager.Missions.SHARE_EXPERIENCE_MODUL;
        action = ConfigManager.Missions.SHARE_EXPERIENCE_AVAILABILITY_ACTION;
    }
}
