package com.ebizu.manis.service.manis.requestbodyv2.data;

import android.content.Context;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.DataRequestBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class ShareExperienceMissionData extends DataRequestBody {

    @SerializedName("perPage")
    @Expose
    private Integer perPage;
    @SerializedName("currentPage")
    @Expose
    private Integer currentPage;

    public ShareExperienceMissionData(Context context) {
        super(context);
        perPage = ConfigManager.Missions.perPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }
}
