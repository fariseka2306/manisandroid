package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.InviteTerm;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 8/14/2017.
 */

public class WrapperInviteTerm extends ResponseData {

    @SerializedName("data")
    @Expose
    InviteTerm inviteTerm;

    public InviteTerm getInviteTerm() {
        return inviteTerm;
    }

}
