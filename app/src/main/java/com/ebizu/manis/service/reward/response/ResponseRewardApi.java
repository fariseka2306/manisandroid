package com.ebizu.manis.service.reward.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/26/17.
 */

public class ResponseRewardApi<Response> {

    @SerializedName("errorCode")
    @Expose
    private Integer errorCode = 666;
    @SerializedName("time")
    @Expose
    private Double time = 0.0;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage = "";

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "ResponseRewardApi{" +
                "errorCode=" + errorCode +
                ", time=" + time +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
