package com.ebizu.manis.service.manis.requestbody.snap;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 8/21/17.
 */

public class ReceiptSnap implements Parcelable {


    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("amount")
    @Expose
    private double amount;

    public void setNumber(String number) {
        this.number = number;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.number);
        dest.writeDouble(this.amount);
    }

    public ReceiptSnap() {
    }

    protected ReceiptSnap(Parcel in) {
        this.number = in.readString();
        this.amount = in.readDouble();
    }

    public static final Parcelable.Creator<ReceiptSnap> CREATOR = new Parcelable.Creator<ReceiptSnap>() {
        @Override
        public ReceiptSnap createFromParcel(Parcel source) {
            return new ReceiptSnap(source);
        }

        @Override
        public ReceiptSnap[] newArray(int size) {
            return new ReceiptSnap[size];
        }
    };
}
