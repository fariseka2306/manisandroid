package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by ebizu on 24/07/17.
 */

public class StoreSearchBody {

    private String keyword;
    private int multiplier;
    private int page;
    private int size;

    public StoreSearchBody() {

    }

    public StoreSearchBody(String keyword, int multiplier, int page, int size) {
        this.keyword = keyword;
        this.multiplier = multiplier;
        this.page = page;
        this.size = size;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
