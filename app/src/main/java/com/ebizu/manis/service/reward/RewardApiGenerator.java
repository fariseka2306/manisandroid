package com.ebizu.manis.service.reward;

import android.content.Context;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.RequestBuilderHeader;
import com.ebizu.manis.service.reward.requestbody.RewardRegisterBody;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ebizu on 9/26/17.
 */

public class RewardApiGenerator {

    public static <S> RewardApi createService(Context context) {
        return retroBuilder(okBuilder(context).build()).create(RewardApi.class);
    }

    public static <S> RewardApi createServiceApiAry(Context context) {
        return retroBuilderApiAry(okBuilderAPiAry(context).build()).create(RewardApi.class);
    }

    private static OkHttpClient.Builder okBuilderAPiAry(Context context) {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS);
    }

    private static OkHttpClient.Builder okBuilder(Context context) {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(chain -> chain.proceed(requestBuilder(chain, context)));
    }

    private static Request requestBuilder(Interceptor.Chain chain, Context context) {
        Request.Builder requestBuilder = chain.request().newBuilder();
        RequestBuilderHeader requestBuilderHeader = new RequestBuilderHeader(requestBuilder, context);
        requestBuilderHeader.addToken();
        requestBuilderHeader.addCoordinate();
        requestBuilderHeader.addLang();
        requestBuilderHeader.addManisVer();
        requestBuilderHeader.addManisBuild();
        return requestBuilder.build();
    }

    private static Retrofit retroBuilder(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL_REWARD_API)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private static Retrofit retroBuilderApiAry(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl("http://private-ba5e76-bulkpurchase1.apiary-mock.com/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }


}
