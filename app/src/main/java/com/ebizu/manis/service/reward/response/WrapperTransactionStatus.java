package com.ebizu.manis.service.reward.response;

import com.ebizu.manis.model.purchasevoucher.Purchase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/7/17.
 */

public class WrapperTransactionStatus extends ResponseRewardApi {

    @SerializedName("data")
    @Expose
    private Purchase purchase;

    public Purchase getPurchase() {
        return purchase;
    }
}
