package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Reward;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Raden on 11/16/17.
 */

public class WrapperVoucherAPI {

    @SerializedName("errorCode")
    @Expose
    private int errorCode;

    @SerializedName("time")
    @Expose
    private double time;

    @SerializedName("data")
    @Expose
    private List<Reward> data = null;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public List<Reward> getData() {
        return data;
    }

    public void setData(List<Reward> data) {
        this.data = data;
    }
}
