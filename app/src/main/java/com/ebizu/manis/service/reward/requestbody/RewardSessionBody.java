package com.ebizu.manis.service.reward.requestbody;

import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.RewardRegisterBody;
import com.ebizu.manis.service.reward.requestbody.require.RewardApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/26/17.
 */

public class RewardSessionBody extends RewardApiBody {

    @SerializedName("data")
    @Expose
    private Data data;

    public RewardSessionBody(RewardSessionBody.Data data) {
        super();
        this.data = data;
        this.module = RewardApiConfig.Module.SESSION;
        this.action = RewardApiConfig.Action.GET_SESSION;
    }

    public static class Data {
        @SerializedName("userId")
        @Expose
        private String userId;

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }
}
