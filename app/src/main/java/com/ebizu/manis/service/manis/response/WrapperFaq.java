package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Faq;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by abizu-alvio on 7/31/2017.
 */

public class WrapperFaq extends ResponseData {

    @SerializedName("data")
    @Expose
    private List<Faq> data = null;

    public List<Faq> getFaq() {
        return data;
    }
}
