package com.ebizu.manis.service.reward.requestbody;

import android.content.Context;

import com.ebizu.manis.sdk.rest.data.RewardLoginSessionPost;
import com.ebizu.manis.sdk.rest.data.VoucherTermPost;
import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/4/17.
 */

public class RedeemPinTncBody extends RewardSessionApiBody {

    @SerializedName("data")
    @Expose
    private RedeemPinTncBody.Data data;

    public RedeemPinTncBody(Context context, RedeemPinTncBody.Data data) {
        super(context);
        this.data = data;
        this.module = RewardApiConfig.Module.VOUCHER;
        this.action = RewardApiConfig.Action.BRAND_TERMS;
    }


    public static class Data {

        private String voucherId;

        public String getVoucherId() {
            return voucherId;
        }

        public void setVoucherId(String voucherId) {
            this.voucherId = voucherId;
        }
    }

    /*public static class RequestBody {

        public Data data;
        public String session;
        public String module;
        public String action;
        public String token;

        public RequestBody(Data data, String session, String module, String action, String token) {
            this.data = data;
            this.session = session;
            this.module = module;
            this.action = action;
            this.token = token;
        }

        public static class Data {

            public Data(String voucherId) {
                this.voucherId = voucherId;
            }

            private String voucherId;
        }
    }*/
}
