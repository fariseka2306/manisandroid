package com.ebizu.manis.service.manis.requestbody;

import com.ebizu.manis.model.DataAuth;
import com.ebizu.manis.sdk.rest.data.AuthSignIn;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 16/07/2017.
 */

public class DataFacebookAuth extends DataAuth {

    @SerializedName("facebook")
    AuthSignIn.RequestBody.Facebook facebook;

    public void setFacebook(AuthSignIn.RequestBody.Facebook facebook) {
        this.facebook = facebook;
    }
}
