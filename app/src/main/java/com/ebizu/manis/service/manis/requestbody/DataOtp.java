package com.ebizu.manis.service.manis.requestbody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 28/07/2017.
 */

public class DataOtp {
    @SerializedName("handphone_no")
    @Expose
    private String handphoneNo;
    @SerializedName("module")
    @Expose
    private String module;

    public String getHandphoneNo() {
        return handphoneNo;
    }

    public void setHandphoneNo(String handphoneNo) {
        this.handphoneNo = handphoneNo;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

}
