package com.ebizu.manis.service.manis.key;

import android.content.Context;
import android.util.Base64;

import com.ebizu.manis.preference.KeySession;

/**
 * Created by Ebizu-User on 14/07/2017.
 */

public class ManisKeyGenerator {

    private RSACrypto rsaCrypto;
    private KeySession keySession;

    private static ManisKeyGenerator instance;

    public static ManisKeyGenerator getInstance(Context context) {
        if (null == instance) {
            instance = new ManisKeyGenerator(context);
        }
        return instance;
    }

    public ManisKeyGenerator(Context context) {
        keySession = new KeySession(context);
    }

    public String getPublicKeyApi() {
        if (keySession.haveKey()) {
            return keySession.getPublicKey();
        } else {
            rsaCrypto = com.ebizu.manis.service.manis.key.RSACrypto.getInstance();
            return Base64.encodeToString(rsaCrypto.getPublicKey().getEncoded(), Base64.NO_WRAP);
        }
    }

    public String getPrivateKeyApi() {
        if (keySession.haveKey()) {
            return keySession.getPrivateKey();
        } else {
            rsaCrypto = com.ebizu.manis.service.manis.key.RSACrypto.getInstance();
            return Base64.encodeToString(rsaCrypto.getPrivateKey().getEncoded(), Base64.NO_WRAP);
        }
    }
}