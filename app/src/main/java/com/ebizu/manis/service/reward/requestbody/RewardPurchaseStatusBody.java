package com.ebizu.manis.service.reward.requestbody;

import android.content.Context;

import com.ebizu.manis.model.purchasevoucher.CustomerDetails;
import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/4/17.
 */

public class RewardPurchaseStatusBody extends RewardSessionApiBody {

    @SerializedName("data")
    @Expose
    private Data data;

    public RewardPurchaseStatusBody(Context context) {
        super(context);
        this.module = RewardApiConfig.Module.VOUCHER;
        this.action = RewardApiConfig.Action.PURCHASE_STATUS;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("orderId")
        @Expose
        private String orderId;
        @SerializedName("transactionStatus")
        @Expose
        private String transactionStatus;
        @SerializedName("paymentGatewayName")
        @Expose
        private String paymentGatewayName;
        @SerializedName("grossAmount")
        @Expose
        private double grossAmount;
        @SerializedName("transactionTime")
        @Expose
        private String transactionTime;
        @SerializedName("statusMessage")
        @Expose
        private String statusMessage;
        @SerializedName("customer")
        @Expose
        private CustomerDetails customer;
        @SerializedName("paymentType")
        @Expose
        private String paymentType;
        @SerializedName("statusCode")
        @Expose
        private String statusCode;

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public void setTransactionStatus(String transactionStatus) {
            this.transactionStatus = transactionStatus;
        }

        public void setPaymentGatewayName(String paymentGatewayName) {
            this.paymentGatewayName = paymentGatewayName;
        }

        public void setGrossAmount(double grossAmount) {
            this.grossAmount = grossAmount;
        }

        public void setTransactionTime(String transactionTime) {
            this.transactionTime = transactionTime;
        }

        public void setStatusMessage(String statusMessage) {
            this.statusMessage = statusMessage;
        }

        public void setCustomer(CustomerDetails customer) {
            this.customer = customer;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }
    }

}
