package com.ebizu.manis.service.manis.requestbodyv2.bodysuper;

import android.content.Context;

import com.ebizu.manis.model.snap.Photo;
import com.ebizu.manis.service.manis.headerv2.Header;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 12/18/17.
 */

public class RequestBody {

    @SerializedName("function")
    @Expose
    public String function;

    @SerializedName("command")
    @Expose
    public String command;

    @SerializedName("param")
    @Expose
    protected RequestBody.Param param;

    public RequestBody(Context context) {
        this.command = "Utilities";
        this.param = new RequestBody.Param(context);
    }

    public void addData(Object data) {
        this.param.addData(data);
    }

    public void setPhoto(Photo photo) {
        this.param.setPhoto(photo);
    }

    public void discardHeaderToken() {
        param.discardHeaderToken();
    }

    public class Param {

        @SerializedName("header")
        @Expose
        private Header header;

        @SerializedName("data")
        private Object data;

        @SerializedName("photo")
        private Photo photo;

        public Param(Context context) {
            this.header = new Header(context);
        }

        public void addData(Object data) {
            this.data = data;
        }

        public void setPhoto(Photo photo) {
            this.photo = photo;
        }

        public void discardHeaderToken() {
            header.discardHeaderToken();
        }

    }


}
