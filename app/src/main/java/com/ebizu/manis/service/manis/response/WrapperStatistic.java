package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Statistic;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by abizu-alvio on 7/10/2017.
 */

public class WrapperStatistic {

    @SerializedName("statistic")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public class Data{
        @SerializedName("total")
        @Expose
        private Double total;
        @SerializedName("data")
        @Expose
        private List<Statistic> statisticList = null;

        public Double getTotal() {
            return total;
        }

        public List<Statistic> getStatisticList() {
            return statisticList;
        }
    }

}
