package com.ebizu.manis.service.manis.responsev2.wrapper;

import com.ebizu.manis.model.missionspost.MissionsPost;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WrapperMissionExecute extends ResponseWrapper{

    @SerializedName("data")
    @Expose
    private MissionsPost missionsPost;

    public MissionsPost getMissionsPost() {
        return missionsPost;
    }
}
