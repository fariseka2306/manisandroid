package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.snap.SnapStoreResult;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public class WrapperSnapStore extends ResponseData {

    @SerializedName("data")
    SnapStoreResult snapStoreResult;

    public SnapStoreResult getSnapStoreResult() {
        return snapStoreResult;
    }
}
