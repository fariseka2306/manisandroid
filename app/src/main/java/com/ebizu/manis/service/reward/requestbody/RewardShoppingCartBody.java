package com.ebizu.manis.service.reward.requestbody;

import android.content.Context;

import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/29/17.
 */

public class RewardShoppingCartBody extends RewardSessionApiBody {

    @SerializedName("data")
    @Expose
    private Data data;

    public RewardShoppingCartBody(Context context, Data data) {
        super(context);
        this.module = RewardApiConfig.Module.VOUCHER;
        this.action = RewardApiConfig.Action.ORDER_DETAIL;
        this.data = data;
    }

    public static class Data {
        @SerializedName("voucherId")
        @Expose
        private String voucherId;
        @SerializedName("bulkId")
        @Expose
        private String bulkId;
        @SerializedName("qty")
        @Expose
        private Integer qty;

        public void setVoucherId(String voucherId) {
            this.voucherId = voucherId;
        }

        public void setBulkId(String bulkId) {
            this.bulkId = bulkId;
        }

        public void setQty(Integer qty) {
            this.qty = qty;
        }
    }
}
