package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.snap.SnapData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Raden on 7/7/17.
 */

public class WrapperSnap extends ResponseData {

    @SerializedName("data")
    @Expose
    ArrayList<SnapData> snapDatas;

    public ArrayList<SnapData> getSnapDatas() {
        return snapDatas;
    }
}
