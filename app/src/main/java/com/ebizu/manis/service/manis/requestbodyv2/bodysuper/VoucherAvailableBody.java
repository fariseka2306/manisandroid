package com.ebizu.manis.service.manis.requestbodyv2.bodysuper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 11/16/17.
 */

public class VoucherAvailableBody {

    @SerializedName("session")
    @Expose
    protected String session;

    @SerializedName("module")
    @Expose
    protected String module;

    @SerializedName("action")
    @Expose
    protected String action;

    @SerializedName("token")
    @Expose
    protected String token;
}
