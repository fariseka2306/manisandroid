package com.ebizu.manis.service.manis.requestbody;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 04/08/2017.
 */

public class SnapStoreBody {

    @SerializedName("page")
    private int page;
    @SerializedName("size")
    private int size;
    @SerializedName("multiplier")
    private int multiplier;

    public void setPage(int page) {
        this.page = page;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }
}
