package com.ebizu.manis.service.manis.requestbodyv2.body;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.ManisRequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.data.ThematicPostData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/24/17.
 */

public class ThematicPostRB extends ManisRequestBody {

    @SerializedName("data")
    private ThematicPostData data;

    public void setData(ThematicPostData data) {
        this.data = data;
    }

    public ThematicPostRB() {
        module = ConfigManager.Missions.THEMATIC_MODULE;
        action = ConfigManager.Missions.THEMATIC_ACTION;
    }

}
