package com.ebizu.manis.service.reward.response;

import com.ebizu.manis.model.redeempinvalidation.RedeemPinValidation;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 20/11/17.
 */

public class WrapperPinValidation extends ResponseRewardApi {

    @SerializedName("data")
    @Expose
    private RedeemPinValidation redeemPinValidation;

    public RedeemPinValidation getRedeemPinValidation() {
        return redeemPinValidation;
    }
}
