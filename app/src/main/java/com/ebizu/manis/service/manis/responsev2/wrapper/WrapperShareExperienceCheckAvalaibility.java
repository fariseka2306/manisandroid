package com.ebizu.manis.service.manis.responsev2.wrapper;

import com.ebizu.manis.model.shareexpeeriencecheckavailability.ShareExperienceCheckAvalaibility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by FARIS_mac on 10/12/17.
 */

public class WrapperShareExperienceCheckAvalaibility extends ResponseWrapper {

    @SerializedName("data")
    @Expose
    private ShareExperienceCheckAvalaibility shareExperienceCheckAvalaibilityList;

    public ShareExperienceCheckAvalaibility getShareExperienceCheckAvalaibilityList() {
        return shareExperienceCheckAvalaibilityList;
    }
}
