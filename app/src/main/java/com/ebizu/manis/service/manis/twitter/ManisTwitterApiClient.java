package com.ebizu.manis.service.manis.twitter;

import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;

public class ManisTwitterApiClient extends TwitterApiClient {

    public ManisTwitterApiClient(TwitterSession session) {
        super(session);
    }

    /**
     * Provide CustomService with defined endpoints
     */
    public CustomTwitterApi getCustomService() {
        return getService(CustomTwitterApi.class);
    }
}
