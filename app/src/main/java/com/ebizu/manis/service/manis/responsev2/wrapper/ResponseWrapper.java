package com.ebizu.manis.service.manis.responsev2.wrapper;

import com.ebizu.manis.service.manis.responsev2.meta.Meta;
import com.ebizu.manis.service.manis.responsev2.status.Status;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class ResponseWrapper {

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("status")
    @Expose
    private Status status;

    public Meta getMeta() {
        return meta;
    }

    public Status getStatus() {
        return status;
    }
}
