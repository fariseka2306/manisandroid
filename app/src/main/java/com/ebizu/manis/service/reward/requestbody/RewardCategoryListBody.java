package com.ebizu.manis.service.reward.requestbody;

import android.content.Context;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardApiBody;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class RewardCategoryListBody extends RewardSessionApiBody {

    @SerializedName("data")
    @Expose
    private Data data;

    public RewardCategoryListBody(Context context, Data data) {
        super(context);
        this.data = data;
        this.module = RewardApiConfig.Module.CATEGORY;
        this.action = RewardApiConfig.Action.LIST;
    }


    public static class Data {
        @SerializedName("all")
        @Expose
        private Boolean all;
        @SerializedName("noCache")
        @Expose
        private String noCache;

        public Boolean getAll() {
            return all;
        }

        public void setAll(Boolean all) {
            this.all = all;
        }

        public String getNoCache() {
            return noCache;
        }

        public void setNoCache(String noCache) {
            this.noCache = noCache;
        }
    }
}
