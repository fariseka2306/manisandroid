package com.ebizu.manis.service.reward.requestbody;

import android.content.Context;

import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class RewardBulkListBody extends RewardSessionApiBody {

    @SerializedName("data")
    @Expose
    private Data data;

    public RewardBulkListBody(Context context, Data data) {
        super(context);
        this.data = data;
        this.module = RewardApiConfig.Module.VOUCHER;
        this.action = RewardApiConfig.Action.COMBO_DEALS;
    }

    public static class Data {
        @SerializedName("size")
        @Expose
        private Integer size;
        @SerializedName("page")
        @Expose
        private Integer page;
        @SerializedName("keyword")
        @Expose
        private String keyword;
        @SerializedName("order")
        @Expose
        private Integer order;
        @SerializedName("noCache")
        @Expose
        private String noCache;

        public void setSize(Integer size) {
            this.size = size;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public void setOrder(Integer order) {
            this.order = order;
        }

        public void setNoCache(String noCache) {
            this.noCache = noCache;
        }
    }

}
