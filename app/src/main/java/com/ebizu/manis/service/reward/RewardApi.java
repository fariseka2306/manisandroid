package com.ebizu.manis.service.reward;

import com.ebizu.manis.service.manis.requestbody.PurchaseHistoryBody;
import com.ebizu.manis.service.manis.response.WrapperBrandTerms;
import com.ebizu.manis.service.reward.requestbody.RedeemPinTncBody;
import com.ebizu.manis.service.reward.requestbody.PinValidationBody;
import com.ebizu.manis.service.reward.requestbody.RewardBulkListBody;
import com.ebizu.manis.service.reward.requestbody.RewardCancelPaymentBody;
import com.ebizu.manis.service.reward.requestbody.RewardCategoryListBody;
import com.ebizu.manis.service.reward.requestbody.RewardMyVoucherBody;
import com.ebizu.manis.service.reward.requestbody.RewardPurchaseStatusBody;
import com.ebizu.manis.service.reward.requestbody.RewardRegisterBody;
import com.ebizu.manis.service.reward.requestbody.RewardSessionBody;
import com.ebizu.manis.service.reward.requestbody.RewardShoppingCartBody;
import com.ebizu.manis.service.reward.requestbody.RewardGeneralListBody;
import com.ebizu.manis.service.reward.response.ResponseRewardApi;
import com.ebizu.manis.service.reward.response.WrapperGetSessionNew;
import com.ebizu.manis.service.reward.response.WrapperMyVoucher;
import com.ebizu.manis.service.reward.response.WrapperPinValidation;
import com.ebizu.manis.service.reward.response.WrapperPurchaseHistory;
import com.ebizu.manis.service.reward.response.WrapperRegisterSession;
import com.ebizu.manis.service.reward.response.WrapperRegisterSessionNew;
import com.ebizu.manis.service.reward.response.WrapperRewardCategoryList;
import com.ebizu.manis.service.reward.response.WrapperRewardVoucherList;
import com.ebizu.manis.service.reward.response.WrapperShoppingCart;
import com.ebizu.manis.service.reward.response.WrapperTransactionStatus;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by ebizu on 9/26/17.
 */

public interface RewardApi {

    @POST("api")
    Observable<WrapperRegisterSession> registerSession(
            @Body RewardRegisterBody rewardRegisterBody);

//    @POST("api/Voucher/myVoucherAvailable")
//    Observable<WrapperRewardVoucherList> listMyVoucher(
//            @Body RewardMyVoucherBody rewardMyVoucherBody);

    @POST("api")
    Observable<WrapperRewardVoucherList> listMyVoucher(
            @Body RewardMyVoucherBody rewardMyVoucherBody);

    @POST("api")
    Observable<WrapperShoppingCart> getShoppingCart(
            @Body RewardShoppingCartBody rewardShoppingCartBody);

    @POST("api/Voucher/orderDetail+Physical")
    Observable<WrapperShoppingCart> getShoppingCartApiAry(
            @Body RewardShoppingCartBody rewardShoppingCartBody);

    @POST("api")
    Observable<WrapperBrandTerms> getBrandTerms
            (@Body RedeemPinTncBody redeemPinTncBody);

    @POST("api")
    Observable<WrapperPinValidation> getPinValidation
            (@Body PinValidationBody pinValidationBody);

    @POST("api")
    Observable<WrapperTransactionStatus> sendResultPurchaseStatus(
            @Body RewardPurchaseStatusBody rewardPurchaseStatusBody);

    @POST("/api/Voucher/purchaseStatus+PhysicalSingle&bulk")
    Observable<WrapperTransactionStatus> sendResultPurchaseStatusApiAry(
            @Body RewardPurchaseStatusBody rewardPurchaseStatusBody);

    @POST("api")
    Observable<WrapperPurchaseHistory> listPurchaseHistory(
            @Body PurchaseHistoryBody purchaseHistoryBody);

    @POST("api")
    Observable<WrapperRegisterSessionNew> registerSessionRewardNew
            (@Body RewardRegisterBody rewardRegisterBody);

    @POST("api")
    Observable<WrapperGetSessionNew> getSession
            (@Body RewardSessionBody rewardSessionBody);

    @POST("api")
    Observable<WrapperRewardCategoryList> getRewardCategoryList
            (@Body RewardCategoryListBody rewardCategoryListBody);

    @POST("api")
    Observable<WrapperRewardVoucherList> getRewardVoucherList
            (@Body RewardGeneralListBody rewardGeneralListBody);

    @POST("api")
    Observable<WrapperRewardVoucherList> getRewardBulkList
            (@Body RewardBulkListBody rewardBulkListBody);

    @POST("api/Voucher/comboDeals")
    Observable<WrapperRewardVoucherList> getRewardBulkListApiAry
            (@Body RewardBulkListBody rewardBulkListBody);

    @POST("api")
    Observable<WrapperShoppingCart> cancelPayment(
            @Body RewardCancelPaymentBody rewardCancelPaymentBody);

}
