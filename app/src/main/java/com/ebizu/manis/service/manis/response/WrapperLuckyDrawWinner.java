package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.LuckyDrawWinnerData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 8/6/17.
 */

public class WrapperLuckyDrawWinner extends ResponseData{

    @SerializedName("data")
    @Expose
    LuckyDrawWinnerData luckyDrawWinnerData;

    public LuckyDrawWinnerData getLuckyDrawWinnerData() {
        return luckyDrawWinnerData;
    }
}
