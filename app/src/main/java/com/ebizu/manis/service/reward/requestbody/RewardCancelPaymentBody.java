package com.ebizu.manis.service.reward.requestbody;

import android.content.Context;

import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/29/17.
 */

public class RewardCancelPaymentBody extends RewardSessionApiBody {

    @SerializedName("data")
    @Expose
    private Data data;

    public RewardCancelPaymentBody(Context context, Data data) {
        super(context);
        module = RewardApiConfig.Module.VOUCHER;
        action = RewardApiConfig.Action.CANCEL_PAYMENT;
        this.data = data;
    }

    public static class Data {

        @SerializedName("orderId")
        @Expose
        private String orderId;
        @SerializedName("purchaseId")
        @Expose
        private String purchaseId;

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public void setPurchaseId(String purchaseId) {
            this.purchaseId = purchaseId;
        }
    }
}
