package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by Halim on 9/23/17.
 */

public class RewardBody {

    private String session;
    private String module;
    private String action;
    private String token;

    public RewardBody(String session, String module, String action, String token) {
        this.session = session;
        this.module = module;
        this.action = action;
        this.token = token;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
