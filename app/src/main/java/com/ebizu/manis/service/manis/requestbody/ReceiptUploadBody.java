package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class ReceiptUploadBody {

    private String number;
    private Integer amount;
    private String date;

    public ReceiptUploadBody(String number, Integer amount, String date) {
        this.number = number;
        this.amount = amount;
        this.date = date;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
