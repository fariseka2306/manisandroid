package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.ViewHistoryResult;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 7/24/17.
 */

public class WrapperViewHistory extends ResponseData {

    @SerializedName("data")
    @Expose
    ViewHistoryResult viewHistoryResult;

    public ViewHistoryResult getViewHistoryResult() {
        return viewHistoryResult;
    }
}
