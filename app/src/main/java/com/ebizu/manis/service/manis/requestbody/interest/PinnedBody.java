package com.ebizu.manis.service.manis.requestbody.interest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ebizu-User on 24/07/2017.
 */

public class PinnedBody {

    @SerializedName("id")
    List<Integer> ids;

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }
}
