package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.InterestStore;
import com.ebizu.manis.model.InterestsStore;
import com.ebizu.manis.model.StoreResults;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Halim on 7/23/17.
 */

public class WrapperInterestStore extends ResponseData {

    @SerializedName("data")
    @Expose
    StoreResults storeResults;

    public StoreResults getStoreResults() {
        return storeResults;
    }
}
