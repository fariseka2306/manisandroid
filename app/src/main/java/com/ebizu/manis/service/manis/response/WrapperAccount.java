package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Account;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 7/7/2017.
 */

public class WrapperAccount extends ResponseData {

    @SerializedName("data")
    @Expose
    Account account;

    public Account getAccount() {
        return account;
    }
}