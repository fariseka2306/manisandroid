package com.ebizu.manis.service.reward.requestbody.require;

import android.content.Context;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.preference.RewardSession;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/2/17.
 */

public class RewardSessionApiBody {

    private RewardSession rewardSession;

    @SerializedName("module")
    @Expose
    protected String module;
    @SerializedName("action")
    @Expose
    protected String action;
    @SerializedName("token")
    @Expose
    protected String token;
    @SerializedName("session")
    protected String session ;

    public RewardSessionApiBody(Context context) {
        this.token = BuildConfig.REWARD_KEY;
        if (null == rewardSession)
            rewardSession = new RewardSession(context);
        this.session = rewardSession.getSession();
    }
}
