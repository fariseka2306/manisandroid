package com.ebizu.manis.service.manis.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.Dummy;
import com.ebizu.manis.mvp.beacon.BeaconPresenter;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbody.BeaconPromoBody;

/**
 * Created by ebizu on 9/6/17.
 */

public class BeaconBroadcastReceiver extends BroadcastReceiver {

    private BeaconPresenter beaconPresenter;
    private BeaconPromoBody beaconPromoBody;

    @Override
    public void onReceive(Context context, Intent intent) {
        initializeComponent(context, intent);
        if (beaconPresenter != null)
            beaconPresenter.getBeaconPromos(ManisApiGenerator.createServiceWithToken(context), Dummy.getBeaconPromoBody());
//            beaconPresenter.getBeaconPromos(ManisApiGenerator.createServiceWithToken(context), beaconPromoBody);
    }

    private void initializeComponent(Context context, Intent intent) {
        initializeBeaconPresenter(context);
        getIntentData(intent);
    }

    private void getIntentData(Intent intent) {
        try {
            Bundle bundle = intent.getExtras();
            String uuid = bundle.getString(ConfigManager.Beacon.INTENT_STRING_UUID);
            String major = bundle.getString(ConfigManager.Beacon.INTENT_STRING_MAJOR);
            String minor = bundle.getString(ConfigManager.Beacon.INTENT_STRING_MINOR);
            String serial = uuid.concat("_").concat(major).concat("_").concat(minor);
            beaconPromoBody = new BeaconPromoBody();
            beaconPromoBody.setUuid(uuid);
            beaconPromoBody.setMajor(major);
            beaconPromoBody.setMinor(minor);
            beaconPromoBody.setSn(serial);
            Log.i("EBN-Campaign", "BEACON DETECTION " + beaconPromoBody.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeBeaconPresenter(Context context) {
        if (beaconPresenter == null)
            beaconPresenter = new BeaconPresenter(context);
    }

}
