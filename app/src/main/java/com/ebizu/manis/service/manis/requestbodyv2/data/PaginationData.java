package com.ebizu.manis.service.manis.requestbodyv2.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 12/18/17.
 */

public class PaginationData {
    @SerializedName("page")
    @Expose
    private String page = "1";
    @SerializedName("size")
    @Expose
    private String size = "20";

    public PaginationData() {
        this.size = "20";
    }

    public void setPage(String page) {
        this.page = page;
    }
}
