package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.BrandTerms;
import com.ebizu.manis.model.Point;
import com.ebizu.manis.service.reward.response.ResponseRewardApi;
import com.ebizu.manis.service.reward.response.ResponseRewardApiSecond;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/4/17.
 */

public class WrapperBrandTerms extends ResponseRewardApi {

    @SerializedName("data")
    @Expose
    BrandTerms brandTerms;

    public BrandTerms getBrandTerms() {
        return brandTerms;
    }
}
