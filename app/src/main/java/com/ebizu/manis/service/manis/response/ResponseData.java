package com.ebizu.manis.service.manis.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 7/3/17.
 */

public class ResponseData<Response> {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private Object message;
    @SerializedName("elapsed")
    @Expose
    private Integer elapsed;

    public Boolean getSuccess() {
        return success;
    }

    public Object getMessage() {
        return message;
    }

    public Integer getElapsed() {
        return elapsed;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "success=" + success +
                ", message=" + message +
                ", elapsed=" + elapsed +
                '}';
    }
}
