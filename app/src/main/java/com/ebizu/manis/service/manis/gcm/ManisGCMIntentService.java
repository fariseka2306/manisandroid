package com.ebizu.manis.service.manis.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.model.notification.snap.SnapHistory;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.view.manis.notification.GCMNotification;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import io.intercom.android.sdk.push.IntercomPushClient;

public class ManisGCMIntentService extends IntentService {

    public ManisGCMIntentService() {
        super("ManisGCMIntentService");
    }

    private final IntercomPushClient intercomPushClient = new IntercomPushClient();

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            Bundle extras = intent.getExtras();
            String app = extras.getString(ConfigManager.GCM.INTENT_STRING_APP);
            String silent = extras.getString(ConfigManager.GCM.INTENT_STRING_SILENT);
            String type = extras.getString(ConfigManager.GCM.INTENT_STRING_TYPE);

            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
            String messageType = gcm.getMessageType(intent);

            if (!extras.isEmpty()) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType) && app != null) {
                    if (app.equalsIgnoreCase(getResources().getString(R.string.app_name))) {
                        if (silent.equalsIgnoreCase("true")) {
                            processSilentNotif(extras, type);
                        } else {
                            sendNotification(extras, type);
                            setResponseIntent();
                        }
                    }
                }
                onMessageReceivedIntercom(extras);
            }
        }
    }

    private void processSilentNotif(Bundle extras, String type) {
        ManisSession manisSession = new ManisSession(this);
        if (type != null && type.equals(ConfigManager.GCM.INTENT_STRING_POINT)) {
            String point = extras.getString(ConfigManager.GCM.INTENT_STRING_POINT);
            try {
                JSONObject jsonPoint = new JSONObject(point);
                String currentPoint = jsonPoint.getString(ConfigManager.GCM.INTENT_STRING_CURRENT);

                if (currentPoint != null) {
                    manisSession.setPoint(Integer.parseInt(currentPoint));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setResponseIntent();
        } else if (type != null && type.equals(ConfigManager.GCM.INTENT_STRING_ACCOUNT)) {
            String photo = extras.getString(ConfigManager.GCM.INTENT_STRING_ACC_PHOTO);
            manisSession.setPhoto(photo);
        }
    }

    private void setResponseIntent() {
        Intent responseIntent = new Intent(UtilStatic.GOT_NOTIF);
        sendOrderedBroadcast(responseIntent, null);
    }

    private void sendNotification(Bundle extras, String type) {
        String message = extras.getString(ConfigManager.GCM.INTENT_STRING_MESSAGE);
        String stringUri = extras.getString(ConfigManager.GCM.INTENT_STRING_URI);
        String status = extras.getString(ConfigManager.GCM.INTENT_STRING_STATUS);

        if (type != null) {
            if (type.equals(ConfigManager.GCM.INTENT_STRING_SNAP)) {
                Uri uri = Uri.parse(stringUri);
                String id = uri.getQueryParameter(ConfigManager.GCM.INTENT_STRING_ID);
                SnapHistory snapHistory = new SnapHistory(Long.parseLong(id), message, stringUri, status);
                GCMNotification mGCMNotification = new GCMNotification(this);
                mGCMNotification.showSnapNotification(snapHistory);
            } else if (type.equals(ConfigManager.GCM.INTENT_STRING_OFFER)) {
                // not implement yet
            }
        }
    }

    public void onMessageReceivedIntercom(Bundle message) {
        if (intercomPushClient.isIntercomPush(message)) {
            intercomPushClient.handlePush(getApplication(), message);
        } else {
            //DO HOST LOGIC HERE
        }
    }
}
