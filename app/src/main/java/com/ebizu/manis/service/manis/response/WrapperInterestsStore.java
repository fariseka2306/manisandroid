package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.InterestsStore;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by firef on 7/10/2017.
 */

public class WrapperInterestsStore extends ResponseData {

    @SerializedName("data")
    @Expose
    ArrayList<InterestsStore> interestsStores;

    public ArrayList<InterestsStore> getInterestsStores() {
        return interestsStores;
    }
}
