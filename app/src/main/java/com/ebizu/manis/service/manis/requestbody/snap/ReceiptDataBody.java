package com.ebizu.manis.service.manis.requestbody.snap;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 09/08/2017.
 */

public class ReceiptDataBody implements Parcelable {

    @SerializedName("receipt")
    private ReceiptSnap receiptSnap;
    @SerializedName("store")
    private ReceiptStore receiptStore;

    public void setReceiptSnap(ReceiptSnap receiptSnap) {
        this.receiptSnap = receiptSnap;
    }

    public void setReceiptStore(ReceiptStore receiptStore) {
        this.receiptStore = receiptStore;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.receiptSnap, flags);
        dest.writeParcelable(this.receiptStore, flags);
    }

    public ReceiptDataBody() {
    }

    protected ReceiptDataBody(Parcel in) {
        this.receiptSnap = in.readParcelable(ReceiptSnap.class.getClassLoader());
        this.receiptStore = in.readParcelable(ReceiptStore.class.getClassLoader());
    }

    public static final Creator<ReceiptDataBody> CREATOR = new Creator<ReceiptDataBody>() {
        @Override
        public ReceiptDataBody createFromParcel(Parcel source) {
            return new ReceiptDataBody(source);
        }

        @Override
        public ReceiptDataBody[] newArray(int size) {
            return new ReceiptDataBody[size];
        }
    };
}
