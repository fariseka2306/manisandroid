package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.LuckyDrawEntryData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 08/08/17.
 */

public class WrapperLuckyDrawEntry extends ResponseData {

    @SerializedName("data")
    @Expose
    LuckyDrawEntryData luckyDrawEntryData;

    public LuckyDrawEntryData getLuckyDrawEntryData() {
        return luckyDrawEntryData;
    }
}
