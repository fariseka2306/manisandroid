package com.ebizu.manis.service.manis;

import com.ebizu.manis.model.Redeem;
import com.ebizu.manis.service.manis.requestbody.RewardCategoryBody;
import com.ebizu.manis.service.manis.requestbody.VersioningBody;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperienceCheckAvalaibilityRB;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperienceMissionsRB;
import com.ebizu.manis.service.manis.requestbodyv2.body.ShareExperiencePostRB;
import com.ebizu.manis.service.manis.requestbodyv2.body.ThematicPostRB;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.service.manis.response.WrapperAccount;
import com.ebizu.manis.service.manis.response.WrapperBeaconPromo;
import com.ebizu.manis.service.manis.response.WrapperBrand;
import com.ebizu.manis.service.manis.response.WrapperDataStatistic;
import com.ebizu.manis.service.manis.response.WrapperFaq;
import com.ebizu.manis.service.manis.response.WrapperInterest;
import com.ebizu.manis.service.manis.response.WrapperInterestStore;
import com.ebizu.manis.service.manis.response.WrapperInterestsStore;
import com.ebizu.manis.service.manis.response.WrapperInterval;
import com.ebizu.manis.service.manis.response.WrapperInviteTerm;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawCheckSnapable;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawEntry;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawScreen;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawTerm;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawUpload;
import com.ebizu.manis.service.manis.response.WrapperLuckyDrawWinner;
import com.ebizu.manis.service.manis.response.WrapperNotifications;
import com.ebizu.manis.service.manis.response.WrapperOfferClaim;
import com.ebizu.manis.service.manis.response.WrapperPin;
import com.ebizu.manis.service.manis.response.WrapperPoint;
import com.ebizu.manis.service.manis.response.WrapperPrivacy;
import com.ebizu.manis.service.manis.response.WrapperRedeemCloseFailed;
import com.ebizu.manis.service.manis.response.WrapperRedemptionHistory;
import com.ebizu.manis.service.manis.response.WrapperReward;
import com.ebizu.manis.service.manis.response.WrapperRewardRedeem;
import com.ebizu.manis.service.manis.response.WrapperSaved;
import com.ebizu.manis.service.manis.response.WrapperSearchStore;
import com.ebizu.manis.service.manis.response.WrapperSnap;
import com.ebizu.manis.service.manis.response.WrapperSnapDetails;
import com.ebizu.manis.service.manis.response.WrapperSnapEarnTicketTerms;
import com.ebizu.manis.service.manis.response.WrapperSnapStore;
import com.ebizu.manis.service.manis.response.WrapperSnapStoreSuggest;
import com.ebizu.manis.service.manis.response.WrapperSnapTerms;
import com.ebizu.manis.service.manis.response.WrapperSnapable;
import com.ebizu.manis.service.manis.response.WrapperStore;
import com.ebizu.manis.service.manis.response.WrapperStoreDetail;
import com.ebizu.manis.service.manis.response.WrapperTerm;
import com.ebizu.manis.service.manis.response.WrapperUnpin;
import com.ebizu.manis.service.manis.response.WrapperVersioning;
import com.ebizu.manis.service.manis.response.WrapperViewHistory;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperMissionExecute;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperMissions;
import com.ebizu.manis.service.manis.responsev2.wrapper.WrapperShareExperienceCheckAvalaibility;
import com.ebizu.manis.service.reward.requestbody.RewardBulkListBody;
import com.ebizu.manis.service.reward.response.WrapperRewardVoucherList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Raden on 7/3/17.
 */

public interface ManisApi {

    @GET("v1/account/point")
    Observable<WrapperPoint> getPoint();

    @GET("v1/snap/store")
    Observable<WrapperPoint> getStores(
            @Query("data") String data);

    @GET("v1/account/get")
    Observable<WrapperAccount> getAccount();

    @FormUrlEncoded
    @POST("v1/auth/signin")
    Observable<WrapperAccount> signInFacebook(@Field("data") String dataJson);

    @FormUrlEncoded
    @POST("v1/auth/signin/google")
    Observable<WrapperAccount> signInGoogle(@Field("data") String dataJson);

    @FormUrlEncoded
    @POST("v1/auth/signin/phone")
    Observable<WrapperAccount> signInPhone(@Field("data") String dataJson);

    @FormUrlEncoded
    @POST("v1/signup/otp")
    Observable<ResponseData> signUpOtp(@Field("data") String dataJson);

    @GET("v1/stores/nearby")
    Observable<WrapperStore> getStoreNearby(@Query("data") String data);

    @FormUrlEncoded
    @POST("v1/stores/search")
    Observable<WrapperSearchStore> getSearchStore(
            @Field("data") String dataJson);

    @GET("v1/stores/interests")
    Observable<WrapperInterestsStore> getInterestsStore();

    @GET("v1/stores/interest")
    Observable<WrapperInterestStore> getInterestStore(
            @Query("data") String data);

    @GET("v1/stores/detail")
    Observable<WrapperStoreDetail> getStoreDetail(
            @Query("data") String data);

    @FormUrlEncoded
    @POST("v1/stores/pin")
    Observable<WrapperPin> getPinnedFollower(
            @Field("data") String dataJson);

    @FormUrlEncoded
    @POST("v1/stores/unpin")
    Observable<WrapperUnpin> getUnpinnedFollower(
            @Field("data") String dataJson);

    @FormUrlEncoded
    @POST("v1/stores/offer/pin")
    Observable<ResponseData> getOfferPinned(
            @Field("data") String dataJson);

    @FormUrlEncoded
    @POST("v1/stores/offer/unpin")
    Observable<ResponseData> getOfferUnpinned(
            @Field("data") String dataJson);

    @FormUrlEncoded
    @POST("v1/stores/offer/redeem")
    Observable<WrapperOfferClaim> postClaimOffer(
            @Field("data") String dataJson);

    @GET("v1/luckydraw/screen")
    Observable<WrapperLuckyDrawScreen> getLuckyDrawScreen();

    @GET("v1/luckydraw/winners")
    Observable<WrapperLuckyDrawWinner> getLuckyDrawWinner(
            @Query("data") String data);

    @GET("v1/luckydraw/entries")
    Observable<WrapperLuckyDrawEntry> getLuckyDrawEntry(
            @Query("data") String data);

    @FormUrlEncoded
    @POST("v1/luckydraw/upload")
    Observable<WrapperLuckyDrawUpload> getLuckyDrawUpload(
            @Field("data") String luckyDrawData,
            @Field("photo") String luckyDrawPhoto);

    @GET("v1/luckydraw/terms")
    Observable<WrapperLuckyDrawTerm> getLuckyDrawTerm();

    @GET("v1/luckydraw/checksnapable")
    Observable<WrapperLuckyDrawCheckSnapable> getLuckyDrawCheckSnapable(
            @Query("data") String data);

    @POST("api")
    Observable<WrapperReward> getListReward(
            @Body RewardCategoryBody rewardCategoryBody);

    @GET("v1/brands/featured")
    Observable<WrapperBrand> getBrands();

    @GET("v1/snap/last")
    Observable<WrapperSnap> getSnap();

    @GET("v1/snap/last")
    Observable<WrapperSnapDetails> getSnapDetails();

    @GET("v1/snap/statistic")
    Observable<WrapperDataStatistic> getStatistic(
            @Query("data") String data);

    @FormUrlEncoded
    @POST("v1/snap/stores/search")
    Observable<WrapperSnapStore> getBrandDetailList(@Field("data") String data);

    @GET("v1/snap/history")
    Observable<WrapperViewHistory> getViewHistory(@Query("data") String data);

    @FormUrlEncoded
    @POST("v1/interests/update/bulk")
    Observable<ResponseData> saveInterest(@Field("data") String data);

    @GET("v1/interests")
    Observable<WrapperInterest> getInterests();

    @GET("v1/misc/help/legal")
    Observable<WrapperTerm> getTerm();

    @GET("v1/misc/help/legal")
    Observable<WrapperPrivacy> getPrivacy();

    @GET("v1/account/settings/notifications")
    Observable<WrapperNotifications> getNotifications();

    @FormUrlEncoded
    @POST("v1/account/settings/notifications/update")
    Observable<WrapperNotifications> saveAccountNotifications(@Field("data") String dataJson);

    @FormUrlEncoded
    @POST("v1/account/profile/update")
    Observable<WrapperAccount> saveAccountProfile(@Field("data") String dataJson);

    @Multipart
    @POST("v1/account/profile/picture/update")
    Observable<WrapperAccount> saveProfilePhoto(@Part MultipartBody.Part file);

    @GET("v1/misc/help")
    Observable<WrapperFaq> getFaq();

    @GET("v1/rewards/claimed")
    Observable<WrapperRedemptionHistory> getRedemptionHistory(@Query("data") String data);

    @GET("v1/account/invite/terms")
    Observable<WrapperInviteTerm> getInviteTerm();

    @GET("v1/saved")
    Observable<WrapperSaved> getSaved();

    @POST("VersionDev")
    Observable<WrapperVersioning> getVersioning(@Body VersioningBody dataJson);

    @GET("v1/review")
    Observable<ResponseData> getReviewStatus();

    @FormUrlEncoded
    @POST("v1/review/post")
    Observable<ResponseData> saveReview(@Field("data") String dataJson);

    @GET("v1/stores/nearby")
    Observable<WrapperSnapStore> getSnapStoresNearby(@Query("data") String data);

    @GET("v1/snap/stores/recent")
    Observable<WrapperSnapStore> getSnapStoresRecent(@Query("data") String data);

    @GET("v1/snap/suggestions")
    Observable<WrapperSnapStoreSuggest> getSnapStoresSuggest(@Query("data") String data);

    @FormUrlEncoded
    @POST("v1/snap/stores/search")
    Observable<WrapperSnapStore> findMerchantStore(@Field("data") String data);

    @Multipart
    @POST("v1/snap/receipt/upload")
    Observable<ResponseData> uploadReceiptImage(
            @Part("data") RequestBody description,
            @Part MultipartBody.Part file);

    @Multipart
    @POST("v1/luckydraw/upload")
    Observable<WrapperLuckyDrawUpload> uploadReceiptImageLuckyDraw(
            @Part("data") RequestBody description,
            @Part MultipartBody.Part file);

    @GET("v1/beacon/promo/nearby")
    Observable<WrapperBeaconPromo> getBeaconPromos(@Query("data") String data);

    @GET("v1/beacon/interval")
    Observable<WrapperInterval> getIntervalBeacon();

    @FormUrlEncoded
    @POST("v1/rewards/redeem/new")
    Observable<WrapperRewardRedeem> getRewardRedeem(@Field("data") String dataJson);

    @FormUrlEncoded
    @POST("v1/rewards/redeem/close/new")
    Observable<ResponseData> getRedeemClose(@Field("data") String dataJson);

    @FormUrlEncoded
    @POST("v1/rewards/redeem/close/failed/new")
    Observable<WrapperRedeemCloseFailed> getRedeemCloseFailed(@Field("data") String dataJson);

    @GET("v1/snap/checksnapable")
    Observable<WrapperSnapable> isSnapable(@Query("data") String data);

    @GET("v1/luckydraw/terms")
    Observable<WrapperSnapEarnTicketTerms> getTermsSnapEarnTicket();

    @GET("v1/snap/terms")
    Observable<WrapperSnapTerms> getTermsSnapEarnPoint();

    @POST
    Observable<WrapperMissions> getMissions(
            @Url String url,
            @Body ShareExperienceMissionsRB shareExperienceMissionsRB);

    @POST
    Observable<WrapperShareExperienceCheckAvalaibility> checkAvailabilityMission(
            @Url String url,
            @Body ShareExperienceCheckAvalaibilityRB shareExperienceCheckAvalaibilityRB);

    @POST
    Observable<WrapperMissionExecute> postShareExperience(
            @Url String url,
            @Body ShareExperiencePostRB shareExperiencePostRB);

    @POST
    Observable<WrapperMissionExecute> postThematic(
            @Url String url,
            @Body ThematicPostRB thematicPostRB);

    @POST("mobile/Mission/executeWithPin")
    Observable<WrapperMissionExecute> getThematic(@Body ThematicPostRB dataJson);

    @POST("mobile/Mission/list")
    Observable<WrapperMissions> getMissionApiary(
            @Body ShareExperienceMissionsRB shareExperienceMissionsRB);

    @POST("mobile/Mission/checkAvailability")
    Observable<WrapperShareExperienceCheckAvalaibility> checkAvailabilityApiary(
            @Body ShareExperienceCheckAvalaibilityRB shareExperienceCheckAvalaibilityRB);

    @POST("mobile/Mission/execute")
    Observable<WrapperMissionExecute> getShareExperiencePostApiary(
            @Body ShareExperiencePostRB shareExperiencePostRB);

//    @POST("/api/Voucher/myVoucherAvailable")
//    Observable<>

}