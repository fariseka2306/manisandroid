package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class LuckyDrawUploadBody {

    ReceiptUploadBody receiptUploadBody;

    public StoreUploadBody getStoreUploadBody() {
        return storeUploadBody;
    }

    public void setStoreUploadBody(StoreUploadBody storeUploadBody) {
        this.storeUploadBody = storeUploadBody;
    }

    StoreUploadBody storeUploadBody;

    public ReceiptUploadBody getReceiptUploadBody() {
        return receiptUploadBody;
    }

    public void setReceiptUploadBody(ReceiptUploadBody receiptUploadBody) {
        this.receiptUploadBody = receiptUploadBody;
    }
}
