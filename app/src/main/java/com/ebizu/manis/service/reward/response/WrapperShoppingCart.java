package com.ebizu.manis.service.reward.response;

import com.ebizu.manis.model.purchasevoucher.ShoppingCart;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/29/17.
 */

public class WrapperShoppingCart extends ResponseRewardApi {

    @SerializedName("data")
    @Expose
    WrapperShoppingCart.Data data;

    public Data getData() {
        return data;
    }

    public class Data {

        @SerializedName("shoppingCart")
        @Expose
        ShoppingCart shoppingCart;

        public ShoppingCart getShoppingCart() {
            return shoppingCart;
        }
    }
}
