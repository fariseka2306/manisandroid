package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.snap.SnapTerm;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/12/17.
 */

public class WrapperSnapTerms extends ResponseData {

    @SerializedName("data")
    @Expose
    private SnapTerm snapTerm;

    public SnapTerm getSnapTerm() {
        return snapTerm;
    }
}
