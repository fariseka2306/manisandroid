package com.ebizu.manis.service.manis.requestbody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firef on 7/7/2017.
 */

public class StoreBody {

    private int page;
    private int size;
    private int multiplier;

    @SerializedName("merchant_tier")
    @Expose
    private String merchantTier;

    public StoreBody() {

    }

    public StoreBody(int page, int size, int multiplier, String merchantTier) {
        this.page = page;
        this.size = size;
        this.multiplier = multiplier;
        this.merchantTier = merchantTier;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public String getMerchantTier() {
        return merchantTier;
    }

    public void setMerchantTier(String merchantTier) {
        this.merchantTier = merchantTier;
    }
}
