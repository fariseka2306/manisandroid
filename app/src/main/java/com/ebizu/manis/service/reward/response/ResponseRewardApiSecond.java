package com.ebizu.manis.service.reward.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/26/17.
 */

public class ResponseRewardApiSecond<Response> {

    @SerializedName("errorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("time")
    @Expose
    private Double time;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "ResponseRewardApi{" +
                "errorCode=" + errorCode +
                ", time=" + time +
                '}';
    }
}
