package com.ebizu.manis.service.reward.response;

import com.ebizu.manis.model.RewardCategoryList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class WrapperRewardCategoryList extends ResponseRewardApi {

    @SerializedName("data")
    @Expose
    private List<RewardCategoryList> getRewardCategoryList = null;

    public List<RewardCategoryList> getGetRewardCategoryList() {
        return getRewardCategoryList;
    }
}
