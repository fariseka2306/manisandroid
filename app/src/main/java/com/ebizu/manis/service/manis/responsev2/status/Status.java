package com.ebizu.manis.service.manis.responsev2.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class Status {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("messageServer")
    @Expose
    private String messageServer;
    @SerializedName("messageClient")
    @Expose
    private String messageClient;

    public Integer getCode() {
        return code;
    }

    public String getMessageServer() {
        return messageServer;
    }

    public String getMessageClient() {
        return messageClient;
    }
}
