package com.ebizu.manis.service.manis.requestbody;

import android.content.Context;

import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public class PurchaseHistoryBody extends RewardSessionApiBody {

    public static final int STATUS_PENDING = 3;
    public static final int STATUS_UNPAID = 2;
    public static final int STATUS_PAID = 1;
    public static final int STATUS_ALL = 0;

    public static final int ORDER_DATE = 1;
    public static final int ORDER_AMOUNT = 2;


    @SerializedName("data")
    @Expose
    private PurchaseHistoryBody.Data data;

    public PurchaseHistoryBody(Context context, PurchaseHistoryBody.Data data) {
        super(context);
        this.data = data;
        this.module = RewardApiConfig.Module.VOUCHER;
        this.action = RewardApiConfig.Action.PURCHASE_HISTORY;
    }

    public static class Data {


        private int size;
        private int page;
        private int order;
        private int condition;

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public int getCondition() {
            return condition;
        }

        public void setCondition(int condition) {
            this.condition = condition;
        }
    }
}
