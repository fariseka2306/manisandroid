package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by halim_ebizu on 9/8/17.
 */

public class RewardCategoryBody {

    public int page;
    public int size;

    public RewardCategoryBody(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
