package com.ebizu.manis.service.manis.requestbodyv2.bodysuper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class ManisRequestBody {

    @SerializedName("module")
    @Expose
    protected String module;
    @SerializedName("action")
    @Expose
    protected String action;

}
