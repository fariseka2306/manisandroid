package com.ebizu.manis.service.reward.response;

import com.ebizu.manis.model.rewardbulk.Data;
import com.ebizu.manis.model.rewardbulk.RewardBulk;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by FARIS_mac on 11/14/17.
 */

public class WrapperRewardBulk extends ResponseRewardApi {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }
}
