package com.ebizu.manis.service.manis.requestbodyv2.data;

import android.content.Context;

import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.DataRequestBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/24/17.
 */

public class ThematicPostData extends DataRequestBody {

    @SerializedName("missionId")
    @Expose
    private Integer missionId;
    @SerializedName("pin")
    @Expose
    private String pin;
    @SerializedName("voucherId")
    @Expose
    private String voucherId;

    public ThematicPostData(Context context) {
        super(context);
    }

    public void setMissionId(Integer missionId) {
        this.missionId = missionId;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }
}
