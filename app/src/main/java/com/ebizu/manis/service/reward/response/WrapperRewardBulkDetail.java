package com.ebizu.manis.service.reward.response;

import com.ebizu.manis.model.rewardbulk.Data;
import com.ebizu.manis.model.rewardbulkdetail.RewardBulkDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 11/14/17.
 */

public class WrapperRewardBulkDetail extends ResponseRewardApi {

    @SerializedName("data")
    @Expose
    private RewardBulkDetail rewardBulkDetail;

    public RewardBulkDetail getRewardBulkDetail() {
        return rewardBulkDetail;
    }
}
