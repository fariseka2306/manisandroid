package com.ebizu.manis.service.reward.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.manis.model.RewardPaging;
import com.ebizu.manis.sdk.entities.MyVoucher;
import com.ebizu.sdk.reward.models.Reward;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ebizu on 9/27/17.
 */

public class WrapperMyVoucher extends ResponseRewardApi implements Parcelable {

    @SerializedName("data")
    @Expose
    private RewardPaging rewardPaging;

    public RewardPaging getRewardPaging() {
        return rewardPaging;
    }

    public void setRewardPaging(RewardPaging rewardPaging) {
        this.rewardPaging = rewardPaging;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.rewardPaging, flags);
    }

    public WrapperMyVoucher() {
    }

    protected WrapperMyVoucher(Parcel in) {
        this.rewardPaging = in.readParcelable(RewardPaging.class.getClassLoader());
    }

    public static final Creator<WrapperMyVoucher> CREATOR = new Creator<WrapperMyVoucher>() {
        @Override
        public WrapperMyVoucher createFromParcel(Parcel source) {
            return new WrapperMyVoucher(source);
        }

        @Override
        public WrapperMyVoucher[] newArray(int size) {
            return new WrapperMyVoucher[size];
        }
    };
}
