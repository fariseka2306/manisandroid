package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class StoreUploadBody {

    private String name;
    private String id;
    private String category;
    private String location;

    public StoreUploadBody(String name, String id, String category, String location) {
        this.name = name;
        this.id = id;
        this.category = category;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
