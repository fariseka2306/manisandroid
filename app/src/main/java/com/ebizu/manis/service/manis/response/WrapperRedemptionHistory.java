package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.RedemptionHistoryData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 8/7/2017.
 */

public class WrapperRedemptionHistory extends ResponseData {

    @SerializedName("data")
    @Expose
    private RedemptionHistoryData data;

    public RedemptionHistoryData getClaimedData() {
        return data;
    }
}
