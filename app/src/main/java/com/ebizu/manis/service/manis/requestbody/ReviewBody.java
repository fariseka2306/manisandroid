package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by mac on 8/23/17.
 */

public class ReviewBody {

    private String happy;

    private String review;

    private String comment;

    public String getHappy() {
        return happy;
    }

    public String getReview() {
        return review;
    }

    public String getComment() {
        return comment;
    }

    public void setHappy(String happy) {
        this.happy = happy;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "ReviewBody{" +
                "happy='" + happy + '\'' +
                ", review='" + review + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
