package com.ebizu.manis.service.reward;

/**
 * Created by ebizu on 9/26/17.
 */

public class RewardApiConfig {

    public static class Preference {
        public static String SESSION = "reward.session-session";
    }

    public static class Module {
        public static String VOUCHER = "Voucher";
        public static String SESSION = "Session";
        public static String CATEGORY = "Category";
    }

    public static class Action {
        public static String LIST = "list";
        public static String ORDER_DETAIL = "orderDetail";
        public static String BRAND_TERMS = "brandTerms";
        public static String PURCHASE_HISTORY = "purchaseHistory";
        public static String CANCEL_PAYMENT = "cancelPayment";
        public static String PURCHASE_STATUS = "purchaseStatus";
        public static String MY_VOUCHER = "myVoucher";
        public static String PIN_VALIDATION = "pinValidation";
        public static String PAYMENT_TERMS = "paymentTerms";
        public static String GET_SESSION = "getSession";
        public static String REGISTER = "register";
        public static String COMBO_DEALS = "comboDeals";
    }

    public static class ResponseCode {
        public static int SUCCESS = -1;
        public static int SESSION_EMPTY = 258;
        public static int SESSION_INVALID = 513;
    }

    public static String ERROR_EMPTY_TOKEN = "Field 'session' is required, can't be empty";
    public static String ERROR_SESSION_IS_REQUIRED = "Field 'session' is required";
    public static String ERROR_USER_IS_LOGGED_OUT = "hmmm...that's weird. you seem to have been logged out. please login again.";
}
