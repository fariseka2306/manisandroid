package com.ebizu.manis.service.manis;

import android.content.Context;

import com.ebizu.manis.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ebizu-User on 05/07/2017.
 */

public class ManisApiGenerator {

    public ManisApi provideManisApi(Context context) {
        return createService(context);
    }

    public static <S> ManisApi createServiceWithToken(Context context) {
        return retroBuilder(okBuilderToken(context).build()).create(ManisApi.class);
    }

    public static <S> ManisApi createService(Context context) {
        return retroBuilder(okBuilder(context).build()).create(ManisApi.class);
    }

    public static <S> ManisApi createServiceApiAry(Context context) {
        return retroBuilderApiAry(okBuilderAPiAry(context).build()).create(ManisApi.class);
    }

    public static <S> ManisApi createServiceMission(Context context) {
        return retroBuilderMission(okBuilderToken(context).build()).create(ManisApi.class);
    }

    public static <S> ManisApi createServiceVersioning(Context context) {
        return retroBuilderVersioning(okBuilder(context).build()).create(ManisApi.class);
    }

    public static <S> ManisApiV2 createServiceV2(Context context) {
        return retroBuilderV2(okBuilder(context).build()).create(ManisApiV2.class);
    }

    public static <S> ManisApiV2 createServiceTracker(Context context) {
        return retroBuilderTracker(okBuilder(context).build()).create(ManisApiV2.class);
    }

    public static <S> ManisApi createServiceUpdateAccount(Context context) {
        return retroBuilder(okBuilder(context).build()).create(ManisApi.class);
    }

    private static OkHttpClient.Builder okBuilder(Context context) {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(chain -> chain.proceed(requestBuilder(chain, context)));
    }

    private static OkHttpClient.Builder okBuilderToken(Context context) {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .addInterceptor(chain -> chain.proceed(requestBuilderToken(chain, context)));
    }

    private static OkHttpClient.Builder okBuilderAPiAry(Context context) {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS);
    }

    private static Request requestBuilder(Interceptor.Chain chain, Context context) {
        Request.Builder requestBuilder = chain.request().newBuilder();
        RequestBuilderHeader requestBuilderHeader = new RequestBuilderHeader(requestBuilder, context);
        requestBuilderHeader.addCoordinate();
        requestBuilderHeader.addPubKey();
        requestBuilderHeader.addDevice();
        requestBuilderHeader.addManisVer();
        requestBuilderHeader.addManisBuild();
        return requestBuilder.build();
    }

    private static Request requestBuilderToken(Interceptor.Chain chain, Context context) {
        Request.Builder requestBuilder = chain.request().newBuilder();
        RequestBuilderHeader requestBuilderHeader = new RequestBuilderHeader(requestBuilder, context);
        requestBuilderHeader.addToken();
        requestBuilderHeader.addCoordinate();
        requestBuilderHeader.addLang();
        requestBuilderHeader.addManisVer();
        requestBuilderHeader.addManisBuild();
        return requestBuilder.build();
    }

    private static Retrofit retroBuilder(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(createGsonConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private static Retrofit retroBuilderV2(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL_LAMBDA)
                .client(okHttpClient)
                .addConverterFactory(createGsonConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private static Retrofit retroBuilderTracker(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL_TRACKER_API)
                .client(okHttpClient)
                .addConverterFactory(createGsonConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private static Retrofit retroBuilderApiAry(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl("http://private-ba5e76-bulkpurchase1.apiary-mock.com")
                .client(okHttpClient)
                .addConverterFactory(createGsonConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private static Retrofit retroBuilderMission(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL_MISSION_API)
                .client(okHttpClient)
                .addConverterFactory(createGsonConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private static Retrofit retroBuilderVersioning(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl("https://ut70c9j83e.execute-api.ap-southeast-1.amazonaws.com/")
                .client(okHttpClient)
                .addConverterFactory(createGsonConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private static GsonConverterFactory createGsonConverterFactory(){
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return GsonConverterFactory.create(gson);
    }
}