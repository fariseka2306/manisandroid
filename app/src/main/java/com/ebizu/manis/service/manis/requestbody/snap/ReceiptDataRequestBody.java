package com.ebizu.manis.service.manis.requestbody.snap;

import com.google.gson.GsonBuilder;

import okhttp3.RequestBody;

/**
 * Created by ebizu on 8/22/17.
 */

public class ReceiptDataRequestBody {

    private ReceiptDataBody receiptDataBody;

    public ReceiptDataRequestBody(ReceiptDataBody receiptDataBody) {
        this.receiptDataBody = receiptDataBody;
    }

    public RequestBody create() {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(receiptDataBody));
    }
}
