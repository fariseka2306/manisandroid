package com.ebizu.manis.service.manis.requestbody.snap;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/10/17.
 */

public class SnapableRequestBody {

    @SerializedName("com_id")
    int comId;
    @SerializedName("merchant_tier")
    String merchantTier;

    public void setComId(int comId) {
        this.comId = comId;
    }

    public void setMerchantTier(String merchantTier) {
        this.merchantTier = merchantTier;
    }
}
