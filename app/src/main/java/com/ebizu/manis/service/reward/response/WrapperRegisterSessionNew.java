package com.ebizu.manis.service.reward.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class WrapperRegisterSessionNew extends ResponseRewardApi {

    @SerializedName("data")
    @Expose
    private RegisterRewardSessionNew registerRewardSessionNew;

    public RegisterRewardSessionNew getRegisterRewardSessionNew() {
        return registerRewardSessionNew;
    }

    public void setRegisterRewardSessionNew(RegisterRewardSessionNew registerRewardSessionNew) {
        this.registerRewardSessionNew = registerRewardSessionNew;
    }
}
