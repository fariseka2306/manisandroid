package com.ebizu.manis.service.manis.requestbodyv2.body;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.VoucherAvailableBody;
import com.ebizu.manis.service.manis.requestbodyv2.data.ShareExperiencePostData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 11/16/17.
 */

public class ShareExperienceMyVoucherAvailable extends VoucherAvailableBody {

    @SerializedName("data")
    private VoucherAvailableBody data;

    public void setData(VoucherAvailableBody data) {
        this.data = data;
    }

    public ShareExperienceMyVoucherAvailable() {
        session = ConfigManager.MyVoucherAvailable.SHARE_EXPERIENCE_SESSION;
        module  = ConfigManager.MyVoucherAvailable.SHARE_EXPERIENCE_MODULE;
        action  = ConfigManager.MyVoucherAvailable.SHARE_EXPERIENCE_ACTION;
        token   = ConfigManager.MyVoucherAvailable.SHARE_EXPERIENCE_TOKEN;
    }
}
