package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Point;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/4/17.
 */

public class WrapperRedeemCloseFailed extends ResponseData {

    @SerializedName("data")
    @Expose
    Point point;

    public Point getPoint() {
        return point;
    }

}
