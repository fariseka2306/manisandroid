package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.LuckyDrawScreen;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ebizu on 04/08/17.
 */

public class WrapperLuckyDrawScreen extends ResponseData {

    @SerializedName("data")
    @Expose
    LuckyDrawScreen luckyDrawScreen;

    public LuckyDrawScreen getLuckyDrawScreen() {
        return luckyDrawScreen;
    }
}
