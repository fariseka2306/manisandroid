package com.ebizu.manis.service.manis.requestbodyv2.data;

import android.content.Context;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.DataRequestBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/12/17.
 */

public class ShareExperiencePostData extends DataRequestBody {

    @SerializedName("missionId")
    @Expose
    private Integer missionId;

    @SerializedName("manis-version")
    @Expose
    private String manisVersion;

    public ShareExperiencePostData(Context context, Integer missionId) {
        super(context);
        this.missionId = missionId;
        this.manisVersion = BuildConfig.VERSION_NAME;
    }
}
