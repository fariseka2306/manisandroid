package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.notification.beacon.BeaconPromo;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 8/23/17.
 */

public class WrapperBeaconPromo extends ResponseData {

    @SerializedName("data")
    private BeaconPromo beaconPromo;

    public BeaconPromo getBeaconPromo() {
        return beaconPromo;
    }
}
