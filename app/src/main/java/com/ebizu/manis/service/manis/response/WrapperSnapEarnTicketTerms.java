package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.snap.SnapTermLuckyDraw;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/12/17.
 */

public class WrapperSnapEarnTicketTerms {
    @SerializedName("data")
    @Expose
    private SnapTermLuckyDraw snapTermLuckyDraw;

    public SnapTermLuckyDraw getSnapTermLuckyDraw() {
        return snapTermLuckyDraw;
    }
}
