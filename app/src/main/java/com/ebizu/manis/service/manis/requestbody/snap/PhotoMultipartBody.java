package com.ebizu.manis.service.manis.requestbody.snap;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by ebizu on 8/22/17.
 */

public class PhotoMultipartBody {

    private File photoFile;

    public PhotoMultipartBody(File photoFile) {
        this.photoFile = photoFile;
    }

    public MultipartBody.Part createFormData() {
        return MultipartBody.Part.createFormData("photo", photoFile.getName(), requestPhotoFile());
    }

    private RequestBody requestPhotoFile() {
        return RequestBody.create(MediaType.parse("image/jpeg"), photoFile);
    }

}
