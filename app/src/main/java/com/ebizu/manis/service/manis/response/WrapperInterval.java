package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.notification.IntervalBeaconPromo;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/6/17.
 */

public class WrapperInterval extends ResponseData {

    @SerializedName("data")
    private IntervalBeaconPromo intervalBeaconPromo;

    public IntervalBeaconPromo getIntervalBeaconPromo() {
        return intervalBeaconPromo;
    }
}
