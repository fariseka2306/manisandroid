package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Term;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class WrapperTerm extends ResponseData {

    @SerializedName("data")
    @Expose
    private Term[] term;

    public Term[] getTerm() {
        return term;
    }
}
