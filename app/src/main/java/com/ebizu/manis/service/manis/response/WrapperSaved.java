package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.saved.Saved;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 8/21/17.
 */

public class WrapperSaved extends ResponseData {

    @SerializedName("data")
    @Expose
    private Saved getSaved;

    public Saved getGetSaved() {
        return getSaved;
    }
}
