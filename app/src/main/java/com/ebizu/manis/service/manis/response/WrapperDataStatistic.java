package com.ebizu.manis.service.manis.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class WrapperDataStatistic extends ResponseData {
    @SerializedName("data")
    @Expose
    private WrapperStatistic wrapperStatistic;

    public WrapperStatistic getWrapperStatistic() {
        return wrapperStatistic;
    }
}
