package com.ebizu.manis.service.manis.requestbodyv2.bodysuper;

import android.content.Context;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.GPSTracker;
import com.ebizu.manis.preference.LocationSession;
import com.ebizu.manis.preference.ManisSession;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Locale;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class DataRequestBody {

    @SerializedName("applicationId")
    @Expose
    private String applicationId;
    @SerializedName("coordinate")
    @Expose
    private String coordinate;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("lang")
    @Expose
    private String lang;

    public DataRequestBody(Context context) {
        applicationId = ConfigManager.Missions.applicationId;
        ManisSession manisSession = new ManisSession(context);
        GPSTracker gpsTracker = new GPSTracker(context);
        LocationSession locationSession = new LocationSession(context);
        gpsTracker.updateLocation();
        token = manisSession.getTokenSession();
        coordinate = locationSession.getLatLong();
        lang = Locale.getDefault().toString();
    }

}
