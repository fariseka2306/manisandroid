package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Privacy;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class WrapperPrivacy extends ResponseData {

    @SerializedName("data")
    private Privacy[] privacies;

    public Privacy[] getPrivacies() {
        return privacies;
    }
}
