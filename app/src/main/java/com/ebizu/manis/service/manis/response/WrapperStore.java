package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.StoreResults;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raden on 7/6/17.
 */

public class WrapperStore extends ResponseData {

    @SerializedName("data")
    @Expose
    StoreResults storeResults;

    public StoreResults getStoreResults() {
        return storeResults;
    }

}
