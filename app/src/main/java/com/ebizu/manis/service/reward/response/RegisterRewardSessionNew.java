package com.ebizu.manis.service.reward.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class RegisterRewardSessionNew {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("userEmail")
    @Expose
    private String userEmail;
    @SerializedName("userCountry")
    @Expose
    private String userCountry;
    @SerializedName("userTimezone")
    @Expose
    private String userTimezone;
    @SerializedName("userJoin")
    @Expose
    private String userJoin;
    @SerializedName("userAgent")
    @Expose
    private String userAgent;
    @SerializedName("ipAddress")
    @Expose
    private String ipAddress;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("osName")
    @Expose
    private String osName;
    @SerializedName("osVersion")
    @Expose
    private String osVersion;
    @SerializedName("applicationBuild")
    @Expose
    private String applicationBuild;
    @SerializedName("applicationVersion")
    @Expose
    private String applicationVersion;
    @SerializedName("manufacture")
    @Expose
    private String manufacture;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("provider")
    @Expose
    private String provider;
    @SerializedName("msisdn")
    @Expose
    private String msisdn;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("imsi")
    @Expose
    private String imsi;
    @SerializedName("session")
    @Expose
    private String session;

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getUserCountry() {
        return userCountry;
    }

    public String getUserTimezone() {
        return userTimezone;
    }

    public String getUserJoin() {
        return userJoin;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getOsName() {
        return osName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getApplicationBuild() {
        return applicationBuild;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public String getManufacture() {
        return manufacture;
    }

    public String getModel() {
        return model;
    }

    public String getProvider() {
        return provider;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getImei() {
        return imei;
    }

    public String getImsi() {
        return imsi;
    }

    public String getSession() {
        return session;
    }
}
