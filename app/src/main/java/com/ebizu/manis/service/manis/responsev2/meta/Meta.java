package com.ebizu.manis.service.manis.responsev2.meta;

import com.ebizu.manis.service.manis.responsev2.meta.Pagination;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class Meta {

    @SerializedName("pagination")
    @Expose
    private Pagination pagination;

    public Pagination getPagination() {
        return pagination;
    }
}
