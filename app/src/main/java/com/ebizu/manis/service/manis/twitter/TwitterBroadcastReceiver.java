package com.ebizu.manis.service.manis.twitter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.mvp.mission.shareexperience.ShareExperienceDetailActivity;
import com.ebizu.manis.mvp.mission.shareexperience.TwitterResultPresenter;
import com.twitter.sdk.android.tweetcomposer.TweetUploadService;

public class TwitterBroadcastReceiver extends BroadcastReceiver {

    private TwitterResultPresenter twitterResultPresenter;
    private ShareExperienceDetailActivity shareExperienceDetailActivity;

    public void setActivityHandler(ShareExperienceDetailActivity shareExperienceDetailActivity) {
        this.shareExperienceDetailActivity = shareExperienceDetailActivity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (TweetUploadService.UPLOAD_SUCCESS.equals(intent.getAction())) {
            initializeTwitterPresenter(shareExperienceDetailActivity);
            if (twitterResultPresenter != null)
                twitterResultPresenter.onSuccessTweet();
        } else if (TweetUploadService.UPLOAD_FAILURE.equals(intent.getAction())) {
            if (shareExperienceDetailActivity != null) {
                shareExperienceDetailActivity.dismissProgressBarDialog();
                shareExperienceDetailActivity.showFailureDialog();
            }
        } else if (TweetUploadService.TWEET_COMPOSE_CANCEL.equals(intent.getAction())) {
            if (shareExperienceDetailActivity != null)
                shareExperienceDetailActivity.dismissProgressBarDialog();
        }
    }

    private void initializeTwitterPresenter(Context context) {
        if (twitterResultPresenter == null)
            twitterResultPresenter = new TwitterResultPresenter(context);
    }
}