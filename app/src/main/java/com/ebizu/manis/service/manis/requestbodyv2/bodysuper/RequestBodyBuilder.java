package com.ebizu.manis.service.manis.requestbodyv2.bodysuper;

import android.content.Context;

import com.ebizu.manis.model.snap.Photo;

/**
 * Created by ebizu on 12/18/17.
 */

public class RequestBodyBuilder {

    private RequestBody requestBody;
    private RequestBodyBuilder builder;

    /**
     * This construct instance requestBody,
     * Default: requestBody.command = "Utilities"
     * header
     *
     * @param context
     */
    public RequestBodyBuilder(Context context) {
        requestBody = new RequestBody(context);
        this.builder = this;
    }

    /**
     * Default: requestBody.command = "Utilities"
     *
     * @param command
     * @return
     */
    public RequestBodyBuilder setCommand(String command) {
        this.requestBody.command = command;
        return builder;
    }

    /**
     * Every end point function has different value
     *
     * @param function
     * @return
     */
    public RequestBodyBuilder setFunction(String function) {
        this.requestBody.function = function;
        return builder;
    }

    public RequestBodyBuilder setData(Object data) {
        this.requestBody.addData(data);
        return builder;
    }

    public RequestBodyBuilder setPhoto(Photo photo) {
        this.requestBody.setPhoto(photo);
        return builder;
    }

    public RequestBodyBuilder discardHeaderToken() {
        this.requestBody.discardHeaderToken();
        return builder;
    }

    public RequestBody create() {
        return requestBody;
    }
}
