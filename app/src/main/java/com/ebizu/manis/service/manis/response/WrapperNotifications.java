package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.NotificationsSetting;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by abizu-alvio on 7/24/2017.
 */

public class WrapperNotifications extends ResponseData {
    @SerializedName("data")
    @Expose
    private List<NotificationsSetting> notificationsSettings = null;

    public List<NotificationsSetting> getNotificationsSettings() {
        return notificationsSettings;
    }

    public void setNotificationsSettings(List<NotificationsSetting> notificationsSettings) {
        this.notificationsSettings = notificationsSettings;
    }
}
