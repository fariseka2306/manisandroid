package com.ebizu.manis.service.manis.requestbodyv2.body;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.ManisRequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.data.ShareExperienceMissionData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class ShareExperienceMissionsRB extends ManisRequestBody {

    @SerializedName("data")
    private ShareExperienceMissionData data;

    public void setData(ShareExperienceMissionData data) {
        this.data = data;
    }

    public ShareExperienceMissionsRB() {
        module = ConfigManager.Missions.SHARE_EXPERIENCE_MODUL;
        action = ConfigManager.Missions.SHARE_EXPERIENCE_LIST_ACTION;
    }
}
