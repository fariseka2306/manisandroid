package com.ebizu.manis.service.manis.requestbodyv2.data;

import android.content.Context;

import com.ebizu.manis.helper.BuildConfig;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.DataRequestBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/12/17.
 */

public class ShareExperienceCheckAvalaibilityData extends DataRequestBody{

    @SerializedName("missionId")
    @Expose
    private Integer missionId;

    @SerializedName("manis-version")
    @Expose
    private String manisVersion;

    @SerializedName("voucherId")
    @Expose
    private String voucherId;

    public ShareExperienceCheckAvalaibilityData(Context context, Integer missionId) {
        super(context);
        manisVersion = BuildConfig.VERSION_NAME;
        this.missionId = missionId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }
}
