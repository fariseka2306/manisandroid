package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.LuckyDrawTerm;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class WrapperLuckyDrawTerm extends ResponseData {

    @SerializedName("data")
    @Expose
    public LuckyDrawTerm luckyDrawTerm;

    public LuckyDrawTerm getLuckyDrawTerm() {
        return luckyDrawTerm;
    }

    public void setLuckyDrawTerm(LuckyDrawTerm luckyDrawTerm) {
        this.luckyDrawTerm = luckyDrawTerm;
    }
}
