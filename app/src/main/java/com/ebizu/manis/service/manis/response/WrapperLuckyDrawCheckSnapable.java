package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.LuckyDrawCheckSnapable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class WrapperLuckyDrawCheckSnapable extends ResponseData {

    @SerializedName("data")
    @Expose
    public LuckyDrawCheckSnapable luckyDrawCheckSnapable;

    public LuckyDrawCheckSnapable getLuckyDrawCheckSnapable() {
        return luckyDrawCheckSnapable;
    }

    public void setLuckyDrawCheckSnapable(LuckyDrawCheckSnapable luckyDrawCheckSnapable) {
        this.luckyDrawCheckSnapable = luckyDrawCheckSnapable;
    }
}
