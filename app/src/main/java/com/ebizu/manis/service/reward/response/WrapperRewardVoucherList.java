package com.ebizu.manis.service.reward.response;

import com.ebizu.manis.model.rewardvoucher.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class WrapperRewardVoucherList extends ResponseRewardApi {
    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }
}
