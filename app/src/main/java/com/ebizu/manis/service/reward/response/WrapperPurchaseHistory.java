package com.ebizu.manis.service.reward.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.ebizu.manis.model.PurchaseHistoryPaging;
import com.ebizu.manis.model.RewardPaging;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public class WrapperPurchaseHistory extends ResponseRewardApi implements Parcelable {

    @SerializedName("data")
    @Expose
    private PurchaseHistoryPaging purchaseHistoryPaging;

    public PurchaseHistoryPaging getPurchaseHistoryPaging() {
        return purchaseHistoryPaging;
    }

    public void setPurchaseHistoryPaging(PurchaseHistoryPaging purchaseHistoryPaging) {
        this.purchaseHistoryPaging = purchaseHistoryPaging;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.purchaseHistoryPaging, flags);
    }

    public WrapperPurchaseHistory() {
    }

    protected WrapperPurchaseHistory(Parcel in) {
        this.purchaseHistoryPaging = in.readParcelable(PurchaseHistoryPaging.class.getClassLoader());
    }

    public static final Creator<WrapperPurchaseHistory> CREATOR = new Creator<WrapperPurchaseHistory>() {
        @Override
        public WrapperPurchaseHistory createFromParcel(Parcel source) {
            return new WrapperPurchaseHistory(source);
        }

        @Override
        public WrapperPurchaseHistory[] newArray(int size) {
            return new WrapperPurchaseHistory[size];
        }
    };
}
