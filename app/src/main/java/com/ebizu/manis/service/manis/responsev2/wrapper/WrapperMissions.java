package com.ebizu.manis.service.manis.responsev2.wrapper;

import com.ebizu.manis.model.missions.Missions;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class WrapperMissions extends ResponseWrapper {

    @SerializedName("data")
    @Expose
    private List<Missions> experienceList = null;

    public List<Missions> getExperienceList() {
        return experienceList;
    }

    @Override
    public String toString() {
        return "WrapperMissions{" +
                "experienceList=" + experienceList +
                '}';
    }
}
