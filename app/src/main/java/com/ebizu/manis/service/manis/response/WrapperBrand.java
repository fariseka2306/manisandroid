package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Brand;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Raden on 7/6/17.
 */

public class WrapperBrand extends ResponseData {

    @SerializedName("data")
    @Expose
    Data getData;

    public class Data{
        @SerializedName("brands")
        @Expose
        ArrayList<Brand> brands;

        @SerializedName("title")
        @Expose
        private String title;

        public ArrayList<Brand> getBrands(){
            return brands;
        }

        public String getTitle() {
            return title;
        }
    }

    public Data getGetData() {
        return getData;
    }
}
