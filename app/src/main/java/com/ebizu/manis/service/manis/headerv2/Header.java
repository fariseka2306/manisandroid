package com.ebizu.manis.service.manis.headerv2;

import android.content.Context;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.helper.DeviceUtils;
import com.ebizu.manis.preference.LocationSession;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.service.manis.key.ManisKeyGenerator;
import com.google.gson.annotations.SerializedName;

import java.util.Locale;

/**
 * Created by ebizu on 10/31/17.
 */

public class Header {

    @SerializedName("coordinate")
    private String coordinate;
    @SerializedName("device")
    private String device;
    @SerializedName("manis-version")
    private String manisVersion;
    @SerializedName("manis-build")
    private String manisBuild;
    @SerializedName("lang")
    private String language;
    @SerializedName("token")
    private String token;
    @SerializedName("pubkey")
    private String pubkey;

    public Header(Context context) {
        this.coordinate = LocationSession.getInstance(context).getLatLong();
        if (ManisSession.getInstance(context).isLoggedIn())
            this.token = ManisSession.getInstance(context).getTokenSession();
        this.pubkey = ManisKeyGenerator.getInstance(context).getPublicKeyApi();
        this.device = DeviceUtils.getDeviceID(context);
        this.manisVersion = BuildConfig.VERSION_NAME;
        this.manisBuild = String.valueOf(BuildConfig.VERSION_CODE);
        this.language = Locale.getDefault().toString();
    }

    public void discardHeaderToken() {
        this.token = null;
    }

}
