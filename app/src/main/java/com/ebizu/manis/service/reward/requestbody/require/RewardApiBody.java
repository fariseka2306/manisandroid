package com.ebizu.manis.service.reward.requestbody.require;

import com.ebizu.manis.BuildConfig;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/26/17.
 */

public class RewardApiBody {

    @SerializedName("module")
    @Expose
    protected String module;
    @SerializedName("action")
    @Expose
    protected String action;
    @SerializedName("token")
    @Expose
    protected String token;

    public RewardApiBody() {
        this.token = BuildConfig.REWARD_KEY;
    }
}
