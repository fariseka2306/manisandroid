package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.rewardredeem.RewardRedeem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/3/17.
 */

public class WrapperRewardRedeem extends ResponseData {

    @SerializedName("data")
    @Expose
    RewardRedeem rewardRedeem;

    public RewardRedeem getRewardRedeem() {
        return rewardRedeem;
    }
}
