package com.ebizu.manis.service.manis.requestbody;

import com.ebizu.manis.model.DataAuth;
import com.ebizu.manis.model.Google;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 16/07/2017.
 */

public class DataGmailAuth extends DataAuth {

    @SerializedName("google")
    Google google;

    public void setGoogle(Google google) {
        this.google = google;
    }
}
