package com.ebizu.manis.service.manis.responsev2.meta;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/10/17.
 */

public class Pagination {

    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("perPage")
    @Expose
    private Integer perPage;
    @SerializedName("currentPage")
    @Expose
    private Integer currentPage;
    @SerializedName("totalPage")
    @Expose
    private Integer totalPage;


    public Integer getTotal() {
        return total;
    }

    public Integer getCount() {
        return count;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public Integer getTotalPage() {
        return totalPage;
    }
}
