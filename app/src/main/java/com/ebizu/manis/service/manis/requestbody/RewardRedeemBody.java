package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by FARIS_mac on 10/3/17.
 */

public class RewardRedeemBody {

    private String id;
    private int point;
    private String voucher_type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getVoucher_type() {
        return voucher_type;
    }

    public void setVoucher_type(String voucher_type) {
        this.voucher_type = voucher_type;
    }

}
