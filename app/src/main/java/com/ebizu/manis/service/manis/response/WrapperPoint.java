package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Point;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 7/10/2017.
 */

public class WrapperPoint extends ResponseData {
    @SerializedName("data")
    @Expose
    Point point;

    public Point getPoint() {
        return point;
    }
}
