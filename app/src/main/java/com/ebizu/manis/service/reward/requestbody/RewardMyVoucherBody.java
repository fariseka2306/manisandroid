package com.ebizu.manis.service.reward.requestbody;

import android.content.Context;

import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/27/17.
 */

public class RewardMyVoucherBody extends RewardSessionApiBody {

    @SerializedName("data")
    @Expose
    private RewardMyVoucherBody.Data data;

    public RewardMyVoucherBody(Context context, RewardMyVoucherBody.Data data) {
        super(context);
        this.data = data;
        this.module = RewardApiConfig.Module.VOUCHER;
        this.action = RewardApiConfig.Action.MY_VOUCHER;
    }

    public Data getData() {
        return data;
    }

    public static class Data {

        @SerializedName("size")
        @Expose
        private Integer size;
        @SerializedName("page")
        @Expose
        private Integer page;
        @SerializedName("order")
        @Expose
        private Integer order;
        @SerializedName("condition")
        @Expose
        private String condtion;

        public Integer getSize() {
            return size;
        }

        public void setSize(Integer size) {
            this.size = size;
        }

        public Integer getPage() {
            return page;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public Integer getOrder() {
            return order;
        }

        public void setOrder(Integer order) {
            this.order = order;
        }

        public String getCondtion() {
            return condtion;
        }

        public void setCondtion(String condtion) {
            this.condtion = condtion;
        }
    }
}
