package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by mac on 8/22/17.
 */

public class VersioningBody {

    private int code;
    private String platform;
    private String lang;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
}
