package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.snap.SnapableLuckyDraw;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 10/10/17.
 */

public class WrapperSnapable extends ResponseData {

    @SerializedName("data")
    @Expose
    private SnapableLuckyDraw snapableLuckyDraw;

    public SnapableLuckyDraw getSnapableLuckyDraw() {
        return snapableLuckyDraw;
    }
}
