package com.ebizu.manis.service.manis;

import android.content.Context;
import android.location.LocationManager;
import android.util.Log;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.DeviceUtils;
import com.ebizu.manis.helper.GPSTracker;
import com.ebizu.manis.manager.location.ManisLocationManager;
import com.ebizu.manis.model.Location;
import com.ebizu.manis.preference.LocationSession;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.service.manis.key.ManisKeyGenerator;

import java.util.Locale;

import okhttp3.Request;

/**
 * Created by Ebizu-User on 05/07/2017.
 */

public class RequestBuilderHeader {

    private Request.Builder requestBuilder;
    private ManisKeyGenerator manisKeyGenerator;
    private Context context;
    private ManisLocationManager manisLocationManager;

    public RequestBuilderHeader(Request.Builder requestBuilder, Context context) {
        this.requestBuilder = requestBuilder;
        this.context = context;
        manisKeyGenerator = new ManisKeyGenerator(context);
    }

    public void addCoordinate() {
        if (manisLocationManager == null)
            manisLocationManager = new ManisLocationManager(context);
        manisLocationManager.updateLocation();
        LocationSession locationSession = new LocationSession(context);
        requestBuilder.addHeader(ConfigManager.Header.COORDINATE, locationSession.getLatLong());
    }

    public void addToken() {
        String token = new ManisSession(context).getTokenSession();
        requestBuilder.addHeader(ConfigManager.Header.TOKEN, token);
        Log.d(getClass().getSimpleName(), "addToken: " + token);
    }

    public void addPubKey() {
        requestBuilder.addHeader(ConfigManager.Header.PUBKEY, manisKeyGenerator.getPublicKeyApi());
    }

    public void addDevice() {
        requestBuilder.addHeader(ConfigManager.Header.DEVICE, DeviceUtils.getDeviceID(context));
    }

    public void addManisVer() {
        requestBuilder.addHeader(ConfigManager.Header.MANIS_VERSION, BuildConfig.VERSION_NAME);
    }

    public void addManisBuild() {
        requestBuilder.addHeader(ConfigManager.Header.MANIS_BUILD, String.valueOf(BuildConfig.VERSION_CODE));
    }

    public void addLang() {
        requestBuilder.addHeader(ConfigManager.Header.LANG, Locale.getDefault().toString());
    }

    public void addContentType() {
        requestBuilder.addHeader(ConfigManager.Header.CONTENT_TYPE, "application/x-www-form-urlencoded");
    }

}