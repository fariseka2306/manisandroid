package com.ebizu.manis.service.manis.response;

import android.os.Parcelable;

import com.ebizu.manis.model.StoreDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firef on 7/21/2017.
 */

public class WrapperStoreDetail extends ResponseData {

    @SerializedName("data")
    @Expose
    StoreDetail storeDetail;

    public StoreDetail getStoreDetail() {
        return storeDetail;
    }

    public void setStoreDetail(StoreDetail storeDetail) {
        this.storeDetail = storeDetail;
    }
}
