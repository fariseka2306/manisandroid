package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.RewardPaging;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Halim on 9/23/17.
 */

public class WrapperReward extends RewardResponse{

    @SerializedName("data")
    @Expose
    public RewardPaging rewardPaging;

    public RewardPaging getRewardPaging() {
        return rewardPaging;
    }

    public void setRewardPaging(RewardPaging rewardPaging) {
        this.rewardPaging = rewardPaging;
    }
}
