package com.ebizu.manis.service.manis.key;

import android.util.Base64;
import android.util.Log;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;

import javax.crypto.Cipher;

/**
 * Created by rezkya on 17/05/16.
 */
public class RSACrypto {

    public static final String TAG = "AsymmetricAlgorithmRSA";
    private static com.ebizu.manis.service.manis.key.RSACrypto instance;
    private Key publicKey = null;
    private Key privateKey = null;

    public static RSACrypto getInstance() {
        if (instance == null) {
            instance = new RSACrypto();
        }
        KeyPairGenerator kpg;
        try {
            kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(1024);
            KeyPair kp = kpg.genKeyPair();
            instance.privateKey = kp.getPrivate();
            instance.publicKey = kp.getPublic();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return instance;
    }

    public static RSACrypto getInstanceNoKey() {
        if (instance == null) {
            instance = new RSACrypto();
        }
        return instance;
    }

    public String decrypt(String encryptedText, Key privateKey) {
        String[] encryptedTextArray = encryptedText.split("\\|manis\\|");
        String result = "";
        for (int i = 0; i < encryptedTextArray.length; i++) {
            encryptedText = encryptedTextArray[i];
            byte[] encryptedByte = Base64.decode(encryptedText.getBytes(), Base64.NO_WRAP);
            byte[] decodedBytes;
            try {
                Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                c.init(Cipher.DECRYPT_MODE, privateKey);
                decodedBytes = c.doFinal(encryptedByte);
            } catch (Exception e) {
                Log.e(TAG, "RSA decryption error " + e.getMessage());
                break;
            }
            if (decodedBytes != null) {
                result += new String(decodedBytes);
            } else {
                Log.d(TAG, "[DECODED]: failed ");
                break;
            }
        }
        return result;
    }

    public Key getPrivateKey() {
        return privateKey;
    }

    public Key getPublicKey() {
        return publicKey;
    }

    public String ConvertParamsToJson(String params) {
        String[] separated = params.split("&");
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        sb.append('"');
        sb.append('{');
        for (String line : separated) {
            if (sb.length() > 3)
                sb.append(",");
            StringBuilder sb_content = new StringBuilder();
            String[] separated1 = line.split("=");
            for (String line1 : separated1) {
                if (sb_content.length() > 0)
                    sb_content.append(":");
                sb_content.append('"');
                sb_content.append(line1);
                sb_content.append('"');
                if (separated1.length == 1) {
                    sb_content.append(':');
                    sb_content.append('"');
                    sb_content.append('"');
                }
            }
            sb.append(sb_content.toString());
        }
        sb.append('}');
        sb.append('"');
        sb.append('}');
        return sb.toString();
    }

    public String encrypt(String plaintext) {
        ArrayList<String> arraytext = new ArrayList<>();
        int leng = (int) Math.ceil(plaintext.length() / 100.0);
        for (int i = 0; i < leng; i++) {
            System.out.println(i);
            if (i == leng - 1) {
                arraytext.add(plaintext);
            } else {
                arraytext.add(plaintext.substring(0, 100));
                plaintext = plaintext.substring(100);
            }
        }
        byte[] encodedBytes;
        String publickey = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA8fCtwbVpJWApWxGSWfzpk36egc5rn4mGHvw0c+6m7DWG3LUbHlKxz3/MmQMBINM/nhdJLzwMtwAQl9SNsYSBHqsWtqt1CWjnAVynyuKx2dMCCtnxsdaAcbCcxyqHlVPtT9XKWz0gg8zKbRaPXSqyjkBgtvEGRxGfFruvR27asawVTxZAT/afAGpNCxnER1VfJMA48C7/ap2FSPsnVD4oX1ECOjoVxHsxHmAufbtcFrVoQvQM/FRZwlON+R05ljEq31oIv3TW+p65Bgxoo1W+U05i0ugco6M/sQ+jl9mTLG0ocd+z//LGyyJJ3RgbOl2G+vPp4csegIRdc4uPBXnXPGoAa16aKNv29Qi0ll/yhVWPuIg6oc46TqWayYtZqSeYHic9CKNvYs3N2d4B44i2GVHzECDfbsKWkZxiwlt9s08PPKuh2kKc3aAKavJd1e+5wfvW/Fk1j2435rSvhwNn1reMjDIZlQFzEBO2oJzSD5oDEfNNURk+UMU7y9lRfYfRiWbjokqtdlHcsM/etYCTnzgDaE+41NW1NZ7Ve0DrOh8d3xRS5upsrDiirCq8TtwZm5Dom79GrTFwH8Vgk+dZUnLK2Lo7yhl+TgXBdwdWxzEKKvJFYzLaUTETFQxuabpwo/yocCAFl5XL8WHuVdxICRWEg38tHnbp93Co3vcZAGsCAwEAAQ==";
        StringBuilder sb = new StringBuilder();
        try {
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            byte[] decodedKey = Base64.decode(publickey, 0);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            publicKey = kf.generatePublic(new X509EncodedKeySpec(decodedKey));
            for (String text : arraytext) {
                if (sb.length() > 1)
                    sb.append("|manis|");
                c.init(Cipher.ENCRYPT_MODE, publicKey);
                encodedBytes = c.doFinal(text.getBytes());
                String temp = Base64.encodeToString(encodedBytes, Base64.NO_WRAP);
                sb.append(temp);
            }

        } catch (Exception e) {
            Log.e(TAG, "RSA encryption error");
        }
        return sb.toString();
    }
}
