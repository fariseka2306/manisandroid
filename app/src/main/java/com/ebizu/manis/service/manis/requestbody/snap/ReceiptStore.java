package com.ebizu.manis.service.manis.requestbody.snap;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 8/21/17.
 */

public class ReceiptStore implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("location")
    @Expose
    private String location;

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ReceiptStore() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.id);
        dest.writeString(this.category);
        dest.writeString(this.location);
    }

    protected ReceiptStore(Parcel in) {
        this.name = in.readString();
        this.id = in.readString();
        this.category = in.readString();
        this.location = in.readString();
    }

    public static final Creator<ReceiptStore> CREATOR = new Creator<ReceiptStore>() {
        @Override
        public ReceiptStore createFromParcel(Parcel source) {
            return new ReceiptStore(source);
        }

        @Override
        public ReceiptStore[] newArray(int size) {
            return new ReceiptStore[size];
        }
    };
}
