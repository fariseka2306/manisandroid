package com.ebizu.manis.service.reward.requestbody;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/26/17.
 */

public class RewardRegisterBody extends RewardApiBody {

    @SerializedName("data")
    @Expose
    private Data data;

    public RewardRegisterBody(RewardRegisterBody.Data data) {
        super();
        this.data = data;
        this.module = RewardApiConfig.Module.SESSION;
        this.action = RewardApiConfig.Action.REGISTER;
    }

    public static class Data {
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("userName")
        @Expose
        private String userName;
        @SerializedName("userEmail")
        @Expose
        private String userEmail;
        @SerializedName("userCountry")
        @Expose
        private String userCountry;
        @SerializedName("userTimezone")
        @Expose
        private String userTimezone;
        @SerializedName("userJoin")
        @Expose
        private String userJoin;
        @SerializedName("userAgent")
        @Expose
        private String userAgent;
        @SerializedName("ipAddress")
        @Expose
        private String ipAddress = "";
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lon")
        @Expose
        private String lon;
        @SerializedName("osName")
        @Expose
        private String osName = "";
        @SerializedName("osVersion")
        @Expose
        private String osVersion;
        @SerializedName("applicationBuild")
        @Expose
        private String applicationBuild = "";
        @SerializedName("applicationVersion")
        @Expose
        private String applicationVersion = "";
        @SerializedName("manufacture")
        @Expose
        private String manufacture;
        @SerializedName("model")
        @Expose
        private String model;
        @SerializedName("provider")
        @Expose
        private String provider;
        @SerializedName("msisdn")
        @Expose
        private String msisdn;
        @SerializedName("imei")
        @Expose
        private String imei;
        @SerializedName("imsi")
        @Expose
        private String imsi;

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        public void setUserCountry(String userCountry) {
            this.userCountry = userCountry;
        }

        public void setUserTimezone(String userTimezone) {
            this.userTimezone = userTimezone;
        }

        public void setUserJoin(String userJoin) {
            this.userJoin = userJoin;
        }

        public void setUserAgent(String userAgent) {
            this.userAgent = userAgent;
        }

        public void setIpAddress(String ipAddress) {
            this.ipAddress = ipAddress;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public void setOsName(String osName) {
            this.osName = osName;
        }

        public void setOsVersion(String osVersion) {
            this.osVersion = osVersion;
        }

        public void setApplicationBuild(String applicationBuild) {
            this.applicationBuild = applicationBuild;
        }

        public void setApplicationVersion(String applicationVersion) {
            this.applicationVersion = applicationVersion;
        }

        public void setManufacture(String manufacture) {
            this.manufacture = manufacture;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public void setMsisdn(String msisdn) {
            this.msisdn = msisdn;
        }

        public void setImei(String imei) {
            this.imei = imei;
        }

        public void setImsi(String imsi) {
            this.imsi = imsi;
        }
    }
}
