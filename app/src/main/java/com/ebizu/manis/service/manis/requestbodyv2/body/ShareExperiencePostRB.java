package com.ebizu.manis.service.manis.requestbodyv2.body;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.ManisRequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.data.ShareExperiencePostData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 10/12/17.
 */

public class ShareExperiencePostRB extends ManisRequestBody {

    @SerializedName("data")
    private ShareExperiencePostData data;

    public void setData(ShareExperiencePostData data) {
        this.data = data;
    }

    public ShareExperiencePostRB() {
        module = ConfigManager.Missions.SHARE_EXPERIENCE_MODUL;
        action = ConfigManager.Missions.SHARE_EXPERIENCE_EXECUTE_ACTION;
    }
}
