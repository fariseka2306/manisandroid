package com.ebizu.manis.service.manis.requestbody;

import com.ebizu.manis.model.DataAuth;
import com.ebizu.manis.model.Phone;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 27/07/2017.
 */

public class DataPhoneAuth extends DataAuth {

    @SerializedName("phone")
    Phone phone;

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }
}
