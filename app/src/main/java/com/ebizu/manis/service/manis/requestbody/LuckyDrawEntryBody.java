package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by ebizu on 08/08/17.
 */

public class LuckyDrawEntryBody {

    private int page;
    private int size;

    public LuckyDrawEntryBody() {
    }

    public LuckyDrawEntryBody(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
