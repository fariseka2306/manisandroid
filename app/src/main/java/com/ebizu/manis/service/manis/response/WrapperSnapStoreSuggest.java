package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.snap.SnapStoreSuggest;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ebizu-User on 07/08/2017.
 */

public class WrapperSnapStoreSuggest extends ResponseData {

    @SerializedName("data")
    SnapStoreSuggest snapStoreSuggest;

    public SnapStoreSuggest getSnapStoreSuggest() {
        return snapStoreSuggest;
    }
}
