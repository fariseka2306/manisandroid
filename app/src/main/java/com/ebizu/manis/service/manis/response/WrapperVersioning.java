package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Versioning;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 8/22/17.
 */

public class WrapperVersioning extends ResponseData {

    @SerializedName("data")
    @Expose
    private Versioning versioning;

    public Versioning getVersioning() {
        return versioning;
    }

    public void setVersioning(Versioning versioning) {
        this.versioning = versioning;
    }
}
