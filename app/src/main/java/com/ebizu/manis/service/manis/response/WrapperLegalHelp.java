package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.LegalHelp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abizu-alvio on 7/24/2017.
 */

public class WrapperLegalHelp {
    @SerializedName("data")
    @Expose
    LegalHelp legalHelp;

    public LegalHelp getLegalHelp() {
        return legalHelp;
    }
}
