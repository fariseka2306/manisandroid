package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by FARIS_mac on 10/4/17.
 */

public class RedeemCloseFailedBody {

    private String id;
    private String ref;
    private long point;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public long getPoint() {
        return point;
    }

    public void setPoint(long point) {
        this.point = point;
    }
}
