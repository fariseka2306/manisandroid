package com.ebizu.manis.service.manis.requestbodyv2.bodysuper;

import com.ebizu.manis.model.tracker.ManisTracker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ebizu on 12/14/17.
 */

public class ManisTrackerRequestBody {

    @SerializedName("function")
    @Expose
    private String function;
    @SerializedName("command")
    @Expose
    private String command;
    @SerializedName("param")
    @Expose
    private List<ManisTracker> manisTrackers;

    public ManisTrackerRequestBody() {
        this.function = "trackMeManis";
        this.command = "Route";
    }

    public void setManisTrackers(List<ManisTracker> manisTrackers) {
        this.manisTrackers = manisTrackers;
    }
}
