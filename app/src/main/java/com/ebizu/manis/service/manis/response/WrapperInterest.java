package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.Interest;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Ebizu-User on 21/07/2017.
 */

public class WrapperInterest extends ResponseData {

    @SerializedName("data")
    ArrayList<Interest> interests;

    public ArrayList<Interest> getInterests() {
        return interests;
    }
}
