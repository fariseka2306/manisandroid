package com.ebizu.manis.service.reward.requestbody;

import android.content.Context;

import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class RewardBulkDetailBody extends RewardSessionApiBody {

    @SerializedName("data")
    @Expose
    private Data data;

    public RewardBulkDetailBody(Context context, Data data) {
        super(context);
        this.data = data;
        this.module = RewardApiConfig.Module.VOUCHER;
        this.action = RewardApiConfig.Action.COMBO_DEALS;
    }

    public static class Data {
        @SerializedName("bulkId")
        @Expose
        private String bulkId = "";

        public void setBulkId(String bulkId) {
            this.bulkId = bulkId;
        }
    }

}
