package com.ebizu.manis.service.reward.requestbody;

import android.content.Context;

import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.requestbody.require.RewardSessionApiBody;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by FARIS_mac on 11/9/17.
 */

public class RewardGeneralListBody extends RewardSessionApiBody {

    @SerializedName("data")
    @Expose
    private Data data;

    public RewardGeneralListBody(Context context, Data data) {
        super(context);
        this.data = data;
        this.module = RewardApiConfig.Module.VOUCHER;
        this.action = RewardApiConfig.Action.LIST;
    }

    public static class Data {
        @SerializedName("noCache")
        @Expose
        private String noCache;
        @SerializedName("showEmptyStock")
        @Expose
        private String showEmptyStock;
        @SerializedName("brands")
        @Expose
        private List<Object> brands = null;
        @SerializedName("categories")
        @Expose
        private List<String> categories = null;
        @SerializedName("order")
        @Expose
        private Integer order;
        @SerializedName("halal")
        @Expose
        private List<Object> halal = null;
        @SerializedName("keyword")
        @Expose
        private String keyword;
        @SerializedName("beacons")
        @Expose
        private List<Object> beacons = null;
        @SerializedName("page")
        @Expose
        private Integer page;
        @SerializedName("size")
        @Expose
        private Integer size;

        public void setNoCache(String noCache) {
            this.noCache = noCache;
        }

        public void setShowEmptyStock(String showEmptyStock) {
            this.showEmptyStock = showEmptyStock;
        }

        public void setBrands(List<Object> brands) {
            this.brands = brands;
        }

        public void setCategories(List<String> categories) {
            this.categories = categories;
        }

        public void setOrder(Integer order) {
            this.order = order;
        }

        public void setHalal(List<Object> halal) {
            this.halal = halal;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public void setBeacons(List<Object> beacons) {
            this.beacons = beacons;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public void setSize(Integer size) {
            this.size = size;
        }
    }

}
