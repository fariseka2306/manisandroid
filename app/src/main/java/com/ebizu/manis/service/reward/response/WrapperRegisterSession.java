package com.ebizu.manis.service.reward.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ebizu on 9/26/17.
 */

public class WrapperRegisterSession extends ResponseRewardApi {

    @SerializedName("data")
    @Expose
    private RewardSession rewardSession;


    public RewardSession getRewardSession() {
        return rewardSession;
    }

    public void setRewardSession(RewardSession rewardSession) {
        this.rewardSession = rewardSession;
    }
}
