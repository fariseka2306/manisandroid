package com.ebizu.manis.service.manis.requestbody;

/**
 * Created by abizu-alvio on 8/1/2017.
 */

public class ProfileBody {
    private String photo;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
