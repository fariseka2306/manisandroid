package com.ebizu.manis.service.manis.response;

import com.ebizu.manis.model.LuckyDrawUpload;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by halim_ebizu on 8/21/17.
 */

public class WrapperLuckyDrawUpload extends ResponseData {

    @SerializedName("data")
    @Expose
    public LuckyDrawUpload luckyDrawUpload;

    public LuckyDrawUpload getLuckyDrawUpload() {
        return luckyDrawUpload;
    }

    public void setLuckyDrawUpload(LuckyDrawUpload luckyDrawUpload) {
        this.luckyDrawUpload = luckyDrawUpload;
    }
}
