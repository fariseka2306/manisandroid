package com.ebizu.manis.payment.molpay;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.model.purchasevoucher.CustomerDetails;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.model.purchasevoucher.Product;
import com.molpay.molpayxdk.MOLPayActivity;

import java.util.HashMap;

/**
 * Created by ebizu on 10/9/17.
 */

public class MolPayTransactionRequest {

    private Context context;
    private Product product;
    private CustomerDetails customerDetails;

    public MolPayTransactionRequest(Context context, Order order) {
        this.context = context;
        this.product = order.getProduct();
        this.customerDetails = order.getCustomerDetails();
    }

    public HashMap<String, Object> getPaymentDetails() {
        HashMap<String, Object> paymentDetails = new HashMap<>();
        paymentDetails.put(MOLPayActivity.mp_amount, product.getAmount());

        paymentDetails.put(MOLPayActivity.mp_username, context.getString(R.string.molPayUserName));
        paymentDetails.put(MOLPayActivity.mp_password, context.getString(R.string.molPayPassword));
        paymentDetails.put(MOLPayActivity.mp_merchant_ID, context.getString(R.string.molPayMerchantId));
        paymentDetails.put(MOLPayActivity.mp_app_name, context.getString(R.string.molPayAppName));
        paymentDetails.put(MOLPayActivity.mp_currency, "MYR");
        paymentDetails.put(MOLPayActivity.mp_country, "MY");
        paymentDetails.put(MOLPayActivity.mp_verification_key, context.getString(R.string.molpayVerificationKey));
        paymentDetails.put(MOLPayActivity.mp_channel_editing, false);
        paymentDetails.put(MOLPayActivity.mp_editing_enabled, false);
        paymentDetails.put(MOLPayActivity.mp_request_type, "");
        paymentDetails.put(MOLPayActivity.mp_sandbox_mode, context.getResources().getBoolean(R.bool.molPaySandBox));
        paymentDetails.put(MOLPayActivity.mp_dev_mode, context.getResources().getBoolean(R.bool.molPayDevMob));

        paymentDetails.put(MOLPayActivity.mp_order_ID, product.getOrderId());
        paymentDetails.put(MOLPayActivity.mp_bill_description, product.getName());
        paymentDetails.put(MOLPayActivity.mp_bill_name, customerDetails.getName());
        paymentDetails.put(MOLPayActivity.mp_bill_email, customerDetails.getEmail());
        paymentDetails.put(MOLPayActivity.mp_bill_mobile, customerDetails.getPhoneNo());
        return paymentDetails;
    }

    public HashMap<String, Object> getPaymentDetailsExample() {
        HashMap<String, Object> paymentDetails = new HashMap<>();

        // Mandatory String. A value not less than '1.00'
        paymentDetails.put(MOLPayActivity.mp_amount, product.getAmount());

        // Mandatory String. Values obtained from MOLPay
        paymentDetails.put(MOLPayActivity.mp_username, context.getString(R.string.molPayUserName));
        paymentDetails.put(MOLPayActivity.mp_password, context.getString(R.string.molPayPassword));
        paymentDetails.put(MOLPayActivity.mp_merchant_ID, context.getString(R.string.molPayMerchantId));
        paymentDetails.put(MOLPayActivity.mp_app_name, context.getString(R.string.molPayAppName));

        // Mandatory String. Payment values
        paymentDetails.put(MOLPayActivity.mp_order_ID, product.getOrderId());
        paymentDetails.put(MOLPayActivity.mp_currency, "MYR");
        paymentDetails.put(MOLPayActivity.mp_country, "MY");

        // Optional String.
        paymentDetails.put(MOLPayActivity.mp_channel, "multi"); // Use 'multi' for all available channels option. For individual channel seletion, please refer to "Channel Parameter" in "Channel Lists" in the MOLPay API Spec for Merchant pdf.
        paymentDetails.put(MOLPayActivity.mp_bill_description, product.getName());
        paymentDetails.put(MOLPayActivity.mp_bill_name, customerDetails.getName());
        paymentDetails.put(MOLPayActivity.mp_bill_email, customerDetails.getEmail());
        paymentDetails.put(MOLPayActivity.mp_bill_mobile, customerDetails.getPhoneNo());
        paymentDetails.put(MOLPayActivity.mp_channel_editing, false); // Option to allow channel selection.
        paymentDetails.put(MOLPayActivity.mp_editing_enabled, false); // Option to allow billing information editing.

        // Optional for Escrow
        paymentDetails.put(MOLPayActivity.mp_is_escrow, ""); // Put "1" to enable escrow

        // Optional for credit card BIN restrictions
        String binlock[] = {"414170", "414171"};
        paymentDetails.put(MOLPayActivity.mp_bin_lock, binlock);
        paymentDetails.put(MOLPayActivity.mp_bin_lock_err_msg, "Only UOB allowed");

        // For transaction request use only, do not use this on payment process
        paymentDetails.put(MOLPayActivity.mp_transaction_id, "");

        // Optional, provide a valid cash channel transaction id here will display a payment instruction screen.
        paymentDetails.put(MOLPayActivity.mp_request_type, "");

        // Optional, use this to customize the UI theme for the payment info screen, the original XDK custom.css file is provided at Example project source for reference and implementation.
        paymentDetails.put(MOLPayActivity.mp_custom_css_url, "file:///android_asset/custom.css");

        // Optional, set the token id to nominate a preferred token as the default selection, set "new" to allow new card only
        paymentDetails.put(MOLPayActivity.mp_preferred_token, "");

        // Optional, credit card transaction type, set "AUTH" to authorize the transaction
        paymentDetails.put(MOLPayActivity.mp_tcctype, "");

        // Optional, set true to process this transaction through the recurring api, please refer the MOLPay Recurring API pdf
        paymentDetails.put(MOLPayActivity.mp_is_recurring, false);

        // Optional for channels restriction
        String allowedchannels[] = {"credit", "credit3"};
        paymentDetails.put(MOLPayActivity.mp_allowed_channels, allowedchannels);

        // Optional for sandboxed development environment, set boolean value to enable.
        paymentDetails.put(MOLPayActivity.mp_sandbox_mode, true);

        // Optional, required a valid mp_channel value, this will skip the payment info page and go direct to the payment screen.
        paymentDetails.put(MOLPayActivity.mp_express_mode, true);

        // Optional, enable this for extended email format validation based on W3C standards.
        paymentDetails.put(MOLPayActivity.mp_advanced_email_validation_enabled, true);

        // Optional, enable this for extended phone format validation based on Google i18n standards.
        paymentDetails.put(MOLPayActivity.mp_advanced_phone_validation_enabled, true);

        // Optional, explicitly force disable billing name edit.
        paymentDetails.put(MOLPayActivity.mp_bill_name_edit_disabled, true);

        // Optional, explicitly force disable billing email edit.
        paymentDetails.put(MOLPayActivity.mp_bill_email_edit_disabled, true);

        // Optional, explicitly force disable billing mobile edit.
        paymentDetails.put(MOLPayActivity.mp_bill_mobile_edit_disabled, true);

        // Optional, explicitly force disable billing description edit.
        paymentDetails.put(MOLPayActivity.mp_bill_description_edit_disabled, true);

        // Optional, EN, MS, VI, TH, FIL, MY, KM, ID, ZH.
        paymentDetails.put(MOLPayActivity.mp_language, "EN");

        // Optional, enable for online sandbox testing.
        paymentDetails.put(MOLPayActivity.mp_dev_mode, false);
        return paymentDetails;
    }

}
