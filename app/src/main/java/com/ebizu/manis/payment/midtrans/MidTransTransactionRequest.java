package com.ebizu.manis.payment.midtrans;

import android.content.Context;

import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.purchasevoucher.Order;
import com.ebizu.manis.preference.ManisSession;
import com.midtrans.sdk.corekit.core.Constants;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.models.BillingAddress;
import com.midtrans.sdk.corekit.models.CustomerDetails;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.ShippingAddress;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.CreditCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu on 10/4/17.
 */

public class MidTransTransactionRequest extends TransactionRequest {

    private Order order;
    private Account account;
    private CustomerDetails customerDetails;
    private ArrayList<UserAddress> userAddresses;
    private ShippingAddress shippingAddress;
    private BillingAddress billingAddress;

    public MidTransTransactionRequest(Context context, Order order) {
        super(order.getProduct().getOrderId(), (int) order.getProduct().getTotalAmount());
        this.order = order;
        this.account = new ManisSession(context).getAccountSession();
        this.customerDetails = new CustomerDetails();
        this.userAddresses = new ArrayList<>();
        this.shippingAddress = new ShippingAddress();
        this.billingAddress = new BillingAddress();
        setUserDetails();
        setCustomerDetails();
        setShippingAddress();
        setBillingAddress();
        setItemDetails();
        setCreditCard();
    }

    public void setUserDetails() {
        UserDetail userDetail = new UserDetail();
        userDetail.setUserFullName(order.getCustomerDetails().getName());
        userDetail.setEmail(order.getCustomerDetails().getEmail());
        userDetail.setPhoneNumber(order.getCustomerDetails().getPhoneNo());
        userDetail.setUserId(this.account.getAccId());

        UserAddress shippingUserAddress = new UserAddress();
        shippingUserAddress.setAddress("");
        shippingUserAddress.setCity("");
        shippingUserAddress.setCountry("");
        shippingUserAddress.setZipcode("");
        shippingUserAddress.setAddressType(Constants.ADDRESS_TYPE_SHIPPING);
        userAddresses.add(shippingUserAddress);

        UserAddress billingUserAddress = new UserAddress();
        billingUserAddress.setAddress("");
        billingUserAddress.setCity("");
        billingUserAddress.setCountry("");
        billingUserAddress.setZipcode("");
        billingUserAddress.setAddressType(Constants.ADDRESS_TYPE_BILLING);
        userAddresses.add(billingUserAddress);

        UserAddress userAddress = new UserAddress();
        userAddress.setAddress("");
        userAddress.setCity("");
        userAddress.setCountry("");
        userAddress.setZipcode("");
        userAddress.setAddressType(Constants.ADDRESS_TYPE_BOTH);
        userAddresses.add(userAddress);
        userDetail.setUserAddresses(userAddresses);

        LocalDataHandler.saveObject("user_details", userDetail);
    }

    private void setCustomerDetails() {
        CustomerDetails customerDetails = new CustomerDetails();
        customerDetails.setFirstName(order.getCustomerDetails().getName());
        customerDetails.setEmail(order.getCustomerDetails().getEmail());
        customerDetails.setPhone(order.getCustomerDetails().getPhoneNo()
        );

        setCustomerDetails(customerDetails);
    }

    public void setShippingAddress() {
        ArrayList<ShippingAddress> shippingAddresses = new ArrayList<>();

        shippingAddress.setFirstName(order.getCustomerDetails().getName());
        shippingAddress.setPhone(order.getCustomerDetails().getPhoneNo());
        customerDetails.setShippingAddress(shippingAddress);

        shippingAddresses.add(shippingAddress);
        setShippingAddressArrayList(shippingAddresses);
    }

    public void setBillingAddress() {
        ArrayList<BillingAddress> billingAddresses = new ArrayList<>();
        billingAddress.setFirstName(order.getCustomerDetails().getName());
        billingAddress.setPhone(order.getCustomerDetails().getPhoneNo());
        customerDetails.setBillingAddress(billingAddress);

        billingAddresses.add(billingAddress);
        setBillingAddressArrayList(billingAddresses);
    }

    public void setItemDetails() {
        ItemDetails itemDetails = new ItemDetails(order.getProduct().getOrderId(), (int) order.getProduct().getAmount(), 1, order.getProduct().getName());
        List<ItemDetails> listItem = new ArrayList<>();
        listItem.add(itemDetails);

        ArrayList<ItemDetails> itemDetailsArrayList = new ArrayList<>();
        itemDetailsArrayList.addAll(listItem);
        setItemDetails(itemDetailsArrayList);
    }

    public void setCreditCard() {
        CreditCard creditCardOptions = new CreditCard();
        creditCardOptions.setSaveCard(true);
        creditCardOptions.setSecure(false);
        setCreditCard(creditCardOptions);
    }
}
