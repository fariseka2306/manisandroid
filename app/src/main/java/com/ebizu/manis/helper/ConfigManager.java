package com.ebizu.manis.helper;

import android.os.Environment;

/**
 * Created by Ebizu-User on 12/07/2017.
 */

public class ConfigManager {

    public static class App {
        public static String INTRODUCTION = "introduction";
        public static String INTERVAL_BEACON = "interval";
        public static String TYPE_INTERVAL_BEACON = "type-interval";
        public static String LOGIN_ASK_DIALOG_PERMISSION = "type-dialog-permission";
        public static String SNAP_EARN_GUIDE_SESSION = "device.session-snap-earn-guide";
        public static String SNAP_EARN_TICKET_GUIDE_SESSION = "device.session-snap-earn-ticket-guide";
    }

    public static class Analytic {

        public static class ScreenView {
            public static String FRAGMENT_ACCOUNT = "Fragment Account";
            public static String FRAGMENT_ONBOARDING_EARN = "Fragment OnBoarding Earn";
            public static String FRAGMENT_ONBOARDING_SNAP = "Fragment OnBoarding Snap";
            public static String FRAGMENT_ONBOARDING_STUFF = "Fragment OnBoarding Stuff";
            public static String FRAGMENT_HOME = "Fragment Home";
            public static String FRAGMENT_LUCKY_DRAW = "Fragment Lucky Draw";
            public static String FRAGMENT_REWARD = "Fragment Reward";
            public static String FRAGMENT_SAVED_ACTIVITY_ALL = "Fragment SavedActivity All";
            public static String FRAGMENT_SAVED_ACTIVITY_OFFERS = "Fragment SavedActivity Offers";
            public static String FRAGMENT_SAVED_ACTIVITY_REDEEMABLE = "Fragment SavedActivity Redeemable";
            public static String FRAGMENT_SAVED_ACTIVITY_REWARDS = "Fragment SavedActivity Rewards";
            public static String FRAGMENT_SNAP_FOLLOWED = "Fragment Snap Followed";
            public static String FRAGMENT_SNAP_NEARBY = "Fragment Snap Nearby";
            public static String FRAGMENT_SNAP_GUIDE_FIRST = "Fragment Snap Guide First";
            public static String FRAGMENT_SNAP_GUIDE_SECOND = "Fragment Snap Guide Second";
            public static String FRAGMENT_SNAP_GUIDE_THIRD = "Fragment Snap Guide Third";
            public static String FRAGMENT_SNAP_GUIDE_FOURTH = "Fragment Snap Guide Fourth";
            public static String FRAGMENT_STORE_CATEGORIES = "Fragment Stores Categories";
            public static String FRAGMENT_STORES = "Fragment Stores";
            public static String FRAGMENT_STORES_NEARBY = "Fragment Stores Nearby";
        }

        public static class Category {
            public static String ABOUT_ACTIVITY = "AboutActivity";
            public static String SNAP_HISTORY = "Snap History";
            public static String DIALOG_CHANGE = "Dialog Change";
            public static String DIALOG_TERMS = "Dialog Terms";
            public static String DIALOG_OK = "Dialog OK";
            public static String DIALOG_CLOSE = "Dialog Close";
            public static String DIALOG_REVIEW = "Dialog Fragment";
            public static String DIALOG_REDEMPTION_HISTORY_PHYSICAL = "Dialog Redemption History Physical";
            public static String FRAGMENT_SNAP_STORE = "Fragment Snap Store";
            public static String FRAGMENT_HOME = "Fragment Home";
            public static String FRAGMENT_NOTIFICATION = "Fragment Notification";
            public static String FRAGMENT_STORES = "Fragment Stores";
            public static String FRAGMENT_STORES_CATEGORIES = "Fragment Stores Categories";
            public static String FRAGMENT_STORES_NEARBY = "Fragment Stores Nearby";
            public static String FRAGMENT_REWARD_FREE = "Fragment Reward Free";
            public static String FRAGMENT_SNAP_RECENT = "Fragment Snap Recent";
            public static String FRAGMENT_SAVED_ACTIVITY_REDEEMABLE = "Fragment SavedActivity Redeemable";
            public static String FRAGMENT_ACCOUNT = "Fragment Account";
            public static String FRAGMENT_REWARD_POINT = "Fragment Reward Point";
            public static String HELP_ACTIVITY = "Help Activity";
            public static String HELP_ACTIVITY_DETAIL = "HelpActivity Detail";
            public static String INTEREST = "Interest";
            public static String LOGIN_ACTIVITY = "Login Activity";
            public static String MAIN_ACTIVITY = "MainActivity";
            public static String OFFER = "Offer";
            public static String ON_BOARDING = "OnBoarding";
            public static String PROFILE_ACTIVITY = "Profile Activity";
            public static String REDEMPTION_HISTORY = "Redemption History";
            public static String REWARD_POINT_DETAIL = "Reward Point Detail";
            public static String STORE_LOCATION = "Store Location";
            public static String SNAP = "Snap";
            public static String SNAP_POP_UP_DETAIL = "Snap Pop Up Detail";
            public static String SNAP_CONFIRMATION = "Snap Confirmation";
            public static String SNAP_GUIDE_FIRST_FRAGMENT = "SnapGuideFirstFragment";
            public static String DIALOG_REDEMPTION_HISTORY = "Dialog Redemption History";
            public static String DIALOG_CHANGE_PROFILEACTIVITY_PICTURE = "Dialog Change ProfileActivity Picture";
            public static String NOTIFICATION_SETTINGS_ACTIVITY = "Notification SettingsActivity";
            public static String PURCHASED_HISTORY = "Purchased History";
            public static String SAVED_ACTIVITY = "SavedActivity";
            public static String SETTING_ACTIVITY = "SettingsActivity";
            public static String SNAP_BY_BRAND = "Snap by brand";
        }

        public static class Action {
            public static String CLICK = "Click";
            public static String ITEM_CLICK = "Item Click";
            public static String SWIPE = "Swipe";
            public static String REFRESH = "Item Click";
            public static String SEARCH = "Refresh";
            public static String ITEM_STORE = "Item Store";
        }
    }

    public static class FacebookParam {
        public static String EMAIL = "email";
        public static String USER_BIRTHDAY = "user_birthday";
        public static String USER_LOCATION = "user_location";
        public static int REQUEST_CODE = 64206;
    }

    public static class TwitterParam {
        public static int REQUEST_CODE = 140;
    }

    public static class ManisEndPoint {
        public static final String SIGNIN_PREF = "com.ebizu.manis.signin";
        public static final String KEY_PREF = "com.ebizu.manis.key";
        public static final String API_ENV_DEVELOPMENT = "DEVELOPMENT";
        public static final String API_ENV_STAGING = "STAGING";
        public static final String API_ENV_PRODUCTION = "PRODUCTION";
        public static final String TOKEN_EXPIRED_INTENT_ACTION = "com.ebizu.manis.TOKEN_EXPIRED";
        public static final String TOKEN_EXPIRED_INTENT_NAME = "com.ebizu.manis.TOKEN_EXPIRED.message";
    }

    public class SessionName {
        public String ACTIVE_ENV = "com.ebizu.manis.sdk.session-ENV";
        public String LANG = "com.ebizu.manis.sdk.session-lang";
        public String TOKEN = "com.ebizu.manis.sdk.session-token";
        public String DEVICE = "com.ebizu.manis.sdk.session-device";
        public String VERSION = "com.ebizu.manis.sdk.session-version";
        public String ENCRYPT_ENABLED = "com.ebizu.manis.sdk.encrypted";
        public String LAST_KNOWN_LOCATION = "com.ebizu.manis.sdk.session-lastlocation";
        public String SHOW_EXPLANATION = "com.ebizu.manis.sdk.session-SHOW_EXPLANATION";
        public String VERSIONNAME = "com.ebizu.manis.sdk.session-versionname";
        public String VERSIONCODE = "com.ebizu.manis.sdk.session-versioncode";
    }

    public static class AccountSession {
        public static String TOKEN = "manis.session-token";
        public static String ID = "manis.session-id";
        public static String FACEBOOK_ID = "manis.session-facebook-id";
        public static String FACEBOOK_EMAIL = "manis.session-facebook-email";
        public static String GOOGLE_ID = "manis.session-google-id";
        public static String GOOGLE_EMAIL = "manis.session-google-email";
        public static String SCREEN_NAME = "manis.session-screen-name";
        public static String COUNTRY = "manis.session-country";
        public static String PHOTO = "manis.session-photo";
        public static String LAST_LOGIN = "manis.session-last-login";
        public static String DEVICE_ID = "manis.session-device-id";
        public static String STATUS = "manis.session-status";
        public static String TMZ_NAME = "manis.session-tmz-name";
        public static String GENDER = "manis.session-gender";
        public static String MSISDN = "manis.session-msisdn";
        public static String CURRENCY_ISO2 = "manis.session-currency-iso2";
        public static String CURRENCY_ISO3 = "manis.session-currency-iso3";
        public static String BIRTH_DATE = "manis.session-birth-date";
        public static String ADDRESS = "manis.session-address";
        public static String ADDRESS_POSTCODE = "manis.session-address-postcode";
        public static String ADDRESS_CITY = "manis.session-address-city";
        public static String ADDRESS_STATE = "manis.session-address-state";
        public static String ADDRESS_COUNTRY = "manis.session-address-country";
        public static String OTP_STATUS = "manis.session-otp-status";
        public static String FIRST_LOGIN = "manis.session-first-login";
        public static String POINT = "manis.session-point";
        public static String UPDATEABLE_EMAIL = "manis.session-email";
        public static String UPDATEABLE_PHONE = "manis.session-phone";
        public static String CREATED_DATE_TIME = "manis.session-created-date-timesess";
        public static String OTP_STATUS_DIALOG = "manis.session-otp-status-dialog";
        public static String SNAP_RECEIPT_TERM_AND_CONDITION = "manis.session-receipt-term-and-condition";
        public static String SNAP_LUCKY_DRAW_TERM_AND_CONDITION = "manis.session-lucky-draw-term-and-condition";
        public static String SNAP_EARN_POINTS_TNC = "manis.session-snap-earn-points";
        public static String SNAP_EARN_TICKETS_TNC = "manis.session-snap-earn-tickets";
    }

    public static class LocationSession {
        public static String LATITUDE = "manis.session-latitude";
        public static String LONGITUDE = "manis.session-longitude";
    }

    public static class DeviceSession {
        public static String IMEI = "manis.session-imei";
        public static String IMSI = "manis.session-imsi";
        public static String MANUFACTURE = "manis.session-manufacture";
        public static String MSISDN = "manis.session-msisdn";
        public static String OPERATOR = "manis.session-operator";
        public static String TOKEN = "manis.session-token";
        public static String VERSION = "manis.session-version";
    }

    public static class Header {
        public static String TOKEN = "token";
        public static String COORDINATE = "coordinate";
        public static String PUBKEY = "pubkey";
        public static String DEVICE = "device";
        public static String MANIS_VERSION = "manis-version";
        public static String MANIS_BUILD = "manis-build";
        public static String LANG = "lang";
        public static String CONTENT_TYPE = "Content-Type";
    }

    public static class Permission {
        public static int CAMERA_REQUEST_CODE = 711;
        public static int ALL_PERMISSION_REQUEST_CODE = 987;
        public static int GMAIL_REQUEST_CODE = 9187;
        public static int FACEBOOK_REQUEST_CODE = 9287;
        public static int PHONE_REQUEST_CODE = 9387;
        public static int LOCATION_REQUEST_CODE = 93872;
    }

    public static class UpdateProfile {
        public static int CAMERA = 59;
        public static int GALLERY = 69;
        public static String UPDATE_PROFILE_TYPE_INTENT = "manis.change-profile-type";
        public static int UPDATE_PROFILE_LUCKY_DRAW_TYPE = 323;
        public static int UPDATE_PROFILE_LOGIN_TYPE = 326;
        public static int CHANGE_PROFILE_PICTURE_REQ_CODE = 324;
        public static int SUCCESS_UPDATE_PICTURE_RES_CODE = 325;
    }

    public static class Auth {
        public static String PHONE_NUMBER = "phone-number";
        public static String PHONE_AUTH = "phone-auth";
        public static String LOGIN = "login";
        public static String PHONE_AUTH_HAS_CONFIRM = "phone-auth-has_confirm";
        public static int GMAIL_REQUEST_CODE = 134;
        public static int PHONE_REQUEST_CODE = 144;
        public static String REGID_GCM_INTENT = "regid-gcm-intent";
    }

    public static class Otp {
        public static final int SIGN_IN_PHONE_REQUEST_CODE = 1441;
        public static final int SIGN_UP_OTP_REQUEST_CODE = 1442;
        public static final int FORCE_SIGN_UP_OTP = 1443;
        public static final int SUCCESS_SIGN_UP_OTP = 144;
        public static final String REQUEST_CODE = "snap-request-code";
    }

    public static class Snap {
        // end point
        public static final String URL_PATH = "snap";
        public static final String COMMAND = "snap";
        public static final String FUNCTION_UPLOAD_RECEIPT = "receiptUpload";

        public static String DATA = "snap_data";
        public static String MERCHANT_DATA = "snap_data";
        public static String RECEIPT_BITMAP_ID = "snap_receipt_bitmap_id";
        public static String RECEIPT_BITMAP_COUNT = "snap_receipt_bitmap_count";
        public static String URI_RECEIPT_PATH = Environment.getExternalStorageDirectory() + "/manis/";
        public static String URI_RECEIPT_FILE_BITMAP = "receipt";
        public static int SNAP_EARN_GUIDE_TYPE_SNAP = 235;
        public static int SNAP_EARN_GUIDE_TYPE_BRAND = 234;
        public static int SNAP_EARN_GUIDE_TYPE_STORE_DETAIL = 232;
        public static int SNAP_EARN_GUIDE_TYPE_STORE = 236;
        public static String SNAP_EARN_GUIDE = "snap-earn-guide";
        public static String RECEIPT_DATA_PARCEL = "manis.intent.receipt.data";
        public static String LUCKY_DRAW_INTENT_DATA = "intent-data-snap-lucky-draw";
        public static String OPEN_LUCKY_DRAW_FRAGMENT = "intent-data-snap-lucky-draw-fragment";
        public static int RECEIPT_REQUEST_CODE = 511;
        public static int EARN_POINT_RECEIPT_SUCCESS_UPLOAD_RS = 512;
        public static int EARN_TICKET_RECEIPT_SUCCESS_UPLOAD_RS = 514;
        public static int RECEIPT_RETAKE_RESULT_CODE = 513;
        public static int EARN_POINT_RECEIPT_DETAIL = 515;
        public static int STORE_FINISH_RECEIPT_RS = 516;
        public static int LONG_RECEIPT_RETAKE_RESULT_CODE = 518;
        public static int LONG_RECEIPT_ADD_RESULT_CODE = 519;
        public static int SNAP_EARN_GUIDE_REQUEST_CODE = 517;
        public static int GET_MORE_POINTS_RS = 517;
        public static int CANT_ACCESS_CAMERA = 897;
        public static String IS_FROM_STORES = "is_from_stores";
    }

    public static class Store {
        // intent data
        public static String INTEREST_STORE = "interest-store";
        public static String INTEREST_STORE_ID = "interest-store-id";
        public static String STORE_DETAIL_TITLE = "store-detail-title";
        public static String STORE_DETAIL_MORE = "store-detail-more";
        public static String STORE_MAP_LOCATION = "store-map-location";
        public static String STORE_OFFER = "store-offer";
        public static String STORE_REWARD = "store-reward";

        //pagination
        public static int MAX_PAGE_SIZE = 20;
        public static int FIRST_PAGE = 1;

        //tab multiplier
        public static int MULTIPLIER_ALL = -1;
        public static int MULTIPLIER_TICKET = -2;
        public static int MULTIPLIER_ONE = 1;
        public static int MULTIPLIER_TWO = 2;
        public static int MULTIPLIER_THREE = 3;
        public static int MULTIPLIER_FOUR = 4;
        public static int MULTIPLIER_FIVE = 5;
        public static String MULTIPLIER_TIER_LONGTAIL = "longtail";
        public static String MULTIPLIER_TIER_NON_LONGTAIL = "nonlongtail";
        public static String MULTIPLIER_TIER_ALL = "";
        public static int LOCATION_RC = 2331;
    }

    public static class LuckyDraw {

        // end point
        public static final String URL_PATH = "luckydraw";
        public static final String FUNCTION_GET_WINNERS = "getWinners";
        public static final String FUNCTION_GET_ENTRIES = "getEntries";
        public static final String FUNCTION_GET_SCREEN = "getScreen";
        public static final String FUNCTION_GET_TERMS = "getTerms";
        public static final String FUNCTION_UPLOAD_LUCKY_DRAW = "getSnapLuckyDraw";
        public static final String FUNCTION_CHECK_SNAP = "getChecksnap";

        // request body
        public static int LUCKY_DRAW_WINNER_PAGE = 1;
        public static int LUCKY_DRAW_WINNER_MAX_PAGE = 10;
        public static int LUCKY_DRAW_ENTRY_PAGE = 1;
        public static int LUCKY_DRAW_ENTRY_MAX_PAGE = 10;

        // lucky draw help fragment
        public static int LUCKY_DRAW_SECOND_FRAGMENT = 1;
        public static int LUCKY_DRAW_THIRD_FRAGMENT = 2;

        // page indicator
        public static final int NUM_PAGES = 3;
        public static final int SCROLL_DURATION_FACTOR = 3;
        public static final int STROKE_WIDTH = 1;

    }

    public static class Intercom {
        public static String FACEBOOK_EMAIL = "email";
        public static String FACEBOOK_ID = "facebook_id";
        public static String GOOGLE_EMAIL = "email";
        public static String GOOGLE_ID = "google_id";
        public static String PHONE_NUMBER = "msisdn";
    }

    public static class Saved {
        public static int SAVED_REQUEST_CODE = 111;
        public static int STORES_RESULT_CODE = 112;
        public static int REWARDS_RESULT_CODE = 113;
    }

    public static class Reward {

        public static class RequestRewardType {
            public static String REQUEST_REWARD_TYPE_HOME = "Home";
            public static String REQUEST_REWARD_TYPE_CATEGORY = "Category";
            public static String REQUEST_REWARD_TYPE_SEARCH = "Search";
        }

        public static String REQUEST_REWARD_TYPE = "request-reward-type";

        public static int REWARD_DATA_HOME = 631;

        //reward module
        public static String REWARD_MODULE_VOUCHER = "Voucher";

        //reward action
        public static String REWARD_ACTION_LIST = "list";

        // request body
        public static int REWARD_CATEGORY_PAGE = 1;
        public static int REWARD_CATEGORY_MAX_PAGE = 20;
        public static String REWARD_CATEGORY_KEYWORD = "category-keyword";
        public static String REWARD_BRAND_NAME = "reward-brand-name";

        public static String REWARD_CATEGORY_NAME = "category-name";
        public static String REWARD_CATEGORY_ID = "category-id";

        //        public static String REWARD_CATEGORY_ID = "reward-category-id";
        public static String REWARD = "reward";
        public static String REWARD_MY_VOUCHER = "my-voucher";
        public static String REWARD_BULK = "bulk";

        public static String REWARD_MY_VOUCHER_TITLE = "My Vouchers";

        // reward category bulk
        public static String REWARD_BULK_TITLE = "Combo Deals";

        // reward category reward
        public static String REWARD_TITLE = "REWARD";

        // My Voucher Request Body
        public static int MY_VOUCHER_FIRST_PAGE = 1;
        public static int MY_VOUCHER_MAX_PAGE_SIZE = 8;
        public static int MY_VOUCHER_ORDER = 1;
        public static String MY_VOUCHER_CONDITION_AVAILABLE = "available";
        public static String MY_VOUCHER_CONDITION_EXPIRED = "expired";


        // Reward Bulk Request Body
        public static int REWARD_BULK_FIRST_PAGE = 1;
        public static int REWARD_BULK_SIZE = 10;
        public static String REWARD_BULK_KEYWORD = "voucher";

        // Reward Bulk Request Body
        public static int REWARD_GENERAL_FIRST_PAGE = 1;
        public static int REWARD_GENERAL_SIZE = 10;

        // Purchase History Request Body
        public static int PURCHASE_HISTORY_FIRST_PAGE = 1;
        public static int PURCHASE_HISTORY_MAX_PAGE_SIZE = 10;
        public static int PURCHASE_HISTORY_ORDER = 1;

        public static String VOUCHER_TYPE_PHYSICAL = "Physical Voucher";
        public static String VOUCHER_TYPE_ECODE = "E-Code";
        public static String VOUCHER_TYPE_THIRD_PARTY = "Third Party Reward";
        public static String VOUCHER_TYPE_ETU = "ETU";
        public static String VOUCHER_TYPE_BULK = "Combo Deals";

        public static String REDEEM_TYPE_PIN = "Pin";

        // keyword for search reward voucher list
        public static String REWARD_SEARCH_KEYWORD = "rewar-search-keyword";

        // Load Type
        public enum LoadType {
            CLICK_LOAD,
            SCROLL_LOAD,
            SWIPE_LOAD
        }

        public static class RedeemType {
            public static String CODE = "Code";
            public static String PIN = "Pin";
        }
    }

    public static class Walkthrough {
        public static int WALKTHROUGH = 911;
        public static int NOTIFICATION_WALK_THROUGH = 912;
        public static int FINISH_WALK_THROUGH = 913;
    }

    public static class BrandDetails {
        public static String BRAND_DETAILS = "brand_detail";
    }

    public static class RedeemHome {
        public static String REEDEM_HOME = "redeem_home";
    }

    public static class Preference {
        public static final String KEY = "com.ebizu.manis.preference";
        public static final String KEY_DEVICE = "com.ebizu.manis.preference.device";
    }

    public static class Reedeem {
        public static final String RECEIVER_REWARD = "receiver_reward";
        public static final String FILTER_REWARD = "filter_reward";
        public static final String SORT_REWARD_TYPE = "sort_reward_type";
        public static final String ADD_REMOVE_FILTER_CATEGORY = "add_remove_filter_category";
        public static final String CLEAR_FILTER = "clear_filter";
        public static final String ADD_REMOVE_FILTER_BRAND = "add_remove_filter_brand";
        public static final String MOBILE = "Mobile";
        public static final String ADDRESS = "Address";
        public static final String CITY = "City";
        public static final String STATE = "State";
        public static final String POSTCODE = "Postcode";
    }

    public static class Beacon {
        public static final String ACTION_DETECTION = "com.ebizu.sdk.BEACON_DETECTION";
        public static final String INTENT_STRING_UUID = "uuid";
        public static final String INTENT_STRING_MAJOR = "major";
        public static final String INTENT_STRING_MINOR = "minor";
    }

    public static class BrandDetail {
        public static final String KEYWORD_BRAND = "keyword";
    }

    public static class GCM {
        public static final String INTENT_STRING_APP = "app";
        public static final String INTENT_STRING_SILENT = "silent";
        public static final String INTENT_STRING_TYPE = "type";
        public static final String INTENT_STRING_STATUS = "status";
        public static final String INTENT_STRING_URI = "uri";
        public static final String INTENT_STRING_MESSAGE = "message";
        public static final String INTENT_STRING_SNAP = "snap";
        public static final String INTENT_STRING_OFFER = "offer";
        public static final String INTENT_STRING_ID = "id";
        public static final String INTENT_STRING_POINT = "point";
        public static final String INTENT_STRING_CURRENT = "current";
        public static final String INTENT_STRING_ACCOUNT = "account";
        public static final String INTENT_STRING_ACC_PHOTO = "acc_photo";
    }

    public static class PurchaseVoucher {
        public static final String REWARD_PARCELABLE = "intent-reward";
        public static final String SHOPPING_CART_PARCELABLE = "intent-shopping-cart";
        public static final String ORDER_PARCELABLE = "intent-order";
        public static final int REQUEST_CODE = 2144;
        public static final int SUCCESS_RESULT_CODE = 2145;
        public static final int TRANS_FINISH_RESULT_CODE = 2146;
    }

    public static class Missions {
        public static final int perPage = 8;
        public static final int currentPage = 1;
        public static int MISSION_DETAIL_CODE = 152;
        public static final String applicationId = "123";
        public static final String SHARE_EXPERIENCE_MODUL = "Mission";
        public static final String SHARE_EXPERIENCE_LIST_ACTION = "list";
        public static final String SHARE_EXPERIENCE_AVAILABILITY_ACTION = "checkAvailability";
        public static final String SHARE_EXPERIENCE_EXECUTE_ACTION = "execute";
        public static final String THEMATIC_MODULE = "Mission";
        public static final String THEMATIC_ACTION = "executeWithPin";
    }

    public static class SnapViewHistory {
        public static final int STATUS_PENDING = 0;
        public static final int STATUS_APPROVED = 1;
        public static final int STATUS_REJECTED = 2;
        public static final int STATUS_PENDING_APPEAL = 3;
        public static final int STATUS_ALL = 4;
        public static final int ORDER_DATE = 0;
        public static final int ORDER_AMOUNT = 1;
        public static final int SORT_ASC = 0;
        public static final int SORT_DESC = 1;
        public static final int PAGE = 1;
        public static final int SIZE = 20;
    }

    public static class PurchaseHistory {
        public static final String FILTER_BY_ALL = "All";
        public static final String FILTER_BY_PAID = "Paid";
        public static final String FILTER_BY_UNPAID = "Unpaid";
        public static final String FILTER_BY_PROCESS = "Process";

        public static final String STATUS_PAID = "paid";
        public static final String STATUS_UNPAID = "unpaid";
        public static final String STATUS_PROCESSING = "processing";
        public static final String STATUS_PENDING = "pending";

        public static final String PYSHICAL_STATUS_SHIPPED = "SHIPPED";

        public static final String ORDER_BY_DATE = "Date";
        public static final String ORDER_BY_AMOUNT = "Amount";
    }

    public static class MyVoucherAvailable {
        public static final String SHARE_EXPERIENCE_SESSION = "5a356c5022fb76885835d14b57e9e372";
        public static final String SHARE_EXPERIENCE_MODULE = "Voucher";
        public static final String SHARE_EXPERIENCE_ACTION = "myVoucherAvailable";
        public static final String SHARE_EXPERIENCE_TOKEN = "6591c2bbdfbc551da16c4344eeb54817";
        public static final String REWARD_TYPE_ETU = "ETU";
        public static final String REWARD_TYPE_ECODE = "E-Code";
        public static final String REWARD_TYPE_PIN = "Pin";
        public static final String REWARD_TYPE_PHYSICAL_VOUCHER = "Physical Voucher";

    }

    public static class RedeemPhysicalInput {
        public static final String INPUT_PHONE_NUMBER = "Phone Number";
        public static final String INPUT_MOBILE = "Mobile";
        public static final String INPUT_CITY = "City";
        public static final String INPUT_POST_CODE = "Postcode";
        public static final String INPUT_SHIPPING_ADDRESS = "Shipping Address";
        public static final String INPUT_STATE = "State";
    }

    public static class MissionStatusCode {
        public static final int MISSION_SUCCESS = 11;
        public static final int MISSION_ERROR = 10;
        public static final int MISSION_TOKEN_EXPIRED = 12;
        public static final int MISSION_EXECUTE_PIN_FAILED = 15;
        public static final int MISSION_EXECUTE_FAILED = 16;
    }

    public static class Account {
        // end point
        public static final String URL_PATH = "account";
        public static final String FUNCTION_GET_POINT = "authPoint";
        public static final String FUNCTION_UPDATE_PROFILE = "authUpdateProfile";
        public static final String FUNCTION_GET_NOTIF = "authSettingNotification";
        public static final String FUNCTION_UPDATE_NOTIF = "authUpdateSettingNotification";
        public static final String FUNCTION_UPDATE_DEVICE_TOKEN = "authUpdateDeviceToken";
        public static final String FUNCTION_GET_TERMS = "authTerms";
        public static final String FUNCTION_GET_ACCOUNT = "authAccountResponse";
        public static final String FUNCTION_UPDATE_PROFILE_PICT = "authUpdatePictureProfile";
    }

    public static class Tracker {
        public static String PREF_IP_ADDRESS = "ip-address";
        public static String PREF_LAST_UPDATE = "last-update-tracker";
    }

    public static class ManisLegal {
        public static final String URL_PATH = "miscellaneous";
        public static final String FUNCTION_MANIS_LEGAL = "legalHelp";
        public static final String COMMAND_MANIS_LEGAL = "Miscellaneous";
        public static final String TNC_TYPE_SNE = "tnc_sne";
        public static final String TNC_TYPE_NON_SNE = "";
        public static final String TNC_TOU = "Terms of Use";
        public static final String TNC_PP = "Privacy Policy";
    }

    public static final String API_TAG = "request API";

}