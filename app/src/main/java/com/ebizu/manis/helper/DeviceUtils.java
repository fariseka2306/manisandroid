package com.ebizu.manis.helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.ebizu.manis.model.Device;


/**
 * Created by Ebizu-User on 05/07/2017.
 */

public class DeviceUtils {
    private static final String TAG = "DeviceUtils";
    private static String deviceId = "No device ID";

    public static String getDeviceID(Context context) {
        try {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                deviceId = telephonyManager.getDeviceId();
            }
        } catch (Exception e) {
            Log.e(TAG, "getDeviceID: " + e.getMessage());
        }
        return deviceId;
    }

    public static Device getDevice(AppCompatActivity appCompatActivity) {
        Device device = new Device();
        try {
            TelephonyManager phone = (TelephonyManager) appCompatActivity.getSystemService(Context.TELEPHONY_SERVICE);
            device.setManufacture(Build.MANUFACTURER);
            device.setModel(Build.MODEL);
            device.setVersion(android.os.Build.VERSION.RELEASE);
            device.setMsisdn("");
            device.setImei(phone.getDeviceId());
            device.setImsi(phone.getSubscriberId());
            device.setToken("");
            device.setOperator(phone.getNetworkOperatorName());
        } catch (Exception e) {
            Log.e(TAG, "getDevice: " + e.getMessage());
        }
        return device;
    }
}