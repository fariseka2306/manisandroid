package com.ebizu.manis.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class AvatarEditor extends android.support.v7.widget.AppCompatImageView implements OnTouchListener {

    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private static final int ROTATE = 3;
    private Matrix matrix;
    private Matrix savedMatrix;
    private int mode = NONE;
    // these PointF objects are used to record the point(s) the user is touching
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist;
    private float width, height;
    private int minSize = 20, maxSize = 1000; // in %
    private float zoom, left, top, minLeft, maxLeft, minTop, maxTop;

    public AvatarEditor(Context context) {
        super(context);
        init();
    }

    public AvatarEditor(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AvatarEditor(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public static String getBase64Bitmap(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] array = baos.toByteArray();
        return Base64.encodeToString(array, Base64.DEFAULT);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        getLayoutParams().height = w;
    }

    private void init() {
        setOnTouchListener(this);
        setScaleType(ScaleType.MATRIX);
    }

    public boolean isCropable() {
        float[] matrixValue = new float[9];
        getImageMatrix().getValues(matrixValue);
        zoom = matrixValue[0];
        return !(width * zoom < getWidth() || height * zoom < getHeight());
    }

    public boolean checking(int sH, int sW) {

        return false;
    }

    public boolean isInFullSpace() {
        boolean isInSpace = true;
        float[] values = new float[9];
        getImageMatrix().getValues(values);
        float zoom = values[0];
        float globalX = values[Matrix.MTRANS_X];
        float globalY = values[Matrix.MTRANS_Y];
        float zoomWidth = width * zoom;
        float zoomHeight = height * zoom;
        if (width > 640 || height > 640) {
            if (height > width) {
                if (globalX > 10 || globalY > 10 || globalX < -10 || globalY < -140)
                    isInSpace = false;
            } else {
                if (globalX > 10 || globalY > 10 || globalX < -140 || globalY < -10)
                    isInSpace = false;
            }

        } else {
            if (globalX > 10 || globalY > 10 || globalX < -10 || globalY < -10)
                isInSpace = false;
        }
        return isInSpace;
    }

    public String getBase64Bitmap() {
        Bitmap bitmap = getBitmap();
        if (bitmap != null)
            return getBase64Bitmap(bitmap);
        else
            return null;
    }

    public Bitmap getBitmap() {
        float[] matrixValue = new float[9];
        getImageMatrix().getValues(matrixValue);
        zoom = matrixValue[0];
        left = matrixValue[2];
        top = matrixValue[5];
        Bitmap source = Bitmap.createScaledBitmap(((BitmapDrawable) getDrawable()).getBitmap(), (int) (width * zoom), (int) (height * zoom), true);
        return Bitmap.createBitmap(source, (int) left * -1, (int) top * -1, getWidth(), getHeight());
    }

    public File getFile() {
        final String dir = Environment.getExternalStorageDirectory() + "/Manis";
        File newdir = new File(dir);
        if (!newdir.exists()) {
            newdir.mkdirs();
        }
        String imageFileName = newdir + "/ProfilePicture.jpg";
        File imageFile = new File(imageFileName);

        try {
            FileOutputStream fos = new FileOutputStream(imageFile);
            getBitmap().compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (IOException e) {
            Log.d("AvatarEditor", "Error accessing file: " + e.getMessage());
        }
        return imageFile;
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        if (drawable != null) {
            try {
                BitmapDrawable d = (BitmapDrawable) drawable;
                width = d.getBitmap().getWidth();
                height = d.getBitmap().getHeight();
                float w = getWidth();
                float h = getHeight();
                float scale = Math.max(getWidth() / width, getHeight() / height);
                matrix = new Matrix();
                savedMatrix = new Matrix();
                savedMatrix.set(matrix);
                matrix.set(savedMatrix);
                matrix.postTranslate((w - width) / 2f, (h - height) / 2f);
                matrix.postScale(scale, scale, w / 2, h / 2);
                setImageMatrix(matrix);
            } catch (Exception e) {
                //setImageDrawable(drawable);
            }
        }
    }

    public void resetMatrix() {
        Matrix reset = new Matrix();
//        savedMatrix = reset;
        matrix = reset;
        setImageMatrix(reset);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        setScaleType(ScaleType.MATRIX);
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:

                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                mode = DRAG;
                break;

            case MotionEvent.ACTION_POINTER_DOWN:

                oldDist = spacing(event);
                if (oldDist > 10f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
                }
                break;

            case MotionEvent.ACTION_MOVE:

                if (mode == DRAG) {

                    matrix.set(savedMatrix);
                    matrix.postTranslate(event.getX() - start.x,
                            event.getY() - start.y);
                    setImageMatrix(matrix);
                } else if (mode == ZOOM) {

                    float newDist = spacing(event);
                    if (newDist > 10f) {
                        matrix.set(savedMatrix);
                        float scale = newDist / oldDist;
                        float[] check = new float[9];
                        Matrix temp = new Matrix();
                        temp.set(matrix);
                        temp.postScale(scale, scale, mid.x, mid.y);
                        temp.getValues(check);
                        float mapZoom = check[Matrix.MSCALE_X];
                        matrix.postScale(scale, scale, mid.x, mid.y);
                        setImageMatrix(matrix);
//
//                        matrix.postScale(scale, scale, mid.x, mid.y);
//                        setImageMatrix(matrix);
                    }
                }
                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:

                mode = NONE;
                break;
        }

        // Perform the transformation
        Log.d("point",
                (event.getX() - start.x) + " "
                        + (event.getY() - start.y));
        // Log.d("point",start.x +" "+start.y);
        float[] values = new float[9];
        matrix.getValues(values);
        float a = values[Matrix.MTRANS_X];
        float b = values[Matrix.MTRANS_Y];
        Log.d("size image", width + " " + height);
        Log.d("touch matrix", values[Matrix.MPERSP_0] + " "
                + values[Matrix.MPERSP_1] + " "
                + values[Matrix.MPERSP_2]);
        Log.d("touch matrix scale", values[Matrix.MSCALE_X] + " "
                + values[Matrix.MSCALE_Y]);
        Log.d("touch matrix scew", values[Matrix.MSKEW_X] + " "
                + values[Matrix.MSKEW_Y]);
        Log.d("touch matrix trans", values[Matrix.MTRANS_X] + " "
                + values[Matrix.MTRANS_Y]);
        // images.set(position, view);
        return true; // indicate event was handled
    }

    private float angle(MotionEvent event) {
        return 0;
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }
}
