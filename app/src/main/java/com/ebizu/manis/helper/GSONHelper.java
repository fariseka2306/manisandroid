package com.ebizu.manis.helper;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by ebizu on 12/11/17.
 */

public class GSONHelper {
    private static GSONHelper gsonHelper;
    private static Gson gson;

    private GSONHelper() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(String.class, new GsonStringDeserializer());
        gsonBuilder.registerTypeAdapter(String.class, new GsonStringSerializer());
        gson = gsonBuilder.create();
    }

    public static GSONHelper newInstance() {
        if (null == gsonHelper || null == gson) {
            gsonHelper = new GSONHelper();
        }
        return gsonHelper;
    }

    public Gson getGson() {
        return gson;
    }

    public class GsonStringDeserializer implements JsonDeserializer<String> {

        private static final String TAG = "StringGson";

        @Override
        public String deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
            if (element.isJsonObject() || element.isJsonArray()) {
                String encodedUrl = null;
                try {
                    encodedUrl = URLDecoder.decode(element.toString(), "UTF-8");
                    Log.d(TAG, "encodedUrl: ".concat(encodedUrl));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.e(TAG, "deserialize: ".concat(e.getMessage()));
                }
                return encodedUrl;
            } else {
                return getNullAsEmptyString(element);
            }
        }

        private String getNullAsEmptyString(JsonElement jsonElement) {
            return jsonElement.isJsonNull() ? null : jsonElement.getAsString().trim();
        }
    }

    public class GsonStringSerializer implements JsonSerializer<String> {

        private static final String TAG = "StringGson";

        public JsonElement serialize(String src, Type typeOfSrc, JsonSerializationContext context) {
            if (null != src) {
                String encodedUrl = "";
                try {
                    encodedUrl = URLEncoder.encode(src.trim(), "UTF-8");
                    Log.d(TAG, "encodedUrl: ".concat(src.trim()));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.e(TAG, "serialize: ".concat(e.getMessage()));
                }
                return new JsonPrimitive(encodedUrl);
            } else {
                return new JsonPrimitive("");
            }
        }

    }
}
