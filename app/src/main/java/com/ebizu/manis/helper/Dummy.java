package com.ebizu.manis.helper;

import com.ebizu.manis.service.manis.requestbody.BeaconPromoBody;

/**
 * Created by ebizu on 8/28/17.
 */

public class Dummy {

    public static class BeaconPromo {
        public static String MAJOR = "43275";
        public static String MINOR = "17874";
        public static String BEACON_UUID = "1693E1B7-BEED-11E5-A8A2-062BDA9E2E4B";
        public static String SERIAL = "1693e1b7-beed-11e5-a8a2-062bda9e2e4b";
    }

    public static class BeaconPromo2 {
        public static String MAJOR = "19866";
        public static String MINOR = "63665";
        public static String BEACON_UUID = "23A01AF0-232A-4518-9C0E-323FB773F5EF";
        public static String SERIAL = "23a01af0-232a-4518-9c0e-323fb773f5ef";
    }

    public static class BeaconPromo3 {
        public static String MAJOR = "43275";
        public static String MINOR = "17874";
        public static String BEACON_UUID = "1693E1B7-BEED-11E5-A8A2-062BDA9E2E4B";
        public static String SERIAL = "693e1b7-beed-11e5-a8a2-062bda9e2e4b";
    }

    public static BeaconPromoBody getBeaconPromoBody() {
        BeaconPromoBody beaconPromoBody = new BeaconPromoBody();
        beaconPromoBody.setMajor(BeaconPromo.MAJOR);
        beaconPromoBody.setMinor(BeaconPromo.MINOR);
        beaconPromoBody.setUuid(BeaconPromo.BEACON_UUID);
        beaconPromoBody.setSn(BeaconPromo.SERIAL);
        return beaconPromoBody;
    }

    public static class Number {
        public static String malaysiaPhoneNumber = "607138294123";
    }
}
