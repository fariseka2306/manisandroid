package com.ebizu.manis.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * Created by firef on 7/18/2017.
 */

public class ConnectionDetector {

    private static final String TAG = "ConnectionDetector";

    private ConnectionDetector() {

    }

    public static boolean isNetworkConnected(Context context) {
        try {
            ConnectivityManager cm = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        } catch (Exception e) {
            Log.e(TAG, "isNetworkConnected: ".concat(e.getMessage()));
        }
        return false;
    }

    public static String getMobileIP(SharedPreferences sharedPreferences) {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ipAddress = inetAddress.getHostAddress().toUpperCase().toString();
                        setTemporaryIpAddress(sharedPreferences.edit(), ipAddress);
                        return ipAddress;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e(TAG, "Exception in Get IP Address: " + ex.toString());
        }
        return getTemporaryIpAddress(sharedPreferences);
    }

    private static void setTemporaryIpAddress(SharedPreferences.Editor editor, String ipAddress) {
        editor.putString(ConfigManager.Tracker.PREF_IP_ADDRESS, ipAddress);
        editor.commit();
    }

    private static String getTemporaryIpAddress(SharedPreferences sharedPreferences) {
        return sharedPreferences.getString(ConfigManager.Tracker.PREF_IP_ADDRESS, "");
    }
}
