package com.ebizu.manis.helper;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import java.util.StringTokenizer;

/**
 * Created by andrifashbir on 14/12/17.
 */

public class PeriodsThousandSeparator implements TextWatcher {

    private EditText editText;
    private int pos;
    private String last;

    public PeriodsThousandSeparator(EditText editText) {
        this.editText = editText;
        this.last = editText.getText().toString();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        pos = editText.getSelectionStart();
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        // not implemented method
    }

    @Override
    public void afterTextChanged(Editable editable) {
        try {
            editText.removeTextChangedListener(this);
            String value = editText.getText().toString();
            if (value.replaceAll("\\.", "").length() < 10) {
                if (!value.equals("")) {
                    if (value.startsWith("0")) {
                        value = value.substring(1);
                    }

                    String str = value.replaceAll("\\.", "");
                    if (!value.equals("")) {
                        editText.setText(getDecimalFormattedString(str));
                        String result = editText.getText().toString();
                        int newPos;
                        if (result.length() > last.length()) {
                            int diff = result.length() - last.length();
                            newPos = pos + diff;
                            newPos = newPos > result.length() ? result.length() : newPos;
                        } else {
                            if (pos == last.length()) {
                                newPos = result.length();
                            } else if (last.length() == result.length() && pos > 0) {
                                newPos = (result.charAt(pos - 1) == '.') ? pos - 1 : pos + 1;
                            } else {
                                newPos = pos - 1;
                            }
                        }
                        pos = newPos <= 0 ? 1 : newPos;
                        last = result;
                        editText.setSelection(pos);
                    }
                } else {
                    last = value;
                }
            } else {
                editText.setText(last);
                editText.setSelection(last.length());
            }
            editText.addTextChangedListener(this);
            return;
        } catch (Exception ex) {
            Log.e(PeriodsThousandSeparator.class.getSimpleName(), ex.getMessage());
            editText.addTextChangedListener(this);
        }
    }

    private static String getDecimalFormattedString(String value) {
        StringTokenizer lst = new StringTokenizer(value, ".");
        String str1 = value;
        String str2 = "";
        if (lst.countTokens() > 1) {
            str1 = lst.nextToken();
            str2 = lst.nextToken();
        }
        String str3 = "";
        int i = 0;
        int j = -1 + str1.length();
        for (int k = j;; k--) {
            if (k < 0) {
                if (str2.length() > 0)
                    str3 = str3 + "." + str2;
                return str3;
            }
            if (i == 3) {
                str3 = "." + str3;
                i = 0;
            }
            str3 = str1.charAt(k) + str3;
            i++;
        }
    }

    public static String trimPeriodsOfString(String string) {
        if(string.contains(".")){
            return string.replace(".","");}
        else {
            return string;
        }
    }
}
