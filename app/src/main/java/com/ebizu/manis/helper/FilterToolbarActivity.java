package com.ebizu.manis.helper;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ebizu.manis.R;
import com.ebizu.manis.mvp.main.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * base class for activity using toolbar
 * Created by egiadtya on 13/09/16.
 */
public abstract class FilterToolbarActivity extends MainActivity {
    Toolbar toolbar;
    TextView tvTitle;
    ImageView btnRightAction;

    protected abstract int getLayout();

    protected abstract String pageTitle();

    private int activityView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(activityView);
        setMainActivityView(); // optional
        setSupportActionBar(toolbar);
        if (tvTitle != null && pageTitle() != null) {
            if (pageTitle() != null) {
                tvTitle.setText(pageTitle());
            }
        }
    }

    protected void setMainActivityView() {
        //override this method on sub class
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) findViewById(R.id.tv_page_title);
        btnRightAction = (ImageView) findViewById(R.id.iv_right_action);
    }

    protected void setLayout(int layout) {
        if (layout == 0)
            setContentView(getLayout());
        else {
            setContentView(layout);
        }
    }

    /**
     * for set listener method of navigation
     *
     * @param listener OnClickListener
     */
    public void setNavigationListener(View.OnClickListener listener) {
        if (toolbar != null) {
            btnRightAction.setImageResource(R.drawable.close_icon);
            btnRightAction.setOnClickListener(listener);
        }
    }

    /**
     * for set back action listener
     */
    public void setBackNavigationListener() {
        setNavigationListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setActivityView(int activityView) {
        this.activityView = activityView;
    }

    /**
     * For calculate the indexes for name
     *
     * @param items data reward
     * @return data indexed mapped
     */
    protected HashMap<String, Integer> calculateIndexesForName(ArrayList<String> items) {
        HashMap<String, Integer> mapIndex = new LinkedHashMap<>();
        int position = 0;
        for (int i = 0; i < 26; i++) {
            String nameIndex = Character.toString((char) (65 + i));
            for (int j = position; j < items.size(); j++) {
                String name = items.get(j);
                String index = name.substring(0, 1);
                if (index.equalsIgnoreCase(nameIndex)) {
                    position = j;
                    break;
                }
            }
            if (!mapIndex.containsKey(nameIndex)) mapIndex.put(nameIndex, position);
        }
        return mapIndex;
    }
}