package com.ebizu.manis.helper;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckedTextView;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raden on 8/2/17.
 */

public class FilterHolder extends RecyclerView.ViewHolder {

//    @BindView(R.id.ctv_content)
//    CheckedTextView cekTxtContent;

    public FilterHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(itemView);
//        cekTxtContent = (CheckedTextView) itemView.findViewById(R.id.ctv_content);
    }
}
