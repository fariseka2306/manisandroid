package com.ebizu.manis.helper;

import android.content.SharedPreferences;

public class UtilSessionFirstIn {

    private final static String SHOW_GUIDE_SNAP = "SHOW_GUIDE_SNAP";
    private final static String SHOW_INVITE_TERMS = "SHOW_INVITE_TERMS";
    private final static String SHOW_INTEREST = "SHOW_INTEREST";
    private final static String WIPE_DATA = "WIPE_DATA";
    private final static String SHOW_COACHMARK = "SHOW_COACHMARK";
    private final static String LOGIN_WITHPHONE = "LOGIN_WITHPHONE";
    private final static String SHOW_SNAP_TUTORIAL = "SHOW_SNAP_TUTORIAL";
    private final static String SHOW_WALKTROUGH = "SHOW_WALKTROUGH";
    private final static String SHOW_WALKTROUGH_NOTIF = "SHOW_WALKTROUGH_NOTIF";
    private final static String VALUE_AUTO_EDGE_DETECT = "VALUE_AUTO_EDGE_DETECT";
    private static final String LUCKY_DRAW_INTRO = "LUCKY_DRAW_INTRO";
    private static final String LUCKY_DRAW_GUIDE = "LUCKY_DRAW_GUIDE";

    public static void setShowGuideSnap(SharedPreferences preferences, boolean show) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SHOW_GUIDE_SNAP, show);
        editor.apply();
    }

    public static boolean isShowGuidSnap(SharedPreferences preferences) {
        return preferences.getBoolean(SHOW_GUIDE_SNAP, true);
    }

    public static void setShowInviteTerms(SharedPreferences preferences, boolean show) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SHOW_INVITE_TERMS, show);
        editor.apply();
    }

    public static boolean isShowInviteTerms(SharedPreferences preferences) {
        return preferences.getBoolean(SHOW_INVITE_TERMS, true);
    }

    public static void setShowInterest(SharedPreferences preferences, boolean show) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SHOW_INTEREST, show);
        editor.apply();
    }

    public static boolean isShowInterest(SharedPreferences preferences) {
        return preferences.getBoolean(SHOW_INTEREST, true);
    }

    public static void setWipeData(SharedPreferences preferences, boolean show) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(WIPE_DATA, show);
        editor.apply();
    }

    public static boolean isWipeData(SharedPreferences preferences) {
        return preferences.getBoolean(WIPE_DATA, false);
    }

    public static void setShowCoachMark(SharedPreferences preferences, boolean show) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SHOW_COACHMARK, show);
        editor.apply();
    }

    public static boolean isShowCoachMark(SharedPreferences preferences) {
        return preferences.getBoolean(SHOW_COACHMARK, true);
    }

    public static void setLoginWithphone(SharedPreferences preferences, boolean phone) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(LOGIN_WITHPHONE, phone);
        editor.apply();
    }

    public static boolean isLoginWithPhone(SharedPreferences preferences) {
        return preferences.getBoolean(LOGIN_WITHPHONE, false);
    }

    public static void setShowSnapTutorial(SharedPreferences preferences, boolean phone) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SHOW_SNAP_TUTORIAL, phone);
        editor.apply();
    }

    public static boolean isShowSnapTutorial(SharedPreferences preferences) {
        return preferences.getBoolean(SHOW_SNAP_TUTORIAL, true);
    }

    public static void clearAllFirstIn(SharedPreferences preferences) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void setShowWalktrough(SharedPreferences preferences, boolean show) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SHOW_WALKTROUGH, show);
        editor.apply();
    }

    public static boolean isShowWalktrough(SharedPreferences preferences) {
        return preferences.getBoolean(SHOW_WALKTROUGH, true);
    }

    public static void setShowWalktroughNotif(SharedPreferences preferences, boolean show) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SHOW_WALKTROUGH_NOTIF, show);
        editor.apply();
    }

    public static boolean isShowWalktroughNotif(SharedPreferences preferences) {
        return preferences.getBoolean(SHOW_WALKTROUGH_NOTIF, false);
    }

    public static void setValueAutoEdgeSwitch(SharedPreferences preferences, boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(VALUE_AUTO_EDGE_DETECT, value);
        editor.apply();
    }

    public static boolean isAutoEdgeSwitch(SharedPreferences preferences) {
        return preferences.getBoolean(VALUE_AUTO_EDGE_DETECT, false);
    }

    public static void setShowLuckyDrawGuide(SharedPreferences preferences, boolean show) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(LUCKY_DRAW_GUIDE, show);
        editor.apply();
    }

    public static boolean isShowLuckyDrawGuide(SharedPreferences preferences) {
        return preferences.getBoolean(LUCKY_DRAW_GUIDE, true);
    }

    public static void setShowLuckyDrawIntro(SharedPreferences preferences, boolean show) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(LUCKY_DRAW_INTRO, show);
        editor.apply();
    }

    public static boolean isShowLuckyDrawIntro(SharedPreferences preferences) {
        return preferences.getBoolean(LUCKY_DRAW_INTRO, false);
    }


}