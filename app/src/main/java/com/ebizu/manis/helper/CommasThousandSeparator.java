package com.ebizu.manis.helper;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by andrifashbir on 14/12/17.
 */

public class CommasThousandSeparator implements TextWatcher {

    private EditText editText;
    private int pos;
    private String last;

    public CommasThousandSeparator(EditText editText) {
        this.editText = editText;
        this.editText.setOnKeyListener((v, keyCode, event) -> false);
        this.last = editText.getText().toString();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        pos = editText.getSelectionStart();
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        // not implemented
    }

    @Override
    public void afterTextChanged(Editable editable) {
        try {
            editText.removeTextChangedListener(this);
            String originalString = editable.toString();
            String pureDigit = originalString.replaceAll("[.,]", "");
            if ((pureDigit.length() < 10 || (hasCent(originalString) && pureDigit.length() < 12)) &&
                    !pureDigit.equals(last.replaceAll("[.,]", ""))) {
                setTextFormatSeparator(pureDigit, originalString);
            } else {
                editText.setText(last);
                editText.setSelection(pureDigit.equals(last.replaceAll("[.,]", "")) ? pos - 1 : pos);
            }
            editText.addTextChangedListener(this);
        } catch (Exception nfe) {
            Log.e(CommasThousandSeparator.class.getSimpleName(), nfe.getMessage());
            editText.addTextChangedListener(this);
        }
    }

    private boolean hasCent(String value) {
        return value.contains(".");
    }

    public static String trimCommasOfString(String string) {
        if (string.contains(",")) return string.replace(",", "");
        else return string;
    }

    private void setTextFormatSeparator(String pureDigit, String originalString) {
        double factor = 100;
        double doubleVal = Double.parseDouble(pureDigit) / factor;

        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###,###.##");
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(last.length() > originalString.length() ? 1 : 2);
        String formattedString = doubleVal == 0 ? "" : formatter.format(doubleVal);
        editText.setText(formattedString);

        updatePosValue(formattedString);
        last = formattedString;
        editText.setSelection(pos);
    }

    private void updatePosValue(String formattedString) {
        int newPos;
        if (formattedString.length() > last.length()) {
            int diff = formattedString.length() - last.length();
            newPos = pos + diff;
            if (((pos == 0) && (last.isEmpty())) || (newPos > formattedString.length())) {
                newPos = formattedString.length();
            }
            if (newPos < formattedString.length()) {
                if (formattedString.charAt(newPos - 1) == ',') {
                    newPos -= 1;
                }
            }
        } else {
            if (last.length() == formattedString.length() && pos > 0) {
                newPos = (formattedString.charAt(pos - 1) == ',') ? pos - 1 : pos + 1;
            } else {
                newPos = pos - 1;
            }
        }
        if (newPos <= 0) {
            pos = (formattedString.length() > 1) ? 1 : 0;
        } else if (newPos > formattedString.length() - 1) {
            pos = formattedString.length();
        } else {
            pos = newPos;
        }
    }
}