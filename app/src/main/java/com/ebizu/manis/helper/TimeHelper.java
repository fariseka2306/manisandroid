package com.ebizu.manis.helper;

import java.util.Calendar;

/**
 * Created by ebizu on 8/28/17.
 */

public class TimeHelper {

    public static long getTimeStamp(){
        java.sql.Date currentTimestamp = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        return currentTimestamp.getTime();
    }
}
