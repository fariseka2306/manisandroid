package com.ebizu.manis.helper;

/**
 * Created by abizu-alvio on 8/11/2017.
 */

public enum LoginChoices {
    GOOGLE,
    FACEBOOK,
    PHONE
}
