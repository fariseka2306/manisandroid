package com.ebizu.manis.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

//import com.ebizu.sdk.entities.CampaignObject;
//import com.ebizu.sdk.entities.CustomEvent;
import com.ebizu.manis.R;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileView;
import com.ebizu.manis.manager.intercom.IntercomManager;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.mvp.main.MainActivity;
import com.ebizu.sdk.Ebizu;
import com.ebizu.sdk.entities.CampaignObject;
import com.ebizu.sdk.entities.CustomEvent;
import com.google.android.gms.maps.model.LatLng;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.hbb20.CountryCodePicker;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.zxing.BarcodeFormat;
//import com.google.zxing.EncodeHintType;
//import com.google.zxing.MultiFormatWriter;
//import com.google.zxing.WriterException;
//import com.google.zxing.common.BitMatrix;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import io.branch.referral.Branch;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.identity.Registration;
import io.intercom.android.sdk.push.IntercomPushClient;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;


/**
 * Created by Rizky Riadhy on 03-May-16.
 */
public class UtilManis {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final int WEEK_MILLIS = 7 * DAY_MILLIS;
    private static final NavigableMap<Long, String> suffixes = new TreeMap<>();
    private static final String OFFER = "offer";
    private static final String OFFER_DETAIL = "offer_detail";
    private static final String STORE = "store";
    private static final String STORE_DETAIL = "store_detail";
    private static final String CASHVOUCHER = "cashvoucher";
    private static final String CASHVOUCHER_DETAIL = "cashvoucher_detail";
    private static final String RM_CURRENCY = "RM";
    public static String exManis = "com.android.vending";

    static {
        suffixes.put(1_000L, "K");
//        suffixes.put(1_000_000L, "M");
//        suffixes.put(1_000_000_000L, "G");
//        suffixes.put(1_000_000_000_000L, "T");
//        suffixes.put(1_000_000_000_000_000L, "P");
//        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    public static void info(Context context, String title, String message) {
        info(context, title, message, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
    }

    public static void info(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        if (context != null && context instanceof AppCompatActivity) {
            new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setIcon(R.drawable.ic_launcher)
                    .setPositiveButton(context.getString(R.string.text_ok), listener)
                    .setNegativeButton(null, null)
                    .create()
                    .show();
        }
    }

    public static void infoFix(Context context, String title, String message, DialogInterface.OnClickListener listener) {
        if (context != null && context instanceof AppCompatActivity) {
            new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setIcon(R.drawable.ic_launcher)
                    .setPositiveButton(context.getString(R.string.um_ok), listener)
                    .setNegativeButton(null, null)
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    public static void selection(Context context, String title, String message, String positive, String negative, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener) {
        if (context != null && context instanceof AppCompatActivity && !((AppCompatActivity) context).isFinishing()) {
            new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setIcon(R.drawable.ic_launcher)
                    .setPositiveButton(positive, positiveListener)
                    .setNegativeButton(negative, negativeListener)
                    .create()
                    .show();
        }
    }

    public static String getTimeAgo(long time, Context ctx) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        try {
            if (diff < MINUTE_MILLIS) {
                return ctx.getString(R.string.hm_txt_just_now);
            } else if (diff < 2 * MINUTE_MILLIS) {
                return ctx.getString(R.string.hm_txt_a_min_ago);
            } else if (diff < 50 * MINUTE_MILLIS) {
                return diff / MINUTE_MILLIS + ctx.getString(R.string.hm_txt_min_ago);
            } else if (diff < 90 * MINUTE_MILLIS) {
                return ctx.getString(R.string.hm_txt_an_hour_ago);
            } else if (diff < 24 * HOUR_MILLIS) {
                return diff / HOUR_MILLIS + ctx.getString(R.string.hm_txt_hour_ago);
            } else if (diff < 48 * HOUR_MILLIS) {
                return ctx.getString(R.string.hm_txt_1_day_ago);
            } else {
                return diff / DAY_MILLIS + ctx.getString(R.string.hm_txt_days_ago);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return diff / DAY_MILLIS + ctx.getString(R.string.hm_txt_days_ago);
        }
    }

    public static String getTimeAgoMiddle(long time, Context ctx) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return ctx.getString(R.string.sh_txt_just_now);
        } else if (diff < 2 * MINUTE_MILLIS) {
            return ctx.getString(R.string.sh_txt_a_min_ago);
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " " + ctx.getString(R.string.sh_txt_min_ago);
        } else if (diff < 90 * MINUTE_MILLIS) {
            return ctx.getString(R.string.sh_txt_an_hour_ago);
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " " + ctx.getString(R.string.sh_txt_hour_ago);
        } else if (diff < 48 * HOUR_MILLIS) {
            return ctx.getString(R.string.sh_txt_1_day_ago);
        } else if (diff < 7 * DAY_MILLIS) {
            return diff / DAY_MILLIS + " " + ctx.getString(R.string.sh_txt_days_ago);
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
            Date date = new Date(time);
            return dateFormat.format(date);

        }
    }

    public static String longToLocaleNumberFormat(long value) {
        NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
        return nf.format(value);
    }

    public static String doubleToLocaleNumberFormat(double value) {
        NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
        return nf.format(value);
    }

    public static boolean isInSameDay(long timeA, long timeB) {
        Calendar a = Calendar.getInstance();
        a.setTimeInMillis(timeA);
        a.set(Calendar.HOUR_OF_DAY, 0);
        a.set(Calendar.MINUTE, 0);
        a.set(Calendar.SECOND, 0);
        a.set(Calendar.MILLISECOND, 0);

        Calendar b = Calendar.getInstance();
        b.setTimeInMillis(timeB);
        b.set(Calendar.HOUR_OF_DAY, 0);
        b.set(Calendar.MINUTE, 0);
        b.set(Calendar.SECOND, 0);
        b.set(Calendar.MILLISECOND, 0);

        return (a.getTimeInMillis() == b.getTimeInMillis());
    }

    public static boolean isInInterval(long timeNow, long timeBeacon, long interval) {
        Calendar a = Calendar.getInstance();
        a.setTimeInMillis(timeNow);

        long timeInterval = timeBeacon + interval;
        Calendar b = Calendar.getInstance();
        b.setTimeInMillis(timeInterval);
        return (a.before(b));
    }

    public static String convertPhoneNumber(Account account) {
        String phoneNumber = "";

        if (account.getAccMsisdn().startsWith("+620")|account.getAccMsisdn().startsWith("+600")) {
            phoneNumber = account.getAccMsisdn().substring(4);

        } else if (account.getAccMsisdn().startsWith("+62")|account.getAccMsisdn().startsWith("+60")){
            phoneNumber = account.getAccMsisdn().substring(3);

        } else if (account.getAccMsisdn().startsWith("62")|account.getAccMsisdn().startsWith("60")){
            phoneNumber = account.getAccMsisdn().substring(2);

        } else if (account.getAccMsisdn().startsWith("0")|account.getAccMsisdn().startsWith("+")){
            phoneNumber = account.getAccMsisdn().substring(1);

        } else {
            phoneNumber = account.getAccMsisdn();

        }
        return phoneNumber;
    }

    public static Drawable getPhoneCountryImage(Context context, Account account) {
        Drawable drawable = null;
        if (account.getAccCountry().equals("ID")) {
            drawable = ContextCompat.getDrawable(context,R.drawable.flag_indonesia);
        }else {
            drawable = ContextCompat.getDrawable(context,R.drawable.flag_malaysia);
        }

        return drawable;
    }


  /*  public static Drawable getChangeFlag(ProfileView profileView, Account account){
        String codeNumber ="";
        if (account.getAccMsisdn().startsWith("+62")) {
            profileView.onCountrySelected();
        }

    }*/
//    public static String createBarCode(String code, ImageView img) {
//        try {
//            Map<EncodeHintType, Object> hints;
//            String encoding = "UTF-8";
//            hints = new EnumMap<>(EncodeHintType.class);
//            hints.put(EncodeHintType.CHARACTER_SET, encoding);
//            MultiFormatWriter writer = new MultiFormatWriter();
//            BitMatrix result;
//            result = writer.encode(code, BarcodeFormat.QR_CODE, 320, 320, hints);
//            int width = result.getWidth() - 80;
//            int height = result.getHeight() - 80;
//            int[] pixels = new int[width * height];
//            for (int y = 0; y < height; y++) {
//                int offset = y * width;
//                for (int x = 0; x < width; x++) {
//                    pixels[offset + x] = result.get(x + 40, y + 40) ? BLACK : WHITE;
//                }
//            }
//            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
//            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
//            img.setImageBitmap(bitmap);
//            return code;
//        } catch (WriterException e) {
//        }
//        return null;
//    }

    public static List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            //LatLng p = new LatLng((int) (((double) lat / 1E5) * 1E6), (int) (((double) lng / 1E5) * 1E6));
            LatLng p = new LatLng(lat / 1E5, lng / 1E5);
            poly.add(p);
        }

        return poly;
    }

    public static String formatMoney(String currency, double value) {
        if (currency.equalsIgnoreCase(RM_CURRENCY))
            return formatMoney(value);
        else
            return formatMoney(Double.valueOf(value).longValue());
    }

    public static String formatMoney(double value) {
        if (value < 1000)
            return String.format(Locale.getDefault(), "%.2f", value);
        else
            return formatMoney(Double.valueOf(value).longValue());
    }

    public static String formatMoney(long value) {
        String kiloSuffix = "K";
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return formatMoney(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + formatMoney(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        long number = (long) Math.floor(value / 1000);
        long decimal = (long) Math.round((value % 1000) / 100);

        if (decimal == 0) {
            return String.format(Locale.getDefault(), "%,d", number) + kiloSuffix;
        } else {
            return String.format(Locale.getDefault(), "%,d", number) + "." + decimal + kiloSuffix;
        }
    }

    public static int getIconNotification() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return R.drawable.ic_notif_new;
        } else {
            return R.drawable.ic_launcher;
        }
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static void ebizuTrackEvent(Context context, String event, String type, String id) {
        CustomEvent customEvent = new CustomEvent();
        CampaignObject campaignObject = new CampaignObject();
        campaignObject.setEvent(event);
        campaignObject.add(UtilStatic.MANIS_KEY_TYPE, type);
        campaignObject.add(UtilStatic.MANIS_KEY_ITEM_ID, id);
        customEvent.addEvent(campaignObject);

        customEvent.send();
    }

    public static void ebizuTrackCustomEvent(Context context, String event, HashMap<String, String> eventData) {
        CustomEvent customEvent = new CustomEvent();
        CampaignObject campaignObject = new CampaignObject();
        campaignObject.setEvent(event);

        Set<Map.Entry<String, String>> entrySet = eventData.entrySet();
        Iterator<Map.Entry<String, String>> entrySetIterator = entrySet.iterator();
        while (entrySetIterator.hasNext()) {
            Map.Entry entry = entrySetIterator.next();
            campaignObject.add(entry.getKey().toString(), entry.getValue() == null ? "" : entry.getValue().toString());
        }

        customEvent.addEvent(campaignObject);

        customEvent.send();
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static void closeKeyboard(Activity activity, View view) {
        View focusView = (view == null) ? activity.getCurrentFocus() : view;
        if (focusView != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
        }
    }

    public static String getDateInSpecificFormat(String dateFormat, String date) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.getDefault());
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dayNumberSuffix = getDayNumberSuffix(cal.get(Calendar.DAY_OF_MONTH));
        return new SimpleDateFormat("MMMM d'" + dayNumberSuffix + "', yyyy", Locale.getDefault()).format(cal.getTime());
    }

    private static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static String getStoreName(String merchant) {
        String[] parseMerchant = merchant.trim().split("[@]");
        return parseMerchant.length > 0 ? parseMerchant[0].trim() : "";
    }

    public static String getStoreLocation(String merchant, String address) {
        String[] parseMerchant = merchant.trim().split("[@]");
        return (parseMerchant.length > 1) ? "@ " + parseMerchant[1].trim() : address;
    }

    public static String getPhoneCode(Context context, double lat, double lng) {
        String countryCode = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses.size() != 0) {
                Address obj = addresses.get(0);
                countryCode = obj.getCountryCode();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (countryCode.equals(context.getString(R.string.localization_IDN_tag)))
            return context.getString(R.string.localization_IDN_area_code);
        else if (countryCode.equals(context.getString(R.string.localization_MYS_tag)))
            return context.getString(R.string.localization_MYS_area_code);
        return "";
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static Uri replaceUriHost(Uri uri) {
        Uri newUri;
        switch (uri.getHost()) {
            case CASHVOUCHER:
                newUri = Uri.parse(uri.toString().replace(CASHVOUCHER, CASHVOUCHER_DETAIL));
                return newUri;
            case OFFER:
                newUri = Uri.parse(uri.toString().replace(OFFER, OFFER_DETAIL));
                return newUri;
            case STORE:
                newUri = Uri.parse(uri.toString().replace(STORE, STORE_DETAIL));
                return newUri;
            default:
                return uri;
        }
    }

    public static String replaceAllSymbols(String text){
        return text.replaceAll("[^-\\d]","").replaceAll("(?<!^)-","");
    }

    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics;
        metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    public static String getTimeCountDownLeft(long millisUntilFinished, String dayText) {
        StringBuilder timeCountDown = new StringBuilder();
        String days = String.valueOf(TimeUnit.HOURS.toDays(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)));
        String hours = String.valueOf(
                new DecimalFormat("00").format(
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished)
                                - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished))
                )
        );
        String minutes = String.valueOf(
                new DecimalFormat("00").format(
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished))
                )
        );
        String seconds = String.valueOf(
                new DecimalFormat("00").format(
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))
                )
        );
        if (!days.equalsIgnoreCase("0"))
            timeCountDown.append(days).append(" ").append(dayText).append(" ");
        timeCountDown.append(hours).append(":").append(minutes).append(":").append(seconds);
        return timeCountDown.toString();
    }

    public static boolean isContextMainActivity(Context context) {
        if (context != null && context instanceof MainActivity)
            return true;
        return false;
    }

    public static String createBarCode(String code, ImageView img) {
        try {
            Map<EncodeHintType, Object> hints;
            String encoding = "UTF-8";
            hints = new EnumMap<>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
            MultiFormatWriter writer = new MultiFormatWriter();
            BitMatrix result;
            result = writer.encode(code, BarcodeFormat.QR_CODE, 320, 320, hints);
            int width = result.getWidth() - 80;
            int height = result.getHeight() - 80;
            int[] pixels = new int[width * height];
            for (int y = 0; y < height; y++) {
                int offset = y * width;
                for (int x = 0; x < width; x++) {
                    pixels[offset + x] = result.get(x + 40, y + 40) ? BLACK : WHITE;
                }
            }
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
            img.setImageBitmap(bitmap);
            return code;
        } catch (WriterException e) {
        }
        return null;
    }

    public static void setBranchSDK(Context context, Account account) {
        Branch.getInstance(context).setIdentity(account.getAccId());
        if (account.isFirstLogin()) {
            JSONObject eventData = new JSONObject();
            try {
                TelephonyManager phone = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                eventData.put("username", account.getAccId());
                eventData.put("imei", phone.getDeviceId());
                Branch.getInstance(context).userCompletedAction("sign_up_event", eventData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void setEbizuCampaign(Context context, Account account, LoginChoices loginChoices) {
        CampaignObject campaignObject = new CampaignObject();
        campaignObject.add("name", account.getAccScreenName());
        campaignObject.add("email", account.getEmail());
        campaignObject.add("user_id", account.getAccId());
        if (loginChoices == LoginChoices.FACEBOOK) {
            campaignObject.add("facebook_id", String.valueOf(account.getAccFacebookId()));
        } else if (loginChoices == LoginChoices.GOOGLE) {
            campaignObject.add("google_id", account.getAccGoogleId());
        }
        campaignObject.add("birthdate", String.valueOf(account.getAccBirthdate()));
        campaignObject.add("gender", account.getAccGender());
        campaignObject.setUniqueKey("user_id");
        Ebizu.getInstance(context.getApplicationContext()).setUser(campaignObject);
    }

    public static void setIntercomSdk(Application application, Account account, String regIdGCM, LoginChoices loginChoices){
        Intercom.client().registerIdentifiedUser(new Registration().withUserId(account.getAccId()));
        new IntercomPushClient().sendTokenToIntercom(application, regIdGCM);
        IntercomManager.getIntercom(account, String.valueOf(loginChoices));
    }

    public static String addSeparator(String currency, double value) {
        return String.format(Locale.getDefault(), currency.equalsIgnoreCase("RM") ? "%,.2f" : "%,.0f", value);
    }
}