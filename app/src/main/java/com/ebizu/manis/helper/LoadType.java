package com.ebizu.manis.helper;

/**
 * Created by Raden on 8/8/17.
 */

public enum LoadType {
    FIRST_LOAD,
    SWIPE_LOAD,
    SCROLL_LOAD,
    SEARCH_LOAD

}
