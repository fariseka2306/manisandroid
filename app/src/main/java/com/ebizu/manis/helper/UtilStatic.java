package com.ebizu.manis.helper;

public interface UtilStatic {

    String SIGNIN_PREF = "com.ebizu.manis.signin";
    String KEY_PREF = "com.ebizu.manis.key";
    String FIRSTIN_PREF = "com.ebizu.manis.firstin";
    String NOTIFICATION_PREF = "com.ebizu.manis.notification";
    String GOT_NOTIF = "com.ebizu.manis.GOT_NOTIFICATION";
    String REFRESH_SNAP = "com.ebizu.manis.REFRESH_SNAP";
    String EXPIRED_TOKEN = "com.ebizu.manis.TOKEN_EXPIRED";
    String BLUETOOTH_COUNT = "com.ebizu.manis.BLUETOOTH_COUNT";
    String BLUETOOTH_LAST_SHOW = "com.ebizu.manis.BLUETOOTH_LAST_SHOW";
    String ARG_POSITION = "com.ebizu.manis.ARG_POSITION";

    // ## =======================================================================================
    // ## language
    // ## =======================================================================================

    String EN = "en";
    String IN = "in";

    int API_LIMIT = 20;

    // ## =======================================================================================
    // ## load type
    // ## =======================================================================================

    int LOAD_TYPE_FIRSTTIME = 1;
    int LOAD_TYPE_NEW = 2;
    int LOAD_TYPE_MORE = 3;

    // ## =======================================================================================
    // ## request type
    // ## =======================================================================================

    int REQUEST_SAVED = 400;
    int RESULT_SAVED_STORES = 401;
    int RESULT_SAVED_REWARD = 402;
    int REQUEST_PROPIC = 401;
    int REQUEST_SNAP = 500;
    int REQUEST_BRAND = 501;
    int REQUEST_SNAP_HISTORY = 502;
    int REQUEST_STORES_CATEGORIES = 600;

    // ## =======================================================================================
    // ## ebizu campaign static
    // ## =======================================================================================

    String MANIS_KEY_EVENT = "event";
    String MANIS_EVENT_IMPRESSION = "manisv3_add_impression";
    String MANIS_EVENT_CLICK = "manisv3_add_click";
    String MANIS_EVENT_REDEEMED = "manisv3_add_redeemed";
    String MANIS_EVENT_SNAP_UPLOAD = "manisv3_snap_upload";
    String MANIS_EVENT_SNAP_APPROVED = "manisv3_snap_approved";
    String MANIS_EVENT_STORE_FOLLOW = "manisv3_store_follow";
    String MANIS_EVENT_STORE_UNFOLLOW = "manisv3_store_unfollow";
    String MANIS_EVENT_VOUCHER_SCANNED = "manisv3_voucher_scanned";

    String MANIS_KEY_TYPE = "type";
    String MANIS_TYPE_VOUCHER = "voucher";
    String MANIS_TYPE_OFFER = "offer";
    String MANIS_TYPE_REWARD = "reward";
    String MANIS_TYPE_STORE = "store";

    String MANIS_KEY_ITEM_ID = "item_id";
    String MANIS_KEY_TIMESTAMP = "timestamp";

    String MANIS_KEY_POINT = "point";
    String MANIS_KEY_SNAP_ID = "snap_id";
    String MANIS_KEY_STORE_ID = "store_id";
    String MANIS_KEY_STORE_NAME = "store_name";
    String MANIS_KEY_STORE_CATEGORY = "store_category";
    String MANIS_KEY_STORE_CATEGORY_ID = "store_category_id";
    String MANIS_KEY_AMOUNT = "amount";
    String MANIS_KEY_RECEIPT_DATE = "receipt_date";
    String MANIS_KEY_UPLOAD_DATE = "upload_date";
    String MANIS_KEY_VOUCHER_ID = "voucher_id";
    String MANIS_KEY_VOUCHER_NAME = "voucher_name";
    String MANIS_KEY_VOUCHER_VALID_DATE = "voucher_valid_date";
    String MANIS_KEY_SN = "sn";

    String MANIS_EBIZU_STORE_ID = "160";
    String MANIS_EBIZU_STORE_NAME = "Manis @ Ebizu";
    String MANIS_EBIZU_STORE_CATEGORY = "Service";
    String MANIS_EBIZU_STORE_CATEGORY_ID = "6";

    String MANIS_GA_ACTION_CLICK = "Click";

    // ## =======================================================================================
    // ## intercom static
    // ## =======================================================================================

    String INTERCOM_APIKEY_PRODUCTION = "android_sdk-74dbfc083e3c7115e8831485fae0b1c7c21be5d6";
    String INTERCOM_APIID_PRODUCTION = "j023u6c8";
    String INTERCOM_APIKEY_STAGING = "android_sdk-8fffee0bd081dd61d32c0c51e7df6d5ceca5d2c3";
    String INTERCOM_APIID_STAGING = "g7sn3760";

    // Deeplink
    String OFFER = "offer";
    String OFFER_DETAIL = "offer_detail";
    String STORE = "store";
    String STORE_DETAIL = "store_detail";
    String CASHVOUCHER = "cashvoucher";
    String CASHVOUCHER_DETAIL = "cashvoucher_detail";

    // Reward Type
    String BEACON_TYPE_OFFER = "offer";
    String BEACON_TYPE_REWARD = "reward";

    // Reward New Type
    String REWARD_INPUT_TYPE_NUMBER = "N";
    String REWARD_INPUT_TYPE_PHONE = "P";
    String REWARD_INPUT_TYPE_MULTILINE = "M";
    String REWARD_INPUT_TEXT_MOBILE = "Mobile Number";
    String REWARD_INPUT_TEXT_PHONE = "Phone Number";
    String REWARD_INPUT_TEXT_ADDRESS = "Address";
    String REWARD_INPUT_TEXT_TOTAL_POINTS = "Total Points";
    String REWARD_INPUT_TEXT_POINTS = "Points";

    // Reward Type
    String REWARD_INPUT_TYPE_ETU = "ETU";
    String REWARD_INPUT_TYPE_ECODE = "E-Code";
    String REWARD_INPUT_TYPE_PHYSYCAL = "Physical Voucher";
    String REWARD_INPUT_TYPE_THIRDPARTY = "Third Party Reward";

    // Intent
    String REWARD_CATEGORY_NAME = "category-name";
    String REWARD_CATEGORY_ID = "category-id";
    String REWARD_LOGIN_SESSION = "reward-login-session";
    String REWARD = "reward";
    String REWARD_SHOPPING_CART = "shopping-cart";
    String REWARD_VOUCHER_PIN = "voucher-pin";
    String REWARD_SHOPPING_CART_MODEL_FORM = "shopping-cart-model-form";
    String REWARD_SHIPPING_INFO = "shipping-info";
    String REWARD_TRANSACTION_STATUS = "transation-status";
    String REWARD_PURCHASE_STATUS = "purchase-status";
    String REWARD_MY_VOUCHER = "my-voucher";
    String REWARD_REF_CODE = "ref-code";

    // Reward Module
    String REWARD_MODULE_SESSION = "Session";
    String REWARD_MODULE_VOUCHER = "Voucher";

    //Reward Action
    String REWARD_ACTION_GETSESSION = "getSession";
    String REWARD_ACTION_ORDER_DETAIL = "orderDetail";
    String REWARD_ACTION_REGISTER = "register";
    String REWARD_ACTION_PURCHASE_STATUS = "purchaseStatus";
    String REWARD_ACTION_PIN_VALIDATION = "pinValidation";
    String REWARD_ACTION_CANCEL_PAYMENT = "cancelPayment";
    String REWARD_ACTION_BRAND_TERMS = "brandTerms";
    String REWARD_ACTION_MY_VOUCHER = "myVoucher";
    String REWARD_ACTION_PURCHASE_HISTORY = "purchaseHistory";
}