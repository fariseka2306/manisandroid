package com.ebizu.manis.helper;

/**
 * Created by Raden on 7/4/17.
 */

public class Constants {

    //    MainActivity
    public static final int POS_HOME = 0;
    public static final int POS_DISCOVER = 1;
    public static final int POS_LUCKY_DRAW = 2;
    public static final int POS_REDEEM = 3;
    public static final int POS_PROFILE = 4;
    public static final int MAX_FRAGMENT_TO_DESTROY = 5;

    public static final int RC_SIGN_IN = 0;

}
