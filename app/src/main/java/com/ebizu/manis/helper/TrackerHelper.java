package com.ebizu.manis.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;

import com.ebizu.manis.manager.location.ManisLocationManager;
import com.ebizu.manis.manager.tracker.ComponentManager;
import com.ebizu.manis.manager.tracker.IdentifierManager;
import com.ebizu.manis.manager.tracker.ScreenManager;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.model.tracker.ManisTrackerTable;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.preference.ManisSession;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ebizu on 12/15/17.
 */

public class TrackerHelper {

    private static TrackerHelper instance;
    private static ManisLocationManager manisLocationManager;

    private TrackerStandartRequest trackerStandartRequest;
    private ManisTrackerTable manisTrackerTable;
    private SharedPreferences sharedPreferences;

    public static TrackerHelper getInstance(Context context) {
        if (null == instance)
            instance = new TrackerHelper(context);
        return instance;
    }

    private TrackerHelper(Context context) {
        manisLocationManager = new ManisLocationManager(context);
    }

    public ManisTrackerTable generateManisTrackerTable(TrackerStandartRequest trackerStandartRequest, SharedPreferences sharedPreferences) {
        this.trackerStandartRequest = trackerStandartRequest;
        this.manisTrackerTable = new ManisTrackerTable();
        this.sharedPreferences = sharedPreferences;
        setLocalDateTime();
        setComponent();
        setIpAddress();
        setAccountId();
        setOriginPage();
        setCurrentPage();
        setComponent();
        setIdentifier();
        setSubFeature();
        setLongLat();
        setCountryId();
        return manisTrackerTable;
    }

    private void setLocalDateTime() {
        manisTrackerTable.setLocalDatetime(getLocalDatetime());
    }

    private void setComponent() {
        ComponentManager.getInstance().setComponent(trackerStandartRequest.getComponent());
        manisTrackerTable.setComponent(ComponentManager.getInstance().getComponent());
    }

    private void setIpAddress() {
        String ipAddress = ConnectionDetector.getMobileIP(this.sharedPreferences);
        manisTrackerTable.setIpAddress(ipAddress);
    }

    private void setAccountId() {
        String accountId = sharedPreferences.getString(ConfigManager.AccountSession.ID, "");
        manisTrackerTable.setAccountId(accountId);
    }

    private void setOriginPage() {
        ScreenManager.getInstance().setOriginPage(trackerStandartRequest.getOriginPage());
        manisTrackerTable.setOriginPage(ScreenManager.getInstance().getOriginPage());
    }

    private void setCurrentPage() {
        ScreenManager.getInstance().setCurrentPage(trackerStandartRequest.getCurrentPage());
        manisTrackerTable.setCurrentPage(ScreenManager.getInstance().getCurrentPage());
        manisTrackerTable.setLastPage(ScreenManager.getInstance().getLastPage());
    }

    private void setIdentifier() {
        IdentifierManager.getInstance().setIdentifierName(trackerStandartRequest.getIdentifierName());
        IdentifierManager.getInstance().setIdentifierNumber(trackerStandartRequest.getIdentifierNumber());
        manisTrackerTable.setIdentifierName(IdentifierManager.getInstance().getIdentifierName());
        manisTrackerTable.setIdentifierNumber(IdentifierManager.getInstance().getIdentifierNumber());
    }

    private void setSubFeature() {
        manisTrackerTable.setSubFeature(trackerStandartRequest.getSubFeature());
    }

    private void setLongLat() {
        manisLocationManager.updateLocation();
        double longitude = manisLocationManager.getLocationSession().getLong();
        double latitude = manisLocationManager.getLocationSession().getLat();
        manisTrackerTable.setLongitude(String.valueOf(longitude));
        manisTrackerTable.setLatitude(String.valueOf(latitude));
    }

    private void setCountryId() {
        String countryId = sharedPreferences.getString(ConfigManager.AccountSession.COUNTRY, "");
        manisTrackerTable.setCountryId(countryId);
    }

    private String getLocalDatetime() {
        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("id", "ID"));
        return simpleDateFormat.format(new Date(TimeHelper.getTimeStamp()));
    }
}