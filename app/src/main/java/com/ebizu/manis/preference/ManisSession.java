package com.ebizu.manis.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.Account;
import com.ebizu.manis.model.Currency;
import com.ebizu.manis.model.Device;
import com.ebizu.manis.model.Point;
import com.ebizu.manis.model.snap.SnapTerm;
import com.ebizu.manis.model.snap.SnapTermLuckyDraw;
import com.google.gson.Gson;

/**
 * Created by Ebizu-User on 06/07/2017.
 */

public class ManisSession {

    public final String TAG = getClass().getSimpleName();
    private SharedPreferences preferencesManis;
    private static ManisSession instance;

    public static void initialize(Context context) {
        if (null == instance)
            instance = new ManisSession(context);
    }

    public static ManisSession getInstance(Context context) {
        if (null == instance)
            instance = new ManisSession(context);
        return instance;
    }

    public ManisSession(Context context) {
        preferencesManis = context.getSharedPreferences(
                ConfigManager.Preference.KEY, Context.MODE_PRIVATE);
    }

    public void setSession(String key, String value) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setSession(String key, boolean value) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void setSession(String key, long value) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public void updateSession(Account account) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putString(ConfigManager.AccountSession.TOKEN, account.getToken());
        editor.putString(ConfigManager.AccountSession.ID, account.getAccId());
        editor.putLong(ConfigManager.AccountSession.FACEBOOK_ID, account.getAccFacebookId());
        editor.putString(ConfigManager.AccountSession.DEVICE_ID, account.getAccDeviceId());
        editor.putString(ConfigManager.AccountSession.ADDRESS, account.getAccAddress());
        editor.putString(ConfigManager.AccountSession.ADDRESS_POSTCODE, account.getAciPostcode());
        editor.putString(ConfigManager.AccountSession.ADDRESS_CITY, account.getAciCity());
        editor.putString(ConfigManager.AccountSession.ADDRESS_STATE, account.getAciState());
        editor.putString(ConfigManager.AccountSession.ADDRESS_COUNTRY, account.getAciCountry());
        editor.putString(ConfigManager.AccountSession.COUNTRY, account.getAccCountry());
        editor.putString(ConfigManager.AccountSession.FACEBOOK_EMAIL, account.getAccFacebookEmail());
        editor.putString(ConfigManager.AccountSession.GENDER, account.getAccGender());
        editor.putString(ConfigManager.AccountSession.GOOGLE_EMAIL, account.getAccGoogleEmail());
        editor.putString(ConfigManager.AccountSession.GOOGLE_ID, account.getAccGoogleId());
        editor.putString(ConfigManager.AccountSession.MSISDN, account.getAccMsisdn());
        editor.putString(ConfigManager.AccountSession.PHOTO, account.getAccPhoto());
        editor.putString(ConfigManager.AccountSession.SCREEN_NAME, account.getAccScreenName());
        editor.putString(ConfigManager.AccountSession.TMZ_NAME, account.getTmzName());
        editor.putLong(ConfigManager.AccountSession.BIRTH_DATE, account.getAccBirthdate());
        editor.putString(ConfigManager.AccountSession.CURRENCY_ISO2, account.getCurrency().getIso2());
        editor.putString(ConfigManager.AccountSession.CURRENCY_ISO3, account.getCurrency().getIso3());
        editor.putLong(ConfigManager.AccountSession.LAST_LOGIN, account.getAccLastLogin());
        editor.putInt(ConfigManager.AccountSession.STATUS, account.getAccStatus());
        editor.putLong(ConfigManager.AccountSession.CREATED_DATE_TIME, account.getAccCreatedDateTime());
        editor.apply();
    }

    public void setSession(Account account) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putString(ConfigManager.AccountSession.TOKEN, account.getToken());
        editor.putString(ConfigManager.AccountSession.ID, account.getAccId());
        editor.putLong(ConfigManager.AccountSession.FACEBOOK_ID, account.getAccFacebookId());
        editor.putString(ConfigManager.AccountSession.DEVICE_ID, account.getAccDeviceId());
        editor.putString(ConfigManager.AccountSession.ADDRESS, account.getAccAddress());
        editor.putString(ConfigManager.AccountSession.ADDRESS_POSTCODE, account.getAciPostcode());
        editor.putString(ConfigManager.AccountSession.ADDRESS_CITY, account.getAciCity());
        editor.putString(ConfigManager.AccountSession.ADDRESS_STATE, account.getAciState());
        editor.putString(ConfigManager.AccountSession.ADDRESS_COUNTRY, account.getAciCountry());
        editor.putString(ConfigManager.AccountSession.COUNTRY, account.getAccCountry());
        editor.putString(ConfigManager.AccountSession.FACEBOOK_EMAIL, account.getAccFacebookEmail());
        editor.putString(ConfigManager.AccountSession.GENDER, account.getAccGender());
        editor.putString(ConfigManager.AccountSession.GOOGLE_EMAIL, account.getAccGoogleEmail());
        editor.putString(ConfigManager.AccountSession.GOOGLE_ID, account.getAccGoogleId());
        editor.putString(ConfigManager.AccountSession.MSISDN, account.getAccMsisdn());
        editor.putString(ConfigManager.AccountSession.PHOTO, account.getAccPhoto());
        editor.putString(ConfigManager.AccountSession.SCREEN_NAME, account.getAccScreenName());
        editor.putString(ConfigManager.AccountSession.TMZ_NAME, account.getTmzName());
        editor.putLong(ConfigManager.AccountSession.BIRTH_DATE, account.getAccBirthdate());
        editor.putString(ConfigManager.AccountSession.CURRENCY_ISO2, account.getCurrency().getIso2());
        editor.putString(ConfigManager.AccountSession.CURRENCY_ISO3, account.getCurrency().getIso3());
        editor.putLong(ConfigManager.AccountSession.LAST_LOGIN, account.getAccLastLogin());
        editor.putInt(ConfigManager.AccountSession.STATUS, account.getAccStatus());
        editor.putLong(ConfigManager.AccountSession.OTP_STATUS, account.getAccOtpStatus());
        editor.putBoolean(ConfigManager.AccountSession.FIRST_LOGIN, account.isFirstLogin());
        editor.putLong(ConfigManager.AccountSession.CREATED_DATE_TIME, account.getAccCreatedDateTime());
        editor.putBoolean(ConfigManager.AccountSession.UPDATEABLE_EMAIL, account.isUpdateableEmail());
        editor.putBoolean(ConfigManager.AccountSession.UPDATEABLE_PHONE, account.isUpdateablePhone());
        editor.apply();
    }

    public void setDeviceSession(Device device) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putString(ConfigManager.DeviceSession.IMEI, device.getImei());
        editor.putString(ConfigManager.DeviceSession.IMSI, device.getImsi());
        editor.putString(ConfigManager.DeviceSession.MANUFACTURE, device.getManufacture());
        editor.putString(ConfigManager.DeviceSession.MSISDN, device.getMsisdn());
        editor.putString(ConfigManager.DeviceSession.OPERATOR, device.getOperator());
        editor.putString(ConfigManager.DeviceSession.TOKEN, device.getToken());
        editor.putString(ConfigManager.DeviceSession.VERSION, device.getVersion());
        editor.apply();
    }

    public void setPoint(int point) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putInt(ConfigManager.AccountSession.POINT, point);
        editor.apply();
    }

    public void setPhoto(String photoUrl) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putString(ConfigManager.AccountSession.PHOTO, photoUrl);
        editor.apply();
    }

    public Account getAccountSession() {
        Account account = new Account();
        Currency currency = new Currency();
        currency.setIso2(preferencesManis.getString(ConfigManager.AccountSession.CURRENCY_ISO2, ""));
        currency.setIso3(preferencesManis.getString(ConfigManager.AccountSession.CURRENCY_ISO3, ""));
        account.setToken(preferencesManis.getString(ConfigManager.AccountSession.TOKEN, ""));
        account.setAccId(preferencesManis.getString(ConfigManager.AccountSession.ID, ""));
        account.setAccFacebookId(preferencesManis.getLong(ConfigManager.AccountSession.FACEBOOK_ID, 0));
        account.setAccDeviceId(preferencesManis.getString(ConfigManager.AccountSession.DEVICE_ID, ""));
        account.setAccAddress(preferencesManis.getString(ConfigManager.AccountSession.ADDRESS, ""));
        account.setAciPostcode(preferencesManis.getString(ConfigManager.AccountSession.ADDRESS_POSTCODE, ""));
        account.setAciCity(preferencesManis.getString(ConfigManager.AccountSession.ADDRESS_CITY, ""));
        account.setAciState(preferencesManis.getString(ConfigManager.AccountSession.ADDRESS_STATE, ""));
        account.setAciCountry(preferencesManis.getString(ConfigManager.AccountSession.ADDRESS_COUNTRY, ""));
        account.setAccCountry(preferencesManis.getString(ConfigManager.AccountSession.COUNTRY, ""));
        account.setAccFacebookEmail(preferencesManis.getString(ConfigManager.AccountSession.FACEBOOK_EMAIL, ""));
        account.setAccGender(preferencesManis.getString(ConfigManager.AccountSession.GENDER, ""));
        account.setAccGoogleEmail(preferencesManis.getString(ConfigManager.AccountSession.GOOGLE_EMAIL, ""));
        account.setAccGoogleId(preferencesManis.getString(ConfigManager.AccountSession.GOOGLE_ID, ""));
        account.setAccMsisdn(preferencesManis.getString(ConfigManager.AccountSession.MSISDN, ""));
        account.setAccPhoto(preferencesManis.getString(ConfigManager.AccountSession.PHOTO, ""));
        account.setAccScreenName(preferencesManis.getString(ConfigManager.AccountSession.SCREEN_NAME, ""));
        account.setTmzName(preferencesManis.getString(ConfigManager.AccountSession.TMZ_NAME, ""));
        account.setAccBirthdate((int) preferencesManis.getLong(ConfigManager.AccountSession.BIRTH_DATE, 0));
        account.setAccLastLogin(preferencesManis.getLong(ConfigManager.AccountSession.LAST_LOGIN, 0));
        account.setAccStatus(preferencesManis.getInt(ConfigManager.AccountSession.STATUS, 0));
        account.setAccOtpStatus(preferencesManis.getLong(ConfigManager.AccountSession.OTP_STATUS, 0));
        account.setFirstLogin(preferencesManis.getBoolean(ConfigManager.AccountSession.FIRST_LOGIN, false));
        account.setAccCreatedDateTime(preferencesManis.getLong(ConfigManager.AccountSession.CREATED_DATE_TIME, 0));
        account.setUpdateableEmail(preferencesManis.getBoolean(ConfigManager.AccountSession.UPDATEABLE_EMAIL, false));
        account.setUpdateablePhone(preferencesManis.getBoolean(ConfigManager.AccountSession.UPDATEABLE_PHONE, false));
        account.setCurrency(currency);
        android.util.Log.d(TAG, "getAccountSession: " + account.toString());
        return account;
    }

    public Device getDeviceSession() {
        Device device = new Device();
        device.setImei(preferencesManis.getString(ConfigManager.DeviceSession.IMEI, ""));
        device.setImsi(preferencesManis.getString(ConfigManager.DeviceSession.IMSI, ""));
        device.setManufacture(preferencesManis.getString(ConfigManager.DeviceSession.MANUFACTURE, ""));
        device.setMsisdn(preferencesManis.getString(ConfigManager.DeviceSession.MSISDN, ""));
        device.setOperator(preferencesManis.getString(ConfigManager.DeviceSession.OPERATOR, ""));
        device.setToken(preferencesManis.getString(ConfigManager.DeviceSession.TOKEN, ""));
        device.setVersion(preferencesManis.getString(ConfigManager.DeviceSession.VERSION, ""));
        android.util.Log.d(TAG, "getDeviceSession: " + device.toString());
        return device;
    }

    public Point getPointSession() {
        Point point = new Point();
        point.setPoint(preferencesManis.getInt(ConfigManager.AccountSession.POINT, 0));
        return point;
    }

    public String getSession(String key) {
        return preferencesManis.getString(key, "");
    }

    public String getTokenSession() {
        return preferencesManis.getString(ConfigManager.AccountSession.TOKEN, "");
    }

    public void clearSession() {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.remove(ConfigManager.AccountSession.CURRENCY_ISO2);
        editor.remove(ConfigManager.AccountSession.CURRENCY_ISO3);
        editor.remove(ConfigManager.AccountSession.TOKEN);
        editor.remove(ConfigManager.AccountSession.ID);
        editor.remove(ConfigManager.AccountSession.FACEBOOK_ID);
        editor.remove(ConfigManager.AccountSession.DEVICE_ID);
        editor.remove(ConfigManager.AccountSession.ADDRESS);
        editor.remove(ConfigManager.AccountSession.ADDRESS_POSTCODE);
        editor.remove(ConfigManager.AccountSession.ADDRESS_CITY);
        editor.remove(ConfigManager.AccountSession.ADDRESS_STATE);
        editor.remove(ConfigManager.AccountSession.ADDRESS_COUNTRY);
        editor.remove(ConfigManager.AccountSession.COUNTRY);
        editor.remove(ConfigManager.AccountSession.FACEBOOK_EMAIL);
        editor.remove(ConfigManager.AccountSession.GENDER);
        editor.remove(ConfigManager.AccountSession.GOOGLE_EMAIL);
        editor.remove(ConfigManager.AccountSession.GOOGLE_ID);
        editor.remove(ConfigManager.AccountSession.MSISDN);
        editor.remove(ConfigManager.AccountSession.PHOTO);
        editor.remove(ConfigManager.AccountSession.SCREEN_NAME);
        editor.remove(ConfigManager.AccountSession.TMZ_NAME);
        editor.remove(ConfigManager.AccountSession.BIRTH_DATE);
        editor.remove(ConfigManager.AccountSession.LAST_LOGIN);
        editor.remove(ConfigManager.AccountSession.STATUS);
        editor.remove(ConfigManager.AccountSession.OTP_STATUS);
        editor.remove(ConfigManager.AccountSession.FIRST_LOGIN);
        editor.remove(ConfigManager.AccountSession.CREATED_DATE_TIME);
        editor.remove(ConfigManager.AccountSession.SNAP_RECEIPT_TERM_AND_CONDITION);
        editor.remove(ConfigManager.AccountSession.SNAP_LUCKY_DRAW_TERM_AND_CONDITION);
        editor.remove(ConfigManager.AccountSession.SNAP_EARN_POINTS_TNC);
        editor.remove(ConfigManager.AccountSession.SNAP_EARN_TICKETS_TNC);
        editor.apply();
    }

    public void clearDeviceSession() {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.remove(ConfigManager.DeviceSession.IMEI);
        editor.remove(ConfigManager.DeviceSession.IMSI);
        editor.remove(ConfigManager.DeviceSession.MANUFACTURE);
        editor.remove(ConfigManager.DeviceSession.MSISDN);
        editor.remove(ConfigManager.DeviceSession.OPERATOR);
        editor.remove(ConfigManager.DeviceSession.TOKEN);
        editor.remove(ConfigManager.DeviceSession.VERSION);
        editor.apply();
    }

    public void removeSession(String key) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.remove(key);
        editor.apply();
    }

    public boolean isLoggedIn() {
        return !getSession(ConfigManager.AccountSession.TOKEN).isEmpty();
    }

    public boolean isFirstLogin() {
        return preferencesManis.getBoolean(ConfigManager.AccountSession.FIRST_LOGIN, false);
    }

    public boolean isOtpRegistered() {
        return preferencesManis.getLong(ConfigManager.AccountSession.OTP_STATUS, 1) == 1;
    }

    public boolean isNotAvailableNameUser() {
        String userName = preferencesManis.getString(ConfigManager.AccountSession.SCREEN_NAME, "");
        return userName.isEmpty() || userName.equals("") || userName.equalsIgnoreCase("Customer Name N/A");
    }

    public boolean isFirstIntro() {
        return preferencesManis.getBoolean(ConfigManager.App.INTRODUCTION, true);
    }

    public void completeIntro() {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putBoolean(ConfigManager.App.INTRODUCTION, false);
        editor.apply();
    }

    public void acceptAlwaysTncSnapEarnPoint() {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putBoolean(ConfigManager.AccountSession.SNAP_RECEIPT_TERM_AND_CONDITION, true);
        editor.apply();
    }

    public void acceptAlwaysTncSnapEarnTicket() {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putBoolean(ConfigManager.AccountSession.SNAP_LUCKY_DRAW_TERM_AND_CONDITION, true);
        editor.apply();
    }

    public boolean isAcceptAlwaysTncSnap() {
        return preferencesManis.getBoolean(ConfigManager.AccountSession.SNAP_RECEIPT_TERM_AND_CONDITION, false);
    }

    public boolean isAcceptAlwaysTncLuckyDraw() {
        return preferencesManis.getBoolean(ConfigManager.AccountSession.SNAP_LUCKY_DRAW_TERM_AND_CONDITION, false);
    }

    public void setTermsSnapEarnPoints(SnapTerm snapTerm) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putString(ConfigManager.AccountSession.SNAP_EARN_POINTS_TNC, new Gson().toJson(snapTerm));
        editor.apply();
    }

    public void setTermsSnapEarnTickets(SnapTermLuckyDraw snapTermLuckyDraw) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putString(ConfigManager.AccountSession.SNAP_EARN_TICKETS_TNC, new Gson().toJson(snapTermLuckyDraw));
        editor.apply();
    }

    public String getTermsSnapEarnPoints() {
        return preferencesManis.getString(ConfigManager.AccountSession.SNAP_EARN_POINTS_TNC, "");
    }

    public String getTermsSnapEarnTickets() {
        return preferencesManis.getString(ConfigManager.AccountSession.SNAP_EARN_TICKETS_TNC, "");
    }
}