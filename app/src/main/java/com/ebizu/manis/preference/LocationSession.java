package com.ebizu.manis.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

import com.ebizu.manis.helper.ConfigManager;

/**
 * Created by Ebizu-User on 03/08/2017.
 */

public class LocationSession {

    private Context context;
    private SharedPreferences sharedPreferences;

    private static LocationSession instance;

    public static LocationSession getInstance(Context context) {
        if (null == instance) {
            instance = new LocationSession(context);
        }
        return instance;
    }

    public LocationSession(Context context) {
        sharedPreferences = context.getSharedPreferences(
                ConfigManager.Preference.KEY, Context.MODE_PRIVATE);
    }

    public void setSession(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setSession(Location location) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ConfigManager.LocationSession.LATITUDE, String.valueOf(location.getLatitude()));
        editor.putString(ConfigManager.LocationSession.LONGITUDE, String.valueOf(location.getLongitude()));
        editor.apply();
    }

    public void clearSession() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(ConfigManager.LocationSession.LATITUDE);
        editor.remove(ConfigManager.LocationSession.LONGITUDE);
        editor.apply();
    }

    public com.ebizu.manis.model.Location getLocation() {
        com.ebizu.manis.model.Location location = new com.ebizu.manis.model.Location();
        location.setLatitude(sharedPreferences.getString(ConfigManager.LocationSession.LATITUDE, ""));
        location.setLongitude(sharedPreferences.getString(ConfigManager.LocationSession.LONGITUDE, ""));
        return location;
    }

    public String getLatLong() {
        String latitude = sharedPreferences.getString(ConfigManager.LocationSession.LATITUDE, "");
        String longitude = sharedPreferences.getString(ConfigManager.LocationSession.LONGITUDE, "");
        return latitude.concat(", ").concat(longitude);
    }

    public double getLat() {
        String latitude = sharedPreferences.getString(ConfigManager.LocationSession.LATITUDE, "");
        if (latitude.isEmpty()) {
            return 0;
        } else {
            return Double.valueOf(latitude);
        }
    }

    public double getLong() {
        String longitude = sharedPreferences.getString(ConfigManager.LocationSession.LONGITUDE, "");
        if (longitude.isEmpty()) {
            return 0;
        } else {
            return Double.valueOf(longitude);
        }
    }
}
