package com.ebizu.manis.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.notification.IntervalBeaconPromo;

/**
 * Created by ebizu on 9/4/17.
 */

public class DeviceSession {

    private SharedPreferences preferencesDevice;

    public DeviceSession(Context context) {
        preferencesDevice = context.getSharedPreferences(
                ConfigManager.Preference.KEY_DEVICE, Context.MODE_PRIVATE);
    }

    public void setSession(String key, boolean value) {
        SharedPreferences.Editor editor = preferencesDevice.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void setSessionIntervalBeacon(IntervalBeaconPromo intervalBeacon) {
        SharedPreferences.Editor editor = preferencesDevice.edit();
        editor.putInt(ConfigManager.App.INTERVAL_BEACON, intervalBeacon.getInterval());
        editor.putString(ConfigManager.App.TYPE_INTERVAL_BEACON, intervalBeacon.getType());
        editor.apply();
    }

    public void setHasIntroduction() {
        SharedPreferences.Editor editor = preferencesDevice.edit();
        editor.putBoolean(ConfigManager.App.INTRODUCTION, true);
        editor.apply();
    }

    public IntervalBeaconPromo getSessionIntervalBeacon() {
        IntervalBeaconPromo intervalBeaconPromo = new IntervalBeaconPromo();
        intervalBeaconPromo.setInterval(preferencesDevice.getInt(ConfigManager.App.INTERVAL_BEACON, 1));
        intervalBeaconPromo.setType(preferencesDevice.getString(ConfigManager.App.TYPE_INTERVAL_BEACON, "DAY"));
        return intervalBeaconPromo;
    }

    public boolean isFirstIntro() {
        return preferencesDevice.getBoolean(ConfigManager.App.INTRODUCTION, true);
    }

    public boolean isAllowDialogPermission() {
        return preferencesDevice.getBoolean(ConfigManager.App.LOGIN_ASK_DIALOG_PERMISSION, true);
    }

    public boolean hasShowSnapEarnGuide() {
        return preferencesDevice.getBoolean(ConfigManager.App.SNAP_EARN_GUIDE_SESSION, false);
    }

    public void completeShowSnapEarnGuide() {
        SharedPreferences.Editor editor = preferencesDevice.edit();
        editor.putBoolean(ConfigManager.App.SNAP_EARN_GUIDE_SESSION, true);
        editor.apply();
    }

    public void completeIntro() {
        SharedPreferences.Editor editor = preferencesDevice.edit();
        editor.putBoolean(ConfigManager.App.INTRODUCTION, false);
        editor.apply();
    }

    public void allowDialogPermission() {
        SharedPreferences.Editor editor = preferencesDevice.edit();
        editor.putBoolean(ConfigManager.App.LOGIN_ASK_DIALOG_PERMISSION, false);
        editor.apply();
    }

    public boolean isAlwaysShowGuideEarnTicket() {
        return preferencesDevice.getBoolean(ConfigManager.App.SNAP_EARN_TICKET_GUIDE_SESSION, false);
    }

    public void completeShowSnapEarnTicketGuide() {
        SharedPreferences.Editor editor = preferencesDevice.edit();
        editor.putBoolean(ConfigManager.App.SNAP_EARN_TICKET_GUIDE_SESSION, true);
        editor.apply();
    }

    public void clearSessionGuide() {
        SharedPreferences.Editor editor = preferencesDevice.edit();
        editor.remove(ConfigManager.App.SNAP_EARN_TICKET_GUIDE_SESSION);
        editor.apply();
    }

}
