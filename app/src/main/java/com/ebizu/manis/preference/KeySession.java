package com.ebizu.manis.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.ebizu.manis.helper.ConfigManager;

/**
 * Created by Ebizu-User on 14/07/2017.
 */

public class KeySession {

    private final String PUBLIC_KEY = "PUBLIC_KEY";
    private final String PRIVATE_KEY = "PRIVATE_KEY";

    private SharedPreferences preferencesKey;

    public KeySession(Context context) {
        preferencesKey = context.getSharedPreferences(
                ConfigManager.Preference.KEY, Context.MODE_PRIVATE);
    }

    public void setPreferencesKey(String publicKey, String privateKey) {
        SharedPreferences.Editor editor = preferencesKey.edit();
        editor.putString(PUBLIC_KEY, publicKey);
        editor.putString(PRIVATE_KEY, privateKey);
        editor.apply();
    }

    public boolean haveKey() {
        boolean key = false;
        String privateKey = getPrivateKey();
        if (!privateKey.equals("")) {
            key = true;
        }
        return key;
    }

    public void removePreferencesKey() {
        SharedPreferences.Editor editor = preferencesKey.edit();
        editor.clear().apply();
    }

    public void setPublicKey(long ping) {
        SharedPreferences.Editor editor = preferencesKey.edit();
        editor.putLong(PUBLIC_KEY, ping);
        editor.apply();
    }

    public String getPublicKey() {
        return preferencesKey.getString(PUBLIC_KEY, "");
    }

    public void setPrivateKey(long pong) {
        SharedPreferences.Editor editor = preferencesKey.edit();
        editor.putLong(PRIVATE_KEY, pong);
        editor.apply();
    }

    public String getPrivateKey() {
        return preferencesKey.getString(PRIVATE_KEY, "");
    }
}
