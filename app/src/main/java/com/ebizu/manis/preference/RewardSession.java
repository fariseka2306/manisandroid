package com.ebizu.manis.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.reward.RewardApiConfig;

/**
 * Created by ebizu on 9/27/17.
 */

public class RewardSession {

    private SharedPreferences preferencesManis;
    private static RewardSession instance;

    public static RewardSession getInstance(Context context) {
        if (null == instance) {
            instance = new RewardSession(context);
        }
        return instance;
    }

    public RewardSession(Context context) {
        preferencesManis = context.getSharedPreferences(
                ConfigManager.Preference.KEY, Context.MODE_PRIVATE);
    }

    public void setSession(String session) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putString(RewardApiConfig.Preference.SESSION, session);
        editor.apply();
    }

    public void clearSession() {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.remove(RewardApiConfig.Preference.SESSION);
        editor.apply();
    }

    public String getSession() {
        return preferencesManis.getString(RewardApiConfig.Preference.SESSION, "");
    }
}
