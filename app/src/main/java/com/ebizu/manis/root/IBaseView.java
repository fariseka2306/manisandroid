package com.ebizu.manis.root;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.preference.RewardSession;
import com.ebizu.manis.view.manis.nodatastorefound.NoDataStoreFoundView;

/**
 * Created by Ebizu-User on 13/07/2017.
 */

public interface IBaseView {

    void createView(Context context);

    void attachPresenter(IBaseViewPresenter iBaseViewPresenter);

    void showProgressBar();

    void showNotificationMessage(String message);

    void dismissNotificationMessage();

    void dismissProgressBar();

    void dismissNoInternetConnection();

    void dismissProgressIndeterminate();

    void dismissGpsDeactivateView();

    void dismissNoDataStoreView();

    void brintToFrontOfBaseView(View view);

    void setActivity(BaseActivity appCompatActivity);

    BaseActivity getBaseActivity();

    AnalyticManager getAnalyticManager();

    RewardSession getRewardSession();

    void showNoInternetConnection();

    void showGpsDeactivateView();

    void showGpsDeactivateView(OnGPSListener onGPSListener);

    void showNoDataStoreView();

    void showNoDataStoreView(NoDataStoreFoundView.Type typeView);

    void showProgressIndeterminate();

    void onRetry();

    enum LoadType {
        CLICK_LOAD,
        SWIPE_LOAD,
        SCROLL_LOAD,
        SEARCH_LOAD
    }

    interface OnGPSListener {
        void onClick();
    }

}
