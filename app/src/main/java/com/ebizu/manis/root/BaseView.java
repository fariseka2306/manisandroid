package com.ebizu.manis.root;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.preference.RewardSession;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.reward.response.WrapperMyVoucher;
import com.ebizu.manis.view.manis.location.GpsLocationInactiveView;
import com.ebizu.manis.view.manis.nodatastorefound.NoDataStoreFoundView;
import com.ebizu.manis.view.manis.nointernetconnection.NoInternetConnectionView;
import com.ebizu.manis.view.manis.notification.NotificationMessageView;

import rx.Subscription;

/**
 * Created by Ebizu-User on 13/07/2017.
 */

public abstract class BaseView extends RelativeLayout implements IBaseView {

    private IBaseViewPresenter iBaseViewPresenter;
    private BaseActivity baseActivity;
    private ManisApi manisApi;
    private ManisSession manisSession;
    private RewardSession rewardSession;
    private AnalyticManager analyticManager;

    //*** View Component ***
    private ProgressBar progressBar;
    private ProgressBar progressBarIndeterminate;
    private NoInternetConnectionView noInternetConnectionView;
    private NotificationMessageView notificationMessageView;
    private GpsLocationInactiveView gpsLocationInactiveView;
    private NoDataStoreFoundView noDataStoreFoundView;

    public BaseView(Context context) {
        super(context);
    }

    public BaseView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BaseView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void createView(Context context) {
    }

    @Override
    public void attachPresenter(IBaseViewPresenter iBaseViewPresenter) {
        this.iBaseViewPresenter = iBaseViewPresenter;
    }

    @Override
    public void brintToFrontOfBaseView(View view) {
        getParent().bringChildToFront(view);
        getParent().requestLayout();
    }

    @Override
    public void showProgressBar() {
        if (progressBar == null)
            addViewProgressBar();
        progressBar.setVisibility(VISIBLE);
        brintToFrontOfBaseView(progressBar);
    }

    @Override
    public void dismissProgressBar() {
        if (progressBar == null)
            addViewProgressBar();
        progressBar.setVisibility(GONE);
    }

    @Override
    public void setActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    @Override
    public void showNoInternetConnection() {
        if (noInternetConnectionView == null)
            addViewInternetConnection();
        if (!noInternetConnectionView.isShown())
            noInternetConnectionView.setVisibility(VISIBLE);
    }

    @Override
    public void dismissNoInternetConnection() {
        if (noInternetConnectionView == null)
            addViewInternetConnection();
        if (noInternetConnectionView.isShown())
            noInternetConnectionView.setVisibility(GONE);
    }

    @Override
    public void onRetry() {
        dismissNoInternetConnection();
    }

    @Override
    public void showNotificationMessage(String message) {
        if (notificationMessageView == null)
            addViewNotificationMessage();
        notificationMessageView.showNotif(message);
    }

    @Override
    public void showProgressIndeterminate() {
        if (progressBarIndeterminate == null)
            addProgressBarIndeterminate();
        progressBarIndeterminate.setVisibility(VISIBLE);
        progressBarIndeterminate.setIndeterminate(true);
    }

    @Override
    public void showGpsDeactivateView() {
        if (gpsLocationInactiveView == null)
            addGpsDeactiveView();
        gpsLocationInactiveView.setVisibility(VISIBLE);
    }

    @Override
    public void showGpsDeactivateView(OnGPSListener onGPSListener) {
        if (gpsLocationInactiveView == null)
            addGpsDeactiveView();
        gpsLocationInactiveView.setVisibility(VISIBLE);
        gpsLocationInactiveView.setOnClickListener(view -> {
            onGPSListener.onClick();
        });
    }

    @Override
    public void showNoDataStoreView() {
        if (null == noDataStoreFoundView) {
            addNoDataStoreView();
        }

        noDataStoreFoundView.setVisibility(VISIBLE);
    }

    @Override
    public void showNoDataStoreView(NoDataStoreFoundView.Type typeView) {
        if (null == noDataStoreFoundView) {
            addNoDataStoreView();
        }
        noDataStoreFoundView.setTypeView(typeView);
        noDataStoreFoundView.setVisibility(VISIBLE);
    }

    @Override
    public void dismissGpsDeactivateView() {
        if (gpsLocationInactiveView == null)
            addGpsDeactiveView();
        gpsLocationInactiveView.setVisibility(GONE);
    }

    @Override
    public void dismissProgressIndeterminate() {
        if (progressBarIndeterminate == null)
            addProgressBarIndeterminate();
        progressBarIndeterminate.setVisibility(GONE);
        progressBarIndeterminate.setIndeterminate(false);
    }

    @Override
    public void dismissNotificationMessage() {
        notificationMessageView.dismissNotif();
    }

    @Override
    public void dismissNoDataStoreView() {
        if (null == noDataStoreFoundView) {
            addNoDataStoreView();
        }
        noDataStoreFoundView.setVisibility(GONE);
    }

    @Override
    public BaseActivity getBaseActivity() {
        return baseActivity;
    }

    @Override
    public AnalyticManager getAnalyticManager() {
        if (null == analyticManager) {
            analyticManager = new AnalyticManager(getContext());
        }
        return analyticManager;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (iBaseViewPresenter != null) {
            iBaseViewPresenter.releaseAllSubscribes();
        }
    }

    @Override
    public RewardSession getRewardSession() {
        if (null == rewardSession) {
            rewardSession = new RewardSession(getContext());
        }
        return rewardSession;
    }

    private void addViewProgressBar() {
        addView(LayoutInflater.from(getContext()).inflate(R.layout.progress_bar, null, false),
                new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(GONE);
    }

    private void addViewInternetConnection() {
        noInternetConnectionView = new NoInternetConnectionView(getContext());
        addView(noInternetConnectionView);
        centerOnViewGroup(noInternetConnectionView);
        noInternetConnectionView.setVisibility(GONE);
        noInternetConnectionView.setOnClickListener(this::onRetry);
    }

    private void addViewNotificationMessage() {
        notificationMessageView = new NotificationMessageView(getContext());
        addView(notificationMessageView);
    }

    private void addProgressBarIndeterminate() {
        addView(LayoutInflater.from(getContext()).inflate(R.layout.progress_bar_horizontal, null, false),
                new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        progressBarIndeterminate = (ProgressBar) findViewById(R.id.progress_bar_indeterminate);
    }

    private void addGpsDeactiveView() {
        gpsLocationInactiveView = new GpsLocationInactiveView(getContext());
        addView(gpsLocationInactiveView, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        gpsLocationInactiveView.setVisibility(GONE);
    }

    private void addNoDataStoreView() {
        noDataStoreFoundView = new NoDataStoreFoundView(getContext());
        addView(noDataStoreFoundView, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        noDataStoreFoundView.setVisibility(GONE);
    }

    private void centerOnViewGroup(View view) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        view.setLayoutParams(layoutParams);
    }

    public ManisSession getManisSession() {
        if (manisApi == null)
            manisSession = ManisSession.getInstance(getContext());
        return manisSession;
    }

    public ManisApi getManisApi() {
        if (manisApi == null) {
            if (getManisSession().isLoggedIn()) {
                manisApi = ManisApiGenerator.createServiceWithToken(getContext());
            } else {
                manisApi = ManisApiGenerator.createService(getContext());
            }
        }
        return manisApi;
    }

    public void unSubscribeAll(SparseArray<Subscription> subscriptionSparse) {
        int size = subscriptionSparse.size();
        for (int i = 0; i < size; i++) {
            int key = subscriptionSparse.keyAt(i);
            Subscription subscription = subscriptionSparse.get(key);
            if (subscription != null) {
                subscription.unsubscribe();
                subscriptionSparse.remove(key);
            }
        }
    }
}