package com.ebizu.manis.root;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.di.component.ApplicationComponent;
import com.ebizu.manis.di.component.DaggerApplicationComponent;
import com.ebizu.manis.di.module.ApplicationModule;
import com.ebizu.manis.helper.AnalyticsTrackers;
import com.ebizu.manis.manager.tracker.TrackerManager;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.sdk.Ebizu;
import com.ebizu.sdk.reward.EbizuReward;
import com.ebizu.sdk.reward.core.Config;
import com.facebook.FacebookSdk;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.SdkCoreFlowBuilder;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import org.litepal.LitePal;

import io.fabric.sdk.android.Fabric;
import io.intercom.android.sdk.Intercom;

/**
 * Created by Raden on 7/4/17.
 */

public class ManisApplication extends MultiDexApplication {

    private ApplicationComponent applicationComponent;
    private static ManisApplication instance;

    public static ManisApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Ebizu.getInstance(this, true);
        Ebizu.getInstance().start();
        FacebookSdk.sdkInitialize(this);
        Intercom.initialize(this, BuildConfig.INTERCOM_APIKEY, BuildConfig.INTERCOM_APIID);
        LitePal.initialize(this);
        initializeMidtrans();
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(BuildConfig.TWITTER_CONSUMER_KEY, BuildConfig.TWITTER_CONSUMER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);
        AnalyticsTrackers.initialize(this);
        ManisSession.initialize(this);
        TrackerManager.initialize(this);
        setupCrashlytics();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        setManisEnv();
        applicationComponent = DaggerApplicationComponent.builder().
                applicationModule(new ApplicationModule(this)).
                build();
    }

    private void setManisEnv() {
        if (BuildConfig.FLAVOR.equals("prod")) {
            EbizuReward.getInstance().init(this, Config.PRODUCTION);
        } else if (BuildConfig.FLAVOR.equals("stag")) {
            EbizuReward.getInstance().init(this, Config.STAGING);
        }
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    private void initializeMidtrans() {
        SdkCoreFlowBuilder.init(this, BuildConfig.MIDTRANS_CLIENT_KEY, BuildConfig.MIDTRANS_BASE_URL)
                .enableLog(true)
                .buildSDK();
        MidtransSDK midtransSDK = MidtransSDK.getInstance();
    }

    private void setupCrashlytics() {
        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit);
    }
}