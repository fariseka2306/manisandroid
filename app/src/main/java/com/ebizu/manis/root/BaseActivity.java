package com.ebizu.manis.root;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.ebizu.manis.R;
import com.ebizu.manis.database.litepal.NotificationDatabase;
import com.ebizu.manis.di.component.ApplicationComponent;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.manager.permission.PermissionManager;
import com.ebizu.manis.mvp.login.LoginActivity;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.preference.RewardSession;
import com.ebizu.manis.view.dialog.DialogPermissionManis;
import com.ebizu.manis.view.dialog.inviteterm.InviteTermDialog;
import com.ebizu.manis.view.manis.nointernetconnection.NoInternetConnectionView;
import com.ebizu.manis.view.manis.notification.NotificationMessageView;
import com.ebizu.sdk.reward.EbizuReward;
import com.facebook.AccessToken;
import com.facebook.accountkit.AccountKit;
import com.google.android.gms.common.ConnectionResult;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;

import io.intercom.android.sdk.Intercom;
import rx.Subscription;

/**
 * Created by Ebizu-User on 06/07/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements IBaseActivity {

    private final String TAG = getClass().getSimpleName();

    private IBaseActivityPresenter iBaseActivityPresenter;
    private PermissionManager permissionManager;
    private AnalyticManager analyticManager;
    private ManisSession manisSession;
    private DeviceSession deviceSession;
    private RewardSession rewardSession;
    private OnDialogPermissionsListener onDialogPermissionsListener;

    /* All view component */
    private ViewGroup decorView;
    private ProgressBar progressBarBaseView;
    private ProgressDialog progressDialog;
    private NoInternetConnectionView noInternetConnectionView;
    private NotificationMessageView notificationMessageView;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepareInjection();
        permissionManager = new PermissionManager(this);
        createView();
        attachPresenter(iBaseActivityPresenter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (iBaseActivityPresenter != null) {
            iBaseActivityPresenter.releaseAllSubscribe();
        }
    }

    @Override
    public void finish() {
        super.finish();
        UtilManis.closeKeyboard(this, null);
    }

    @Override
    public void createView() {
        decorView = (ViewGroup) getWindow().getDecorView();
        LayoutInflater.from(this).inflate(R.layout.progress_bar, decorView, true);
        progressBarBaseView = (ProgressBar) findViewById(R.id.progress_bar);
        progressBarBaseView.setVisibility(View.GONE);
    }

    protected void prepareInjection() {

    }

    protected ApplicationComponent getApplicationComponent() {
        return ((ManisApplication) getApplicationContext()).getApplicationComponent();
    }

    @Override
    public void showAlertDialog(String title, String message, boolean cancelAble, String positive, DialogInterface.OnClickListener positiveListener) {
        if (null == alertDialog)
            alertDialog = new AlertDialog.Builder(this).setTitle(title).setMessage(message).setCancelable(cancelAble)
                    .setPositiveButton(positive, positiveListener)
                    .create();
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    public void showAlertDialog(String title, String message, boolean cancelAble, String positive, DialogInterface.OnClickListener positiveListener, String negative, DialogInterface.OnClickListener negativeListener) {
        if (null == alertDialog)
            alertDialog = new AlertDialog.Builder(this).setTitle(title).setMessage(message).setCancelable(cancelAble)
                    .setPositiveButton(positive, positiveListener)
                    .setNegativeButton(negative, negativeListener)
                    .create();
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    public void showAlertDialog(String title, String message, boolean cancelAble, int drawable, String positive, DialogInterface.OnClickListener positiveListener, String negative, DialogInterface.OnClickListener negativeListener) {
        if (null == alertDialog)
            alertDialog = new AlertDialog.Builder(this).setTitle(title).setMessage(message).setCancelable(cancelAble).setIcon(drawable)
                    .setPositiveButton(positive, positiveListener)
                    .setNegativeButton(negative, negativeListener)
                    .create();
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    public void showAlertDialog(String title, String message, boolean cancelAble, int drawable, String positive, DialogInterface.OnClickListener positiveListener) {
        if (null == alertDialog)
            alertDialog = new AlertDialog.Builder(this).setTitle(title).setMessage(message).setCancelable(cancelAble).setIcon(drawable)
                    .setPositiveButton(positive, positiveListener)
                    .create();
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    public void showAlertDialog(String message, boolean cancelAble, String positive, DialogInterface.OnClickListener positiveListener, String negative, DialogInterface.OnClickListener negativeListener) {
        if (null == alertDialog)
            alertDialog = new AlertDialog.Builder(this).setMessage(message).setCancelable(cancelAble)
                    .setPositiveButton(positive, positiveListener)
                    .setNegativeButton(negative, negativeListener)
                    .create();
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    public void showDialogPermission(int requestCode, OnDialogPermissionsListener onDialogPermissionsListener) {
        DialogPermissionManis dialogPermissionManis = new DialogPermissionManis(this);
        dialogPermissionManis.setNextListener(() -> {
                    if (onDialogPermissionsListener != null)
                        onDialogPermissionsListener.onAllow(requestCode);
                    dialogPermissionManis.dismiss();
                }
        );
        dialogPermissionManis.setCancelListener(() -> {
            if (onDialogPermissionsListener != null)
                onDialogPermissionsListener.onDeny();
            dialogPermissionManis.dismiss();
        });
        dialogPermissionManis.show();
    }

    public void showManisAlertDialog(String message) {
        if (null == alertDialog)
            alertDialog = new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.error))
                    .setMessage(message).setCancelable(true).setIcon(ContextCompat.getDrawable(this, R.drawable.manis_logo))
                    .setPositiveButton(getResources().getString(R.string.positive_dialog_ok), null)
                    .create();
        alertDialog.setOnShowListener(dialog -> {
            Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(view -> {
                dialog.dismiss();
            });
        });
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    public void showManisAlertDialog(String message, DialogInterface.OnDismissListener onDismissListener) {
        if (null == alertDialog)
            alertDialog = new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.error))
                    .setMessage(message).setCancelable(true).setIcon(ContextCompat.getDrawable(this, R.drawable.manis_logo))
                    .setPositiveButton(getResources().getString(R.string.positive_dialog_ok), null)
                    .create();
        alertDialog.setOnShowListener(dialog -> {
            Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(view -> {
                dialog.dismiss();
            });
        });
        alertDialog.setOnDismissListener(onDismissListener);
        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    @Override
    public void showDialogPermission(int requestCode) {
        DialogPermissionManis dialogPermissionManis = new DialogPermissionManis(this);
        dialogPermissionManis.setNextListener(() -> {
                    if (onDialogPermissionsListener != null)
                        onDialogPermissionsListener.onAllow(requestCode);
                    dialogPermissionManis.dismiss();
                }
        );
        dialogPermissionManis.setCancelListener(() -> {
            if (onDialogPermissionsListener != null)
                onDialogPermissionsListener.onDeny();
            dialogPermissionManis.dismiss();
        });
        dialogPermissionManis.show();
    }

    @Override
    public void showBaseProgressBar() {
        if (!progressBarBaseView.isShown())
            progressBarBaseView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressBarDialog(String message, boolean cancelAble) {
        if (null == progressDialog)
            addProgressBarDialog();
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void showProgressBarDialog(String message) {
        if (null == progressDialog)
            addProgressBarDialog();
        progressDialog.setMessage(message);
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void showProgressBarDialog(String message, DialogInterface.OnCancelListener onCancelListener) {
        if (null == progressDialog)
            addProgressBarDialog();
        progressDialog.setOnCancelListener(onCancelListener);
        progressDialog.setMessage(message);
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void dismissProgressBarDialog() {
        if (null == progressDialog)
            addProgressBarDialog();
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void dismissBaseProgressBar() {
        if (progressBarBaseView.isShown())
            progressBarBaseView.setVisibility(View.GONE);
    }

    @Override
    public void attachPresenter(IBaseActivityPresenter presenter) {
        iBaseActivityPresenter = presenter;
    }

    @Override
    public Context baseContext() {
        return this;
    }

    @Override
    public AppCompatActivity baseActivity() {
        return this;
    }

    @Override
    public ManisSession getManisSession() {
        if (manisSession == null)
            manisSession = new ManisSession(this);
        return manisSession;
    }

    @Override
    public DeviceSession getDeviceSession() {
        if (deviceSession == null)
            deviceSession = new DeviceSession(this);
        return deviceSession;
    }

    @Override
    public void showGoogleConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void showNotificationMessage(String message) {
        if (notificationMessageView == null)
            addNotificationMessageView();
        notificationMessageView.showNotif(message);
    }

    @Override
    public void dismissNotificationMessage() {
        if (notificationMessageView == null)
            addNotificationMessageView();
        notificationMessageView.dismissNotif();
    }

    @Override
    public void showNoInternetConnection() {
        if (noInternetConnectionView == null)
            addNoInternetConnectionView();
        noInternetConnectionView.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissNoInternetConnection() {
        if (noInternetConnectionView == null)
            addNoInternetConnectionView();
        noInternetConnectionView.setVisibility(View.VISIBLE);
    }

    @Override
    public PermissionManager getPermissionManager() {
        return permissionManager;
    }

    @Override
    public AnalyticManager getAnalyticManager() {
        if (null == analyticManager)
            analyticManager = new AnalyticManager(this);
        return analyticManager;
    }

    @Override
    public RewardSession getRewardSession() {
        if (null == rewardSession) {
            rewardSession = new RewardSession(this);
        }
        return rewardSession;
    }

    @Override
    public void showInviteFriendDialog() {
        SharedPreferences prefFirst = getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
        if (UtilSessionFirstIn.isShowInviteTerms(prefFirst)) {
            InviteTermDialog inviteTermDialog = new InviteTermDialog(this);
            inviteTermDialog.setActivity(this);
            inviteTermDialog.show();
            inviteTermDialog.getInviteTermPresenter().loadInviteTerm(this.getApplicationContext());
        } else {
            InviteTermDialog inviteTermDialog = new InviteTermDialog(this);
            inviteTermDialog.setActivity(this);
            inviteTermDialog.getInviteTermPresenter().loadInviteTerm(this.getApplicationContext());
            inviteTermDialog.generateBranch();
        }
    }

    protected void slideInVerticalAnim() {
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.stay);
    }

    protected void slideInHorizontalAnim() {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    protected void slideOutVerticalAnim() {
        overridePendingTransition(R.anim.stay, R.anim.slide_out_bottom);
    }

    protected void slideOutHorizontalAnim() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void addNoInternetConnectionView() {
        noInternetConnectionView = new NoInternetConnectionView(this);
        decorView.addView(noInternetConnectionView);
        noInternetConnectionView.setVisibility(View.GONE);
    }

    private void addNotificationMessageView() {
        notificationMessageView = new NotificationMessageView(this);
        decorView.addView(notificationMessageView);
        notificationMessageView.setVisibility(View.GONE);
    }

    private void addProgressBarDialog() {
        progressDialog = new ProgressDialog(this);
    }

    @Override
    public void signOut() {
        showProgressBarDialog(getString(R.string.please_wait));
        getManisSession().clearSession();
        getManisSession().clearDeviceSession();
        NotificationDatabase.getInstance().deleteAll();
        EbizuReward.getSession(this).logout();
        getDeviceSession().clearSessionGuide();
        Intercom.client().reset();
        SharedPreferences prefFirst = getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
        UtilSessionFirstIn.setShowGuideSnap(prefFirst, true);
        UtilSessionFirstIn.setLoginWithphone(prefFirst, false);
        AccountKit.logOut();
        clearFBSession();
        clearTwitterSession();
        new Handler().postDelayed(() -> {
            if (!getManisSession().isLoggedIn()) {
                dismissProgressBarDialog();
                Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
                startActivity(intent);
                slideInHorizontalAnim();
                finishAffinity();
            } else {
                signOut();
            }
        }, 1000);
    }

    private void clearFBSession() {
        if (AccessToken.getCurrentAccessToken() != null) {
            com.facebook.login.LoginManager.getInstance().logOut();
        }
    }

    private void clearTwitterSession() {
        final TwitterSession activeSession = TwitterCore.getInstance()
                .getSessionManager().getActiveSession();
        if (activeSession != null) {
            TwitterCore.getInstance().getSessionManager().clearActiveSession();
        }
    }

    public void unSubscribeAll(SparseArray<Subscription> subscriptionSparse) {
        int size = subscriptionSparse.size();
        for (int i = 0; i < size; i++) {
            int key = subscriptionSparse.keyAt(i);
            Subscription subscription = subscriptionSparse.get(key);
            if (subscription != null) {
                subscription.unsubscribe();
                subscriptionSparse.remove(key);
            }
        }
    }
}