package com.ebizu.manis.root;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;

import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.manager.permission.PermissionManager;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.preference.RewardSession;
import com.google.android.gms.common.ConnectionResult;

/**
 * Created by Ebizu-User on 06/07/2017.
 */

public interface IBaseActivity<Presenter> {

    /**
     * Every extend class IBaseActivity must be attach view to load Presenter
     */
    void attachPresenter(IBaseActivityPresenter<Presenter> presenter);

    AppCompatActivity baseActivity();

    Context baseContext();

    PermissionManager getPermissionManager();

    ManisSession getManisSession();

    DeviceSession getDeviceSession();

    AnalyticManager getAnalyticManager();

    RewardSession getRewardSession();

    void createView();

    void showProgressBarDialog(String message);

    void showProgressBarDialog(String message, boolean cancelAble);

    void showProgressBarDialog(String message, DialogInterface.OnCancelListener onCancelListener);

    void showAlertDialog(String title,
                         String message,
                         boolean cancelAble,
                         String positive,
                         DialogInterface.OnClickListener positiveListener);

    void showAlertDialog(String title,
                         String message,
                         boolean cancelAble,
                         String positive,
                         DialogInterface.OnClickListener positiveListener,
                         String negative,
                         DialogInterface.OnClickListener negativeListener);

    void showAlertDialog(String title,
                         String message,
                         boolean cancelAble,
                         int drawable,
                         String positive,
                         DialogInterface.OnClickListener positiveListener,
                         String negative,
                         DialogInterface.OnClickListener negativeListener);

    void showAlertDialog(String message,
                         boolean cancelAble,
                         String positive,
                         DialogInterface.OnClickListener positiveListener,
                         String negative,
                         DialogInterface.OnClickListener negativeListener);

    void showAlertDialog(String title,
                         String message,
                         boolean cancelAble,
                         int drawable,
                         String positive,
                         DialogInterface.OnClickListener positiveListener);

    void showBaseProgressBar();

    void dismissBaseProgressBar();

    void dismissProgressBarDialog();

    void showDialogPermission(int requestCode, OnDialogPermissionsListener onDialogPermissionsListener);

    void showDialogPermission(int requestCode);

    void showGoogleConnectionFailed(ConnectionResult connectionResult);

    void showNotificationMessage(String message);

    void showInviteFriendDialog();

    void dismissNotificationMessage();

    void showNoInternetConnection();

    void dismissNoInternetConnection();

    void signOut();

    interface OnDialogPermissionsListener {

        void onAllow(int requestCode);

        void onDeny();
    }

}