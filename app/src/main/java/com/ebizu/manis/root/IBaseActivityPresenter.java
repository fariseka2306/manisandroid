package com.ebizu.manis.root;

import android.content.Context;

import com.ebizu.manis.manager.google.GoogleApiManager;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.ManisApiV2;
import com.ebizu.manis.service.manis.key.ManisKeyGenerator;
import com.ebizu.manis.service.reward.RewardApi;

/**
 * Created by Ebizu-User on 05/07/2017.
 */

public interface IBaseActivityPresenter<View> {

    /**
     * Every extend class IBaseActivityPresenter must be attach view to load View
     *
     * @param view
     */
    void attachView(BaseActivity view);

    void releaseSubscribe(int key);

    void releaseAllSubscribe();

    Context getContext();

    GoogleApiManager getGoogleApiManager();

    ManisKeyGenerator getManisKeyGenerator();

    IBaseActivity getBaseActivity();

    RewardApi getRewardApi();

    ManisApi getManisApi();

    ManisApiV2 getManisApiV2();

    void updateCountrySession(String phoneNumber);

    void updateOtpSession(String phoneNumber);
}