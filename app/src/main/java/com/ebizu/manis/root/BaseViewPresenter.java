package com.ebizu.manis.root;

import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.GPSTracker;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.ManisApiV2;
import com.ebizu.manis.service.reward.RewardApi;
import com.ebizu.manis.service.reward.RewardApiGenerator;

/**
 * Created by Ebizu-User on 13/07/2017.
 */

public abstract class BaseViewPresenter implements IBaseViewPresenter {

    private BaseView baseView;
    private GPSTracker gpsTracker;
    private RewardApi rewardApi;
    private ManisApi manisApi;
    private ManisApiV2 manisApiV2;

    @Override
    public void attachView(BaseView baseView) {
        this.baseView = baseView;
    }

    @Override
    public void checkingGps() {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(baseView.getContext());
        if (!gpsTracker.isGPSEnabled()) {
            baseView.showGpsDeactivateView();
        } else {
            baseView.dismissGpsDeactivateView();
        }
    }

    @Override
    public void checkingGps(OnGPSListener onGPSListener) {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(baseView.getContext());
        if (!gpsTracker.isGPSEnabled()) {
            baseView.showGpsDeactivateView(onGPSListener::onClick);
        } else {
            baseView.dismissGpsDeactivateView();
        }
    }

    @Override
    public boolean isGpsEnable() {
        if (gpsTracker == null)
            gpsTracker = new GPSTracker(baseView.getContext());
        if (!gpsTracker.isGPSEnabled()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public RewardApi getRewardApi() {
        if (rewardApi == null)
            rewardApi = RewardApiGenerator.createService(baseView.getContext());
        return rewardApi;
    }

    @Override
    public RewardApi getRewardApiAry() {
        if (rewardApi == null)
            rewardApi = RewardApiGenerator.createServiceApiAry(baseView.getContext());
        return rewardApi;
    }

    @Override
    public ManisApi getManisApi() {
        if (null == manisApi) {
            if (baseView.getManisSession().isLoggedIn()) {
                manisApi = ManisApiGenerator.createServiceWithToken(baseView.getContext());
            } else {
                manisApi = ManisApiGenerator.createService(baseView.getContext());
            }
        }
        return manisApi;
    }

    @Override
    public void updateCountrySession(String phoneNumber) {
        if (phoneNumber.startsWith("62")) {
            baseView.getManisSession().setSession(ConfigManager.AccountSession.ADDRESS_COUNTRY, "Indonesia");
            baseView.getManisSession().setSession(ConfigManager.AccountSession.COUNTRY, "ID");
        } else {
            baseView.getManisSession().setSession(ConfigManager.AccountSession.ADDRESS_COUNTRY, "Malaysia");
            baseView.getManisSession().setSession(ConfigManager.AccountSession.COUNTRY, "MY");
        }
    }

    @Override
    public void updateOtpSession(String phoneNumber) {
        baseView.getManisSession().setSession(ConfigManager.AccountSession.MSISDN, phoneNumber);
        baseView.getManisSession().setSession(ConfigManager.AccountSession.OTP_STATUS, 1);
    }

    @Override
    public ManisApiV2 getManisApiV2() {
        if (null == manisApiV2)
            manisApiV2 = ManisApiGenerator.createServiceV2(baseView.getContext());
        return manisApiV2;
    }
}
