package com.ebizu.manis.root;

import android.content.Context;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.google.GoogleApiManager;
import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.ManisApiV2;
import com.ebizu.manis.service.manis.key.ManisKeyGenerator;
import com.ebizu.manis.service.reward.RewardApi;
import com.ebizu.manis.service.reward.RewardApiGenerator;

/**
 * Created by Ebizu-User on 05/07/2017.
 */

public abstract class BaseActivityPresenter implements IBaseActivityPresenter {

    private BaseActivity baseView;
    private GoogleApiManager googleApiManager;
    private RewardApi rewardApi;
    private ManisApi manisApi;
    private ManisApiV2 manisApiV2;

    @Override
    public void attachView(BaseActivity view) {
        baseView = view;
    }

    @Override
    public Context getContext() {
        return baseView.baseContext();
    }

    @Override
    public GoogleApiManager getGoogleApiManager() {
        if (googleApiManager == null)
            googleApiManager = new GoogleApiManager(baseView);
        return googleApiManager;
    }

    @Override
    public ManisKeyGenerator getManisKeyGenerator() {
        return new ManisKeyGenerator(getContext());
    }

    @Override
    public IBaseActivity getBaseActivity() {
        return baseView;
    }

    @Override
    public RewardApi getRewardApi() {
        if (rewardApi == null)
            rewardApi = RewardApiGenerator.createService(baseView);
        return rewardApi;
    }

    @Override
    public ManisApi getManisApi() {
        if (null == manisApi) {
            if (baseView.getManisSession().isLoggedIn()) {
                manisApi = ManisApiGenerator.createServiceWithToken(baseView);
            } else {
                manisApi = ManisApiGenerator.createService(baseView);
            }
        }
        return manisApi;
    }

    @Override
    public ManisApiV2 getManisApiV2() {
        if (null == manisApiV2) {
            manisApiV2 = ManisApiGenerator.createServiceV2(getContext());
        }
        return manisApiV2;
    }

    @Override
    public void updateCountrySession(String phoneNumber) {
        if (phoneNumber.startsWith("62")) {
            baseView.getManisSession().setSession(ConfigManager.AccountSession.ADDRESS_COUNTRY, "Indonesia");
            baseView.getManisSession().setSession(ConfigManager.AccountSession.COUNTRY, "ID");
        } else {
            baseView.getManisSession().setSession(ConfigManager.AccountSession.ADDRESS_COUNTRY, "Malaysia");
            baseView.getManisSession().setSession(ConfigManager.AccountSession.COUNTRY, "MY");
        }
    }

    @Override
    public void updateOtpSession(String phoneNumber) {
        baseView.getManisSession().setSession(ConfigManager.AccountSession.MSISDN, phoneNumber);
        baseView.getManisSession().setSession(ConfigManager.AccountSession.OTP_STATUS, 1);
    }
}