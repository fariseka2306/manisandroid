package com.ebizu.manis.root;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.manager.permission.PermissionManager;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.preference.RewardSession;
import com.google.android.gms.common.ConnectionResult;

/**
 * Created by Ebizu-User on 09/07/2017.
 */

public abstract class BaseFragment extends Fragment implements IBaseActivity {

    private IBaseActivityPresenter iBaseActivityPresenter;

    private ProgressDialog progressDialog;
    private AnalyticManager analyticManager;
    private RewardSession rewardSession;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (iBaseActivityPresenter != null) {
            iBaseActivityPresenter.releaseAllSubscribe();
        }
    }

    @Override
    public void attachPresenter(IBaseActivityPresenter presenter) {
        iBaseActivityPresenter = presenter;
    }

    @Override
    public AppCompatActivity baseActivity() {
        return null;
    }

    @Override
    public void showProgressBarDialog(String message) {
        progressDialog.setMessage(message);
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void showProgressBarDialog(String message, DialogInterface.OnCancelListener onCancelListener) {

    }

    @Override
    public Context baseContext() {
        return getContext();
    }

    @Override
    public void createView() {

    }

    @Override
    public void showAlertDialog(String title, String message, boolean cancelAble, String positive, DialogInterface.OnClickListener positiveListener, String negative, DialogInterface.OnClickListener negativeListener) {
        new AlertDialog.Builder(getContext()).setTitle(title).setMessage(message).setCancelable(cancelAble)
                .setPositiveButton(positive, positiveListener)
                .setNegativeButton(negative, negativeListener)
                .create().show();
    }

    @Override
    public void showAlertDialog(String title, String message, boolean cancelAble, int drawable, String positive, DialogInterface.OnClickListener positiveListener, String negative, DialogInterface.OnClickListener negativeListener) {
        new AlertDialog.Builder(getContext()).setTitle(title).setMessage(message).setCancelable(cancelAble).setIcon(drawable)
                .setPositiveButton(positive, positiveListener)
                .setNegativeButton(negative, negativeListener)
                .create().show();
    }


    @Override
    public void showAlertDialog(String title, String message, boolean cancelAble, int drawable, String positive, DialogInterface.OnClickListener positiveListener) {
        new AlertDialog.Builder(getContext()).setTitle(title).setMessage(message).setCancelable(cancelAble)
                .setPositiveButton(positive, positiveListener)
                .create().show();
    }

    @Override
    public void showAlertDialog(String title, String message, boolean cancelAble, String positive, DialogInterface.OnClickListener positiveListener) {
        new AlertDialog.Builder(getContext()).setTitle(title).setMessage(message).setCancelable(cancelAble)
                .setPositiveButton(positive, positiveListener)
                .create().show();
    }

    @Override
    public void showBaseProgressBar() {

    }

    @Override
    public void dismissBaseProgressBar() {

    }

    @Override
    public void dismissProgressBarDialog() {

    }

    @Override
    public void showGoogleConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public ManisSession getManisSession() {
        return null;
    }

    @Override
    public PermissionManager getPermissionManager() {
        return null;
    }

    @Override
    public void showAlertDialog(String message, boolean cancelAble, String positive, DialogInterface.OnClickListener positiveListener, String negative, DialogInterface.OnClickListener negativeListener) {

    }

    @Override
    public void showDialogPermission(int requestCode, OnDialogPermissionsListener onDialogPermissionsListener) {

    }

    @Override
    public void showProgressBarDialog(String message, boolean cancelAble) {

    }

    @Override
    public void dismissNoInternetConnection() {

    }

    @Override
    public void dismissNotificationMessage() {

    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override
    public void showNotificationMessage(String message) {

    }

    @Override
    public void showDialogPermission(int requestCode) {

    }

    @Override
    public void signOut() {

    }

    @Override
    public DeviceSession getDeviceSession() {
        return null;
    }

    @Override
    public AnalyticManager getAnalyticManager() {
        if (null == analyticManager)
            analyticManager = new AnalyticManager(getActivity());
        return analyticManager;
    }

    @Override
    public RewardSession getRewardSession() {
        if (null == rewardSession) {
            rewardSession = new RewardSession(getContext());
        }
        return rewardSession;
    }

    @Override
    public void showInviteFriendDialog() {

    }
}