package com.ebizu.manis.root;

import com.ebizu.manis.service.manis.ManisApi;
import com.ebizu.manis.service.manis.ManisApiV2;
import com.ebizu.manis.service.reward.RewardApi;

/**
 * Created by Ebizu-User on 13/07/2017.
 */

public interface IBaseViewPresenter {

    void attachView(BaseView baseView);

    void checkingGps();

    void checkingGps(OnGPSListener onGPSListener);

    void releaseSubscribe(int key);

    void releaseAllSubscribes();

    boolean isGpsEnable();

    ManisApi getManisApi();

    ManisApiV2 getManisApiV2();

    RewardApi getRewardApi();

    RewardApi getRewardApiAry();

    void updateCountrySession(String phoneNumber);

    void updateOtpSession(String phoneNumber);

    interface OnGPSListener {
        void onClick();
    }

}
