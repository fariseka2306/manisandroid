package com.ebizu.manis.rx;

import android.content.Context;
import android.support.annotation.CallSuper;

import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.google.gson.Gson;

import retrofit2.HttpException;
import rx.Subscriber;

/**
 * Created by Ebizu-User on 07/07/2017.
 */
public class ResponseSubscriber<WP extends ResponseData> extends Subscriber<WP> {

    private final String TAG = "Manis Api Response : ";
    private BaseActivity baseActivity;
    private BaseView baseView;
    private BaseDialogManis baseDialogManis;

    public ResponseSubscriber(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public ResponseSubscriber(BaseView baseView) {
        this.baseView = baseView;
    }

    public ResponseSubscriber(BaseDialogManis baseDialogManis) {
        this.baseDialogManis = baseDialogManis;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    @CallSuper
    public void onError(Throwable throwable) {
        Throwable error = throwable;
        try {
            if (throwable instanceof HttpException) {
                HttpException httpException = (HttpException) error;
                ErrorResponse errorResponse = new Gson().fromJson(httpException.response().errorBody().string(), ErrorResponse.class);
                String errorTokenIsRequired = "Token is required".trim().toLowerCase();
                String errorTokenExpired = "Field 'session' is required, can't be empty".trim().toLowerCase();
                String errorMessage = errorResponse.getMessage().trim().toLowerCase();
                String errorLoggedOut = getContext().getString(R.string.error_reward_session).trim().toLowerCase();
                if (errorMessage.equals(errorTokenIsRequired) || errorMessage.equals(errorTokenExpired) || errorMessage.equals(errorLoggedOut)) {
                    autoLogout(errorLoggedOut);
                    return;
                }
                onErrorFailure(errorResponse);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onErrorFailure(ErrorResponse errorResponse) {

    }

    @Override
    @CallSuper
    public void onNext(WP wp) {
    }

    private void autoLogout(String messageError) {
        if (null != baseActivity) {
            baseActivity.showAlertDialog(baseActivity.getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> {
                        baseActivity.signOut();
                    });
        } else if (null != baseView) {
            baseView.getBaseActivity().showAlertDialog(baseView.getContext().getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> {
                        baseView.getBaseActivity().signOut();
                    });
        } else if (null != baseDialogManis) {
            baseDialogManis.getBaseActivity().showAlertDialog(baseDialogManis.getContext().getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> {
                        baseDialogManis.getBaseActivity().signOut();
                    });
        }
    }

    private Context getContext() {
        if (null != baseActivity) {
            return baseActivity;
        } else if (null != baseView) {
            return baseView.getContext();
        } else if (null != baseDialogManis) {
            return baseDialogManis.getContext();
        } else {
            return null;
        }
    }
}