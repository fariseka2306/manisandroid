package com.ebizu.manis.rx;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseFragment;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.service.reward.RewardApiConfig;
import com.ebizu.manis.service.reward.response.ResponseRewardApi;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import rx.Subscriber;

/**
 * Created by ebizu on 9/26/17.
 */

public class ResponseRewardSubscriber<WRR extends ResponseRewardApi> extends Subscriber<WRR> {

    private final String TAG = "Reward Api Response : ";
    private BaseActivity baseActivity;
    private BaseView baseView;
    private BaseFragment baseFragment;
    private BaseDialogManis baseDialogManis;

    public ResponseRewardSubscriber(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public ResponseRewardSubscriber(BaseView baseView) {
        this.baseView = baseView;
    }

    public ResponseRewardSubscriber(BaseDialogManis baseDialogManis) {
        this.baseDialogManis = baseDialogManis;
    }

    public ResponseRewardSubscriber(BaseFragment baseFragment) {
        this.baseFragment = baseFragment;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        Log.e(TAG, "onError: " + e.getMessage());
    }

    @Override
    public void onNext(WRR wrr) {
        if (null != wrr.getErrorMessage()) {
            String errorMessage = wrr.getErrorMessage();
            if (errorMessage.equals(RewardApiConfig.ERROR_EMPTY_TOKEN)
                    || errorMessage.equals(RewardApiConfig.ERROR_SESSION_IS_REQUIRED)
                    || errorMessage.equals(getContext().getString(R.string.error_reward_session))) {
                autoLogout(RewardApiConfig.ERROR_USER_IS_LOGGED_OUT);
                return;
            } else if (wrr.getErrorCode() != -1) {
                onFailure(wrr);
            } else {
                onSuccess(wrr);
            }
        }
        Log.i(TAG, "onNext: " + wrr.toString());
    }

    public void onFailure(ResponseRewardApi responseRewardApi) {

    }


    protected void onSuccess(WRR wrr) {

    }

    private void autoLogout(String messageError) {
        if (null != baseActivity) {
            baseActivity.showAlertDialog(baseActivity.getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> baseActivity.signOut());
        } else if (null != baseView) {
            baseView.getBaseActivity().showAlertDialog(baseView.getContext().getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> baseView.getBaseActivity().signOut());
        } else if (null != baseDialogManis) {
            baseDialogManis.getBaseActivity().showAlertDialog(baseDialogManis.getContext().getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> baseDialogManis.getBaseActivity().signOut());
        }
    }

    private Context getContext() {
        if (null != baseActivity) {
            return baseActivity;
        } else if (null != baseView) {
            return baseView.getContext();
        } else if (null != baseDialogManis) {
            return baseDialogManis.getContext();
        } else {
            return null;
        }
    }
}