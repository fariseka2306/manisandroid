package com.ebizu.manis.rx;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.service.manis.responsev2.status.Status;
import com.ebizu.manis.service.manis.responsev2.wrapper.ResponseWrapper;
import com.ebizu.manis.view.dialog.BaseDialogManis;

import java.io.IOException;

import rx.Subscriber;

/**
 * Created by FARIS_mac on 10/17/17.
 */

public class ResponseV2Subscriber<WRR extends ResponseWrapper> extends Subscriber<WRR> {

    private final String TAG = getClass().getSimpleName();
    private BaseActivity baseActivity;
    private BaseView baseView;
    private BaseDialogManis baseDialogManis;
    private Context context;

    public ResponseV2Subscriber(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
        this.context = baseActivity;
    }

    public ResponseV2Subscriber(BaseView baseView) {
        this.baseView = baseView;
        this.context = baseView.getContext();
    }

    public ResponseV2Subscriber(BaseDialogManis baseDialogManis) {
        this.baseDialogManis = baseDialogManis;
        this.context = baseDialogManis.getContext();
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof IOException) {
            onNetworkFailed(context.getResources().getString(R.string.error_connection));
        } else {
            onNetworkFailed(context.getResources().getString(R.string.server_busy));
        }
        Log.e(TAG, "onError: " + e.getMessage());
    }

    @Override
    public void onNext(WRR wrr) {
        if (null != wrr.getStatus()) {
            checkResponseCode(wrr);
        }
        Log.i(TAG, "onNext: " + wrr.toString());
    }

    private void checkResponseCode(WRR wrr) {
        Status status = wrr.getStatus();
        int statusCode = status.getCode();
        if (statusCode == ConfigManager.MissionStatusCode.MISSION_TOKEN_EXPIRED) {
            tokenValidation(status);
            return;
        } else if (statusCode == ConfigManager.MissionStatusCode.MISSION_ERROR || statusCode == ConfigManager.MissionStatusCode.MISSION_EXECUTE_PIN_FAILED) {
            onErrorFailure(status);
            return;
        } else if (statusCode == ConfigManager.MissionStatusCode.MISSION_SUCCESS) {
            onSuccess(wrr);
        } else {
            onNetworkFailed(context.getResources().getString(R.string.server_busy));
        }
    }

    private void tokenValidation(Status status) {
        String errorTokenIsRequired = "Token is required".trim().toLowerCase();
        String errorTokenExpired = "Field 'session' is required, can't be empty".trim().toLowerCase();
        String errorMessage = status.getMessageClient().trim().toLowerCase();
        String errorLoggedOut = getContext().getString(R.string.error_reward_session).trim().toLowerCase();
        if (errorMessage.equals(errorTokenIsRequired) || errorMessage.equals(errorTokenExpired) || errorMessage.equals(errorLoggedOut)) {
            autoLogout(errorLoggedOut);
        }
    }

    private void autoLogout(String messageError) {
        if (null != baseActivity) {
            baseActivity.showAlertDialog(baseActivity.getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> {
                        baseActivity.signOut();
                    });
        } else if (null != baseView) {
            baseView.getBaseActivity().showAlertDialog(baseView.getContext().getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> {
                        baseView.getBaseActivity().signOut();
                    });
        } else if (null != baseDialogManis) {
            baseDialogManis.getBaseActivity().showAlertDialog(baseDialogManis.getContext().getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> {
                        baseDialogManis.getBaseActivity().signOut();
                    });
        }
    }

    protected void onErrorFailure(Status status) {

    }

    protected void onSuccess(WRR wrr) {

    }

    protected void onNetworkFailed(String message) {

    }

    private Context getContext() {
        if (null != baseActivity) {
            return baseActivity;
        } else if (null != baseView) {
            return baseView.getContext();
        } else if (null != baseDialogManis) {
            return baseDialogManis.getContext();
        } else {
            return null;
        }
    }
}
