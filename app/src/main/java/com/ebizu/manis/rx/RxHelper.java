package com.ebizu.manis.rx;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by Ebizu-User on 01/08/2017.
 */

public class RxHelper {

    public static Observable<String> setTextWatcherObservable(@NonNull final EditText editText) {
        final PublishSubject<String> textChanged = PublishSubject.create();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                textChanged.onNext(s.toString());
            }
        });
        return textChanged;
    }

    public static Observable<String> setTextWatcherObservable(@NonNull final SearchView searchView) {
        final PublishSubject<String> textChanged = PublishSubject.create();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 2) {
                    textChanged.onNext(newText);
                } else {
                    Throwable throwable = new Throwable("Need 3 char");
                    textChanged.onError(throwable);
                }
                return false;
            }
        });
        return textChanged;
    }

}