package com.ebizu.manis.rx;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.service.manis.response.ResponseData;
import com.ebizu.manis.view.dialog.BaseDialogManis;
import com.google.gson.Gson;

import java.io.IOException;

import rx.Subscriber;

/**
 * Created by ebizu on 12/7/17.
 */

public class ResponseSubsriberLambda<WRR extends ResponseData> extends Subscriber<WRR> {

    private final String TAG = getClass().getSimpleName().concat(" API");
    private BaseActivity baseActivity;
    private BaseView baseView;
    private BaseDialogManis baseDialogManis;
    private Context context;

    public ResponseSubsriberLambda(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
        this.context = baseActivity;
    }

    public ResponseSubsriberLambda(BaseView baseView) {
        this.baseView = baseView;
        this.context = baseView.getContext();
    }

    public ResponseSubsriberLambda(BaseDialogManis baseDialogManis) {
        this.baseDialogManis = baseDialogManis;
        this.context = baseDialogManis.getContext();
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof IOException) {
            onNetworkFailed(context.getResources().getString(R.string.error_connection));
        } else {
            onNetworkFailed(context.getResources().getString(R.string.server_busy));
        }
        Log.e(TAG, "Error Exception : " + e.getMessage());
    }

    @Override
    public void onNext(WRR wrr) {
        checkResponseCode(wrr);
        String jsonResponse = new Gson().toJson(wrr);
        Log.d(TAG, "Response JSON API: ".concat(jsonResponse));
    }

    private void checkResponseCode(WRR wrr) {
        if (wrr.getSuccess()) {
            onSuccess(wrr);
        } else {
            tokenValidation(wrr.getMessage().toString());
        }
    }

    private void tokenValidation(String message) {
        String errorTokenIsRequired = "Token is required".trim().toLowerCase();
        String errorTokenExpired = "Field 'session' is required, can't be empty".trim().toLowerCase();
        String errorMessage = message.trim().toLowerCase();
        String errorLoggedOut = getContext().getString(R.string.error_reward_session).trim().toLowerCase();
        if (errorMessage.equals(errorTokenIsRequired) || errorMessage.equals(errorTokenExpired) || errorMessage.equals(errorLoggedOut)) {
            autoLogout(errorLoggedOut);
        } else {
            onErrorFailure(message);
        }
    }

    private void autoLogout(String messageError) {
        if (null != baseActivity) {
            baseActivity.showAlertDialog(baseActivity.getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> {
                        baseActivity.signOut();
                    });
        } else if (null != baseView) {
            baseView.getBaseActivity().showAlertDialog(baseView.getContext().getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> {
                        baseView.getBaseActivity().signOut();
                    });
        } else if (null != baseDialogManis) {
            baseDialogManis.getBaseActivity().showAlertDialog(baseDialogManis.getContext().getString(R.string.error), messageError, false,
                    R.drawable.manis_logo, "Ok", (dialogInterface, i) -> {
                        baseDialogManis.getBaseActivity().signOut();
                    });
        }
    }

    protected void onErrorFailure(String message) {

    }

    protected void onSuccess(WRR wrr) {

    }

    protected void onNetworkFailed(String message) {

    }

    private Context getContext() {
        if (null != baseActivity) {
            return baseActivity;
        } else if (null != baseView) {
            return baseView.getContext();
        } else if (null != baseDialogManis) {
            return baseDialogManis.getContext();
        } else {
            return null;
        }
    }
}
