package com.ebizu.manis.database.litepal;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ebizu.manis.helper.TimeHelper;
import com.ebizu.manis.model.notification.NotificationTableList;
import com.ebizu.manis.model.notification.beacon.BeaconPromo;
import com.ebizu.manis.model.notification.snap.SnapHistory;
import com.google.gson.Gson;

import org.litepal.LitePal;
import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ebizu on 8/28/17.
 */

public class NotificationDatabase {

    private final String TAG = getClass().getSimpleName().concat(" Database");

    public static final String TYPE_BEACON = "type-beacon";
    public static final String TYPE_SNAP = "type-snap";

    private static NotificationDatabase instance;

    public static NotificationDatabase getInstance() {
        if (instance == null)
            instance = new NotificationDatabase();
        return instance;
    }

    public void insert(BeaconPromo beaconPromo) {
        try {
            NotificationTableList notificationTableList = new NotificationTableList();
            notificationTableList.setNotificationId(beaconPromo.getId());
            notificationTableList.setBeaconCompanyId(beaconPromo.getCompanyId());
            notificationTableList.setBeaconPromoDesc(beaconPromo.getDescription());
            notificationTableList.setRead(false);
            notificationTableList.setType(TYPE_BEACON);
            notificationTableList.setSavedTime(TimeHelper.getTimeStamp());
            Log.d(TAG, "notification insert: " + notificationTableList.toString());
            notificationTableList.save();
        } catch (Exception e) {
            Log.e(TAG, "notification insert : " + e.getMessage());
        }
    }

    public void insert(SnapHistory snapHistory) {
        try {
            NotificationTableList notificationTableList = new NotificationTableList();
            notificationTableList.setSnapMessage(snapHistory.getMessage());
            notificationTableList.setSnapStatus(snapHistory.getStatus());
            notificationTableList.setNotificationId(snapHistory.getId());
            notificationTableList.setRead(false);
            notificationTableList.setType(TYPE_SNAP);
            notificationTableList.setSavedTime(TimeHelper.getTimeStamp());
            Log.d(TAG, "notification insert : " + notificationTableList.toString());
            notificationTableList.save();
        } catch (Exception e) {
            Log.e(TAG, "notification insert : " + e.getMessage());
        }
    }

    public void update(NotificationTableList notificationTableList) {
        Log.d(TAG, "notification update id : " + notificationTableList.getNotificationId());
        notificationTableList.update(notificationTableList.getNotificationId());
    }

    public void delete(NotificationTableList notification) {
        Log.d(TAG, "notification delete id : " + notification.getNotificationId());
        DataSupport.deleteAll(NotificationTableList.class, "notificationId = ?", String.valueOf(notification.getNotificationId()));
    }

    public void deleteAll() {
        Log.d(TAG, "notification delete all");
        DataSupport.deleteAll(NotificationTableList.class);
    }

    public List<NotificationTableList> getNotificationList() {
        try {
            List<NotificationTableList> notificationList = DataSupport.findAll(NotificationTableList.class);
            Collections.sort(notificationList, (notificationList1, notificationList2) -> String.valueOf(notificationList2.getSavedTime()).compareTo(
                    String.valueOf(notificationList1.getSavedTime())));
            Log.d(TAG, "notification list : ".concat(new Gson().toJson(notificationList)));
            return notificationList;
        } catch (Exception e) {
            Log.e(TAG, "notification saved : " + e.getMessage());
            return new ArrayList<>();
        }
    }

    public NotificationTableList getNotification(int id) {
        NotificationTableList notificationTableList = new NotificationTableList();
        try {
            for (NotificationTableList notification : getNotificationList()) {
                if (notification.getNotificationId() == id)
                    notificationTableList = notification;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notificationTableList;
    }

    public boolean dataIsExist(BeaconPromo beaconPromo) {
        for (NotificationTableList notification : getNotificationList()) {
            if (notification.getNotificationId() == beaconPromo.getId())
                return true;
        }
        return false;
    }
}