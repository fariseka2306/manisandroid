package com.ebizu.manis.database.litepal;

import android.util.Log;

import com.ebizu.manis.model.tracker.ManisTracker;
import com.ebizu.manis.model.tracker.ManisTrackerTable;
import com.google.gson.Gson;

import org.litepal.crud.DataSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ebizu on 12/13/17.
 */

public class TrackerDatabase {

    private final String TAG = getClass().getSimpleName().concat(" Database");

    private static TrackerDatabase instance;

    private TrackerDatabase() {
    }

    public static TrackerDatabase getInstance() {
        if (instance == null)
            instance = new TrackerDatabase();
        return instance;
    }

    public void insert(ManisTrackerTable manisTrackerTable) {
        try {
            manisTrackerTable.save();
            Log.d(TAG, "insert: ".concat(String.valueOf(manisTrackerTable.isSaved())));
        } catch (Exception e) {
            Log.e(TAG, "insert: ".concat(e.getMessage()));
        }
    }

    public void delete(ManisTrackerTable manisTrackerTable) {
        DataSupport.deleteAll(ManisTrackerTable.class, "id = ?", String.valueOf(manisTrackerTable.getId()));
        Log.d(TAG, "delete: ".concat("accountId = ".concat(manisTrackerTable.getAccountId())));
    }

    public void deleteAll() {
        DataSupport.deleteAll(ManisTrackerTable.class);
        Log.d(TAG, "deleteAll: ");
    }

    public List<ManisTracker> getSendingTrackers() {
        try {
            List<ManisTrackerTable> manisTrackerTables = DataSupport.findAll(ManisTrackerTable.class);
            List<ManisTracker> manisTrackers = new ArrayList<>();
            for (ManisTrackerTable manisTrackerTable : manisTrackerTables) {
                manisTrackers.add(convertToManisTracker(manisTrackerTable));
            }
            Log.d(TAG, "getSendingTrackers: ".concat(new Gson().toJson(manisTrackers)));
            return manisTrackers;
        } catch (Exception e) {
            Log.e(TAG, "getSendingTrackers: ".concat(e.getMessage()));
            return new ArrayList<>();
        }
    }

    public boolean dataIsExist(long manisTrackerId) {
        List<ManisTrackerTable> manisTrackerTables = DataSupport.findAll(ManisTrackerTable.class);
        for (ManisTrackerTable manisTrackerTable : manisTrackerTables) {
            if (manisTrackerId == manisTrackerTable.getId()) {
                Log.d(TAG, "dataIsExist: ".concat(String.valueOf(true)));
                return true;
            }
        }
        Log.d(TAG, "dataIsExist: ".concat(String.valueOf(false)));
        return false;
    }

    public ManisTracker convertToManisTracker(ManisTrackerTable manisTrackerTable) {
        ManisTracker manisTracker = new ManisTracker();
        manisTracker.setAccountId(manisTrackerTable.getAccountId());
        manisTracker.setComponent(manisTrackerTable.getComponent());
        manisTracker.setCurrentPage(manisTrackerTable.getCurrentPage());
        manisTracker.setIpAddress(manisTrackerTable.getIpAddress());
        manisTracker.setLastPage(manisTrackerTable.getLastPage());
        manisTracker.setLocalDatetime(manisTrackerTable.getLocalDatetime());
        manisTracker.setOriginPage(manisTrackerTable.getOriginPage());
        manisTracker.setSubFeature(manisTrackerTable.getSubFeature());
        manisTracker.setIdentifierName(manisTrackerTable.getIdentifierName());
        manisTracker.setIdentifierNumber(manisTrackerTable.getIdentifierNumber());
        manisTracker.setLongitude(manisTrackerTable.getLongitude());
        manisTracker.setLatitude(manisTrackerTable.getLatitude());
        manisTracker.setCountryId(manisTrackerTable.getCountryId());
        Log.d(TAG, "convertToManisTracker: ".concat(new Gson().toJson(manisTracker)));
        return manisTracker;
    }

    public int size() {
        try {
            List<ManisTrackerTable> manisTrackerTables =
                    DataSupport.findAll(ManisTrackerTable.class);
            Log.d(TAG, "sizeData: ".concat(String.valueOf(manisTrackerTables.size())));
            return manisTrackerTables.size();
        } catch (Exception e) {
            Log.e(TAG, "sizeData: ".concat(e.getMessage()));
        }
        return 0;
    }

}
