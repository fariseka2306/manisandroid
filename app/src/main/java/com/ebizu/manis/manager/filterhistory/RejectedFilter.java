package com.ebizu.manis.manager.filterhistory;

import com.ebizu.manis.helper.ConfigManager;

/**
 * Created by Raden on 7/25/17.
 */

public class RejectedFilter implements FilterSpendingBar {
    @Override
    public String name() {
        return "rejected";
    }

    @Override
    public int value() {
        return ConfigManager.SnapViewHistory.STATUS_REJECTED;
    }

    @Override
    public int position() {
        return 2;
    }
}
