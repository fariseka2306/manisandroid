package com.ebizu.manis.manager.accountlistmenusettings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.mvp.account.accountmenulist.settings.notification.NotificationActivity;

/**
 * Created by abizu-alvio on 7/20/2017.
 */

public class SettingsNotification implements AccountListSettings {

    @Override
    public int name() {
        return R.string.st_txt_notifications;
    }

    @Override
    public int thumbnail() {
        return R.drawable.account_settings_notifications;
    }

    @Override
    public int id() {
        return 0;
    }

    @Override
    public void click(Context context) {
        // Do nothing
    }

    @Override
    public void click(Activity activity) {
        Intent intent = new Intent(activity, NotificationActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        new AnalyticManager(activity).trackEvent(
                ConfigManager.Analytic.Category.SETTING_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Notification"
        );
    }
}
