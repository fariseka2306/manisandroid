package com.ebizu.manis.manager.vouchertransactiontype;

import android.content.Context;

import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.rewarddetail.RewardDetailAbstractActivity;
import com.ebizu.manis.view.adapter.rewardvoucher.SingleRewardViewHolder;
import com.ebizu.sdk.reward.models.Reward;

/**
 * Created by FARIS_mac on 22/11/17.
 */

public interface VoucherTransactionType {

    boolean isMatch(RewardVoucher rewardVoucher);

    boolean isMatch(Reward rewardVoucher);

    void setButtonVoucher(RewardDetailAbstractActivity rewardDetailAbstractActivity);

    void setIconVoucher(SingleRewardViewHolder singleRewardViewHolder, Context context);

}
