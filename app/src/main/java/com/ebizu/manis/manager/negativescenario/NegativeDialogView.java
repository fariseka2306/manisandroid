package com.ebizu.manis.manager.negativescenario;

import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;

/**
 * Created by ebizu on 8/28/17.
 */

public class NegativeDialogView implements NegativeInterface {

    @Override
    public NegativeScenarioManager.NegativeView getNegativeView() {
        return NegativeScenarioManager.NegativeView.DIALOG;
    }

    @Override
    public void showNegativeView(String message, BaseActivity baseActivity) {
        baseActivity.showManisAlertDialog(message);
    }

    @Override
    public void showNegativeView(String message, BaseView baseView) {
        baseView.getBaseActivity().showManisAlertDialog(message);
    }
}
