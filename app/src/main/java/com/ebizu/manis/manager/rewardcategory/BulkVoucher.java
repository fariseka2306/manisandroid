package com.ebizu.manis.manager.rewardcategory;

import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.reward.rewardlistcategory.rewardbulk.RewardBulkActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by FARIS_mac on 11/13/17.
 */

public class BulkVoucher implements RewardCategory {

    @Override
    public String categoryName(String categoryName) {
        return ConfigManager.Reward.REWARD_BULK_TITLE;
    }

    @Override
    public String categoryId(String categoryId) {
        return categoryId;
    }

    @Override
    public boolean isMatch(Context context, String categoryName) {
        return categoryName.equals(ConfigManager.Reward.REWARD_BULK_TITLE);
    }

    @Override
    public void click(BaseActivity baseActivity, String categoryName, String categoryId) {
        Intent intent = new Intent(baseActivity, RewardBulkActivity.class);
        intent.putExtra(ConfigManager.Reward.REWARD_BULK_TITLE, categoryName);
        baseActivity.startActivity(intent);
    }

}
