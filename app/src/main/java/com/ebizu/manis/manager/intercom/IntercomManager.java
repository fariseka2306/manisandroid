package com.ebizu.manis.manager.intercom;

import com.ebizu.manis.model.Account;

import java.util.Map;

/**
 * Created by abizu-alvio on 8/11/2017.
 */

public class IntercomManager {

    private static LoginTypeChoices[] loginTypeChoices = {
            new Google(),
            new Facebook(),
            new Phone()
    };

    public static Map<String, String> getIntercom(Account account, String id) {
        for (LoginTypeChoices loginTypeChoicesCheck : loginTypeChoices) {
            if (id == loginTypeChoicesCheck.id()) {
                return loginTypeChoicesCheck.getIntercom(account);
            }
        }
        return new Google().getIntercom(account);
    }
}
