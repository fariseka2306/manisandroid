package com.ebizu.manis.manager.permission;

import android.Manifest;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.IBaseActivity;

/**
 * Created by Ebizu-User on 06/07/2017.
 */

public class PermissionManager {

    private AppCompatActivity activity;
    private IBaseActivity iBaseActivity;
    private static PermissionManager instance;

    public static PermissionManager getInstance(IBaseActivity iBaseActivity) {
        if (null == instance)
            instance = new PermissionManager(iBaseActivity);
        return instance;
    }

    public String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.READ_SMS,
            Manifest.permission.GET_ACCOUNTS
    };

    public String[] PERMISSIONS_CAMERA = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    public String[] PERMISSIONS_SMS = {
            Manifest.permission.READ_SMS
    };

    public String[] PERMISSIONS_APP = {
            AppOpsManager.OPSTR_FINE_LOCATION,
            AppOpsManager.OPSTR_CALL_PHONE,
            AppOpsManager.OPSTR_READ_SMS,
    };

    public String[] PERMISSIONS_CAMERA_APP = {
            AppOpsManager.OPSTR_CAMERA,
            AppOpsManager.OPSTR_READ_EXTERNAL_STORAGE,
            AppOpsManager.OPSTR_WRITE_EXTERNAL_STORAGE,
    };

    public String[] PERMISSIONS_SMS_APP = {
            AppOpsManager.OPSTR_READ_SMS
    };

    public PermissionManager(IBaseActivity iBaseActivity) {
        this.iBaseActivity = iBaseActivity;
        this.activity = iBaseActivity.baseActivity();
    }

    public boolean isValidAllPermissions(int requestCode) {
        if (!hasPermissions(PERMISSIONS)) {
            iBaseActivity.showDialogPermission(requestCode, new IBaseActivity.OnDialogPermissionsListener() {
                @Override
                public void onAllow(int requestCode) {

                }

                @Override
                public void onDeny() {

                }
            });
            return false;
        } else {
            return true;
        }
    }

    public boolean checkPermissionCamera() {
        if (!hasPermissions(getCameraPermissions())) {
            ActivityCompat.requestPermissions(activity, getCameraPermissions(), ConfigManager.Permission.CAMERA_REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }


    public boolean checkPermissionCamera(boolean requestPermission) {
        if (!hasPermissions(getCameraPermissions())) {
            if(requestPermission)
            ActivityCompat.requestPermissions(activity, getCameraPermissions(), ConfigManager.Permission.CAMERA_REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkPermissionSMS() {
        if (!hasPermissions(getSmsPermissions())) {
            ActivityCompat.requestPermissions(activity, getSmsPermissions(), ConfigManager.Otp.FORCE_SIGN_UP_OTP);
            return false;
        } else {
            return true;
        }
    }

    public boolean checkPermissionLocation() {
        if (!hasPermissions(PERMISSIONS_CAMERA)) {
            return false;
        } else {
            return true;
        }
    }

    public void requestPermission() {
        ActivityCompat.requestPermissions(activity, PERMISSIONS, ConfigManager.Permission.ALL_PERMISSION_REQUEST_CODE);
    }

    public void requestPermission(int requestCode) {
        ActivityCompat.requestPermissions(activity, PERMISSIONS, requestCode);
    }

    public void requestPermission(int requestCode, String... permissions) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode);
    }

    public boolean hasPermissions(String... permissions) {
        if (activity != null && permissions != null) {
            if ((android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)) {
                return hasAndroidPermissions(permissions);
            } else if (isXiaomiOSUnderM()) {
                return hasSelfPermissionForXiaomi(permissions);
            }
        }
        return true;
    }

    private boolean hasAndroidPermissions(String... permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private boolean hasSelfPermissionForXiaomi(String... permissions) {
        try {
            for (String permission : permissions) {
                AppOpsManager appOpsManager = (AppOpsManager) iBaseActivity.baseContext().getSystemService(Context.APP_OPS_SERVICE);
                if (!TextUtils.isEmpty(permission)) {
                    int checkOp = appOpsManager.checkOp(permission, Process.myUid(), iBaseActivity.baseContext().getPackageName());
                    return checkOp == AppOpsManager.MODE_ALLOWED && ActivityCompat.checkSelfPermission(iBaseActivity.baseContext(), permission) == PackageManager.PERMISSION_GRANTED;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private String[] getCameraPermissions() {
        if (!isXiaomiOSUnderM()) {
            return PERMISSIONS_CAMERA;
        } else {
            return PERMISSIONS_CAMERA_APP;
        }
    }

    private String[] getSmsPermissions() {
        if (!isXiaomiOSUnderM()) {
            return PERMISSIONS_SMS;
        } else {
            return PERMISSIONS_SMS_APP;
        }
    }

    private boolean isXiaomiOSUnderM() {
        return "Xiaomi".equalsIgnoreCase(Build.MANUFACTURER) && (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M);
    }
}