package com.ebizu.manis.manager.orderhistory;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import com.ebizu.manis.manager.filterhistory.AllFilter;
import com.ebizu.manis.manager.filterhistory.ApprovedFilter;
import com.ebizu.manis.manager.filterhistory.FilterSpendingBar;
import com.ebizu.manis.manager.filterhistory.PendingFilter;
import com.ebizu.manis.manager.filterhistory.RejectedFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raden on 7/25/17.
 */

public class OrderHistoryManager {

    private static OrderSpendingBar[] orderSpendingBars = {
            new DateOrder(),
            new AmountOrder()
    };

    public static SpinnerAdapter getOrderSpinnerAdapter(Context context) {
        List<String> orders = new ArrayList<>();
        for (OrderSpendingBar orderSpendingBar : orderSpendingBars) {
            orders.add(orderSpendingBar.name());
        }
        ArrayAdapter<String> orderAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, orders);
        orderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return orderAdapter;
    }

    public static String getOrderValue(int position) {
        for (OrderSpendingBar orderSpendingBar : orderSpendingBars
                ) {
            if (position == orderSpendingBar.position()) {
                return orderSpendingBar.name();
            }
        }
        return new DateOrder().name();
    }

    public static int getOrderValueInt(int position) {
        for (OrderSpendingBar orderSpendingBar : orderSpendingBars) {
            if (position == orderSpendingBar.position()) {
                return orderSpendingBar.value();
            }
        }
        return new DateOrder().value();
    }
}
