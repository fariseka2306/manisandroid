package com.ebizu.manis.manager.input;

import android.widget.EditText;

import com.ebizu.manis.model.rewardvoucher.Input;
import com.ebizu.manis.preference.ManisSession;

/**
 * Created by FARIS_mac on 21/11/17.
 */

public interface InputType {

    String inputType();

    void setEditTextInput(Input input, EditText editTextInput, ManisSession manisSession);

}
