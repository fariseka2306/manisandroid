package com.ebizu.manis.manager.multiplier;

import android.view.View;

import com.ebizu.manis.service.manis.requestbody.StoreBody;

/**
 * Created by ebizu on 01/08/17.
 */

public interface IMultiplierBehaviour {

    void disableMultiplierClick(View view);

    void checkMultiplierLine(View view);
}
