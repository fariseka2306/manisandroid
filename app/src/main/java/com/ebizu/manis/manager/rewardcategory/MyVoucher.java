package com.ebizu.manis.manager.rewardcategory;

import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.reward.purchasevoucher.myvouchers.MyVouchersActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by FARIS_mac on 11/13/17.
 */

public class MyVoucher implements RewardCategory {

    @Override
    public String categoryName(String categoryName) {
        return ConfigManager.Reward.REWARD_MY_VOUCHER_TITLE;
    }

    @Override
    public String categoryId(String categoryId) {
        return categoryId;
    }

    @Override
    public boolean isMatch(Context context, String categoryName) {
        return categoryName.equals(context.getString(R.string.my_voucher_text));
    }

    @Override
    public void click(BaseActivity baseActivity, String categoryName, String categoryId) {
        Intent intent = new Intent(baseActivity, MyVouchersActivity.class);
        intent.putExtra(ConfigManager.Reward.REWARD_MY_VOUCHER, categoryName);
        baseActivity.startActivity(intent);
    }

}
