package com.ebizu.manis.manager.datespendingbar;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import com.ebizu.manis.model.StatisticBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abizu-alvio on 7/13/2017.
 */

public class DateSpendingManager {

    private DateSpendingManager() {
    }

    private static IDateSpendingBar[] IDateSpendingBars;

    public static SpinnerAdapter getSpendingSpinnerAdapter(Context context) {
        IDateSpendingBars = new IDateSpendingBar[]{
                new ThisMonthIDate(context),
                new LastMonthIDate(context),
                new ThisWeekIDate(context),
                new LastWeekIDate(context)
        };
        List<String> dates = new ArrayList<>();
        for (IDateSpendingBar IDateSpendingBar : IDateSpendingBars) {
            dates.add(IDateSpendingBar.name(context));
        }
        ArrayAdapter dataAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, dates);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return dataAdapter;
    }

    public static StatisticBody getStatisticBody(int index, Context context) {
        IDateSpendingBars = new IDateSpendingBar[]{
                new ThisMonthIDate(context),
                new LastMonthIDate(context),
                new ThisWeekIDate(context),
                new LastWeekIDate(context)
        };
        for (IDateSpendingBar IDateSpendingBarCheck : IDateSpendingBars) {
            if (index == IDateSpendingBarCheck.index()) {
                return IDateSpendingBarCheck.getStatisticBody();
            }
        }
        return new ThisMonthIDate(context).getStatisticBody();
    }
}

