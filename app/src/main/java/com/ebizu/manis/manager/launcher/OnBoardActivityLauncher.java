package com.ebizu.manis.manager.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.mvp.onboard.activity.OnBoardActivity;
import com.ebizu.manis.preference.DeviceSession;
import com.ebizu.manis.preference.ManisSession;

/**
 * Created by ebizu on 8/22/17.
 */

public class OnBoardActivityLauncher implements LauncherPriority {

    @Override
    public boolean notCompleted(Context context) {
        DeviceSession deviceSession = new DeviceSession(context);
        return deviceSession.isFirstIntro();
    }

    @Override
    public boolean notCompleted(Context context, int requestCode) {
        return false;
    }

    @Override
    public Intent getIntent(Activity activity) {

        return new Intent(activity, OnBoardActivity.class);
    }
}
