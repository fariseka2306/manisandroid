package com.ebizu.manis.manager.datespendingbar;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.StatisticBody;

/**
 * Created by abizu-alvio on 7/13/2017.
 */

public class ThisMonthIDate extends
        DateSpendingBar implements IDateSpendingBar {

    private StatisticBody statisticBody;

    public ThisMonthIDate(Context context) {
        super(context);
    }

    @Override
    public int index() {
        return 0;
    }

    @Override
    public String name(Context context) {
        return context.getString(R.string.hm_this_month);
    }

    @Override
    public StatisticBody getStatisticBody() {
        if (statisticBody == null) {
            statisticBody = new StatisticBody();
            statisticBody.setLimit("tm");
        }
        new AnalyticManager(context).trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_HOME,
                ConfigManager.Analytic.Action.ITEM_CLICK,
                "Item This Month"
        );
        return statisticBody;
    }
}
