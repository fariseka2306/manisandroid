package com.ebizu.manis.manager.filterhistory;

import com.ebizu.manis.helper.ConfigManager;

/**
 * Created by Raden on 7/25/17.
 */

public class PendingFilter implements FilterSpendingBar {

    @Override
    public String name() {
        return "pending";
    }

    @Override
    public int value() {
        return ConfigManager.SnapViewHistory.STATUS_PENDING;
    }

    @Override
    public int position() {
        return 3;
    }
}
