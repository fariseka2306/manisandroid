package com.ebizu.manis.manager.accountlistmenusettings;

import android.app.Activity;
import android.content.Context;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public interface AccountListSettings {
    int name();

    int thumbnail();

    int id();

    void click(Context context);

    void click(Activity activity);

}