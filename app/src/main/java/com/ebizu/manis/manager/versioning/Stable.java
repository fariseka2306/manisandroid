package com.ebizu.manis.manager.versioning;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.BuildConfig;
import com.ebizu.manis.model.Versioning;
import com.ebizu.manis.mvp.splashscreen.SplashScreenActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by mac on 8/22/17.
 */

public class Stable implements VersioningStatus {

    SplashScreenActivity splashScreenActivity;

    @Override
    public String status() {
        return "stable";
    }

    @Override
    public void showVersioningStatus(Versioning versioning, Context context, BaseActivity baseActivity) {
        splashScreenActivity = (SplashScreenActivity) baseActivity;
        splashScreenActivity.nextActivity();
    }
}