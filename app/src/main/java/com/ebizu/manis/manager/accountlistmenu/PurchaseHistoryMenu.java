package com.ebizu.manis.manager.accountlistmenu;

import android.content.Intent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.account.accountmenulist.purchasehistory.PurchaseHistoryActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by halim_ebizu on 10/11/17.
 */

public class PurchaseHistoryMenu implements AccountListMenu {

    @Override
    public int name() {
        return R.string.fa_txt_purchase_history;
    }

    @Override
    public int thumbnail() {
        return R.drawable.ic_purchase_history;
    }

    @Override
    public int id() {
        return 1;
    }

    @Override
    public void click(BaseActivity activity) {
        Intent intent = new Intent(activity, PurchaseHistoryActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        activity.getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_ACCOUNT,
                ConfigManager.Analytic.Action.CLICK,
                "Button PurchaseHistoryActivity");
    }

}
