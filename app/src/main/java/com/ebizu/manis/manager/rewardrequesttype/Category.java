package com.ebizu.manis.manager.rewardrequesttype;

import android.content.Context;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.reward.rewardlistcategory.rewardregular.RewardListCategoryView;
import com.ebizu.manis.service.manis.requestbody.RewardCategoryBody;
import com.ebizu.manis.service.reward.requestbody.RewardGeneralListBody;
import com.ebizu.sdk.reward.models.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARIS_mac on 16/11/17.
 */

public class Category implements RewardRequestType {
    @Override
    public String requestRewardType() {
        return ConfigManager.Reward.RequestRewardType.REQUEST_REWARD_TYPE_CATEGORY;
    }

    @Override
    public void setParameterRequest(Context context, RewardListCategoryView rewardCategoryView, String rewardSearchKeyword, String rewardCategoryId) {
//        RewardGeneralListBody.Data data = new RewardGeneralListBody.Data();
//        data.setNoCache("N");
//        data.setShowEmptyStock("Y");
//        ArrayList<String> rewardCategoryIdList = new ArrayList<>();
//        rewardCategoryIdList.add(rewardCategoryId);
//        data.setCategories(rewardCategoryIdList);
//        data.setOrder(1);
//        data.setKeyword(rewardSearchKeyword);
//        data.setPage(ConfigManager.Reward.REWARD_GENERAL_FIRST_PAGE);
//        data.setSize(ConfigManager.Reward.REWARD_GENERAL_SIZE);
//        RewardGeneralListBody requestBody = new RewardGeneralListBody(context, data);
//        rewardCategoryView.getRewardCategoryPresenter().loadRewardVoucherList(requestBody);

        Filter filter = new Filter();
        List<String> filterCategories = new ArrayList<>();
        filterCategories.add(rewardCategoryId);
        filter.setCategories(filterCategories);
        filter.setSorting(0);
        rewardCategoryView.getRewardCategoryPresenter().loadRewardVoucherList(
                new RewardCategoryBody(ConfigManager.Reward.REWARD_CATEGORY_PAGE,
                        ConfigManager.Reward.REWARD_CATEGORY_MAX_PAGE), rewardCategoryId == null ? new Filter() : filter, rewardSearchKeyword);
    }
}
