package com.ebizu.manis.manager.rewardrequesttype;

import android.content.Context;

import com.ebizu.manis.mvp.reward.rewardlistcategory.rewardregular.RewardListCategoryView;

/**
 * Created by FARIS_mac on 16/11/17.
 */

public interface RewardRequestType {

    String requestRewardType();

    void setParameterRequest(Context context, RewardListCategoryView rewardCategoryView, String rewardSearchKeyword, String rewardCategoryId);

}
