package com.ebizu.manis.manager.intercom;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.LoginChoices;
import com.ebizu.manis.model.Account;

import java.util.HashMap;
import java.util.Map;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;

/**
 * Created by abizu-alvio on 8/11/2017.
 */

public class Facebook implements LoginTypeChoices {

    LoginChoices loginChoices;

    @Override
    public String id() {
        loginChoices = LoginChoices.FACEBOOK;
        return String.valueOf(loginChoices);
    }

    @Override
    public Map<String, String> getIntercom(Account account) {
        Intercom client = Intercom.client();
        Map<String, String> userMap = new HashMap<>();
        userMap.put("name", account.getAccScreenName());
        userMap.put(ConfigManager.Intercom.FACEBOOK_EMAIL, account.getAccFacebookEmail());
        userMap.put(ConfigManager.Intercom.FACEBOOK_ID, String.valueOf(account.getAccFacebookId()));
        UserAttributes userAttributes = new UserAttributes.Builder()
                .withName(account.getAccScreenName())
                .withEmail(account.getAccFacebookEmail())
                .build();
        client.updateUser(userAttributes);
        return userMap;
    }
}
