package com.ebizu.manis.manager.datespendingbar;

import android.content.Context;

import com.ebizu.manis.model.StatisticBody;

/**
 * Created by abizu-alvio on 7/13/2017.
 */

public interface IDateSpendingBar {

    int index();

    String name(Context context);

    StatisticBody getStatisticBody();


}
