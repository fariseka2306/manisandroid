package com.ebizu.manis.manager.login;

import com.ebizu.manis.R;
import com.ebizu.manis.mvp.login.ILoginActivityPresenter;

/**
 * Created by Ebizu-User on 12/07/2017.
 */

public class LoginManager {

    private static LoginType[] loginTypes = {
            new PhoneLogin(),
            new FacebookLogin(),
            new GoogleSignInLogin()
    };

    public static void login(ILoginActivityPresenter iLoginPresenter, LoginType loginType, String regIdGCM) {
        for (LoginType checkLoginType : loginTypes) {
            if (checkLoginType.id() == loginType.id()) {
                checkLoginType.login(iLoginPresenter, regIdGCM);
            }
        }
    }

    public static void login(ILoginActivityPresenter iLoginPresenter, int requestCode, String regIdGCM) {
        for (LoginType checkLoginType : loginTypes) {
            if (checkLoginType.requestCode() == requestCode) {
                checkLoginType.login(iLoginPresenter, regIdGCM);
            }
        }
    }

    public static String getMessageDeny(ILoginActivityPresenter loginPresenter, int requestCode) {
        String message = loginPresenter.getContext().getString(R.string.permission_grant);
        for (LoginType checkLoginType : loginTypes) {
            if (checkLoginType.requestCode() == requestCode) {
                message = message.concat(" " + checkLoginType.message()).concat(" permission to run");
            }
        }
        return message;
    }

    public static String[] getPermissions(int requestCode) {
        for (LoginType checkLoginType : loginTypes) {
            if (checkLoginType.requestCode() == requestCode) {
                return checkLoginType.permissions();
            }
        }
        return new GoogleSignInLogin().permissions();
    }

    public static String getPermissionRequires(ILoginActivityPresenter iLoginPresenter, int requestCode){
        for (LoginType checkLoginType : loginTypes) {
            if (checkLoginType.requestCode() == requestCode) {
                return checkLoginType.requirePermission(iLoginPresenter);
            }
        }
        return new GoogleSignInLogin().requirePermission(iLoginPresenter);
    }
}
