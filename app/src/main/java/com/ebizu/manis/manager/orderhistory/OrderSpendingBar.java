package com.ebizu.manis.manager.orderhistory;

/**
 * Created by Raden on 7/25/17.
 */

public interface OrderSpendingBar {

    String name();

    int value();

    int position();
}
