package com.ebizu.manis.manager.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.preference.LocationSession;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * Created by Ebizu-User on 31/07/2017.
 */

public class ManisLocationManager implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private final String TAG = getClass().getSimpleName();

    private Context context;
    private LocationSession locationSession;
    private GoogleApiClient googleApiClient;

    public ManisLocationManager(Context context) {
        this.context = context;
        this.locationSession = new LocationSession(context);
        this.googleApiClient = new GoogleApiClient.Builder(context, this, this).addApi(LocationServices.API).build();
    }

    public void updateLocation() {
        this.googleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            try {
                Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                lastLocation.getLatitude();
                lastLocation.getLongitude();
                locationSession.setSession(lastLocation);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.googleApiClient.disconnect();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public LocationSession getLocationSession() {
        return locationSession;
    }
}