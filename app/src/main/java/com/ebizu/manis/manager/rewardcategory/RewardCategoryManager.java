package com.ebizu.manis.manager.rewardcategory;

import android.content.Context;

import com.ebizu.manis.root.BaseActivity;

/**
 * Created by FARIS_mac on 11/13/17.
 */

public class RewardCategoryManager {

    private static RewardCategory[] rewardCategories = {
            new RegularVoucher(),
            new MyVoucher(),
            new BulkVoucher()
    };

    public static void onRewardClick(Context context, BaseActivity baseActivity, String categoryName, String categoryId) {
        for (RewardCategory rewardCategoryCheck : rewardCategories) {
            if (rewardCategoryCheck.isMatch(context, categoryName)) {
                rewardCategoryCheck.click(baseActivity, categoryName, categoryId);
                return;
            }
        }
    }
}
