package com.ebizu.manis.manager.facebook;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.UtilManis;
import com.ebizu.manis.mvp.login.ILoginActivity;
import com.ebizu.manis.mvp.login.ILoginActivityPresenter;
import com.ebizu.manis.mvp.mission.shareexperience.ISocialMedia;
import com.ebizu.manis.mvp.mission.shareexperience.ShareExperienceDetailActivity;
import com.ebizu.manis.sdk.rest.data.AuthSignIn;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by Ebizu-User on 12/07/2017.
 */

public class FacebookManager {

    private final String TAG = getClass().getSimpleName();
    public int friendsCount;
    private ILoginActivity iLoginView;
    private ILoginActivityPresenter iLoginPresenter;
    private ShareExperienceDetailActivity iShareXView;
    private ISocialMedia iShareXPresenter;
    private CallbackManager callbackManagerFacebook;

    public FacebookManager(ShareExperienceDetailActivity iMissionDetailView, ISocialMedia iMissionPresenter) {
        this.iShareXView = iMissionDetailView;
        this.iShareXPresenter = iMissionPresenter;
    }

    public FacebookManager(ILoginActivity iLoginView, ILoginActivityPresenter iLoginPresenter) {
        this.iLoginView = iLoginView;
        this.iLoginPresenter = iLoginPresenter;
    }

    public void startLoginFacebook(boolean isMission) {
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().registerCallback(getCallbackManagerFacebook(), new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                if (!isMission) loginToManis(loginResult);
                else iShareXPresenter.takePhoto();
            }

            @Override
            public void onCancel() {
                //Do nothing
            }

            @Override
            public void onError(FacebookException error) {
                dismissProgressBarDialogLogin();
                if (getActivity() != null) {
                    UtilManis.info(getActivity(), getActivity().getString(R.string.error),
                            getActivity().getString(R.string.error_facebook));
                }
                Log.e(TAG, "onError: " + error.getMessage());
            }
        });
        LoginManager.getInstance().logInWithReadPermissions(getActivity(),
                Arrays.asList(ConfigManager.FacebookParam.EMAIL,
                        ConfigManager.FacebookParam.USER_BIRTHDAY,
                        ConfigManager.FacebookParam.USER_LOCATION));
    }

    public CallbackManager getCallbackManagerFacebook() {
        if (callbackManagerFacebook == null) {
            callbackManagerFacebook = CallbackManager.Factory.create();
            FacebookSdk.sdkInitialize(getActivity());
            LoginManager.getInstance().logOut();
        }
        return callbackManagerFacebook;
    }

    private AppCompatActivity getActivity() {
        if (iLoginView != null) {
            return iLoginView.baseActivity();
        } else if (iShareXView != null) {
            return  iShareXView.baseActivity();
        }
        return  null;
    }

    private void loginToManis(LoginResult loginResult) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), (object, response) -> {
            AuthSignIn.RequestBody.Facebook facebook = null;
            try {
                facebook = new AuthSignIn.RequestBody.Facebook(
                        object.getString("id"),
                        object.getString("name"),
                        object.has("email") ? object.getString("email") : "",
                        object.has("gender") ? object.getString("gender") : "",
                        object.has("birthday") ? object.getString("birthday") : "",
                        object.has("verified") && object.getBoolean("verified"));
                iLoginPresenter.loginAccountManis(facebook);
            } catch (Exception e) {
                dismissProgressBarDialogLogin();
                Log.e(TAG, "loginFacebook: " + e.getMessage());
                if (getActivity() != null) {
                    UtilManis.info(getActivity(), iLoginPresenter.getContext().getString(R.string.error), iLoginPresenter.getContext().getString(R.string.error_server));
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday,link,location,locale,timezone,updated_time,verified");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    public void setFriendCount() {
        GraphRequest graphRequestAsyncTask = new GraphRequest(
                AccessToken.getCurrentAccessToken(), "/me/friends", null, HttpMethod.GET, response -> {
            try {
                JSONObject rawSummary = response.getJSONObject().getJSONObject("summary");
                friendsCount = (int) rawSummary.get("total_count");
                iShareXPresenter.checkFollowerCount(friendsCount);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        graphRequestAsyncTask.executeAsync();
    }

    private void dismissProgressBarDialogLogin() {
        if (null != iLoginView)
            iLoginView.dismissProgressBarDialog();
    }
}