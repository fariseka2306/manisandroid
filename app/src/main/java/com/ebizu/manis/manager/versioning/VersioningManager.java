package com.ebizu.manis.manager.versioning;

import android.content.Context;

import com.ebizu.manis.model.Versioning;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by mac on 8/22/17.
 */

public class VersioningManager {

    public static VersioningStatus[] versioningStatuses = {
            new Obsolete(),
            new Optional(),
            new Stable()
    };

    public static void getVersioningStatus(Versioning versioning, Context context, BaseActivity baseActivity) {
        for (VersioningStatus versioningStatus : versioningStatuses) {
            if (versioning.getVersionStatus().equals(versioningStatus.status())) {
                versioningStatus.showVersioningStatus(versioning, context, baseActivity);
            }
        }
    }
}
