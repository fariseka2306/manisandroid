package com.ebizu.manis.manager.categorycolor;

import android.content.Context;

/**
 * Created by abizu-alvio on 7/18/2017.
 */

public class CategoryColorManager {

    private CategoryColorManager() {
    }

    private static CategoryColor[] categoryColors = {
            new CategoryColorRed(),
            new CategoryColorGreen(),
            new CategoryColorYellow(),
            new CategoryColorBlue()
    };

    public static int getColor(Context context, int index) {
        if (index == categoryColors[index].index()) {
            return categoryColors[index].color(context);
        } else {
            return new CategoryColorRed().color(context);
        }
    }
}
