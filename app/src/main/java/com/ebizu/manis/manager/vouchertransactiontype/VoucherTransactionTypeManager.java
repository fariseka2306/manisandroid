package com.ebizu.manis.manager.vouchertransactiontype;

import android.content.Context;

import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.rewarddetail.RewardDetailAbstractActivity;
import com.ebizu.manis.view.adapter.rewardvoucher.SingleRewardViewHolder;
import com.ebizu.sdk.reward.models.Reward;

/**
 * Created by FARIS_mac on 22/11/17.
 */

public class VoucherTransactionTypeManager {

    public static VoucherTransactionType[] voucherTransactionTypes = {
            new Free(),
            new Redeemable(),
            new Purchaseable(),
            new Both(),
            new Else()
    };

    public static void checkDetailVoucherTransactionType(RewardVoucher rewardVoucher, RewardDetailAbstractActivity rewardDetailAbstractActivity) {
        for (VoucherTransactionType voucherTransactionType : voucherTransactionTypes) {
            if (voucherTransactionType.isMatch(rewardVoucher)) {
                voucherTransactionType.setButtonVoucher(rewardDetailAbstractActivity);
                return;
            }
        }
    }

    public static void checkCategoryVoucherTransactionType(SingleRewardViewHolder singleRewardViewHolder, Reward reward, Context context) {
        for (VoucherTransactionType voucherTransactionType : voucherTransactionTypes) {
            if (voucherTransactionType.isMatch(reward)) {
                voucherTransactionType.setIconVoucher(singleRewardViewHolder, context);
                return;
            }
        }
    }

}
