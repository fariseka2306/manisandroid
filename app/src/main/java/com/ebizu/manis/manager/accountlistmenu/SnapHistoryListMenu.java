package com.ebizu.manis.manager.accountlistmenu;

import android.content.Intent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.viewhistory.SnapViewHistoryActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class SnapHistoryListMenu implements AccountListMenu {
    @Override
    public int name() {
        return R.string.fa_txt_snap;
    }

    @Override
    public int thumbnail() {
        return R.drawable.account_snap_history;
    }

    @Override
    public int id() {
        return 2;
    }

    @Override
    public void click(BaseActivity activity) {
        Intent intent = new Intent(activity, SnapViewHistoryActivity.class);
        activity.startActivity(intent);
        activity.getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_ACCOUNT,
                ConfigManager.Analytic.Action.CLICK,
                "Button Snap History");
    }

}
