package com.ebizu.manis.manager.tracker;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.format.DateUtils;
import android.util.Log;

import com.ebizu.manis.database.litepal.TrackerDatabase;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.TrackerHelper;
import com.ebizu.manis.model.tracker.ManisTrackerTable;
import com.ebizu.manis.model.tracker.TrackerStandartRequest;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.service.manis.ManisApiGenerator;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.ManisTrackerRequestBody;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ebizu on 12/13/17.
 */

public class TrackerManager {

    private static final String TAG = "TrackerManager";
    private static TrackerManager instance;

    private Context context;
    private ManisSession manisSession;
    private TrackerDatabase trackerDatabase;
    private SharedPreferences preferencesManis;

    private final int MIN_POST_DATA_TRACKER = 25;

    public static TrackerManager initialize(Application application) {
        if (null == instance) {
            instance = new TrackerManager(application);
        }
        return instance;
    }

    private TrackerManager(Context context) {
        this.context = context;
        this.manisSession = ManisSession.getInstance(context);
        this.preferencesManis = context.getSharedPreferences(
                ConfigManager.Preference.KEY, Context.MODE_PRIVATE);
        this.trackerDatabase = TrackerDatabase.getInstance();
    }

    public static TrackerManager getInstance() {
        try {
            instance.isLastUpdateToday();
        } catch (Exception e) {
            Log.e(TAG, "Please initialize before using this manager class");
        }
        return instance;
    }

    public void setTrackerData(TrackerStandartRequest trackerData) {
        saveToStorage(trackerData);
        if (isSendingTime()) {
            sendBatchTracker();
        }
    }

    private void saveToStorage(TrackerStandartRequest trackerStandartRequest) {
        ManisTrackerTable manisTrackerTable = TrackerHelper.getInstance(context).generateManisTrackerTable(trackerStandartRequest, preferencesManis);
        trackerDatabase.insert(manisTrackerTable);
        setLastUpdate(getTime());
        Log.d(TAG, "saveToStorage: ".concat(manisTrackerTable.toString()));
    }

    private void sendBatchTracker() {
        ManisTrackerRequestBody manisTrackerRequestBody = new ManisTrackerRequestBody();
        manisTrackerRequestBody.setManisTrackers(trackerDatabase.getSendingTrackers());
        ManisApiGenerator.createServiceTracker(context).postingTracker(manisTrackerRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseData -> Log.d(TAG, "response sendBatchTracker: ".concat(new Gson().toJson(responseData))),
                        throwable -> Log.e(TAG, "response sendBatchTracker: ".concat(throwable.getMessage())));
        trackerDatabase.deleteAll();
        Log.d(TAG, "sendBatchTracker: ".concat(new Gson().toJson(manisTrackerRequestBody)));
    }

    private boolean isSendingTime() {
        return trackerDatabase.size() >= MIN_POST_DATA_TRACKER || !isLastUpdateToday();
    }

    private String getLocalDatetime() {
        String pattern = "yyyy-MM-dd HH:mm";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("id", "ID"));
        return simpleDateFormat.format(new Date(getTime()));
    }

    private boolean isLastUpdateToday() {
        return DateUtils.isToday(getLastUpdate());
    }

    private long getTime() {
        return Calendar.getInstance().getTime().getTime();
    }

    private long getLastUpdate() {
        return preferencesManis.getLong(ConfigManager.Tracker.PREF_LAST_UPDATE, getTime());
    }

    private void setLastUpdate(long lastUpdate) {
        SharedPreferences.Editor editor = preferencesManis.edit();
        editor.putLong(ConfigManager.Tracker.PREF_LAST_UPDATE, lastUpdate);
        editor.commit();
    }
}
