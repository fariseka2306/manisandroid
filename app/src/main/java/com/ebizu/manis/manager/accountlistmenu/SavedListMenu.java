package com.ebizu.manis.manager.accountlistmenu;

import android.app.Activity;
import android.content.Intent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.account.accountmenulist.saved.SavedActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class SavedListMenu implements AccountListMenu {

    @Override
    public int name() {
        return R.string.fa_txt_saved;
    }

    @Override
    public int thumbnail() {
        return R.drawable.account_saved;
    }

    @Override
    public int id() {
        return 0;
    }

    @Override
    public void click(BaseActivity activity) {
        Intent intent = new Intent(activity, SavedActivity.class);
        activity.startActivityForResult(intent, ConfigManager.Saved.SAVED_REQUEST_CODE);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        activity.getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_ACCOUNT,
                ConfigManager.Analytic.Action.CLICK,
                "Button SavedActivity");
    }
}
