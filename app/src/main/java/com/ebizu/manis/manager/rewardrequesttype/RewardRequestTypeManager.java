package com.ebizu.manis.manager.rewardrequesttype;

import android.content.Context;

import com.ebizu.manis.mvp.reward.rewardlistcategory.rewardregular.RewardListCategoryView;

/**
 * Created by FARIS_mac on 16/11/17.
 */

public class RewardRequestTypeManager {

    private static RewardRequestType[] rewardRequestTypes = {
            new Home(),
            new Search(),
            new Category()
    };

    public static void getRequestType(Context context, RewardListCategoryView rewardCategoryView, String requestRewardType, String rewardSearchKeyword, String rewardCategoryId) {
        for (RewardRequestType rewardRequestType : rewardRequestTypes) {
            if (requestRewardType.equals(rewardRequestType.requestRewardType())) {
                rewardRequestType.setParameterRequest(context, rewardCategoryView, rewardSearchKeyword, rewardCategoryId);
            }
        }
    }

}
