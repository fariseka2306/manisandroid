package com.ebizu.manis.manager.orderhistory;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.orderhistory.OrderSpendingBar;

/**
 * Created by Raden on 7/25/17.
 */

public class AmountOrder implements OrderSpendingBar {
    @Override
    public String name() {
        return "amount";
    }

    @Override
    public int value() {
        return ConfigManager.SnapViewHistory.ORDER_AMOUNT;
    }

    @Override
    public int position() {
        return 1;
    }
}
