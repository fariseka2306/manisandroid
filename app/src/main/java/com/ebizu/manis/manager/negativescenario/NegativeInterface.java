package com.ebizu.manis.manager.negativescenario;

import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;

/**
 * Created by ebizu on 8/28/17.
 */

public interface NegativeInterface {

    NegativeScenarioManager.NegativeView getNegativeView();

    void showNegativeView(String message, BaseActivity baseActivity);

    void showNegativeView(String message, BaseView baseView);

}
