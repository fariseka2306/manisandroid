package com.ebizu.manis.manager.login;

import android.Manifest;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.login.ILoginActivityPresenter;

/**
 * Created by Ebizu-User on 12/07/2017.
 */

public class FacebookLogin implements LoginType {
    @Override
    public int id() {
        return R.id.lg_btn_facebook;
    }

    @Override
    public int requestCode() {
        return ConfigManager.Permission.FACEBOOK_REQUEST_CODE;
    }

    @Override
    public void login(ILoginActivityPresenter iLoginPresenter, String regIdGCM) {
        iLoginPresenter.loginByFacebook(regIdGCM);
    }

    @Override
    public String requirePermission(ILoginActivityPresenter loginActivityPresenter) {
        return loginActivityPresenter.getContext().getResources().getString(R.string.permission_must_fb);
    }

    @Override
    public String[] permissions() {
        String[] PERMISSIONS_FACEBOOK = {
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CALL_PHONE,
        };
        return PERMISSIONS_FACEBOOK;
    }

    @Override
    public String message() {
        return "location and phone";
    }
}
