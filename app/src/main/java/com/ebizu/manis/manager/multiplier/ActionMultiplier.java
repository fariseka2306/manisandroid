package com.ebizu.manis.manager.multiplier;

import android.view.View;
import android.widget.LinearLayout;

import com.ebizu.manis.service.manis.requestbody.StoreBody;

/**
 * Created by firef on 7/13/2017.
 */

public interface ActionMultiplier extends IMultiplierBehaviour {

    int id();

    StoreBody getStoreBody();

    int multiplier();

    int drawable();

    int drawable(String merchantTier);

    String text();

    String text(String merchantTier);

    String merchantTier();

    View bottomLineView(LinearLayout viewGroupMultiplier);
}
