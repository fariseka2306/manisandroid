package com.ebizu.manis.manager.accountlistmenusettings;

import android.app.Activity;
import android.content.Context;

import com.ebizu.manis.view.adapter.account.AccountListMenuSettingsAdapter;

/**
 * Created by abizu-alvio on 7/20/2017.
 */

public class AccountListMenuSettingsManager {

    private AccountListMenuSettingsManager() {

    }

    private static AccountListSettings[] accountListSettings = {
            new SettingsNotification(),
            new SettingsRateThisApp(),
            new SettingsAboutThisApp()
    };

    public static void addAccountListMenu(AccountListMenuSettingsAdapter accountListMenuSettingsAdapter) {
        for (AccountListSettings accountList : accountListSettings) {
            accountListMenuSettingsAdapter.addAccountList(accountList);
        }
    }

    public static void onClickMenu(Context context, AccountListSettings accountListMenu) {
        for (AccountListSettings accountListMenuCheck : accountListSettings) {
            if (accountListMenu.id() == accountListMenuCheck.id()) {
                accountListMenuCheck.click(context);
            }
        }
    }

    public static void onClickMenu(Activity activity, AccountListSettings accountListMenu) {
        for (AccountListSettings accountListMenuCheck : accountListSettings) {
            if (accountListMenu.id() == accountListMenuCheck.id()) {
                accountListMenuCheck.click(activity);
            }
        }
    }
}