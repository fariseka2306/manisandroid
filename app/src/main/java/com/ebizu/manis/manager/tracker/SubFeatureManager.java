package com.ebizu.manis.manager.tracker;

/**
 * Created by ebizu on 12/15/17.
 */

public class SubFeatureManager {

    private static SubFeatureManager instance;

    private static String subFeature = "";

    private SubFeatureManager() {

    }

    public static SubFeatureManager getInstance() {
        if (null == instance)
            instance = new SubFeatureManager();
        return instance;
    }

    public static void reset() {
        subFeature = "";
    }

    public static String getSubFeature() {
        return subFeature;
    }

    public static void setSubFeature(String subFeature) {
        SubFeatureManager.subFeature = subFeature;
    }
}
