package com.ebizu.manis.manager.twitter;

import android.widget.Toast;

import com.ebizu.manis.mvp.mission.shareexperience.ISocialMedia;
import com.ebizu.manis.mvp.mission.shareexperience.ShareExperienceDetailActivity;
import com.ebizu.manis.service.manis.twitter.ManisTwitterApiClient;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

public class TwitterManager {

    private final String TAG = getClass().getSimpleName();

    private TwitterAuthClient twitterAuthClient;
    private ShareExperienceDetailActivity iShareXView;
    private ISocialMedia iSocialMediaPresenter;

    public TwitterManager(ShareExperienceDetailActivity iMissionDetailView, ISocialMedia iSocialMediaPresenter) {
        this.iShareXView = iMissionDetailView;
        this.iSocialMediaPresenter = iSocialMediaPresenter;
        twitterAuthClient = new TwitterAuthClient();
    }

    public void connectTwitter() {
        final TwitterSession activeSession = TwitterCore.getInstance()
                .getSessionManager().getActiveSession();
        if (activeSession == null) {
            twitterAuthClient.authorize(iShareXView, new Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> twitterSessionResult) {
                    TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                    getFollowerCount(session);
                }

                @Override
                public void failure(TwitterException e) {
                    Toast.makeText(iShareXView, "cannot connect to twitter", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            getFollowerCount(activeSession);
        }
    }

    public TwitterAuthClient getTwitterAuthClient() {
        return twitterAuthClient;
    }

    private void getFollowerCount(TwitterSession session) {
        ManisTwitterApiClient apiclients = new ManisTwitterApiClient(session);
        apiclients.getCustomService().show(session.getId()).enqueue(new Callback<User>() {
            @Override
            public void success(Result<User> result) {
                iSocialMediaPresenter.checkFollowerCount(result.data.followersCount);
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(iShareXView, "failed to get total follower", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
