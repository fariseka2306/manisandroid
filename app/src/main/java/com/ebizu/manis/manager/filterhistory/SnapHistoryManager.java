package com.ebizu.manis.manager.filterhistory;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raden on 7/25/17.
 */

public class SnapHistoryManager {

    private SnapHistoryManager() {
    }

    private static FilterSpendingBar[] filterSpendingBars = {
            new AllFilter(),
            new ApprovedFilter(),
            new RejectedFilter(),
            new PendingFilter()
    };

    public static SpinnerAdapter getFilterSpinnerAdapter(Context context){
        List<String> filters = new ArrayList<>();
        for (FilterSpendingBar filterSpendingBar: filterSpendingBars
             ) {
            filters.add(filterSpendingBar.name());
        }

        ArrayAdapter<String> filterAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, filters);
        filterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return filterAdapter;
    }

    public static String getFilterValue(int position){
        for (FilterSpendingBar filterSpendingBar: filterSpendingBars
             ) {
            if (position == filterSpendingBar.position()){
                return filterSpendingBar.name();
            }
        }
        return new AllFilter().name();
    }

    public static int getFilterValueInt(int position) {
        for (FilterSpendingBar filterSpendingBar:filterSpendingBars) {
            if (position == filterSpendingBar.position()) {
                return filterSpendingBar.value();
            }
        }
        return new AllFilter().value();
    }
}
