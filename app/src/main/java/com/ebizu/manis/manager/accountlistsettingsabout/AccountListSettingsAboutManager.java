package com.ebizu.manis.manager.accountlistsettingsabout;

import android.content.Context;

import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.adapter.account.AccountListSettingAboutAdapter;

/**
 * Created by abizu-alvio on 7/21/2017.
 */

public class AccountListSettingsAboutManager {

    private AccountListSettingsAboutManager() {
    }

    private static AccountListSettingsAbout[] accountListSettingsAbouts = {
            new AboutTerm(),
            new AboutPrivacy(),
            new AboutVersion(),
            new AboutBuild()
    };

    public static void addAccountListMenu(AccountListSettingAboutAdapter accountListSettingAbout) {
        for (AccountListSettingsAbout accountListSettingsAbout : accountListSettingsAbouts) {
            accountListSettingAbout.addAccountList(accountListSettingsAbout);
        }
    }

    public static void onClickMenu(Context context, AccountListSettingsAbout accountListSettingsAbout) {
        for (AccountListSettingsAbout accountListSettingCheck : accountListSettingsAbouts) {
            if (accountListSettingsAbout.id() == accountListSettingCheck.id()) {
                accountListSettingCheck.click(context);
            }
        }
    }

    public static void onClickMenu(BaseActivity baseActivity, AccountListSettingsAbout accountListSettingsAbout) {
        for (AccountListSettingsAbout accountListSettingCheck : accountListSettingsAbouts) {
            if (accountListSettingsAbout.id() == accountListSettingCheck.id()) {
                accountListSettingCheck.click(baseActivity);
            }
        }
    }

}
