package com.ebizu.manis.manager.multiplier;

import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.manis.requestbody.StoreBody;

import butterknife.BindView;

/**
 * Created by firef on 7/13/2017.
 */

public class MultiplierAll extends MultiplierBehaviour implements ActionMultiplier {

    @BindView(R.id.ll_tab_multiplier)
    LinearLayout llTabMultiplier;

    @Override
    public int id() {
        return R.id.item_tab_store_multiplier_all;
    }

    @Override
    public StoreBody getStoreBody() {
        StoreBody storeBody = new StoreBody(ConfigManager.Store.FIRST_PAGE,
                ConfigManager.Store.MAX_PAGE_SIZE, ConfigManager.Store.MULTIPLIER_ALL
                , ConfigManager.Store.MULTIPLIER_TIER_ALL);
        return storeBody;
    }

    @Override
    public int multiplier() {
        return -1;
    }

    @Override
    public int drawable() {
        return Color.TRANSPARENT;
    }

    @Override
    public int drawable(String merchantTier) {
        if (merchantTier.equalsIgnoreCase(ConfigManager.Store.MULTIPLIER_TIER_LONGTAIL)) {
            return R.drawable.ticket_icon;
        } else {
            return 0;
        }
    }

    @Override
    public String text() {
        return "All";
    }

    @Override
    public String text(String merchantTier) {
        return merchantTier.equalsIgnoreCase(ConfigManager.Store.MULTIPLIER_TIER_LONGTAIL) ? "" : "All";
    }

    @Override
    public String merchantTier() {
        return ConfigManager.Store.MULTIPLIER_TIER_ALL;
    }

    @Override
    public View bottomLineView(LinearLayout viewGroupMultiplier) {
        return (View) viewGroupMultiplier.findViewById(R.id.multiplier_line_all);
    }
}