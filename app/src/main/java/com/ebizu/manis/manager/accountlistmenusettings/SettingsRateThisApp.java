package com.ebizu.manis.manager.accountlistmenusettings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.ebizu.manis.BuildConfig;
import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;

/**
 * Created by abizu-alvio on 7/20/2017.
 */

public class SettingsRateThisApp implements AccountListSettings {

    @Override
    public int name() {
        return R.string.st_txt_rate;
    }

    @Override
    public int thumbnail() {
        return R.drawable.account_settings_rate_app;
    }

    @Override
    public int id() {
        return 1;
    }

    @Override
    public void click(Context context) {
        //Do nothing
    }

    @Override
    public void click(Activity activity) {
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(activity.getString(R.string.market_id) + BuildConfig.APPLICATION_ID)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(activity.getString(R.string.playstore_id) + BuildConfig.APPLICATION_ID)));
        }
        new AnalyticManager(activity).trackEvent(
                ConfigManager.Analytic.Category.SETTING_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK,
                "Button Rate"
        );
    }
}