package com.ebizu.manis.manager.accountlistmenu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.widget.Toast;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.BuildConfig;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.helper.UtilSessionFirstIn;
import com.ebizu.manis.helper.UtilStatic;
import com.ebizu.manis.model.InviteTerm;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.dialog.inviteterm.InviteTermDialog;
import com.ebizu.manis.view.dialog.term.TermDialog;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class InviteSnapListMenu implements AccountListMenu {
    @Override
    public int name() {
        return R.string.fa_txt_invite;
    }

    @Override
    public int thumbnail() {
        return R.drawable.account_invite_friends;
    }

    @Override
    public int id() {
        return 8;
    }

    @Override
    public void click(BaseActivity baseActivity) {
        if (!ConnectionDetector.isNetworkConnected(baseActivity)) {
            baseActivity.showAlertDialog(baseActivity.getString(R.string.error),
                    "Can't Load Content.\n" +
                            "Check Your internet connection",
                    false,
                    R.drawable.manis_logo,
                    "OK", (dialog, which) -> {
                        dialog.dismiss();
                    });
        } else {
            SharedPreferences prefFirst = baseActivity.getSharedPreferences(UtilStatic.FIRSTIN_PREF, Context.MODE_PRIVATE);
            if (UtilSessionFirstIn.isShowInviteTerms(prefFirst)) {
                InviteTermDialog inviteTermDialog = new InviteTermDialog(baseActivity);
                inviteTermDialog.setActivity(baseActivity);
                inviteTermDialog.show();
                inviteTermDialog.getInviteTermPresenter().loadInviteTerm(baseActivity.getApplicationContext());
            } else {
                InviteTermDialog inviteTermDialog = new InviteTermDialog(baseActivity);
                inviteTermDialog.setActivity(baseActivity);
                inviteTermDialog.getInviteTermPresenter().loadInviteTerm(baseActivity.getApplicationContext());
                inviteTermDialog.generateBranch();
            }
        }
        baseActivity.getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_ACCOUNT,
                ConfigManager.Analytic.Action.CLICK,
                "Button Invite Snap");
    }

}
