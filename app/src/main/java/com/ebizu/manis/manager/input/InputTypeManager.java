package com.ebizu.manis.manager.input;

import android.widget.EditText;

import com.ebizu.manis.model.rewardvoucher.Input;
import com.ebizu.manis.preference.ManisSession;

/**
 * Created by FARIS_mac on 21/11/17.
 */

public class InputTypeManager {

    public static InputType[] inputTypes = {
            new City(),
            new Mobile(),
            new PostCode(),
            new ShippingAddress(),
            new State(),
            new PhoneNumber()
    };

    public static void checkInputType(Input input, EditText editTextInput, ManisSession manisSession) {
        for (InputType inputType : inputTypes) {
            if (input.getText().equals(inputType.inputType())) {
                inputType.setEditTextInput(input, editTextInput, manisSession);
            }
        }
    }

}
