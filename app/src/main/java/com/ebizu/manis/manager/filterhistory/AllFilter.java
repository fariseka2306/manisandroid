package com.ebizu.manis.manager.filterhistory;

import com.ebizu.manis.helper.ConfigManager;

/**
 * Created by Raden on 7/25/17.
 */

public class AllFilter implements FilterSpendingBar {
    @Override
    public String name() {
        return "all";
    }

    @Override
    public int value() {
        return ConfigManager.SnapViewHistory.STATUS_ALL;
    }

    @Override
    public int position() {
        return 0;
    }
}
