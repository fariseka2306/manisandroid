package com.ebizu.manis.manager.login;

import android.Manifest;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.login.ILoginActivityPresenter;

/**
 * Created by Ebizu-User on 12/07/2017.
 */

public class GoogleSignInLogin implements LoginType {
    @Override
    public int id() {
        return R.id.lg_btn_google;
    }

    @Override
    public int requestCode() {
        return ConfigManager.Permission.GMAIL_REQUEST_CODE;
    }

    @Override
    public void login(ILoginActivityPresenter iLoginPresenter, String regIdGCM) {
        iLoginPresenter.loginByGoogle(regIdGCM);
    }

    @Override
    public String requirePermission(ILoginActivityPresenter loginActivityPresenter) {
        return loginActivityPresenter.getContext().getResources().getString(R.string.permission_must_google);
    }

    @Override
    public String[] permissions() {
        String[] PERMISSIONS_GMAIL = {
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.GET_ACCOUNTS
        };
        return PERMISSIONS_GMAIL;
    }

    @Override
    public String message() {
        return "location, phone and contact";
    }
}
