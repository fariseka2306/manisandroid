package com.ebizu.manis.manager.rewardcategory;

import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.reward.rewardlistcategory.rewardregular.RewardRegularActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by FARIS_mac on 16/11/17.
 */

public class RegularVoucher implements RewardCategory {

    @Override
    public String categoryName(String categoryName) {
        if (categoryName.equals(ConfigManager.Reward.REWARD_BULK_TITLE) && categoryName.equals(ConfigManager.Reward.REWARD_MY_VOUCHER_TITLE)) {

        }
        return null;
    }

    @Override
    public String categoryId(String categoryId) {
        return categoryId;
    }

    @Override
    public boolean isMatch(Context context, String categoryName) {
        if (!categoryName.equals(ConfigManager.Reward.REWARD_BULK_TITLE) && !categoryName.equals(context.getString(R.string.my_voucher_text))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void click(BaseActivity baseActivity, String categoryName, String categoryId) {
        getRegularVoucher(baseActivity, categoryName, categoryId, ConfigManager.Reward.RequestRewardType.REQUEST_REWARD_TYPE_CATEGORY);
    }

    private void getRegularVoucher(BaseActivity baseActivity, String categoryName, String categoryId, String requestRewardType) {
        Intent intent = new Intent(baseActivity, RewardRegularActivity.class);
        intent.putExtra(ConfigManager.Reward.REWARD_CATEGORY_NAME, categoryName);
        intent.putExtra(ConfigManager.Reward.REWARD_CATEGORY_ID, categoryId);
        intent.putExtra(ConfigManager.Reward.REQUEST_REWARD_TYPE, requestRewardType);
        baseActivity.startActivity(intent);
    }

}
