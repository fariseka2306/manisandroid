package com.ebizu.manis.manager.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.mvp.interest.InterestActivity;
import com.ebizu.manis.preference.ManisSession;

/**
 * Created by Ebizu-User on 23/07/2017.
 */

public class InterestActivityLauncher implements LauncherPriority {

    @Override
    public boolean notCompleted(Context context) {
        ManisSession manisSession = new ManisSession(context);
        return manisSession.isFirstLogin();
    }

    @Override
    public boolean notCompleted(Context context, int requestCode) {
        ManisSession manisSession = new ManisSession(context);
        return manisSession.isFirstLogin();
    }

    @Override
    public Intent getIntent(Activity activity) {
        return new Intent(activity, InterestActivity.class);
    }
}
