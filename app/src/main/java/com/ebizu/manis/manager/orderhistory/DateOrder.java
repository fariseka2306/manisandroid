package com.ebizu.manis.manager.orderhistory;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.orderhistory.OrderSpendingBar;

/**
 * Created by Raden on 7/25/17.
 */

public class DateOrder implements OrderSpendingBar {

    @Override
    public String name() {
        return "date";
    }

    @Override
    public int value() {
        return ConfigManager.SnapViewHistory.ORDER_DATE;
    }

    @Override
    public int position() {
        return 0;
    }
}
