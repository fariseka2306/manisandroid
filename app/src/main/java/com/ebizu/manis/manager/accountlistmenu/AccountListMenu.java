package com.ebizu.manis.manager.accountlistmenu;

import com.ebizu.manis.root.BaseActivity;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public interface AccountListMenu {
    int name();

    int thumbnail();

    int id();

    void click(BaseActivity baseActivity);

}