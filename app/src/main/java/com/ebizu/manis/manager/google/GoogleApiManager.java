package com.ebizu.manis.manager.google;

import android.support.annotation.NonNull;
import android.util.Log;

import com.ebizu.manis.root.IBaseActivity;
import com.ebizu.manis.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;

/**
 * Created by Ebizu-User on 10/07/2017.
 */

public class GoogleApiManager implements GoogleApiClient.OnConnectionFailedListener {

    private final String TAG = getClass().getSimpleName();

    private IBaseActivity iBaseActivity;
    private GoogleSignInOptions googleSignInOptions;
    private GoogleApiClient googleApiClient;

    public GoogleApiManager(IBaseActivity iBaseActivity) {
        this.iBaseActivity = iBaseActivity;
        googleApiClient();
    }

    public GoogleApiClient googleApiClient() {
        if (googleSignInOptions == null)
            googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(iBaseActivity.baseActivity().getString(R.string.google_web_client_id))
                    .requestEmail()
                    .requestScopes(Plus.SCOPE_PLUS_LOGIN)
                    .build();

        if (googleApiClient == null)
            googleApiClient = new GoogleApiClient.Builder(iBaseActivity.baseActivity())
                    .enableAutoManage(iBaseActivity.baseActivity(), this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                    .addApi(Plus.API)
                    .build();
        return googleApiClient;
    }

    public void disconnect() {
        if (googleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(googleApiClient);
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        iBaseActivity.dismissProgressBarDialog();
        iBaseActivity.showGoogleConnectionFailed(connectionResult);
        if (connectionResult.getErrorMessage() != null)
            Log.e(TAG, "onConnectionFailed: ".concat(connectionResult.getErrorMessage()));
    }
}