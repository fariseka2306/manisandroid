package com.ebizu.manis.manager.login;

import com.ebizu.manis.mvp.login.ILoginActivityPresenter;

/**
 * Created by Ebizu-User on 12/07/2017.
 */

public interface LoginType {

    int id();

    int requestCode();

    void login(ILoginActivityPresenter iLoginPresenter, String regIdGCM);

    String requirePermission(ILoginActivityPresenter loginActivityPresenter);

    String message();

    String[] permissions();
}
