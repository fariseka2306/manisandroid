package com.ebizu.manis.manager.tracker;

import com.ebizu.manis.model.tracker.ManisTrackerTable;

import org.litepal.crud.DataSupport;

import java.util.List;

/**
 * Created by ebizu on 12/15/17.
 */

public class ScreenManager {

    private static ScreenManager instance;

    private static String originPage = "";
    private static String currentPage = "";
    private static String lastPage = "";

    private ScreenManager() {
    }

    public static ScreenManager getInstance() {
        return instance;
    }

    public static void setCurrentPage(String page) {
        if ("" == lastPage) {
            ManisTrackerTable manisTrackerTable = DataSupport.findLast(ManisTrackerTable.class);
            if (null != manisTrackerTable)
                lastPage = manisTrackerTable.getCurrentPage();
        } else {
            lastPage = currentPage;
        }
        currentPage = page;
    }

    public static void setOriginPage(String page) {
        ScreenManager.originPage = page;
    }

    public static String getOriginPage() {
        return originPage;
    }

    public static String getCurrentPage() {
        return currentPage;
    }

    public static String getLastPage() {
        return lastPage;
    }
}
