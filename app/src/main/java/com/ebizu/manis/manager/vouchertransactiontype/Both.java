package com.ebizu.manis.manager.vouchertransactiontype;

import android.content.Context;
import android.view.View;

import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.rewarddetail.RewardDetailAbstractActivity;
import com.ebizu.manis.view.adapter.rewardvoucher.SingleRewardViewHolder;
import com.ebizu.sdk.reward.models.Reward;

/**
 * Created by FARIS_mac on 22/11/17.
 */

public class Both implements VoucherTransactionType {
    @Override
    public boolean isMatch(RewardVoucher rewardVoucher) {
        if (rewardVoucher.getPoint() > 0 &&
                rewardVoucher.getVoucherPurchaseValue() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isMatch(Reward reward) {
        if (reward.getPoint() > 0 &&
                reward.getVoucherPurchaseValue() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setButtonVoucher(RewardDetailAbstractActivity rewardDetailAbstractActivity) {
        rewardDetailAbstractActivity.rfRlBuy.setVisibility(View.VISIBLE);
        rewardDetailAbstractActivity.rfRlRedeem.setVisibility(View.VISIBLE);
    }

    @Override
    public void setIconVoucher(SingleRewardViewHolder singleRewardViewHolder, Context context) {
        singleRewardViewHolder.rlBtnFree.setVisibility(View.INVISIBLE);
        singleRewardViewHolder.llPoint.setVisibility(View.VISIBLE);
        singleRewardViewHolder.llValue.setVisibility(View.VISIBLE);
    }
}
