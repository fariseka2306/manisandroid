package com.ebizu.manis.manager.datespendingbar;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.StatisticBody;

/**
 * Created by abizu-alvio on 7/13/2017.
 */

public class LastMonthIDate extends DateSpendingBar
        implements IDateSpendingBar {

    private StatisticBody statisticBody;

    public LastMonthIDate(Context context) {
        super(context);
    }

    @Override
    public int index() {
        return 1;
    }

    @Override
    public String name(Context context) {
        context.getString(R.string.hm_last_month);
        return context.getString(R.string.hm_last_month);
    }

    @Override
    public StatisticBody getStatisticBody() {
        if (statisticBody == null) {
            statisticBody = new StatisticBody();
            statisticBody.setLimit("lm");
        }
        new AnalyticManager(context).trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_HOME,
                ConfigManager.Analytic.Action.ITEM_CLICK,
                "Item Last Month"
        );
        return statisticBody;
    }
}
