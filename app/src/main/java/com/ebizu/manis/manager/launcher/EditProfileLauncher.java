package com.ebizu.manis.manager.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.account.accountmenulist.profile.ProfileActivity;
import com.ebizu.manis.preference.ManisSession;

/**
 * Created by ebizu on 10/20/17.
 */

public class EditProfileLauncher implements LauncherPriority {

    @Override
    public boolean notCompleted(Context context) {
        return new ManisSession(context).isFirstLogin();
    }

    @Override
    public boolean notCompleted(Context context, int requestCode) {
        return new ManisSession(context).isFirstLogin() && requestCode != ConfigManager.Otp.SIGN_UP_OTP_REQUEST_CODE;
    }

    @Override
    public Intent getIntent(Activity activity) {
        Intent intent = new Intent(activity, ProfileActivity.class);
        intent.putExtra(ConfigManager.UpdateProfile.UPDATE_PROFILE_TYPE_INTENT, ConfigManager.UpdateProfile.UPDATE_PROFILE_LOGIN_TYPE);
        return intent;
    }
}
