package com.ebizu.manis.manager.accountlistmenu;

import android.content.Intent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.account.accountmenulist.snapearnguide.SnapGuideActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class SnapGuideListMenu implements AccountListMenu {
    @Override
    public int name() {
        return R.string.fa_txt_snapguide;
    }

    @Override
    public int thumbnail() {
        return R.drawable.account_snap_and_earn_guide_icon;
    }

    @Override
    public int id() {
        return 5;
    }

    @Override
    public void click(BaseActivity activity) {
        Intent intent = new Intent(activity, SnapGuideActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        activity.getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_ACCOUNT,
                ConfigManager.Analytic.Action.CLICK,
                "Button Snap Guide");
    }

}
