package com.ebizu.manis.manager.vouchertransactiontype;

import android.content.Context;
import android.view.View;

import com.ebizu.manis.R;
import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.rewarddetail.RewardDetailAbstractActivity;
import com.ebizu.manis.view.adapter.rewardvoucher.SingleRewardViewHolder;
import com.ebizu.sdk.reward.models.Reward;

/**
 * Created by FARIS_mac on 22/11/17.
 */

public class Free implements VoucherTransactionType {
    @Override
    public boolean isMatch(RewardVoucher rewardVoucher) {
        if (rewardVoucher.getPoint() == 0 &&
                (rewardVoucher.getVoucherTransactionType().equalsIgnoreCase(Reward.VOUCHER_TRANSACTION_REDEEMABLE) ||
                        rewardVoucher.getVoucherTransactionType().equalsIgnoreCase(Reward.VOUCHER_TRANSACTION_BOTH) ||
                        rewardVoucher.getVoucherTransactionType().equals("")) &&
                rewardVoucher.getVoucherPurchaseValue() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isMatch(Reward reward) {
        if (reward.getPoint() == 0 &&
                reward.getVoucherTransactionType().equalsIgnoreCase(RewardVoucher.VOUCHER_TRANSACTION_REDEEMABLE) &&
                reward.getVoucherPurchaseValue() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setButtonVoucher(RewardDetailAbstractActivity rewardDetailAbstractActivity) {
        rewardDetailAbstractActivity.rfRlRedeem.setVisibility(View.VISIBLE);
    }

    @Override
    public void setIconVoucher(SingleRewardViewHolder singleRewardViewHolder, Context context) {
        singleRewardViewHolder.tvPoint.setText(context.getString(R.string.rd_txt_free));
        singleRewardViewHolder.llPoint.setVisibility(View.INVISIBLE);
        singleRewardViewHolder.llValue.setVisibility(View.INVISIBLE);
        singleRewardViewHolder.rlBtnFree.setVisibility(View.VISIBLE);
    }
}
