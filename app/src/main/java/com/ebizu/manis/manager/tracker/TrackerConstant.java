package com.ebizu.manis.manager.tracker;

/**
 * Created by ebizu on 12/15/17.
 */

public class TrackerConstant {

    public static int kTrackerBatchLimit = 10;
    public static String kTrackerLastUpdateTimeKey = "TrackerLastUpdateTime";
    public static String kTrackerLastScreenKey = "TrackerLastScreen";
    public static String kTrackerStandardKey = "TrackerStandardKey";
    public static String kTrackerLocalDateTimeKey = "local_datetime";
    public static String kTrackerIPAddressKey = "ip_address";
    public static String kTrackerAccountIDKey = "account_id";
    public static String kTrackerOriginPageKey = "origin_page";
    public static String kTrackerLastPageKey = "last_page";
    ;
    public static String kTrackerCurrentPageKey = ";current_page";
    public static String kTrackerComponentKey = "component";
    public static String kTrackerSubFeatureKey = "sub_feature";
    public static String kTrackerIdentifierNumber = "identifier_number";
    public static String kTrackerIdentifierName = "identifier_name";

    //Mark: Origin Page
    public static String kTrackerOriginPageLogin = "Login";
    public static String kTrackerOriginPageHome = "Home";
    public static String kTrackerOriginPageStores = "Stores";
    public static String kTrackerOriginPageMissions = "Missions";
    public static String kTrackerOriginPageRewards = "Rewards";
    public static String kTrackerOriginPageLuckyDraw = "Lucky Draw";
    public static String kTrackerOriginPageAccount = "Account";
    public static String kTrackerOriginNotification = "Notification";

    //Mark: Pages
    public static String kTrackerPageMissionDetail = "Mission Detail";
    public static String kTrackerPageMissionSuccessPopup = "Mission Success Popup";
    public static String kTrackerPageMissionTandC = "Mission - Terms and Condition";
    public static String kTrackerPageLuckyDrawHelpPopup = "Lucky Draw - Help Popup";
    public static String kTrackerPageLuckyDrawTotalTicketsEnteredPopup = "Lucky Draw - Total Tickets Entered Popup";
    public static String kTrackerPageLuckyDrawViewOurPastWinnersPopup = "Lucky Draw - View Our Past Winners Popup";
    public static String kTrackerPageImagePicker = "Mission image picker";
    public static String kTrackerPageMissionErrorPopup = "Lucky Draw - View Our Past Winners Popup";

    //Mark: Component
    public static String kTrackerComponentButton = "Button";
    public static String kTrackerComponentList = "List";
    public static String kTrackerComponentTabBar = "TabBar";
    public static String kTrackerComponentTab = "Tab";

    //Mark: SubFeature
    public static String kTrackerSubFeatureHelp = "Help";
    public static String kTrackerSubFeatureTotalTicketsEntered = "Total Tickets Entered";
    public static String kTrackerSubFeatureViewPastWinners = "View Past Winners";
    public static String kTrackerSubFeatureImagePrizesThisWeek = "Image Prizes This Week";
    public static String kTrackerSubFeatureGoldenTicket = "Golden Ticket";
    public static String kTrackerSubFeatureTandC = "Terms and Conditions";
    public static String kTrackerSubFeatureExecuteMission = "Execute Mission";
    public static String kTrackerSubFeatureCapturePicture = "Capture Picture";
    public static String kTrackerSubFeatureDismissView = "Dismiss View";
    
}
