package com.ebizu.manis.manager.datespendingbar;

import android.content.Context;

import com.ebizu.manis.model.StatisticBody;

/**
 * Created by ebizu on 10/26/17.
 */

public class DateSpendingBar implements IDateSpendingBar {

    protected Context context;

    public DateSpendingBar(Context context) {
        this.context = context;
    }

    @Override
    public int index() {
        return 0;
    }

    @Override
    public String name(Context context) {
        return null;
    }

    @Override
    public StatisticBody getStatisticBody() {
        return null;
    }
}
