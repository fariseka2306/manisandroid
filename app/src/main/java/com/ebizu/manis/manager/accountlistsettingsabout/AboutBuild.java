package com.ebizu.manis.manager.accountlistsettingsabout;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by abizu-alvio on 7/21/2017.
 */

public class AboutBuild implements AccountListSettingsAbout {
    @Override
    public int id() {
        return 3;
    }

    @Override
    public int name() {
        return R.string.ab_txt_build;
    }

    @Override
    public String build(Activity activity) {
        String versionCode = "";
        PackageInfo packageInfo = null;
        try {
            packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            if (null == packageInfo) {
                versionCode = "1";
            } else {
                versionCode = String.valueOf(packageInfo.versionCode);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    @Override
    public void click(Context context) {
        // Do nothing because we don't use it.
    }

    @Override
    public void click(BaseActivity baseActivity) {
        // Do nothing because we don't use it.
    }
}
