package com.ebizu.manis.manager.accountlistsettingsabout;

import android.app.Activity;
import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBody;
import com.ebizu.manis.service.manis.requestbodyv2.bodysuper.RequestBodyBuilder;
import com.ebizu.manis.service.manis.requestbodyv2.data.ManisTncData;
import com.ebizu.manis.view.dialog.term.TermDialog;

/**
 * Created by abizu-alvio on 7/21/2017.
 */

public class AboutPrivacy implements AccountListSettingsAbout {
    @Override
    public int id() {
        return 1;
    }

    @Override
    public int name() {
        return R.string.ab_txt_privacy;
    }

    @Override
    public String build(Activity activity) {
        return null;
    }

    @Override
    public void click(Context context) {
        // Do nothing because we don't use it.
    }

    @Override
    public void click(BaseActivity baseActivity) {
        TermDialog termDialog = new TermDialog(baseActivity);
        termDialog.setIsManisLegal(true);
        termDialog.show();
        termDialog.setActivity(baseActivity);
        termDialog.getTermPresenter().loadTerm(setRequestBody(baseActivity), ConfigManager.ManisLegal.TNC_PP);
        baseActivity.getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.ABOUT_ACTIVITY,
                ConfigManager.Analytic.Action.CLICK, "Text Privacy");
    }

    private RequestBody setRequestBody(Context context) {
        ManisTncData manisTncData = new ManisTncData();
        manisTncData.setType(ConfigManager.ManisLegal.TNC_TYPE_NON_SNE);
        return new RequestBodyBuilder(context)
                .setCommand(ConfigManager.ManisLegal.COMMAND_MANIS_LEGAL)
                .setFunction(ConfigManager.ManisLegal.FUNCTION_MANIS_LEGAL)
                .setData(manisTncData)
                .create();
    }
}
