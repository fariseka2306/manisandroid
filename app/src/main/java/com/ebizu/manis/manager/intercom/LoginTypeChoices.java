package com.ebizu.manis.manager.intercom;

import com.ebizu.manis.model.Account;

import java.util.Map;

/**
 * Created by abizu-alvio on 8/11/2017.
 */

public interface LoginTypeChoices {
    String id();
    Map<String, String> getIntercom(Account account);
}
