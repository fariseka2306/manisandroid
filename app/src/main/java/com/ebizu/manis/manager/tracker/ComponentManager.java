package com.ebizu.manis.manager.tracker;

/**
 * Created by ebizu on 12/15/17.
 */

public class ComponentManager {

    private static ComponentManager instance;

    private static String component = "";

    private ComponentManager() {
    }

    public static ComponentManager getInstance() {
        if (null == instance)
            instance = new ComponentManager();
        return instance;
    }

    public void reset(){
        component = "";
    }

    public void setInstance(ComponentManager instance) {
        ComponentManager.instance = instance;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        ComponentManager.component = component;
    }
}
