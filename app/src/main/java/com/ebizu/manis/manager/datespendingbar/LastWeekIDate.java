package com.ebizu.manis.manager.datespendingbar;

import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.manager.analytic.AnalyticManager;
import com.ebizu.manis.model.StatisticBody;

/**
 * Created by abizu-alvio on 7/13/2017.
 */

public class LastWeekIDate
        extends DateSpendingBar implements IDateSpendingBar {

    private StatisticBody statisticBody;

    public LastWeekIDate(Context context) {
        super(context);
    }

    @Override
    public int index() {
        return 3;
    }

    @Override
    public String name(Context context) {
        context.getString(R.string.hm_last_week);
        return context.getString(R.string.hm_last_week);
    }

    @Override
    public StatisticBody getStatisticBody() {
        if (statisticBody == null) {
            statisticBody = new StatisticBody();
            statisticBody.setLimit("lw");
        }
        new AnalyticManager(context).trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_HOME,
                ConfigManager.Analytic.Action.ITEM_CLICK,
                "Item Last Week"
        );
        return statisticBody;
    }
}
