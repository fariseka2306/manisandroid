package com.ebizu.manis.manager.tracker;

/**
 * Created by ebizu on 12/15/17.
 */

public class IdentifierManager {

    private static IdentifierManager instance;

    private static String identifierNumber = "";
    private static String identifierName = "";

    private IdentifierManager() {
    }

    public static IdentifierManager getInstance() {
        if (null == instance)
            instance = new IdentifierManager();
        return instance;
    }

    public void reset() {
        identifierNumber = "";
        identifierName = "";
    }

    public void setIdentifierNumber(String identifierNumber) {
        IdentifierManager.identifierNumber = identifierNumber;
    }

    public void setIdentifierName(String identifierName) {
        IdentifierManager.identifierName = identifierName;
    }

    public String getIdentifierNumber() {
        return identifierNumber;
    }

    public String getIdentifierName() {
        return identifierName;
    }
}
