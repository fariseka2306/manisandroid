package com.ebizu.manis.manager.camera;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by Ebizu-User on 10/08/2017.
 */

public class CameraManager {

    private Context context;
    private static CameraManager cameraManager;

    public CameraManager(Context context) {
        this.context = context;
    }

    public static CameraManager init(Context context) {
        if (cameraManager == null)
            cameraManager = new CameraManager(context);
        return cameraManager;
    }

    public boolean hasCameraFeature() {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }
}
