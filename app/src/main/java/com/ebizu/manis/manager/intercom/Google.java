package com.ebizu.manis.manager.intercom;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.helper.LoginChoices;
import com.ebizu.manis.model.Account;

import java.util.HashMap;
import java.util.Map;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;

/**
 * Created by abizu-alvio on 8/11/2017.
 */

public class  Google implements LoginTypeChoices {

    LoginChoices loginChoices;


    @Override
    public String id() {
        loginChoices = LoginChoices.GOOGLE;
        return String.valueOf(loginChoices);
    }

    @Override
    public Map<String, String> getIntercom(Account account) {
        Intercom client = Intercom.client();
        Map<String, String> userMap = new HashMap<>();
        userMap.put("name", account.getAccScreenName());
        userMap.put(ConfigManager.Intercom.GOOGLE_EMAIL, account.getAccGoogleEmail());
        userMap.put(ConfigManager.Intercom.GOOGLE_ID, String.valueOf(account.getAccGoogleId()));
        UserAttributes userAttributes = new UserAttributes.Builder()
                .withName(account.getAccScreenName())
                .withPhone(account.getAccGoogleEmail())
                .build();
        client.updateUser(userAttributes);
        return userMap;
    }
}
