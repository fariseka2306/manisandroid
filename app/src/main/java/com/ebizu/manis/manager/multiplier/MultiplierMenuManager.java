package com.ebizu.manis.manager.multiplier;

import android.view.View;
import android.widget.LinearLayout;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.manis.requestbody.StoreBody;

/**
 * Created by firef on 7/13/2017.
 */

public class MultiplierMenuManager {

    private static ActionMultiplier[] actionMultipliers = {
            new MultiplierAll(),
            new MultiplierZero(),
            new MultiplierOne(),
            new MultiplierTwo(),
            new MultiplierThree(),
            new MultiplierFour(),
            new MultiplierFive()
    };

    public static int getMultiplier(int viewId) {
        for (ActionMultiplier actionMultiplier : actionMultipliers) {
            if (actionMultiplier.id() == viewId) {
                return actionMultiplier.multiplier();
            }
        }
        return new MultiplierZero().multiplier();
    }

    public static String getMerchantTier(int viewId) {
        for (ActionMultiplier actionMultiplier : actionMultipliers) {
            if (actionMultiplier.id() == viewId) {
                return actionMultiplier.merchantTier();
            }
        }
        return new MultiplierZero().merchantTier();
    }

    public static int getDrawble(int multiplier, String merchantTier) {
        for (ActionMultiplier actionMultiplier : actionMultipliers) {
            if (actionMultiplier.multiplier() == multiplier) {
                return actionMultiplier.drawable(merchantTier);
            }
        }
        return new MultiplierZero().drawable(merchantTier);
    }

    public static int getDrawble(int multiplier) {
        for (ActionMultiplier actionMultiplier : actionMultipliers) {
            if (actionMultiplier.multiplier() == multiplier) {
                return actionMultiplier.drawable();
            }
        }
        return new MultiplierZero().drawable();
    }

    public static String getText(int multiplier, String merchantTier) {
        for (ActionMultiplier actionMultiplier : actionMultipliers) {
            if (actionMultiplier.multiplier() == multiplier) {
                return actionMultiplier.text(merchantTier);
            }
        }
        return new MultiplierZero().text(merchantTier);
    }

    public static String getText(int multiplier) {
        for (ActionMultiplier actionMultiplier : actionMultipliers) {
            if (actionMultiplier.multiplier() == multiplier) {
                return actionMultiplier.text();
            }
        }
        return new MultiplierZero().text();
    }

    public static void activeMultiplierMenu(LinearLayout viewGroupMultiplier, int viewId) {
        for (ActionMultiplier actionMultiplier : actionMultipliers) {
            if (actionMultiplier.id() == viewId) {
                actionMultiplier.bottomLineView(viewGroupMultiplier).setVisibility(View.VISIBLE);
            } else {
                actionMultiplier.bottomLineView(viewGroupMultiplier).setVisibility(View.INVISIBLE);
            }
        }
    }

    public static boolean isLuckyDraw(String merchantTier) {
        return merchantTier.equals(ConfigManager.Store.MULTIPLIER_TIER_LONGTAIL);
    }

}
