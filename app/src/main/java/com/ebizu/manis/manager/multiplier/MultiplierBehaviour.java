package com.ebizu.manis.manager.multiplier;

import android.view.View;
import android.widget.RelativeLayout;

import com.ebizu.manis.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ebizu on 01/08/17.
 */

public class MultiplierBehaviour implements IMultiplierBehaviour {

    @BindView(R.id.item_tab_store_multiplier_all)
    RelativeLayout rlTabStoreMultiplierAll;

    @BindView(R.id.item_tab_store_multiplier_ticket)
    RelativeLayout rlTabStoreMultiplierZero;

    @BindView(R.id.item_tab_store_multiplier_one)
    RelativeLayout rlTabStoreMultiplierOne;

    @BindView(R.id.item_tab_store_multiplier_two)
    RelativeLayout rlTabStoreMultiplierTwo;

    @BindView(R.id.item_tab_store_multiplier_three)
    RelativeLayout rlTabStoreMultiplierThree;

    @BindView(R.id.item_tab_store_multiplier_four)
    RelativeLayout rlTabStoreMultiplierFour;

    @BindView(R.id.item_tab_store_multiplier_five)
    RelativeLayout rlTabStoreMultiplierFive;

    @BindView(R.id.multiplier_line_all)
    View multiplierLineAll;

    @BindView(R.id.multiplier_line_zero)
    View multiplierLineZero;

    @BindView(R.id.multiplier_line_one)
    View multiplierLineOne;

    @BindView(R.id.multiplier_line_two)
    View multiplierLineTwo;

    @BindView(R.id.multiplier_line_three)
    View multiplierLineThree;

    @BindView(R.id.multiplier_line_four)
    View multiplierLineFour;

    @BindView(R.id.multiplier_line_five)
    View multiplierLineFive;


    @Override
    public void disableMultiplierClick(View view) {
        ButterKnife.bind(this, view);
        RelativeLayout[] tabStoreMultipliers = {
                rlTabStoreMultiplierAll,
                rlTabStoreMultiplierZero,
                rlTabStoreMultiplierOne,
                rlTabStoreMultiplierTwo,
                rlTabStoreMultiplierThree,
                rlTabStoreMultiplierFour,
                rlTabStoreMultiplierFive
        };
        for (RelativeLayout tabStore : tabStoreMultipliers) {
            tabStore.setClickable(false);
        }
    }

    @Override
    public void checkMultiplierLine(View view) {
        ButterKnife.bind(this, view);
        View[] multiplierLines = {
                multiplierLineAll,
                multiplierLineZero,
                multiplierLineOne,
                multiplierLineTwo,
                multiplierLineThree,
                multiplierLineFour,
                multiplierLineFive
        };
        if (rlTabStoreMultiplierAll.isPressed()) {
            for (View multiplierLine : multiplierLines) {
                multiplierLine.setVisibility(View.INVISIBLE);
            }
            multiplierLineAll.setVisibility(View.VISIBLE);
        } else if (rlTabStoreMultiplierZero.isPressed()) {
            for (View multiplierLine : multiplierLines) {
                multiplierLine.setVisibility(View.INVISIBLE);
            }
            multiplierLineZero.setVisibility(View.VISIBLE);
        } else if (rlTabStoreMultiplierOne.isPressed()) {
            for (View multiplierLine : multiplierLines) {
                multiplierLine.setVisibility(View.INVISIBLE);
            }
            multiplierLineOne.setVisibility(View.VISIBLE);
        } else if (rlTabStoreMultiplierTwo.isPressed()) {
            for (View multiplierLine : multiplierLines) {
                multiplierLine.setVisibility(View.INVISIBLE);
            }
            multiplierLineTwo.setVisibility(View.VISIBLE);
        } else if (rlTabStoreMultiplierThree.isPressed()) {
            for (View multiplierLine : multiplierLines) {
                multiplierLine.setVisibility(View.INVISIBLE);
            }
            multiplierLineThree.setVisibility(View.VISIBLE);
        } else if (rlTabStoreMultiplierFour.isPressed()) {
            for (View multiplierLine : multiplierLines) {
                multiplierLine.setVisibility(View.INVISIBLE);
            }
            multiplierLineFour.setVisibility(View.VISIBLE);
        } else if (rlTabStoreMultiplierFive.isPressed()) {
            for (View multiplierLine : multiplierLines) {
                multiplierLine.setVisibility(View.INVISIBLE);
            }
            multiplierLineFive.setVisibility(View.VISIBLE);
        }
    }

}
