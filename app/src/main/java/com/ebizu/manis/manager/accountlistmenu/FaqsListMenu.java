package com.ebizu.manis.manager.accountlistmenu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.account.accountmenulist.faq.FaqActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class FaqsListMenu implements AccountListMenu {
    @Override
    public int name() {
        return R.string.fa_txt_faq;
    }

    @Override
    public int thumbnail() {
        return R.drawable.account_settings_help;
    }

    @Override
    public int id() {
        return 4;
    }

    @Override
    public void click(BaseActivity baseActivity) {
        Intent intent = new Intent(baseActivity, FaqActivity.class);
        baseActivity.startActivity(intent);
        baseActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        baseActivity.getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_ACCOUNT,
                ConfigManager.Analytic.Action.CLICK,
                "Button FAQS");
    }

}
