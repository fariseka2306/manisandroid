package com.ebizu.manis.manager.analytic;

import android.content.Context;
import android.util.Log;

import com.ebizu.manis.helper.AnalyticsTrackers;
import com.ebizu.manis.preference.ManisSession;
import com.ebizu.manis.sdk.ManisLocalData;
import com.ebizu.manis.sdk.entities.LoginData;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by ebizu on 10/24/17.
 */

public class AnalyticManager {

    private ManisSession manisSession;
    private Context context;

    public AnalyticManager(Context context) {
        this.context = context;
        this.manisSession = new ManisSession(context);
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(context).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(context, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();
        try {
            LoginData loginData = ManisLocalData.getLoginData();
            if (manisSession.isLoggedIn() && loginData != null) {
                t.set("&uid", String.valueOf(loginData.accId));
            } else {
                t.set("&uid", "");
            }
        } catch (Exception e) {
            Log.e(this.getClass().getName(), e.getMessage());
            t.set("&uid", "");
        } finally {
            // Build and send an Event.
            t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
        }
    }


}
