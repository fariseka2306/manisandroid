package com.ebizu.manis.manager.input;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.model.rewardvoucher.Input;
import com.ebizu.manis.preference.ManisSession;

/**
 * Created by FARIS_mac on 21/11/17.
 */

public class PhoneNumber implements InputType {

    @Override
    public String inputType() {
        return ConfigManager.RedeemPhysicalInput.INPUT_PHONE_NUMBER;
    }

    @Override
    public void setEditTextInput(Input input, EditText editTextInput, ManisSession manisSession) {
        if (manisSession.getAccountSession().getAccMsisdn().equals("")) {
            editTextInput.setHint(input.getText());
        } else {
            editTextInput.setText(manisSession.getAccountSession().getAccMsisdn()
            );
        }
        editTextInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0)
                    editTextInput.setHint(input.getText());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}
