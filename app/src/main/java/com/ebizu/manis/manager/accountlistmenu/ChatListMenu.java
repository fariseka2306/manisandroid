package com.ebizu.manis.manager.accountlistmenu;

import android.app.Activity;
import android.content.Context;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.root.BaseActivity;

import io.intercom.android.sdk.Intercom;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class ChatListMenu implements AccountListMenu {
    @Override
    public int name() {
        return R.string.fa_txt_chat;
    }

    @Override
    public int thumbnail() {
        return R.drawable.navigation_intercomio_icon;
    }

    @Override
    public int id() {
        return 6;
    }

    @Override
    public void click(BaseActivity baseActivity) {
        Intercom.client().displayConversationsList();
        baseActivity.getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_ACCOUNT,
                ConfigManager.Analytic.Action.CLICK,
                "Button Chat With Us");
    }

}
