package com.ebizu.manis.manager.vouchertransactiontype;

import android.content.Context;
import android.view.View;

import com.ebizu.manis.model.rewardvoucher.RewardVoucher;
import com.ebizu.manis.mvp.reward.rewarddetail.RewardDetailAbstractActivity;
import com.ebizu.manis.view.adapter.rewardvoucher.SingleRewardViewHolder;
import com.ebizu.sdk.reward.models.Reward;

/**
 * Created by FARIS_mac on 22/11/17.
 */

public class Else implements VoucherTransactionType {
    @Override
    public boolean isMatch(RewardVoucher rewardVoucher) {
        return true;
    }

    @Override
    public boolean isMatch(Reward rewardVoucher) {
        return true;
    }

    @Override
    public void setButtonVoucher(RewardDetailAbstractActivity rewardDetailAbstractActivity) {

    }

    @Override
    public void setIconVoucher(SingleRewardViewHolder singleRewardViewHolder, Context context) {
        singleRewardViewHolder.llPoint.setVisibility(View.INVISIBLE);
        singleRewardViewHolder.llValue.setVisibility(View.INVISIBLE);
        singleRewardViewHolder.rlBtnFree.setVisibility(View.INVISIBLE);
    }
}
