package com.ebizu.manis.manager.versioning;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.BuildConfig;
import com.ebizu.manis.model.Versioning;
import com.ebizu.manis.mvp.splashscreen.SplashScreenActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by mac on 8/22/17.
 */

public class Obsolete implements VersioningStatus {

    @Override
    public String status() {
        return "obsolete";
    }

    @Override
    public void showVersioningStatus(Versioning versioning, Context context, BaseActivity baseActivity) {
        baseActivity.showAlertDialog(context.getString(R.string.error),
                versioning.getVersionMessage(),
                false,
                R.drawable.manis_logo,
                "UPDATE", (dialog, which) -> {
                    try {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.market_id) + BuildConfig.APPLICATION_ID)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.playstore_id) + BuildConfig.APPLICATION_ID)));
                    }
                    baseActivity.finish();
                });
    }
}