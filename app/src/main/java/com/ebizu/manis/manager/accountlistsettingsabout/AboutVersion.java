package com.ebizu.manis.manager.accountlistsettingsabout;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.ebizu.manis.R;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by abizu-alvio on 7/21/2017.
 */

public class AboutVersion implements AccountListSettingsAbout {
    @Override
    public int id() {
        return 2;
    }

    @Override
    public int name() {
        return R.string.ab_txt_version;
    }

    @Override
    public String build(Activity activity) {
        PackageInfo packageInfo = new PackageInfo();
        try {
            packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo.versionName;
    }

    @Override
    public void click(Context context) {
        //Do nothing
    }

    @Override
    public void click(BaseActivity baseActivity) {
        //Do nothing
    }
}
