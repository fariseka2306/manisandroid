package com.ebizu.manis.manager.categorycolor;

import android.content.Context;

/**
 * Created by abizu-alvio on 7/18/2017.
 */

public interface CategoryColor {
    int index();

    int color(Context context);
}
