package com.ebizu.manis.manager.rewardcategory;

import android.content.Context;

import com.ebizu.manis.root.BaseActivity;

/**
 * Created by FARIS_mac on 11/13/17.
 */

public interface RewardCategory {

    String categoryName(String categoryName);

    String categoryId(String categoryId);

    boolean isMatch(Context context, String categoryName);

    void click(BaseActivity baseActivity, String categoryName, String categoryId);

}
