package com.ebizu.manis.manager.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.preference.ManisSession;

/**
 * Created by Ebizu-User on 23/07/2017.
 */

public interface LauncherPriority {

    boolean notCompleted(Context context);

    boolean notCompleted(Context context, int requestCode);

    Intent getIntent(Activity activity);
}