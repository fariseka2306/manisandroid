package com.ebizu.manis.manager.negativescenario;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConnectionDetector;
import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.root.BaseView;
import com.ebizu.manis.service.manis.response.ErrorResponse;
import com.ebizu.manis.service.manis.responsev2.status.Status;
import com.ebizu.manis.service.reward.response.ResponseRewardApi;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.HttpException;

/**
 * Created by Ebizu-User on 28/07/2017.
 */

public class NegativeScenarioManager {

    public enum NegativeView {
        NONE,
        NOTIFICATION,
        FULL_PAGE,
        DIALOG
    }

    private static NegativeInterface[] negativeInterfaces = {
            new NegativeDialogView(),
            new NegativeFullPageView(),
            new NegativeNotificationView()
    };

    public static void show(Throwable throwable, BaseView baseView, NegativeView negativeView) {
        for (NegativeInterface negativeInterface : negativeInterfaces) {
            if (negativeInterface.getNegativeView() == negativeView) {
                negativeInterface.showNegativeView(getErrorMessage(throwable, baseView), baseView);
                return;
            }
        }
    }

    public static void show(Throwable throwable, BaseActivity baseActivity, NegativeView negativeView) {
        for (NegativeInterface negativeInterface : negativeInterfaces) {
            if (negativeInterface.getNegativeView() == negativeView) {
                negativeInterface.showNegativeView(getErrorMessage(throwable, baseActivity), baseActivity);
            }
        }
    }

    public static void show(Throwable throwable, ErrorResponse errorResponse, BaseView baseView, NegativeView negativeView) {
        for (NegativeInterface negativeInterface : negativeInterfaces) {
            if (negativeInterface.getNegativeView() == negativeView) {
                negativeInterface.showNegativeView(getErrorMessage(throwable, baseView), baseView);
                return;
            }
        }
    }

    public static void show(ErrorResponse errorResponse, BaseActivity baseActivity, NegativeView negativeView) {
        for (NegativeInterface negativeInterface : negativeInterfaces) {
            if (negativeInterface.getNegativeView() == negativeView) {
                negativeInterface.showNegativeView(errorResponse.getMessage(), baseActivity);
            }
        }
    }

    public static void show(ErrorResponse errorResponse, BaseView baseView, NegativeView negativeView) {
        for (NegativeInterface negativeInterface : negativeInterfaces) {
            if (negativeInterface.getNegativeView() == negativeView) {
                negativeInterface.showNegativeView(errorResponse.getMessage(), baseView);
            }
        }
    }

    public static void show(ResponseRewardApi responseRewardApi, BaseView baseView, NegativeView negativeView) {
        for (NegativeInterface negativeInterface : negativeInterfaces) {
            if (negativeInterface.getNegativeView() == negativeView) {
                negativeInterface.showNegativeView(responseRewardApi.getErrorMessage(), baseView);
            }
        }
    }

    public static void show(String status, BaseActivity baseActivity, NegativeView negativeView) {
        for (NegativeInterface negativeInterface : negativeInterfaces) {
            if (negativeInterface.getNegativeView() == negativeView) {
                negativeInterface.showNegativeView(status, baseActivity);
            }
        }
    }

    public static void show(String status, BaseView baseView, NegativeView negativeView) {
        for (NegativeInterface negativeInterface : negativeInterfaces) {
            if (negativeInterface.getNegativeView() == negativeView) {
                negativeInterface.showNegativeView(status, baseView);
            }
        }
    }

    private static String getErrorMessage(Throwable throwable, BaseView baseView) {
        Throwable error = throwable;
        String errorMessage = "";
        if (throwable instanceof IOException) {
            return baseView.getBaseActivity().getResources().getString(R.string.error_connection);
        } else if (throwable instanceof HttpException) {
            try {
                HttpException httpException = (HttpException) error;
                ErrorResponse errorResponse = new Gson().fromJson(httpException.response().errorBody().string(), ErrorResponse.class);
                errorMessage = errorResponse.getMessage();
            } catch (Exception e) {
                e.printStackTrace();
                errorMessage = baseView.getBaseActivity().getResources().getString(R.string.server_busy);
            }
        } else {
            errorMessage = baseView.getBaseActivity().getResources().getString(R.string.server_busy);
        }
        return errorMessage;
    }

    private static String getErrorMessage(Throwable throwable, BaseActivity baseActivity) {
        String errorMessage = "";
        if (throwable instanceof IOException) {
            return baseActivity.getResources().getString(R.string.error_connection);
        } else if (throwable instanceof HttpException) {
            try {
                HttpException httpException = (HttpException) throwable;
                ErrorResponse errorResponse = new Gson().fromJson(httpException.response().errorBody().string(), ErrorResponse.class);
                errorMessage = errorResponse.getMessage();
            } catch (Exception e) {
                e.printStackTrace();
                errorMessage = baseActivity.getResources().getString(R.string.server_busy);
            }
        } else {
            errorMessage = baseActivity.getResources().getString(R.string.server_busy);
        }
        return errorMessage;
    }

    public static void showNotificationMessage(BaseView baseView) {
        if (!ConnectionDetector.isNetworkConnected(baseView.getBaseActivity())) {
            baseView.showNotificationMessage(
                    baseView.getBaseActivity().getResources().getString(R.string.error_connection));
        } else {
            baseView.showNotificationMessage(
                    baseView.getBaseActivity().getResources().getString(R.string.server_busy));
        }
    }

    public static void showNotificationMessage(Throwable throwable, BaseView baseView) {
        if (throwable instanceof IOException) {
            baseView.showNotificationMessage(
                    baseView.getBaseActivity().getResources().getString(R.string.error_connection));
        } else if (throwable instanceof HttpException) {
            baseView.showNotificationMessage(
                    baseView.getBaseActivity().getResources().getString(R.string.server_busy));
        }
    }

    public static void showNotificationMessage(BaseView baseView, String message) {
        baseView.showNotificationMessage(message);
    }

}
