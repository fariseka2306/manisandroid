package com.ebizu.manis.manager.accountlistmenu;

import android.content.Context;

import com.ebizu.manis.root.BaseActivity;
import com.ebizu.manis.view.adapter.account.AccountListMenuAdapter;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class AccountListMenuManager {

    private Context context;

    public AccountListMenuManager(Context context){
        this.context = context;
    }

    private static AccountListMenu[] accountList = {
            new SavedListMenu(),
            new PurchaseHistoryMenu(),
            new SnapHistoryListMenu(),
            new ProfileListMenu(),
            new FaqsListMenu(),
            new SnapGuideListMenu(),
            new ChatListMenu(),
            new SettingsListMenu(),
            new InviteSnapListMenu(),
            new RedemptionListMenu(),
            new InterestListMenu()
    };

    public static void addAccountListMenu(AccountListMenuAdapter accountListMenuAdapter) {
        for (AccountListMenu accountListMenu : accountList) {
            accountListMenuAdapter.addAccountList(accountListMenu);
        }
    }

    public static void onClickMenu(BaseActivity baseActivity, AccountListMenu accountListMenu) {
        for (AccountListMenu accountListMenuCheck : accountList) {
            if (accountListMenu.id() == accountListMenuCheck.id()) {
                accountListMenuCheck.click(baseActivity);
            }
        }
    }
}
