package com.ebizu.manis.manager.categorycolor;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.ebizu.manis.R;

/**
 * Created by abizu-alvio on 7/18/2017.
 */

public class CategoryColorYellow implements CategoryColor {
    @Override
    public int index() {
        return 2;
    }

    @Override
    public int color(Context context) {
        return ContextCompat.getColor(context, R.color.color_graph_yellow);
    }
}
