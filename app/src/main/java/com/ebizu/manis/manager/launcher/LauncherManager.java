package com.ebizu.manis.manager.launcher;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Ebizu-User on 23/07/2017.
 */

public class LauncherManager {

    static LauncherPriority[] launcherPriorities = {
            new OnBoardActivityLauncher(),
            new InterestActivityLauncher(),
            new LoginActivityLauncher(),
            new MainActivityLaiuncher()
    };

    static LauncherPriority[] launchPrioritiesLoginPhone = {
            new EditProfileLauncher(),
            new InterestActivityLauncher(),
            new MainActivityLaiuncher()
    };

    static LauncherPriority[] launchPrioritiesLoginEmail = {
            new InterestActivityLauncher(),
            new MainActivityLaiuncher()
    };

    public static Intent getIntentNextSplash(AppCompatActivity activity, Context context) {
        for (LauncherPriority launcherPriority : launcherPriorities) {
            if (launcherPriority.notCompleted(context)) {
                return launcherPriority.getIntent(activity);
            }
        }
        return new LoginActivityLauncher().getIntent(activity);
    }

    public static Intent getIntentNextLoginPhone(AppCompatActivity activity, Context context, int requestCode) {
        for (LauncherPriority launcherPriority : launchPrioritiesLoginPhone) {
            if (launcherPriority.notCompleted(context, requestCode)) {
                return launcherPriority.getIntent(activity);
            }
        }
        return new MainActivityLaiuncher().getIntent(activity);
    }

    public static Intent getIntentNextLoginEmail(AppCompatActivity activity, Context context) {
        for (LauncherPriority launcherPriority : launchPrioritiesLoginEmail) {
            if (launcherPriority.notCompleted(context)) {
                return launcherPriority.getIntent(activity);
            }
        }
        return new MainActivityLaiuncher().getIntent(activity);
    }
}