package com.ebizu.manis.manager.versioning;

import android.content.Context;

import com.ebizu.manis.model.Versioning;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by mac on 8/22/17.
 */

public interface VersioningStatus {
    String status();
    void showVersioningStatus(Versioning versioning, Context context, BaseActivity baseActivity);
}
