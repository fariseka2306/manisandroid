package com.ebizu.manis.manager.multiplier;

import android.view.View;
import android.widget.LinearLayout;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.service.manis.requestbody.StoreBody;

/**
 * Created by firef on 7/13/2017.
 */

public class MultiplierZero extends MultiplierBehaviour implements ActionMultiplier {
    @Override
    public int id() {
        return R.id.item_tab_store_multiplier_ticket;
    }


    @Override
    public StoreBody getStoreBody() {
        StoreBody storeBody = new StoreBody(ConfigManager.Store.FIRST_PAGE,
                ConfigManager.Store.MAX_PAGE_SIZE, ConfigManager.Store.MULTIPLIER_TICKET,
                ConfigManager.Store.MULTIPLIER_TIER_LONGTAIL);
        return storeBody;
    }

    @Override
    public int multiplier() {
        return 0;
    }

    @Override
    public int drawable() {
        return  R.drawable.drawable_multiplier_zero;
    }

    @Override
    public int drawable(String merchantTier) {
        if (merchantTier.equalsIgnoreCase(ConfigManager.Store.MULTIPLIER_TIER_LONGTAIL)) {
            return R.drawable.ticket_icon;
        } else {
            return R.drawable.drawable_multiplier_zero;
        }
    }

    @Override
    public String text() {
        return "0x";
    }

    @Override
    public String text(String merchantTier) {
        return merchantTier.equalsIgnoreCase(ConfigManager.Store.MULTIPLIER_TIER_LONGTAIL) ? "" : "0x";
    }

    @Override
    public String merchantTier() {
        return ConfigManager.Store.MULTIPLIER_TIER_LONGTAIL;
    }

    @Override
    public View bottomLineView(LinearLayout viewGroupMultiplier) {
        return (View) viewGroupMultiplier.findViewById(R.id.multiplier_line_zero);
    }
}
