package com.ebizu.manis.manager.accountlistsettingsabout;

import android.app.Activity;
import android.content.Context;

import com.ebizu.manis.root.BaseActivity;

/**
 * Created by abizu-alvio on 7/21/2017.
 */

public interface AccountListSettingsAbout {
    int id();

    int name();

    String build(Activity activity);

    void click(Context context);

    void click(BaseActivity baseActivity);
}
