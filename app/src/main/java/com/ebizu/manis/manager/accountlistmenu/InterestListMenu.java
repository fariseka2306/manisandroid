package com.ebizu.manis.manager.accountlistmenu;

import android.app.Activity;
import android.content.Intent;

import com.ebizu.manis.R;
import com.ebizu.manis.helper.ConfigManager;
import com.ebizu.manis.mvp.interest.InterestActivity;
import com.ebizu.manis.root.BaseActivity;

/**
 * Created by abizu-alvio on 7/11/2017.
 */

public class InterestListMenu implements AccountListMenu {
    @Override
    public int name() {
        return R.string.fa_txt_editinterest;
    }

    @Override
    public int thumbnail() {
        return R.drawable.account_settings_edit_interest;
    }

    @Override
    public int id() {
        return 10;
    }

    @Override
    public void click(BaseActivity baseActivity) {
        Intent intent = new Intent(baseActivity, InterestActivity.class);
        baseActivity.startActivity(intent);
        baseActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        baseActivity.getAnalyticManager().trackEvent(
                ConfigManager.Analytic.Category.FRAGMENT_ACCOUNT,
                ConfigManager.Analytic.Action.CLICK,
                "Button Edit InterestActivity");
    }

}
