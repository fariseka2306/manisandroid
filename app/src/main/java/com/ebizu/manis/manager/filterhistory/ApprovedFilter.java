package com.ebizu.manis.manager.filterhistory;

import com.ebizu.manis.helper.ConfigManager;

/**
 * Created by Raden on 7/25/17.
 */

public class ApprovedFilter implements FilterSpendingBar {

    @Override
    public String name() {
        return "approved";
    }

    @Override
    public int value() {
        return ConfigManager.SnapViewHistory.STATUS_APPROVED;
    }

    @Override
    public int position() {
        return 1;
    }
}
