package com.ebizu.manis.manager.launcher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.ebizu.manis.mvp.login.LoginActivity;
import com.ebizu.manis.preference.ManisSession;

/**
 * Created by Ebizu-User on 23/07/2017.
 */

public class LoginActivityLauncher implements LauncherPriority {

    @Override
    public boolean notCompleted(Context context) {
        ManisSession manisSession = new ManisSession(context);
        return !manisSession.isLoggedIn();
    }

    @Override
    public boolean notCompleted(Context context, int requestCode) {
        return false;
    }

    @Override
    public Intent getIntent(Activity activity) {
        return new Intent(activity, LoginActivity.class);
    }
}
