package com.ebizu.manis.manager.filterhistory;

/**
 * Created by Raden on 7/25/17.
 */

public interface FilterSpendingBar {
    String name();

    int value();

    int position();
}
