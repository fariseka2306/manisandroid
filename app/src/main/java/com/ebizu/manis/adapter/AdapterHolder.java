package com.ebizu.manis.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class AdapterHolder extends FragmentPagerAdapter {

    private List<Fragment> fragments;

    public AdapterHolder(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
        System.out.println("COUNT FRAGMENT = " + fragments.size());
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}