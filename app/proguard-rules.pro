# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\AndroidSDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keep class org.apache.http.** { *; }
-keepclassmembers class org.apache.http.** {*;}
-dontwarn org.apache.**

-keep class android.net.http.** { *; }
-keepclassmembers class android.net.http.** {*;}
-dontwarn android.net.**

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep class com.uber.sdk.** { *; }
-dontwarn com.uber.sdk.**

-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**

-keep class io.realm.** { *; }
-dontwarn io.realm.**

-keep class com.ebizu.ebn.** { *; }
-dontwarn com.ebizu.ebn.**

-keep class com.ebizu.manis.views.activities.ImageProcessingActivity { *; }
-dontwarn com.ebizu.manis.views.activities.ImageProcessingActivity

-keep class com.ebizu.manis.views.fragments.CropImageFragment { *; }
-dontwarn com.ebizu.manis.views.fragments.CropImageFragment

-dontwarn com.squareup.**
-dontwarn okio.**

-dontnote junit.framework.**
-dontnote junit.runner.**

-dontwarn android.test.**
-dontwarn android.support.test.**
-dontwarn org.junit.**
-dontwarn org.hamcrest.**
-dontwarn com.squareup.javawriter.JavaWriter

-keep class com.viewpagerindicator.LinePageIndicator { *; }
-dontwarn com.viewpagerindicator.LinePageIndicator

-keep class com.abbyy.mobile.** { *; }
-dontwarn com.abbyy.mobile.**

-keepclassmembers class * extends java.lang.Enum {
    <fields>;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

## OkHttp
#-keepattributes Signature
#-keepattributes *Annotation*
#-keep class okhttp3.** { *; }
#-keep interface okhttp3.** { *; }
#-dontwarn okhttp3.**
#
#-dontwarn retrofit2.**
#-keep class retrofit2.** { *; }
#-keepattributes Signature
#-keepattributes Exceptions
#
#-keepclasseswithmembers class * {
#    @retrofit2.http.* <methods>;
#}

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature,RuntimeVisibleAnnotations,AnnotationDefault

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }
-keep class com.ebizu.manis.sdk.rest.data.** { <fields>; }

##---------------End: proguard configuration for Gson  ----------

-keep class com.ebizu.sdk.** { *; }
-dontwarn com.ebizu.sdk.**

-keep class com.ebizu.manis.services.** { *; }
-dontwarn com.ebizu.manis.services.**

-keepclassmembers class * {
   @com.google.api.client.util.Key <fields>;
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}

-keep @interface android.support.annotation.Keep
-keep @android.support.annotation.Keep class *
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <fields>;
}
-keepclasseswithmembers class * {
  @android.support.annotation.Keep <methods>;
}

-keep @interface com.google.android.gms.common.annotation.KeepName
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
  @com.google.android.gms.common.annotation.KeepName *;
}

-keep @interface com.google.android.gms.common.util.DynamiteApi
-keep public @com.google.android.gms.common.util.DynamiteApi class * {
  public <fields>;
  public <methods>;
}


##---------------Begin: proguard configuration for LitePal  ----------
-keep class org.litepal.** {
    *;
}
-keep class * extends org.litepal.crud.DataSupport {
    *;
}
##---------------End: proguard configuration for LitePal  ----------

-dontwarn android.security.NetworkSecurityPolicy

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-printseeds seeds.txt
-printusage unused.txt
-printmapping mapping.txt
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable
-optimizations !code/simplification/arithmetic,!code/simplification/cast,!field/*,!class/merging/*
-optimizationpasses 5
-allowaccessmodification
-dontpreverify
-repackageclasses ''

-ignorewarnings
-keep class * {
    public private *;
}

-dontwarn java.lang.invoke**

-dontwarn com.google.firebase.appindexing.**

# RxJava setting
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
    long producerIndex;
    long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}